<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Helpers\Midtrans;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use App\Model\Transaction;
use HansSchouten\LaravelPageBuilder\LaravelPageBuilder;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx\Rels;

Route::get('/', function () {
    return view('login');
});

Auth::routes();


Route::get('/home', 'HomeController@index')->name('home');
// Route::get('/testing', 'HomeController@testingABC');
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
Route::group(['prefix' => 'reset-password'], function () {
    Route::get('show', 'Customer\PasswordResetController@show')->name('reset.show');
    Route::post('/forget-password', 'Customer\PasswordResetController@postEmail');
    Route::get('/{token}', 'Customer\PasswordResetController@getPassword');
    Route::post('/update-password', 'Customer\PasswordResetController@updatePassword');
});

Route::get('storage/foto-ktp/{filename}', function ($filename) {
    $userLogin = Auth::user()->id;
    $path = storage_path('app/public/foto_ktp/' . $filename);
    if (!File::exists($path)) {
        abort(404);
    }
    $file = File::get($path);
    $type = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});

Route::get('storage/sales-training-image/{filename}', function ($filename) {
    $userLogin = Auth::user()->id;
    $path = storage_path('app/public/salesTrainingImages/' . $filename);
    if (!File::exists($path)) {
        abort(404);
    }
    $file = File::get($path);
    $type = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});

Route::get('storage/image-transaction/{filename}', function ($filename) {
    $userLogin = Auth::user()->id;
    $path = storage_path('app/public/transaction-attachment/' . $filename);
    if (!File::exists($path)) {
        abort(404);
    }
    $file = File::get($path);
    $type = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});

Route::get('storage/image-kategori/{filename}', function ($filename) {
    $userLogin = Auth::user()->id;
    $path = storage_path('app/public/kategori/' . $filename);
    if (!File::exists($path)) {
        abort(404);
    }
    $file = File::get($path);
    $type = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});

Route::get('storage/avatar/{filename}', function ($filename) {
    $userLogin = Auth::user()->id;
    $path = storage_path('app/public/avatar-users/' . $filename);
    if (!File::exists($path)) {
        abort(404);
    }
    $file = File::get($path);
    $type = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});


Route::get('storage/bank-icon/{filename}', function ($filename) {
    $userLogin = Auth::user()->id;
    $path = storage_path('app/public/bankIcon/' . $filename);
    if (!File::exists($path)) {
        abort(404);
    }
    $file = File::get($path);
    $type = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});
Route::get('storage/events-image/{filename}', function ($filename) {
    $userLogin = Auth::user()->id;
    $path = storage_path('app/public/eventsImages/' . $filename);
    if (!File::exists($path)) {
        abort(404);
    }
    $file = File::get($path);
    $type = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});

Route::get('storage/product-image/{filename}', function ($filename) {
    $userLogin = Auth::user()->id;
    $path = storage_path('app/public/productImages/' . $filename);
    if (!File::exists($path)) {
        abort(404);
    }
    $file = File::get($path);
    $type = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});

Route::get('storage/reguler-package/{filename}', function ($filename) {
    $userLogin = Auth::user()->id;
    $path = storage_path('app/public/paketRegulers/' . $filename);
    if (!File::exists($path)) {
        abort(404);
    }
    $file = File::get($path);
    $type = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});

Route::get('storage/package-upgrade-ms/{filename}', function ($filename) {
    $userLogin = Auth::user()->id;
    $path = storage_path('app/public/paketUpdateMS/' . $filename);
    if (!File::exists($path)) {
        abort(404);
    }
    $file = File::get($path);
    $type = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});

Route::get('storage/img_slide-show/{filename}', function ($filename) {
    $userLogin = Auth::user()->id;
    $path = storage_path('app/public/slide-show/' . $filename);
    if (!File::exists($path)) {
        abort(404);
    }
    $file = File::get($path);
    $type = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});

Route::get('storage/konten/{filename}', function ($filename) {
    $path = storage_path('app/public/konten/' . $filename);
    if (!File::exists($path)) { 
        abort(404);
    }
    $file = File::get($path);
    $type = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
})->name('img-konten');


// landingpage show image 
Route::get('storage/landing-page-image-1/{filename}', function ($filename) {
    $path = storage_path('app/public/landing-page-images/' . $filename);
    if (!File::exists($path)) {
        abort(404);
    }
    $file = File::get($path);
    $type = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});

Route::get('storage/landing-page-image-2/{filename}', function ($filename) {
    $path = storage_path('app/public/landing-page-images/' . $filename);
    if (!File::exists($path)) {
        abort(404);
    }
    $file = File::get($path);
    $type = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});

Route::get('storage/landing-page-image-3/{filename}', function ($filename) {
    $path = storage_path('app/public/landing-page-images/' . $filename);
    if (!File::exists($path)) {
        abort(404);
    }
    $file = File::get($path);
    $type = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});

Route::get('storage/landing-page-image-4/{filename}', function ($filename) {
    $path = storage_path('app/public/landing-page-images/' . $filename);
    if (!File::exists($path)) {
        abort(404);
    }
    $file = File::get($path);
    $type = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});

Route::get('storage/landing-page-image-5/{filename}', function ($filename) {
    $path = storage_path('app/public/landing-page-images/' . $filename);
    if (!File::exists($path)) {
        abort(404);
    }
    $file = File::get($path);
    $type = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});

Route::get('storage/landing-page-image-6/{filename}', function ($filename) {
    $path = storage_path('app/public/landing-page-images/' . $filename);
    if (!File::exists($path)) {
        abort(404);
    }
    $file = File::get($path);
    $type = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});

Route::get('storage/landing-page-image-7/{filename}', function ($filename) {
    $path = storage_path('app/public/landing-page-images/' . $filename);
    if (!File::exists($path)) {
        abort(404);
    }
    $file = File::get($path);
    $type = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});

Route::get('storage/landing-page-image-8/{filename}', function ($filename) {
    $path = storage_path('app/public/landing-page-images/' . $filename);
    if (!File::exists($path)) {
        abort(404);
    }
    $file = File::get($path);
    $type = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});

Route::get('storage/landing-page-image-9/{filename}', function ($filename) {
    $path = storage_path('app/public/landing-page-images/' . $filename);
    if (!File::exists($path)) {
        abort(404);
    }
    $file = File::get($path);
    $type = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});

Route::get('storage/landing-page-image-10/{filename}', function ($filename) {
    $path = storage_path('app/public/landing-page-images/' . $filename);
    if (!File::exists($path)) {
        abort(404);
    }
    $file = File::get($path);
    $type = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});

Route::get('storage/landing-page-image-11/{filename}', function ($filename) {
    $path = storage_path('app/public/landing-page-images/' . $filename);
    if (!File::exists($path)) {
        abort(404);
    }
    $file = File::get($path);
    $type = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});

Route::get('storage/logo-email/{filename}', function ($filename) {
    $path = storage_path('app/public/email-logo/' . $filename);
    if (!File::exists($path)) {
        abort(404);
    }
    $file = File::get($path);
    $type = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});

Route::get('/pages/{is_template}/{phone?}', 'Customer\LandingPagesController@pageTemplate')->name('page-template-1.show');
// Route::get('/pages', 'Customer\LandingPagesController@pageTemplate')->name('page-template-1.show');
// Route::get('/pages/{is_template}/{slug}/{phone}/{message}', 'Customer\LandingPagesController@pageTemplate')->name('page-template-1.show');
// Route::get('/template/2', 'Customer\LandingPagesController@pageTemplate2')->name('page-template-2.show');
// Route::get('/template/3', 'Customer\LandingPagesController@pageTemplate3')->name('page-template-3.show');
// Route::get('/template/4', 'Customer\LandingPagesController@pageTemplate4')->name('page-template-4.show');
// Route::get('/template/5', 'Customer\LandingPagesController@pageTemplate5')->name('page-template-5.show');
// Route::get('/template/6', 'Customer\LandingPagesController@pageTemplate6')->name('page-template-6.show');

Route::group(['prefix' => 'customer','middleware'=>'auth'], function () {

    Route::get('/dasboard', 'Customer\CustomerController@showDashboard')->name('cs-dashboard');
    Route::get('/status-pengiriman', 'Customer\CustomerController@cekResi');

    Route::group(['prefix' => 'landing-pages'], function () {
    });


    Route::group(['prefix' => 'my-profile'], function () {
        Route::get('show', 'Customer\MyProfileController@show')->name('profile.show');
        Route::get('update/{id}', 'Customer\MyProfileController@update')->name('profile.update');
        Route::post('update-my-profile/{id}', 'Customer\MyProfileController@updateDataProfile')->name('profile.update');
        Route::post('upload-image', 'Customer\MyProfileController@uploadImage')->name('upload.update');
        Route::post('change-password', 'Customer\MyProfileController@changePassword')->name('password.update');
        Route::post('/whatsapp_no-update/{id}', 'Customer\MyProfileController@updateWhatsappNo')->name('wa.update');
    });

    

    Route::group(['prefix' => 'news-update'], function () {
        Route::get('show', 'Customer\NewsAndUpdateController@newsAndUpdate')->name('news.customer');
        Route::get('detail/{id}', 'Customer\NewsAndUpdateController@detail');
    });

    Route::group(['prefix' => 'sales-training'], function () {
        Route::get('show', 'Customer\SalesTrainingController@salesTraining'); 
        Route::get('detail/{id}', 'Customer\SalesTrainingController@salesTrainingDetail'); // tambahkan id
    });

    Route::group(['prefix' => 'live-training'], function () {
        Route::get('show', 'Customer\LiveTrainingController@liveTraining')->name('live.training.show'); 
        Route::get('detail/{id}', 'Customer\LiveTrainingController@liveTrainingDetail'); // tambahkan id
        Route::get('payment/{id}', 'Customer\LiveTrainingController@paymentCard')->name('live.training.payment'); // tambahkan id
        Route::post('payment/{id}', 'Customer\LiveTrainingController@checkoutEvent');
    });

    Route::group(['prefix' => 'order-produk'], function () {
        Route::match(['get','post'],'show', 'Customer\ListProductAndPromoController@orderProduk')->name('cs-order-produk.show');
        Route::get('generate', 'Customer\ListProductAndPromoController@generateOrderCode');
        Route::get('detail/{id}', 'Customer\ListProductAndPromoController@detailProduk');
        Route::match(['post','get'],'checkout', 'Customer\ListProductAndPromoController@checkout');
        // Route::get('cart', 'Customer\ListProductAndPromoController@cart')->name('pageCart');
        Route::match(['post','get'],'cart', 'Customer\ListProductAndPromoController@cart')->name('pageCart');
        Route::get('add-to-cart/{id}/{qty}', 'Customer\ListProductAndPromoController@addToChart');
        Route::patch('update-cart', 'Customer\ListProductAndPromoController@update');
        Route::post('remove-from-cart', 'Customer\ListProductAndPromoController@remove')->name('delete-cart-cs');
        Route::get('province', 'Customer\ListProductAndPromoController@get_province');
        Route::get('kota/{id}','Customer\ListProductAndPromoController@get_city');
        Route::get('origin={city_origin}&destination={city_destination}&weight={weight}&courier={courier}','Customer\ListProductAndPromoController@get_ongkir');
        Route::get('checkout/{id}/{qty}','Customer\ListProductAndPromoController@itemCheckout');
    });

    Route::group(['prefix' => 'new-promo'], function () {
        Route::get('show', 'Customer\ListProductAndPromoController@orderProdukPromo');
    });

    Route::group(['prefix' => 'member'], function () {
        Route::get('show', 'Customer\MembershipsController@showMemberShip')->name('member.show');
        Route::get('upgrade-member-mobile-stokiest/{id}', 'Customer\MembershipsController@upgradeMember');
        Route::post('upgrade-mobile-stokiest', 'Customer\MembershipsController@storeMemberMS');
        Route::get('province', 'Customer\ListProductAndPromoController@get_province');
        Route::get('kota/{id}','Customer\ListProductAndPromoController@get_city');
    });

    Route::group(['prefix' => 'status-order'], function () {
        Route::get('show', 'Customer\StatusOrderController@show')->name('statusOrder');
        Route::get('detail/{orderId}', 'Customer\StatusOrderController@statusOrderDetail'); // tambahkan id
        Route::get('pembayaran/{orderId}', 'Customer\StatusOrderController@payment'); // tambahkan id
        Route::get('konfirmasi-bayar/{orderId}', 'Customer\StatusOrderController@konfirmasiBayar')->name('payment.detail'); 
        Route::post('konfirmasi/{orderId?}','Customer\StatusOrderController@konfirmasiPayment');
        Route::post('konfirmasi/pesanan-sampai/{orderId}','Customer\StatusOrderController@konfirmasiPesananSampai');
        Route::post('/update-status/{orderId}', 'Admin\AllTransactionsController@updateStatus')->name('transaksi.update.status');
    });

    Route::group(['prefix' => 'menu-mobile-stokiest'], function () {
        Route::get('show', 'Customer\MenuMobileStokiestController@showMenuBaruMS');
    });

    Route::group(['prefix' => 'bonus-member-ms'], function () {
        Route::get('show', 'Customer\MenuMobileStokiestController@showBonusMember');
    });

    Route::group(['prefix' => 'input-reguler-member'], function () {
        Route::get('show', 'Customer\MenuMobileStokiestController@showPaketReguler');
        Route::get('detail/{id}', 'Customer\MenuMobileStokiestController@detailPaketReguler');
        Route::get('package-checkout/{id}', 'Customer\MenuMobileStokiestController@packageCheckout');
        Route::get('province', 'Customer\MenuMobileStokiestController@get_province');
        Route::get('/kota/{id}','Customer\MenuMobileStokiestController@get_city');
        Route::get('/origin={city_origin}&destination={city_destination}&weight={weight}&courier={courier}','Customer\MenuMobileStokiestController@get_ongkir');
        Route::post('checkout', 'Customer\MenuMobileStokiestController@checkout');
    });

    Route::group(['prefix' => 'list-team-upgrade-ms'], function () {
        Route::get('show', 'Customer\MyTeamUpgradeMsController@index')->name('teams.show');
        Route::get('update-posisi/{id}', 'Customer\MyTeamUpgradeMsController@update');
        Route::get('update-posisi-random/{id}', 'Customer\MyTeamUpgradeMsController@updateRandom');
        Route::post('store-position', 'Customer\MyTeamUpgradeMsController@storePosition');
        Route::post('store-random-position', 'Customer\MyTeamUpgradeMsController@storeRandomPosition');
    });

    Route::group(['prefix' => 'my-team'], function () {
        Route::get('show', 'Customer\MyTeamController@index');
    });

    Route::group(['prefix' => 'starter-kit-download'], function () {
        Route::get('/', 'Customer\StarterKitController@index')->name('skd.show');
    });

    Route::group(['prefix' => 'myproduct'], function () {
        Route::get('','Customer\CSProductController@index')->name('myproduct');
        Route::match(['post','delete'],'sent','Customer\CSProductController@sendItem')->name('sent-myproduct');
        Route::get('cart','Customer\CSProductController@cart')->name('myproduct-cart');
        Route::post('cart','Customer\CSProductController@checkout');
    });

    Route::group(['prefix' => 'konten'], function () {
        Route::get('', 'Customer\KontenController@index')->name('konten.cs');
        Route::get('{id}', 'Customer\KontenController@detail')->name('konten.cs.detail');
    });
    
    Route::group(['prefix' => 'paket-ms-upgrade'], function () {
        Route::get('show', 'Customer\PaketMsUpgradeController@index')->name('paket-ms.show');
        Route::get('detail/{id}', 'Customer\PaketMsUpgradeController@detail');
        Route::get('package-upgrade/{id}', 'Customer\PaketMsUpgradeController@checkoutPaketMs');
        Route::post('checkout', 'Customer\PaketMsUpgradeController@checkout');
    });

    Route::group(['prefix' => 'landing-pages'], function () {
        Route::get('show', 'Customer\LandingPagesController@showPage')->name('landing-page.show');
    });

    Route::group(['prefix' => 'syarat-ketentuan'], function () {
        Route::get('show', 'Customer\FAQController@index')->name('s-k.show');
        Route::get('detail/{id}', 'Customer\FAQController@detail')->name('s-k.detail');
    });

    Route::group(['prefix' => 'abot-panel-hwi'], function () {
        Route::get('show', 'Customer\AboutPanelHWIController@index')->name('about-hwi.show');
    });
});

Route::group(['prefix' => 'admin','middleware'=>['auth']], function () {

    Route::get('dashboard/{type?}', 'Admin\DasboardAdminController@index')->name('adm-dashboards');

    Route::group(['prefix' => 'role'], function () {
        Route::get('show', 'Admin\RoleController@index')->name('role.show');
        Route::get('create', 'Admin\RoleController@create')->name('role.create');
        Route::post('store', 'Admin\RoleController@store')->name('role.store');
        Route::post('destroy/{id}', 'Admin\RoleController@destroy')->name('role.destroy');
        Route::post('update/{id}', 'Admin\RoleController@edit')->name('role.update');
        Route::post('edit/{id}', 'Admin\RoleController@update')->name('role.edit');
    });

    Route::group(['prefix' => 'general-setting'], function () {
        Route::get('show', 'Admin\GeneralSettingController@index')->name('general-setting.show');
        Route::post('update', 'Admin\GeneralSettingController@updateSetting')->name('general-setting.update');
    });

    Route::group(['prefix' => 'panel-hwi'], function () {
        Route::get('show', 'Admin\PanelHWIController@index')->name('hwi.show');
        Route::get('create', 'Admin\PanelHWIController@create')->name('hwi.create');
        Route::get('edit/{id}', 'Admin\PanelHWIController@edit')->name('hwi.edit');
        Route::post('store', 'Admin\PanelHWIController@store')->name('hwi.store');
        Route::post('update/{id}', 'Admin\PanelHWIController@update')->name('hwi.update');
        Route::post('destroy/{id}', 'Admin\PanelHWIController@destroy')->name('hwi.destroy');
    });

    Route::group(['prefix' => 'my-profile'], function () {
        Route::get('show', 'Customer\MyProfileController@show')->name('profile.admin.show');
        Route::get('update/{id}', 'Customer\MyProfileController@update')->name('profile.admin.update');
        Route::post('update-my-profile-admin/{id}', 'Customer\MyProfileController@updateDataProfileAdmin')->name('profile.admin.update');
        // Route::post('upload-image', 'Customer\MyProfileController@uploadImage')->name('upload.update');
        // Route::post('change-password', 'Customer\MyProfileController@changePassword')->name('password.update');
        // Route::post('/whatsapp_no-update/{id}', 'Customer\MyProfileController@updateWhatsappNo')->name('wa.update');
    });

    Route::group(['prefix' => 'courier'], function () {
        Route::get('show', 'Admin\KurirController@index')->name('kurir.show');
        Route::get('update/{id}', 'Admin\KurirController@update')->name('kurir.update');
        Route::post('edit/{id}', 'Admin\KurirController@edit')->name('kurir.edit');
    });

    Route::group(['prefix' => 'new-ms-upgrade-request'], function () {
        Route::get('show', 'Admin\NewMsUpgradeRequestController@index')->name('NewMS.show');
        Route::get('detail/{id}', 'Admin\NewMsUpgradeRequestController@detail')->name('NewMS.detail');
        Route::post('update/{id}', 'Admin\NewMsUpgradeRequestController@update')->name('NewMS.update');
        Route::post('approve-ms-update/{id}', 'Admin\NewMsUpgradeRequestController@approveMS')->name('NewMS.approve.MS');
        Route::get('edit/{id}', 'Admin\NewMsUpgradeRequestController@edit')->name('NewMS.edit');
    });

    Route::group(['prefix' => 'new-member-request'], function () {
        Route::get('show', 'Admin\NewMemberRequestController@index')->name('member-request.show');
        Route::get('update/{id}', 'Admin\NewMemberRequestController@update')->name('member-request.update');
        Route::post('update-data-user/{id}', 'Admin\NewMemberRequestController@edit');
        Route::post('approve-new-memner/{id}', 'Admin\NewMemberRequestController@approveNewUser');
    });

    Route::group(['prefix' => 'new-team-upgrade-ms'], function () {
        Route::get('show', 'Admin\NewTeamUpgradeMsController@index')->name('upgrade-request.show');
        Route::get('edit/{id}', 'Admin\NewTeamUpgradeMsController@edit')->name('upgrade-request.edit');
        Route::get('detail/{id}', 'Admin\NewTeamUpgradeMsController@detail')->name('upgrade-request.detail');
        Route::post('update/{id}', 'Admin\NewTeamUpgradeMsController@update')->name('upgrade-request.update');
        Route::post('my-team-approve-ms/{id}', 'Admin\NewTeamUpgradeMsController@approveMS')->name('NewMS.approve.MS');
    });

    Route::group(['prefix' => 'user'], function () {
        Route::get('show', 'Admin\UserController@index')->name('users.show');
        Route::get('create', 'Admin\UserController@create')->name('users.create');
        Route::post('store', 'Admin\UserController@store')->name('users.store');
        Route::post('destroy/{id}', 'Admin\UserController@destroy')->name('users.destroy');
        Route::get('update/{id}', 'Admin\UserController@edit')->name('users.update');
        Route::post('edit/{id}', 'Admin\UserController@update')->name('users.edit');
        Route::get('/export-excel', 'Admin\UserController@exportExcel');
    });

    Route::group(['prefix' => 'list-admin'], function () {
        Route::get('show', 'Admin\ListAdminController@index')->name('admin.show');
        Route::get('create', 'Admin\ListAdminController@create')->name('admin.create');
        Route::post('store', 'Admin\ListAdminController@store')->name('admin.store');
        Route::post('destroy/{id}', 'Admin\ListAdminController@destroy')->name('admin.destroy');
        Route::get('update/{id}', 'Admin\ListAdminController@edit')->name('admin.update');
        Route::post('edit/{id}', 'Admin\ListAdminController@update')->name('admin.edit');
    });

    Route::group(['prefix' => 'product'], function () {
        Route::get('show', 'Admin\ProductController@index')->name('product.show');
        Route::get('create', 'Admin\ProductController@create')->name('product.create');
        Route::post('store', 'Admin\ProductController@store')->name('product.store');
        Route::post('destroy/{id}', 'Admin\ProductController@destroy')->name('product.destroy');
        Route::get('update/{id}', 'Admin\ProductController@edit')->name('product.update');
        Route::post('edit/{id}', 'Admin\ProductController@update')->name('product.edit');
        Route::get('detail/{id}', 'Admin\ProductController@detail')->name('product.detail');
    });

    Route::group(['prefix' => 'news-and-update'], function () {
        Route::get('show', 'Admin\NewsAndUpdateController@index')->name('news.show');
        Route::get('create', 'Admin\NewsAndUpdateController@create')->name('news.create');
        Route::post('store', 'Admin\NewsAndUpdateController@store')->name('news.store');
        Route::post('destroy/{id}', 'Admin\NewsAndUpdateController@destroy')->name('news.destroy');
        Route::get('update/{id}', 'Admin\NewsAndUpdateController@edit')->name('news.update');
        Route::get('detail/{id}', 'Admin\NewsAndUpdateController@detail')->name('news.detail');
        Route::post('edit/{id}', 'Admin\NewsAndUpdateController@update')->name('news.edit');
        
    });

    Route::group(['prefix' => 'sales-and-training'], function () {
        Route::get('show', 'Admin\SalesAndTrainingController@index')->name('sales.show');
        Route::get('get_vidio/{id}', 'Admin\SalesAndTrainingController@getVidio')->name('sales.show.vidio');
        Route::get('create', 'Admin\SalesAndTrainingController@create')->name('sales.create');
        Route::post('store', 'Admin\SalesAndTrainingController@store')->name('sales.store');
        Route::post('store_vidio', 'Admin\SalesAndTrainingController@storeVidio')->name('sales.store.vidio');
        Route::post('destroy_vidio/{id}', 'Admin\SalesAndTrainingController@destroyVidio')->name('sales.destroy.vidio');
        Route::post('destroy/{id}', 'Admin\SalesAndTrainingController@destroy')->name('sales.destroy');
        Route::get('update/{id}', 'Admin\SalesAndTrainingController@edit')->name('sales.update');
        Route::get('detail/{id}', 'Admin\SalesAndTrainingController@detail')->name('sales.detail');
        Route::post('edit/{id}', 'Admin\SalesAndTrainingController@update')->name('sales.edit');
        Route::post('edit_vidio/{id}', 'Admin\SalesAndTrainingController@updateVidio')->name('sales.edit.vidio');

    });

    Route::group(['prefix' => 'events'], function () {
        Route::get('show', 'Admin\EventsController@index')->name('events.show');
        Route::get('create', 'Admin\EventsController@create')->name('events.create');
        Route::post('store', 'Admin\EventsController@store')->name('events.store');
        Route::post('destroy/{id}', 'Admin\EventsController@destroy')->name('events.destroy');
        Route::get('update/{id}', 'Admin\EventsController@edit')->name('events.update');
        Route::get('detail/{id}', 'Admin\EventsController@detail')->name('events.detail');
        Route::post('edit/{id}', 'Admin\EventsController@update')->name('events.edit');
    });

    Route::group(['prefix' => 'mobile-stokiest'], function () {
        Route::get('show', 'Admin\MobileStokiestController@index')->name('MS.show');
        Route::get('create', 'Admin\MobileStokiestController@create')->name('MS.create');
        Route::post('store', 'Admin\MobileStokiestController@store')->name('MS.store');
        Route::post('destroy/{id}', 'Admin\MobileStokiestController@destroy')->name('MS.destroy');
        Route::get('update/{id}', 'Admin\MobileStokiestController@edit')->name('MS.update');
        Route::get('detail/{id}', 'Admin\MobileStokiestController@detail')->name('MS.detail');
        Route::post('edit/{id}', 'Admin\MobileStokiestController@update')->name('MS.edit');
    });

    Route::group(['prefix' => 'bonus-mobile-stokiest'], function () {
        Route::get('show', 'Admin\BonusMSController@index')->name('BonusMS.show');
        Route::get('create', 'Admin\BonusMSController@create')->name('BonusMS.create');
        Route::post('store', 'Admin\BonusMSController@store')->name('BonusMS.store');
        Route::post('destroy/{id}', 'Admin\BonusMSController@destroy')->name('BonusMS.destroy');
        Route::get('update/{id}', 'Admin\BonusMSController@edit')->name('BonusMS.update');
        Route::post('edit/{id}', 'Admin\BonusMSController@update')->name('BonusMS.edit');
        Route::post('deleteAll', 'Admin\BonusMSController@teruncate')->name('BonusMS.truncate');
    });

    Route::group(['prefix' => 'daftar-paket-reguler'], function () {
        Route::get('show', 'Admin\PaketRegulerController@index')->name('PR.show');
        Route::get('create', 'Admin\PaketRegulerController@create')->name('PR.create');
        Route::post('store', 'Admin\PaketRegulerController@store')->name('PR.store');
        Route::post('destroy/{id}', 'Admin\PaketRegulerController@destroy')->name('PR.destroy');
        Route::get('update/{id}', 'Admin\PaketRegulerController@edit')->name('PR.update');
        Route::post('edit/{id}', 'Admin\PaketRegulerController@update')->name('PR.edit');
        Route::post('deleteAll', 'Admin\PaketRegulerController@teruncate')->name('PR.truncate');
    });

    Route::group(['prefix' => 'email-template'], function () {
        Route::get('show', 'Admin\EmailController@index')->name('email.show');
        Route::get('update/{id}', 'Admin\EmailController@edit')->name('email.update');
        Route::post('edit/{id}', 'Admin\EmailController@update')->name('email.edit');
        Route::post('create-update-logo', 'Admin\EmailController@createUpdateLogo')->name('email.edit.logo');
    });

    Route::group(['prefix' => 'whatsapp-template'], function () {
        Route::get('show', 'Admin\WhatsAppTemplateController@index')->name('wa.show');
        Route::get('update/{id}', 'Admin\WhatsAppTemplateController@edit')->name('wa.update');
        Route::post('edit/{id}', 'Admin\WhatsAppTemplateController@update')->name('wa.edit');
    });

    Route::group(['prefix' => 'kategori'], function () {
        Route::get('/', 'Admin\KategoriController@index')->name('kategori');
        Route::match(['get','post'],'create','Admin\KategoriController@store')->name('kategori-create');
        Route::match(['get','post'],'edit/{id}','Admin\KategoriController@edit')->name('kategori-update');
        Route::post('destroy/{id}', 'Admin\KategoriController@destroy')->name('kategori-destroy');
    });

    Route::group(['prefix' => 'sub-kategori'], function () {
        Route::get('/', 'Admin\SubKategoriController@index')->name('sub-kategori');
        Route::match(['get','post'],'create','Admin\SubKategoriController@store')->name('sub-kategori-create');
        Route::match(['get','post'],'edit/{id}','Admin\SubKategoriController@edit')->name('sub-kategori-update');
        Route::post('destroy/{id}', 'Admin\SubKategoriController@destroy')->name('sub-kategori-destroy');
    });

    Route::group(['prefix' => 'konten'], function () {
        Route::get('/', 'Admin\KontenController@index')->name('konten');
        Route::match(['get','post'],'create','Admin\KontenController@store')->name('konten-create');
        Route::match(['get','post'],'edit/{id}','Admin\KontenController@edit')->name('konten-update');
        Route::post('destroy/{id}', 'Admin\KontenController@destroy')->name('konten-destroy');
    });

    Route::group(['prefix' => 'konten-dashboard'], function () {
        Route::get('/', 'Admin\KontenDashboardController@index')->name('konten.dashboard.show');
        Route::post('/store', 'Admin\KontenDashboardController@create');
        Route::post('/update/{id}', 'Admin\KontenDashboardController@update');
    });

    Route::group(['prefix' => 'starterkit-downloads'], function () {
        Route::get('/', 'Admin\StarterkitDownloadController@index')->name('download.show');
        Route::get('/create', 'Admin\StarterkitDownloadController@create')->name('download.create');
        Route::get('/show/{id}', 'Admin\StarterkitDownloadController@show')->name('download.edit');
        Route::post('/store', 'Admin\StarterkitDownloadController@store');
        Route::post('/update/{id}', 'Admin\StarterkitDownloadController@update');
        Route::post('/destroy/{id}', 'Admin\StarterkitDownloadController@destroy');
    });

    Route::group(['prefix' => 'member-performence'], function () {
        Route::get('/', 'Admin\MemberPerfomenceReportController@index')->name('m-p.show');
        Route::get('/download-report', 'Admin\MemberPerfomenceReportController@export')->name('m-p.download');
    });
    
    Route::group(['prefix' => 'report'], function () {
        Route::get('product/{type?}', 'Admin\ReportController@indexProduct')->name('report.product');
        Route::get('omset/{type?}', 'Admin\ReportController@indexOmset')->name('report.omset');
        Route::get('member/{type?}', 'Admin\ReportController@indexMember')->name('report.member');
    });

    Route::group(['prefix' => 'transactions'], function () {
        Route::get('show', 'Admin\TransactionController@index')->name('transaction.show');
        Route::get('get-product/{orderId}', 'Admin\TransactionController@showDetailProduct')->name('transaksi.detail');
        Route::get('get-address/{id}', 'Admin\TransactionController@getAddress')->name('transaksi.get-address');
        Route::post('ubah-alamat/{id}', 'Admin\TransactionController@updateAlamatPengiriman')->name('transaksi.ubah-alamat');
        Route::post('transaction-address/{orderId}', 'Admin\TransactionController@transactionAddress')->name('transaksi-address');
        Route::post('terima-pembayaran/{orderId}', 'Admin\TransactionController@acceptPayment')->name('transaksi.konfirmasi');
    });

    Route::group(['prefix' => 'payment'], function () {
        Route::get('/', 'Admin\PaymentController@index')->name('payment.show');
        Route::get('detail', 'Admin\PaymentController@detail')->name('payment.detail');
        Route::get('create', 'Admin\PaymentController@create')->name('payment.create');
        Route::post('store', 'Admin\PaymentController@store')->name('payment.store');
        Route::get('update/{id}', 'Admin\PaymentController@update')->name('payment.update');
        Route::get('ms-payment-update/{id}', 'Admin\PaymentController@msPaymentUpdate')->name('payment.update-ms-payment');
        Route::post('edit/{id}', 'Admin\PaymentController@edit')->name('payment.edit');
        Route::post('edit-ms-payment/{id}', 'Admin\PaymentController@editMsPayment')->name('ms-payment.edit');
        Route::post('destroy/{id}', 'Admin\PaymentController@destroy')->name('payment.destroy');
    });

    Route::group(['prefix' => 'update-resi'], function () {
        Route::get('/show', 'Admin\UpdateResiController@index')->name('resi.show');
        Route::get('/transaction-detail/{orderId}', 'Admin\UpdateResiController@detail')->name('resi.detail');
        Route::post('/update-resi-pengiriman/{orderId}', 'Admin\UpdateResiController@resi')->name('resi.resi.update');
    });

    Route::group(['prefix' => 'data-transactions'], function () {
        Route::match(['get','post'],'show', 'Admin\AllTransactionsController@index')->name('transaksi.show');
        // Route::match(['get','post'],'show', 'Customer\ListProductAndPromoController@orderProduk')->name('cs-order-produk.show');
        Route::get('/detail/{orderId}', 'Admin\AllTransactionsController@detail')->name('transaksi.detail');
        Route::post('/update-multiple-resi', 'Admin\AllTransactionsController@updateMultipleResi')->name('transaksi.update');
        Route::post('/update-status/{orderId}', 'Admin\AllTransactionsController@updateStatus')->name('transaksi.update.status');
        Route::post('konfirmasi/pesanan-sampai/{orderId}','Customer\StatusOrderController@konfirmasiPesananSampai');
        Route::post('/show/search','Admin\AllTransactionsController@search');
    });

    Route::group(['prefix' => 'kategori-produk'], function () {
        Route::get('/', 'Admin\KategoriProdukController@index')->name('kategoori-produk.show');
        Route::get('/create', 'Admin\KategoriProdukController@create')->name('kategoori-produk.create');
        Route::get('/edit/{id}', 'Admin\KategoriProdukController@edit')->name('kategoori-produk.edit');
        Route::post('/store', 'Admin\KategoriProdukController@store')->name('kategoori-produk.store');
        Route::post('/update/{id}', 'Admin\KategoriProdukController@update')->name('kategoori-produk.update');
        Route::post('/destroy/{id}', 'Admin\KategoriProdukController@destroy')->name('kategoori-produk.destroy');
        Route::post('deleteAll', 'Admin\KategoriProdukController@teruncate')->name('kategoori-produk.destroy');
    });

    Route::group(['prefix' => 'paket-upgrade-ms'], function () {
        Route::get('/show', 'Admin\PaketUpgradeMSController@index')->name('paket.upgrade.show');
        Route::get('/create', 'Admin\PaketUpgradeMSController@create')->name('paket.upgrade.create');
        Route::get('/edit/{id}', 'Admin\PaketUpgradeMSController@edit')->name('paket.upgrade.edit');
        Route::get('/detail/{id}', 'Admin\PaketUpgradeMSController@detail')->name('paket.upgrade.detail');
        Route::post('/store', 'Admin\PaketUpgradeMSController@store')->name('paket.upgrade.store');
        Route::post('/update/{id}', 'Admin\PaketUpgradeMSController@update')->name('paket.upgrade.update');
        Route::post('/destroy/{id}', 'Admin\PaketUpgradeMSController@destroy')->name('paket.upgrade.destroy');
    });

    Route::group(['prefix' => 'term-and-conditions'], function () {
        Route::get('/show', 'Admin\TermAndConditionController@index')->name('term-and-condition.show');
        Route::get('/update/{id}', 'Admin\TermAndConditionController@update')->name('term-and-condition.update');
        Route::get('/update-faq/{id}', 'Admin\TermAndConditionController@updateFaq')->name('term-and-condition.update.faq');
        Route::get('/create', 'Admin\TermAndConditionController@create')->name('term-and-condition.create');
        Route::post('/store', 'Admin\TermAndConditionController@store')->name('term-and-condition.store');
        Route::post('/store-faq', 'Admin\TermAndConditionController@storeFaq')->name('term-and-condition.store.faq');
        Route::post('/edit/{id}', 'Admin\TermAndConditionController@edit')->name('term-and-condition.edit');
        Route::post('/edit-faq/{id}', 'Admin\TermAndConditionController@editFaq')->name('term-and-condition.edit.faq');
        Route::post('/delete-faq/{id}', 'Admin\TermAndConditionController@deleteFaq')->name('term-and-condition.delete.faq');
        Route::post('/delete/{id}', 'Admin\TermAndConditionController@delete')->name('term-and-condition.delete');
    });

    Route::group(['prefix' => 'faq-kategori'], function () {
        Route::get('/show', 'Admin\FaqKategoriController@index')->name('faq.show');
        Route::get('/create', 'Admin\FaqKategoriController@create')->name('faq.create');
        Route::get('/update/{id}', 'Admin\FaqKategoriController@update')->name('faq.update');
        Route::post('/store', 'Admin\FaqKategoriController@store')->name('faq.store');
        Route::post('/edit/{id}', 'Admin\FaqKategoriController@edit')->name('faq.edit');
        Route::post('/destroy/{id}', 'Admin\FaqKategoriController@destroy')->name('faq.destroy');
    });

    Route::group(['prefix' => 'slide-show'], function () {
        Route::get('/show', 'Admin\SlideShowController@index')->name('slide.show');
        Route::get('/create', 'Admin\SlideShowController@create')->name('slide.show.create');
        Route::get('/detail-parent/{is_template}', 'Admin\SlideShowController@detailParent')->name('slide.show.detail-parent');
        Route::get('/update/{id}', 'Admin\SlideShowController@update')->name('slide.show.update');
        Route::post('/store', 'Admin\SlideShowController@store')->name('slide.show.store');
        Route::post('/edit/{id}', 'Admin\SlideShowController@edit')->name('slide.show.edit');
        Route::post('/destroy/{id}', 'Admin\SlideShowController@destroy')->name('slide.show.destroy');
    });

    Route::group(['prefix' => 'landing-page'], function () {
        Route::get('/show', 'Admin\LandingPageController@showList')->name('landing-page.show.list');
        Route::get('/create', 'Admin\LandingPageController@create')->name('landing-page.create');
        Route::post('/store', 'Admin\LandingPageController@store')->name('landing-page.store');
        Route::get('/update/{id}', 'Admin\LandingPageController@update')->name('landing-page.update');
        Route::post('/destroy/{id}', 'Admin\LandingPageController@destroy')->name('landing-page.destroy');
        // Route::get('/show', 'Admin\LandingPageController@index')->name('landing-page.show');
        Route::post('/page_1/store', 'Admin\LandingPage1Cotroller@store')->name('landing-page-1.store');
        Route::post('/page_1/update/{id}', 'Admin\LandingPage1Cotroller@update')->name('landing-page-1.update');
        Route::post('/page_1/delete-image/{id}', 'Admin\LandingPage1Cotroller@deleteAllImage')->name('landing-page-1.delete.image');
        Route::post('/page_2/store', 'Admin\LandingPage2Cotroller@store')->name('landing-page-2.store');
        Route::post('/page_2/update/{id}', 'Admin\LandingPage2Cotroller@update')->name('landing-page-2.update');
        Route::post('/page_2/delete-image/{id}', 'Admin\LandingPage2Cotroller@deleteAllImage')->name('landing-page-2.delete.image');
        Route::post('/page_3/store', 'Admin\LandingPage3Cotroller@store')->name('landing-page-3.store');
        Route::post('/page_3/update/{id}', 'Admin\LandingPage3Cotroller@update')->name('landing-page-3.update');
        Route::post('/page_3/delete-image/{id}', 'Admin\LandingPage3Cotroller@deleteAllImage')->name('landing-page-3.delete.image');
        Route::post('/page_4/store', 'Admin\LandingPage4Cotroller@store')->name('landing-page-4.store');
        Route::post('/page_4/update/{id}', 'Admin\LandingPage4Cotroller@update')->name('landing-page-4.update');
        Route::post('/page_4/delete-image/{id}', 'Admin\LandingPage4Cotroller@deleteAllImage')->name('landing-page-4.delete.image');
        Route::post('/page_5/store', 'Admin\LandingPage5Cotroller@store')->name('landing-page-5.store');
        Route::post('/page_5/update/{id}', 'Admin\LandingPage5Cotroller@update')->name('landing-page-5.update');
        Route::post('/page_5/delete-image/{id}', 'Admin\LandingPage5Cotroller@deleteAllImage')->name('landing-page-5.delete.image');
        Route::post('/page_6/store', 'Admin\LandingPage6Cotroller@store')->name('landing-page-6.store');
        Route::post('/page_6/update/{id}', 'Admin\LandingPage6Cotroller@update')->name('landing-page-6.update');
        Route::post('/page_6/delete-image/{id}', 'Admin\LandingPage6Cotroller@deleteAllImage')->name('landing-page-6.delete.image');
    });
});

