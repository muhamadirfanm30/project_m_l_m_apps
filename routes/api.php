<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Country
Route::group(['prefix' => 'rajaongkir'], function () {
    Route::get('provinsi', 'Api\Rajaongkir@provinsi');
    Route::get('city', 'Api\Rajaongkir@city');
    Route::get('subdistrict', 'Api\Rajaongkir@subdistrict');
    Route::get('courier', 'Api\Rajaongkir@courier');
    Route::post('cost', 'Api\Rajaongkir@cost');
});

// Country
Route::group(['prefix' => 'country'], function () {
    Route::get('select2', 'Api\AddresesController@selectCountry');
});

// Address Search
Route::group(['prefix' => 'address_search'], function () {
    Route::get('find_country/{country_id?}', 'Api\AddresesController@findCountry');
    Route::get('find_province/{province_id?}', 'Api\AddresesController@findProvince');
    Route::get('find_city/{city_id?}', 'Api\AddresesController@findCity');
    Route::get('find_district/{district_id?}', 'Api\AddresesController@findDistrict');
    Route::get('find_village/{village_id?}', 'Api\AddresesController@findVillage');
    Route::get('find_zipcode/{zipcode_id?}', 'Api\AddresesController@findZipcode');
});
