<?php

use Illuminate\Database\Seeder;
use App\User;
class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr= [
            'role'=>0,
            'first_name'=>'Adm',
            'last_name'=>'-',
            'no_ktp'=>'000',
            'gender'=>0,
            'phone'=>'087711214893',
            'provinsi'=>'Jawa Barat',
            'kota'=>'Kota Bogor',
            'kode_pos'=>'16146',
            'membership'=>null,
            'membership_id'=>null,
            'email'=>'m.fauzan441@gmail.com',
            'password'=>\Hash::make('qwerty123'),
            'myreferal'=>'RANSD12',
            'referal_code'=>null,
            'status'=>1,
        ];
        User::create($arr);
    }
}
