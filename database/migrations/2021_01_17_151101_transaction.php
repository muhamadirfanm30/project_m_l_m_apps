<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Transaction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('orderId');
            $table->string('nama_lengkap')->nullable();
            $table->string('nomor_ponsel')->nullable();
            $table->string('jenis_kelamin')->nullable();
            $table->string('nama_pengirim')->nullable();
            $table->string('alamat')->nullable();
            $table->string('kurir')->nullable();
            $table->string('totalHarga')->nullable();
            $table->string('totalOngkir')->nullable();
            $table->string('totalKeseluruhan')->nullable();
            $table->string('payment_method')->nullable();
            $table->bigInteger('cs_id');
            $table->string('va');
            $table->text('kategori');
            $table->tinyInteger('status')->default(0); 
            $table->string('photo')->nullable();
            $table->integer('own_transaction')->default(0);
            $table->integer('own_product')->default(0);
            $table->integer('user_id');
            $table->integer('type');
            $table->string('email');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction');
    }
}
