<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_produk');
            $table->double('harga');
            $table->integer('stok');
            $table->integer('is_promo');
            $table->double('harga_promo');
            $table->text('deskripsi_produk');
            $table->text('image_produk');
            $table->integer('berat');
            $table->integer('panjang');
            $table->integer('lebar');
            $table->integer('tinggi');
            $table->date('valid_at');
            $table->date('expired_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
