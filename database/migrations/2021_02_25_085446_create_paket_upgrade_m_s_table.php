<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaketUpgradeMSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paket_upgrade_m_s', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_paket');
            $table->double('harga');
            $table->integer('stok');
            $table->text('deskripsi_produk');
            $table->text('image_produk');
            $table->integer('berat');
            $table->integer('panjang');
            $table->integer('lebar');
            $table->integer('tinggi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paket_upgrade_m_s');
    }
}
