<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePositionUpdateModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('position_update_models', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_calom_ms');
            $table->string('posisi');
            $table->integer('user_sponsor_name');
            $table->integer('user_sponsor_status');
            $table->integer('user_sponsor_id');
            $table->integer('calon_ms_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('position_update_models');
    }
}
