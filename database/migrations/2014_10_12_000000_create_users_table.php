<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('roles');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('no_ktp');
            $table->string('gender')->default(0);
            $table->string('phone');
            $table->string('province_id');
            $table->string('kota_id');
            $table->string('kode_pos');
            $table->string('membersip_status')->nullable();;
            $table->string('membership_id')->nullable();;
            $table->string('myreferal')->nullable();;
            $table->string('referal_code')->nullable();
            $table->string('status');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('whatsapp_no');
            $table->integer('is_upgrade_ms');
            $table->text('address');
            $table->string('rekening_bank');
            $table->string('nama_pemilik_rekening');
            $table->string('nomor_rekening');
            $table->integer('is_tmp_user');
            $table->integer('is_approve_admin');
            $table->integer('is_admin_created');
            $table->integer('is_random_position');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
