<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKontenDashboardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('konten_dashboards', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('url_vidio_generasi_online');
            $table->text('url_vidio_tutorial_dashboard');
            $table->text('link_telegram');
            $table->text('link_group_content');
            $table->text('nama_instagram');
            $table->text('nama_facebook');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('konten_dashboards');
    }
}
