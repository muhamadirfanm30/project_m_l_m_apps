<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Konten extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('konten', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('free_text');
            $table->string('url_download');
            $table->string('foto');
            $table->bigInteger('kategori_id');
            $table->bigInteger('sub_kategori_id');
            $table->tinyInteger('type');//URL VIDEO, input file, accordion
            $table->string('judul');
            $table->string('source');
            $table->text('deskripsi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('konten');
    }
}
