@extends('home')
@section('content')
<div class="content-wrapper">
    <section class="content"><br>
        <div class="row">
            <div class="col-md-12">
                <div class="card card-outline">
                    <div class="card-header">
                        <h3 class="card-title">
                            <strong>
                                Konfirmasi Pembayaran
                            </strong>
                        </h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="card">
                            <div class="card-body">
                                <h4>
                                    <center><strong>Pesanan anda Berhasil Dibuat</strong></center>
                                </h4>
                                <h5>
                                    <center>
                                        Pesanan dengan <strong>Order ID 123,</strong> Akan segera diproses setelah menyelesaikan pembayaran
                                    </center>
                                </h5>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-3"></div>
                                            <div class="col-md-6">
                                                <div class="card-body border border-primary">
                                                    <div class="col-md-12">
                                                        <center><strong><h5>Jumlah yang Harus di bayarkan : <br><strong> Rp.200.123</strong></h5></strong></center>
                                                        <center><strong><h5>Batas Waktu Pembayaran : <br><strong> 1 Jam - 30 Menit- 40 Detik</strong></h5></strong></center>
                                                    </div>
                                                </div><br>
                                                <h6>
                                                    <center>Pastikan Transfer dengan nomor Unik 3 Digit Terahir, Untik mempercepat proses Transaksi</center>
                                                </h6><br>
                                                <h5><center><strong>Transfer Pembayaran ke Rekening Berikut :</strong></center></h5>
                                                <center><img src="{{asset('bca.jpg')}}" alt="asd" width="70px"></center><br>
                                                <center><h5><strong>987987868767</strong></h5></center>
                                                <center><h5  style="margin-bottom: 5px"><strong>KCP Daerah</strong></h5></center>
                                                <center><h5  style="margin-bottom: 20px"><strong>a/n Nama Pemilik Bank</strong></h5></center><hr>
                                                <center><h5><strong>Vidio Tutorial Bank Transfer</strong></h5></center>
                                                <center>
                                                    <div class="embed-responsive embed-responsive-16by9" style="width:auto">
                                                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/zpOULjyy-n8?rel=0" allowfullscreen></iframe>
                                                    </div><br>
                                                </center>
                                                <center><h5><strong>Lakukan Konfirmasi di Order Status Setelah Melakukan Transfer</strong></h5></center>
                                            </div>
                                            <div class="col-md-3"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <a href="http://" class="btn btn-primary btn-block">Cek Status Pesanan </a>
                    </div>
                </div>
            </div>
        </div><br>
    </section>
</div>
@endsection
