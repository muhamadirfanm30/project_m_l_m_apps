@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header d-flex p-0">
                            <ul class="nav nav-pills p-2">
                                <li class="nav-item"><a class="nav-link active" href="#tab_0" data-toggle="tab">Informasi Produk</a></li>
                                @foreach($kategori->sortBy('created_at') as $r)
                                    <li class="nav-item"><a class="nav-link" href="#tab_{{$r->id}}" data-toggle="tab">{{$r->nama}}</a></li>
                                @endforeach
                            </ul>
                        </div><!-- /.card-header -->
                        <div class="card-body">
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_0">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <img src="{{ url('storage/image-kategori/'.$detailData->foto) }}" alt="" style="width:100%; margin-bottom:10px">
                                        </div>
                                        <div class="col-md-9">
                                            <div class="card">
                                                <div class="card-body">
                                                    {!!  $detailData->deskripsi !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @foreach($kategori as $key => $value)
                                    <div class="tab-pane" id="tab_{{$value->id}}">
                                        <div class="row">
                                            @foreach($r->getKonten($value->id,$id) as $d)
                                                @if($d->type == 0)
                                                    <div class="col-md-3">
                                                        <div class="card">
                                                            <div class="card-body txt-center">
                                                                <iframe src="{{ $d->url_download }}" frameborder="0" style="width:100%"></iframe>
                                                                <h4>{!! strip_tags($d->free_text) !!}</h4>
                                                                <a href="{{ $d->deskripsi }}" target="_blank" class="btn btn-warning">Download</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @elseif($d->type == 1)
                                                    <div class="col-md-3">
                                                        <div class="card">
                                                            <div class="card-body txt-center">
                                                                <img src="{{ url('storage/konten/'.$d->foto) }}" alt="asd" style="width:100%; margin-bottom:10px">
                                                                {{-- <img src="{{ route('img-konten',['filename'=>$d->foto]) }}" alt="" style="width:100%; margin-bottom:10px"> --}}
                                                                <h3 class="card-title">{{ $r->judul }}</h3>

                                                                @if($d->free_text !="")
                                                                    <a href="#" class="btn btn-warning">{!! strip_tags($d->free_text) !!}</a>
                                                                @endif
                                                                <a href="{{ $d->url_download }}" target="_blank" class="btn btn-warning">Download</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @elseif($d->type == 2)
                                                    <div class="col-md-12">
                                                        <div class="card">
                                                            <div class="card-header">
                                                                <h4 class="card-title">
                                                                    <a data-toggle="collapse" href="#collapse-{{$d->id}}">
                                                                       {!! strip_tags($d->free_text) !!}
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse-{{$d->id}}" class="panel-collapse collapse in">
                                                                <div class="card-body">
                                                                    {!! $d->deskripsi !!}
                                                                    <input type="hidden" value="{{ $d->deskripsi }}" id="cp-text-{{$d->id}}"/>
                                                                    <br>
                                                                    <button id="btn_copy" data-cp="{{ strip_tags($d->deskripsi) }}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Copy To Clipboard" style="float:right"><i class="fa fa-copy"></i></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <input id="input_copy" type="text" name="id" style="width:1%; color:white; border:none;">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.60/inputmask/jquery.inputmask.js"></script>
    <script>
        $(document).on('click', '#btn_copy', function(){
            var values = $(this).attr('data-cp');
            console.log(values);
            $('#input_copy').val(values);
            var copyText = document.getElementById("input_copy");
            copyText.select();
            copyText.setSelectionRange(0, 99999)
            document.execCommand("copy");
            swal({
                title: "Sukses!",
                text: "Berhasil dicopy.",
                type: 'success'
            });
        });
    </script>
@endsection