@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                @foreach($data as $r)
                    <div class="col-md-3">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">{{ $r->judul }}</h3>
                            </div>
                            <div class="card-body txt-center">
                                <img src="{{ url('storage/image-kategori/'.$r->foto) }}" alt="" style="width:100%; margin-bottom:10px">
                                <a href="{{ route('konten.cs.detail',['id'=>$r->id]) }}" class="btn btn-warning">Lihat Konten</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </section>
    </div>
@endsection