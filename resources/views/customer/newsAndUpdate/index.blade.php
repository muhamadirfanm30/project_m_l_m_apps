@extends('home')
@section('content')
<div class="content-wrapper">
    <section class="content"><br>
        
        <div class="row">
            <div class="col-md-12">
                <div class="card card-outline">
                    <div class="card-header">
                        <h3 class="card-title">
                            <strong>
                                News & Updates
                            </strong>
                        </h3>
                    </div>
                    <!-- /.card-header -->
                    <br>
                        
                        @foreach ($getNews as $k => $news)
                            @if($news->publish_at <= date('Y-m-d') )
                                <div class="card-body border border-primary">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-4">
                                                {{-- <img src="storage/app/public/" alt=""> --}}
                                                <div class="embed-responsive embed-responsive-16by9" style="width:auto">
                                                    <iframe class="embed-responsive-item" src="{{$news->url_vidio}}" allowfullscreen></iframe>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-8">
                                                <strong><h4>{{$news->judul}}</h4></strong>
                                                    <small>
                                                        {{-- {!! i !!}  --}}
                                                        {!! Str::limit($news->deskripsi, $limit = 400, $end = '...') !!}
                                                    </small><br><br>
                                                <div class="row">
                                                    <div class="col-md-9">
                                                        <center>
                                                            <a href="{{ url('customer/news-update/detail/'.$news->id) }}" class="btn btn-warning" style="color: white">Lihat Selengkapnya</a>
                                                        </center>
                                                    </div>
                                                    {{-- <div class="col-md-4">
                                                        Tanggal Terbit  {{ date('d M Y', strtotime($news->publish_at)) }}
                                                    </div> --}}
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div><br>
                            @endif
                        @endforeach
                        {!! $getNews->render() !!}
                    
                    <div class="card-footer">
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
