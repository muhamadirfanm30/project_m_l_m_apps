@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('success') }}</strong>
                        </div>
                    @endif
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3 class="card-title">
                                        Syarat & Ketentuan
                                    </h3>
                                </div>
                                <div class="col-md-2">
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                                
                            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                                    <span class="navbar-toggler-icon"></span>
                                </button>
                                <div class="collapse navbar-collapse" id="navbarNav">
                                    <ul class="navbar-nav">
                                        @foreach ($getMenuFaq->sortBy('created_at') as $item)
                                            @php
                                                $style = '';
                                                if(Request::segment(4) == $item->id){
                                                    $style = 'active';
                                                }else{
                                                    $style = '';
                                                }
                                            @endphp
                                            <li class="nav-item {{ $style }}">
                                                <a class="nav-link" href="{{ url('/customer/syarat-ketentuan/detail/'.$item->id) }}">{{$item->faqKategori->name}}</a>
                                            </li>
                                        @endforeach
                                        
                                    </ul>
                                </div>
                            </nav>
                                
                            <div class="jumbotron" style="background-color: white">
                                <div class="col-md-12">
                                <div class="form-group">
                                    <h5>{{ !empty($detailFaq->judul_utama) ? $detailFaq->judul_utama : '' }}</h5> <hr>
                                    <small>{!! !empty($detailFaq->deskripsi_utama) ? $detailFaq->deskripsi_utama : '' !!}</small>
                                </div>
                            </div><hr>
                            <div class="col-md-12">
                                


                                <style>
                                    table {
                                        font-size: 1em;
                                    }
        
                                    .ui-draggable, .ui-droppable {
                                        background-position: top;
                                    }
                                </style>
        
                                <div id="accordion">
                                    @if (!empty($detailFaq))
                                        @foreach($detailFaq->termDetail as $index => $val)
                                            <h6> <strong>{{ $val->judul_accordion }}</strong></h6>
                                            <div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h6 style="white-space: pre-wrap;">{!! $val->desc_accordion !!}</h6>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                            </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <link rel="stylesheet" href="{{ asset('styles.css') }}">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $( function() {
            $( "#accordion" ).accordion({
                collapsible: true
            });
        } );
    </script>
    {{-- <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script> --}}
    <script>
        $(document).ready(function() {
            $('#userManagement').DataTable();
        } );
    </script>
@endsection