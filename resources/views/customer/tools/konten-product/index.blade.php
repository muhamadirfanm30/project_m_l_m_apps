@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-outline card-info">

                        <div class="card-header">
                            <h3 class="card-title">
                            Daftar Paket Upgrade Membership Mobile Stokiest
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="col">
                                <div class="row">
                                    @foreach($data as $r)
                                    <div class="col-12 col-md-6 col-lg-3">
                                        <div class="card">
                                            <img class="card-img-top" src="{{Storage::url('kategori/'.$r->foto)}}" alt="Card image cap" style="max-height:200px">
                                            <div class="card-body">
                                                <h4 class="card-title"><a href="product.html" title="View Product">{{ $r->judul }}</a></h4>
                                                <p class="card-text">{!! substr(strip_tags($r->deskripsi),0,150) !!}...</p>
                                                <div class="row">
                                                    <div class="col">
                                                        <a href="{{ route('cs-detail-tkp',['id'=>$r->id]) }}" class="btn btn-success btn-block">Lihat Konten</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    {{-- modal --}}

    @include('customer.memberships._modalUpgradeMs')
@endsection
