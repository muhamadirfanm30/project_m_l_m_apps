@extends('home')
@section('content')
@include('customer.order-product._styleDetailProduct')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('success') }}</strong>
                        </div>
                    @endif
                    <div class="card card-outline card-info">

                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <h3 class="card-title">
                                                list Produk
                                            </h3>
                                        </div>
                                        <div class="col-md-4">
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                @foreach($showPaket as $k => $paketMS)
                                    @php
                                        $id = 0;
                                        $id = $paketMS->id;
                                    @endphp
                                    <div class="col-md-3" style="max-width:280px">
                                        <div class="card">
                                            <div class="card-header">
                                                <a href="{{url('customer/paket-ms-upgrade/detail/'.$paketMS->id)}}" class="card-title">{{$paketMS->nama_paket}}</a>
                                            </div>
                                            <div class="card-body txt-center">
                                                <a href="{{url('customer/paket-ms-upgrade/detail/'.$paketMS->id)}}">
                                                    <img src="{{url('storage/package-upgrade-ms/'.$paketMS->image_produk)}}" class="img-thumbnail" alt="">
                                                </a>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-2"></div>
                                                        <div class="col-md-8">
                                                        </div>
                                                        <div class="col-md-2"></div>
                                                    </div>
                                                </div><br>
                                                <a href="{{ url('customer/paket-ms-upgrade/detail/'.$paketMS->id) }}" class="btn btn-warning btn-sm"><i class="fa fa-eye" style="color:white"> Detail Paket</i></a>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div><br>
                            {!! $showPaket->render() !!}
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @section('javascript')
    @parent
        <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>

    @endsection
@endsection
