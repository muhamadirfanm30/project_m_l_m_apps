@extends('home')
@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-outline card-warning">
                        <div class="card-header">
                            <h3 class="card-title">
                                Cart
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="col-md-12">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="2">Product</th>
                                            <th>Quantity</th>
                                            <th class="text-center">Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $total = 0;
                                            $berat = 0;
                                        @endphp
                                        @foreach ($data as $k => $paket)
                                            @php
                                                $total += $paket->harga * 1;
                                                $price = $paket->harga * 1;
                                                $berat += $paket->berat * 1;
                                                $nama_produk = $paket->nama_paket;
                                                $harga = $paket->harga;
                                                $id = $paket->id;
                                            @endphp
                                            <tr>
                                                <td>
                                                    <a class="thumbnail pull-left" href="#">
                                                        <img width="100" height="100" class="img-responsive" src="{{ url('storage/package-upgrade-ms/'.$paket->image_produk) }}">
                                                    </a>
                                                </td>
                                                <td>
                                                    <h4 class="media-heading"><a href="#">{{$paket->nama_paket}}</a></h4>
                                                </td>
                                                <td>1</td>
                                                <td>Rp. {{number_format($paket->harga)}}</td>
                                            </tr>

                                            
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="3" class="text-right"><strong>Berat</strong></td>
                                            <td class="text-right"><strong>{{ number_format($berat) }} Gram</strong></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" class="text-right"><strong>Total</strong></td>
                                            <td class="text-right"><strong>Rp. {{ number_format($total) }}</strong></td>
                                        </tr>
                                        <tr>
                                            {{-- <td colspan="4">
                                                <a href="{{url('customer/order-produk/checkout')}}" class="btn btn-success btn-sm">
                                                    <span class="fa fa-play"></span> &nbsp; Checkout 
                                                </a>
                                            </td> --}}
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-outline card-warning">
                        <div class="card-header">
                            <h3 class="card-title">
                                Alamat Pengiriman
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Nama Lengkap</label>
                                            <input type="text" id="nama_lengkap" class="form-control" placeholder="Nama Lengkap ...">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Nomor Ponsel Penerima</label>
                                            <input type="text" id="nomor_ponsel" class="form-control" placeholder="Nomor Ponsel ...">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input type="email" id="email" name="email" class="form-control" placeholder="Email ..." readonly value="{{ auth()->user()->email }}">
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="row">
                                    {{-- <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Jenis Kelamin</label>
                                            <input type="text" id="jenis_kelamin" class="form-control" placeholder="Enter ...">
                                        </div>
                                    </div> --}}
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>Nama Pengirim</label>
                                            <input type="text" id="nama_pengirim" class="form-control" placeholder="Nama Pengirim ...">
                                        </div>
                                    </div>
                                </div> -->
                                <div class="row">
                                    <div class="col-sm-6" id="provHide">
                                        <div class="form-group">
                                            <label>Provinsi</label>
                                            <select class="form-control" name="province_id" id="province_id" style="width:100%">
                                                <option>Pilih Provinsi</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6" id="cityHide">
                                        <div class="form-group">
                                            <label>Kota / Kabupaten</label>
                                            <select class="form-control" name="kota_id" id="kota_id" style="width:100%">
                                                <option>Pilih Provinsi Terlebih dahulu</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6" id="cityHide">
                                        <div class="form-group">
                                            <label>Kecamatan</label>
                                            <select class="form-control" name="distrik_id" id="distrik_id" style="width:100%">
                                                <option>Pilih Kota / Kabupaten Terlebih Dahulu</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Kode Pos</label>
                                            <input type="text" name="kode_pos" class="form-control" id="kodepos">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Kurir</label>
                                            <select class="form-control" name="kurir" id="kurir" style="width:100%">
                                                <option>Pilih Kecamatan Terlebih Dahulu</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Pilih Layanan</label>
                                            <select name="layanan" id="layanan" class="form-control">
                                            <option>Pilih Kurir Terlebih dahulu</option>
                                            </select>
                                        </div>
                                    </div>
                                    <input type="hidden" value="40" class="form-control" id="city_origin" name="city_origin">
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Alamat Lengkap</label>
                                            <textarea name="alamat" class="form-control" id="alamat" cols="30" rows="10"></textarea>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-outline card-warning">
                        <div class="card-header">
                            <h3 class="card-title">
                                Metode Pembayaran
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="accordion" id="accordionExample">
                                @if (!empty($bank->payment_name))
                                    <div class="card">
                                        <div class="card-header" id="headingOne">
                                            <h2 class="mb-0">
                                                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                    {{$bank->payment_name}}
                                                </button>
                                            </h2>
                                        </div>
                                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                            <div class="card-body">
                                                <div class="row">
                                                    @foreach($listPayment as $key => $bank)
                                                        <div class="col-md-4">
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="radio" name="payment_method" value="{{$bank->nama_bank}}">
                                                                <label class="form-check-label">
                                                                    <img src="{{url('storage/bank-icon/'.$bank->image)}}" alt="{{$bank->nama_bank}}" width="50px">
                                                                </label>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                
                                @if (!empty($midtrans->payment_name))
                                    <div class="card">
                                        <div class="card-header" id="headingTwo">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                {{ $midtrans->payment_name }}
                                            </button>
                                        </h2>
                                        </div>
                                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="col-md-4">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="payment_method" value="0">
                                                    <label class="form-check-label">
                                                        <img src="https://docs.midtrans.com/asset/image/main/midtrans-logo.png" alt="Midtrans" width="90px">
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <hr>
                            <div class="col-md-12">
                                <div class="card card-outline card-warning">
                                    <div class="card-header">
                                        <h3 class="card-title">
                                            Ringkasan Pesanan
                                        </h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <table id="list-card-product" class="display" style="width:100%">
                                            <tbody>
                                                <tr>
                                                    <td colspan="2">Subtotal Produk</td>
                                                    <td id="lbltotalHargaItem">Rp. {{ number_format($total) }}</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">Subtotal Pengiriman</td>
                                                    <td><span id="lbltotalOngkir"></span></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">Subtotal Pesanan</td>
                                                    <td id="lbltotalKeseluruhan"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <input type="hidden" name="totalHargaItem" id="totalHargaItem" value="{{$total}}">
                                    <input type="hidden" name="totalOngkir" id="totalOngkir" >
                                    <input type="hidden" name="weight" id="weight" value="{{$berat}}" >
                                    <input type="hidden" name="totalKeseluruhan" id="totalKeseluruhan" value="{{$total}}">
                                    <input type="hidden" name="product_name" id="product_name" value="{{$nama_produk}}">
                                    <input type="hidden" name="harga" id="harga" value="{{$harga}}">
                                    <input type="hidden" name="qty" id="qty" value="1">
                                    <input type="hidden" name="id" id="id" value="{{$id}}">
                                    <div class="card-footer">
                                        <button class="btn btn-primary btn-block" id="checkout">Checkout</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer"></div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @section('javascript')
        @parent
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
        <script type="text/javascript"
        src="https://app.sandbox.midtrans.com/snap/snap.js"
        data-client-key="{{ env('MD_CLIENT_KEY') }}"></script>
        <script>
            function ajaxRequest(type, url, callbackSuccess = null) {
                $.ajax({
                    url: url,
                    type: type,
                    success: function(response) {
                        if (callbackSuccess != null) {
                            callbackSuccess(response);
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus, errorThrown);
                    }
                });
            }
            
         
            
            function ajaxGetCostRajaOngkir(callbackSuccess = null) {
                
                var wg = $("#weight").val();

                costParam = {
                    origin: $('#distrik_id').val(), // ID kota/kabupaten atau kecamatan asal
                    originType: 'subdistrict', // Tipe origin: 'city' atau 'subdistrict'.
                    weight: wg, // Berat kiriman dalam gram
                    courier: $('#kurir').val(),
                };

                $.ajax({
                    url: '/api/rajaongkir/cost',
                    type: 'POST',
                    data: costParam,
                    success: function(response) {
                        callbackSuccess(response);
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus, errorThrown);
                    }
                });
            }

            function loopingLayananRajaOngkir(rajaongkir) {
                costs = rajaongkir.results[0].costs; // ambil data pertama
                if (costs.length == 0) {
                    alert('data tidak ada');
                    return [];
                }

                return $.map(costs, function(item) {
                    return $.map(item.cost, function(c) {
                        text = item.service + ' - (' + formatRupiah(c.value) + ') - ' + c.etd;
                        return {
                            text: text,
                            id: c.value
                        }
                    })
                });

            }

            function createSelect2(selector, data) {
                console.log('createSelect2', data)
                $(selector).select2({
                    data: data
                });
            }

            $(document).ready(function(){
                onlyNumber('#nomor_ponsel');
                onlyNumber('#kodepos');
                Helper.loadingStart()
                ajaxRequest('GET', '/api/rajaongkir/provinsi', function(response) {
                    var select = '<option value="">Pilih Provinsi</option>';
                    $.each(response.data.rajaongkir.results, function(item,item2) {
                        // console.log(item2)
                        select += `<option value="${item2.province_id}">${item2.province}</option>` ;
                    });
                    Helper.loadingStop()
                    $('#province_id').html(select)
                })
                

                // on chnege provinsi
                $('#province_id').change(function() {
                    province_id = $(this).val();
                    if(province_id == ""){
                        swal({
                            title: "Ups!",
                            text: "Silahkan Pilih Provinsi Yang Tersedia! ",
                            type: 'error'
                        });
                    }else{
                        Helper.loadingStart()
                        ajaxRequest('GET', '/api/rajaongkir/city?province=' + province_id, function(response) {
                            console.log(response)
                            var city = '<option value="">Pilih Kota / Kabupaten</option>';
                            $.each(response.data.rajaongkir.results, function(item,item2) {
                                city += `<option value="${item2.city_id}">${item2.type + ' ' + item2.city_name}</option>`;
                            });
                            Helper.loadingStop()
                            $('#kota_id').html(city)
                        })
                        // empty select
                        $('#kota_id').html('').append('<option value="">Pilih Kota / Kabupaten</option>');
                        $('#distrik_id').html('').append('<option value="">Pilih Kota / Kabupaten Terlebih Dahulu</option>');
                        $('#kurir').html('').append('<option value="">Pilih Kecamatan Terlebih Dahulu</option>');
                        $('#layanan').html('').append('<option value="">Pilih Kurir Terlebih Dahulu</option>');
                    }
                })

                // on chnege city
                $('#kota_id').change(function() {
                    city_id = $(this).val();
                    if(city_id == ""){
                        swal({
                            title: "Ups!",
                            text: "Silahkan Pilih Kota / Kabupaten Yang Tersedia! ",
                            type: 'error'
                        });
                    }else{
                        Helper.loadingStart()
                        // load subdistrict
                        ajaxRequest('GET', '/api/rajaongkir/subdistrict?city=' + city_id, function(response) {
                            var distrik = '<option value="">Pilih Kecamatan</option>';
                            $.each(response.data.rajaongkir.results, function(item,item2) {
                                distrik += `<option value="${item2.subdistrict_id}">${item2.subdistrict_name}</option>`;
                            })
                            Helper.loadingStop()
                            $('#distrik_id').html(distrik)
                        })

                        // empty select
                        $('#kodepos').val("");
                        $('#distrik_id').html('').append('<option value="">Pilih Kecamatan</option>');
                        $('#kurir').html('').append('<option value="">Pilih Kecamatan Terlebih Dahulu</option>');
                        $('#layanan').html('').append('<option value="">Pilih layanan terlebih dahulu</option>');
                    }
                })

                // on chnege distrik
                $('#distrik_id').change(function() {
                    // load subdistrict
                    if($(this).val() == ""){
                        swal({
                            title: "Ups!",
                            text: "Silahkan Pilih Kecamatan Yang Tersedia! ",
                            type: 'error'
                        });
                    }else{
                        Helper.loadingStart()
                        ajaxRequest('GET', '/api/rajaongkir/courier', function(response) {
                            var kurir = '<option value="">Pilih Kurir</option>';
                            $.each(response.data, function(item,item2) {
                                console.log(item2)
                                kurir += `<option value="${item2.code}">${item2.text}</option>`;
                            })
                            Helper.loadingStop()
                            $('#kurir').html(kurir)

                        })
                        // empty select
                        $('#kurir').html('').append('<option value="">Pilih kurir</option>');
                        $('#layanan').html('').append('<option value="">Pilih layanan</option>');
                    }
                })

                // on chnege distrik
                $('#kurir').change(function() {
                    if($(this).val() == ""){
                        swal({
                            title: "Ups!",
                            text: "Silahkan Pilih Kecamatan Yang Tersedia! ",
                            type: 'error'
                        });
                    }else{
                        Helper.loadingStart()
                        ajaxGetCostRajaOngkir(function(response) {
                            data = loopingLayananRajaOngkir(response.data.rajaongkir);
                            console.log(data)
                            var cost = '<option value="">Pilih Layanan Jasa</option>';
                            $.each(data, function(item,item2) {
                                console.log(item2)
                                cost += `<<option value="${item2.id}">${item2.text} Hari</option>`;
                            })
                            // createSelect2('#layanan', data);
                            // console.log(data)
                            Helper.loadingStop()
                            $('#layanan').html(cost)
                        })
                    }
                })

                $('select[name="layanan"]').on('change', function() {
                    let price = $('#layanan').val();
                    $('#lbltotalOngkir').val(price);
                    $('#totalOngkir').val(price);
                    var hrgItem = $('#totalHargaItem').val();
                    let total = parseInt(hrgItem) + parseInt(price);
                    $('#lbltotalOngkir').text(formatRupiah(price));
                    $('#lbltotalKeseluruhan').text(formatRupiah(total));
                    $('#totalKeseluruhan').val(total);
                });

                $('#checkout').on('click', function(){
                    let kota_id = $('select[name="kota_id"] option:selected').text();
                    let province_id = $('select[name="province_id"] option:selected').text();
                    let district_id = $('select[name="distrik_id"] option:selected').text();
                    let kode_pos = $('#kodepos').val();
                    let total_berat = $('input[type="hidden"][name="weight"]').val();

                    let product_name = $('#product_name').val();
                    let harga = $('#harga').val();
                    let qty = $('#qty').val();
                    let id = $('#id').val();
                    let nama_lengkap = $('#nama_lengkap').val();
                    let email = $('#email').val();
                    let nomor_ponsel = $('#nomor_ponsel').val();
                    let jenis_kelamin = $('#jenis_kelamin').val();
                    // let nama_pengirim = $('#nama_pengirim').val();
                    let alamat = $('#alamat').val();

                    let totalHarga = $('#totalHargaItem').val();
                    let totalOngkir = $('#totalOngkir').val();
                    let totalKeseluruhan = $('#totalKeseluruhan').val();
                    let payment_method = $('input[type="radio"][name="payment_method"]:checked').val();

                    let namaPaketOngkir = "";
                    if($('#layanan').val() !=""){
                        namaPaketOngkir = $('#kurir').val() + ' - ' + $('#layanan option:selected').text().split("-")[0];
                    }
                    if(nama_lengkap == "" || email == "" || nomor_ponsel == "" || jenis_kelamin == "" || province_id == "" || kota_id == "" || product_name =="" || namaPaketOngkir ==""){
                        swal({
                        title: "Ups!",
                            text: "Mohon lengkapi form pengiriman",
                            type: 'error'
                        });
                    }else if(typeof payment_method == 'undefined' || payment_method == ''){
                        swal({
                            title: "Ups!",
                            text: " Silahkan Pilih Metode Pembayaran",
                            type: 'error'
                        });
                    }else{
                        Helper.loadingStart()
                        jQuery.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url:"/customer/paket-ms-upgrade/checkout",
                            type:'POST',
                            dataType:'json',
                            data:{
                                product_name,
                                kota_id,
                                province_id,
                                district_id,
                                kode_pos,
                                total_berat,
                                harga,
                                qty,
                                id,
                                nama_lengkap,
                                email,
                                nomor_ponsel,
                                jenis_kelamin,
                                // nama_pengirim,
                                alamat,
                                totalHarga,
                                totalOngkir,
                                totalKeseluruhan,
                                namaPaketOngkir,
                                payment_method:payment_method
                            },
                            success:function(data){
                                Helper.loadingStop()
                                if(payment_method == 0){
                                    snap.pay(data.token,{
                                        onSuccess: function(result){
                                            swal({
                                                title: "Sukses!",
                                                text: "Transaksi anda berhasil, silahkan segera lakukan pembayaran!",
                                                type: 'success'
                                            }, function() {
                                                window.location.href = '/customer/status-order/pembayaran/' + data.orderId;
                                            });
                                        },
                                        onPending: function(result){
                                            swal({
                                                title: "Sukses!",
                                                text: "Transaksi anda berhasil, silahkan segera lakukan pembayaran!",
                                                type: 'success'
                                            }, function() {
                                                window.location.href = '/customer/status-order/pembayaran/' + data.orderId;
                                            });
                                        },
                                        onError: function(result){
                                            swal({
                                                title: "Ups!",
                                                text: result.status_message,
                                                type: 'error'
                                            });
                                        },
                                        onClose: function(){
                                            console.log('customer closed the popup without finishing the payment');
                                        }
                                    });
                                }else{
                                    swal({
                                        title: "Sukses!",
                                        text: "Transaksi anda berhasil, silahkan segera lakukan pembayaran!",
                                        type: 'success'
                                    }, function() {
                                        window.location.href = '/customer/status-order/pembayaran/' + data.orderId;
                                    });
                                }
                            },
                            error:function(a,v,c){
                                Helper.loadingStop()
                                var msg = JSON.parse(a.responseText);
                                console.log(msg);
                                swal({
                                    title: "Ups!",
                                        text: msg.message,
                                        type: 'error'
                                    });
                            }
                        });
                        
                    }
                    
                });
            });

            function formatRupiah(bilangan) {
                var number_string = bilangan.toString(),
                    sisa = number_string.length % 3,
                    rupiah = number_string.substr(0, sisa),
                    ribuan = number_string.substr(sisa).match(/\d{3}/g);

                if (ribuan) {
                    separator = sisa ? ',' : '';
                    rupiah += separator + ribuan.join(',');
                }

                return 'Rp. ' + rupiah;
            }

            function convertInteger(id) {
                let total = document.getElementById(id).textContent;
                total = total.replace(/ |Rp |\- |\./gi, "")
                total = parseInt(total)
                return total;
            }
        </script>
    @endsection
@endsection