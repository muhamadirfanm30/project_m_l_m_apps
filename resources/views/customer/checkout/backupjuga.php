



@extends('home')
@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-outline card-warning">
                        <div class="card-header">
                            <h3 class="card-title">
                                Cart
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="col-md-12">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="2">Product</th>
                                            <th>Quantity</th>
                                            <th class="text-center">Price</th>
                                            {{-- <th colspan="2">Action</th> --}}
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $total = 0;
                                            $berat = 0;
                                        @endphp

                                        @if (session('cart'))
                                            @foreach (session('cart') as $id => $details)
                                                @php
                                                    $total += $details['harga'] * $details['qty'];
                                                    $price = $details['harga'] * $details['qty'];
                                                    $berat += $details['berat'] * $details['qty'];
                                                @endphp
                                                <tr>
                                                    <td>
                                                        <a class="thumbnail pull-left" href="#">
                                                            <img width="100" height="100" class="img-responsive" src="{{ url('storage/product-image/'.$details['image_produk']) }}">
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <h4 class="media-heading"><a href="#">{{$details['nama_produk']}}</a></h4>
                                                    </td>
                                                    <td>
                                                        {{ $details['qty'] }}
                                                    </td>
                                                    <td class="text-center" data-th="price"><strong>Rp. {{ number_format($price) }}</strong></td>
                                                    {{-- <td>
                                                        <button class="btn btn-info btn-sm update-cart" data-id="{{ $id }}"><i class="fa fa-check"></i></button>
                                                        <button class="btn btn-danger btn-sm remove-from-cart" data-id="{{ $id }}"><i class="fa fa-trash"></i></button>
                                                    </td> --}}
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="3" class="text-right"><strong>Berat</strong></td>
                                            <td class="text-right"><strong>{{ number_format($berat) }} Gram</strong></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" class="text-right"><strong>Total</strong></td>
                                            <td class="text-right"><strong>Rp. {{ number_format($total) }}</strong></td>
                                        </tr>
                                        <tr>
                                            {{-- <td colspan="4">
                                                <a href="{{url('customer/order-produk/checkout')}}" class="btn btn-success btn-sm">
                                                    <span class="fa fa-play"></span> &nbsp; Checkout 
                                                </a>
                                            </td> --}}
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-outline card-warning">
                        <div class="card-header">
                            <h3 class="card-title">
                                Alamat Pengiriman
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>Kirim Ke</label>
                                            <select name="kirim_ke" id="kirim_ke" class="form-control">
                                                <option value="0">Konsumen</option>
                                                <option value="1">Jadikan Stok Produk Online</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Nama Lengkap</label>
                                            <input type="text" id="nama_lengkap" class="form-control" placeholder="Enter ...">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Nomor Ponsel</label>
                                            <input type="text" id="nomor_ponsel" class="form-control" placeholder="Enter ...">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Jenis Kelamin</label>
                                            <input type="text" id="jenis_kelamin" class="form-control" placeholder="Enter ...">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Nama Pengirim</label>
                                            <input type="text" id="nama_pengirim" class="form-control" placeholder="Enter ...">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6" id="provHide">
                                        <div class="form-group">
                                            <label>Provinsi</label>
                                            <select class="form-control" name="province_id" id="province_id" style="width:100%">
                                                <option >Pilih Provinsi</option>
                                                @foreach ($provinsi  as $row)
                                                    <option data-price="25000" value="{{$row['province_id']}}" namaprovinsi="{{$row['province']}}">{{$row['province']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6" id="cityHide">
                                        <div class="form-group">
                                            <label>Kota / Kabupaten</label>
                                            <select class="form-control" name="kota_id" id="kota_id" style="width:100%">
                                                <option >Pilih Kota</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12" id="cityHide">
                                        <div class="form-group">
                                            <label>distrik</label>
                                            <select class="form-control" name="district_id" id="district_id" style="width:100%">
                                                <option >Pilih Kota</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Kurir</label>
                                            <select class="form-control" name="kurir" id="kurir" style="width:100%">
                                                <option value="jne">JNE</option>
                                                <option value="tiki">TIKI</option>
                                                <option value="pos">POS INDONESIA</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Pilih Layanan</label>
                                            <select name="layanan" id="layanan" class="form-control">

                                            </select>
                                        </div>
                                    </div>
                                    <input type="hidden" value="79" class="form-control" id="city_origin" name="city_origin">
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Alamat Lengkap</label>
                                            <textarea name="alamat" class="form-control" id="alamat" cols="30" rows="10"></textarea>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-outline card-warning">
                        <div class="card-header">
                            <h3 class="card-title">
                                Ringkasan Pesanan
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table  id="list-card-product" class="display" style="width:100%">
                                <tbody>
                                    <tr>
                                        <td colspan="2">Subtotal Produk</td>
                                        <td id="lbltotalHargaItem">Rp. {{ number_format($total) }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">Subtotal Pengiriman</td>
                                        <td><span id="lbltotalOngkir"></span></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">Subtotal Pesanan</td>
                                        <td id="lbltotalKeseluruhan"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">Metode Pembayaran</td>
                                        <td>
                                            <select id="payment_method" class="form-control">
                                                <option value="">Pilih Metode Pembayaran</option>
                                                <optgroup label="Transfer Bank">
                                                    <option value="bca">BCA</option>
                                                </optgroup>
                                            </select>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <input type="hidden" name="totalHargaItem" id="totalHargaItem" value="{{$total}}">
                        <input type="hidden" name="totalOngkir" id="totalOngkir" >
                        <input type="hidden" name="weight" value="{{$berat}}" >
                        <input type="hidden" name="totalKeseluruhan" id="totalKeseluruhan" value="{{$total}}">
                        <div class="card-footer">
                            <button class="btn btn-primary btn-block" id="checkout">Checkout</button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @section('javascript')
        @parent
        <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
        <script>
            $(document).ready(function(){
                $('select[name="province_id"]').on('change', function(){
                    let provinceid = $(this).val();
                    if(provinceid){
                        jQuery.ajax({
                            url:"kota/"+provinceid,
                            type:'GET',
                            dataType:'json',
                            success:function(data){
                                $('select[name="kota_id"]').empty();
                                $('select[name="kota_id"]').append('<option value="" >Pilih Kota</option>');
                                if(data.length > 0){
                                    $.each(data, function(key, value){
                                        $('select[name="kota_id"]').append('<option value="'+ value.city_id +'" namakota="'+ value.type +' ' +value.city_name+ '">' + value.type + ' ' + value.city_name + '</option>');
                                    });
                                }
                            }
                        });
                    }else {
                        $('select[name="kota_id"]').empty();
                    }
                });

                $('select[name="kota_id"]').on('change', function(){
                    let kota_id = $(this).val();
                    if(kota_id){
                        jQuery.ajax({
                            url:"distrik/"+kota_id,
                            type:'GET',
                            dataType:'json',
                            success:function(data){
                                $('select[name="district_id"]').empty();
                                $('select[name="district_id"]').append('<option value="" >Pilih Kota</option>');
                                if(data.length > 0){
                                    $.each(data, function(key, value){
                                        $('select[name="district_id"]').append('<option value="'+ value.subdistrict_id +'" namakota="' +value.subdistrict_name +'">'+ value.subdistrict_name +'</option>');
                                    });
                                }
                            }
                        });
                    }else {
                        $('select[name="district_id"]').empty();
                    }
                });

                $('select[name="kurir"]').on('change', function(){
                    let origin = $("input[name=city_origin]").val();
                    let destination = $("select[name=kota_id]").val();
                    let destinationType = $("select[name=district_id]").val();
                    let courier = $("select[name=kurir]").val();
                    let weight = $("input[name=weight]").val();
                    if(courier){
                        jQuery.ajax({
                            url:"origin=40&originType=city&destination="+destination+"&destinationType="+destinationType+"&weight=1&courier="+courier,
                            type:'GET',
                            dataType:'json',
                            success:function(data){
                                $('select[name="layanan"]').empty();
                                $('select[name="layanan"]').append('<option value="" >Pilih Layanan</option>');
                                if(data.length > 0){
                                    $.each(data, function(key, value){
                                        $.each(value.costs, function(key1, value1){
                                            $.each(value1.cost, function(key2, value2){
                                                $('select[name="layanan"]').append('<option value="'+ value1.service  +'" data-price="'+value2.value+'" >' + value1.service  + ' ( ' +value2.etd + 'Hari ) - ' + formatRupiah(value2.value) +'</option>');
                                            });
                                        });
                                    });
                                }
                            }
                        });
                    } else {
                        $('select[name="layanan"]').empty();
                    }
                });

                $('select[name="district_id"]').on('change', function(){
                    let origin = $("input[name=city_origin]").val();
                    let destination = $("select[name=kota_id]").val();
                    let destinationType = $("select[name=district_id]").val();
                    let courier = $("select[name=kurir]").val();
                    let weight = $("input[name=weight]").val();
                    console.log(courier)
                    if(destinationType){
                        jQuery.ajax({
                            url:"origin=501&originType=city&destination=574&destinationType=subdistrict&weight=1700&courier=jne",
                            type:'GET',
                            dataType:'json',
                            success:function(data){
                                $('select[name="layanan"]').empty();
                                $('select[name="layanan"]').append('<option value="">Pilih Layanan</option>');
                                if(data.length > 0){
                                    $.each(data, function(key, value){
                                        $.each(value.costs, function(key1, value1){
                                            $.each(value1.cost, function(key2, value2){
                                                $('select[name="layanan"]').append('<option value="'+ value1.service  +'" data-price="'+value2.value+'" >' + value1.service  + ' ( ' +value2.etd + 'Hari ) - ' + formatRupiah(value2.value) +'</option>');
                                            });
                                        });
                                    });
                                }
                            }
                        });
                    } else {
                        $('select[name="layanan"]').empty();
                    }
                });

                $('select[name="layanan"]').on('change', function(){
                    let kurir = $(this).val();
                    let price = $(this).find(':selected').attr('data-price');
                    $('#lbltotalOngkir').val(price);
                    $('#totalOngkir').val(price);
                    var hrgItem = $('#totalHargaItem').val();
                    let total = parseInt(hrgItem) + parseInt(price);
                    $('#lbltotalOngkir').text(formatRupiah(price));
                    $('#lbltotalKeseluruhan').text(formatRupiah(total));
                    $('#totalKeseluruhan').val(total);
                });

                $('select[name="kirim_ke"]').on('change', function(){
                    let val = $(this).val();
                    if(val == 1){
                        $('#nama_lengkap').attr("readonly","true");
                        $('#nomor_ponsel').attr("readonly","true");
                        $('#jenis_kelamin').attr("readonly","true");
                        $('#nama_pengirim').attr("readonly","true");
                        $('#alamat').attr("readonly","true");

                        $('select[name="province_id"]').attr("readonly","true");
                        $('select[name="kurir"]').attr("readonly","true");
                        $('select[name="kota_id"]').attr("readonly","true");
                        $('select[name="layanan"]').attr("readonly","true");
                    }else{
                        $('#nama_lengkap').removeAttr("readonly");
                        $('#nomor_ponsel').removeAttr("readonly");
                        $('#jenis_kelamin').removeAttr("readonly");
                        $('#nama_pengirim').removeAttr("readonly");
                        $('#alamat').removeAttr("readonly");

                        $('select[name="province_id"]').removeAttr("readonly");
                        $('select[name="kurir"]').removeAttr("readonly");
                        $('select[name="kota_id"]').removeAttr("readonly");
                        $('select[name="layanan"]').removeAttr("readonly");
                    }
                });

                $('#checkout').on('click', function(){
                    let kirim_ke = $('#kirim_ke').val();
                    let nama_lengkap = $('#nama_lengkap').val();
                    let nomor_ponsel = $('#nomor_ponsel').val();
                    let jenis_kelamin = $('#jenis_kelamin').val();
                    let nama_pengirim = $('#nama_pengirim').val();
                    let alamat = $('#alamat').val();

                    let totalHarga = $('#totalHargaItem').val();
                    let totalOngkir = $('#totalOngkir').val();
                    let totalKeseluruhan = $('#totalKeseluruhan').val();
                    let payment_method = $('#payment_method').val();

                    let namaPaketOngkir = "";
                    if($('#layanan').val() !=""){
                        namaPaketOngkir = $('#kurir').val() + ' - ' + $('#layanan option:selected').text().split("-")[0];
                    }
                    Helper.loadingStart()
                    jQuery.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url:"",
                        type:'POST',
                        dataType:'json',
                        data:{
                            kirim_ke,
                            nama_lengkap,
                            nomor_ponsel,
                            jenis_kelamin,
                            nama_pengirim,
                            alamat,
                            totalHarga,
                            totalOngkir,
                            totalKeseluruhan,
                            namaPaketOngkir,
                            payment_method:payment_method
                        },
                        success:function(data){
                            Helper.loadingStop()
                            swal({
                            title: "Sukses!",
                                text: "Transaksi anda berhasil, silahkan segera lakukan pembayaran!",
                                type: 'success'
                            }, function() {
                                window.location.href='/customer/status-order/pembayaran/'+data.orderId;
                            });
                        },
                        error:function(a,v,c){
                            Helper.loadingStop()
                            var msg = JSON.parse(a.responseText);
                            console.log(msg);
                            swal({
                                title: "Ups!",
                                    text: msg.message,
                                    type: 'error'
                                });
                        }
                    });
                    
                });
            });

            function formatRupiah(bilangan){
                var	number_string = bilangan.toString(),
                    sisa 	= number_string.length % 3,
                    rupiah 	= number_string.substr(0, sisa),
                    ribuan 	= number_string.substr(sisa).match(/\d{3}/g);
                        
                if (ribuan) {
                    separator = sisa ? ',' : '';
                    rupiah += separator + ribuan.join(',');
                }

                return 'Rp. '+ rupiah;
            }

            function convertInteger(id){
                let total = document.getElementById(id).textContent;
                total = total.replace(/ |Rp |\- |\./gi, "")
                total = parseInt(total)
                return total;
            }


        </script>
    @endsection
@endsection