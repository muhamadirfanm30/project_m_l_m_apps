{{-- <script>

    var _distric;
        // select 2 where location id
    $(document).ready(function() {
        if ($("#countries").val() == null) {
            $("#provHide").hide('');
            $("#cityHide").hide('');
            $("#districHide").hide('');
            $("#zipHide").hide('');
            $("#vilageHide").hide('');
        }
        if ($("#province_id").val() == null) {
            $("#provHide").hide('');
        }
        if($("#city_id").val() == null) {
            $("#cityHide").hide('');
        }
        if($("#distric").val() == null) {
            $("#districHide").hide('');
            $("#zipHide").hide('');
        }
        if ($("#vilage").val() == null) {
            $("#vilageHide").hide('');
        }

        $("#countries").select2({
            ajax: {
                type: "GET",
                url: 'http://localhost:8000/api/country/select2',
                data: function(params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function(data) {
                    var res = $.map(data.data, function(item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    });

                    return {
                        results: res
                    };
                }
            }
        });

        $("#countries").change(function() {
            $("#province_id").html('')
            $("#provHide").show('')
            $("#cityHide").hide('')
            $("#districHide").hide('')
            $("#vilageHide").hide('')
            $("#zipHide").hide('');
            provinceSelect($(this).val())
        });
        provinceSelect($(this).val())

        $("#province_id").change(function() {
            $('#city_id').val('');
            $("#cityHide").show('')
            $("#districHide").hide('')
            $("#vilageHide").hide('')
            $("#zipHide").hide('');
            citySelect($(this).val());
        })
        citySelect($(this).val());

        $("#city_id").change(function() {
            $("#distric").val('')
            $("#districHide").show('')
            $("#vilageHide").hide('')
            $("#zipHide").hide('');
            districtSelect($(this).val());
        });
        districtSelect($(this).val());

        $("#distric").change(function() {
            $("#vilage").val('')
            $("#vilageHide").show('')
            $("#zipHide").show('');
            var idDistric = $(this).val()
            var district = _.find(_distric, function(item){
                return item.id == idDistric

            })
            console.log('distric', district)
            vilageSelect($(this).val());
        })
        vilageSelect($(this).val());
    });

    function provinceSelect(id) {
        $("#province_id").select2({
            ajax: {
                type: "GET",
                url: 'http://localhost:8000/api/address_search/find_province?country_id=' + id,
                // url: 'http://localhost:8001/api/address_search/find_district?city_id=' + $(this).val(),
                data: function(params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function(data) {
                    var res = $.map(data.data, function(item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    });

                    return {
                        results: res
                    };
                }
            }
        });
    }

    function citySelect(id) {
        $("#city_id").select2({
            ajax: {
                type: "GET",
                url: 'http://localhost:8000/api/address_search/find_city?province_id=' + id,
                // url: 'http://localhost:8001/api/address_search/find_village?district_id=' + $(this).val(),
                data: function(params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function(data) {
                    var res = $.map(data.data, function(item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    });

                    return {
                        results: res
                    };
                }
            }
        });
    }

    function districtSelect(id) {
        $("#distric").select2({
            ajax: {
                type: "GET",
                url: 'http://localhost:8000/api/address_search/find_district?city_id=' + id,
                // url: 'http://localhost:8001/api/address_search/find_village?district_id=' + $(this).val(),
                data: function(params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function(data) {
                    _distric = data.data
                    var res = $.map(data.data, function(item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    });

                    return {
                        results: res
                    };
                }
            }
        });
    }

    function vilageSelect(id){
        $("#vilage").select2({
            ajax: {
                type: "GET",
                url:'http://localhost:8000/address_search/find_village?district_id=' + id,
                // url: 'http://localhost:8001/api/address_search/find_village?district_id=' + $(this).val(),
                data: function(params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function(data) {
                    var res = $.map(data.data, function(item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    });

                    return {
                        results: res
                    };
                }
            }
        });
    } --}}
</script>