@extends('home')
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<div class="content-wrapper"><br>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-outline card-warning">
                    <div class="card-header">
                        <h3 class="card-title">
                            Cart
                        </h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="col-md-12">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th colspan="2">Product</th>
                                        <th>Quantity</th>
                                        <th class="text-center">Price</th>
                                        {{-- <th colspan="2">Action</th> --}}
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                    $total = 0;
                                    $berat = 0;
                                    @endphp

                                    @if (session('cart'))
                                    @foreach (session('cart') as $id => $details)
                                    @php
                                    $total += $details['harga'] * $details['qty'];
                                    $price = $details['harga'] * $details['qty'];
                                    $berat += $details['berat'] * $details['qty'];
                                    @endphp
                                    <tr>
                                        <td>
                                            <a class="thumbnail pull-left" href="#">
                                                <img width="100" height="100" class="img-responsive" src="{{ url('storage/product-image/'.$details['image_produk']) }}">
                                            </a>
                                        </td>
                                        <td>
                                            <h4 class="media-heading"><a href="#">{{$details['nama_produk']}}</a></h4>
                                        </td>
                                        <td>
                                            {{ $details['qty'] }}
                                        </td>
                                        <td class="text-center" data-th="price"><strong>Rp. {{ number_format($price) }}</strong></td>
                                        {{-- <td>
                                                        <button class="btn btn-info btn-sm update-cart" data-id="{{ $id }}"><i class="fa fa-check"></i></button>
                                        <button class="btn btn-danger btn-sm remove-from-cart" data-id="{{ $id }}"><i class="fa fa-trash"></i></button>
                                        </td> --}}
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="3" class="text-right"><strong>Berat</strong></td>
                                        <td class="text-right"><strong>{{ number_format($berat) }} Gram</strong></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" class="text-right"><strong>Total</strong></td>
                                        <td class="text-right"><strong>Rp. {{ number_format($total) }}</strong></td>
                                    </tr>
                                    <tr>
                                        {{-- <td colspan="4">
                                                <a href="{{url('customer/order-produk/checkout')}}" class="btn btn-success btn-sm">
                                        <span class="fa fa-play"></span> &nbsp; Checkout
                                        </a>
                                        </td> --}}
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card card-outline card-warning">
                    <div class="card-header">
                        <h3 class="card-title">
                            Alamat Pengiriman
                        </h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Kirim Ke</label>
                                    <select name="kirim_ke" id="kirim_ke" class="form-control">
                                        <option value="0">Konsumen</option>
                                        <option value="1">Stok Produk Di Stokiest</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Nama Lengkap</label>
                                    <input type="text" id="nama_lengkap" class="form-control" placeholder="Nama Lengkap ...">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Nomor Ponsel Penerima</label>
                                    <input type="text" id="nomor_ponsel" class="form-control" placeholder="Nomor Ponsel ...">
                                </div>
                            </div>
                        </div>
                        <!-- <div class="row">
                            {{-- <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Jenis Kelamin</label>
                                    <input type="text" id="jenis_kelamin" class="form-control" placeholder="Enter ...">
                                </div>
                            </div> --}}
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Nama Pengirim</label>
                                    <input type="text" id="nama_pengirim" class="form-control" placeholder="Nama Pengirim ...">
                                </div>
                            </div>
                        </div> -->
                        <div class="row">
                            <div class="col-sm-4" id="provHide">
                                <div class="form-group">
                                    <label>Provinsi</label>
                                    <select class="form-control" name="province_id" id="province_id" style="width:100%">
                                        <option>Pilih Provinsi</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4" id="cityHide">
                                <div class="form-group">
                                    <label>Kota / Kabupaten</label>
                                    <select class="form-control" name="kota_id" id="kota_id" style="width:100%">
                                        <option>Pilih Provinsi Terlebih dahulu</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4" id="cityHide">
                                <div class="form-group">
                                    <label>Kecamatan</label>
                                    <select class="form-control" name="distrik_id" id="distrik_id" style="width:100%">
                                        <option>Pilih Kota / Kabupaten Terlebih Dahulu</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <!-- text input -->
                                <div class="form-group">
                                    <label>Kurir</label>
                                    <select class="form-control" name="kurir" id="kurir" style="width:100%">
                                        <option>Pilih Kecamatan Terlebih dahulu</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Pilih Layanan</label>
                                    <select name="layanan" id="layanan" class="form-control">
                                    <option>Pilih Kurir Terlebih dahulu</option>
                                    </select>
                                </div>
                            </div>
                            <input type="hidden" value="40" class="form-control" id="city_origin" name="city_origin">
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <!-- text input -->
                                <div class="form-group">
                                    <label>Alamat Lengkap</label>
                                    <textarea name="alamat" class="form-control" id="alamat" cols="30" rows="10"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card card-outline card-warning">
                    <div class="card-header">
                        <h3 class="card-title">
                            Metode Pembayaran
                        </h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="accordion" id="accordionExample">
                            <div class="card">
                                <div class="card-header" id="headingOne">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            Bank Transfer
                                        </button>
                                    </h2>
                                </div>

                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                    <div class="card-body">
                                            <div class="row">
                                                @foreach($listPayment as $key => $bank)
                                                    <div class="col-md-4">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" id="payment_method" name="payment_method" value="{{$bank->nama_bank}}">
                                                            <label class="form-check-label">
                                                                <img src="{{url('storage/bank-icon/'.$bank->image)}}" alt="{{$bank->nama_bank}}" width="80px">
                                                            </label>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingTwo">
                                <h2 class="mb-0">
                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        Midtrans
                                    </button>
                                </h2>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                <div class="card-body">
                                    <div class="col-md-4">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="payment_method" value="0">
                                            <label class="form-check-label">
                                                <img src="https://docs.midtrans.com/asset/image/main/midtrans-logo.png" alt="Midtrans" width="90px">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="col-md-12">
                            <div class="card card-outline card-warning">
                                <div class="card-header">
                                    <h3 class="card-title">
                                        Ringkasan Pesanan
                                    </h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <table id="list-card-product" class="display" style="width:100%">
                                        <tbody>
                                            <tr>
                                                <td colspan="2">Subtotal Produk</td>
                                                <td id="lbltotalHargaItem">Rp. {{ number_format($total) }}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">Subtotal Pengiriman</td>
                                                <td><span id="lbltotalOngkir"></span></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">Subtotal Pesanan</td>
                                                <td id="lbltotalKeseluruhan"></td>
                                            </tr>
                                            <!-- <tr>
                                                <td colspan="2">Metode Pembayaran</td>
                                                <td>
                                                    <select id="payment_method" class="form-control">
                                                        <option value="">Pilih Metode Pembayaran</option>
                                                        <optgroup label="Transfer Bank">
                                                            <option value="bca">BCA</option>
                                                        </optgroup>
                                                    </select>
                                                </td>
                                            </tr> -->
                                        </tbody>
                                    </table>
                                </div>
                                <input type="hidden" name="totalHargaItem" id="totalHargaItem" value="{{$total}}">
                                <input type="hidden" name="totalOngkir" id="totalOngkir">
                                <input type="hidden" name="weight" value="{{$berat}}">
                                <input type="hidden" name="totalKeseluruhan" id="totalKeseluruhan" value="{{$total}}">
                                <div class="card-footer">
                                    <button class="btn btn-primary btn-block" id="checkout">Checkout</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer"></div>
                </div>
            </div>
        </div>
    </section>
</div>
@section('javascript')
@parent
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>

    $(document).ready(function() {
        onlyNumber('#nomor_ponsel');
        // load provinsi
        ajaxRequest('GET', '/api/rajaongkir/provinsi', function(response) {
            createSelect2('#province_id',
                $.map(response.data.rajaongkir.results, function(item) {
                    return {
                        text: item.province,
                        id: item.province_id
                    }
                })
            )
        })
        

        // on chnege provinsi
        $('#province_id').change(function() {
            province_id = $(this).val();
            if(province_id == ""){
                swal({
                    title: "Ups!",
                    text: "Silahkan Pilih Provinsi Yang Tersedia! ",
                    type: 'error'
                });
            }else{
                ajaxRequest('GET', '/api/rajaongkir/city?province=' + province_id, function(response) {
                    createSelect2('#kota_id',
                        $.map(response.data.rajaongkir.results, function(item) {
                            return {
                                text: item.city_name,
                                id: item.city_id
                            }
                        })
                    )
                })
                // empty select
                $('#kota_id').html('').append('<option value="">Pilih Kota / Kabupaten</option>');
                $('#distrik_id').html('').append('<option value="">Pilih Kota / Kabupaten Terlebih Dahulu</option>');
                $('#kurir').html('').append('<option value="">Pilih Kecamatan Terlebih Dahulu</option>');
                $('#layanan').html('').append('<option value="">Pilih Kurir Terlebih Dahulu</option>');
            }
        })

        // on chnege city
        $('#kota_id').change(function() {
            city_id = $(this).val();
            if(city_id == ""){
                swal({
                    title: "Ups!",
                    text: "Silahkan Pilih Kota / Kabupaten Yang Tersedia! ",
                    type: 'error'
                });
            }else{
                // load subdistrict
                ajaxRequest('GET', '/api/rajaongkir/subdistrict?city=' + city_id, function(response) {
                    createSelect2('#distrik_id',
                        $.map(response.data.rajaongkir.results, function(item) {
                            return {
                                text: item.subdistrict_name,
                                id: item.subdistrict_id
                            }
                        })
                    )
                })

                // empty select
                $('#distrik_id').html('').append('<option value="">Pilih Kecamatan</option>');
                $('#kurir').html('').append('<option value="">Pilih Kecamatan Terlebih Dahulu</option>');
                $('#layanan').html('').append('<option value="">Pilih layanan terlebih dahulu</option>');
            }
        })

        // on chnege distrik
        $('#distrik_id').change(function() {
            if($(this).val() == ""){
                swal({
                    title: "Ups!",
                    text: "Silahkan Pilih Kecamatan Yang Tersedia! ",
                    type: 'error'
                });
            }else{
                // load subdistrict
                ajaxRequest('GET', '/api/rajaongkir/courier', function(response) {
                    createSelect2('#kurir',
                        $.map(response.data, function(item) {
                            return {
                                text: item.text,
                                id: item.code
                            }
                        })
                    )
                })

                // empty select
                $('#kurir').html('').append('<option value="">Pilih kurir</option>');
                $('#layanan').html('').append('<option value="">Pilih layanan</option>');
            }
        })

        // on chnege distrik
        $('#kurir').change(function() {
            if($(this).val() == ""){
                swal({
                    title: "Ups!",
                    text: "Silahkan Pilih Kecamatan Yang Tersedia! ",
                    type: 'error'
                });
            }else{
                ajaxGetCostRajaOngkir(function(response) {
                    data = loopingLayananRajaOngkir(response.data.rajaongkir);
                    createSelect2('#layanan', data);
                })
            }
        })

        $('select[name="layanan"]').on('change', function() {
            let price = $('#layanan').val();
            $('#lbltotalOngkir').val(price);
            $('#totalOngkir').val(price);
            var hrgItem = $('#totalHargaItem').val();
            let total = parseInt(hrgItem) + parseInt(price);
            $('#lbltotalOngkir').text(formatRupiah(price));
            $('#lbltotalKeseluruhan').text(formatRupiah(total));
            $('#totalKeseluruhan').val(total);
        });

        $('select[name="kirim_ke"]').on('change', function() {
            let val = $(this).val();
            if (val == 1) {
                $('#nama_lengkap').attr("readonly", "true");
                $('#nomor_ponsel').attr("readonly", "true");
                $('#jenis_kelamin').attr("readonly", "true");
                // $('#nama_pengirim').attr("readonly", "true");
                $('#alamat').attr("readonly", "true");

                $('select[name="province_id"]').attr("readonly", "true");
                $('select[name="kurir"]').attr("readonly", "true");
                $('select[name="kota_id"]').attr("readonly", "true");
                $('select[name="layanan"]').attr("readonly", "true");
            } else {
                $('#nama_lengkap').removeAttr("readonly");
                $('#nomor_ponsel').removeAttr("readonly");
                $('#jenis_kelamin').removeAttr("readonly");
                // $('#nama_pengirim').removeAttr("readonly");
                $('#alamat').removeAttr("readonly");

                $('select[name="province_id"]').removeAttr("readonly");
                $('select[name="kurir"]').removeAttr("readonly");
                $('select[name="kota_id"]').removeAttr("readonly");
                $('select[name="layanan"]').removeAttr("readonly");
            }
        });

        $('#checkout').on('click', function() {
            let kirim_ke = $('#kirim_ke').val();
            let nama_lengkap = $('#nama_lengkap').val();
            let nomor_ponsel = $('#nomor_ponsel').val();
            let jenis_kelamin = $('#jenis_kelamin').val();
            // let nama_pengirim = $('#nama_pengirim').val();
            let alamat = $('#alamat').val();

            let totalHarga = $('#totalHargaItem').val();
            let totalOngkir = $('#totalOngkir').val();
            let totalKeseluruhan = $('#totalKeseluruhan').val();
            let payment_method = $('#payment_method').val();
            let namaPaketOngkir = "";
            if ($('#layanan').val() != "") {
                namaPaketOngkir = $('#kurir').val() + ' - ' + $('#layanan option:selected').text().split("-")[0];
            }

            dataInput = {
                kirim_ke,
                nama_lengkap,
                nomor_ponsel,
                jenis_kelamin,
                // nama_pengirim,
                alamat,
                totalHarga,
                totalOngkir,
                totalKeseluruhan,
                namaPaketOngkir,
                payment_method: payment_method
            };

            console.log('dataInput', dataInput);

            Helper.loadingStart()
            jQuery.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "",
                type: 'POST',
                dataType: 'json',
                data: dataInput,
                success: function(data) {
                    Helper.loadingStop()
                    swal({
                        title: "Sukses!",
                        text: "Transaksi anda berhasil, silahkan segera lakukan pembayaran!",
                        type: 'success'
                    }, function() {
                        window.location.href = '/customer/status-order/pembayaran/' + data.orderId;
                    });
                },
                error: function(a, v, c) {
                    Helper.loadingStop()
                    var msg = JSON.parse(a.responseText);
                    console.log(msg);
                    swal({
                        title: "Ups!",
                        text: msg.message,
                        type: 'error'
                    });
                }
            });

        });
    });

    function formatRupiah(bilangan) {
        var number_string = bilangan.toString(),
            sisa = number_string.length % 3,
            rupiah = number_string.substr(0, sisa),
            ribuan = number_string.substr(sisa).match(/\d{3}/g);

        if (ribuan) {
            separator = sisa ? ',' : '';
            rupiah += separator + ribuan.join(',');
        }

        return 'Rp. ' + rupiah;
    }

    function convertInteger(id) {
        let total = document.getElementById(id).textContent;
        total = total.replace(/ |Rp |\- |\./gi, "")
        total = parseInt(total)
        return total;
    }
</script>
@endsection
@endsection