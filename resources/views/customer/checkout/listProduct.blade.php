@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-outline card-info">

                        <div class="card-header">
                            <h3 class="card-title">
                            Daftar Paket Upgrade Membership Mobile Stokiest
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="col">
                                <div class="row">
                                    <div class="col-12 col-md-6 col-lg-4">
                                        <div class="card">
                                            <img class="card-img-top" src="https://dummyimage.com/600x400/55595c/fff" alt="Card image cap">
                                            <div class="card-body">
                                                <h4 class="card-title"><a href="product.html" title="View Product">Product title</a></h4>
                                                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                                <div class="row">
                                                    <div class="col">
                                                        <p class="btn btn-danger btn-block">Rp. 150.000</p>
                                                    </div>
                                                    <div class="col">
                                                        <a href="{{ url('/customer/product/checkout-list') }}" class="btn btn-success btn-block">Checkout</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    {{-- modal --}}

    @include('customer.memberships._modalUpgradeMs')
@endsection
