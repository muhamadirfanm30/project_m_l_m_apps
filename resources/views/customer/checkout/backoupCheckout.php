<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Model\ItemTransaction;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Model\Transaction;
use App\Helpers\Midtrans;
use App\Mail\OrderEmail;
use App\Model\Product;
use Carbon\Carbon;
use Validator;
use Auth;

class ListProductAndPromoController extends Controller
{
    public function orderProduk(Request $request)
    {
        $showProduct = Product::whereNull('harga_promo')->paginate(17);
        return view('customer.order-product.showProduct', compact('showProduct'))
                ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function orderProdukPromo(Request $request)
    {

        $showProduct = Product::whereNotNull('harga_promo')->paginate(17);
        return view('customer.order-produk-promo.index', compact('showProduct'))
                ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function detailProduk($id)
    {
        $showProduct = Product::where('id', $id)->first();
        return view('customer.order-product.detail', compact('showProduct'));
    }

    public function checkout(Request $request)
    {
        if($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'nama_lengkap'=>'required_if:kirim_ke,0',
                'nomor_ponsel'=>'required_if:kirim_ke,0',
                'jenis_kelamin'=>'required_if:kirim_ke,0',
                'nama_pengirim'=>'required_if:kirim_ke,0',
                'alamat'=>'required_if:kirim_ke,0',
                'namaPaketOngkir'=>'required_if:kirim_ke,0',
                'payment_method'=>'required_if:kirim_ke,0',
            ],[
                'namaPaketOngkir.required'=>'Layanan Kurir Wajib Dipilih'
            ]);
            
            if ($validator->fails()) {
                $messages = '';
                $i=1;
                foreach ($validator->errors()->all() as $message) {
                    $messages .= $i.". ".$message."\n";
                    $i++;
                }
                $messages =rtrim($messages,"\n");

                $err = [
                    'status'=>false,
                    'message'=>$messages
                ];
                return response()->json($err, 400, [] , JSON_UNESCAPED_SLASHES);
            }

            $orderId = Str::random(8).auth()->user()->id;
            $item = [];
            foreach (session('cart') as $id => $details){
                $item[] = [
                    'own_transaction'=>$request->kirim_ke,//0 Ke Konsumen & 1 : Nimbun
                    'orderId'=>$orderId,
                    'product_id'=>$details['id'],
                    'qty'=> $details['qty'],
                    'harga'=>$details['qty'] * $details['harga'],
                    'created_at'=>now(),
                    'updated_at'=>now(),
                    // 'status'=> 0,
                    // 'cs_id'=>auth()->user()->id
                ];

                $decrementStok = Product::find($details['id']);
                if($details['qty'] > $decrementStok->stok){
                    $err = [
                        'status'=>false,
                        'message'=>"Sisa stok ".$details['nama_produk']." : ".$decrementStok->stok
                    ];
                    return response()->json($err, 400, [] , JSON_UNESCAPED_SLASHES);
                }

                $stok = (int)$decrementStok->stok - (int)$details['qty'];
                $decrementStok->update(['stok'=>$stok]);
            }

            $hitPayment = Midtrans::Transaction($orderId,$request->totalKeseluruhan,$request->payment_method);
            if($hitPayment['status'] == true){
                $detailTransaction = [
                    'own_transaction'=>$request->kirim_ke,//0 Ke Konsumen & 1 : Nimbun
                    'orderId'=>$orderId,
                    'nama_lengkap' =>$request->nama_lengkap,
                    'nomor_ponsel' =>$request->nomor_ponsel,
                    'jenis_kelamin' =>$request->jenis_kelamin,
                    'nama_pengirim' =>$request->nama_pengirim,
                    'alamat' =>$request->alamat,
                    'kurir'=>$request->namaPaketOngkir,
                    'totalHarga' =>$request->totalHarga,
                    'totalOngkir' =>$request->totalOngkir,
                    'totalKeseluruhan' =>$request->totalKeseluruhan,
                    'payment_method'=>$request->payment_method,
                    'va'=>$hitPayment['detailVA'][0]->va_number,
                    'cs_id'=>auth()->user()->id,
                    'kategori' => 'Pembelian Produk',
                    'type' => 0,
                ];
                $saveTransaction = Transaction::create($detailTransaction);
                $saveItem = ItemTransaction::insert($item);
                $this->sendMail($orderId);
                $resp = [
                    'status'=>true,
                    'orderId'=>$orderId,
                ];
                session()->forget('cart');
                return response()->json($resp, 200, [] , JSON_UNESCAPED_SLASHES);
            }else{
                return response()->json($hitPayment, 400, [] , JSON_UNESCAPED_SLASHES);
            }
        }
        $provinsi = $this->get_province();
        return view('customer.checkout.checkout-list', compact('provinsi'));
    }

    // public function sendMail()
    // {
    //     if(!empty(Auth::user()->email)){
    //         Mail::to(Auth::user()->email)->send(new OrderEmail($trxId));
    //         return "Email telah dikirim";
    //     }else{
    //         return "Email Gagal Terkirim";
    //     }
        
    // }

    public function sendMail($orderId)
    {
        if(!empty(Auth::user()->email)){
            Mail::to(Auth::user()->email)->send(new OrderEmail($orderId));
            return "Email telah dikirim";
        }else{
            return "Email Gagal Terkirim";
        }
        
    }

    public function get_province(){
        $curl = curl_init();

        curl_setopt_array($curl, array(
        //   CURLOPT_URL => "https://api.rajaongkir.com/starter/province",
          CURLOPT_URL => "https://pro.rajaongkir.com/api/province",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            // "key: 60b137e92094b5e6fa05ddd10e2c1cbb"
            "key: 17461d353db677af78769faf2be1cb82"
          ),
        ));
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        
        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
            //ini kita decode data nya terlebih dahulu
            $response=json_decode($response,true);
            //ini untuk mengambil data provinsi yang ada di dalam rajaongkir resul
            $data_pengirim = $response['rajaongkir']['results'];
            return $data_pengirim;
        }
    }
    
    public function get_city($id){
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://pro.rajaongkir.com/api/city?&province=$id",
        //   CURLOPT_URL => "https://api.rajaongkir.com/starter/city?&province=$id",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "key: 17461d353db677af78769faf2be1cb82"
            // "key: 60b137e92094b5e6fa05ddd10e2c1cbb"
          ),
        ));
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        
        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
            $response=json_decode($response,true);
            $data_kota = $response['rajaongkir']['results'];
            return json_encode($data_kota);
        }
    }
    
    public function get_district($id)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://pro.rajaongkir.com/api/subdistrict?city=$id",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "key: 17461d353db677af78769faf2be1cb82"
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
          } else {
              $response=json_decode($response,true);
              $data_distrik = $response['rajaongkir']['results'];
              return json_encode($data_distrik);
          }
    }

    public function get_ongkir($origin, $destination, $destinationType, $weight, $courier){
        $curl = curl_init();
        curl_setopt_array($curl, array(
            // CURLOPT_URL => "https://api.rajaongkir.com/starter/cost",
            CURLOPT_URL => "https://pro.rajaongkir.com/api/cost",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "origin=$origin&originType=city&destination=$destination&destinationType=subdistrict&weight=$weight&courier=$courier",
            // CURLOPT_POSTFIELDS => "origin=79&originType=city&destination=78&destinationType=1027&weight=900&courier=jne",
            CURLOPT_HTTPHEADER => array(
                "content-type: application/x-www-form-urlencoded",
                "key: 17461d353db677af78769faf2be1cb82"
                // "key: 60b137e92094b5e6fa05ddd10e2c1cbb"
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $response=json_decode($response,true);
            // return $response;
            $data_ongkir = $response['rajaongkir']['results'];
            return json_encode($data_ongkir);
        }
    }

    public function addToChart($id, $qty = 1)
    {
        $product = Product::find($id);
        if(!$product){
            abort(404);
        }
        $cart = session()->get('cart');
        if(!$cart){
            $cart = [
                $id => [
                    "id"=>$id,
                    "nama_produk" => $product->nama_produk,
                    "qty" => $qty,
                    "harga" => $product->harga,
                    "harga_promo" => $product->harga_promo,
                    "image_produk" => $product->image_produk,
                    "berat" => $product->berat,
                    "panjang" => $product->panjang,
                    "lebar" => $product->lebar,
                    "tinggi" => $product->tinggi,
                ]
            ];
            session()->put('cart', $cart);
            return redirect()->back()->with('success', 'Product added to cart successfully!');
        }

        if(isset($cart[$id])){
            $cart[$id]['qty']+=$qty;
            session()->put('cart', $cart);
            return redirect()->back()->with('success', 'Product added to cart successfully!');
        }

        $cart[$id] = [
            "id"=>$id,
            "nama_produk" => $product->nama_produk,
            "qty" => $qty,
            "harga" => $product->harga,
            "harga_promo" => $product->harga_promo,
            "image_produk" => $product->image_produk,
            "berat" => $product->berat,
            "panjang" => $product->panjang,
            "lebar" => $product->lebar,
            "tinggi" => $product->tinggi,
        ];

        session()->put('cart', $cart);
        return redirect()->back()->with('success', 'Product added to cart successfully!');
    }

    public function update(Request $request)
    {
        if($request->id and $request->qty)
        {
            $cekStok = Product::find($request->id);
            if($request->qty > $cekStok->stok ){
                session()->flash('success', 'Stok produk hanya tersisa : '.$cekStok->stok);
            }else{
                $cart = session()->get('cart');
                $cart[$request->id]["qty"] = $request->qty;
                session()->put('cart', $cart);
                session()->flash('success', 'Cart updated successfully');
            }
        }
    }
    public function remove(Request $request)
    {
        if($request->id) {
            $cart = session()->get('cart');
            if(isset($cart[$request->id])) {
                unset($cart[$request->id]);
                session()->put('cart', $cart);
            }
            session()->flash('success', 'Product removed successfully');
        }
    }

    public function cart()
    {
        return view('customer.cart.cart');
    }

    public function itemCheckout($id,$qty){
        $this->addToChart($id,$qty);
        return redirect()->route('pageCart');
    }
}
