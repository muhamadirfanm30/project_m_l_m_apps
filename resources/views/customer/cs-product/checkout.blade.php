@extends('home')
@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-outline card-warning">
                        <div class="card-header">
                            <h3 class="card-title">
                                Cart
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="col-md-12">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="2">Product</th>
                                            <th>Berat</th>
                                            <th>Quantity</th>
                                            <th class="text-center"></th>
                                        </tr>
                                    </thead>
                                    @if (count($dataProduct) > 0)
                                        <tbody>
                                            @php
                                                $totalBerat = 0;
                                            @endphp
                                            @foreach($dataProduct as $r)
                                                @php
                                                    $totalBerat += $r->berat;
                                                @endphp
                                                <tr>
                                                    <td style="vertical-align:middle">
                                                        <a class="thumbnail pull-left" href="#">
                                                            <img width="100" height="100" class="img-responsive" src="{{ url('storage/product-image/'.$r->image_produk) }}">
                                                        </a>
                                                    </td>
                                                    <td style="vertical-align:middle">
                                                        <h4 class="media-heading"><a href="#">{{$r->nama_produk}}</a></h4>
                                                    </td>
                                                    <td style="vertical-align:middle">
                                                        {{ $r->berat }} gram
                                                    </td>
                                                    <td style="vertical-align:middle;">
                                                        <div class="input-group" data-section="{{ $r->id }}">
                                                            <span class="input-group-btn">
                                                                <button type="button" class="btn btn-danger minuss"  data-type="minus" data-id="{{$r->id}}" data-price="{{ $r->harga }}" data-weight="{{ $r->berat }}" data-product="{{ $r->nama_produk }}">
                                                                    <span class="fa fa-minus"></span>
                                                                </button>
                                                            </span>
                                                            <input type="text" class="form-control qty input{{$r->id}}" name="qty[]" id="qty-{{$r->id}}" value="1" data-totalqty="{{ $r->stokCS($r->id) }}" data-weight="{{ $r->berat }}" data-product="{{ $r->nama_produk }}" data-productId="{{$r->id}}" style="text-align: center; width:10px " readonly>
                                                            <span class="input-group-btn">
                                                                <button type="button" class="btn btn-success pluss" data-type="plus" data-id="{{$r->id}}" data-price="{{ $r->harga }}" data-weight="{{ $r->berat }}" data-product="{{ $r->nama_produk }}" data-totalqty="{{ $r->stokCS($r->id) }}">
                                                                    <span class="fa fa-plus"></span>
                                                                </button>
                                                            </span>
                                                        </div>
                                                    </td>
                                                    <td style="vertical-align:middle" class="text-center">
                                                        <a class="nav-link" data-slide="true" role="button" onclick="cancelItem({{$r->id}})">
                                                            Hapus
                                                        </a>
                                                        <form id="delete-item-{{ $r->id }}" action="{{ route('sent-myproduct') }}" method="post" style="display: none;">
                                                            @csrf
                                                            @method('DELETE')
                                                            <input type="hidden" value="{{$r->id}}" name="productId">
                                                        </form>

                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="4" class="text-right" style="vertical-align:middle"><strong>Berat</strong></td>
                                                <td class="text-right" style="vertical-align:middle"><strong id="lblBerat">{{ number_format(0) }} Gram</strong></td>
                                                <input type="hidden" name="weight" id="berat_paket">
                                            </tr>
                                            <tr>
                                                <td colspan="4" class="text-right" style="vertical-align:middle"><strong>Total</strong></td>
                                                <td class="text-right" style="vertical-align:middle"><strong>Rp. {{ number_format(0) }}</strong></td>
                                            </tr>
                                        </tfoot>
                                    @else
                                        <tbody>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td style="vertical-align:middle"><strong>No Data Found</strong></td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    @endif
                                </table>
                            </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-outline card-warning">
                        <div class="card-header">
                            <h3 class="card-title">
                                Alamat Pengiriman
                            </h3>
                        </div>
                        <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Nama Lengkap</label>
                                            <input type="text" id="nama_lengkap" class="form-control" placeholder="Nama Lengkap ...">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Nomor Ponsel Penerima</label>
                                            <input type="text" id="nomor_ponsel" class="form-control" placeholder="Nomor Ponsel ...">
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="row">
                                    {{-- <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Jenis Kelamin</label>
                                            <input type="text" id="jenis_kelamin" class="form-control" placeholder="Enter ...">
                                        </div>
                                    </div> --}}
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>Nama Pengirim</label>
                                            <input type="text" id="nama_pengirim" class="form-control" placeholder="Nama Pengirim ...">
                                        </div>
                                    </div>
                                </div> -->
                                <div class="row">
                                    <div class="col-sm-6" id="provHide">
                                        <div class="form-group">
                                            <label>Provinsi</label>
                                            <select class="form-control" name="province_id" id="province_id" style="width:100%">
                                                <option>Pilih Provinsi</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6" id="cityHide">
                                        <div class="form-group">
                                            <label>Kota / Kabupaten</label>
                                            <select class="form-control" name="kota_id" id="kota_id" style="width:100%">
                                                <option>Pilih Provinsi Terlebih dahulu</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6" id="cityHide">
                                        <div class="form-group">
                                            <label>Kecamatan</label>
                                            <select class="form-control" name="distrik_id" id="distrik_id" style="width:100%">
                                                <option>Pilih Kota / Kabupaten Terlebih Dahulu</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Kode Pos</label>
                                            <input type="text" name="kode_pos" class="form-control" id="kodepos">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Kurir</label>
                                            <select class="form-control" name="kurir" id="kurir" style="width:100%">
                                                <option value="">Pilih Kecamatan Terlebih dahulu</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Pilih Layanan</label>
                                            <select name="layanan" id="layanan" class="form-control">
                                                <option value="">Pilih Kurir Terlebih dahulu</option>
                                            </select>
                                        </div>
                                    </div>
                                    <input type="hidden" value="40" class="form-control" id="city_origin" name="city_origin">
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Alamat Lengkap</label>
                                            <textarea name="alamat" class="form-control" id="alamat" cols="30" rows="10"></textarea>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- <div class="row">
                <div class="col-md-12">
                    <div class="card card-outline card-warning">
                        <div class="card-header">
                            <h3 class="card-title">
                                Ringkasan Pesanan
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table  id="list-card-product" class="display" style="width:100%">
                                <tbody>
                                    <tr>
                                        <td colspan="2">Total Harga</td>
                                        <td id="lbltotalHargaItem">Rp. {{ number_format(0) }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">Ongkos Kirim</td>
                                        <td><span id="lbltotalOngkir"></span></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">Total</td>
                                        <td id="lbltotalKeseluruhan"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">Metode Pembayaran</td>
                                        <td>
                                            <select id="payment_method" class="form-control">
                                                <option value="">Pilih Metode Pembayaran</option>
                                                <optgroup label="Transfer Bank">
                                                    <option value="bca">BCA</option>
                                                </optgroup>
                                            </select>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <input type="hidden" name="totalHargaItem" id="totalHargaItem" value="0">
                        <input type="hidden" name="totalOngkir" id="totalOngkir" >
                        <input type="hidden" name="weight" id="weight">
                        <input type="hidden" name="totalKeseluruhan" id="totalKeseluruhan" value="0">
                        <div class="card-footer">
                            @if (count($dataProduct) > 0)
                                <button class="btn btn-primary btn-block disabled_btn" id="checkout">Checkout</button>
                            @else
                                <button class="btn btn-primary btn-block disabled_btn" id="checkout" disabled>Checkout</button>
                            @endif
                        </div>
                    </div>
                </div>
            </div> --}}
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-outline card-warning">
                        <div class="card-header">
                            <h3 class="card-title">
                                Metode Pembayaran
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="accordion" id="accordionExample">
                                <div class="card">
                                    <div class="card-header" id="headingOne">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link" id="accordion_bank" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                Bank Transfer
                                            </button>
                                        </h2>
                                    </div>

                                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                        <div class="card-body">
                                                <div class="row">
                                                    @foreach($listPayment as $key => $bank)
                                                        <div class="col-md-4">
                                                            <div class="form-check">
                                                                <input id="{{ $bank->id }}" class="form-check-input" type="radio"  name="payment_method" value="{{$bank->nama_bank}}">
                                                                <label for="{{ $bank->id }}" class="form-check-label">
                                                                    <img src="{{url('storage/bank-icon/'.$bank->image)}}" alt="{{$bank->nama_bank}}" width="50px">
                                                                </label>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                        </div>
                                    </div>
                                </div>
                                @if (!empty($midtrans->payment_name))
                                    <div class="card">
                                        <div class="card-header" id="headingTwo">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                {{ $midtrans->payment_name }}
                                            </button>
                                        </h2>
                                        </div>
                                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="col-md-4">
                                                <div class="form-check">
                                                    <input id="01" class="form-check-input" type="radio" name="payment_method" value="0">
                                                    <label for="01" class="form-check-label">
                                                        <img src="https://docs.midtrans.com/asset/image/main/midtrans-logo.png" alt="Midtrans" width="90px">
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <hr>
                            <div class="col-md-12">
                                <div class="card card-outline card-warning">
                                    <div class="card-header">
                                        <h3 class="card-title">
                                            Ringkasan Pesanan
                                        </h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <table id="list-card-product" class="display" style="width:100%">
                                            <tbody>
                                                <tr>
                                                    <td colspan="2">Total Harga</td>
                                                    <td id="lbltotalHargaItem">Rp. {{ number_format(0) }}</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">Ongkos Kirim</td>
                                                    <td><span id="lbltotalOngkir"></span></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">Total</td>
                                                    <td id="lbltotalKeseluruhan"></td>
                                                </tr>
                                                <!-- <tr>
                                                    <td colspan="2">Metode Pembayaran</td>
                                                    <td>
                                                        <select id="payment_method" class="form-control">
                                                            <option value="">Pilih Metode Pembayaran</option>
                                                            <optgroup label="Transfer Bank">
                                                                <option value="bca">BCA</option>
                                                            </optgroup>
                                                        </select>
                                                    </td>
                                                </tr> -->
                                            </tbody>
                                        </table>
                                    </div>
                                    <input type="hidden" name="totalHargaItem" id="totalHargaItem" value="0">
                                    <input type="hidden" name="totalOngkir" id="totalOngkir" >
                                    <input type="hidden" name="weight" id="weight">
                                    <input type="hidden" name="totalKeseluruhan" id="totalKeseluruhan" value="0">
                                    <div class="card-footer">
                                        @if (count($dataProduct) > 0)
                                            <button class="btn btn-primary btn-block disabled_btn" id="checkout">Checkout</button>
                                        @else
                                            <button class="btn btn-primary btn-block disabled_btn" id="checkout" disabled>Checkout</button>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer"></div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @section('javascript')
        @parent
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
        <script type="text/javascript" src="https://app.sandbox.midtrans.com/snap/snap.js" data-client-key="{{ env('MD_CLIENT_KEY') }}"></script>
        <script>
            $(document).ready(function(){
                onlyNumber('#nomor_ponsel');
                var berat = $('#berat_paket').val();
                if(berat == 0){
                    $('#nama_lengkap').removeAttr("readonly");
                    $('#nomor_ponsel').removeAttr("readonly");
                    $('#jenis_kelamin').removeAttr("readonly");
                    // $('#nama_pengirim').removeAttr("readonly");
                    $('#alamat').removeAttr("readonly");
                    $('.disabled_btn').removeAttr("disabled");
                    $('#accordion_midtrans').removeAttr("disabled");
                    $('#accordion_bank').removeAttr("disabled");

                    $('select[name="province_id"]').removeAttr("readonly");
                    $('select[name="kurir"]').removeAttr("readonly");
                    $('select[name="kota_id"]').removeAttr("readonly");
                    $('select[name="distrik_id"]').removeAttr("readonly");
                    $('select[name="layanan"]').removeAttr("readonly");
                    $('input[name=payment_method]').removeAttr("disabled","true");
                }else {
                    $('#nama_lengkap').attr("readonly","true");
                    $('#nomor_ponsel').attr("readonly","true");
                    $('#jenis_kelamin').attr("readonly","true");
                    // $('#nama_pengirim').attr("readonly","true");
                    $('#alamat').attr("readonly","true");
                    $('input[name=payment_method]').attr("disabled","true");

                    $('select[name="province_id"]').attr("readonly","true");
                    $('select[name="kurir"]').attr("readonly","true");
                    $('select[name="kota_id"]').attr("readonly","true");
                    $('select[name="distrik_id"]').attr("readonly", "true");
                    $('select[name="layanan"]').attr("readonly","true");
                    $('#payment_method').attr("readonly","true");
                    $('.disabled_btn').attr("disabled", "true");
                    $('#accordion_midtrans').attr("disabled", "true");
                    $('#accordion_bank').attr("disabled", "true");
                    swal({
                        title: "Ups!",
                        text: 'tidak ada produk yg bisa dikirim.',
                        type: 'error'
                    });
                }
            });
            
            function ajaxGetCostRajaOngkir(callbackSuccess = null) {
                costParam = {
                    origin: $('#distrik_id').val(), // ID kota/kabupaten atau kecamatan asal
                    originType: 'subdistrict', // Tipe origin: 'city' atau 'subdistrict'.
                    weight:  $('#berat_paket').val(), // Berat kiriman dalam gram
                    courier: $('#kurir').val(),
                };

                $.ajax({
                    url: '/api/rajaongkir/cost',
                    type: 'POST',
                    data: costParam,
                    success: function(response) {
                        callbackSuccess(response);
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus, errorThrown);
                    }
                });
            }

            function createSelect2(selector, data) {
                console.log('createSelect2', data)
                $(selector).select2({
                    data: data
                });
            }

            $(document).ready(function(){
                onlyNumber('#nomor_ponsel');
                onlyNumber('#kode_pos');
                let totalBerat = 0;
                let getWeight = 0;
                var values = $('input[type="text"][name="qty[]"]').map(function(){
                    let data = $(this).data();
                    totalBerat += data['weight'];
                }).get();
                $('#lblBerat').text(totalBerat+" Gram");
                $('#berat_paket').val(totalBerat);
                
                ajaxRequest('GET', '/api/rajaongkir/provinsi', function(response) {
                    var select = '<option value="">Pilih Provinsi</option>';
                    $.each(response.data.rajaongkir.results, function(item,item2) {
                        // console.log(item2)
                        select += `<option value="${item2.province_id}">${item2.province}</option>` ;
                    });
                    $('#province_id').html(select)
                }) 

                // on chnege provinsi
                $('#province_id').change(function() {
                    province_id = $(this).val();
                    if(province_id == ""){
                        swal({
                            title: "Ups!",
                            text: "Silahkan Pilih Provinsi Yang Tersedia! ",
                            type: 'error'
                        });
                    }else{
                        Helper.loadingStart()
                        ajaxRequest('GET', '/api/rajaongkir/city?province=' + province_id, function(response) {
                            console.log(response)
                            var city = '<option value="">Pilih Kota / Kabupaten</option>';
                            $.each(response.data.rajaongkir.results, function(item,item2) {
                                city += `<option value="${item2.city_id}">${item2.type + ' ' + item2.city_name}</option>`;
                            });
                            Helper.loadingStop()
                            $('#kota_id').html(city)
                        })
                        // empty select
                        $('#kota_id').html('').append('<option value="">Pilih Kota / Kabupaten</option>');
                        $('#distrik_id').html('').append('<option value="">Pilih Kota / Kabupaten Terlebih Dahulu</option>');
                        $('#kurir').html('').append('<option value="">Pilih Kecamatan Terlebih Dahulu</option>');
                        $('#layanan').html('').append('<option value="">Pilih Kurir Terlebih Dahulu</option>');
                    }
                })

                // on chnege city
                $('#kota_id').change(function() {
                    city_id = $(this).val();
                    if(city_id == ""){
                        swal({
                            title: "Ups!",
                            text: "Silahkan Pilih Kota / Kabupaten Yang Tersedia! ",
                            type: 'error'
                        });
                    }else{
                        Helper.loadingStart()
                        // load subdistrict
                        ajaxRequest('GET', '/api/rajaongkir/subdistrict?city=' + city_id, function(response) {
                            var distrik = '<option value="">Pilih Kecamatan</option>';
                            $.each(response.data.rajaongkir.results, function(item,item2) {
                                distrik += `<option value="${item2.subdistrict_id}">${item2.subdistrict_name}</option>`;
                            })
                            Helper.loadingStop()
                            $('#distrik_id').html(distrik)
                        })

                        // empty select
                        $('#kodepos').val("");
                        $('#distrik_id').html('').append('<option value="">Pilih Kecamatan</option>');
                        $('#kurir').html('').append('<option value="">Pilih Kecamatan Terlebih Dahulu</option>');
                        $('#layanan').html('').append('<option value="">Pilih layanan terlebih dahulu</option>');
                    }
                })

                // on chnege distrik
                $('#distrik_id').change(function() {
                    // load subdistrict
                    if($(this).val() == ""){
                        swal({
                            title: "Ups!",
                            text: "Silahkan Pilih Kecamatan Yang Tersedia! ",
                            type: 'error'
                        });
                    }else{
                        Helper.loadingStart()
                        ajaxRequest('GET', '/api/rajaongkir/courier', function(response) {
                            var kurir = '<option value="">Pilih Kurir</option>';
                            $.each(response.data, function(item,item2) {
                                console.log(item2)
                                kurir += `<option value="${item2.code}">${item2.text}</option>`;
                            })
                            Helper.loadingStop()
                            $('#kurir').html(kurir)

                        })
                        // empty select
                        $('#kurir').html('').append('<option value="">Pilih kurir</option>');
                        $('#layanan').html('').append('<option value="">Pilih layanan</option>');
                    }
                })

                function loopingLayananRajaOngkir(rajaongkir) {
                    costs = rajaongkir.results[0].costs; // ambil data pertama
                    if (costs.length == 0) {
                        swal({
                            title: "Ups!",
                            text: "Layanan Tidak Tersedia!",
                            type: 'error'
                        });
                        return [];
                    }

                    return $.map(costs, function(item) {
                        return $.map(item.cost, function(c) {
                            price = c.value < 10000 ? 10000 : c.value;
                            text = item.service + ' - (' + formatRupiah(price) + ') - ' + c.etd +' Hari';
                            return {
                                text: text,
                                id: price
                            }
                        })
                    });
                }

                // on chnege distrik
                $('#kurir').change(function() {
                    if($(this).val() == ""){
                        swal({
                            title: "Ups!",
                            text: "Silahkan Pilih Kecamatan Yang Tersedia! ",
                            type: 'error'
                        });
                    }else{
                        Helper.loadingStart()
                        ajaxGetCostRajaOngkir(function(response) {
                            data = loopingLayananRajaOngkir(response.data.rajaongkir);
                            var cost = '<option value="">Pilih Layanan Jasa</option>';
                            $.each(data, function(item,item2) {
                                console.log(item2)
                                cost += `<<option value="${item2.id}">${item2.text}</option>`;
                            })
                            Helper.loadingStop()
                            $('#layanan').html(cost)
                        })
                    }
                })

                $('select[name="kirim_ke"]').on('change', function(){
                    let val = $(this).val();
                    if(val == 1){
                        $('#nama_lengkap').attr("readonly","true");
                        $('#nomor_ponsel').attr("readonly","true");
                        $('#jenis_kelamin').attr("readonly","true");
                        // $('#nama_pengirim').attr("readonly","true");
                        $('#alamat').attr("readonly","true");

                        $('select[name="province_id"]').attr("readonly","true");
                        $('select[name="kurir"]').attr("readonly","true");
                        $('select[name="kota_id"]').attr("readonly","true");
                        $('select[name="layanan"]').attr("readonly","true");
                    }else{
                        $('#nama_lengkap').removeAttr("readonly");
                        $('#nomor_ponsel').removeAttr("readonly");
                        $('#jenis_kelamin').removeAttr("readonly");
                        // $('#nama_pengirim').removeAttr("readonly");
                        $('#alamat').removeAttr("readonly");

                        $('select[name="province_id"]').removeAttr("readonly");
                        $('select[name="kurir"]').removeAttr("readonly");
                        $('select[name="kota_id"]').removeAttr("readonly");
                        $('select[name="layanan"]').removeAttr("readonly");
                    }
                });

                $('select[name="layanan"]').on('change', function() {
                    let price = $(this).val();
                    $('#lbltotalOngkir').val(price);
                    $('#totalOngkir').val(price);
                    var hrgItem = $('#totalHargaItem').val();
                    let total = parseInt(hrgItem) + parseInt(price);
                    $('#lbltotalOngkir').text(formatRupiah(price));
                    $('#lbltotalKeseluruhan').text(formatRupiah(total));
                    $('#totalKeseluruhan').val(total);
                });

                $('#checkout').on('click', function(){
                    let arr = [];
                    var a = $('input[type="text"][name="qty[]"]').map(function(){
                        var params = $(this).data();
                        var val = $(this).val();
                        if(val > 0){
                            return arr.push({
                                id:params.productid,
                                qty:val,
                                nama_produk:params.product
                            });
                        }
                    }).get();
                    let kota_id = $('select[name="kota_id"] option:selected').text();
                    let province_id = $('select[name="province_id"] option:selected').text();
                    let district_id = $('select[name="distrik_id"] option:selected').text();
                    let kode_pos = $('#kodepos').val();
                    let total_berat = $('input[type="hidden"][name="weight"]').val();

                    let kirim_ke = $('#kirim_ke').val();
                    let nama_lengkap = $('#nama_lengkap').val();
                    let nomor_ponsel = $('#nomor_ponsel').val();
                    let jenis_kelamin = $('#jenis_kelamin').val();
                    // let nama_pengirim = $('#nama_pengirim').val();
                    let alamat = $('#alamat').val();

                    let totalHarga = $('#totalHargaItem').val();
                    let totalOngkir = $('#totalOngkir').val();
                    let totalKeseluruhan = $('#totalKeseluruhan').val();
                    let payment_method = $('input[type="radio"][name="payment_method"]:checked').val();

                    let namaPaketOngkir = "";
                    if($('#layanan').val() !=""){
                        namaPaketOngkir = $('#kurir').val() + ' - ' + $('#layanan option:selected').text().split("-")[0];
                    }
                    Helper.loadingStart()
                    jQuery.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url:"",
                        type:'POST',
                        dataType:'json',
                        data:{
                            kirim_ke:0,
                            kota_id,
                            province_id,
                            district_id,
                            kode_pos,
                            total_berat,
                            nama_lengkap,
                            nomor_ponsel,
                            jenis_kelamin,
                            // nama_pengirim,
                            alamat,
                            totalHarga,
                            totalOngkir,
                            totalKeseluruhan,
                            namaPaketOngkir,
                            payment_method:payment_method,
                            items:arr
                        },
                        success:function(data){
                            Helper.loadingStop()
                            if(payment_method == 0){
                                snap.pay(data.token,{
                                    onSuccess: function(result){
                                        console.log("Sukses");
                                        // storeData(data.trxId);
                                        swal({
                                            title: "Sukses!",
                                            text: "Transaksi anda berhasil, silahkan segera lakukan pembayaran!",
                                            type: 'success'
                                        }, function() {
                                            window.location.href = '/customer/status-order/pembayaran/' + data.orderId;
                                        });
                                    },
                                    onPending: function(result){
                                        console.log("Pending");
                                        // storeData(data.trxId);
                                        swal({
                                            title: "Sukses!",
                                            text: "Transaksi anda berhasil, silahkan segera lakukan pembayaran!",
                                            type: 'success'
                                        }, function() {
                                            window.location.href = '/customer/status-order/pembayaran/' + data.orderId;
                                        });
                                    },
                                    onError: function(result){
                                        swal({
                                            title: "Ups!",
                                            text: result.status_message,
                                            type: 'error'
                                        });
                                    },
                                    onClose: function(){
                                        console.log('customer closed the popup without finishing the payment');
                                    }
                                });
                            }else{
                                swal({
                                    title: "Sukses!",
                                    text: "Transaksi anda berhasil, silahkan segera lakukan pembayaran!",
                                    type: 'success'
                                }, function() {
                                    window.location.href = '/customer/status-order/pembayaran/' + data.orderId;
                                });
                            }
                        },
                        error:function(a,v,c){
                            Helper.loadingStop()
                            var msg = JSON.parse(a.responseText);
                            swal({
                            title: "Ups!",
                                text: msg.message,
                                type: 'error'
                            });
                        }
                    });
                    
                });
            });

            $(".pluss").on("click",function(){
                id = $(this).attr('data-id')
                var now = $(".input-group > .input"+id).val();
                var ttlQty = $(this).attr('data-totalqty')
                if ($.isNumeric(now)){
                    if( (parseInt(now) + 1 ) <= parseInt(ttlQty)){
                        $(".input-group > .input"+id).val(parseInt(now)+1);
                    }
                }else{
                    $(".input-group > input"+id).val(1);
                }
                calculate();
            }) 

            $(".minuss").on("click",function(){
                id = $(this).attr('data-id')
                var now = $(".input-group > .input"+id).val();
                if ($.isNumeric(now)){
                    if(now > 0){
                        $(".input-group > .input"+id).val(parseInt(now) - 1);
                    }
                }else{
                    $(".input-group > input"+id).val(1);
                }
                calculate();
            })  
            function calculate(){
                totalBerat = 0;
                let error = false;
                let msg = '';
                // let val = $(this).val();
                // let data = $(this).data();

                $('input[type="text"][name="qty[]"]').map(function(){
                    let wght = $(this).data();
                    totalBerat += $(this).val() * wght['weight'];
                    if($(this).val() > wght['totalqty']){
                        error = true;
                        msg += "stok " +wght['product'] + "hanya tersedia : " + wght['totalqty'] + "\n";
                    }
                }).get();
                if(error){
                    swal({
                        title: "Ups!",
                        text: msg,
                        type: 'error'
                    });
                }
                $('#lblBerat').text(totalBerat+" Gram");
                $('#berat_paket').val(totalBerat);
                $('#kota_id').html('').append('<option value="">Pilih Kota / Kabupaten</option>');
                $('#distrik_id').html('').append('<option value="">Pilih Kota / Kabupaten Terlebih Dahulu</option>');
                $('#kurir').html('').append('<option value="">Pilih Kecamatan Terlebih Dahulu</option>');
                $('#layanan').html('').append('<option value="">Pilih Kurir Terlebih Dahulu</option>');
                // $('#weight').val(totalBerat);

                let origin = $("input[name=city_origin]").val();
                let destination = $("select[name=kota_id]").val();
                let courier = $("select[name=kurir]").val();
                let layanan = $("select[name=layanan]").val();
                var berat = $('#berat_paket').val();

                if(berat > 0){
                    $('#nama_lengkap').removeAttr("readonly");
                    $('#nomor_ponsel').removeAttr("readonly");
                    $('#jenis_kelamin').removeAttr("readonly");
                    // $('#nama_pengirim').removeAttr("readonly");
                    $('#alamat').removeAttr("readonly");
                    $('.disabled_btn').removeAttr("disabled");
                    $('#accordion_midtrans').removeAttr("disabled");
                    $('#accordion_bank').removeAttr("disabled");

                    $('select[name="province_id"]').removeAttr("readonly");
                    $('select[name="kurir"]').removeAttr("readonly");
                    $('select[name="kota_id"]').removeAttr("readonly");
                    $('select[name="distrik_id"]').removeAttr("readonly");
                    $('select[name="layanan"]').removeAttr("readonly");
                    $('input[name=payment_method]').removeAttr("disabled","true");
                    
                    if(courier && destination && origin){
                        Helper.loadingStart()
                        jQuery.ajax({
                            url:"/customer/input-reguler-member/origin="+origin+"&destination="+destination+"&weight="+getWeight+"&courier="+courier,
                            type:'GET',
                            dataType:'json',
                            success:function(data){
                                Helper.loadingStop()
                            }
                        });
                    }
                }else {
                    $('#nama_lengkap').attr("readonly","true");
                    $('#nomor_ponsel').attr("readonly","true");
                    $('#jenis_kelamin').attr("readonly","true");
                    // $('#nama_pengirim').attr("readonly","true");
                    $('#alamat').attr("readonly","true");
                    $('input[name=payment_method]').attr("disabled","true");

                    $('select[name="province_id"]').attr("readonly","true");
                    $('select[name="kurir"]').attr("readonly","true");
                    $('select[name="kota_id"]').attr("readonly","true");
                    $('select[name="distrik_id"]').attr("readonly", "true");
                    $('select[name="layanan"]').attr("readonly","true");
                    $('#payment_method').attr("readonly","true");
                    $('.disabled_btn').attr("disabled", "true");
                    $('#accordion_midtrans').attr("disabled", "true");
                    $('#accordion_bank').attr("disabled", "true");
                    swal({
                        title: "Ups!",
                        text: 'tidak ada produk yg bisa dikirim.',
                        type: 'error'
                    });
                }
            }

            function cancelItem(id){
                swal({
                    title: "Alert!",
                    text: "Apakah anda yakin akan membatalkan produk tersebut?",
                    type: 'error',
                    showCancelButton: true,
                    confirmButtonText: 'Ya, Saya yakin',
                    cancelButtonText: 'Tidak',
                },function(result) {
                    if(result){
                        document.getElementById('delete-item-'+id).submit();
                    }
                });
            }
        </script>
    @endsection
@endsection