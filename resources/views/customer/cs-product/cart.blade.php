@extends('home')
@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-outline card-warning">
                        <div class="card-header">
                            <h3 class="card-title">
                                Cart
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="col-md-12">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="2">Product</th>
                                            <th>Quantity</th>
                                            <th class="text-center">Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <a class="thumbnail pull-left" href="#">
                                                    <img width="100" height="100" class="img-responsive" src="{{ url('storage/product-image/'.$dataProduct->image_produk) }}">
                                                </a>
                                            </td>
                                            <td>
                                                <h4 class="media-heading"><a href="#">{{$dataProduct->nama_produk}}</a></h4>
                                            </td>
                                            <td>
                                                <input type="number" class="form-control" id="qty" value="1" style="text-align: center; width:10px ">
                                            </td>
                                            <td class="text-center" data-th="price"><strong>Rp. {{ number_format(0) }}</strong></td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="3" class="text-right"><strong>Berat</strong></td>
                                            <td class="text-right"><strong id="lblBerat">{{ number_format($dataProduct->berat) }} Gram</strong></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" class="text-right"><strong>Total</strong></td>
                                            <td class="text-right"><strong>Rp. {{ number_format(0) }}</strong></td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @section('javascript')
        @parent
        <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
        <script>
            $(document).ready(function(){
                $('#qty').on('change', function(){
                    let val = $(this).val();
                    let bb = $('#weight').val();
                    let getWeight = val * bb;
                    $('#lblBerat').text(getWeight+" Gram");

                    let origin = $("input[name=city_origin]").val();
                    let destination = $("select[name=kota_id]").val();
                    let courier = $("select[name=kurir]").val();
                    let layanan = $("select[name=layanan]").val();
                });
            });

            function formatRupiah(bilangan){
                var	number_string = bilangan.toString(),
                    sisa 	= number_string.length % 3,
                    rupiah 	= number_string.substr(0, sisa),
                    ribuan 	= number_string.substr(sisa).match(/\d{3}/g);
                        
                if (ribuan) {
                    separator = sisa ? ',' : '';
                    rupiah += separator + ribuan.join(',');
                }

                return 'Rp. '+ rupiah;
            }

            function convertInteger(id){
                let total = document.getElementById(id).textContent;
                total = total.replace(/ |Rp |\- |\./gi, "")
                total = parseInt(total)
                return total;
            }


        </script>
    @endsection
@endsection