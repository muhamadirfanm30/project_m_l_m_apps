@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="col-md-8">
                                <h3 class="card-title">
                                    Stok Produk
                                </h3>
                            </div>
                            <div class="col-md-4" style="float:right">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="submit" class="btn btn-danger btn-sm btn-block btn_submit" style="float: right; display: none" id="btnCheckout" value="Checkout">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="col-md-12">
                                <form action="{{ route('sent-myproduct') }}" id="formStokProduk" method="post">
                                    @csrf
                                    <table id="ststus-orders" class="display table table-hover table-bordered" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Produk</th>
                                                <th>Stok</th>
                                                <th>Terakhir Update</th>
                                                <!-- <th>Kirim</th> -->
                                            </tr>
                                        </thead>
                                        <tbody>
                                                @foreach($data as $r)
                                                    <tr>
                                                        <td>
                                                            <input type="checkbox" data-checked="{{ $r->id }}" id="" name="qtyCsStock[]" value="{{ $r->product_id }}" onchange="valueChanged()">
                                                        </td>
                                                        <td>{{ $r->product->nama_produk }}</td>
                                                        <td>{{ $r->stok }}</td>
                                                        <td>{{ $r->updated_at }}</td>
                                                    </tr>
                                                @endforeach
                                        </tbody>
                                    </table>
                                </form>

                            </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>
        function valueChanged()
        {
            var itemCheckbox = 0;
            $('input[type="checkbox"][name="qtyCsStock[]"]').map(function(){
                if($(this).is(':checked')){
                    itemCheckbox +=1;
                };
            }).get();
            if(itemCheckbox > 0)   
                $(".btn_submit").show();
            else
                $(".btn_submit").hide();
        }

        $('#btnCheckout').on("click",function(){
            document.getElementById('formStokProduk').submit();
        })

        $(document).ready(function() {
            $('#ststus-orders').DataTable( {
                processing: true,
                responsive: true,
                aaSorting: [[ 3, "desc" ]],
            } );
        } );
    </script>
@endsection