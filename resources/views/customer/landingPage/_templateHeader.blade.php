<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    @php
        $title = DB::table('general_settings')->where('name', 'setting_title_header')->first();
    @endphp

    <title>{{ $title->value }} | {{str_replace("-", " ", Request::segment(2))}}</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="{{ asset('landingPageTemplate/img/favicon.png') }}" rel="icon">
    <link href="{{ asset('landingPageTemplate//img/apple-touch-icon.png') }}" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="{{ asset('landingPageTemplate/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('landingPageTemplate/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="{{ asset('landingPageTemplate/css/style.css') }}" rel="stylesheet">

    <!-- =======================================================
    * Template Name: Siimple - v2.2.2
    * Template URL: https://bootstrapmade.com/free-bootstrap-landing-page/
    * Author: BootstrapMade.com
    * License: https://bootstrapmade.com/license/
    ======================================================== -->
</head>
<style>
  /*
      code by Iatek LLC 2018 - CC 2.0 License - Attribution required
      code customized by Azmind.com
  */
  @media (min-width: 768px) and (max-width: 991px) {
      /* Show 4th slide on md if col-md-4*/
      .carousel-inner .active.col-md-4.carousel-item + .carousel-item + .carousel-item + .carousel-item {
          position: absolute;
          top: 0;
          right: -33.3333%;  /*change this with javascript in the future*/
          z-index: -1;
          display: block;
          visibility: visible;
      }
  }
  @media (min-width: 576px) and (max-width: 768px) {
      /* Show 3rd slide on sm if col-sm-6*/
      .carousel-inner .active.col-sm-6.carousel-item + .carousel-item + .carousel-item {
          position: absolute;
          top: 0;
          right: -50%;  /*change this with javascript in the future*/
          z-index: -1;
          display: block;
          visibility: visible;
      }
  }
  @media (min-width: 576px) {
      .carousel-item {
          margin-right: 0;
      }
      /* show 2 items */
      .carousel-inner .active + .carousel-item {
          display: block;
      }
      .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left),
      .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item {
          transition: none;
      }
      .carousel-inner .carousel-item-next {
          position: relative;
          transform: translate3d(0, 0, 0);
      }
      /* left or forward direction */
      .active.carousel-item-left + .carousel-item-next.carousel-item-left,
      .carousel-item-next.carousel-item-left + .carousel-item,
      .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item {
          position: relative;
          transform: translate3d(-100%, 0, 0);
          visibility: visible;
      }
      /* farthest right hidden item must be also positioned for animations */
      .carousel-inner .carousel-item-prev.carousel-item-right {
          position: absolute;
          top: 0;
          left: 0;
          z-index: -1;
          display: block;
          visibility: visible;
      }
      /* right or prev direction */
      .active.carousel-item-right + .carousel-item-prev.carousel-item-right,
      .carousel-item-prev.carousel-item-right + .carousel-item,
      .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item {
          position: relative;
          transform: translate3d(100%, 0, 0);
          visibility: visible;
          display: block;
          visibility: visible;
      }
  }
  /* MD */
  @media (min-width: 768px) {
      /* show 3rd of 3 item slide */
      .carousel-inner .active + .carousel-item + .carousel-item {
          display: block;
      }
      .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item + .carousel-item {
          transition: none;
      }
      .carousel-inner .carousel-item-next {
          position: relative;
          transform: translate3d(0, 0, 0);
      }
      /* left or forward direction */
      .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item + .carousel-item {
          position: relative;
          transform: translate3d(-100%, 0, 0);
          visibility: visible;
      }
      /* right or prev direction */
      .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item + .carousel-item {
          position: relative;
          transform: translate3d(100%, 0, 0);
          visibility: visible;
          display: block;
          visibility: visible;
      }
  }
  /* LG */
  @media (min-width: 991px) {
      /* show 4th item */
      .carousel-inner .active + .carousel-item + .carousel-item + .carousel-item {
          display: block;
      }
      .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item + .carousel-item + .carousel-item {
          transition: none;
      }
      /* Show 5th slide on lg if col-lg-3 */
      .carousel-inner .active.col-lg-3.carousel-item + .carousel-item + .carousel-item + .carousel-item + .carousel-item {
          position: absolute;
          top: 0;
          right: -25%;  /*change this with javascript in the future*/
          z-index: -1;
          display: block;
          visibility: visible;
      }
      /* left or forward direction */
      .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item + .carousel-item + .carousel-item {
          position: relative;
          transform: translate3d(-100%, 0, 0);
          visibility: visible;
      }
      /* right or prev direction //t - previous slide direction last item animation fix */
      .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item + .carousel-item + .carousel-item {
          position: relative;
          transform: translate3d(100%, 0, 0);
          visibility: visible;
          display: block;
          visibility: visible;
      }
  }

  .carousel-control-prev-icon,
  .carousel-control-next-icon {
  height: 100px;
  width: 100px;
  background-size: 100%, 100%;
  background-image: none;
  }

  .carousel-control-next-icon:after
  {
      content: '>';
      font-size: 55px;
      color: rgb(0, 0, 0);
  }

  .carousel-control-prev-icon:after {
      content: '<';
      font-size: 55px;
      color: rgb(0, 0, 0);
  }
</style>
<style>
     <style>
    /* Profile container */
    .profile {
      margin: 20px 0;
    }
    
    /* Profile sidebar */
    .profile-sidebar {
      background: #fff;
    }
    
    .profile-userpic img {
      float: none;
      margin: 0 auto;
      width: 50%;
      -webkit-border-radius: 50% !important;
      -moz-border-radius: 50% !important;
      border-radius: 50% !important;
    }
    
    .profile-usertitle {
      text-align: center;
      margin-top: 20px;
    }
    
    .profile-usertitle-name {
      color: #5a7391;
      font-size: 16px;
      font-weight: 600;
      margin-bottom: 7px;
    }
    
    .profile-usertitle-job {
      text-transform: uppercase;
      color: #555;
      font-size: 12px;
      font-weight: 600;
      margin-bottom: 15px;
    }
    
    .profile-userbuttons {
      text-align: center;
      margin-top: 10px;
    }
    
    .profile-userbuttons .btn {
      text-transform: uppercase;
      font-size: 11px;
      font-weight: 600;
      padding: 6px 15px;
      margin-right: 5px;
    }
    
    .profile-userbuttons .btn:last-child {
      margin-right: 0px;
    }
    
    .profile-usermenu {
      margin-top: 30px;
    }
    
    /* Profile Content */
    .profile-content {
      padding: 20px;
      background: #fff;
      min-height: 460px;
    }
    
    .avatar-upload {
      position: relative;
      max-width: 205px;
      margin: 0px auto;
    }
    .avatar-upload .avatar-edit {
      position: absolute;
      right: 12px;
      z-index: 1;
      top: 10px;
    }
    .avatar-upload .avatar-edit input {
      display: none;
    }
    .avatar-upload .avatar-edit input + label {
      display: inline-block;
      width: 34px;
      height: 34px;
      margin-bottom: 0;
      border-radius: 100%;
      background: #FFFFFF;
      border: 1px solid transparent;
      box-shadow: 0 5px 5px rgba(0, 0, 0, 0.2);
      cursor: pointer;
      font-weight: normal;
      transition: all 0.2s ease-in-out;
    }
    .avatar-upload .avatar-edit input + label:hover {
      background: #f1f1f1;
      border-color: #d6d6d6;
    }
    .avatar-upload .avatar-edit input + label:after {
      content: "\f040";
      font-family: 'FontAwesome';
      color: #757575;
      position: absolute;
      top: 10px;
      left: 0;
      right: 0;
      text-align: center;
      margin: auto;
    }
    .avatar-upload .avatar-preview {
      width: 70px;
      height: 70px;
      border-radius: 100%;
      border: 6px solid #F8F8F8;
      box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
    }
    .avatar-upload .avatar-preview > div {
      width: 100%;
      height: 100%;
      border-radius: 100%;
      background-size: cover;
      background-repeat: no-repeat;
      background-position: center;
    }
</style>