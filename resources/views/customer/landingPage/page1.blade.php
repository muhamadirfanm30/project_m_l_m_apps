<!DOCTYPE html>
<html lang="en">
    @include('customer.landingPage._templateHeader')
<body>
    <div class="col-md-12">
       <img src="{{url('storage/landing-page-image-1/'.$page1->image_1)}}" alt="Slider" width=100%><br><br>
       <br>
        <div class="container">
            @if (!empty($page1->judul_vidio_embed))
            <div class="section-title">
                <h2>{{!empty($page1->judul_vidio_embed) ? $page1->judul_vidio_embed : ''}}</h2>
            </div>
            @endif
            @if (!empty($page1->embed_link_vidio))
                <center>
                    <div class="embed-responsive embed-responsive-16by9" style="width:70%">
                        <iframe class="embed-responsive-item" src="{{ !empty($page1->embed_link_vidio) ? $page1->embed_link_vidio : ' ' }}" allowfullscreen></iframe>
                    </div>
                </center><hr>
            @endif
            
              @if (!empty($page1->judul_deskripsi))
            <div class="section-title">
                <h2>{{!empty($page1->judul_deskripsi) ? $page1->judul_deskripsi : ' '}}</h2>
            </div>
            @endif
            <p>
                {!! !empty($page1->deskripsi) ? $page1->deskripsi : ' ' !!}
            </p>
        </div>
    </div>
    <div class="col-md-12">
        @if (!empty($page1->image_2))
            <img src="{{url('storage/landing-page-image-2/'.$page1->image_2)}}" alt="" width=100%><hr>
        @endif
        @php
            $message = str_replace('-', ' ', $_GET['text']);
        @endphp
        @if (!empty($page1->nama_button_1))
            <center>
                <a href="https://wa.me/{{ $noWa }}?text={!! strip_tags($message) !!}" target="_blank" class="btn btn-primary btn-lg">{{!empty($page1->nama_button_1) ? $page1->nama_button_1 : ' '}}</a>
                {{-- <a  href="{{url('https://wa.me/'.$noWa.'?text='.str_replace("-"," ",Request::segment(4)))}}" class="btn btn-primary btn-lg">{{!empty($page1->nama_button_1) ? $page1->nama_button_1 : ' '}}</a> --}}
            </center>
        @endif
        
    </div><br>
    <div class="col-md-12">
        @if (!empty($page1->image_3))
            <img src="{{url('storage/landing-page-image-3/'.$page1->image_3)}}" alt="" width=100%><hr>
        @endif
        @if (!empty($page1->nama_button_2))
            <center>
                <a href="https://wa.me/{{ $noWa }}?text={!! strip_tags($message) !!}" target="_blank" class="btn btn-primary btn-lg">{{!empty($page1->nama_button_2) ? $page1->nama_button_2 : ' '}}</a>
                {{-- <a  href="{{url('https://wa.me/'.$noWa.'?text='.str_replace("-"," ",Request::segment(4)))}}" class="btn btn-primary btn-lg">{{!empty($page1->nama_button_2) ? $page1->nama_button_2 : ' '}}</a> --}}
            </center>
        @endif
    </div><br>
    <div class="col-md-12">
        @if (!empty($page1->image_4))
        <img src="{{url('storage/landing-page-image-4/'.$page1->image_4)}}" alt="" width=100%>
        @endif
    </div>
    <div class="col-md-12">
        @if (!empty($page1->image_5))
        <img src="{{url('storage/landing-page-image-5/'.$page1->image_5)}}" alt="" width=100%>
        @endif
    </div><br>
    <div class="col-md-12">
        @if (!empty($page1->nama_button_3))
            <center>
                <a href="https://wa.me/{{ $noWa }}?text={!! strip_tags($message) !!}" target="_blank" class="btn btn-primary btn-lg">{{!empty($page1->nama_button_3) ? $page1->nama_button_3 : ' '}}</a>
                {{-- <a href="{{url('https://wa.me/'.$noWa.'?text='.str_replace("-"," ",Request::segment(4)))}}" class="btn btn-primary btn-lg">{{!empty($page1->nama_button_3) ? $page1->nama_button_3 : ' '}}asd</a> --}}
            </center>
        @endif
    </div><br>
    
    {{-- slide show disini --}}
    {{-- <div class="section-title">
        <h2>{{!empty($page1->judul_1) ? $page1->judul_1 : ' '}}</h2>
    </div> --}}
    {{-- <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="bd-example">
                    <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                            @foreach ($slideShow1 as $item)
                                <li data-target="#carouselExampleCaptions" data-slide-to="{{$item->id}}"></li>
                            @endforeach
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img src="{{ url('storage/landing-page-image-5/' . $page1-5image_1) }}"
                                    class="d-block w-100" alt="...">
                                <div class="carousel-caption d-none d-md-block">
                                    <h5>First slide label</h5>
                                    <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                                </div>
                            </div>
                            @foreach ($slideShow1 as $item)
                                <div class="carousel-item">
                                    <img src="{{ url('storage/img_slide-show/' . $item->images) }}"
                                        class="d-block w-100" alt="...">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5>Second slide label</h5>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button"
                            data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleCaptions" role="button"
                            data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}

    {{-- <div class="container">
        <div class="col-md-12">
            <div class="container">
                <div class="top-content">
                    <div class="container-fluid">
                        <div id="carousel-example" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner row w-100 mx-auto" role="listbox">
                                @if (!empty($page1->image_6))
                                    <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 active">
                                        <img src="{{url('storage/landing-page-image-6/'.$page1->image_6)}}" class="img-fluid mx-auto d-block" alt="img1">
                                    </div>
                                @endif
                                
                                @foreach ($slideShow1 as $item)
                                    <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
                                        <img src="{{ url('storage/img_slide-show/' . $item->images) }}" class="img-fluid mx-auto d-block" alt="img2">
                                    </div>
                                @endforeach
                            </div>
                            <a class="carousel-control-prev" href="#carousel-example" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carousel-example" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><br><br> --}}
@if (!empty($page1->judul_2))
    <section id="why-us" class="why-us section-bg2">
        <div class="container">
             @if (!empty($page1->judul_2))
            <div class="section-title">
                <h2>{{!empty($page1->judul_2) ? $page1->judul_2 : ' '}}</h2>
            </div>
            @endif
            <div class="row">
                <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
                    @if (!empty($page1->url_vidio_user_1) && !empty($page1->nama_user_1) && !empty($page1->deskripsi_user_1))
                        <div class="card" style="width:100%">
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="{{!empty($page1->url_vidio_user_1) ? $page1->url_vidio_user_1 : ' '}}" allowfullscreen></iframe>
                            </div><hr>
                            
                            <div class="card-body">
                                <h5 class="card-title">{{!empty($page1->nama_user_1) ? $page1->nama_user_1 : ' '}}</h5>
                                <p class="card-text"><center>{!!!empty($page1->deskripsi_user_1) ? $page1->deskripsi_user_1 : ' '!!}</center></p>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
                    @if (!empty($page1->url_vidio_user_2) && !empty($page1->nama_user_2) && !empty($page1->deskripsi_user_2))
                        <div class="card" style="width:100%">
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="{{!empty($page1->url_vidio_user_2) ? $page1->url_vidio_user_2 : ' '}}" allowfullscreen></iframe>
                            </div><hr>
                            
                            <div class="card-body">
                                <h5 class="card-title"><span >{{!empty($page1->nama_user_2) ? $page1->nama_user_2 : ' '}}</span></h5>
                                <p class="card-text"><center>{!!!empty($page1->deskripsi_user_2) ? $page1->deskripsi_user_2 : ' '!!}</center></p>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
                    @if (!empty($page1->url_vidio_user_1) && !empty($page1->nama_user_1) && !empty($page1->deskripsi_user_1))
                        <div class="card" style="width:100%">
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="{{!empty($page1->url_vidio_user_3) ? $page1->url_vidio_user_3 : ' '}}" allowfullscreen></iframe>
                            </div><hr>
                           
                            <div class="card-body">
                                <h5 class="card-title">{{!empty($page1->nama_user_3) ? $page1->nama_user_3 : ' '}}</h5>
                                <p class="card-text"><center>{!!!empty($page1->deskripsi_user_3) ? $page1->deskripsi_user_3 : ' '!!}</center></p>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
            
        </div>
    </section><!-- End Why Us Section -->
     @endif
     
     @if (!empty($page1->nama_button_4))
                <center>
                    <a href="https://wa.me/{{ $noWa }}?text={{ strip_tags($message) }}" target="_blank" class="btn btn-primary btn-lg">{{!empty($page1->nama_button_4) ? $page1->nama_button_4 : ' '}}</a>
                    {{-- <a  href="{{'https://wa.me/'.$noWa.'?text=asdasd'}}" class="btn btn-primary btn-lg">{{!empty($page1->nama_button_4) ? $page1->nama_button_4 : ' '}}</a> --}}
                </center>
            @endif
     
    @if (!empty($page1->image_6))
    <div class="col-md-12">
        <img src="{{url('storage/landing-page-image-6/'.$page1->image_6)}}" alt="" width=100%>
    </div>
    @endif
    @if (!empty($page1->image_7))
    <div class="col-md-12">
        <img src="{{url('storage/landing-page-image-7/'.$page1->image_7)}}" alt="" width=100%>
    </div><hr>
    @endif
    @if (!empty($page1->image_8)))
    <div class="col-md-12">
        <img src="{{url('storage/landing-page-image-8/'.$page1->image_8)}}" alt="" width=100%>
    </div><hr>
    @endif
    @if (!empty($page1->nama_button_5))
        <center>
            <a  href="{{url('https://wa.me/'.$noWa.'?text='.str_replace("-"," ",Request::segment(4)))}}" class="btn btn-primary btn-lg">{{!empty($page1->nama_button_5) ? $page1->nama_button_5 : ' '}}</a>
        </center>
    @endif
    
    

     <!-- ======= Frequenty Asked Questions Section ======= -->
     <section class="faq">
        <div class="container">
             @if (!empty($page1->judul_3))
            <div class="section-title">
                <h2>{{!empty($page1->judul_3) ? $page1->judul_3 : ' '}}</h2>
            </div>
            @endif
            <ul class="faq-list">
                @if (!empty($page1->judul_accordion_1) && !empty($page1->deskripsi_accordion_1))
                <li>
                    <a data-toggle="collapse" class="collapsed" href="#faq1">{{!empty($page1->judul_accordion_1) ? $page1->judul_accordion_1 : ' '}}<i class="bx bx-down-arrow-alt icon-show"></i><i class="bx bx-x icon-close"></i></a>
                    <div id="faq1" class="collapse" data-parent=".faq-list">
                        <p>
                            {!!!empty($page1->deskripsi_accordion_1) ? $page1->deskripsi_accordion_1 : ' '!!}
                        </p>
                    </div>
                </li>
                @endif
                @if (!empty($page1->judul_accordion_2) && !empty($page1->deskripsi_accordion_2))
                <li>
                    <a data-toggle="collapse" href="#faq2" class="collapsed">{{!empty($page1->judul_accordion_2) ? $page1->judul_accordion_2 : ' '}}<i class="bx bx-down-arrow-alt icon-show"></i><i class="bx bx-x icon-close"></i></a>
                    <div id="faq2" class="collapse" data-parent=".faq-list">
                        <p>
                            {!!!empty($page1->deskripsi_accordion_2) ? $page1->deskripsi_accordion_2 : ' '!!}
                        </p>
                    </div>
                </li>
                @endif
                @if (!empty($page1->judul_accordion_3) && !empty($page1->deskripsi_accordion_3))
                <li>
                    <a data-toggle="collapse" href="#faq3" class="collapsed">{{!empty($page1->judul_accordion_3) ? $page1->judul_accordion_3 : ' '}}<i class="bx bx-down-arrow-alt icon-show"></i><i class="bx bx-x icon-close"></i></a>
                    <div id="faq3" class="collapse" data-parent=".faq-list">
                        <p>
                            {!!!empty($page1->deskripsi_accordion_3) ? $page1->deskripsi_accordion_3 : ' '!!}
                        </p>
                    </div>
                </li>
                @endif
                @if (!empty($page1->judul_accordion_4) && !empty($page1->deskripsi_accordion_4))
                <li>
                    <a data-toggle="collapse" href="#faq4" class="collapsed">{{!empty($page1->judul_accordion_4) ? $page1->judul_accordion_4 : ' '}}<i class="bx bx-down-arrow-alt icon-show"></i><i class="bx bx-x icon-close"></i></a>
                    <div id="faq4" class="collapse" data-parent=".faq-list">
                        <p>
                            {!!!empty($page1->deskripsi_accordion_4) ? $page1->deskripsi_accordion_4 : ' '!!}
                        </p>
                    </div>
                </li>
                @endif
                
                
                
                
            </ul>
        </div>
    </section><!-- End Frequenty Asked Questions Section -->
    <!-- ======= Footer ======= -->
    @php
        $title = DB::table('general_settings')->where('name', 'setting_title_header')->first();
    @endphp
    <footer id="footer">
        <div class="container">
            <div class="copyright">
                &copy; Copyright <strong><span>{{date('Y')}}</span></strong>. {{$title->value}}
            </div>
            <div class="credits">
            </div>
        </div>
    </footer><!-- End #footer -->
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
   

    </script>
    @include('customer.landingPage._templateScript')
</body>

</html>