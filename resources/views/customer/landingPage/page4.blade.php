<!DOCTYPE html>
<html lang="en">
    @include('customer.landingPage._templateHeader')
<body>
    @php
        $message = str_replace('-', ' ', html_entity_decode($_GET['text']));
    @endphp
    <br>
    <br>
    @if (!empty($page4->judul_vidio_embed))
        <div class="col-md-12">
            <div class="container">
                <div class="section-title">
                    <h2>{{!empty($page4->judul_vidio_embed) ? $page4->judul_vidio_embed : ''}}</h2>
                </div>
                @if (!empty($page4->judul_vidio_embed))
                    <center>
                        <div class="embed-responsive embed-responsive-16by9" style="width:70%">
                            <iframe class="embed-responsive-item" src="{{ !empty($page4->embed_link_vidio) ? $page4->embed_link_vidio : ' ' }}" allowfullscreen></iframe>
                        </div>
                    </center><hr>
               @endif

            </div>
        </div>
      @endif

    @if (!empty($page4->image_1))
    <div class="col-md-12">
        <img src="{{url('storage/landing-page-image-1/'.$page4->image_1)}}" alt="" width=100%>
    </div>
    @endif
    @if (!empty($page4->image_2))
    <div class="col-md-12">
        <img src="{{url('storage/landing-page-image-2/'.$page4->image_2)}}" alt="" width=100%>
    </div>
    @endif
    @if (!empty($page4->image_3))
    <div class="col-md-12">
        <img src="{{url('storage/landing-page-image-3/'.$page4->image_3)}}" alt="" width=100%>
    </div><hr>
    @endif
    
    @if (!empty($page4->nama_button_1))
    <center>
        <a href="https://wa.me/{{ $noWa }}?text={!! strip_tags($message) !!}" class="btn btn-primary btn-lg">{{!empty($page4->nama_button_1) ? $page4->nama_button_1 : ' '}}</a>
    </center><br>
    @endif
   
    @if (!empty($page4->image_4))
    <div class="col-md-12">
        <img src="{{url('storage/landing-page-image-4/'.$page4->image_4)}}" alt="" width=100%>
    </div>
    @endif
    @if (!empty($page4->image_5))
    <div class="col-md-12">
        <img src="{{url('storage/landing-page-image-5/'.$page4->image_5)}}" alt="" width=100%>
    </div><hr>
    @endif
    
    
@if (!empty($page4->url_vidio_user_1) && !empty($page4->nama_user_1) && !empty($page4->deskripsi_user_1))
    <section id="why-us" class="why-us section-bgj">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
                    <div class="card" style="width:100%">
                        @if (!empty($page4->url_vidio_user_1) && !empty($page4->nama_user_1) && !empty($page4->deskripsi_user_1))
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="{{!empty($page4->url_vidio_user_1) ? $page4->url_vidio_user_1 : ' '}}" allowfullscreen></iframe>
                            </div><hr>
                           
                            <div class="card-body">
                                <h5 class="card-title">{{!empty($page4->nama_user_1) ? $page4->nama_user_1 : ' '}}</h5>
                                <p class="card-text"><center>{!!!empty($page4->deskripsi_user_1) ? $page4->deskripsi_user_1 : ' '!!}</center></p>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
                    <div class="card" style="width:100%">
                        @if (!empty($page4->url_vidio_user_2) && !empty($page4->nama_user_2) && !empty($page4->deskripsi_user_2))
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="{{!empty($page4->url_vidio_user_2) ? $page4->url_vidio_user_2 : ' '}}" allowfullscreen></iframe>
                            </div><hr>
                            
                            <div class="card-body">
                                <h5 class="card-title">{{!empty($page4->nama_user_2) ? $page4->nama_user_2 : ' '}}</h5>
                                <p class="card-text"><center>{!!!empty($page4->deskripsi_user_2) ? $page4->deskripsi_user_2 : ' '!!}</center></p>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
                    <div class="card" style="width:100%">
                        @if (!empty($page4->url_vidio_user_3) && !empty($page4->nama_user_3) && !empty($page4->deskripsi_user_3))
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="{{!empty($page4->url_vidio_user_3) ? $page4->url_vidio_user_3 : ' '}}" allowfullscreen></iframe>
                            </div><hr>
                            
                            <div class="card-body">
                                <h5 class="card-title">{{!empty($page4->nama_user_3) ? $page4->nama_user_3 : ' '}}</h5>
                                <p class="card-text"><center>{!!!empty($page4->deskripsi_user_3) ? $page4->deskripsi_user_3 : ' '!!}</center></p>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            
        </div>
        
    </section><!-- End Why Us Section -->
       @endif
       @if (!empty($page4->nama_button_2))
            <hr>
            <center>
                <a href="https://wa.me/{{ $noWa }}?text={!! strip_tags($message) !!}" class="btn btn-primary btn-lg">{{!empty($page4->nama_button_2) ? $page4->nama_button_2 : ' '}}</a>
            </center>
            @endif
    <br>
    @if (!empty($page4->image_6))
    <div class="col-md-12">
        <img src="{{url('storage/landing-page-image-6/'.$page4->image_6)}}" alt="" width=100%>
    </div>
    @endif
    @if (!empty($page4->image_7))
    <div class="col-md-12">
        <img src="{{url('storage/landing-page-image-7/'.$page4->image_7)}}" alt="" width=100%>
    </div>
    @endif
    @if (!empty($page4->image_8))
    <div class="col-md-12">
        <img src="{{url('storage/landing-page-image-8/'.$page4->image_8)}}" alt="" width=100%>
    </div><hr>
    @endif
    @if (!empty($page4->nama_button_3))
    <center>
        <a href="https://wa.me/{{ $noWa }}?text={!! strip_tags($message) !!}" class="btn btn-primary btn-lg">{{!empty($page4->nama_button_3) ? $page4->nama_button_3 : ' '}}</a>
    </center><br>
    @endif
    @if (!empty($page4->image_9))
    <div class="col-md-12">
        <img src="{{url('storage/landing-page-image-9/'.$page4->image_9)}}" alt="" width=100%>
    </div>
    @endif
    @if (!empty($page4->image_10))
    <div class="col-md-12">
        <img src="{{url('storage/landing-page-image-10/'.$page4->image_10)}}" alt="" width=100%>
    </div><hr>
    @endif
    
@if (!empty($page4->url_vidio_user_4) && !empty($page4->nama_user_4) && !empty($page4->deskripsi_user_4))
    <section id="why-us" class="why-us section-bg">
        <div class="container">
            @if (!empty($page4->judul_1))
                <div class="section-title">
                    <h2>{{!empty($page4->judul_1) ? $page4->judul_1 : ' '}}</h2>
                </div>
            @endif
            <div class="row">
                <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
                    <div class="card" style="width:100%">
                        @if (!empty($page4->url_vidio_user_4) && !empty($page4->nama_user_4) && !empty($page4->deskripsi_user_4))
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="{{!empty($page4->url_vidio_user_4) ? $page4->url_vidio_user_4 : ' '}}" allowfullscreen></iframe>
                            </div><hr>
                            
                            <div class="card-body">
                                <h5 class="card-title">{{!empty($page4->nama_user_4) ? $page4->nama_user_4 : ' '}}</h5>
                                <p class="card-text"><center>{!!!empty($page4->deskripsi_user_4) ? $page4->deskripsi_user_4 : ' '!!}</center></p>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
                    <div class="card" style="width:100%">
                        @if (!empty($page4->url_vidio_user_5) && !empty($page4->nama_user_5) && !empty($page4->deskripsi_user_5))
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="{{!empty($page4->url_vidio_user_5) ? $page4->url_vidio_user_5 : ' '}}" allowfullscreen></iframe>
                            </div><hr>
                           
                            <div class="card-body">
                                <h5 class="card-title">{{!empty($page4->nama_user_5) ? $page4->nama_user_5 : ' '}}</h5>
                                <p class="card-text"><center>{!!!empty($page4->deskripsi_user_5) ? $page4->deskripsi_user_5 : ' '!!}</center></p>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
                    <div class="card" style="width:100%">
                        @if (!empty($page4->url_vidio_user_6) && !empty($page4->nama_user_6) && !empty($page4->deskripsi_user_6))
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="{{!empty($page4->url_vidio_user_6) ? $page4->url_vidio_user_6 : ' '}}" allowfullscreen></iframe>
                            </div><hr>
                          
                            <div class="card-body">
                                <h5 class="card-title">{{!empty($page4->nama_user_6) ? $page4->nama_user_6 : ' '}}</h5>
                                <p class="card-text"><center>{!!!empty($page4->deskripsi_user_6) ? $page4->deskripsi_user_6 : ' '!!}</center></p>
                            </div>
                        @endif
                    </div>
                </div>
            </div><hr>
           </div> 
    </section><!-- End Why Us Section -->
     @endif
        @if (!empty($page4->image_11))
            <div class="col-md-12">
                <img src="{{url('storage/landing-page-image-10/'.$page4->image_11)}}" alt="" width=100%>
                </div><hr>
                <center>
                    <a href="https://wa.me/{{ $noWa }}?text={!! strip_tags($message) !!}" class="btn btn-primary btn-lg">{{!empty($page4->nama_button_4) ? $page4->nama_button_4 : ' '}}</a>
                </center>
            </div>
        @endif

    <!-- ======= Frequenty Asked Questions Section ======= -->
    <section class="faq">
        <div class="container">
            @if (!empty($page4->judul_3))
                <div class="section-title">
                    <h2>{{!empty($page4->judul_3) ? $page4->judul_3 : ' '}}</h2>
                </div>
            @endif
            
            <ul class="faq-list">
                @if (!empty($page4->judul_accordion_1) && !empty($page4->deskripsi_accordion_1))
                    <li>
                        <a data-toggle="collapse" class="collapsed" href="#faq1">{{!empty($page4->judul_accordion_1) ? $page4->judul_accordion_1 : ' '}}<i class="bx bx-down-arrow-alt icon-show"></i><i class="bx bx-x icon-close"></i></a>
                        <div id="faq1" class="collapse" data-parent=".faq-list">
                            <p>
                                {!!!empty($page4->deskripsi_accordion_1) ? $page4->deskripsi_accordion_1 : ' '!!}
                            </p>
                        </div>
                    </li>
                @endif
                @if (!empty($page4->judul_accordion_2) && !empty($page4->deskripsi_accordion_2))
                    <li>
                        <a data-toggle="collapse" href="#faq2" class="collapsed">{{!empty($page4->judul_accordion_2) ? $page4->judul_accordion_2 : ' '}}<i class="bx bx-down-arrow-alt icon-show"></i><i class="bx bx-x icon-close"></i></a>
                        <div id="faq2" class="collapse" data-parent=".faq-list">
                            <p>
                                {!!!empty($page4->deskripsi_accordion_2) ? $page4->deskripsi_accordion_2 : ' '!!}
                            </p>
                        </div>
                    </li>
                @endif
                @if (!empty($page4->judul_accordion_3) && !empty($page4->deskripsi_accordion_3))
                <li>
                    <a data-toggle="collapse" href="#faq3" class="collapsed">{{!empty($page4->judul_accordion_3) ? $page4->judul_accordion_3 : ' '}}<i class="bx bx-down-arrow-alt icon-show"></i><i class="bx bx-x icon-close"></i></a>
                    <div id="faq3" class="collapse" data-parent=".faq-list">
                        <p>
                            {!!!empty($page4->deskripsi_accordion_3) ? $page4->deskripsi_accordion_3 : ' '!!}
                        </p>
                    </div>
                </li>
                @endif
                @if (!empty($page4->judul_accordion_4) && !empty($page4->deskripsi_accordion_4))
                <li>
                    <a data-toggle="collapse" href="#faq4" class="collapsed">{{!empty($page4->judul_accordion_4) ? $page4->judul_accordion_4 : ' '}}<i class="bx bx-down-arrow-alt icon-show"></i><i class="bx bx-x icon-close"></i></a>
                    <div id="faq4" class="collapse" data-parent=".faq-list">
                        <p>
                            {!!!empty($page4->deskripsi_accordion_4) ? $page4->deskripsi_accordion_4 : ' '!!}
                        </p>
                    </div>
                </li>
                @endif
                
                
                
                
            </ul>
        </div>
    </section><!-- End Frequenty Asked Questions Section -->

    <!-- ======= Footer ======= -->
    @php
        $title = DB::table('general_settings')->where('name', 'setting_title_header')->first();
    @endphp
    <footer id="footer">
        <div class="container">
            <div class="copyright">
                &copy; Copyright <strong><span>{{date('Y')}}</span></strong>. {{$title->value}}
            </div>
            <div class="credits">
            </div>
        </div>
    </footer><!-- End #footer -->
    @include('customer.landingPage._templateScript')
</body>

</html>