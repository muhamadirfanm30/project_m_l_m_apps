<!DOCTYPE html>
<html lang="en">
    @include('customer.landingPage._templateHeader')
<body>
    @php
        $message = str_replace('-', ' ', html_entity_decode($_GET['text']));
    @endphp
    <br>
    <br>
     @if (!empty($page3->embed_link_vidio))
    <div class="col-md-12">
        <div class="container">
            <div class="section-title">
                <h2>{{!empty($page3->judul_vidio_embed) ? $page3->judul_vidio_embed : ''}}</h2>
            </div>
            @if (!empty($page3->embed_link_vidio))
            <center>
                <div class="embed-responsive embed-responsive-16by9" style="width:70%">
                    <iframe class="embed-responsive-item" src="{{ !empty($page3->embed_link_vidio) ? $page3->embed_link_vidio : ' ' }}" allowfullscreen></iframe>
                </div>
            </center><hr>
            @endif
            
        </div>
    </div>
     @endif
    <div class="col-md-12">
        <img src="{{url('storage/landing-page-image-1/'.$page3->image_1)}}" alt="" width=100%>
    </div>
    <div class="col-md-12">
        <img src="{{url('storage/landing-page-image-2/'.$page3->image_2)}}" alt="" width=100%>
    </div>
    <div class="col-md-12">
        <img src="{{url('storage/landing-page-image-3/'.$page3->image_3)}}" alt="" width=100%>
    </div>
    @if (!empty($page3->nama_button_1))
    <hr>
    <center>
        <a href="https://wa.me/{{ $noWa }}?text={!! strip_tags($message) !!}" class="btn btn-primary btn-lg">{{!empty($page3->nama_button_1) ? $page3->nama_button_1 : ' '}}</a>
    </center><br>
    @endif
    
    <div class="col-md-12">
        <img src="{{url('storage/landing-page-image-4/'.$page3->image_4)}}" alt="" width=100%>
    </div>
    <div class="col-md-12">
        <img src="{{url('storage/landing-page-image-5/'.$page3->image_5)}}" alt="" width=100%>
    </div>
@if (!empty($page3->url_vidio_user_1) && !empty($page3->nama_user_1) && !empty($page3->deskripsi_user_1))
<hr>
    <section id="why-us" class="why-us section-bg2">
        <div class="container">
            @if (!empty($page3->judul_1))
            <div class="section-title">
                <h2>{{!empty($page3->judul_1) ? $page3->judul_1 : ' '}}</h2>
            </div>
             @endif
            <div class="row">
                <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
                    @if (!empty($page3->url_vidio_user_1) && !empty($page3->nama_user_1) && !empty($page3->deskripsi_user_1))
                    <div class="card" style="width:100%">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="{{!empty($page3->url_vidio_user_1) ? $page3->url_vidio_user_1 : ' '}}" allowfullscreen></iframe>
                        </div><hr>
                       
                        <div class="card-body">
                            <h5 class="card-title">{{!empty($page3->nama_user_1) ? $page3->nama_user_1 : ' '}}</h5>
                            <p class="card-text"><center>{!!!empty($page3->deskripsi_user_1) ? $page3->deskripsi_user_1 : ' '!!}</center></p>
                        </div>
                    </div>
                    @endif
                    
                </div>
                <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
                    @if (!empty($page3->url_vidio_user_2) && !empty($page3->nama_user_2) && !empty($page3->deskripsi_user_2))
                    <div class="card" style="width:100%">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="{{!empty($page3->url_vidio_user_2) ? $page3->url_vidio_user_2 : ' '}}" allowfullscreen></iframe>
                        </div><hr>
                       
                        <div class="card-body">
                            <h5 class="card-title">{{!empty($page3->nama_user_2) ? $page3->nama_user_2 : ' '}}</h5>
                            <p class="card-text"><center>{!!!empty($page3->deskripsi_user_2) ? $page3->deskripsi_user_2 : ' '!!}</center></p>
                        </div>
                    </div>
                    @endif
                </div>
                <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
                    @if (!empty($page3->url_vidio_user_3) && !empty($page3->nama_user_3) && !empty($page3->deskripsi_user_3))
                    <div class="card" style="width:100%">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="{{!empty($page3->url_vidio_user_3) ? $page3->url_vidio_user_3 : ' '}}" allowfullscreen></iframe>
                        </div><hr>
                        
                        <div class="card-body">
                            <h5 class="card-title">{{!empty($page3->nama_user_3) ? $page3->nama_user_3 : ' '}}</h5>
                            <p class="card-text"><center>{!!!empty($page3->deskripsi_user_3) ? $page3->deskripsi_user_3 : ' '!!}</center></p>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
            @if (!empty($page3->nama_button_3))
            <center>
                <a href="https://wa.me/{{ $noWa }}?text={!! strip_tags($message) !!}" class="btn btn-primary btn-lg">{{!empty($page3->nama_button_3) ? $page3->nama_button_3 : ' '}}</a>
            </center>
            @endif
            
        </div>
    </section><!-- End Why Us Section -->
     @endif
    <br>

    <div class="col-md-12">
        <img src="{{url('storage/landing-page-image-6/'.$page3->image_6)}}" alt="" width=100%>
    </div>
    <div class="col-md-12">
        <img src="{{url('storage/landing-page-image-7/'.$page3->image_7)}}" alt="" width=100%>
    </div>
    <div class="col-md-12">
        <img src="{{url('storage/landing-page-image-8/'.$page3->image_8)}}" alt="" width=100%>
    </div>
    @if (!empty($page3->nama_button_2))
    <hr>
    <center>
        <a href="https://wa.me/{{ $noWa }}?text={!! strip_tags($message) !!}" class="btn btn-primary btn-lg">{{!empty($page3->nama_button_2) ? $page3->nama_button_2 : ' '}}</a>
    </center><br>
    @endif
    
    <div class="col-md-12">
        <img src="{{url('storage/landing-page-image-9/'.$page3->image_9)}}" alt="" width=100%>
    </div>
    <div class="col-md-12">
        <img src="{{url('storage/landing-page-image-10/'.$page3->image_10)}}" alt="" width=100%>
    </div>
    @if (!empty($page3->nama_button_3))
    <hr>
    <center>
        <a href="https://wa.me/{{ $noWa }}?text={!! strip_tags($message) !!}" class="btn btn-primary btn-lg">{{!empty($page3->nama_button_3) ? $page3->nama_button_3 : ' '}}</a>
    </center><br>
    @endif
  

    <!-- ======= Frequenty Asked Questions Section ======= -->
    @if (!empty($page3->judul_1))
    <section class="faq">
        <div class="container">
             
            <ul class="faq-list">
                @if (!empty($page3->judul_accordion_1) && !empty($page3->deskripsi_accordion_1))
                <li>
                    <a data-toggle="collapse" class="collapsed" href="#faq1">{{!empty($page3->judul_accordion_1) ? $page3->judul_accordion_1 : ' '}}<i class="bx bx-down-arrow-alt icon-show"></i><i class="bx bx-x icon-close"></i></a>
                    <div id="faq1" class="collapse" data-parent=".faq-list">
                        <p>
                            {!!!empty($page3->deskripsi_accordion_1) ? $page3->deskripsi_accordion_1 : ' '!!}
                        </p>
                    </div>
                </li>
                @endif
                @if (!empty($page3->judul_accordion_2) && !empty($page3->deskripsi_accordion_2))
                <li>
                    <a data-toggle="collapse" href="#faq2" class="collapsed">{{!empty($page3->judul_accordion_2) ? $page3->judul_accordion_2 : ' '}}<i class="bx bx-down-arrow-alt icon-show"></i><i class="bx bx-x icon-close"></i></a>
                    <div id="faq2" class="collapse" data-parent=".faq-list">
                        <p>
                            {!!!empty($page3->deskripsi_accordion_2) ? $page3->deskripsi_accordion_2 : ' '!!}
                        </p>
                    </div>
                </li>
                @endif
                @if (!empty($page3->judul_accordion_3) && !empty($page3->deskripsi_accordion_3))
                <li>
                    <a data-toggle="collapse" href="#faq3" class="collapsed">{{!empty($page3->judul_accordion_3) ? $page3->judul_accordion_3 : ' '}}<i class="bx bx-down-arrow-alt icon-show"></i><i class="bx bx-x icon-close"></i></a>
                    <div id="faq3" class="collapse" data-parent=".faq-list">
                        <p>
                            {!!!empty($page3->deskripsi_accordion_3) ? $page3->deskripsi_accordion_3 : ' '!!}
                        </p>
                    </div>
                </li>
                @endif
                @if (!empty($page3->judul_accordion_4) && !empty($page3->deskripsi_accordion_4))
                <li>
                    <a data-toggle="collapse" href="#faq4" class="collapsed">{{!empty($page3->judul_accordion_4) ? $page3->judul_accordion_4 : ' '}}<i class="bx bx-down-arrow-alt icon-show"></i><i class="bx bx-x icon-close"></i></a>
                    <div id="faq4" class="collapse" data-parent=".faq-list">
                        <p>
                            {!!!empty($page3->deskripsi_accordion_4) ? $page3->deskripsi_accordion_4 : ' '!!}
                        </p>
                    </div>
                </li>
                @endif
            </ul>
        </div>
    </section><!-- End Frequenty Asked Questions Section -->
  @endif
    <!-- ======= Footer ======= -->
    @php
        $title = DB::table('general_settings')->where('name', 'setting_title_header')->first();
    @endphp
    <footer id="footer">
        <div class="container">
            <div class="copyright">
                &copy; Copyright <strong><span>{{date('Y')}}</span></strong>. {{$title->value}}
            </div>
            <div class="credits">
            </div>
        </div>
    </footer><!-- End #footer -->
    @include('customer.landingPage._templateScript')
</body>

</html>