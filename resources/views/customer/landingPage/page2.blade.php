<!DOCTYPE html>
<html lang="en">
    @include('customer.landingPage._templateHeader')
<body>
    @php
        $message = str_replace('-', ' ', html_entity_decode($_GET['text']));
    @endphp
    <div class="col-md-12">
        <img src="{{url('storage/landing-page-image-1/'.$page2->image_1)}}" alt="" width=100%>
        <br>
         @if (!empty($page2->judul_vidio_embed))
        <div class="container">
            <div class="section-title">
                <h2>{{!empty($page2->judul_vidio_embed) ? $page2->judul_vidio_embed : ''}}</h2>
            </div>
            @if (!empty($page2->embed_link_vidio))
                <center>
                    <div class="embed-responsive embed-responsive-16by9" style="width:70%">
                        <iframe class="embed-responsive-item" src="{{ !empty($page2->embed_link_vidio) ? $page2->embed_link_vidio : ' ' }}" allowfullscreen></iframe>
                    </div>
                </center><hr>
            @endif
        </div>
          @endif
    </div>
    <div class="col-md-12">
        <img src="{{url('storage/landing-page-image-2/'.$page2->image_2)}}" alt="" width=100%>
    </div>
    <div class="col-md-12">
        <img src="{{url('storage/landing-page-image-3/'.$page2->image_3)}}" alt="" width=100%>
        @if (!empty($page2->nama_button_1))
        <hr>
        <center>
            <a href="https://wa.me/{{ $noWa }}?text={!! strip_tags($message) !!}" class="btn btn-primary btn-lg">{{!empty($page2->nama_button_1) ? $page2->nama_button_1 : ' '}}</a>
        </center>
        @endif
        
    </div><br>
    <div class="col-md-12">
        <img src="{{url('storage/landing-page-image-4/'.$page2->image_4)}}" alt="" width=100%>
    </div>
    <div class="col-md-12">
        <img src="{{url('storage/landing-page-image-5/'.$page2->image_5)}}" alt="" width=100%>
    </div>
    <div class="col-md-12">
        <img src="{{url('storage/landing-page-image-6/'.$page2->image_6)}}" alt="" width=100%>
        @if (!empty($page2->nama_button_2))
        <hr>
        <center>
            <a href="https://wa.me/{{ $noWa }}?text={!! strip_tags($message) !!}" class="btn btn-primary btn-lg">{{!empty($page2->nama_button_2) ? $page2->nama_button_2 : ' '}}</a>
        </center>
        @endif
        
    </div><br>

    {{-- <div class="section-title">
        <h2>{{!empty($page2->judul_1) ? $page2->judul_1 : ' '}}</h2>
    </div>
    <div class="container">
        <div class="col-md-12">
            <div class="container">
                @if (!empty($slideShow2))
                    <div class="top-content">
                        <div class="container-fluid">
                            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner row w-100 mx-auto" role="listbox">
                                    <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 active">
                                        <img src="{{url('storage/landing-page-image-1/'.$page2->image_1)}}" class="img-fluid mx-auto d-block" alt="img1">
                                    </div>
                                    @foreach ($slideShow2 as $item)
                                        <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
                                            <img src="{{ url('storage/img_slide-show/' . $item->images) }}" class="img-fluid mx-auto d-block" alt="img2">
                                        </div>
                                    @endforeach
                                    
                                    
                                </div>
                                <a class="carousel-control-prev" href="#carousel-example" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carousel-example" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div><br><br> --}}
@if (!empty($page2->judul_2))
    <section id="why-us" class="why-us section-bg2">
        <div class="container">
             @if (!empty($page2->judul_2))
            <div class="section-title">
                <h2>{{!empty($page2->judul_2) ? $page2->judul_2 : ' '}}</h2>
            </div>
              @endif
            <div class="row">
                <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
                    @if (!empty($page2->url_vidio_user_1) && !empty($page2->nama_user_1) && !empty($page2->deskripsi_user_1))
                        <div class="card" style="width:100%">
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="{{!empty($page2->url_vidio_user_1) ? $page2->url_vidio_user_1 : ' '}}" allowfullscreen></iframe>
                            </div><hr>
                            <div class="card-icon">
                                <div class="profile-userpic text-center">
                                    <div class="avatar-upload">
                                        <center>
                                            <div class="avatar-preview">
                                                <div id="imagePreview" style="background-image: url( {{url('storage/avatar/default_avatar.jpg')}} )"></div>
                                            </div>
                                        </center>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <h5 class="card-title">{{!empty($page2->nama_user_1) ? $page2->nama_user_1 : ' '}}</h5>
                                <p class="card-text"><center>{!!!empty($page2->deskripsi_user_1) ? $page2->deskripsi_user_1 : ' '!!}</center></p>
                            </div>
                        </div>
                    @endif
                    
                </div>
                <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
                    @if (!empty($page2->url_vidio_user_2) && !empty($page2->nama_user_2) && !empty($page2->deskripsi_user_2))
                        <div class="card" style="width:100%">
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="{{!empty($page2->url_vidio_user_2) ? $page2->url_vidio_user_2 : ' '}}" allowfullscreen></iframe>
                            </div><hr>
                            <div class="card-icon">
                                <div class="profile-userpic text-center">
                                    <div class="avatar-upload">
                                        <center>
                                            <div class="avatar-preview">
                                                <div id="imagePreview" style="background-image: url( {{url('storage/avatar/default_avatar.jpg')}} )"></div>
                                            </div>
                                        </center>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <h5 class="card-title">{{!empty($page2->nama_user_2) ? $page2->nama_user_2 : ' '}}</h5>
                                <p class="card-text"><center>{!!!empty($page2->deskripsi_user_2) ? $page2->deskripsi_user_2 : ' '!!}</center></p>
                                {{-- <p class="card-text"><center>Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta exercitationem adipisci sint porro alias architecto ipsam. Rerum saepe deleniti earum dolorum unde excepturi beatae, consequatur modi perferendis soluta mollitia amet.</center></p> --}}
                            </div>
                        </div>
                    @endif
                </div>
                <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
                    @if (!empty($page2->url_vidio_user_3) && !empty($page2->nama_user_3) && !empty($page2->deskripsi_user_3))
                        <div class="card" style="width:100%">
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="{{!empty($page2->url_vidio_user_3) ? $page2->url_vidio_user_3 : ' '}}" allowfullscreen></iframe>
                            </div><hr>
                            <div class="card-icon">
                                <div class="profile-userpic text-center">
                                    <div class="avatar-upload">
                                        <center>
                                            <div class="avatar-preview">
                                                <div id="imagePreview" style="background-image: url( {{url('storage/avatar/default_avatar.jpg')}} )"></div>
                                            </div>
                                        </center>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <h5 class="card-title">{{!empty($page2->nama_user_3) ? $page2->nama_user_3 : ' '}}</h5>
                                <p class="card-text"><center>{!!!empty($page2->deskripsi_user_3) ? $page2->deskripsi_user_3 : ' '!!}</center></p>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
             @if (!empty($page2->judul_3)) 
            <div class="section-title">
                <h2>{{!empty($page2->judul_3) ? $page2->judul_3 : ' '}}</h2>
            </div>
             @endif
            <div class="row">
                <div class="col-lg-2 d-flex align-items-stretch"></div>
                <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
                    @if (!empty($page2->url_vidio_user_4) && !empty($page2->nama_user_4) && !empty($page2->deskripsi_user_4))
                        <div class="card" style="width: 100%">
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="{{!empty($page2->url_vidio_user_4) ? $page2->url_vidio_user_4 : ' '}}" allowfullscreen></iframe>
                            </div><hr>
                            <div class="card-icon">
                                <div class="profile-userpic text-center">
                                    <div class="avatar-upload">
                                        <center>
                                            <div class="avatar-preview">
                                                <div id="imagePreview" style="background-image: url( {{url('storage/avatar/default_avatar.jpg')}} )"></div>
                                            </div>
                                        </center>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <h5 class="card-title">{{!empty($page2->nama_user_4) ? $page2->nama_user_4 : ' '}}</h5>
                                <p class="card-text"><center>{!!!empty($page2->deskripsi_user_4) ? $page2->deskripsi_user_4 : ' '!!}</center></p>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
                    @if (!empty($page2->url_vidio_user_5) && !empty($page2->nama_user_5) && !empty($page2->deskripsi_user_5))
                        <div class="card" style="width: 100%">
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="{{!empty($page2->url_vidio_user_5) ? $page2->url_vidio_user_5 : ' '}}" allowfullscreen></iframe>
                            </div><hr>
                            <div class="card-icon">
                                <div class="profile-userpic text-center">
                                    <div class="avatar-upload">
                                        <center>
                                            <div class="avatar-preview">
                                                <div id="imagePreview" style="background-image: url( {{url('storage/avatar/default_avatar.jpg')}} )"></div>
                                            </div>
                                        </center>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <h5 class="card-title">{{!empty($page2->nama_user_5) ? $page2->nama_user_5 : ' '}}</h5>
                                <p class="card-text"><center>{!!!empty($page2->deskripsi_user_5) ? $page2->deskripsi_user_5 : ' '!!}</center></p>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="col-lg-2 d-flex align-items-stretch"></div>
            </div>
            @if (!empty($page2->nama_button_3))
            <center>
                <a href="https://wa.me/{{ $noWa }}?text={!! strip_tags($message) !!}" class="btn btn-primary btn-lg">{{!empty($page2->nama_button_3) ? $page2->nama_button_3 : ' '}}</a>
            </center>
            @endif
            
        </div>
    </section><!-- End Why Us Section -->
     @endif
    <div class="col-md-12">
        <img src="{{url('storage/landing-page-image-7/'.$page2->image_7)}}" alt="" width=100%>
    </div>
    <div class="col-md-12">
        <img src="{{url('storage/landing-page-image-8/'.$page2->image_8)}}" alt="" width=100%>
    </div><hr>
    <div class="col-md-12">
        <img src="{{url('storage/landing-page-image-9/'.$page2->image_9)}}" alt="" width=100%>
    </div><hr>
    @if (!empty($page2->nama_button_4))
    <center>
        <a href="https://wa.me/{{ $noWa }}?text={!! strip_tags($message) !!}" class="btn btn-primary btn-lg">{{!empty($page2->nama_button_4) ? $page2->nama_button_4 : ' '}}</a>
    </center>
    @endif
    

    <!-- ======= Frequenty Asked Questions Section ======= -->
     @if (!empty($page2->judul_3)) 
    <section class="faq">
        <div class="container">
            @if (!empty($page2->judul_3)) 
            <div class="section-title">
                <h2>{{!empty($page2->judul_4) ? $page2->judul_4 : ' '}}</h2>
            </div>
            @endif
            <ul class="faq-list">
                @if (!empty($page2->judul_accordion_1) && !empty($page2->deskripsi_accordion_1))
                <li>
                    <a data-toggle="collapse" class="collapsed" href="#faq1">{{!empty($page2->judul_accordion_1) ? $page2->judul_accordion_1 : ' '}}<i class="bx bx-down-arrow-alt icon-show"></i><i class="bx bx-x icon-close"></i></a>
                    <div id="faq1" class="collapse" data-parent=".faq-list">
                        <p>
                            {!!!empty($page2->deskripsi_accordion_1) ? $page2->deskripsi_accordion_1 : ' '!!}
                        </p>
                    </div>
                </li>
                @endif
                @if (!empty($page2->judul_accordion_2) && !empty($page2->deskripsi_accordion_2))
                <li>
                    <a data-toggle="collapse" href="#faq2" class="collapsed">{{!empty($page2->judul_accordion_2) ? $page2->judul_accordion_2 : ' '}}<i class="bx bx-down-arrow-alt icon-show"></i><i class="bx bx-x icon-close"></i></a>
                    <div id="faq2" class="collapse" data-parent=".faq-list">
                        <p>
                            {!!!empty($page2->deskripsi_accordion_2) ? $page2->deskripsi_accordion_2 : ' '!!}
                        </p>
                    </div>
                </li>
                @endif
                @if (!empty($page2->judul_accordion_3) && !empty($page2->deskripsi_accordion_3))
                <li>
                    <a data-toggle="collapse" href="#faq3" class="collapsed">{{!empty($page2->judul_accordion_3) ? $page2->judul_accordion_3 : ' '}}<i class="bx bx-down-arrow-alt icon-show"></i><i class="bx bx-x icon-close"></i></a>
                    <div id="faq3" class="collapse" data-parent=".faq-list">
                        <p>
                            {!!!empty($page2->deskripsi_accordion_3) ? $page2->deskripsi_accordion_3 : ' '!!}
                        </p>
                    </div>
                </li>
                @endif
                @if (!empty($page2->judul_accordion_4) && !empty($page2->deskripsi_accordion_4))
                <li>
                    <a data-toggle="collapse" href="#faq4" class="collapsed">{{!empty($page2->judul_accordion_4) ? $page2->judul_accordion_4 : ' '}}<i class="bx bx-down-arrow-alt icon-show"></i><i class="bx bx-x icon-close"></i></a>
                    <div id="faq4" class="collapse" data-parent=".faq-list">
                        <p>
                            {!!!empty($page2->deskripsi_accordion_4) ? $page2->deskripsi_accordion_4 : ' '!!}
                        </p>
                    </div>
                </li>
                @endif
                
                
                
                
            </ul>
        </div>
    </section><!-- End Frequenty Asked Questions Section -->
    @endif

    <!-- ======= Footer ======= -->
    @php
        $title = DB::table('general_settings')->where('name', 'setting_title_header')->first();
    @endphp
    <footer id="footer">
        <div class="container">
            <div class="copyright">
                &copy; Copyright <strong><span>{{date('Y')}}</span></strong>. {{$title->value}}
            </div>
            <div class="credits">
            </div>
        </div>
    </footer><!-- End #footer -->
    @include('customer.landingPage._templateScript')
</body>

</html>