
@extends('home')
@section('content')


    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <h3 class="card-title">
                                My Superteam
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="col-md-12">
                                <table id="my_teams" class="display table table-hover table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            {{-- <th>No</th> --}}
                                            <th>Nama Member</th>
                                            <th>Membership</th>
                                            <th>Tanggal Gabung</th>
                                            <th style="display:none"></th>
                                            <th>Total Omset</th>
                                            <th>Status</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    @foreach ($myTeam as $k => $v)
                                        @php
                                            $total = 0;
                                            $color = '';
                                            foreach ($v->order as $key => $value) {
                                                $total += $value->totalHarga;
                                            }
                                            if($v->status == 1){
                                                $color = 'warning';
                                                $status = 'Menunggu Persetujuan Admin';
                                            }else{
                                                $color = 'success';
                                                $status = 'Active';
                                            }
                                        @endphp
                                        
                                            <tr>
                                                {{-- <th>{{$no++}}</th> --}}
                                                <th>{{$v->first_name}}</th>
                                                <th>{{$v->membership_id}}</th>
                                                <th>{{date('d M Y H:i', strtotime($v->created_at))}}</th>
                                                <td style="display:none">{{ date('d m y H:i', strtotime($v->created_at)) }}</td>
                                                <th>Rp {{number_format($total)}}</th>
                                                <th><span class="badge badge-{{$color}}">{{$status}}</span></th>
                                                <th><a href="{{url('https://wa.me/'.$v->whatsapp_no)}}" target="_blank" class="btn btn-success btn-sm"><i class="fab fa-whatsapp"></i></a></th>
                                            </tr>
                                        
                                    @endforeach
                                </table>
                            </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>

        $(document).ready(function() {
            $('#my_teams').DataTable( {
                processing: true,
                responsive: true,
                aaSorting: [[ 3, "desc" ]]
            }  );
        } );
    </script>
@endsection