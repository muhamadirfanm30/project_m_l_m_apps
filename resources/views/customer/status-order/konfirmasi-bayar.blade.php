@extends('home')
@section('content')
<div class="content-wrapper">
    <section class="content"><br>
        <div class="row">
            <div class="col-md-12">
                @if(session()->has('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('success') }}</strong>
                        </div>
                    @elseif(session()->has('error'))
                        <div class="alert alert-danger alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('error') }}</strong>
                        </div>
                    @endif
                <div class="card card-outline">
                    <div class="card-header">
                        <h3 class="card-title">
                            <strong>
                                Konfirmasi Pembayaran
                            </strong>
                        </h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="card">
                            <div class="card-body">
                                <h5>
                                
                                    {{-- <center>
                                        <strong>Order ID {{$show->orderId}},</strong> Akan segera diproses setelah menyelesaikan proses pembayaran
                                    </center> --}}
                                    <center>
                                        Selamat Pesanan Kamu Telah Berhasil Dibuat<br>
                                        <strong>Order ID {{$show->orderId}},</strong>
                                    </center>
                                </h5>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-3"></div>
                                            <div class="col-md-6">
                                                <div class="card-body border border-primary">
                                                    <div class="col-md-12">
                                                        <center><strong><h5>Jumlah yang Harus di bayarkan : <br><strong> {{ $show->totalKeseluruhan > 0 ? 'Rp. '.number_format($show->totalKeseluruhan) : "-" }}</strong></h5></strong></center>
                                                        <center><strong><h5>Batas Waktu Pembayaran : <br><strong style="color:red"> {{ \Carbon\Carbon::parse($show->expired_date)->translatedFormat('d M Y H:i') }}</strong></h5></strong></center>
                                                    </div>
                                                </div><br>
                                                <h6>
                                                    <center>Pastikan Transfer dengan nomor Unik 3 Digit Terahir, Untuk mempercepat proses Transaksi</center>
                                                </h6><br>
                                                <h5>
                                                @if($show->payment_method != "Midtrans" || $show->payment_method != 0 )
                                                    <center><strong>Transfer Pembayaran ke Rekening Berikut :</strong></center></h5>
                                                    <center><img src="{{url('storage/bank-icon/'.$show->getBank->image)}}" alt="Logo Bank" width="70px"></center><br>
                                                    <center><h5><strong>{{ $show->getBank->nomor_rekening }}</strong></h5></center>
                                                    <center><h5  style="margin-bottom: 5px"><strong>KCP Daerah</strong></h5></center>
                                                    <center><h5  style="margin-bottom: 20px"><strong>a/n {{ $show->getBank->nama_rekening }}</strong></h5></center><hr>
                                                    <center><h5> <small style="color: red; font-size:20px">*</small> Pastikan Transfer Dengan Nominal yang Sesuai Hingga 3 Digit Terakhir Perbedaan Nominal Transfer Akan Memperlambat Proses Konfirmasi Pesanan.</h5></center>
                                                   <!-- <center><h5> <small style="color: red; font-size:20px">*</small> Jangan Lupa Melakukan Konfirmasi Pembayaran Setelah Transfer.</h5></center>
                                                    <center><h5> <small style="color: red; font-size:20px">*</small> Konfirmasi Pembayaran anda dengan klik tombol "Saya Sudah Bayar"</h5></center>
                                                    <center><h5> <small style="color: red; font-size:20px">*</small> Melakukan Konfirmasi Sebelum Pembayaran Akan Menyebabkan Proses pesanan anda lebih lama.</h5></center> -->
                                                    <center><h5> <small style="color: red; font-size:20px">*</small> Jangan Lupa Melakukan Konfirmasi Pembayaran Dengan Cara Upload Bukti Transfer Berupa Screenshot/Foto Struk Pembayaran</h5></center>
                                                @elseif($transaction->payment_method == "Midtrans" && in_array($transaction->payment_type,['bank_transfer','other_va']))
                                                    <center><strong id="cekVA">Lihat Virtual Account</strong></center></h5>
                                                @endif
                                            </div>
                                            <div class="col-md-3"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-2"></div>
                                            <div class="col-md-8">
                                                <form action="{{ url('/customer/status-order/konfirmasi/'.$show->orderId) }}" method="post" enctype="multipart/form-data">
                                                    @csrf
                                                    <div class="row">
                                                        <div class="col-md-4"><a href="{{ url('customer/status-order/show') }}" class="btn btn-warning btn-sm btn-block" style="color: white">Tutup</a></div>
                                                        <div class="col-md-4">
                                                            <label class="btn btn-primary btn-sm btn-block">
                                                                Upload Bukti Bayar <input type="file" name="files" id="fileupload" onchange="getFile(this)" hidden>
                                                            </label>
                                                            <label class="btn btn-danger btn-sm btn-block" id="deleteFile" style="display:none">
                                                                 Hapus Bukti Transfer
                                                            </label>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="row">
                                                                <div class="col-md-9">
                                                                    <button type="submit" class="btn btn-secondary btn-sm btn-block">Saya Sudah Bayar</button>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="col-md-2"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            
                        </div>
                    </div>
                </div>
            </div>
        </div><br>
    </section>
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="https://app.sandbox.midtrans.com/snap/snap.js" data-client-key="{{ env('MD_CLIENT_KEY') }}"></script>
<script>
    function getFile(elem){
        if(elem.value != ""){
            document.getElementById('deleteFile').style.display = "block";
        }
    }

    $('#deleteFile').on("click",function(){
        swal({
            title: "success",
            text: "Bukti Transfer yang Anda Upload Berhasil Dihapus.",
            type: 'success'
        });
        $('#fileupload').val("");
        document.getElementById('deleteFile').style.display = "none";
        
    })
</script>
@endsection
