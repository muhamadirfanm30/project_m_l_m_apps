@extends('home')
@section('content')
<div class="content-wrapper">
    <section class="content"><br>
        <div class="row">
            <div class="col-md-12">
                <div class="card card-outline">
                    <div class="card-header">
                        <h3 class="card-title">
                            <strong>
                                Konfirmasi Pembayaran
                            </strong>
                        </h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="card">
                            <div class="card-body">
                                <h5>
                                
                                    <center>
                                        Selamat Pesanan Kamu Telah Berhasil Dibuat<br>
                                        <strong>Order ID {{$transaction->orderId}},</strong>
                                    </center>
                                </h5>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-3"></div>
                                            <div class="col-md-6">
                                                <div class="card-body border border-primary">
                                                    <div class="col-md-12">
                                                        
                                                        <center><strong><h5>Jumlah yang Harus di bayarkan : <br><strong> {{ $transaction->totalKeseluruhan > 0 ? 'Rp. '.number_format($transaction->totalKeseluruhan) : "-" }}</strong></h5></strong></center>
                                                        <center><strong><h5>Batas Waktu Pembayaran : <br><strong style="color:red">  {{ \Carbon\Carbon::parse($transaction->expired_date)->translatedFormat('d M Y H:i') }}</strong></h5></strong></center>
                                                    </div>
                                                </div><br>
                                                <!-- <h6>
                                                    <center>Pastikan Transfer dengan nomor Unik 3 Digit Terahir, Untik mempercepat proses Transaksi</center>
                                                </h6><br> -->
                                                <h5>
                                                @if($transaction->payment_method != "Midtrans" || $transaction->payment_method != 0 )
                                                    <center><strong>Transfer Pembayaran ke Rekening Berikut :</strong></center></h5>
                                                    <center><img src="{{url('storage/bank-icon/'.$transaction->getBank->image)}}" alt="Logo Bank" width="70px"></center><br>
                                                    <center><h5><strong>{{ $transaction->getBank->nomor_rekening }}</strong></h5></center>
                                                    <center><h5  style="margin-bottom: 5px"><strong>KCP Daerah</strong></h5></center>
                                                    <center><h5  style="margin-bottom: 20px"><strong>a/n {{ $transaction->getBank->nama_rekening }}</strong></h5></center><hr>
                                                    <center><h5> <small style="color: red; font-size:20px">*</small> Pastikan Transfer Dengan Nominal yang Sesuai Hingga 3 Digit Terakhir, Perbedaan Nominal Transfer Akan Memperlambat Konfirmasi Pesanan. </h5></center>
                                                   <!-- <center><h5> <small style="color: red; font-size:20px">*</small> Jangan Lupa Melakukan Konfirmasi Pembayaran Setelah Transfer.</h5></center>
                                                    <center><h5> <small style="color: red; font-size:20px">*</small> Konfirmasi Pembayaran anda dengan klik tombol "Saya Sudah Bayar"</h5></center>
                                                    <center><h5> <small style="color: red; font-size:20px">*</small> Melakukan Konfirmasi Sebelum Pembayaran Akan Menyebabkan Proses pesanan anda lebih lama.</h5></center> -->
                                                    <center><h5> <small style="color: red; font-size:20px">*</small> Jangan Lupa Melakukan Konfirmasi Pembayaran Dengan Cara Upload Bukti Transfer Berupa Screenshot/Foto Struk Pembayaran</h5></center>
                                                    
                                                    
                                                    
                                                @else
                                                    {{-- <center><strong id="cekVA">Lihat Virtual Account</strong></center></h5> --}}
                                                @endif
                                                
                                            </div>
                                            <div class="col-md-3"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <a href="{{url('customer/status-order/detail/'.$transaction->orderId)}}" class="btn btn-primary btn-block">Cek Status Pembayaran</a>
                        </div>
                    </div>
                    <!-- <div class="card-footer">
                        <a href="{{ url('customer/status-order/show') }}" class="btn btn-danger">Tutup</a>
                        <button class="btn btn-success">Upload Bukti Pembayaran</button>
                        <button class="btn btn-primary">Saya Sudah Bayar</button>
                    </div> -->
                </div>
            </div>
        </div><br>
    </section>
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
<script type="text/javascript"
        src="https://app.sandbox.midtrans.com/snap/snap.js"
        data-client-key="{{ env('MD_CLIENT_KEY') }}"></script>
<script>
        $('#cekVA').on('click', function(){
            snap.pay('{{ $transaction->va }}',{
                onSuccess: function(result){
                },
                onPending: function(result){
                },
                onError: function(result){
                },
                onClose: function(){
                }
            });
        })
        
    </script>
@endsection
