@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <h3 class="card-title">
                                Status Order
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="col-md-12">
                                <table id="ststus-orders" class="display table table-hover table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th style="display: none"></th>
                                            <th>Order ID</th>
                                            <th>Kategori</th>
                                            <th>Status Pesanan</th>
                                            <th>Total Harga</th>
                                            <th>Tanggal Order</th>
                                            <th>Batas Pembayaran</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($data as $r)
                                            <?php
                                                $color = '';
                                                $arrStatus = ['Menunggu Pembayaran','Pembayaran Diterima','Pesanan Dibatalkan','Expired','Sedang Dikirim','Pesanan Selesai'];
                                                if($r->status == 1){
                                                    $color = 'success';
                                                }elseif($r->status == 2){
                                                    $color = 'danger';
                                                }elseif($r->status == 3){
                                                    $color = 'danger';
                                                }elseif($r->status == 4){
                                                    $color = 'success';
                                                }elseif($r->status == 5){
                                                    $color = 'primary';
                                                }elseif($r->status == 0){
                                                    $color = 'warning';
                                                }
                                            ?>
                                            <tr>
                                                <td style="display: none">{{ $r->id }}</td>
                                                <td>
                                                    <a href="{{url('customer/status-order/detail/'.$r->orderId)}}" style="text-decoration: underline; color:blue">
                                                        <strong>{{ $r->orderId }}</strong>
                                                    </a>
                                                </td>
                                                <td>{{$r->kategori}}</td>
                                                <td>
                                                    @if ($r->status == 0 && !empty($r->photo))
                                                        <span class="badge badge-warning">Pembayaran Sedang di Konfirmasi</span>
                                                    @else
                                                        <span class="badge badge-{{$color}}">{{ $arrStatus[$r->status] }}</span>
                                                    @endif
                                                </td>
                                                <td>{{ $r->totalKeseluruhan > 0 ? 'Rp. '.number_format($r->totalKeseluruhan) : "-" }}</td>
                                                <td>{{ date('d M Y H:i', strtotime($r->created_at)) }}</td>
                                                <td>{{ date('d M Y H:i', strtotime($r->expired_date)) }}</td>
                                                <td>
                                                    @if($r->status == 0 && $r->payment_method != "Midtrans")
                                                        @if (empty($r->photo))
                                                            <a href="{{ url('customer/status-order/konfirmasi-bayar/'.$r->orderId) }}" class="btn btn-success btn-sm"><i class="far fa-check-square"></i></a>
                                                            {{-- <button type="submit" class="btn btn-danger btn-sm" id="batalkan_pesanan" data-order="{{ $r->orderId }}" title="Batalkan Pesanan"><i class="fa fa-times"></i></button> --}}
                                                        @endif
                                                    @endif
                                                    @if($r->status == 4)
                                                        <button type="submit" id="terima_pesanan" class="btn btn-primary btn-sm" data-pesanan="{{ $r->orderId }}"> Pesanan Diterima</button>
                                                    @endif
                                                    <a href="{{ url('customer/status-order/detail/'.$r->orderId) }}" class="btn btn-primary btn-sm" title="Detail Pesanan"><i class="far fa-eye"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="modal fade" id="modal-sm">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Konfirmasi Pembayaran</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <form id="forms" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label>File</label>
                        <input type="file" id="files" name="files" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="submit" id="files" class="btn btn-primary">
                    </div>
                </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#ststus-orders').DataTable({
                "order": [[ 0, "desc" ]]
            });
        } );
    </script>
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()

            $('#modal-sm').on('show.bs.modal', function (event) {
                let orderId = $(event.relatedTarget).data('orderid');
                let url = '/customer/status-order/konfirmasi/'+orderId;
                $('#forms').attr('action', url);
                // var myVal = $(event.relatedTarget).data('val');
                // $(this).find(".modal-body").text(myVal);
            });
        })

        $(document).on('click', '#batalkan_pesanan', function(){
            id = $(this).attr('data-order');
            console.log(id)
            Helper.loadingStart()
            $.ajax({
                url: '/customer/status-order/update-status/' + id,
                type: 'post',
                success: function(resp) {
                    Helper.loadingStop()
                    console.log(resp)
                    swal({
                    title: "Sukses!",
                        text: "Pembatalan Pesanan Sukses",
                        type: 'success'
                    }, function() {
                        window.location.href='/customer/status-order/show';
                    });
                },
                error:function(a,v,c){
                    Helper.loadingStop()
                    var msg = JSON.parse(a.responseText);
                    console.log(msg);
                    swal({
                        title: "Ups!",
                            text: msg.message,
                            type: 'error'
                        });
                }
            })

        });

        $(document).on('click', '#terima_pesanan', function(){
            id = $(this).attr('data-pesanan');
            console.log(id)
            Helper.loadingStart()
            $.ajax({
                url: '/customer/status-order/konfirmasi/pesanan-sampai/' + id,
                type: 'post',
                success: function(resp) {
                    Helper.loadingStop()
                    console.log(resp)
                    swal({
                    title: "Sukses!",
                        text: "Pesanan Sudah Diterima",
                        type: 'success'
                    }, function() {
                        window.location.href='/customer/status-order/show';
                    });
                },
                error:function(a,v,c){
                    Helper.loadingStop()
                    var msg = JSON.parse(a.responseText);
                    console.log(msg);
                    swal({
                        title: "Ups!",
                            text: msg.message,
                            type: 'error'
                        });
                }
            })

        });
    </script>
@endsection