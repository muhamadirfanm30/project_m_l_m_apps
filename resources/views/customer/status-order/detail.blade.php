@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <h3 class="card-title">
                                Status Orders
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="card-header">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-9">
                                            <h3 class="card-title">
                                                Detail Order
                                            </h3>
                                        </div>
                                        <div class="col-md-3">
                                            @if($transaction->status == 0)
                                                @if (empty($transaction->photo) && $transaction->payment_method != "Midtrans")
                                                    <a href="{{ url('customer/status-order/konfirmasi-bayar/'.$transaction->orderId) }}" class="btn btn-success btn sm btn-block"><i class="far fa-check-square"></i>&nbsp; Konfirmasi Pembayaran</a> 
                                                @endif
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="card-body">
                                <div class="col-md-12">
                                    <table id="ststus-orders" class="display table table-hover table-bordered" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Product</th>
                                                <th>Jumlah</th>
                                                <th>Harga</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($orderDetail as $r)
                                            @if ($r->is_produk == 'get_produk' || $r->is_produk == 'get_produk_online')
                                                @foreach ($r->products as $getProduk)
                                                    <tr>
                                                        <td>{{ $getProduk->nama_produk }}</td>
                                                        <td>{{ $r->qty }}</td>
                                                        <td>{{ 'Rp. '.number_format($r->harga) }}</td>
                                                    </tr>
                                                @endforeach
                                                
                                            @else
                                                <tr>
                                                    <td>
                                                        @if($r->is_produk == 'get_paket')
                                                            {{ $r->getpaketupgrade->nama_paket }}
                                                        @elseif($r->is_produk == 'get_paket_reguler')
                                                            {{ $r->getpaketreguler->nama_paket }}
                                                        @else 
                                                            -
                                                        @endif
                                                        {{-- {{ !empty($r->getProduct) ? $r->getProduct->nama_produk : $r->product_id }} --}}
                                                    </td>
                                                    <td>{{ $r->qty }}</td>
                                                    <td>{{ 'Rp. '.number_format($r->harga) }}</td>
                                                </tr>
                                            @endif
                                                
                                            @endforeach
                                        </tbody>
                                        <tbody>
                                            <tr>
                                                <th colspan="2">Ongkir</th>
                                                <th>{{ $transaction->totalOngkir > 0 ? 'Rp. '.number_format($transaction->totalOngkir) : "-" }}</th>
                                            </tr>
                                            <tr>
                                                <th colspan="2">Total Pembayaran</th>
                                                <th>{{ $transaction->totalKeseluruhan > 0 ? 'Rp. '.number_format($transaction->totalKeseluruhan) : "-" }}</th>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="card-header">
                                <h3 class="card-title">
                                    Detail Pengiriman
                                </h3>
                            </div>
                            <div class="card-body">
                                @if(count($shipment) != 0)
                                <style>
                                    table {
                                        font-size: 1em;
                                    }

                                    .ui-draggable, .ui-droppable {
                                        background-position: top;
                                    }
                                </style>
                                <div id="accordion">
                                    @foreach($transaction->getShipment as $index => $r)
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-11"><h5> Alamat {{ $no++ }}</h5></div>
                                                    <div class="col-md-1">
                                                        @if ($transaction->status == 1 || $transaction->status == 0)
                                                            <input type="hidden" name="id" value="{{$r->id}}" id="getIdPayment">
                                                            <a type="button" data-id ="{{$r->id}}" id="btn_update_address" data-toggle="tooltip" data-html="true" title="Edit Alamat Pengiriman" style="float: right;"><i class="fa fa-edit"></i></a>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        
                                        <div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <table class="table table-striped table-bordered">
                                                            <tr>
                                                                <td>Nama Produk</td>
                                                                <td>QTY</td>
                                                                <td>Berat</td>
                                                            </tr>
                                                            <tbody>
                                                                <tr>
                                                                    <td>{{ $r->getProduk->nama_produk }}</td>
                                                                    <td>{{ $r->qty }}</td>
                                                                    <td>{{ $r->getProduk->berat }} (gram)</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div><hr>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <table>
                                                                <tr>
                                                                    <td>Nama Pengirim</td>
                                                                    <td> : {{ $r->nama_pengirim }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Nomor Pengirim</td>
                                                                    <td> : {{ !empty($transaction->get_user) ? $transaction->get_user->whatsapp_no : '-' }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Nama penerima</td>
                                                                    <td> : {{ $r->nama_lengkap }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Nomor Penerima</td>
                                                                    <td> : {{ $r->nomor_ponsel }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Tanggal Order</td>
                                                                    <td> : {{ date('d M Y H:i', strtotime($transaction->created_at)) }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width:26%">Alamat</td>
                                                                    <td> :
                                                                        {{$r->alamat != null ? $r->alamat : '-' }}, 
                                                                        {{$r->province_id != null ? $r->province_id : '-' }},  
                                                                        {{$r->kota_id != null ? $r->kota_id : '-' }}  
                                                                        {{$r->kode_pos != null ? $r->kode_pos : '-' }},
                                                                        {{$r->district_id != null ? $r->district_id : '-' }}
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <table>
                                                                <tr>
                                                                    <td>Status Pembayaran</td>
                                                                    <?php
                                                                        $arrStatus = ['Menunggu Pembayaran','Sukses','Gagal','Expired','Sedang Dikirim','Pesanan Selesai'];
                                                                    ?>
                                                                    <td> : {{ $arrStatus[$transaction->status] }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Pembayaran Berkahir Pada</td>
                                                                    <td> : {{ date('d M Y H:i', strtotime($transaction->expired_date)) }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Kurir</td>
                                                                    <td>  :  {{ !empty($r->kurir) ? strtoupper($r->kurir) : "-" }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Subtotal Pengiriman</td>
                                                                    <td> :  Rp.  {{ number_format($r->totalOngkir) }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Nomor Resi</td>
                                                                    <td> :  {{ !empty($r->no_resi) ? $r->no_resi : "Resi Pengiriman Belum di Update" }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Metode Pembayaran</td>
                                                                    @if($transaction->payment_method != "Midtrans")
                                                                        @if (!empty($transaction->getBank))
                                                                             <td> :  {{ $transaction->payment_method }} - {{ $transaction->getBank->nomor_rekening.' A/N '.$transaction->getBank->nama_rekening  }} </td>
                                                                        @else
                                                                             <td> : -</td>
                                                                        @endif
                                                                    @elseif($transaction->payment_method == "Midtrans")
                                                                        {{-- <td> : Midtrans @if( in_array($transaction->payment_type,['bank_transfer','other_va'])) - <label id="cekVA">Cek VA</label> @endif</td> --}}
                                                                        <td> : Midtrans <label id="cekVA">Lihat Instruksi</label> </td>
                                                                    @endif
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                @else
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    @if($transaction->own_transaction == 0 && $transaction->nama_pengirim != null)
                                                        <div class="row">
                                                            <div class="col-md-3">Nama Pengirim</div>
                                                            <div class="col-md-9">: &nbsp; {{$transaction->nama_pengirim != null ? $transaction->nama_pengirim : '-' }}</div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-3">Nomor Pengirim</div>
                                                            <div class="col-md-9">: &nbsp; {{ !empty($transaction->get_user) ? $transaction->get_user->whatsapp_no : '-' }}</div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-3">Nama Penerima</div>
                                                            <div class="col-md-9">: &nbsp; {{$transaction->nama_lengkap != null ? $transaction->nama_lengkap : '-' }}</div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-3">Nomor Penerima</div>
                                                            <div class="col-md-9">: &nbsp; {{$transaction->nomor_ponsel != null ? $transaction->nomor_ponsel : '-' }}</div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-3">Kurir</div>
                                                            <div class="col-md-9">: &nbsp; {{ strtoupper($transaction->kurir != null ? $transaction->kurir : '-' ) }}</div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-3">Nomor Resi</div>
                                                            <div class="col-md-9">: &nbsp; {{$transaction->resi_pengiriman != null ? $transaction->resi_pengiriman : '-' }}</div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-3">Alamat</div>
                                                            <div class="col-md-9">: &nbsp; 
                                                                {{$transaction->alamat != null ? $transaction->alamat : '-' }}, 
                                                                {{$transaction->province_id != null ? $transaction->province_id : '-' }},  
                                                                {{$transaction->kota_id != null ? $transaction->kota_id : '-' }}  
                                                                {{$transaction->kode_pos != null ? $transaction->kode_pos : '-' }},
                                                                {{$transaction->district_id != null ? $transaction->district_id : '-' }}
                                                            </div>
                                                        </div>
                                                    @endif
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <div class="col-md-4">Status Pembayaran</div>
                                                        <?php
                                                            $arrStatus = ['Menunggu Pembayaran','Sukses','Gagal','Expired','Sedang Dikirim','Pesanan Selesai'];
                                                        ?>
                                                        <div class="col-md-8">
                                                            @if (!empty($transaction->status))
                                                                 : &nbsp; {{ $transaction->status == 0 && !empty($transaction->photo) ? 'Menunggu Konfirmasi Pembayaran' : $arrStatus[$transaction->status] }}
                                                            @else
                                                                 : &nbsp; {{ !empty($transaction->photo) ? 'Menunggu Konfirmasi Pembayaran' : 'Menunggu Pembayaran' }}
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">Pembayaran Berakhir Pada</div>
                                                        <div class="col-md-8">
                                                            : &nbsp; {{ date('d M Y, H:i', strtotime($transaction->expired_date)) }}
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">Metode Pembayaran</div>
                                                        <div class="col-md-8">
                                                            @if (!empty($transaction->getBank))
                                                                : &nbsp; {{ strtoupper($transaction->payment_method != "Midtrans" ? $transaction->getBank->nama_bank : "Midtrans") }}
                                                            @else
                                                                : &nbsp; Midtrans  {{ $transaction->payment_type }}
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">Sub Total Pesanan</div>
                                                        <div class="col-md-8">: &nbsp; Rp. {{ $transaction->totalKeseluruhan > 0 ? number_format($transaction->totalKeseluruhan) : "-" }} </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">Sub Total Pengiriman</div>
                                                        <div class="col-md-8">: &nbsp; Rp. {{ $transaction->totalOngkir > 0 ? number_format($transaction->totalOngkir) : "-" }} </div>
                                                    </div>
                                                    @if($transaction->payment_method != "Midtrans")
                                                        <div class="row">
                                                            <div class="col-md-4">Nomor Rekening</div>
                                                            <div class="col-md-8">
                                                                @if (!empty($transaction->getBank))
                                                                    <p> : &nbsp; <b>{{ $transaction->getBank->nomor_rekening.' A/N '.$transaction->getBank->nama_rekening  }}  </b></p>
                                                                @else
                                                                    <p> : -</p>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    {{-- @elseif($transaction->payment_method == "Midtrans" && in_array($transaction->payment_type,['bank_transfer','other_va'])) --}}
                                                    @elseif($transaction->payment_method == "Midtrans" )
                                                        <div class="row">
                                                            <div class="col-md-4">Intruksi Pembayaran</div>
                                                            <div class="col-md-8">: &nbsp; <b>  <input type="submit" class="btn btn-warning btn-sm" value="Lihat Instruksi" id="cekVA" style="color: white"/></div>
                                                        </div>
                                                    @endif
                                                    <div class="row">
                                                        <div class="col-md-4">Total Berat</div>
                                                        <div class="col-md-8">: &nbsp; <b>{{ $transaction->total_berat > 0 ? number_format($transaction->total_berat)." (Gram)" : "-" }}</b></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    {{-- <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script> --}}
    {{-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> --}}
    <link rel="stylesheet" href="{{ asset('styles.css') }}">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript"
        src="https://app.sandbox.midtrans.com/snap/snap.js"
        data-client-key="{{ env('MD_CLIENT_KEY') }}"></script>
        <script>
            $( function() {
              $( "#accordion" ).accordion({
                collapsible: true
              });
            } );
        </script>
    <script>
        $(document).ready(function() {
            $('#ststus-orders').DataTable();

            
        } );

        $('#cekVA').on('click', function(){
            snap.pay('{{ $transaction->va }}',{
                onSuccess: function(result){
                },
                onPending: function(result){
                },
                onError: function(result){
                },
                onClose: function(){
                }
            });
        })
        
    </script>
@endsection