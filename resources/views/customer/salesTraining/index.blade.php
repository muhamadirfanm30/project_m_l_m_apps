@extends('home')
@section('content')
<div class="content-wrapper">
    <section class="content"><br>
        
        <div class="row">
            <div class="col-md-12">
                <div class="card card-outline">
                    <div class="card-header">
                        <h3 class="card-title">
                            <strong>
                                Sales Training
                            </strong>
                        </h3>
                    </div>
                    <!-- /.card-header -->
                        <br>
                        @foreach ($getSalesTraining->sortBy('publish_at') as $k => $ST)
                            <div class="card-body border border-danger">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <img class="img-thumbnail" src="{{ url('storage/sales-training-image/'.$ST->foto) }}" alt="">
                                        </div>
                                        
                                        <div class="col-md-8">
                                            <strong><h4>{{$ST->judul}}</h4></strong>
                                            @if (strlen(strip_tags($ST->deskripsi)) > 400)
                                                <small>{!! substr($ST->deskripsi,0, 400) !!} .....</small><br><br>
                                            @else
                                                <small>
                                                    {!! $ST->deskripsi !!} 
                                                </small><br>
                                            @endif
                                           
                                        </div>
                                    </div>
                                     <div class="col-md-12 row">
                                              
                                                <div class="col-md-12">
                                                 <center> <a href="{{ url('customer/sales-training/detail/'.$ST->id) }}" class="btn btn-warning" style="color: white">Pelajari Module</a></center>
                                                </div>
                                              
                                                {{-- <div class="col-md-4">
                                                    Tanggal Terbit  {{ date('d M Y', strtotime($ST->publish_at)) }}
                                                </div> --}}
                                            </div>
                                </div>
                            </div><br>
                        @endforeach
                        {!! $getSalesTraining->render() !!}
                    <div class="card-footer">
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
