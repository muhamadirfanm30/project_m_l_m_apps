@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <h3 class="card-title">
                                 Sales Training
                            </h3>
                        </div><br>
                        <div class="card-body">
                            <div class="col-md-12">
                                <div class="row">
                                    @foreach ($getVidio as $k => $v)
                                        <div class="col-md-4">
                                            <div class="embed-responsive embed-responsive-16by9" style="width:auto">
                                                <iframe class="embed-responsive-item" src="{{$v->url_vidio}}" allowfullscreen></iframe>
                                            </div>
                                            <strong>
                                                <center style="padding-bottom: 40px; font-size:20px">{{$v->judul_vidio}}</center>
                                            </strong>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="col-md-3">
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>
        
    </script>
@endsection