@extends('home')
@section('content')
<div class="content-wrapper">
    <section class="content"><br>
        <div class="row">
            <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="card card-outline">
                                <div class="card-header">
                                    <h3 class="card-title">
                                        <strong>
                                            Metode Pembayaran
                                        </strong>
                                    </h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    {{-- <div class="card">
                                        <div class="card-body">
                                            <div class="form-group">
                                                <label for="exampleInputBorderWidth2">Bank Transfer <code></code></label>
                                                <select class="form-control form-control-border border-width-2" name="payment_method" id="exampleInputBorderWidth2">
                                                    <option>BCA</option>
                                                    <option value="">Pilih Metode Pembayaran</option>
                                                    <optgroup label="Transfer Bank">
                                                        <option value="bca">BCA</option>
                                                    </optgroup>
                                                </select>
                                              </div>
                                        </div>
                                    </div> --}}
                                    <div class="accordion" id="accordionExample">
                                        @if (!empty($bank->payment_name))
                                    <div class="card">
                                        <div class="card-header" id="headingOne">
                                            <h2 class="mb-0">
                                                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                    {{$bank->payment_name}}
                                                </button>
                                            </h2>
                                        </div>

                                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                            <div class="card-body">
                                                    <div class="row">
                                                        @foreach($listPayment as $key => $bank)
                                                            <div class="col-md-4">
                                                                <div class="form-check">
                                                                    <input class="form-check-input" type="radio" name="payment_method" value="{{$bank->nama_bank}}">
                                                                    <label class="form-check-label">
                                                                        <img src="{{url('storage/bank-icon/'.$bank->image)}}" alt="{{$bank->nama_bank}}" width="50px">
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                @if (!empty($midtrans->payment_name))
                                    <div class="card">
                                        <div class="card-header" id="headingTwo">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                {{ $midtrans->payment_name }}
                                            </button>
                                        </h2>
                                        </div>
                                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                        <div class="card-body">
                                            <div class="col-md-4">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="payment_method" value="0">
                                                    <label class="form-check-label">
                                                        <img src="https://docs.midtrans.com/asset/image/main/midtrans-logo.png" alt="Midtrans" width="90px">
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                @endif
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" id="checkout" class="btn btn-primary btn-block">Bayar Sekarang</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card card-outline">
                                <div class="card-header">
                                    <h3 class="card-title">
                                        <strong>
                                            Ringkasan Pesanan
                                        </strong>
                                    </h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-8">Total Pesanan</div>
                                            <div class="col-md-4"><strong>Rp. {{number_format($detail->harga)}}</strong></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-8">Ongkos Kirim</div>
                                            <div class="col-md-4"><strong>Rp. 0</strong></div>
                                        </div><hr>
                                        @php
                                            $total = 0;
                                            $total += $detail->harga;
                                        @endphp
                                        <div class="row">
                                            <div class="col-md-8">Sub Total</div>
                                            <div class="col-md-4"><strong>Rp. {{number_format($total)}}</strong></div>
                                        </div>
                                        <input type="hidden" name="totalHarga" value="{{$detail->harga}}">
                                        <input type="hidden" name="totalKeseluruhan" value="{{$total}}">
                                        <input type="hidden" name="namaEvent" value="{{$detail->nama_event}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div><br>
    </section>
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.60/inputmask/jquery.inputmask.js"></script>
<script type="text/javascript"
    src="https://app.sandbox.midtrans.com/snap/snap.js"
    data-client-key="{{ env('MD_CLIENT_KEY') }}"></script>
<script>
    $('#checkout').on('click', function(){
        var payment_method = $('input[type="radio"][name="payment_method"]:checked').val();
        console.log(payment_method);
        jQuery.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "",
            type: 'POST',
            dataType: 'json',
            data: {payment_method,totalKeseluruhan:{{$total}} },
            success: function(data) {
                Helper.loadingStop()
                if(payment_method == 0){
                    snap.pay(data.token,{
                        onSuccess: function(result){
                            console.log("Sukses");
                            // storeData(data.trxId);
                            swal({
                                title: "Sukses!",
                                text: "Transaksi anda berhasil, silahkan segera lakukan pembayaran!",
                                type: 'success'
                            }, function() {
                                window.location.href = '/customer/status-order/pembayaran/' + data.orderId;
                            });
                        },
                        onPending: function(result){
                            console.log("Pending");
                            // storeData(data.trxId);
                            swal({
                                title: "Sukses!",
                                text: "Transaksi anda berhasil, silahkan segera lakukan pembayaran!",
                                type: 'success'
                            }, function() {
                                window.location.href = '/customer/status-order/pembayaran/' + data.orderId;
                            });
                        },
                        onError: function(result){
                            swal({
                                title: "Ups!",
                                text: result.status_message,
                                type: 'error'
                            });
                        },
                        onClose: function(){
                            console.log('customer closed the popup without finishing the payment');
                        }
                    });
                }else{
                    swal({
                        title: "Sukses!",
                        text: "Transaksi anda berhasil, silahkan segera lakukan pembayaran!",
                        type: 'success'
                    }, function() {
                        window.location.href = '/customer/status-order/pembayaran/' + data.orderId;
                    });
                }
            },
            error: function(a, v, c) {
                Helper.loadingStop()
                var msg = JSON.parse(a.responseText);
                swal({
                    title: "Ups!",
                    text: msg.message,
                    type: 'error'
                });
            }
        });
    })
</script>
@endsection
