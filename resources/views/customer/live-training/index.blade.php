@extends('home')
@section('content')
<div class="content-wrapper">
    <section class="content"><br>
        <div class="row">
            <div class="col-md-12">
                <div class="card card-outline">
                    <div class="card-header">
                        <h3 class="card-title">
                            <strong>
                                Events & Live Training
                            </strong>
                        </h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-3"></div>
                                <div class="col-md-6">
                                    <div class="embed-responsive embed-responsive-16by9" style="width:auto">
                                        <iframe class="embed-responsive-item" src="{{$getUrl->value}}" allowfullscreen></iframe>
                                    </div>
                                </div>
                                <div class="col-md-3"></div>
                            </div><br>
                            <div class="col-md-12">
                                <small>
                                    {!!$getDesc->value!!}
                                </small>
                            </div>
                        </div>
                        
                    </div>
                    <div class="card-footer">
                    </div>
                </div>
            </div>
        </div><br>

        <div class="row">
            <div class="col-md-12">
                <div class="card card-outline">
                    <div class="card-header">
                        <h3 class="card-title">
                            <strong>
                                Events & Live Training yang Tersedia
                            </strong>
                        </h3>
                    </div>
                    <!-- /.card-header -->
                    <br>
                        @foreach ($liveTraining as $k => $event)
                            @if ($event->publish_at <= date('Y-m-d'))
                                <div class="card-body border border-primary">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <img class="img-thumbnail" src="{{url('storage/events-image/'.$event->gambar)}}" alt="">
                                            </div>
                                            <div class="col-md-8">
                                                <strong><h4>{{$event->nama_event}}</h4></strong>
                                                <small>
                                                    @if (strlen(strip_tags($event->deskripsi)) > 400)
                                                        <small>{!! substr($event->deskripsi,0, 400) !!} .....</small><br><br>
                                                    @else
                                                        <small>
                                                            {!! $event->deskripsi !!} 
                                                        </small><br>
                                                    @endif
                                                </small>
                                            </div>
                                            <div class="col-md-12 row">
                                                <div class="col-md-12">
                                                    <center><a href="{{ url('customer/live-training/detail/'.$event->id) }}" class="btn btn-warning" style="color: white">Lihat Selengkapnya</a></center>
                                                </div>
                                            {{-- <div class="col-md-4"> Tanggal Terbit  {{ date('d M Y', strtotime($event->publish_at)) }} </div> --}}
                                        </div>
                                        </div>
                                      </div>
                                    </div>
                                
                            @endif
                            {!! $liveTraining->render() !!}
                        @endforeach
                    <div class="card-footer">
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
