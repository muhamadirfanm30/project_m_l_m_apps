@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <h3 class="card-title">
                             Cart
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="col-md-12">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th  class="text-center">Gambar Produk</th>
                                            <th  class="text-center">Product</th>
                                            <th class="text-center">Quantity</th>
                                            <th class="text-center">Price</th>
                                            <th class="text-center">Action</th>

                                            <!-- <th colspan="2">Action</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $total = 0;
                                            $berat = 0;
                                        @endphp

                                        @if (session('cart') != '')
                                            @foreach (session('cart') as $id => $details)
                                                @php
                                                    
                                                    if(!empty($details['harga_promo'])){
                                                        $price = $details['harga_promo'] * $details['qty'];
                                                    }else{
                                                        $price = $details['harga'] * $details['qty'];
                                                    }
                                                    $total += $price;
                                                @endphp
                                                <tr>
                                                    <td class="text-center" style="width: 200px">
                                                        <a class="thumbnail pull-left" href="#">
                                                            <img width="100" height="100" class="img-responsive" src="{{ url('storage/product-image/'.$details['image_produk']) }}">
                                                        </a>
                                                    </td>
                                                    <td  class="text-center" style="width: 300px;vertical-align:middle">
                                                        <h4 class="media-heading"><a href="#">{{$details['nama_produk']}}</a></h4>
                                                    </td>
                                                    <td style="text-align: center; width: 200px;vertical-align:middle">
                                                        <div class="input-group" /*style="padding-bottom:20px;"*/ data-section="{{ $details['id'] }}">
                                                            <span class="input-group-btn">
                                                                <button type="button" class="btn btn-danger minuss"  data-type="minus" data-id="{{$details['id']}}" data-price="{{ $details['harga'] }}" data-weight="{{ $details['berat'] }}" data-product="{{ $details['nama_produk'] }}">
                                                                    <span class="fa fa-minus"></span>
                                                                </button>
                                                            </span>
                                                            {{-- <input type="text" value="{{ $details['qty'] }}" min="1" name="qty[]" class="form-control input{{$details['id']}} qty" data-id="{{ $details['id'] }} data-price="{{ $details['harga'] }}" data-weight="{{ $details['berat'] }}" data-product="{{ $details['nama_produk'] }}" id="qty-{{$details['id']}}" style="text-align: center; width:20px "> --}}
                                                            <input type="text" value="{{ $details['qty'] }}" min="1" name="qty[]" class="form-control qty input{{$details['id']}}" data-id="{{ $details['id'] }}" data-price="{{ $details['harga'] }}" data-weight="{{ $details['berat'] }}" data-product="{{ $details['nama_produk'] }}" style="text-align: center; width:10px "/>
                                                            <span class="input-group-btn">
                                                                <button type="button" class="btn btn-success pluss" data-type="plus" data-id="{{$details['id']}}" data-price="{{ $details['harga'] }}" data-weight="{{ $details['berat'] }}" data-product="{{ $details['nama_produk'] }}">
                                                                    <span class="fa fa-plus"></span>
                                                                </button>
                                                            </span>
                                                        </div>
                                                        {{-- <input type="number" value="{{ $details['qty'] }}" min="1" name="qty[]" class="form-control qty" data-id="{{ $details['id'] }}" data-price="{{ $details['harga'] }}" data-weight="{{ $details['berat'] }}" data-product="{{ $details['nama_produk'] }}"/> --}}
                                                    </td>
                                                    <td class="text-center" data-th="price" style="vertical-align:middle">
                                                        <strong id="lblQty-{{$details['id']}}" class="priceByItem">Rp. {{ number_format($price) }}</strong>
                                                    </td>
                                                    <td class="text-center" style="vertical-align:middle">
                                                        <form action="{{ route('delete-cart-cs') }}" method="post">
                                                            @csrf
                                                            <input type="hidden" name="id" value="{{ $id }}">
                                                            <button class="btn btn-danger btn-sm remove-from-cart" data-id="{{ $id }}"><i class="fa fa-trash"></i></button>
                                                        </form>
                                                    </td>
                                                </tr>
                                                
                                            @endforeach
                                        @endif
                                    </tbody>
                                    <tfoot>
                                        <tr style="border-bottom:1px solid #dee2e6">
                                            <td style="vertical-align:middle"></td>
                                            <td  class="text-center" style="vertical-align:middle"><strong>Sub Total</strong></td>
                                            <td  class="text-center" style="vertical-align:middle"><strong id="lblweight">(Gram)</strong></td>
                                            <td  class="text-center" style="vertical-align:middle"><strong id="allTotal">Rp. {{ number_format($total) }}</strong></td>
                                            <td></td>
                                        </tr>
                                        <!-- <tr>
                                            <td colspan="5">
                                                <a href="{{url('customer/order-produk/checkout')}}" class="btn btn-success btn-sm">
                                                    <span class="fa fa-play"></span> &nbsp; Checkout 
                                                </a>
                                            </td>
                                        </tr> -->
                                    </tfoot>
                                </table>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-3"></div>
                                            <div class="col-md-3"></div>
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-4"><strong>Kirim Ke</strong></div>
                                                    <div class="col-md-8">
                                                        <input  id="001" type="radio" name="kirim_ke" id="kirim_ke" value="0" checked> <label for="001" style="margin-right:20px">Konsumen</label>
                                                
                                                        <input id="002" type="radio" name="kirim_ke" id="kirim_ke" value="1"> <label for="002">Stok Produk Di Stokiest</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card card-outline card-warning">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-10">
                                                Alamat Pengiriman 
                                        </div>
                                        <!-- <div class="col-md-2">
                                            <button class="btn btn-primary btn-sm btn-block btn_tambah_form"  id="addForm" onclick="newForm()"><i class="fa fa-plus"></i> Tambah Alamat</button>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <div id="FormAlamat">
                            <div class="card-body">
                                <div class="accordion" id="accordionAlamat">
                                    <div class="card">
                                        <div class="card-header" id="alamat0">
                                            <h2 class="mb-0" data-toggle="collapse" data-target="#collapse0" aria-expanded="true" aria-controls="collapse0">
                                                <button style="color:black" class="btn btn-link" type="button" >
                                                    <i class="fa fa-chevron-up"></i> Alamat Pengiriman
                                                </button>
                                            </h2>
                                        </div>
                                        <div id="collapse0" class="collapse show" aria-labelledby="alamat0" data-parent="#accordionAlamat">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-sm-6" style="display:none">
                                                        <div class="form-group">
                                                            <label>Produk</label>
                                                            <select class="form-control aProductId" name="aProductId[]" data-index='0'>
                                                                <option value=""> Pilih Produk </option>
                                                                @if (!empty(session('cart')))
                                                                    <option value="Semua Produk" selected> Semua Produk </option>
                                                                    <!-- @foreach (session('cart') as $id => $details)
                                                                        <option value="{{ $details['id'] }}"> {{ $details['nama_produk'] }}</option>
                                                                    @endforeach -->
                                                                @else 
                                                                    <option value=""> Tidak Ada Produk</option>
                                                                @endif
                                                                
                                                            </select>
                                                            <span class="invalid-feedback valaProductId" style="display:block;"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6" style="display:none">
                                                        <div class="form-group">
                                                            <label>QTY</label>
                                                            <input type="text" name="aQtyProduct[]" class="form-control aQtyProduct" placeholder="QTY ..." data-index='0' onchange="getQty(this.value,0)">
                                                            <span class="invalid-feedback valqty" style="display:block;"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label>Nama Penerima</label>
                                                            <input type="text" name="aNamaLengkap[]" class="form-control aNamaLengkap" placeholder="Nama Penerima ..." data-index="0">
                                                            <span class="invalid-feedback valaNamaLengkap" style="display:block;"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label>Nomor Penerima</label>
                                                            <input type="text" name="aNomerPonsel[]" id="phone" class="form-control aNomerPonsel" placeholder="Nomor Ponsel / Whatsapp ..." data-index="0">
                                                            <span class="invalid-feedback valaNomerPonsel" style="display:block;"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- <div class="row">
                                                    {{-- <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label>Jenis Kelamin</label>
                                                            <input type="text" name="aJenisKelamin[]" class="form-control aJenisKelamin" placeholder="Enter ..." data-index="0">
                                                            <span class="invalid-feedback valaJenisKelamin" style="display:block;"></span>
                                                        </div>
                                                    </div> --}}
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Nama Pengirim</label>
                                                            <input type="text" name="aNamaPengirim[]" class="form-control aNamaPengirim" placeholder="Nama Pengirim ..." data-index="0">
                                                            <span class="invalid-feedback valNamaPengirim" style="display:block;"></span>
                                                        </div>
                                                    </div>
                                                </div> -->
                                                <div class="row">
                                                    <div class="col-sm-6" id="provHide">
                                                        <div class="form-group">
                                                            <label>Provinsi</label>
                                                            <select class="form-control aProvinceId" name="aProvinceId[]" data-index="0" id="aProvinceId-0" onchange="getKota(0)">
                                                                <option value="">Pilih Provinsi</option>
                                                            </select>
                                                            <span class="invalid-feedback valaProvinceId" style="display:block;"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6" id="cityHide">
                                                        <div class="form-group">
                                                            <label>Kota / Kabupaten</label>
                                                            <select class="form-control aKotaId" name="aKotaId[]" style="width:100%" data-index="0" id="aKotaId-0" onchange="getDistrik(0)">
                                                                <option value="">Pilih Provinsi Terlebih dahulu</option>
                                                            </select>
                                                            <span class="invalid-feedback valaKotaId" style="display:block;"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-6" id="cityHide">
                                                        <div class="form-group">
                                                            <label>Kecamatan</label>
                                                            <select class="form-control aDistrikId" name="aDistrikId[]" style="width:100%" data-index="0" id="aDistrikId-0" onchange="getKurir(0)">
                                                                <option value="">Pilih Kota / Kabupaten Terlebih Dahulu</option>
                                                            </select>
                                                            <span class="invalid-feedback valaDistrikId" style="display:block;"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label>Kode Pos</label>
                                                            <input type="text" name="aKodePos[]" class="form-control aKodePos" placeholder="Kode Pos" data-index="0">
                                                            <span class="invalid-feedback valaKodePos" style="display:block;"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label>Kurir</label>
                                                            <select class="form-control aKurir" name="aKurir[]" style="width:100%" data-index="0" id="aKurir-0" onchange="getLayanan(0)">
                                                                <option value="">Pilih Kecamatan Terlebih Dahulu</option>
                                                            </select>
                                                            <span class="invalid-feedback valaKurir" style="display:block;"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label>Pilih Layanan</label>
                                                            <select name="aLayanan[]" data-index="0" id="aLayanan-0" class="form-control aLayanan" onchange="getTotalOngkir(this.value,0)">
                                                                <option value="">Pilih Kurir Terlebih dahulu</option>
                                                            </select>
                                                            <span class="invalid-feedback valaLayanan" style="display:block;"></span>
                                                        </div>
                                                    </div>
                                                    <input type="hidden" value="0" class="form-control aWeight" name="aWeight[]" data-index="0" id="aWeight-0">
                                                    <input type="hidden" value="0" name="totalOngkir[]">
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Alamat Lengkap</label>
                                                            <textarea name="aAlamat[]" class="form-control aAlamat" cols="31" rows="11" data-index="0"></textarea>
                                                            <span class="invalid-feedback valaAlamat" style="display:block;"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>      
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                        <div class="card card-outline card-warning">
                            <div class="card-header">
                                <h3 class="card-title">
                                    Metode Pembayaran
                                </h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <div class="accordion" id="accordionExample">
                                    @if (!empty($bank->payment_name))
                                        <div class="card">
                                            <div class="card-header" id="headingOne">
                                                <h2 class="mb-0">
                                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                        {{$bank->payment_name}}
                                                    </button>
                                                </h2>
                                            </div>

                                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                                <div class="card-body">
                                                        <div class="row">
                                                            @foreach($listPayment as $key => $bank)
                                                                <div class="col-md-4">
                                                                    <div class="form-check">
                                                                        <input id="{{ $bank->id }}" class="form-check-input" type="radio"  name="payment_method" value="{{$bank->nama_bank}}">
                                                                        <label for="{{ $bank->id }}" class="form-check-label">
                                                                            <img src="{{url('storage/bank-icon/'.$bank->image)}}" alt="{{$bank->nama_bank}}" width="80px">
                                                                        </label>
                                                                    </div>
                                                                    {{-- <input id="349" type="radio" value="1" name="question1">
                                                                    <label for="349">Abe</label> --}}
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if (!empty($midtrans->payment_name))
                                        <div class="card">
                                            <div class="card-header" id="headingTwo">
                                            <h2 class="mb-0">
                                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                    {{ $midtrans->payment_name }}
                                                </button>
                                            </h2>
                                            </div>
                                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                            <div class="card-body">
                                                <div class="col-md-4">
                                                    <div class="form-check">
                                                        <input id="01" class="form-check-input" type="radio" name="payment_method" value="0">
                                                        <label for="01" class="form-check-label">
                                                            <img src="https://docs.midtrans.com/asset/image/main/midtrans-logo.png" alt="Midtrans" width="90px">
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                                <hr>
                                <div class="col-md-12">
                                    <div class="card card-outline card-warning">
                                        <div class="card-header">
                                            <h3 class="card-title">
                                                Ringkasan Pesanan
                                            </h3>
                                        </div>
                                        <!-- /.card-header -->
                                        <div class="card-body">
                                            <table id="list-card-product" class="display" style="width:100%">
                                                <tbody>
                                                    <tr>
                                                        <td colspan="2">Subtotal Produk</td>
                                                        <td id="lbltotalHargaItem">Rp. {{ number_format($total) }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">Subtotal Pengiriman</td>
                                                        <td><span id="lbltotalOngkir"></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">Total Pesanan</td>
                                                        <td id="lbltotalKeseluruhan"></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <input type="hidden" name="totalHargaItem" id="totalHargaItem" value="{{$total}}">
                                        <input type="hidden" name="totalOngkir" id="totalOngkir">
                                        <input type="hidden" name="weight" id="weight">
                                        <input type="hidden" name="totalKeseluruhan" id="totalKeseluruhan" value="{{$total}}">
                                        <div class="card-footer">
                                            <button class="btn btn-primary btn-block" id="checkout">Checkout</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.60/inputmask/jquery.inputmask.js"></script>
    <script type="text/javascript"
        src="https://app.sandbox.midtrans.com/snap/snap.js"
        data-client-key="{{ env('MD_CLIENT_KEY') }}"></script>
        <script>
           

            $(document).ready(function(){
                $('.add-to-cart').on("click",function(){
                    id = $(this).attr('data-id');
                    window.location.href="/customer/order-produk/add-to-cart/"+id+'/'+$('#qty-'+id).val();
                });
                
                //-- Click on detail
                $("ul.menu-items > li").on("click",function(){
                    $("ul.menu-items > li").removeClass("active");
                    $(this).addClass("active");
                })

                $(".attr,.attr2").on("click",function(){
                    var clase = $(this).attr("class");

                    $("." + clase).removeClass("active");
                    $(this).addClass("active");
                })

                //-- Click on QUANTITY
                clickId = $(".input-group").attr('data-section');
                $(".minuss").on("click",function(){
                    id = $(this).attr('data-id')
                    var now = $(".input-group > .input"+id).val();
                    if ($.isNumeric(now)){
                        if(now > 1){
                            $(".input-group > .input"+id).val(parseInt(now) - 1);
                        }
                    }else{
                        $(".input-group > input"+id).val(1);
                    }
                    calculateItems(".input-group > .input"+id);
                })            
                $(".pluss").on("click",function(){
                    id = $(this).attr('data-id')
                    var now = $(".input-group > .input"+id).val();
                    if ($.isNumeric(now)){
                        $(".input-group > .input"+id).val(parseInt(now)+1);
                    }else{
                        $(".input-group > input"+id).val("1");
                    }
                    calculateItems(".input-group > .input"+id);
                })      
            }) 
        </script>
    <script>

        var arr = [];
        var total = 0;
        let idx = 1;
        let totalQty = 0;
        let option="";
        let totalOngkirKeselurahan = 0;
        let product = {!! json_encode(session('cart')) !!}
        let totalBerat = 0;
        var statusPayment = 0;
        let va="";

        $(document).ready(function() {
            $('#addForm').attr('disabled', true);
            $('#my-team').DataTable();
            option = "";
            $.map(product, function(item) { 
                option+='<option value="'+item.id+'"> '+item.nama_produk+' </option>';
            });
            
            $(".collapse.show").each(function(){
        	    $(this).prev(".card-header").find(".fa").addClass("fa-chevron-down").removeClass("fa-chevron-up");
            });
            
            // Toggle plus minus icon on show hide of collapse element
            $(".collapse").on('show.bs.collapse', function(){
                $(this).prev(".card-header").find(".fa").removeClass("fa-chevron-up").addClass("fa-chevron-down");
            }).on('hide.bs.collapse', function(){
                $(this).prev(".card-header").find(".fa").removeClass("fa-chevron-down").addClass("fa-chevron-up");
            });

            // $('textarea[name="aAlamat[]"]').eq(0).attr('disabled', true);
            // $('select[name="aProvinceId[]"]').eq(0).attr('disabled', true);
            // $('select[name="aKotaId[]"]').eq(0).attr('disabled', true);
            // $('select[name="aDistrikId[]"]').eq(0).attr('disabled', true);
            // $('select[name="aLayanan[]"]').eq(0).attr('disabled', true);
            // $('select[name="aKurir[]"]').eq(0).attr('disabled', true);
            // $('input[type="text"][name="aKodePos[]"]').eq(0).val("");
            // $('input[type="text"][name="aKodePos[]"]').eq(0).attr('disabled', true);

            onchangeProduct();
            onlyNumber('input[type="text"][name="aQtyProduct[]"]');
            onlyNumber('input[type="text"][name="aNomerPonsel[]"]');
            onlyNumber('input[type="text"][name="aKodePos[]"]');
        });

        $( window ).load(function() {
            arr = [];
            total = 0;
            totalBerat = 0;
            var a = $('input[type="text"][name="qty[]"]').map(function(){
                    var params = $(this).data();
                    var value = $(this).val();
                    var harga = 0;
                    total += value * params['price'];
                    totalBerat += value * parseInt(params['weight']);
                    totalQty += parseInt(value);
                    return arr.push({
                        id:params.id,
                        qty:value,
                        nama_produk:params.product,
                        berat:params['weight'],
                        harga:params['price']
                    });
            }).get();
            $('#allTotal').text(formatRupiah(total));
            $('#lbltotalHargaItem').text(formatRupiah(total));
            $('input[type="text"][name="aQtyProduct[]"]').eq(0).val(totalQty);
            $('#totalHargaItem').val(total);
            $('#weight').val(totalBerat);
            $('input[type="hidden"][name="aWeight[]"]').eq(0).val(totalBerat);
            $('#lblweight').text(totalBerat + " (Gram)");
            
            Helper.loadingStart()
            ajaxRequest('GET', '/api/rajaongkir/provinsi', function(response) {
                var select = '<option value="">Pilih Provinsi</option>';
                $.each(response.data.rajaongkir.results, function(item,item2) {
                    // console.log(item2)
                    select += `<option value="${item2.province_id}">${item2.province}</option>` ;
                });
                
                $('.aProvinceId').html(select)
            })
            Helper.loadingStop();
        });

        function calculateItems(element){
            var val = $(element).val();
            var data = $(element).data();
            var calculate = val * data['price'];
            console.log(data);
            console.log(element);
            $('#lblQty-'+data['id']).text(formatRupiah(calculate));
            arr = [];
            total = 0;
            totalQty = 0;
            totalBerat = 0;

            var a = $('input[type="text"][name="qty[]"]').map(function(){
                    var params = $(this).data();
                    var value = $(this).val();
                    var harga = 0;
                    total += value * params['price']
                    totalQty += parseInt(value);
                    totalBerat += value * parseInt(params['weight']);
                    return arr.push({
                        id:params.id,
                        qty:value,
                        nama_produk:params.product,
                        berat:params['weight'],
                        harga:params['price']
                    });
            }).get();
            if(idx > totalQty){
                document.getElementById("formAlamat-"+parseInt(totalQty) ).remove();
                idx--;
            }
            $('#allTotal').text(formatRupiah(total));
            $('#lbltotalHargaItem').text(formatRupiah(total));
            $('input[type="text"][name="aQtyProduct[]"]').eq(0).val(totalQty);
            $('#totalHargaItem').val(total);
            $('#weight').val(totalBerat);
            $('input[type="hidden"][name="aWeight[]"]').eq(0).val(totalBerat);
            $('#lblweight').text(totalBerat + " (Gram)");
            getKurir(0,false);
        }
        $('input[type="text"][name="qty[]"]').on('input', function(){

            $("input.numbers").keypress(function(event) {
                return /\d/.test(String.fromCharCode(event.keyCode));
            });
            calculateItems(this);
        });

        function getKota(index){
            var val = $('#aProvinceId-'+index).val();
            if(val == ""){
                swal({
                    title: "Ups!",
                    text: "Silahkan Pilih Provinsi Yang Tersedia! ",
                    type: 'error'
                });
            }else{
                Helper.loadingStart()
                ajaxRequest('GET', '/api/rajaongkir/city?province=' + val, function(response) {
                    var select = '<option value="">Pilih Kota / Kabupaten</option>';
                    $.each(response.data.rajaongkir.results, function(item,item2) {
                        // console.log(item2)
                        select += `<option value="${item2.city_id}">${item2.type + ' ' + item2.city_name}</option>` ;
                    });
                    Helper.loadingStop()
                    $('#aKotaId-'+index).html(select)
                })
                $('#aKotaId-'+index).html('').append('<option value="">Pilih Kota / Kabupaten</option>');
                $('#aDistrikId-'+index).html('').append('<option value="">Pilih Kota / Kabupaten Terlebih Dahulu</option>');
                $('#aKurir-'+index).html('').append('<option value="">Pilih Kecamatan Terlebih Dahulu</option>');
                $('#aLayanan-'+index).html('').append('<option value="">Pilih Kurir Terlebih Dahulu</option>');
            }
        }

        function getDistrik(index){
            var val = $('#aKotaId-'+index).val();
            if(val == ""){
                swal({
                    title: "Ups!",
                    text: "Silahkan Pilih Kota / Kabupaten Yang Tersedia! ",
                    type: 'error'
                });
            }else{
                Helper.loadingStart()
                ajaxRequest('GET', '/api/rajaongkir/subdistrict?city=' + val, function(response) {
                    var select = '<option value="">Pilih Kecamatan</option>';
                    $.each(response.data.rajaongkir.results, function(item,item2) {
                        select += `<option value="${item2.subdistrict_id}">${item2.subdistrict_name}</option>` ;
                    });
                    Helper.loadingStop()
                    $('#aDistrikId-'+index).html(select)
                })
                $('#aKodePos-'+index).html('').val('');
                $('#aDistrikId-'+index).html('').append('<option value="">Pilih Kota / Kabupaten Terlebih Dahulu</option>');
                $('#aKurir-'+index).html('').append('<option value="">Pilih Kecamatan Terlebih Dahulu</option>');
                $('#aLayanan-'+index).html('').append('<option value="">Pilih Kurir Terlebih Dahulu</option>');
            }
        }

        function getKurir(index, loading=true){
            var val = $('#aDistrikId-'+index).val();
            if(val == "" && loading){
                swal({
                    title: "Ups!",
                    text: "Silahkan Pilih Kecamatan Yang Tersedia! ",
                    type: 'error'
                });
            }else{
                if(loading){
                    Helper.loadingStart()
                }
                ajaxRequest('GET', '/api/rajaongkir/courier', function(response) {
                    var select = '<option value="">Pilih Kurir</option>';
                    $.each(response.data, function(item,item2) {
                        select += `<option value="${item2.code}">${item2.text}</option>` ;
                    });
                    if(loading){
                        Helper.loadingStop()
                    }
                    $('#aKurir-'+index).html(select)
                })
                $('#aKurir-'+index).html('').append('<option value="">Pilih Kecamatan Terlebih Dahulu</option>');
                $('#aLayanan-'+index).html('').append('<option value="">Pilih Kurir Terlebih Dahulu</option>');
            }
        }

        function getLayanan(index){
            var elementDistrik = 'aDistrikId-'+index;
            var elementKurir = 'aKurir-'+index;
            var val = $('#'+elementKurir).val();
            if(val == ""){
                swal({
                    title: "Ups!",
                    text: "Silahkan Pilih Kurir Yang Tersedia! ",
                    type: 'error'
                });
            }else{
                var weight = $('input[type="hidden"][name="aWeight[]"]').eq(index).val();
                Helper.loadingStart()
                ajaxGetCostRajaOngkir(function(response) {
                    data = loopingLayananRajaOngkir(response.data.rajaongkir);
                    var cost = '<option value="">Pilih Layanan Jasa</option>';
                    $.each(data, function(item,item2) {
                        console.log(item2)
                        cost += `<option value="${item2.id}">${item2.text}</option>`;
                    })
                    Helper.loadingStop()
                    $('#aLayanan-'+index).html(cost)
                },elementDistrik,elementKurir,weight )
                $('#aLayanan-'+index).html('').append('<option value="">Pilih Kurir Terlebih Dahulu</option>');
            }
        }

        function getTotalOngkir(val,index){
            var totalBerat = $('input[type="hidden"][name="aWeight[]"]').eq(index).val();
            var totalOngkir = $('input[type="hidden"][name="totalOngkir[]"]').eq(index).val(val);
            var totalSemua = total;
            totalOngkirKeselurahan = 0;
            $('input[type="hidden"][name="totalOngkir[]"]').each(function() {
                totalSemua += parseInt($(this).val());
                totalOngkirKeselurahan += parseInt($(this).val());
            });
            var totalHargaItem = parseInt(totalSemua - totalOngkirKeselurahan);
            $('#totalKeseluruhan').val(totalSemua);
            $('#totalOngkir').val(totalOngkirKeselurahan);
            $('#totalHargaItem').val(totalHargaItem);
            $('#lbltotalKeseluruhan').text(formatRupiah(totalSemua));
            $('#lbltotalHargaItem').text(formatRupiah(totalHargaItem));
            $('#lbltotalOngkir').text(formatRupiah(totalOngkirKeselurahan));
        }

        function onchangeProduct(){
            $('select[name="aProductId[]"]').on('change', function(){
                var data = $(this).data();
                var index = data['index'];
                $('input[type="text"][name="aQtyProduct[]"]').eq(index).val("");

                $('#aProvinceId-'+index).val("");
                $('#aKotaId-'+index).html('').append('<option value="">Pilih Provinsi Terlebih Dahulu</option>');
                $('#aDistrikId-'+index).html('').append('<option value="">Pilih Kota / Kabupaten Terlebih Dahulu</option>');
                $('#aKurir-'+index).html('').append('<option value="">Pilih Kecamatan Terlebih Dahulu</option>');
                $('#aLayanan-'+index).html('').append('<option value="">Pilih Kurir Terlebih Dahulu</option>');
                if($(this).val() == "Semua Produk"){
                    $('#addForm').attr('disabled', true);
                    $('input[type="text"][name="aQtyProduct[]"]').eq(0).val(totalQty);
                    $('input[type="text"][name="aQtyProduct[]"]').eq(0).attr("disabled",true);
                    $('input[type="hidden"][name="aWeight[]"]').eq(0).val(totalBerat);
                    if(idx > 1){
                        for (i = 1; i < idx; i++) {
                            deleteForm(i,false);
                        }
                    }
                    idx=1;
                }else{
                    $('input[type="text"][name="aQtyProduct[]"]').eq(index).attr("disabled",false);
                    $('#addForm').attr('disabled', false);
                  
                }
                $('input[type="text"][name="aKodePos[]"]').eq(index).attr('disabled', false);
                $('textarea[name="aAlamat[]"]').eq(index).attr('disabled', false);
                $('select[name="aProvinceId[]"]').eq(index).attr('disabled', false);
                $('select[name="aKotaId[]"]').eq(index).attr('disabled', false);
                $('select[name="aDistrikId[]"]').eq(index).attr('disabled', false);
                $('select[name="aLayanan[]"]').eq(index).attr('disabled', false);
                $('select[name="aKurir[]"]').eq(index).attr('disabled', false);
            });
        }
        $('input[type="radio"][name="kirim_ke"]').on('change', function(){
            if($(this).val() == 1){
                $('select[name="aProductId[]"]').eq(0).val("");
                $('select[name="aProductId[]"]').eq(0).attr('disabled', true);


                $('input[type="text"][name="aQtyProduct[]"]').eq(0).val();
                $('input[type="text"][name="aQtyProduct[]"]').eq(0).attr('disabled', true);

                $('input[type="text"][name="aKodePos[]"]').eq(0).val();
                $('input[type="text"][name="aKodePos[]"]').eq(0).attr('disabled', true);

                $('input[type="text"][name="aNamaLengkap[]"]').eq(0).val();
                $('input[type="text"][name="aNamaLengkap[]"]').eq(0).attr('disabled', true);

                $('input[type="text"][name="aNomerPonsel[]"]').eq(0).val();
                $('input[type="text"][name="aNomerPonsel[]"]').eq(0).attr('disabled', true);

                $('input[type="text"][name="aJenisKelamin[]"]').eq(0).val();
                $('input[type="text"][name="aJenisKelamin[]"]').eq(0).attr('disabled', true);

                $('input[type="text"][name="aNamaPengirim[]"]').eq(0).val();
                $('input[type="text"][name="aNamaPengirim[]"]').eq(0).attr('disabled', true);

                $('textarea[name="aAlamat[]"]').eq(0).val();
                $('textarea[name="aAlamat[]"]').eq(0).attr('disabled', true);

                $('select[name="aProvinceId[]"]').eq(0).val();
                $('select[name="aProvinceId[]"]').eq(0).attr('disabled', true);

                $('select[name="aKotaId[]"]').eq(0).val();
                $('select[name="aKotaId[]"]').eq(0).attr('disabled', true);

                $('select[name="aDistrikId[]"]').eq(0).val();
                $('select[name="aDistrikId[]"]').eq(0).attr('disabled', true);

                $('select[name="aLayanan[]"]').eq(0).val();
                $('select[name="aLayanan[]"]').eq(0).attr('disabled', true);

                $('select[name="aKurir[]"]').eq(0).val();
                $('select[name="aKurir[]"]').eq(0).attr('disabled', true);

                $('.btn_tambah_form').eq(0).attr('disabled', true);

                document.getElementById('addForm').style.display = 'none';

                var ongkir = parseInt($('#totalOngkir').val());
                var totalAll = parseInt($('#totalKeseluruhan').val());
                if(ongkir > 0){
                    var calculate = totalAll - ongkir;
                }else{
                    var calculate = totalAll;
                }
                $('#totalKeseluruhan').val(calculate);
                $('#totalOngkir').val(0);

                $('#lbltotalOngkir').text(formatRupiah(0));
                $('#lbltotalKeseluruhan').text(formatRupiah(calculate));
                if(idx > 1){
                    for (i = 1; i < idx; i++) {
                        document.getElementById("formAlamat-"+i ).remove();
                    }
                }
                idx=1;
            }else{
                $('select[name="aProductId[]"]').eq(0).val("");
                $('select[name="aProductId[]"]').eq(0).attr('disabled', false);

                $('input[type="text"][name="aKodePos[]"]').eq(0).val();
                $('input[type="text"][name="aKodePos[]"]').eq(0).attr('disabled', false);

                $('select[name="aProductId[]"]').eq(0).val("");
                $('select[name="aProductId[]"]').eq(0).attr('disabled', false);


                $('input[type="text"][name="aQtyProduct[]"]').eq(0).val();
                $('input[type="text"][name="aQtyProduct[]"]').eq(0).attr('disabled', false);

                $('input[type="text"][name="aNamaLengkap[]"]').eq(0).val();
                $('input[type="text"][name="aNamaLengkap[]"]').eq(0).attr('disabled', false);

                $('input[type="text"][name="aNomerPonsel[]"]').eq(0).val();
                $('input[type="text"][name="aNomerPonsel[]"]').eq(0).attr('disabled', false);

                $('input[type="text"][name="aJenisKelamin[]"]').eq(0).val();
                $('input[type="text"][name="aJenisKelamin[]"]').eq(0).attr('disabled', false);

                // $('input[type="text"][name="aNamaPengirim[]"]').eq(0).val();
                // $('input[type="text"][name="aNamaPengirim[]"]').eq(0).attr('disabled', false);

                $('textarea[name="aAlamat[]"]').eq(0).val();
                $('textarea[name="aAlamat[]"]').eq(0).attr('disabled', true);

                $('select[name="aProvinceId[]"]').eq(0).val();
                $('select[name="aProvinceId[]"]').eq(0).attr('disabled', true);

                $('select[name="aKotaId[]"]').eq(0).val();
                $('select[name="aKotaId[]"]').eq(0).attr('disabled', true);

                $('select[name="aDistrikId[]"]').eq(0).val();
                $('select[name="aDistrikId[]"]').eq(0).attr('disabled', true);

                $('select[name="aLayanan[]"]').eq(0).val();
                $('select[name="aLayanan[]"]').eq(0).attr('disabled', true);

                $('select[name="aKurir[]"]').eq(0).val();
                $('select[name="aKurir[]"]').eq(0).attr('disabled', true);

                document.getElementById('addForm').style.display = 'block';
            }
        });

        function getQty(val,index){
            var productId = $('select[name="aProductId[]"]').eq(index).val();
            var berat = 0;
            if(val > 0 || val !=""){
                var value = arr.filter(function(el) {
                    if(el.id == productId){
                        if(val > el.qty){
                            swal({
                                    title: "Ups!",
                                    text: "Jumlah Barang Tidak Sesuai",
                                    type: 'error'
                                });
                        }else{
                            berat = val * el.berat;
                            $('input[type="hidden"][name="aWeight[]"]').eq(index).val(berat);
                        }
                    }
                });
            }
        }

        function deleteForm(idx,confirm=true){
            if(confirm){
                swal({
                    title: "Alert!",
                    text: "Apakah anda yakin akan menghapus alamat "+ (parseInt(idx) + 1)+" tersebut?",
                    type: 'error',
                    showCancelButton: true,
                    confirmButtonText: 'Ya, Saya yakin',
                    cancelButtonText: 'Tidak',
                },function(result) {
                    if(result){
                        var total = $('#totalKeseluruhan').val();
                        var totalOngkir = $('#totalOngkir').val();
                        

                        var ongkir = $('input[type="hidden"][name="totalOngkir[]"]').eq(idx).val();
                        if(ongkir > 0 && ongkir !=""){
                            var calculate = total - ongkir;
                            var calculateOngkir = totalOngkir - ongkir;
                            $('#totalKeseluruhan').val(calculate);
                            $('#totalOngkir').val(calculateOngkir);

                            $('#lbltotalOngkir').text(formatRupiah(calculateOngkir));
                            $('#lbltotalKeseluruhan').text(formatRupiah(calculate));
                        }

                        document.getElementById("formAlamat-"+idx).remove();
                    }
                });
            }else{
                var total = $('#totalKeseluruhan').val();
                var totalOngkir = $('#totalOngkir').val();
                

                var ongkir = $('input[type="hidden"][name="totalOngkir[]"]').eq(idx).val();
                if(ongkir > 0 && ongkir !=""){
                    var calculate = total - ongkir;
                    var calculateOngkir = totalOngkir - ongkir;
                    $('#totalKeseluruhan').val(calculate);
                    $('#totalOngkir').val(calculateOngkir);

                    $('#lbltotalOngkir').text(formatRupiah(calculateOngkir));
                    $('#lbltotalKeseluruhan').text(formatRupiah(calculate));
                }

                document.getElementById("formAlamat-"+idx).remove();
            }
            
        }

        function newForm(){
            var html = '';
            html+='<div class="card-body" id="formAlamat-'+idx+'">'+
                            '<div class="accordion" id="accordionAlamat">'+
                                '<div class="card">'+
                                    '<div class="card-header" id="alamat'+idx+'">'+
                                        '<h2 class="mb-0" data-toggle="collapse" data-target="#collapse'+idx+'" aria-expanded="true" aria-controls="collapse'+idx+'">'+
                                            '<button style="color:black" class="btn btn-link" type="button">'+
                                                '<i class="fa fa-chevron-up"></i> Alamat Pengiriman '+ (parseInt(idx)+1) +
                                                '| <i class="fa fa-trash" onclick="deleteForm('+idx+')"></i>' +
                                            '</button>'+
                                        '</h2>'+
                                    '</div>'+
                                    '<div id="collapse'+idx+'" class="collapse" aria-labelledby="alamat'+idx+'" data-parent="#accordionAlamat">'+
                                        '<div class="card-body">'+
                                            '<div class="row">'+
                                                '<div class="col-sm-6">'+
                                                    '<div class="form-group">'+
                                                        '<label>Produk</label>'+
                                                        '<select class="form-control aProductId" name="aProductId[]" data-index="'+idx+'">'+
                                                            '<option value=""> Pilih Produk </option>'+
                                                                    option+
                                                        '</select>'+
                                                        '<span class="invalid-feedback valaProductId" style="display:block;"></span>'+
                                                    '</div>'+
                                                '</div>'+
                                                '<div class="col-sm-6">'+
                                                    '<div class="form-group">'+
                                                        '<label>QTY</label>'+
                                                        '<input type="text" name="aQtyProduct[]" class="form-control aQtyProduct" placeholder="QTY ..." data-index="'+idx+'" onchange="getQty(this.value,'+idx+')">'+
                                                        '<span class="invalid-feedback valqty" style="display:block;"></span>'+
                                                    '</div>'+
                                                '</div>'+
                                            '</div>'+
                                            '<div class="row">'+
                                                '<div class="col-sm-6">'+
                                                    '<div class="form-group">'+
                                                        '<label>Nama Penerima</label>'+
                                                        '<input type="text" name="aNamaLengkap[]" class="form-control aNamaPengirim" placeholder="Nama Penerima ..." data-index="'+idx+'">'+
                                                        '<span class="invalid-feedback valNamaPengirim" style="display:block;"></span>'+
                                                    '</div>'+
                                                    // '<div class="form-group">'+
                                                    //     '<label>Nama Pengirim</label>'+
                                                    //     '<input type="text" name="aNamaLengkap[]" class="form-control aNamaLengkap" placeholder="Nama Pengirim ..." data-index="'+idx+'">'+
                                                    //     '<span class="invalid-feedback valaNamaLengkap" style="display:block;"></span>'+
                                                    // '</div>'+
                                                '</div>'+
                                                '<div class="col-sm-6">'+
                                                    '<div class="form-group">'+
                                                        '<label>Nomor Ponsel Penerima</label>'+
                                                        '<input type="text" name="aNomerPonsel[]" id="phone'+idx+'" class="form-control aNomerPonsel" placeholder="Nomor Ponsel / Whatsapp ..." data-index="'+idx+'">'+
                                                        '<span class="invalid-feedback valaNomerPonsel" style="display:block;"></span>'+
                                                    '</div>'+
                                                '</div>'+
                                            '</div>'+
                                            // '<div class="row">'+
                                            //     '<div class="col-sm-12">'+
                                            //         '<div class="form-group">'+
                                            //             '<label>Nama Penerima</label>'+
                                            //             '<input type="text" name="aNamaLengkap[]" class="form-control aNamaPengirim" placeholder="Nama Penerima ..." data-index="'+idx+'">'+
                                            //             '<span class="invalid-feedback valNamaPengirim" style="display:block;"></span>'+
                                            //         '</div>'+
                                            //     '</div>'+
                                            // '</div>'+
                                            '<div class="row">'+
                                                '<div class="col-sm-6" id="provHide">'+
                                                    '<div class="form-group">'+
                                                        '<label>Provinsi</label>'+
                                                        '<select class="form-control aProvinceId" name="aProvinceId[]" style="width:100%" data-index="'+idx+'" id="aProvinceId-'+idx+'" onchange="getKota('+idx+')">'+
                                                            '<option value="">Pilih Provinsi</option>'+
                                                        '</select>'+
                                                        '<span class="invalid-feedback valaProvinceId" style="display:block;"></span>'+
                                                    '</div>'+
                                                '</div>'+
                                                '<div class="col-sm-6" id="cityHide">'+
                                                    '<div class="form-group">'+
                                                        '<label>Kota / Kabupaten</label>'+
                                                        '<select class="form-control aKotaId" name="aKotaId[]" style="width:100%" data-index="'+idx+'" id="aKotaId-'+idx+'" onchange="getDistrik('+idx+')">'+
                                                            '<option value="">Pilih Provinsi Terlebih dahulu</option>'+
                                                        '</select>'+
                                                        '<span class="invalid-feedback valaKotaId" style="display:block;"></span>'+
                                                    '</div>'+
                                                '</div>'+
                                            '</div>'+
                                            '<div class="row">'+
                                                '<div class="col-sm-6" id="cityHide">'+
                                                    '<div class="form-group">'+
                                                        '<label>Kecamatan</label>'+
                                                        '<select class="form-control aDistrikId" name="aDistrikId[]" style="width:100%" data-index="'+idx+'" id="aDistrikId-'+idx+'" onchange="getKurir('+idx+')">'+
                                                            '<option value="">Pilih Kota / Kabupaten Terlebih Dahulu</option>'+
                                                        '</select>'+
                                                        '<span class="invalid-feedback valaDistrikId" style="display:block;"></span>'+
                                                    '</div>'+
                                                '</div>'+
                                                '<div class="col-sm-6">'+
                                                    '<div class="form-group">'+
                                                        '<label>Kode Pos</label>'+
                                                        '<input type="text" name="aKodePos[]" id="kodepos'+idx+'" class="form-control aKodePos" placeholder="Kode Pos" data-index="'+idx+'">'+
                                                        '<span class="invalid-feedback valaKodePos" style="display:block;"></span>'+
                                                    '</div>'+
                                                '</div>'+
                                            '</div>'+
                                            '<div class="row">'+
                                                '<div class="col-sm-6">'+
                                                    '<div class="form-group">'+
                                                        '<label>Kurir</label>'+
                                                        '<select class="form-control aKurir" name="aKurir[]" style="width:100%" data-index="'+idx+'" id="aKurir-'+idx+'" onchange="getLayanan('+idx+')">'+
                                                            '<option value="">Pilih Kecamatan Terlebih Dahulu</option>'+
                                                        '</select>'+
                                                        '<span class="invalid-feedback valaKurir" style="display:block;"></span>'+
                                                    '</div>'+
                                                '</div>'+
                                                '<div class="col-sm-6">'+
                                                    '<div class="form-group">'+
                                                        '<label>Pilih Layanan</label>'+
                                                        '<select name="aLayanan[]" data-index="'+idx+'" id="aLayanan-'+idx+'" class="form-control aLayanan" onchange="getTotalOngkir(this.value,'+idx+')">'+
                                                            '<option value="">Pilih Kurir Terlebih dahulu</option>'+
                                                        '</select>'+
                                                        '<span class="invalid-feedback valaLayanan" style="display:block;"></span>'+
                                                    '</div>'+
                                                '</div>'+
                                                '<input type="hidden" value="0" class="form-control aWeight" name="aWeight[]" data-index="'+idx+'" id="aWeight-'+idx+'">'+
                                                '<input type="hidden" value="0" name="totalOngkir[]" data-index="'+idx+'">'+
                                            '</div>'+
                                            '<div class="row">'+
                                                '<div class="col-sm-12">'+
                                                    '<div class="form-group">'+
                                                        '<label>Alamat Lengkap</label>'+
                                                        '<textarea name="aAlamat[]" class="form-control aAlamat" cols="31" rows="11" data-index="'+idx+'"></textarea>'+
                                                        '<span class="invalid-feedback valaAlamat" style="display:block;"></span>'+
                                                    '</div>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>';
            if(parseInt(idx) + 1 > totalQty){
                swal({
                    title: "Ups!",
                    text: "Tidak dapat menambahkan alamat.",
                    type: 'error'
                });
            }else{
                idx +=1;
                $('#FormAlamat').append(html);
                Helper.loadingStart()
                ajaxRequest('GET', '/api/rajaongkir/provinsi', function(response) {
                    var select = '<option value="">Pilih Provinsi</option>';
                    $.each(response.data.rajaongkir.results, function(item,item2) {
                        // console.log(item2)
                        select += `<option value="${item2.province_id}">${item2.province}</option>` ;
                    });
                    Helper.loadingStop()
                    $('.aProvinceId:last').html(select)
                })

                $(".collapse.show").each(function(){
                    $(this).prev(".card-header").find(".fa").addClass("fa-chevron-down").removeClass("fa-chevron-up");
                });
                
                // Toggle plus minus icon on show hide of collapse element
                $(".collapse").on('show.bs.collapse', function(){
                    $(this).prev(".card-header").find(".fa").removeClass("fa-chevron-up").addClass("fa-chevron-down");
                }).on('hide.bs.collapse', function(){
                    $(this).prev(".card-header").find(".fa").removeClass("fa-chevron-down").addClass("fa-chevron-up");
                });

                $('.aKodePos:last').attr('disabled', true);
                $('.aProvinceId:last').attr('disabled', true);
                $('.aAlamat:last').attr('disabled', true);
                $('.aKotaId:last').attr('disabled', true);
                $('.aDistrikId:last').attr('disabled', true);
                $('.aLayanan:last').attr('disabled', true);
                $('.aKurir:last').attr('disabled', true);

                onchangeProduct();
                onlyNumber('input[type="text"][name="aQtyProduct[]"]');
                onlyNumber('input[type="text"][name="aNomerPonsel[]"]');
                onlyNumber('input[type="text"][name="aKodePos[]"]');
            }
        }

        $('#checkout').on('click', function(){
            var defaultIndex = 0;
            var arrFinal = [];
            var payment_method = $('input[type="radio"][name="payment_method"]:checked').val();
            var totalQtyAlamat = 0;
            var suksesValidasi = true;

            $("select[name='aProductId[]']").each(function() {
                var productId=$('select[name="aProductId[]"]').eq(defaultIndex).val();
                var qty=$('input[type="text"][name="aQtyProduct[]"]').eq(defaultIndex).val();
                var namaLengkap=$('input[type="text"][name="aNamaLengkap[]"]').eq(defaultIndex).val();
                var nomorPonsel=$('input[type="text"][name="aNomerPonsel[]"]').eq(defaultIndex).val();
                var jenisKelamin=$('input[type="text"][name="aJenisKelamin[]"]').eq(defaultIndex).val();
                // var namaPengirim=$('input[type="text"][name="aNamaPengirim[]"]').eq(defaultIndex).val();
                var provinsi = $('select[name="aProvinceId[]"]').eq(defaultIndex).val();
                var kota = $('select[name="aKotaId[]"]').eq(defaultIndex).val();
                var distrik = $('select[name="aDistrikId[]"]').eq(defaultIndex).val();
                var kurir=$('select[name="aKurir[]"]').eq(defaultIndex).val();
                var layanan=$('select[name="aLayanan[]"]').eq(defaultIndex).val();
                var alamat=$('textarea[name="aAlamat[]"]').eq(defaultIndex).val();
                var totalOngkir=$('input[type="hidden"][name="totalOngkir[]"]').eq(defaultIndex).val();
                var kodePos = $('input[type="text"][name="aKodePos[]"]').eq(defaultIndex).val();
                
                if(qty > 0){
                    totalQtyAlamat += parseInt(qty);
                }
                if($('input[type="radio"][name="kirim_ke"]:checked').val() == 0){
                    if(productId == ''){
                        $('span[class="invalid-feedback valaProductId"]').eq(defaultIndex).text("Silahkan Pilih Produk");
                        suksesValidasi = false;
                    }else{
                        $('span[class="invalid-feedback valaProductId"]').eq(defaultIndex).text("");
                    }

                    

                    if(qty == ''){
                        $('span[class="invalid-feedback valqty"]').eq(defaultIndex).text("Masukan jumlah sesuai dengan yang diatas.");
                        suksesValidasi = false;
                    }else{
                        $('span[class="invalid-feedback valqty"]').eq(defaultIndex).text("");
                    }

                    if(namaLengkap == ""){
                        $('span[class="invalid-feedback valaNamaLengkap"]').eq(defaultIndex).text("Silahkan lengkapi isian tersebut.");
                        suksesValidasi = false;
                    }else{
                        $('span[class="invalid-feedback valaNamaLengkap"]').eq(defaultIndex).text("");
                    }

                    if(nomorPonsel == ""){
                        $('span[class="invalid-feedback valaNomerPonsel"]').eq(defaultIndex).text("Silahkan lengkapi isian tersebut.");
                        suksesValidasi = false;
                    }else{
                        $('span[class="invalid-feedback valaNomerPonsel"]').eq(defaultIndex).text("");
                    }

                    if(jenisKelamin == ""){
                        $('span[class="invalid-feedback valaJenisKelamin"]').eq(defaultIndex).text("Silahkan lengkapi isian tersebut.");
                        suksesValidasi = false;
                    }else{
                        $('span[class="invalid-feedback valaJenisKelamin"]').eq(defaultIndex).text("");
                    }

                    // if(namaPengirim == ""){
                    //     $('span[class="invalid-feedback valNamaPengirim"]').eq(defaultIndex).text("Silahkan lengkapi isian tersebut.");
                    //     suksesValidasi = false;
                    // }else{
                    //     $('span[class="invalid-feedback valNamaPengirim"]').eq(defaultIndex).text("");
                    // }

                    if(provinsi == ""){
                        $('span[class="invalid-feedback valaProvinceId"]').eq(defaultIndex).text("Silahkan lengkapi isian tersebut.");
                        suksesValidasi = false;
                    }else{
                        $('span[class="invalid-feedback valaProvinceId"]').eq(defaultIndex).text("");
                    }

                    if(kota == ""){
                        $('span[class="invalid-feedback valaKotaId"]').eq(defaultIndex).text("Silahkan lengkapi isian tersebut.");
                        suksesValidasi = false;
                    }else{
                        $('span[class="invalid-feedback valaKotaId"]').eq(defaultIndex).text("");
                    }

                    if(distrik == ""){
                        $('span[class="invalid-feedback valaDistrikId"]').eq(defaultIndex).text("Silahkan lengkapi isian tersebut.");
                        suksesValidasi = false;
                    }else{
                        $('span[class="invalid-feedback valaDistrikId"]').eq(defaultIndex).text("");
                    }

                    if(kurir == ""){
                        $('span[class="invalid-feedback valaKurir"]').eq(defaultIndex).text("Silahkan lengkapi isian tersebut.");
                        suksesValidasi = false;
                    }else{
                        $('span[class="invalid-feedback valaKurir"]').eq(defaultIndex).text("");
                    }

                    if(layanan == ""){
                        $('span[class="invalid-feedback valaLayanan"]').eq(defaultIndex).text("Silahkan lengkapi isian tersebut.");
                        suksesValidasi = false;
                    }else{
                        $('span[class="invalid-feedback valaLayanan"]').eq(defaultIndex).text("");
                    }

                    if(alamat == ""){
                        $('span[class="invalid-feedback valaAlamat"]').eq(defaultIndex).text("Silahkan lengkapi isian tersebut.");
                        suksesValidasi = false;
                    }else{
                        $('span[class="invalid-feedback valaAlamat"]').eq(defaultIndex).text("");
                    }

                    if(totalOngkir == ""){
                        $('span[class="invalid-feedback qtyProductId"]').eq(defaultIndex).text("Silahkan lengkapi isian tersebut.");
                        suksesValidasi = false;
                    }else{
                        $('span[class="invalid-feedback qtyProductId"]').eq(defaultIndex).text("");
                    }
                }

                defaultIndex++;
            });

            if(suksesValidasi == true){
                if(totalQtyAlamat > totalQty){
                    swal({
                        title: "Ups!",
                        text: "Jumlah produk tidak sesuai.",
                        type: 'error'
                    });
                }else if(totalQtyAlamat <  totalQty && $('input[type="radio"][name="kirim_ke"]:checked').val() == 0){
                    swal({
                        title: "Ups!",
                        text: "Pastikan Semua Produk Mempunyai Alamat.",
                        type: 'error'
                    });
                }else if(typeof payment_method == 'undefined' || payment_method == ''){
                    swal({
                        title: "Ups!",
                        text: " Silahkan Pilih Metode Pembayaran",
                        type: 'error'
                    });
                }else{
                    storeData();
                }
            }
            
        })

        function storeData(){
            var defaultIndex = 0;
            var arrFinal = [];
            var payment_method = $('input[type="radio"][name="payment_method"]:checked').val();
            let totalHarga = $('#totalHargaItem').val();
            let totalSeluruhOngkir = $('#totalOngkir').val();
            if($('input[type="radio"][name="kirim_ke"]:checked').val() === "1" ){
                var totalKeseluruhan = $('#totalHargaItem').val();
            }else{
                var totalKeseluruhan = $('#totalKeseluruhan').val();
            }
            
            $("select[name='aProductId[]']").each(function() {
                var productId=$('select[name="aProductId[]"]').eq(defaultIndex).val();
                var qty=$('input[type="text"][name="aQtyProduct[]"]').eq(defaultIndex).val();
                var namaLengkap=$('input[type="text"][name="aNamaLengkap[]"]').eq(defaultIndex).val();
                var nomorPonsel=$('input[type="text"][name="aNomerPonsel[]"]').eq(defaultIndex).val();
                var jenisKelamin=$('input[type="text"][name="aJenisKelamin[]"]').eq(defaultIndex).val();
                // var namaPengirim=$('input[type="text"][name="aNamaPengirim[]"]').eq(defaultIndex).val();
                var alamat=$('textarea[name="aAlamat[]"]').eq(defaultIndex).val();
                var kurir=$('select[name="aKurir[]"]').eq(defaultIndex).val() + ' - ' + $('select[name="aLayanan[]"] option:selected').eq(defaultIndex).text().split("-")[0];
                var totalOngkir=$('input[type="hidden"][name="totalOngkir[]"]').eq(defaultIndex).val();
                var totalBerat = $('input[type="hidden"][name="aWeight[]"]').eq(defaultIndex).val();

                var kode_pos = $('input[type="text"][name="aKodePos[]"]').eq(defaultIndex).val();
                var province_id = $('select[name="aProvinceId[]"] option:selected').eq(defaultIndex).text();
                var kota_id = $('select[name="aKotaId[]"] option:selected').eq(defaultIndex).text();
                var district_id = $('select[name="aDistrikId[]"] option:selected').eq(defaultIndex).text();

                arrFinal.push({
                    productId,
                    qty,
                    namaLengkap,
                    nomorPonsel,
                    jenisKelamin,
                    // namaPengirim,
                    alamat,
                    kurir,
                    totalOngkir,
                    province_id,
                    kota_id,
                    district_id,
                    kode_pos,
                    totalBerat
                });
                defaultIndex++;
            });

            var dataInput = {
                items:arr,
                detailAlamat:arrFinal,
                kirim_ke:$('input[type="radio"][name="kirim_ke"]:checked').val(),
                payment_method,
                totalHarga,
                'totalOngkir':totalSeluruhOngkir,
                totalKeseluruhan,
                statusPayment,
            }

            Helper.loadingStart()
            jQuery.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "",
                type: 'POST',
                dataType: 'json',
                data: dataInput,
                success: function(data) {
                    Helper.loadingStop()
                    if(payment_method == 0){
                        snap.pay(data.token,{
                            onSuccess: function(result){
                                console.log("Sukses");
                                // storeData(data.trxId);
                                swal({
                                    title: "Sukses!",
                                    text: "Transaksi anda berhasil, silahkan segera lakukan pembayaran!",
                                    type: 'success'
                                }, function() {
                                    window.location.href = '/customer/status-order/pembayaran/' + data.orderId;
                                });
                            },
                            onPending: function(result){
                                console.log("Pending");
                                // storeData(data.trxId);
                                swal({
                                    title: "Sukses!",
                                    text: "Transaksi anda berhasil, silahkan segera lakukan pembayaran!",
                                    type: 'success'
                                }, function() {
                                    window.location.href = '/customer/status-order/pembayaran/' + data.orderId;
                                });
                            },
                            onError: function(result){
                                swal({
                                    title: "Ups!",
                                    text: result.status_message,
                                    type: 'error'
                                });
                            },
                            onClose: function(){
                                console.log('customer closed the popup without finishing the payment');
                            }
                        });
                    }else{
                        swal({
                            title: "Sukses!",
                            text: "Transaksi anda berhasil, silahkan segera lakukan pembayaran!",
                            type: 'success'
                        }, function() {
                            window.location.href = '/customer/status-order/pembayaran/' + data.orderId;
                        });
                    }
                },
                error: function(a, v, c) {
                    Helper.loadingStop()
                    var msg = JSON.parse(a.responseText);
                    swal({
                        title: "Ups!",
                        text: msg.message,
                        type: 'error'
                    });
                }
            });
        }
    </script>
@endsection