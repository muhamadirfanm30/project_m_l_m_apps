@extends('home')
@section('content')
@include('customer.order-product._styleDetailProduct')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('success') }}</strong>
                        </div>
                    @endif
                    @if(session()->has('error'))
                        <div class="alert alert-danger alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('error') }}</strong>
                        </div>
                    @endif
                    <div class="card card-outline card-info">

                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <h3 class="card-title">
                                                Detail Produk
                                            </h3>
                                        </div>
                                        <div class="col-md-4">
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                                <div class="container-fliud"><br>
                                    <div class="wrapper row">
                                        <div class="preview col-md-4">
                                            <div class="preview-pic tab-content">
                                                <div class="tab-pane active" id="pic-1"><img src="{{url('storage/product-image/'.$showProduct->image_produk)}}" /></div>
                                            </div>
                                        </div>
                                        <div class="details col-md-8">
                                            <h3 class="product-title">{{$showProduct->nama_produk}}</h3>
                                            <p class="product-description">Stock : <strong>{{$showProduct->stok}}</strong></p>
                                            <div class="section" style="padding-bottom:20px;">
                                                <h6 class="title-attr"><small>QTY</small></h6>                    
                                                <div>
                                                    <div class="btn-minus"><span class="fa fa-minus"></span></div>
                                                    <input value="1" id="qty" name="qty" style="width:50px; text-align: center;">
                                                    <div class="btn-plus"><span class="fa fa-plus"></span></div>
                                                </div>
                                            </div> 
                                            @if (!empty($showProduct->harga_promo))
                                                <div class="price">Rp. {{number_format($showProduct->harga_promo)}}
                                                    <span>Rp. {{number_format($showProduct->harga)}}</span>
                                                </div>
                                            @else
                                                <div class="price">
                                                    Rp. {{number_format($showProduct->harga)}}
                                                </div>
                                            @endif
                                            <div class="action">
                                                <button class="add-to-cart btn btn-default btn-sm" type="button" id="checkoutBtn">Checkout</button>
                                                <a id="btnAddCart" class="like btn btn-default btn-sm"><span class="fa fa-shopping-cart"></span></a>
                                            </div>
                                        </div>
                                    </div><br>
                                </div>
                                <div class="product-info-tabs">
                                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="description-tab" data-toggle="tab" href="#description" role="tab" aria-controls="description" aria-selected="true">Description</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content" id="myTabContent">
                                        <div class="tab-pane fade show active" id="description" role="tabpanel" aria-labelledby="description-tab">
                                            {!!$showProduct->deskripsi_produk!!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>
        $(document).ready(function(){
            //-- Click on detail
            $("ul.menu-items > li").on("click",function(){
                $("ul.menu-items > li").removeClass("active");
                $(this).addClass("active");
            })

            $(".attr,.attr2").on("click",function(){
                var clase = $(this).attr("class");

                $("." + clase).removeClass("active");
                $(this).addClass("active");
            })

            //-- Click on QUANTITY
            $(".btn-minus").on("click",function(){
                var now = $(".section > div > input").val();
                if ($.isNumeric(now)){
                    if (parseInt(now) -1 > 0){ now--;}
                    $(".section > div > input").val(now);
                }else{
                    $(".section > div > input").val("1");
                }
            })            
            $(".btn-plus").on("click",function(){
                var now = $(".section > div > input").val();
                if ($.isNumeric(now)){
                    $(".section > div > input").val(parseInt(now)+1);
                }else{
                    $(".section > div > input").val("1");
                }
            })                        

            $('#checkoutBtn').on("click",function(){
                window.location.href="/customer/order-produk/checkout/{{$showProduct->id}}/"+$('#qty').val();
            })

            $('#btnAddCart').on("click",function(){
                window.location.href="/customer/order-produk/add-to-cart/{{$showProduct->id}}/"+$('#qty').val();
            })
        }) 
    </script>
@endsection
