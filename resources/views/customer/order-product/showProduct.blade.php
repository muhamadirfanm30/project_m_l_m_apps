@extends('home')
@section('content')
    @include('customer.order-product._styleDetailProduct')

    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('success') }}</strong> <a href="{{url('customer/order-produk/cart')}}"> View Shopping Cart</a>
                        </div>
                    @endif
                    @if(session()->has('error'))
                        <div class="alert alert-danger alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('error') }}</strong>
                        </div>
                    @endif
                    <div class="card card-primary card-outline card-tabs">
                        <div class="card-header p-0 pt-1 border-bottom-0">
                            <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="custom-tabs-three-home-tab" data-toggle="pill" href="#custom-tabs-three-home" role="tab" aria-controls="custom-tabs-three-home" aria-selected="true">List Produk</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="custom-tabs-three-profile-tab" data-toggle="pill" href="#custom-tabs-three-profile" role="tab" aria-controls="custom-tabs-three-profile" aria-selected="false">Tutorial Order Produk</a>
                                </li>
                            </ul>
                        </div>
                        <div class="card-body">
                            <div class="tab-content" id="custom-tabs-three-tabContent">
                                <div class="tab-pane fade show active" id="custom-tabs-three-home" role="tabpanel" aria-labelledby="custom-tabs-three-home-tab">
                                    <div class="card card-outline card-info">
                                        <div class="card-header">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        {{-- <div class="col-md-5">
                                                            <div class="row">
                                                                <h3 class="card-title">
                                                                    List produk {{$sort}}
                                                                </h3>
                                                            </div>
                                                        </div> --}}
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                {{-- <div class="col-md-2">
                                                                    Sort :
                                                                </div> --}}
                                                                <div class="col-md-12" style="margin-bottom:10px">
                                                                    <form id="sort-form" action="{{ route('cs-order-produk.show') }}" method="POST">
                                                                        @csrf
                                                                        <select id="sortProduk" name="sort" class="form-control" onchange="event.preventDefault();document.getElementById('sort-form').submit();">
                                                                            <option {{ $sort == "terbaru" ? "selected":"" }} value="terbaru">Urutkan Produk Yang Terbaru</option>
                                                                            <option {{ $sort == "termahal" ? "selected":"" }} value="termahal">Urutkan Produk Dari Yang Termahal</option>
                                                                            <option {{ $sort == "termurah" ? "selected":"" }} value="termurah">Urutkan Produk Dari Yang Termurah</option>
                                                                        </select>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                {{-- <div class="col-md-4">
                                                                    Kategori :
                                                                </div> --}}
                                                                <div class="col-md-12">
                                                                    <form id="kategori-form" action="{{ route('cs-order-produk.show') }}" method="POST">
                                                                        @csrf
                                                                        <select name="kategori" id="kategoriProduk" class="form-control" onchange="event.preventDefault();document.getElementById('kategori-form').submit();">
                                                                            <option value="">Semua Produk</option>
                                                                            @foreach($listKategori as $kt)
                                                                                <option value="{{ $kt->id }}"  {{ $kategori == $kt->id ? "selected":"" }}>{{ $kt->name }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <!-- /.card-header -->
                                        <div class="card-body">
                                            <div class="row">
                                                    @foreach($showProduct as $k => $product_list)
                                                        @php
                                                            $id = 0;
                                                            $id = $product_list->id;
                                                        @endphp
                                                        <div class="col-md-3 col-sm-6" style="max-width:280px">
                                                            <div class="card">
                                                                <div class="card-header">
                                                                    <center><a href="{{url('customer/order-produk/detail/'.$product_list->id)}}" class="card-title">{{$product_list->nama_produk}}</a> </center>
                                                                </div>
                                                                <div class="card-body txt-center">
                                                                    <a href="{{url('customer/order-produk/detail/'.$product_list->id)}}">
                                                                        <img src="{{url('storage/product-image/'.$product_list->image_produk)}}" alt="" class="img-thumbnail" style="width:250px; height:200px; margin-bottom:10px">
                                                                    </a>
                                                                    <div class="col-md-12">
                                                                        <h6 class="title-attr"><small>QTY</small></h6>  
                                                                        <div class="row">
                                                                            <div class="col-xs-4 col-md-2"></div>
                                                                            <div class="col-xs-4 col-md-8">
                                                                                {{-- <div class="input-group" style="padding-bottom:20px;" data-section="{{ $product_list->id }}">                  
                                                                                    <div>
                                                                                        <div class="btn-minus minuss" data-id="{{$product_list->id}}">
                                                                                            <span class="fa fa-minus"></span>
                                                                                        </div>
                                                                                        <input type="text" value="1" class="input{{$product_list->id}}" name="qty" id="qty-{{$product_list->id}}" style="width:70px; text-align: center; ">
                                                                                        <div class="btn-plus pluss" data-id="{{$product_list->id}}">
                                                                                            <span class="fa fa-plus"></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div> --}}
                                                                                <div class="input-group" style="padding-bottom:10px;" data-section="{{ $product_list->id }}">
                                                                                    <span class="input-group-btn">
                                                                                        <button type="button" class="btn btn-danger minuss"  data-type="minus" data-id="{{$product_list->id}}">
                                                                                            <span class="fa fa-minus"></span>
                                                                                        </button>
                                                                                    </span>
                                                                                    <input type="text" value="1" class=" form-control input{{$product_list->id}}" name="qty" id="qty-{{$product_list->id}}" style="text-align: center; ">
                                                                                    <span class="input-group-btn">
                                                                                        <button type="button" class="btn btn-success pluss" data-type="plus" data-id="{{$product_list->id}}">
                                                                                            <span class="fa fa-plus"></span>
                                                                                        </button>
                                                                                    </span>
                                                                                </div>
                                                                                <style>
                                                                                    .center{
                                                                                        width: 150px;
                                                                                        margin: 40px auto;
                                                                                        
                                                                                    }
                                                                                </style>
                                                                            </div>
                                                                            <div class="col-xs-4 col-md-2"></div>
                                                                        </div>
                                                                        <center><strong>Rp.{{ number_format($product_list->harga) }}</strong></center>
                                                                    </div>
                                                                    <button class="add-to-cart btn btn-warning btn-sm" type="button" data-id="{{$product_list->id}}" data-qty="{{$product_list->stok}}" data-produk-name="{{$product_list->nama_produk}}"><i class="fa fa-shopping-cart" style="color:white"> Add To Cart</i></button>
                                                                    <!-- <a href="{{ url('customer/order-produk/add-to-cart/'.$product_list->id) }}" class="btn btn-warning btn-sm"><i class="fa fa-shopping-cart" style="color:white"> Add To Cart</i></a> -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @endforeach
                                            </div><br>
                                            {!! $showProduct->render() !!} 
                                        </div>
                                        <div class="card-footer">
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="custom-tabs-three-profile" role="tabpanel" aria-labelledby="custom-tabs-three-profile-tab">
                                    {{-- <div class="content-wrapper"><br> --}}
                                        <section class="content">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="card card-outline card-info">
                                
                                                        {{-- <div class="card-header">
                                                            <h3 class="card-title">
                                                                News & Update
                                                            </h3>
                                                        </div> --}}
                                                        <!-- /.card-header -->
                                                        <div class="card-body">
                                                            <div class="col-md-12">
                                                                <h6 class="card-title">
                                                                    <h3><strong><center>{{$getJudul->value}}</center></strong></h3>
                                                                </h6>
                                                                <div class="row">
                                                                    <div class="col-md-3"></div>
                                                                    <div class="col-md-6">
                                                                        <div class="embed-responsive embed-responsive-16by9" style="width:auto">
                                                                            <iframe class="embed-responsive-item" src="{{$getVidio->value}}" allowfullscreen></iframe>
                                                                        </div><br>
                                                                    </div>
                                                                    <div class="col-md-3"></div>
                                                                </div><br><br>
                                                                <h6 class="card-title">
                                                                    <center>
                                                                        {!! $getDesc->value !!}
                                                                    </center>
                                                                </h6>
                                                            </div>
                                                        </div>
                                                        <div class="card-footer">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    {{-- </div> --}}
                                </div>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                    
                </div>
            </div>
        </section>
    </div>
    
    @section('javascript')
        @parent
        <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
        <script>
           

            $(document).ready(function(){
                $('.add-to-cart').on("click",function(){
                    id = $(this).attr('data-id');
                    stok = $(this).attr('data-qty');
                    nama_produk = $(this).attr('data-produk-name');
                    qty = $("#qty-"+id).val();
                    console.log('stok', stok)
                    if(parseInt(qty) > parseInt(stok)){
                        // alert('stok lebih')
                        swal({
                            title: "Ups!",
                            text: "Sisa stok Produk" + nama_produk + ":" + stok,
                            type: 'error'
                        });
                    }else{
                        // alert('stok bisa')
                        window.location.href="/customer/order-produk/add-to-cart/"+id+'/'+$('#qty-'+id).val();
                    }
                    
                });
                //-- Click on detail
                $("ul.menu-items > li").on("click",function(){
                    $("ul.menu-items > li").removeClass("active");
                    $(this).addClass("active");
                })

                $(".attr,.attr2").on("click",function(){
                    var clase = $(this).attr("class");

                    $("." + clase).removeClass("active");
                    $(this).addClass("active");
                })

                //-- Click on QUANTITY
                clickId = $(".input-group").attr('data-section');
                $(".minuss").on("click",function(){
                    id = $(this).attr('data-id')
                    var now = $(".input-group > .input"+id).val();
                    if ($.isNumeric(now)){
                        if(now > 1){
                            $(".input-group > .input"+id).val(parseInt(now) - 1);
                        }
                    }else{
                        $(".input-group > input"+id).val(1);
                    }
                })           
                
                $(".pluss").on("click",function(){
                    id = $(this).attr('data-id')
                    var now = $(".input-group > .input"+id).val();
                    if ($.isNumeric(now)){
                        $(".input-group > .input"+id).val(parseInt(now) + 1);
                    }else{
                        $(".input-group > input"+id).val(1);
                    }
                })      
            }) 
        </script>
    @endsection
@endsection
