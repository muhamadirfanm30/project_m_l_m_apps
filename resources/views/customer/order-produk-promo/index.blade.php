@extends('home')
@section('content')
    @include('customer.order-product._styleDetailProduct')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('success') }}</strong> <a href="{{url('customer/order-produk/cart')}}"> View Shopping Cart</a>
                        </div>
                    @endif
                    <div class="card  card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <h3 class="card-title">
                                                Promo Terbaru
                                            </h3>
                                        </div>
                                        <div class="col-md-4"></div>
                                    </div><hr>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    @foreach ($showProduct as $k => $product_list)
                                        @php
                                            $id = 0;
                                            $id = $product_list->id;
                                        @endphp
                                        <div class="col-md-3" style="max-width: 280px">
                                            <div class="card">
                                                <div class="card-header">
                                                    <a href="{{url('customer/order-produk/detail/'.$product_list->id)}}" class="card-title">{{$product_list->nama_produk}}</a>
                                                </div>
                                                
                                                <div class="card-body txt-center">
                                                    <a href="{{url('customer/order-produk/detail/'.$product_list->id)}}">
                                                        <img src="{{url('storage/product-image/'.$product_list->image_produk)}}" class="img-thumbnail" style="width:250px; height:200px; margin-bottom:10px">
                                                    </a>
                                                    @if (!empty($product_list->expired_at) )
                                                        <center><strong>Berakhir : </strong><span class="badge badge-pill badge-danger" id="labelTimer{{$product_list->id}}"></span></center>
                                                    @else 
                                                        <br><br>
                                                    @endif
                                                    <div class="col-md-12">
                                                        <h6 class="title-attr"><small>QTY</small></h6>
                                                        <div class="row">
                                                            <div class="col-md-2"></div>
                                                            <div class="col-md-8">
                                                                {{-- <div class="section" style="padding-bottom:20px;" data-section="{{ $product_list->id }}">                    
                                                                    <div>
                                                                        <div class="btn-minus minuss" data-id="{{$product_list->id}}">
                                                                            <span class="fa fa-minus"></span>
                                                                        </div>
                                                                        <input type="text" value="1" class="input{{$product_list->id}}" name="qty" id="qty" style="width:70px">
                                                                        <div class="btn-plus pluss" data-id="{{$product_list->id}}">
                                                                            <span class="fa fa-plus"></span>
                                                                        </div>
                                                                    </div>
                                                                </div> --}}
                                                                <div class="input-group" style="padding-bottom:10px;" data-section="{{ $product_list->id }}">
                                                                    <span class="input-group-btn">
                                                                        <button type="button" class="btn btn-danger minuss"  data-type="minus" data-id="{{$product_list->id}}">
                                                                            <span class="fa fa-minus"></span>
                                                                        </button>
                                                                    </span>
                                                                    <input type="text" value="1" class=" form-control input{{$product_list->id}}" name="qty" id="qty-{{$product_list->id}}" style="text-align: center; ">
                                                                    <span class="input-group-btn">
                                                                        <button type="button" class="btn btn-success pluss" data-type="plus" data-id="{{$product_list->id}}">
                                                                            <span class="fa fa-plus"></span>
                                                                        </button>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2"></div>
                                                        </div>
                                                        <center><small><strike>Rp.{{ number_format($product_list->harga) }}</strike></small></center>
                                                        <center><h6><strong>Rp.{{number_format($product_list->harga_promo)}}</strong></h6></center>
                                                    </div>
                                                    <button class="add-to-cart btn btn-warning btn-sm" type="button" id="vtn_cart" data-id="{{$product_list->id}}" data-qty="{{$product_list->stok}}" data-produk-name="{{$product_list->nama_produk}}"><i class="fa fa-shopping-cart" style="color:white"> Add To Cart</i></button>
                                                    <!-- <a href="{{ url('customer/order-produk/add-to-cart/'.$product_list->id) }}" class="btn btn-warning btn-sm"><i class="fa fa-shopping-cart" style="color:white"> Add To Cart</i></a> -->
                                                </div>
                                            </div>
                                        </div>  
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>
       

        $(document).ready(function(){
            $('.add-to-cart').on("click",function(){
                id = $(this).attr('data-id');
                stok = $(this).attr('data-qty');
                qty = $("#qty-"+id).val();
                console.log('qty', qty)
                if(parseInt(qty) > parseInt(stok)){
                    // alert('stok lebih')
                    swal({
                        title: "Ups!",
                        text: "QTY yang masukan melebihi Stok yang Ada.",
                        type: 'error'
                    });
                }else{
                    // alert('stok bisa')
                    window.location.href="/customer/order-produk/add-to-cart/"+id+'/'+$('#qty-'+id).val();
                }
                // window.location.href="/customer/order-produk/add-to-cart/"+id+'/'+$('#qty-'+id).val();
            });

            @foreach ($showProduct as $k => $product_list)
                @if($product_list->valid_at != "" && $product_list->expired_at !="")
                    CountDownTimer('{{$product_list->valid_at}}', "labelTimer{{$product_list->id}}",'{{$product_list->expired_at}}');
                @endif
            @endforeach
            
            //-- Click on detail
            $("ul.menu-items > li").on("click",function(){
                $("ul.menu-items > li").removeClass("active");
                $(this).addClass("active");
            })

            $(".attr,.attr2").on("click",function(){
                var clase = $(this).attr("class");

                $("." + clase).removeClass("active");
                $(this).addClass("active");
            })

            //-- Click on QUANTITY
            clickId = $(".input-group").attr('data-section');
            $(".minuss").on("click",function(){
                id = $(this).attr('data-id')
                var now = $(".input-group > .input"+id).val();
                if ($.isNumeric(now)){
                    if(now > 1){
                        $(".input-group > .input"+id).val(parseInt(now) - 1);
                    }
                }else{
                    $(".input-group > input"+id).val(1);
                }
            })   
                     
            $(".pluss").on("click",function(){
                id = $(this).attr('data-id')
                console.log(id)
                var now = $(".input-group > .input"+id).val();
                if ($.isNumeric(now)){
                    $(".input-group > .input"+id).val(parseInt(now)+1);
                }else{
                    $(".input-group > input"+id).val("1");
                }
            })      
        }) 
    </script>
@endsection
