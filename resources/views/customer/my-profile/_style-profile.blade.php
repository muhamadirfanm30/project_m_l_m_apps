<style>
    /* Profile container */
    .profile {
      margin: 20px 0;
    }
    
    /* Profile sidebar */
    .profile-sidebar {
      background: #fff;
    }
    
    .profile-userpic img {
      float: none;
      margin: 0 auto;
      width: 50%;
      -webkit-border-radius: 50% !important;
      -moz-border-radius: 50% !important;
      border-radius: 50% !important;
    }
    
    .profile-usertitle {
      text-align: center;
      margin-top: 20px;
    }
    
    .profile-usertitle-name {
      color: #5a7391;
      font-size: 16px;
      font-weight: 600;
      margin-bottom: 7px;
    }
    
    .profile-usertitle-job {
      text-transform: uppercase;
      color: #555;
      font-size: 12px;
      font-weight: 600;
      margin-bottom: 15px;
    }
    
    .profile-userbuttons {
      text-align: center;
      margin-top: 10px;
    }
    
    .profile-userbuttons .btn {
      text-transform: uppercase;
      font-size: 11px;
      font-weight: 600;
      padding: 6px 15px;
      margin-right: 5px;
    }
    
    .profile-userbuttons .btn:last-child {
      margin-right: 0px;
    }
    
    .profile-usermenu {
      margin-top: 30px;
    }
    
    /* Profile Content */
    .profile-content {
      padding: 20px;
      background: #fff;
      min-height: 460px;
    }
    
    .avatar-upload {
      position: relative;
      max-width: 205px;
      margin: 0px auto;
    }
    .avatar-upload .avatar-edit {
      position: absolute;
      right: 12px;
      z-index: 1;
      top: 10px;
    }
    .avatar-upload .avatar-edit input {
      display: none;
    }
    .avatar-upload .avatar-edit input + label {
      display: inline-block;
      width: 34px;
      height: 34px;
      margin-bottom: 0;
      border-radius: 100%;
      background: #FFFFFF;
      border: 1px solid transparent;
      box-shadow: 0 5px 5px rgba(0, 0, 0, 0.2);
      cursor: pointer;
      font-weight: normal;
      transition: all 0.2s ease-in-out;
    }
    .avatar-upload .avatar-edit input + label:hover {
      background: #f1f1f1;
      border-color: #d6d6d6;
    }
    .avatar-upload .avatar-edit input + label:after {
      content: "\f040";
      font-family: 'FontAwesome';
      color: #757575;
      position: absolute;
      top: 10px;
      left: 0;
      right: 0;
      text-align: center;
      margin: auto;
    }
    .avatar-upload .avatar-preview {
      width: 225px;
      height: 225px;
      position: relative;
      border-radius: 100%;
      border: 6px solid #F8F8F8;
      box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
    }
    .avatar-upload .avatar-preview > div {
      width: 100%;
      height: 100%;
      border-radius: 100%;
      background-size: cover;
      background-repeat: no-repeat;
      background-position: center;
    }
</style>