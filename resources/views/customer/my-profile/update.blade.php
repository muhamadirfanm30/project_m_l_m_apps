@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header-tab card-header header-bg">
                            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                                Edit Profile
                            </div>
                        </div>
                        <div class="card-body">
                            @if (Auth::user()->roles == "Customer")
                                <form action="{{url('customer/my-profile/update-my-profile/'.$dataEdit->id)}}" method="post" enctype="multipart/form-data">
                            @else
                                <form action="{{url('admin/my-profile/update-my-profile-admin/'.$dataEdit->id)}}" method="post" enctype="multipart/form-data">
                            @endif
                            
                                @csrf
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input type="text" name="email" value="{{!empty($dataEdit->email) ? $dataEdit->email : ''}}" class="form-control" placeholder="Email ..." disabled>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>first name</label>
                                            <input type="text" name="first_name" value="{{!empty($dataEdit->first_name) ? $dataEdit->first_name : ''}}" class="form-control" placeholder="First Name ...">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Last name</label>
                                            <input type="text" name="last_name" value="{{!empty($dataEdit->last_name) ? $dataEdit->last_name : ''}}" class="form-control" placeholder="Last name ...">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Nomor KTP</label>
                                            <input type="text" name="no_ktp" value="{{!empty($dataEdit->no_ktp) ? $dataEdit->no_ktp : ''}}" class="form-control" placeholder="Nomor KTP ...">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1">Jenis Kelamin</label>
                                            <select class="form-control" name="gender" id="exampleFormControlSelect1">
                                                <option value="Laki Laki" {{ $dataEdit->gender == "Laki Laki" ? "selected" : '' }}>Laki Laki</option>
                                                <option value="Perempuan" {{ $dataEdit->gender == "Perempuan" ? "selected" : '' }}>Perempuan</option>
                                                <option value="Lain Nya" {{ $dataEdit->gender == "Lain Nya" ? "selected" : '' }}>Lain nya</option>
                                            </select>
                                            {{-- <label>Gender</label>
                                            <input type="text"  value="{{!empty($dataEdit->gender) ? $dataEdit->gender : ''}}" class="form-control" placeholder="Gender ..."> --}}
                                        </div>
                                    </div>
                                    {{-- <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Nama Bank</label>
                                            <input type="text" name="rekening_bank" value="{{!empty($dataEdit->rekening_bank) ? $dataEdit->rekening_bank : ''}}" class="form-control" placeholder="Nama Bank ...">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Nama Rekening Bank</label>
                                            <input type="text" name="nama_pemilik_rekening" value="{{!empty($dataEdit->nama_pemilik_rekening) ? $dataEdit->nama_pemilik_rekening : ''}}" class="form-control" placeholder="Nama Rekening Bank ...">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Nomor Rekening Bank</label>
                                            <input type="text" name="nomor_rekening" value="{{!empty($dataEdit->nomor_rekening) ? $dataEdit->nomor_rekening : ''}}" class="form-control" placeholder="Nomor Rekening Bank ...">
                                        </div>
                                    </div> --}}
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Kode Pos</label>
                                            <input type="text" name="kode_pos" value="{{!empty($dataEdit->kode_pos) ? $dataEdit->kode_pos : ''}}" class="form-control" placeholder="Kode Pos ...">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Nomor Ponsel</label>
                                            <input type="text" name="phone" id="nomor_ponsel" value="{{!empty($dataEdit->phone) ? $dataEdit->phone : ''}}" class="form-control" placeholder="Nomor Ponsel ...">
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-6" id="provHide">
                                        <div class="form-group">
                                            <label>Provinsi</label>
                                            <select class="form-control" name="province_id" id="province_id" style="width:100%">
                                                <option>Pilih Provinsi</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6" id="cityHide">
                                        <div class="form-group">
                                            <label>Kota / Kabupaten</label>
                                            <select class="form-control" name="kota_id" id="kota_id" style="width:100%">
                                                <option>Pilih Provinsi Terlebih dahulu</option>
                                            </select>
                                        </div>
                                    </div>
                                    <input type="hidden" id="txtProvinsi" name="txtProvinsi">
                            <input type="hidden" id="txtKota" name="txtKota">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>No KTP</label>
                                            <input type="text" name="no_ktp" value="{{!empty($dataEdit->no_ktp) ? $dataEdit->no_ktp : ''}}" class="form-control" placeholder="Masukan Nomor KTP...">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <p><strong>Foto KTP (Kartu Tanda Penduduk)</strong></p>
                                            <p>* Pastikan Foto KTP Anda Terlihat Jelas, Bisa Dibaca dan Tidak Terpotong. Format JPEG, PNG, JPG, Maximal Size 5 MB</p>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <input type='file' name="foto_ktp" id="imgInp" /><br><br>
                                                    @if (!empty($dataEdit->foto_ktp))
                                                        <img id="blah" src="{{ url('storage/foto-ktp/'.$dataEdit->foto_ktp) }}" alt="your image" width="90%"/><br>
                                                        <small>Foto KTP User</small>
                                                    @else
                                                        <img id="blah" src="" alt="" width="90%"/>
                                                    @endif
                                                    
                                                </div>
                                                <div class="col-md-8">
                                                    <center><img src="{{ asset('ktp.png') }}" alt="" width="90%"></center>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>Alamat</label>
                                            <textarea id="summernote" name="address">{!! !empty($dataEdit->address) ? $dataEdit->address : '' !!}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <button class="btn btn-primary btn-block">Update</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    @include('customer.my-profile._style-profile')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function(e) {
                $('#blah').attr('src', e.target.result);
                }
                
                reader.readAsDataURL(input.files[0]); // convert to base64 string
            }
        }

        $("#imgInp").change(function() {
            readURL(this);
        });
    </script>
    <script>
        $(document).ready(function() {
            $('#summernote').summernote();
            onlyNumber('#nomor_ponsel');
            var provinsiUser = '{{$dataEdit->province_id}}'
            ajaxRequest('GET', '/api/rajaongkir/provinsi', function(response) {
                var select = '<option value="">Pilih Provinsi</option>';
                $.each(response.data.rajaongkir.results, function(item,item2) {
                    // console.log(item2)
                    select += `<option value="${item2.province_id}" ${item2.province == provinsiUser?"selected":""}>${item2.province}</option>` ;
                    if(item2.province == provinsiUser){
                        getKota(item2.province_id)
                    }
                });
                $('#province_id').html(select)
            });
            
            // on chnege provinsi
            $('#province_id').change(function() {
                province_id = $(this).val();
                var a = $('#province_id option:selected').text();
                $('#txtProvinsi').val(a);
                getKota(province_id);
            })

            $('#kota_id').change(function() {
                var a = $('#kota_id option:selected').text();
                $('#txtKota').val(a);
            })
        });

        function getKota(province_id){
            var kotaUser = '{{ $dataEdit->kota_id }}'
            Helper.loadingStart()
            ajaxRequest('GET', '/api/rajaongkir/city?province=' + province_id, function(response) {
                console.log(response)
                var city = '<option value="">Pilih Kota / Kabupaten</option>';
                $.each(response.data.rajaongkir.results, function(item,item2) {
                    city += `<option value="${item2.city_id}" ${item2.type + ' ' + item2.city_name == kotaUser?"selected":""} >${item2.type + ' ' + item2.city_name}</option>`;
                });
                Helper.loadingStop()
                $('#kota_id').html(city)
            })
            // empty select
            $('#kota_id').html('').append('<option value="">Pilih Kota / Kabupaten</option>');
            $('#distrik_id').html('').append('<option value="">Pilih Kota / Kabupaten Terlebih Dahulu</option>');
            $('#kurir').html('').append('<option value="">Pilih Kecamatan Terlebih Dahulu</option>');
            $('#layanan').html('').append('<option value="">Pilih Kurir Terlebih Dahulu</option>');
        }
    </script>
    {{-- <script>
        $(function () {
            $('#summernote').summernote()
        })

        $(document).ready(function(){
            onlyNumber('#nomor_ponsel');
            ajaxRequest('GET', '/api/rajaongkir/provinsi', function(response) {
                var select = '<option value="">Pilih Provinsi</option>';
                $.each(response.data.rajaongkir.results, function(item,item2) {
                    // console.log(item2)
                    select += `<option value="${item2.province_id}">${item2.province}</option>` ;
                });
                $('#province_id').html(select)
            })
            

            // on chnege provinsi
            $('#province_id').change(function() {
                province_id = $(this).val();
                Helper.loadingStart()
                ajaxRequest('GET', '/api/rajaongkir/city?province=' + province_id, function(response) {
                    console.log(response)
                    var city = '<option value="">Pilih Kota / Kabupaten</option>';
                    $.each(response.data.rajaongkir.results, function(item,item2) {
                        city += `<option value="${item2.city_id}">${item2.type + ' ' + item2.city_name}</option>`;
                    });
                    Helper.loadingStop()
                    $('#kota_id').html(city)
                })
                // empty select
                $('#kota_id').html('').append('<option value="">Pilih Kota / Kabupaten</option>');
                $('#distrik_id').html('').append('<option value="">Pilih Kota / Kabupaten Terlebih Dahulu</option>');
                $('#kurir').html('').append('<option value="">Pilih Kecamatan Terlebih Dahulu</option>');
                $('#layanan').html('').append('<option value="">Pilih Kurir Terlebih Dahulu</option>');
            })
        });
    </script> --}}
    
@endsection