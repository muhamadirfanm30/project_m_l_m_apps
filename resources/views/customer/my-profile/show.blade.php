@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('success') }}</strong>
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-header-tab card-header header-bg">
                                    <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                                        PROFILE
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="profile-sidebar">
                                        <!-- SIDEBAR USERPIC -->
                                        <div class="profile-userpic text-center">
                                            <div class="avatar-upload">
                                                {{-- <div class="avatar-edit">
                                                    <input type='file' id="imageUpload"/>
                                                    <label for="imageUpload"></label>
                                                </div> --}}
                                                <div class="avatar-preview">
                                                    {{-- <div id="imagePreview">
                                                        <img src="{{ Auth::user()->avatar }}" alt="" id="imagePreview">
                                                    </div> --}}
                                                    <div id="imagePreview" style="background-image: url( {{url('storage/avatar/'.Auth::user()->avatar_user)}} )"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- END SIDEBAR USERPIC -->
                                        <!-- SIDEBAR USER TITLE -->
                                        <div class="profile-usertitle">
                                            <div class="profile-usertitle-name">
                                                {{ $user->first_name }} {{ $user->last_name }}
                                            </div>
                                            <div class="profile-usertitle-job">
                                                  <label class="label-danger">{{ $user->membersip_status }}  {{ $user->membership_id }} </label>
                                            </div>
                                        </div>
                                        <label class="btn btn-primary btn-sm btn-block">
                                            Ubah Foto <input type="file" name="files" id="imageUpload" hidden>
                                        </label>
                                        {{-- <input type='file' class="form-control" /> --}}
                                        <!-- END SIDEBAR USER TITLE -->
                                        <!-- SIDEBAR BUTTONS -->
                        
                                        <!-- END SIDEBAR BUTTONS -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="card">
                                <div class="card-header-tab card-header header-bg">
                                    <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                                        DETAIL USER
                                    </div>
                                </div>
                                <div class="card-body">
                                    <table class="table table-user-information">
                                        <tbody>
                                            <tr>
                                                <th>
                                                    Email
                                                </th>
                                                <td>
                                                    {{ $user->email }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    Phone
                                                </th>
                                                <td>
                                                    {{ $user->phone }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    First Name
                                                </th>
                                                <td>
                                                    {{ $user->first_name }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    Last Name
                                                </th>
                                                <td>
                                                    {{ $user->last_name }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    Gender
                                                </th>
                                                <td>
                                                    {{ $user->gender }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    Nomor KTP
                                                </th>
                                                <td>
                                                    {{ $user->no_ktp }}
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table><hr>
                                    @if (Auth::user()->roles == "Customer")
                                        <a href="{{url('customer/my-profile/update/'.$user->id)}}" class="btn btn-success btn-sm btn-block" ><i class="fa fa-pencil"></i> Edit Profile</a>
                                    @else
                                        <a href="{{url('admin/my-profile/update/'.$user->id)}}" class="btn btn-success btn-sm btn-block" ><i class="fa fa-pencil"></i> Edit Profile</a>
                                    @endif
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header-tab card-header header-bg">
                            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                                Ubah Kata Sandi
                            </div>
                        </div>
                        <div class="card-body">
                            <form id="form-change-password" role="form" method="POST" action="{{ url('customer/my-profile/change-password') }}" novalidate class="form-horizontal">
                                @if(session()->has('success'))
                                    <div class="alert alert-success alert-block">
                                        <button type="button" class="close" data-dismiss="alert">×</button>	
                                        <strong>{{ session()->get('success') }}</strong>
                                    </div>
                                @endif
                                @if(session()->has('error'))
                                    <div class="alert alert-danger alert-block">
                                        <button type="button" class="close" data-dismiss="alert">×</button>	
                                        <strong>{{ session()->get('error') }}</strong>
                                    </div>
                                @endif
                                {{-- @if ($errors->any())
                                    <div class="alert alert-danger alert-block">
                                        <button type="button" class="close" data-dismiss="alert">×</button>	
                                        <strong>{{ $error->first() }}</strong>
                                    </div>
                                @endif --}}
                                    <label for="current-password" class="col-sm-4 control-label">Kata Sandi Lama</label>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
                                            <input type="password" class="form-control" id="current-password" name="current-password" placeholder="Password">
                                        </div>
                                    </div>
                                    <label for="password" class="col-sm-4 control-label">Kata Sandi Baru</label>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                                        </div>
                                    </div>
                                    <label for="password_confirmation" class="col-sm-4 control-label">Konfirmasi Kata Sandi</label>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Re-enter Password">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-danger btn-sm btn-block">Ubah Kata Sandi</button>
                                    </div>
                              </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqvmap/1.5.1/jqvmap.min.css" integrity="sha512-RPxGl20NcAUAq1+Tfj8VjurpvkZaep2DeCgOfQ6afXSEgcvrLE6XxDG2aacvwjdyheM/bkwaLVc7kk82+mafkQ==" crossorigin="anonymous" />
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqvmap/1.5.1/jquery.vmap.min.js" integrity="sha512-Zk7h8Wpn6b9LpplWXq1qXpnzJl8gHPfZFf8+aR4aO/4bcOD5+/Si4iNu9qE38/t/j1qFKJ08KWX34d2xmG0jrA==" crossorigin="anonymous"></script>
    @include('customer.my-profile._style-profile')
    <script>
        function readURL(input) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#imagePreview').css('background-image', 'url('+e.target.result +')');
                $('#imagePreview').hide();
                $('#imagePreview').fadeIn(650);
            }
            reader.readAsDataURL(input.files[0]);
        }
       
        $("#imageUpload").change(function() {
        input = this;

        if (input.files && input.files[0]) {
            Helper.loadingStart();
            var formData = new FormData();
            formData.append("image", input.files[0]);

            console.log(formData);
            Axios
                .post(Helper.apiUrl('/customer/my-profile/upload-image'), formData, {
                    headers: {
                      'Content-Type': 'multipart/form-data'
                    }
                })
                .then(function(response) {
                    Helper.loadingStop();
                    readURL(input);
                    swal({
                    title: "Sukses!",
                        text: "Avatar Berhasil di Upload",
                        type: 'success'
                    });
                })
                .catch(function(error) {
                    Helper.loadingStop();
                    swal({
                        title: "Ups!",
                        text: 'error',
                        type: 'error'
                    });
                });
        }
    });
    </script>
    
@endsection