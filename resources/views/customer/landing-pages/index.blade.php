@extends('home')
@section('content')
<div class="content-wrapper">
    <section class="content"><br>
        <div class="row">
            <div class="col-md-12">
                @if(session()->has('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>	
                        <strong>{{ session()->get('success') }}</strong>
                    </div>
                @endif
                
                <div class="card card-outline">
                    <div class="card-header">
                        
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-3"></div>
                                <div class="col-md-6">
                                    <h3 class="card-title">
                                        <center>
                                            <strong>
                                                Even dan Live Training yang Bisa Mendongkrak Omset Anda
                                            </strong>
                                        </center>
                                    </h3><br><br>
                                    <div class="embed-responsive embed-responsive-16by9" style="width:auto">
                                        <iframe class="embed-responsive-item" src="{{ !empty($showUrlVidio->value) ? $showUrlVidio->value : ''  }}" allowfullscreen></iframe>
                                    </div><br>
                                </div>
                                <div class="col-md-3"></div>
                            </div>
                        </div>
                        <form action="{{ url('customer/my-profile/whatsapp_no-update/'.Auth::user()->id) }}" method="post">
                            @csrf
                            <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Generate No WhatsApp</label>
                                            <input class="form-control" id="phone" name="whatsapp_no" value="{{!empty(Auth::user()->whatsapp_no) ? Auth::user()->whatsapp_no : ''}}" data-inputmask="'alias': 'phonebe'">
                                            {{-- <input type="number"  class="form-control" placeholder="Nomor Whatsapp ..." value="{{!empty(Auth::user()->whatsapp_no) ? Auth::user()->whatsapp_no : ''}}"> --}}
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label style="color:white">_asd</label><br>
                                            <button type="submit" class="btn btn-primary">Generate</button>
                                        </div>
                                    </div>
                            </div><hr>
                        </form>
                        <div id="copied_success"></div>
                        <style>
                            table {
                                font-size: 1em;
                            }

                            .ui-draggable, .ui-droppable {
                                background-position: top;
                            }
                        </style>

                        {{-- <div id="accordion">
                            @foreach($showProduk as $index => $val)
                                <h6> <strong>{{ $val->nama_produk }} </strong></h6>
                                <div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table table-striped">
                                                @foreach ($wa as $whatsapp)
                                                    @php
                                                        $url ='';
                                                        $encode = '';
                                                        $encode = '/pages/'.base64_encode($val->slug).'/'.base64_encode($val->template_product).'/'.$noWa.'/'.base64_encode(str_replace(' ', 'QNGTF34907HH', $whatsapp->message));
                                                        <!-- $url = base64_encode($encode); -->
                                                    @endphp
                                                    <tr>
                                                        <td> </td>
                                                        <td>{{ url($encode) }}</td>
                                                        <td><button id="btn_copy" data-order-id="{{ $val->id }}" data-order-code="{{ url($encode) }}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Copy To Clipboard" style="float:right"><i class="fa fa-copy"></i></button></td>
                                                    </tr>
                                                @endforeach
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div> --}}
                        <div id="accordion">
                            @foreach($showTemplate as $index => $val)
                                <h6> <strong>{{ $val->is_judul_landing_page }} </strong></h6>
                                <div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="table table-striped">
                                                @foreach ($wa as $whatsapp)
                                                    @php
                                                        $url ='';
                                                        $encode = '';
                                                        // $encode = '/pages='.$val->is_judul_landing_page.'&msg='.$whatsapp->message.'&idLandingPage='.$val->id;
                                                        $encode = '/pages/'.str_replace(' ', '-', $val->is_judul_landing_page).'/'.$noWa.'/?text='.str_replace(' ', '-', $whatsapp->message);
                                                        $url = base64_encode($encode);
                                                    @endphp
                                                    <tr>
                                                        <td>{{ url($encode) }}</td>
                                                        <td><button id="btn_copy" data-order-id="{{ $val->id }}" data-order-code="{{ url($encode) }}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Copy To Clipboard" style="float:right"><i class="fa fa-copy"></i></button></td>
                                                    </tr>
                                                @endforeach
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="card-footer">
                    </div>
                </div>
            </div>
        </div><br>
        
    </section>
</div>
<input id="input_copy" type="text" name="id" value="" style="width:1%; color:white; border:none">
<link rel="stylesheet" href="{{ asset('styles.css') }}">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.60/inputmask/jquery.inputmask.js"></script>
<script>
    $( function() {
      $( "#accordion" ).accordion({
        collapsible: true
      });
    } );
</script>
<script>
    $("#phone").inputmask({
        mask: '628 9999 9999 99',
        placeholder: ' ',
        showMaskOnHover: false,
        showMaskOnFocus: false,
        onBeforePaste: function (pastedValue, opts) {
        var processedValue = pastedValue;

        //do something with it

        return processedValue;
        }
    });

    $(document).on('click', '#btn_copy', function(){
        $('#input_copy').val($(this).attr('data-order-code'));

        var copyText = document.getElementById("input_copy");
        copyText.select();
        copyText.setSelectionRange(0, 99999)
        document.execCommand("copy");
        $("#copied_success").html('');
        var voucerTemplate = '';
        voucerTemplate += `<div class="alert alert-success alert-success" role="alert">
                                <center>
                                    <i class="fa fa-check"></i> 
                                    <strong>Copied Successfully</strong>
                                </center>
                            </div>`;
        $("#copied_success").append(voucerTemplate);
        // alert("Copied the text: " + copyText.value);
    });
</script>
@endsection
