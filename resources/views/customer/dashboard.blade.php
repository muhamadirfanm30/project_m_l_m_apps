@extends('home')
@section('content')
<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-outline">
                    <div class="card-header">
                        <h3 class="card-title">
                            <center>WELCOME TO GENERASI ONLINE</center>
                        </h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="row">
                                        <div class="col">
                                            <div class="row">
                                                <div class="col">
                                                    <img src="{{ asset('instagram.png') }}" alt="" style="width: 20px">
                                                </div>
                                                <div class="col" style="marg">
                                                    <h6 style="margin-left: -88%;"> {{!empty($getKonten->nama_instagram) ? '@'.$getKonten->nama_instagram : '-'}}</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col">
                                            <div class="row">
                                                <div class="col">
                                                    <img src="{{ asset('facebook.png') }}" alt="" style="width: 20px">
                                                </div>
                                                <div class="col">
                                                    <h6 style="margin-left: -88%;">{{!empty($getKonten->nama_facebook) ? '@'.$getKonten->nama_facebook : '-'}}</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div><br>
                                </div>
                                <div class="col-md-6">
                                    <div class="embed-responsive embed-responsive-16by9" style="width:auto">
                                        <iframe class="embed-responsive-item" src="{{!empty($getKonten->url_vidio_generasi_online) ? $getKonten->url_vidio_generasi_online : ''}}" allowfullscreen></iframe>
                                    </div><br>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <a href="{{!empty($getKonten->link_telegram) ? $getKonten->link_telegram : ''}}" class="btn btn-primary btn-block" target="_blank">Join Group Telegram</a>
                                                </div><br><br>
                                                <div class="col-md-6">
                                                    <a href="{{!empty($getKonten->link_group_content) ? $getKonten->link_group_content : ''}}" class="btn btn-primary btn-block" target="_blank">Chat Admin</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3"></div>
                            </div>
                        </div>
                        
                    </div>
                    <div class="card-footer"></div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card card-outline">
                    <div class="card-header">
                        <h3 class="card-title">
                            <center>Video Tutorial Penggunaan Dashboard dan Fasilitas Generasi Online</center>
                        </h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-3"></div>
                                <div class="col-md-6">
                                    <div class="embed-responsive embed-responsive-16by9" style="width:auto">
                                        <iframe class="embed-responsive-item" src="{{!empty($getKonten->url_vidio_tutorial_dashboard) ? $getKonten->url_vidio_tutorial_dashboard : ''}}" allowfullscreen></iframe>
                                    </div><br>
                                </div>
                                <div class="col-md-3"></div>
                            </div>
                        </div>
                        
                    </div>
                    <div class="card-footer">
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
