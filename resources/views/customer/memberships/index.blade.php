@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('success') }}</strong>
                        </div>
                    @endif
                    <div class="card card-outline card-info">

                        <div class="card-header">
                            <h3 class="card-title">
                            Memberships
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <h4 class="card-title">
                                Membership Status : <strong>{{Auth::user()->membersip_status}}</strong>
                            </h4><br><br>
                            @if (!empty($getSales))
                                @php
                                    $total = 0;
                                @endphp
                                @foreach ($getSales as $k => $v)
                                    @php
                                        $total += $v->totalKeseluruhan;
                                    @endphp
                                @endforeach
                            @endif
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <h6 class="card-title">
                                                {{-- Reguler <br><strong>Rp.{{number_format($total)}}</strong>   --}}
                                                Reguler <br><strong>Rp.0</strong> 
                                            </h6>
                                        </div>
                                        <div class="col-md-8">
                                            @if ($cek_ms > 0)
                                                <input type="range" min="1" max="{{$getValue->value}}" value="{{$getValue->value}}" disabled style="width:100%">
                                            @elseif(Auth::user()->is_upgrade_ms == 1 )
                                                <input type="range" min="1" max="{{$getValue->value}}" value="{{$getValue->value}}" disabled style="width:100%">
                                            @else 
                                                <input type="range" min="1" max="{{$getValue->value}}" value="{{$total}}" disabled style="width:100%">
                                            @endif
                                        </div>
                                        <div class="col-md-2">
                                            <h6 class="card-title">
                                                <center>Mobile Stokiest (MS) <strong>Rp {{ number_format($getValue->value) }}</strong></center>
                                            </h6>
                                        </div>
                                    </div>
                                </div>
                            </div><br>  
                            <div class="row">
                                <div class="col-md-12">
                                    <span class="badge badge-success">
                                        <div class="container">
                                            <strong>Total Sales Saat ini : Rp.{{number_format($total)}}</strong>
                                        </div>
                                    </span>
                                </div>
                            </div><br>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <span class="badge badge-primary">
                                                <div class="container">
                                                    @if(Auth::user()->membersip_status == "MS (Mobile Stokiest)")
                                                        <strong>Sisa RP.0 lagi untuk Free Upgrade ke Membership Member Stokiest (MS)</strong>
                                                    @elseif(Auth::user()->membersip_status != "MS (Mobile Stokiest)") 
                                                        @php
                                                            $sum = 0;
                                                            if($total <= $getValue->value){
                                                                $sum = $getValue->value - $total;
                                                            }else{
                                                                $sum = 0;
                                                            }
                                                        @endphp
                                                        <strong>Sisa RP.{{number_format($sum)}} lagi untuk Free Upgrade ke Membership Member Stokiest (MS)</strong>
                                                    @endif
                                                </div>
                                            </span>
                                        </div>
                                        <div class="col-md-6">
                                            

                                            @if (Auth::user()->is_upgrade_ms == 3)
                                                <span class="badge badge-secondary" style="float: right">
                                                    <div class="container">
                                                        <strong>Memberships MS Sedang di Proses</strong>
                                                    </div>
                                                </span>
                                            @elseif(Auth::user()->is_upgrade_ms == 1)
                                                <span class="badge badge-primary" style="float: right">
                                                    <div class="container">
                                                        <strong>Anda Telah Menjadi MS</strong>
                                                    </div>
                                                </span>
                                            @else
                                                @if ($cek_ms > 0)
                                                    <a href="{{url('/customer/member/upgrade-member-mobile-stokiest/'.Auth::user()->id)}}"  class="badge badge-success" style="float: right">
                                                        <div class="container">
                                                            <i class="fa fa-edit"> Ajukan Upgrade ke MS</i>
                                                        </div>
                                                    </a>
                                                @else
                                                    @if ($total >= $getValue->value)
                                                        <a href="{{url('/customer/member/upgrade-member-mobile-stokiest/'.Auth::user()->id)}}"  class="badge badge-success" style="float: right">
                                                            <div class="container">
                                                                <i class="fa fa-edit"> Ajukan Upgrade ke MS</i>
                                                            </div>
                                                        </a>
                                                    @else
                                                        <a href="{{ url('/customer/paket-ms-upgrade/show') }}" class="badge badge-primary btn-sm" style="float: right"> 
                                                            <div class="container">
                                                                <i class="fa fa-check"></i> &nbsp;<strong>Langsung Upgrade ke MS</strong>
                                                            </div>
                                                        </a>
                                                    @endif
                                                @endif
                                            @endif

                                            
                                        </div>
                                    </div>
                                </div>
                            </div><br>
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="card-title">
                                        Your Performance
                                    </h4><br><hr><br>
                                    <div class="row">
                                        <div class="col-lg-4 col-6">
                                            <!-- small box -->
                                            <div class="small-box bg-info">
                                            <div class="inner">
                                                <h3>Rp.{{number_format($total)}}</h3>
                        
                                                <p>Total Omset</p>
                                            </div>
                                            <div class="icon">
                                                <i class="ion ion-bag"></i>
                                            </div>
                                            <a href="{{url('/customer/status-order/show')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-6">
                                            <!-- small box -->
                                            <div class="small-box bg-success">
                                            <div class="inner">
                                            @php
                                                $productSales = 0;
                                                foreach($getProdukSales as $key => $val){
                                                    foreach($val->order_detail as $key => $orders){
                                                        $productSales += $orders->qty;
                                                    }
                                                }
                                            @endphp
                                                <h3>{{$productSales}}<sup style="font-size: 20px"></sup></h3>
                        
                                                <p>Total Produk Terjual</p>
                                            </div>
                                            <div class="icon">
                                                <i class="ion ion-stats-bars"></i>
                                            </div>
                                            <a href="{{url('/customer/status-order/show')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-6">
                                            <!-- small box -->
                                            <div class="small-box bg-warning">
                                            <div class="inner">
                                                <h3>{{$getMyTeam}}</h3>
                                                <p>Total Member</p>
                                            </div>
                                            <div class="icon">
                                                <i class="ion ion-person-add"></i>
                                            </div>
                                            <a href="{{url('/customer/my-team/show')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <h4 class="card-title">
                                <center>Penjelasan Sistem Memberships di Generasi Online</center>
                            </h4>
                        </div>
                    <!-- /.card-header -->
                        <div class="card-body">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-6">
                                        <div class="embed-responsive embed-responsive-16by9" style="width:auto">
                                            <iframe class="embed-responsive-item" src="{{$getUrl->value}}" allowfullscreen></iframe>
                                        </div><br>
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    {{-- modal --}}

    {{-- @include('customer.memberships._modalUpgradeMs') --}}
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
@endsection