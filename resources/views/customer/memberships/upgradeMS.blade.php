@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                            </ul>
                        </div>
                    @endif
                    @if ($getDataUser->is_upgrade_ms == 0)
                        @php
                            $disabled = '';
                        @endphp
                        <div class="alert alert-danger" role="alert">
                            <strong style="color: white">Silahkan Lengkapi data diri anda yang belum lengkap untuk melanjutkan proses upgrade menuju MS (Mobile Stokiest)</strong> 
                        </div>
                    @elseif($getDataUser->is_upgrade_ms == 1)
                        @php
                            $disabled = 'disabled';
                        @endphp
                        <div class="alert alert-success" role="alert">
                            <strong style="color:white">Selamat Anda Telah Menjadi Memberships Mobile Stokiest (MS)</strong>
                        </div>
                    @elseif($getDataUser->is_upgrade_ms == 2)
                        @php
                            $disabled = '';
                        @endphp
                        <div class="alert alert-warning" role="alert">
                            <strong style="color:white">Verifikasi anda gagal</strong>
                        </div>
                    @elseif($getDataUser->is_upgrade_ms == 3)
                        @php
                            $disabled = 'disabled';
                        @endphp
                        <div class="alert alert-warning" role="alert">
                            <strong style="color: white">Akun Anda sedang di verifikasi admin kami untuk mementukan ke aslian data diri anda</strong>
                        </div>
                    @endif
                    <div class="card card-outline card-info">

                        <div class="card-header">
                            <h3 class="card-title">
                            Memberships
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <form action="{{url('/customer/member/upgrade-mobile-stokiest')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Nama Depan</label>
                                            <input type="text" name="first_name" class="form-control" {{$disabled}} value="{{!empty($getDataUser->first_name) ? $getDataUser->first_name : ''}}" placeholder="First Name ...">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Nama Belakang</label>
                                            <input type="text" name="last_name" class="form-control" {{$disabled}} value="{{!empty($getDataUser->last_name) ? $getDataUser->last_name : ''}}" placeholder="Last name ...">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Status MemberShip</label>
                                            <input type="text" name="membersip_status" value="{{!empty($getDataUser->membersip_status) ? $getDataUser->membersip_status : ''}}" class="form-control" placeholder="membersip status ..." disabled>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Jenis Kelamin</label>
                                            <select class="form-control" id="exampleFormControlSelect1" name="gender" {{$disabled}}>
                                                <option value="">Pilih Jenis Kelamin</option>
                                                <option value="Laki Laki" {{ $getDataUser->gender == "Laki Laki" ? 'checked' : '' }}>Laki Laki</option>
                                                <option value="Perempuan" {{ $getDataUser->gender == "Perempuan" ? 'checked' : '' }}>Perempuan</option>
                                                <option value="Lainnya" {{ $getDataUser->gender == "Lainnya" ? 'checked' : '' }}>Lainnya</option>
                                            </select>
                                            {{-- <input type="text" name="gender" class="form-control" {{$disabled}} value="{{!empty($getDataUser->gender) ? $getDataUser->gender : ''}}" placeholder="JK ..."> --}}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input type="text" name="email" class="form-control" value="{{!empty(Auth::user()->email) ? Auth::user()->email : ''}}" placeholder="Email ..." disabled>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Nomor Whatsaps</label>
                                            <input type="number" name="whatsapp_no" class="form-control" {{$disabled}} value="{{!empty($getDataUser->whatsapp_no) ? $getDataUser->whatsapp_no : ''}}" placeholder="Whatsapp No ...">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>No KTP</label>
                                        <input type="text" name="no_ktp" value="{{!empty($getDataUser->no_ktp) ? $getDataUser->no_ktp : ''}}" class="form-control" placeholder="Masukan Nomor KTP...">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <p><strong>Foto KTP (Kartu Tanda Penduduk)</strong></p>
                                        <p>* Pastikan Foto KTP Anda Terlihat Jelas, Bisa Dibaca dan Tidak Terpotong. Format JPEG, PNG, JPG, Maximal Size 5 MB</p>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <input type='file' name="foto_ktp" id="imgInp" required/><br><br>
                                                @if (!empty($getDataUser->foto_ktp))
                                                    <img id="blah" src="{{ url('storage/foto-ktp/'.$getDataUser->foto_ktp) }}" alt="your image" width="90%"/><br>
                                                    <small>Foto KTP User</small>
                                                @else
                                                    <img id="blah" src="" width="90%"/>
                                                @endif
                                                
                                            </div>
                                            <div class="col-md-8">
                                                <center><img src="{{ asset('ktp.png') }}" alt="" width="90%"></center>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Alamat Lengkap</label>
                                            <textarea name="address" class="form-control" {{$disabled}} id="" cols="30" rows="10">{{!empty($getDataUser->address) ? $getDataUser->address : ''}}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Provinsi</label>
                                            <select class="form-control" name="province_id" id="province_id" {{$disabled}} style="width:100%">
                                                <option>Pilih Provinsi</option>
                                            </select>
                                            {{-- <input type="text" name="provinsi" class="form-control" {{$disabled}} placeholder="Enter ..."> --}}
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Kota / Kabupaten</label>
                                            <select class="form-control" name="kota_id" {{$disabled}} id="kota_id" style="width:100%">
                                                <option>Pilih Provinsi Terlebih dahulu</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Kode Pos</label>
                                            <input type="text" name="kode_pos" class="form-control" {{$disabled}} value="{{!empty($getDataUser->kode_pos) ? $getDataUser->kode_pos : ''}}" placeholder="Kode Pos ...">
                                        </div>
                                    </div>
                                    {{-- <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Memberships</label>
                                            <input type="text" name="membersip_status" class="form-control" disabled value="{{!empty($getDataUser->membersip_status) ? $getDataUser->membersip_status : ''}}" placeholder="Membership ...">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Memberships ID</label>
                                            <input type="text" name="membership_id" class="form-control" disabled value="{{!empty($getDataUser->membership_id) ? $getDataUser->membership_id : ''}}" placeholder="Membership ID ...">
                                            <input type="hidden" name="is_upgrade_ms" value="3">
                                        </div>
                                    </div> --}}
                                </div>
                                {{-- <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Nama Bank</label>
                                            <input type="text" name="rekening_bank" class="form-control" {{$disabled}} value="{{!empty($getDataUser->rekening_bank) ? $getDataUser->rekening_bank : ''}}" placeholder="Rekening Bank ...">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Nama Pemilik Rekening</label>
                                            <input type="text" name="nama_pemilik_rekening" class="form-control" {{$disabled}} value="{{!empty($getDataUser->nama_pemilik_rekening) ? $getDataUser->nama_pemilik_rekening : ''}}" placeholder="Nama Rekening Pemilik ...">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Nomor Rekening</label>
                                            <input type="text" name="nomor_rekening" class="form-control" {{$disabled}} value="{{!empty($getDataUser->nomor_rekening) ? $getDataUser->nomor_rekening : ''}}" placeholder="Nomor Rekening ...">
                                            
                                        </div>
                                    </div>
                                </div>
                                <small style="color: red">* Nama Pemilik Rekening dan nama Pemilik KTP harus sama. Perbedaan Nama Rekening dan KTP akan menyebabkan Bonus tidak akan di Transfer.</small> --}}

                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-sm btn-block" {{$disabled}}>Upgrade Members</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>

    {{-- modal --}}

    {{-- @include('customer.memberships._modalUpgradeMs') --}}
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function(e) {
                $('#blah').attr('src', e.target.result);
                }
                
                reader.readAsDataURL(input.files[0]); // convert to base64 string
            }
        }

        $("#imgInp").change(function() {
            readURL(this);
        });
    </script>
    <script>
         $(document).ready(function(){
            onlyNumber('#nomor_ponsel');
            ajaxRequest('GET', '/api/rajaongkir/provinsi', function(response) {
                var select = '<option value="">Pilih Provinsi</option>';
                $.each(response.data.rajaongkir.results, function(item,item2) {
                    // console.log(item2)
                    select += `<option value="${item2.province_id}">${item2.province}</option>` ;
                });
                $('#province_id').html(select)
            })
            

            // on chnege provinsi
            $('#province_id').change(function() {
                province_id = $(this).val();
                Helper.loadingStart()
                ajaxRequest('GET', '/api/rajaongkir/city?province=' + province_id, function(response) {
                    console.log(response)
                    var city = '<option value="">Pilih Kota / Kabupaten</option>';
                    $.each(response.data.rajaongkir.results, function(item,item2) {
                        city += `<option value="${item2.city_id}">${item2.type + ' ' + item2.city_name}</option>`;
                    });
                    Helper.loadingStop()
                    $('#kota_id').html(city)
                })
                // empty select
                $('#kota_id').html('').append('<option value="">Pilih Kota / Kabupaten</option>');
                $('#distrik_id').html('').append('<option value="">Pilih Kota / Kabupaten Terlebih Dahulu</option>');
                $('#kurir').html('').append('<option value="">Pilih Kecamatan Terlebih Dahulu</option>');
                $('#layanan').html('').append('<option value="">Pilih Kurir Terlebih Dahulu</option>');
            })
        });
    </script>
@endsection