@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('success') }}</strong>
                        </div>
                    @endif
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <h3 class="card-title">
                                List My Team Upgrade Ms
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="col-md-12">
                                <table id="my-team" class="display table table-hover table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Member</th>
                                            <th>Membership</th>
                                            <th>Tanggal Expired</th>
                                            <th>Status</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    @foreach ($dataUpgradeMs as $k => $v)
                                        @php
                                            $color = '';
                                            if($v->get_posisi != null){
                                                $color = 'success';
                                                $status = 'Sedang Menunggu Persetujuan Admin';
                                                $disabled = 'disabled';
                                                $judul = 'Posisi Telah ditetapkan';
                                            }else{
                                                if($v->created_at < \Carbon\Carbon::parse($v->created_at)->addHours(24)){
                                                    $color = 'warning';
                                                    $status = 'Menunggu Menunggu Konfirmasi Posisi';
                                                    $disabled = '';
                                                    $judul = 'Tentukan Posisi Calon Member MS';
                                                }else{
                                                    $color = 'danger';
                                                    $status = 'Expired';
                                                    $disabled = 'disabled';
                                                    $judul = 'Tentukan Posisi Calon Member MS';
                                                }
                                            }
                                            
                                        @endphp
                                        <tbody>
                                            <tr>
                                                <th>{{$no++}}</th>
                                                <th>{{$v->first_name}}</th>
                                                <th>{{$v->membership_id}}</th>
                                                <th>{{ \Carbon\Carbon::parse($v->created_at)->addHours(24)->translatedFormat('d M Y H:i') }}</th>
                                                <th><span class="badge badge-{{$color}}" style="color:white">{{$status}}</span></th>
                                                <th>
                                                    <a href="{{url('customer/list-team-upgrade-ms/update-posisi/'.$v->id)}}" 
                                                        class="btn btn-{{$color}} btn-sm {{$disabled}}" readonly>
                                                        <strong>{{$judul}}</strong>
                                                    </a>
                                                </th>
                                            </tr>
                                        </tbody>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#my-team').DataTable();
        } );
    </script>
@endsection