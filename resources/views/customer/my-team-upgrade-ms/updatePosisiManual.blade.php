@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('error'))
                        <div class="alert alert-danger alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('error') }}</strong>
                        </div>
                    @endif
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <h3 class="card-title">
                                Detail Data Calon Mobile Stokiest
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="col-md-12">
                                <div class="row">
                                    <a href="{{url('customer/list-team-upgrade-ms/update-posisi/'.$data->id)}}" style="font-weight: bold">Tentukan Posisi Sendiri &nbsp;&nbsp;</a>
                                    <a href="{{url('customer/list-team-upgrade-ms/update-posisi-random/'.$data->id)}}"> |&nbsp;&nbsp; Tentukan Posisi Secara Random</a>
                                </div><br>
                                <form method="POST" action="{{url('customer/list-team-upgrade-ms/store-position')}}">
                                    @csrf
                                    <div class="card card-outline card-info">
                                        <div class="card-header">
                                            <h3 class="card-title">
                                                Tentukan Posisi Manual
                                            </h3>
                                        </div>
                                        <!-- /.card-header -->
                                        <div class="card-body">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <input type="hidden" name="id" value="{{$data->id}}">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label>Nama Calon Mobile Stokiest <span style="color: red">*</span></label>
                                                            <input type="text" id="nama_calom_ms" name="nama_calom_ms" class="form-control" value="{{$data->first_name}}" placeholder="Nama Calon Mobile Stokiest ..." readonly>
                                                            <input type="hidden" id="calon_member_id" name="calon_member_id" class="form-control" value="{{$data->id}}" placeholder="calom Member ...">
                                                            @error('nama_calom_ms')
                                                                <span style="color:red"><small>{{ $message }}</small></span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label>No Id Mobile Stokiest (MS) Sponsor Langsung <span style="color: red">*</span></label>
                                                            {{-- <input type="text" id="no_id_sponsor" name="no_id_sponsor" class="form-control" value="{{Auth::user()->first_name}}" placeholder="No Id Mobile Stokiest (MS) Sponsor Langsung ..."> --}}
                                                            <input type="text" id="no_id_sponsor" name="no_id_sponsor" class="form-control" placeholder="No Id Mobile Stokiest (MS) Sponsor Langsung ...">
                                                            @error('no_id_sponsor')
                                                                <span style="color:red"><small>{{ $message }}</small></span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Nama Id Mobile Stokiest (MS) Sponsor Langsung <span style="color: red">*</span></label>
                                                            <input type="text" id="nama_id_sponsor" name="nama_id_sponsor" class="form-control"  placeholder="Nama Id Mobile Stokiest (MS) Sponsor Langsung ...">
                                                            {{-- <input type="text" id="nama_id_sponsor" name="nama_id_sponsor" class="form-control" value="{{Auth::user()->membership_id}}" placeholder="Nama Id Mobile Stokiest (MS) Sponsor Langsung ..."> --}}
                                                            @error('nama_id_sponsor')
                                                                <span style="color:red"><small>{{ $message }}</small></span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <p><strong>Penempatan Posisi :</strong></p>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <p><strong> - Cek Posisi Upline Bersangkutan Melalui Login Mitra Usaha.</strong></p>
                                                        <p><strong> - Tonton Vidio Tutorial Untuk Penjelasan Lebih Lengkap.</strong></p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label>No MS Id Upline <span style="color: red">*</span></label>
                                                            <input type="text" id="no_id_sponsor_upline" name="no_id_sponsor_upline" value="{{!empty($data->sponsor_upline_id) ? $data->sponsor_upline_id : ''}}" class="form-control" placeholder="No MS Id Upline ...">
                                                            @error('no_id_sponsor_upline')
                                                                <span style="color:red"><small>{{ $message }}</small></span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label>Nama Upline <span style="color: red">*</span></label>
                                                            <input type="text" id="nama_upline" name="nama_upline" value="{{!empty($data->sponsor_upline_name) ? $data->sponsor_upline_name : ''}}" class="form-control" placeholder="Nama Upline ...">
                                                            @error('nama_upline')
                                                                <span style="color:red"><small>{{ $message }}</small></span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label>Kaki <span style="color: red">*</span></label>
                                                            <select name="posisi" id="posisi" class="form-control">
                                                                <option value="">Pilih Posisi</option>
                                                                <option value="Kiri">Kiri</option>
                                                                <option value="Kanan">Kanan</option>
                                                            </select>
                                                            @error('posisi')
                                                                <span style="color:red"><small>{{ $message }}</small></span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <p>* Pastikan Untuk Melengkapi Data Dengan Benar, Kami Sarankan Untuk Cek Kembali Sebelum Menekan Tombol Submit. 
                                                            Data yang Sudah di Submit Tidak Bisa Diubah Lagi. Kesalahan Pengisian Data Akan Menyebabkan Calon Mobile Stokiest
                                                            Akan Kami Tetapkan Posisinya Secara Random Kedalam Pohon Jaringan. <br>
                                                            (Penempatan Posisi Random Akan Tetap Berada Dibawah Pohon Jaringan Anda dan Tetap Menjadikan Anda Sebagai Sponsor Langsung)
                                                        </p>
                                                        <p style="color:red">* 
                                                            Dengan Mengklik Tombol Submit Saya Menyetujui Syarat & Ketentuan Berlaku.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-footer">
                                            <a href="{{url('customer/list-team-upgrade-ms/show')}}" class="btn btn-secondary btn-sm">Kembali</a>
                                            <button class="btn btn-warning btn-sm">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    
@endsection