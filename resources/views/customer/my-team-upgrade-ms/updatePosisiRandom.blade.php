@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <h3 class="card-title">
                                Detail Data Calon Mobile Stokiest
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="col-md-12">
                                <div class="row">
                                    <a href="{{url('customer/list-team-upgrade-ms/update-posisi/'.$datas->id)}}" >Tentukan Posisi Sendiri &nbsp;&nbsp;</a>
                                    <a href="{{url('customer/list-team-upgrade-ms/update-posisi-random/'.$datas->id)}}" style="font-weight: bold"> |&nbsp;&nbsp; Tentukan Posisi Secara Random</a>
                                </div><br>
                                <form action="{{url('customer/list-team-upgrade-ms/store-random-position')}}" method="post">
                                    @csrf
                                    <div class="card card-outline card-info">
                                        <div class="card-header">
                                            <h3 class="card-title">
                                                Tentukan Posisi Random
                                            </h3>
                                        </div>
                                        <!-- /.card-header -->
                                        <div class="card-body">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <input type="hidden" name="id" value="{{$datas->id}}">
                                                        <input type="hidden" id="calon_member_id" name="calon_member_id" class="form-control" value="{{$datas->id}}" placeholder="...">
                                                        <div class="form-group">
                                                            <label>Nama Calon Mobile Stokiest <span style="color: red">*</span></label>
                                                            <input type="text" id="nama_calom_ms" name="nama_calom_ms" value="{{$datas->first_name}}" class="form-control" placeholder="Nama Calon Mobile Stokiest  ..." readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <p><strong>Ketentuan Penempatan Posisi Calon Mobile Stokiest Secara Random</strong></p>
                                                        <p><strong>- Anda Akan Menjadi Sponsor Langsung dari Calon Member Mobile Stokiest Bersangkutan</strong></p>
                                                        <p><strong>- Posisi Calon Member MS Akan Ditetapkan Dibawah kaki/pohon Jaringan Anda. </strong></p>
                                                        <p><strong>- Posisi calon Membe MS Akan Ditempatkan Pada Kaki Dengan Omset PV Terkecil (Penempatan Omset PV Terkecil Akan Ditentukan Dari Omset PV Bulan Lalu)</strong></p>
                                                        <p><strong>- Member yang Sudah di Input Kedalam Pohon Jaringan Tidak Bisa Diubah Atau Dipindahkan ke Posisi Lain </strong></p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <p style="color:red">* 
                                                            Dengan Mengklik Tombol Submit Saya Menyetujui Syarat & Ketentuan Berlaku.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-footer">
                                            <a href="{{url('customer/list-team-upgrade-ms/show')}}" class="btn btn-secondary btn-sm">Kembali</a>
                                            <button class="btn btn-warning btn-sm">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    
@endsection