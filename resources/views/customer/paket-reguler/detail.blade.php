@extends('home')
@section('content')
@include('customer.order-product._styleDetailProduct')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('success') }}</strong>
                        </div>
                    @endif
                    <div class="card card-outline card-info">

                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <h3 class="card-title">
                                                Detail Produk
                                            </h3>
                                        </div>
                                        <div class="col-md-4">
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                                    <div class="container-fliud"><br>
                                        <div class="wrapper row">
                                            <div class="preview col-md-5">
                                                <div class="preview-pic tab-content">
                                                  <div class="tab-pane active" id="pic-1">
                                                      <center>
                                                        <img src="{{url('storage/reguler-package/'.$detail->image_produk)}}" style="width:50%; height:10%; ">
                                                      </center>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="details col-md-7">
                                                <h3 class="product-title">{{$detail->nama_paket}}</h3>
                                                {{-- <div class="rating">
                                                    <div class="stars">
                                                        <span class="fa fa-star checked"></span>
                                                        <span class="fa fa-star checked"></span>
                                                        <span class="fa fa-star checked"></span>
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star"></span>
                                                    </div>
                                                    <span class="review-no">41 reviews</span>
                                                </div> --}}
                                                <p class="product-description">Stock : <strong>{{$detail->stok}}</strong></p>
                                                <div class="price">
                                                    Rp. {{number_format($detail->harga)}}
                                                </div><br>
                                                <div class="action">
                                                    {{-- <button class="add-to-cart btn btn-default btn-sm" type="button">Checkout</button> --}}
                                                    <a href="{{ url('customer/input-reguler-member/package-checkout/'.$detail->id) }}" class="like btn btn-default btn-sm">Checkout</a>
                                                </div>
                                            </div>
                                        </div><br>
                                    </div>
                                    <div class="product-info-tabs">
                                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" id="description-tab" data-toggle="tab" href="#description" role="tab" aria-controls="description" aria-selected="true">Description</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content" id="myTabContent">
                                            <div class="tab-pane fade show active" id="description" role="tabpanel" aria-labelledby="description-tab">
                                                {!!$detail->deskripsi_produk!!}
                                            </div>
                                        </div>
                                    </div>
                                {{-- </div> --}}
                            </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>
        $(document).ready(function(){
            //-- Click on detail
            $("ul.menu-items > li").on("click",function(){
                $("ul.menu-items > li").removeClass("active");
                $(this).addClass("active");
            })

            $(".attr,.attr2").on("click",function(){
                var clase = $(this).attr("class");

                $("." + clase).removeClass("active");
                $(this).addClass("active");
            })

            //-- Click on QUANTITY
            $(".btn-minus").on("click",function(){
                var now = $(".section > div > input").val();
                if ($.isNumeric(now)){
                    if (parseInt(now) -1 > 0){ now--;}
                    $(".section > div > input").val(now);
                }else{
                    $(".section > div > input").val("1");
                }
            })            
            $(".btn-plus").on("click",function(){
                var now = $(".section > div > input").val();
                if ($.isNumeric(now)){
                    $(".section > div > input").val(parseInt(now)+1);
                }else{
                    $(".section > div > input").val("1");
                }
            })                        
        }) 
    </script>
@endsection
