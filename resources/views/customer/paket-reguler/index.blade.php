@extends('home')
@section('content')
@include('customer.order-product._styleQty')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('success') }}</strong>
                        </div>
                    @endif
                    <div class="card card-outline card-info">

                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <h3 class="card-title">
                                               Daftar Paket Pendaftaran
                                            </h3>
                                        </div>
                                        <div class="col-md-4">
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                @foreach ($paketReguler as $k => $paketRegulers)
                                    <div class="card-body border border-danger">
                                        <div class="col-md-12" style="max-width: 999px">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    {{-- <img src="storage/app/public/" alt=""> --}}
                                                    <a href="{{url('customer/input-reguler-member/detail/'.$paketRegulers->id)}}">
                                                        <img src="{{url('storage/reguler-package/'.$paketRegulers->image_produk)}}" alt="" class="img-thumbnail">
                                                    </a>
                                                </div>
                                                
                                                <div class="col-md-7">
                                                    <strong><h4>{{$paketRegulers->nama_paket}}</h4></strong>
                                                     <strong style="color:orange">Rp. {{number_format($paketRegulers->harga)}}</strong>
                                                     <br>
                                                     <br>
                                                    
                                                    <small>Deskripsi</small>
                                                    @if (strlen(strip_tags($paketRegulers->deskripsi_produk)) > 900)
                                                        <small>{!! substr($paketRegulers->deskripsi_produk,0, 900) !!} <a href="{{url('customer/input-reguler-member/detail/'.$paketRegulers->id)}}">...Read More</a></small><br><br>
                                                    @else
                                                        <small>
                                                            {!! $paketRegulers->deskripsi_produk !!} 
                                                        </small><br><br>
                                                    @endif
                                                    <div class="row">
                                                        
                                                        <div class="col-md-8">
                                                            <a href="{{url('customer/input-reguler-member/detail/'.$paketRegulers->id)}}" class="btn btn-primary" style="float:right">Lihat Selengkapnya</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div><br>
                                @endforeach
                            </div><br>
                            {!! $paketReguler->render() !!}
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>
        $(document).ready(function(){

var quantitiy=0;
   $('.quantity-right-plus').click(function(e){
        
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        var quantity = parseInt($('#quantity').val());
        
        // If is not undefined
            
            $('#quantity').val(quantity + 1);

          
            // Increment
        
    });

     $('.quantity-left-minus').click(function(e){
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        var quantity = parseInt($('#quantity').val());
        
        // If is not undefined
      
            // Increment
            if(quantity>0){
            $('#quantity').val(quantity - 1);
            }
    });
    
});
    </script>
@endsection
