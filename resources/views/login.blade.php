<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @php
        $getTitle = DB::table('general_settings')->where('id', 1)->first();
    @endphp
    <title>{{!empty($getTitle->value) ? $getTitle->value : ''}}</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/fontawesome-free/css/all.min.css') }}">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('assets/dist/css/adminlte.min.css') }}">

    <style>
    .password-show {
        
    margin-top:-10px;    
            padding-left: 30px;
           
    }
    
    .login-card-body, .register-card-body {
    background-color: #fff;
    border-top: 0;
    color: #000!important;
    padding: 20px;
}
     .login-page, .register-page{
             background-position: center!important;
    background-repeat: no-repeat!important;
    background-size: cover!important;
    background-image: url('https://www.generasisehat.id/Background.jpg')!important;
        } 

        .txt-center{
            text-align:center;
        }

        .card{
            border-radius:16px;
        }
    </style>
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="card">
        @if(session()->has('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ session()->get('success') }}</strong>
            </div>
        @endif
        @if(session()->has('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>{{ session()->get('error') }}</strong>
            </div>
        @endif
        <div class="card-body login-card-body">
            @php
                $getTitle = DB::table('general_settings')->where('id', 2)->first();
            @endphp
            <h2 class="login-box-msg">{{!empty($getTitle->value) ? $getTitle->value : ''}}</h2>
            <p class="login-box-msg">
                Silahkan Login
            </p>
            <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="input-group mb-3">
                    <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                        </div>
                    </div>
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="input-group mb-3">
                    <input type="password" id="myInput" name="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="icheck-primary">
                            <input type="checkbox" id="remember">
                            <label for="remember">
                                Remember Me
                            </label>
                        </div>
                    </div>
                    <div class="password-show col-6">
                        <label><input type="checkbox" onclick="myFunction()">  <span style="color:#000"> Show Password  </span></label>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12  txt-center" style="margin-top:20px;margin-bottom:20px">
                        <button type="submit" class="btn btn-danger">Sign In</button>
                    </div>
                </div>
                 <div class="col-md-12" style="text-align:center">
                        <a href="{{url('reset-password/show')}}"><p class="text-danger">Lupa Password?</p></a>
                        <!-- <button type="submit" class="btn btn-primary btn-block">Sign In</button> -->
                    </div>
            </form>
            {{-- <div class="social-auth-links text-center mb-3">
                <p>- OR -</p>
                <a href="#" class="btn btn-block btn-primary">
                <i class="fab fa-facebook mr-2"></i> Sign in using Facebook
                </a>
                <a href="#" class="btn btn-block btn-danger">
                <i class="fab fa-google-plus mr-2"></i> Sign in using Google+
                </a>
            </div> --}}
            <!-- /.social-auth-links -->

            {{-- <p class="mb-1">
                <a href="forgot-password.html">I forgot my password</a>
            </p>
            <p class="mb-0">
                <a href="register.html" class="text-center">Register a new membership</a>
            </p> --}}
        </div>
        <!-- /.login-card-body -->
    </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="{{ asset('assets/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('assets/dist/js/adminlte.min.js') }}"></script>

<script>
    
    function myFunction() {
  var x = document.getElementById("myInput");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}
    
</script>
</body>
</html>
