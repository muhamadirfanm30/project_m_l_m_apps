
<!DOCTYPE html>
<html>

<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css'>
    <script src='https://stackpath.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
    <style type="text/css">
        @media screen {
            @font-face {
                font-family: 'Lato';
                font-style: normal;
                font-weight: 400;
                src: local('Lato Regular'), local('Lato-Regular'), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');
            }

            @font-face {
                font-family: 'Lato';
                font-style: normal;
                font-weight: 700;
                src: local('Lato Bold'), local('Lato-Bold'), url(https://fonts.gstatic.com/s/lato/v11/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format('woff');
            }

            @font-face {
                font-family: 'Lato';
                font-style: italic;
                font-weight: 400;
                src: local('Lato Italic'), local('Lato-Italic'), url(https://fonts.gstatic.com/s/lato/v11/RYyZNoeFgb0l7W3Vu1aSWOvvDin1pK8aKteLpeZ5c0A.woff) format('woff');
            }

            @font-face {
                font-family: 'Lato';
                font-style: italic;
                font-weight: 700;
                src: local('Lato Bold Italic'), local('Lato-BoldItalic'), url(https://fonts.gstatic.com/s/lato/v11/HkF_qI1x_noxlxhrhMQYELO3LdcAZYWl9Si6vvxL-qU.woff) format('woff');
            }
        }

        /* CLIENT-SPECIFIC STYLES */
        body,
        table,
        td,
        a {
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }

        table,
        td {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        img {
            -ms-interpolation-mode: bicubic;
        }

        /* RESET STYLES */
        img {
            border: 0;
            height: auto;
            line-height: 100%;
            outline: none;
            text-decoration: none;
        }

        table {
            border-collapse: collapse !important;
        }

        body {
            height: 100% !important;
            margin: 0 !important;
            padding: 0 !important;
            width: 100% !important;
        }

        /* iOS BLUE LINKS */
        a[x-apple-data-detectors] {
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }

        /* MOBILE STYLES */
        @media screen and (max-width:600px) {
            h1 {
                font-size: 32px !important;
                line-height: 32px !important;
            }
        }

        /* ANDROID CENTER FIX */
        div[style*="margin: 16px 0;"] {
            margin: 0 !important;
        }
    </style>
</head>

<body style="background-color: #f4f4f4; margin: 0 !important; padding: 0 !important;">
    <!-- HIDDEN PREHEADER TEXT -->
    <div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: 'Lato', Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">  </div>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <!-- LOGO -->
        <tr>
            <td bgcolor="#FFA73B" align="center">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                    <tr>
                        <td align="center" valign="top" style="padding: 40px 10px 40px 10px;"> </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td bgcolor="#FFA73B" align="center" style="padding: 0px 10px 0px 10px;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                    <tr>
                        <td bgcolor="#ffffff" align="center" valign="top" style="padding: 40px 20px 20px 20px; border-radius: 4px 4px 0px 0px; color: #111111; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 48px; font-weight: 400; letter-spacing: 4px; line-height: 48px;">
                            {{-- <h1 style="font-size: 48px; font-weight: 400; margin: 2;">Welcome!</h1> <img src=" https://img.icons8.com/clouds/100/000000/handshake.png" width="125" height="120" style="display: block; border: 0px;" /> --}}
                            @if (!empty($logo))
                                <img src=" {{url('storage/logo-email/'.$logo->logo)}}" width="125" height="120" style="display: block; border: 0px;" />
                                <h1 style="font-size: 25px; font-weight: 400; margin: 2;">{{$content_email->subject}}</h1> 
                            @else
                                <h1 style="font-size: 25px; font-weight: 400; margin: 2;">{{$content_email->subject}}</h1> 
                            @endif
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td bgcolor="#f4f4f4" align="center" style="padding: 0px 10px 0px 10px;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                    <tr>
                        <td bgcolor="#ffffff" align="left" style="padding: 20px 30px 40px 30px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">
                            {{-- <p style="margin: 0;">{!! $content_email->content !!}</p><br> --}}
                            <p style="margin: 0;">{!! str_replace(
                                array(
                                    "[{FIRST_NAME}]",
                                    "[{EMAIL}]",
                                    "[{PASSWORD}]"
                                ),
                                array(
                                    $user->first_name,
                                    $user->email,
                                    $password
                                )
                                ,$content_email->content); !!}</p><br>
                        </td>
                        
                    </tr>
                    {{-- <tr>
                        <td bgcolor="#ffffff" align="left">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td bgcolor="#ffffff" align="center" style="padding: 20px 30px 60px 30px;">
                                        <table border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td align="center" style="border-radius: 3px;" bgcolor="#FFA73B"><a href="#" target="_blank" style="font-size: 20px; font-family: Helvetica, Arial, sans-serif; color: #ffffff; text-decoration: none; color: #ffffff; text-decoration: none; padding: 15px 25px; border-radius: 2px; border: 1px solid #FFA73B; display: inline-block;">Confirm Account</a></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr> <!-- COPY --> --}}
                </table>
            </td>
        </tr>
        <tr>
            <td bgcolor="#f4f4f4" align="center" style="padding: 30px 10px 0px 10px;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                    <tr>
                        <td bgcolor="#FFECD1" align="center" style="padding: 30px 30px 30px 30px; border-radius: 4px 4px 4px 4px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 18px; font-weight: 400; line-height: 25px;">
                            <table activate="activate" align="center" alignment="alignment" cellpadding="0" cellspacing="0" class="social_table" role="presentation" style="table-layout: fixed;vertical-align: top;border-spacing: 0;border-collapse: undefined;mso-table-tspace: 0;mso-table-rspace: 0;mso-table-bspace: 0;mso-table-lspace: 0;line-height: inherit;" to="to" valign="top">
                                <tbody style="line-height: inherit;">
                                    <tr align="center" style="vertical-align: top;display: inline-block;text-align: center;line-height: inherit;border-collapse: collapse;" valign="top">
                                        <td style="word-break: break-word;vertical-align: top;padding-bottom: 5px;padding-right: 8px;padding-left: 8px;line-height: inherit;border-collapse: collapse;" valign="top"><a href="{{ !empty($twiter->value) ? $twiter->value : '#' }}" target="_blank" style="line-height: inherit;background-color: transparent;"><img alt="Twitter" height="32" src="{{ asset('twitter_2x.png') }}" style="text-decoration: none;-ms-interpolation-mode: bicubic;height: auto;border: none;display: block;line-height: inherit;border-style: none;" title="Twitter" width="32"></a></td>
                                        <td style="word-break: break-word;vertical-align: top;padding-bottom: 5px;padding-right: 8px;padding-left: 8px;line-height: inherit;border-collapse: collapse;" valign="top"><a href="{{ !empty($instagram->value) ? $instagram->value : '#' }}" target="_blank" style="line-height: inherit;background-color: transparent;"><img alt="Instagram" height="32" src="{{ asset('instagram_2x.png') }}" style="text-decoration: none;-ms-interpolation-mode: bicubic;height: auto;border: none;display: block;line-height: inherit;border-style: none;" title="Instagram" width="32"></a></td>
                                        <td style="word-break: break-word;vertical-align: top;padding-bottom: 5px;padding-right: 8px;padding-left: 8px;line-height: inherit;border-collapse: collapse;" valign="top"><a href="{{ !empty($youtube->value) ? $youtube->value : '#' }}" target="_blank" style="line-height: inherit;background-color: transparent;"><img alt="youtube" height="32" src="{{ asset('youtube.png') }}" style="text-decoration: none;-ms-interpolation-mode: bicubic;height: auto;border: none;display: block;line-height: inherit;border-style: none;" title="Instagram" width="32"></a></td>
                                        <td style="word-break: break-word;vertical-align: top;padding-bottom: 5px;padding-right: 8px;padding-left: 8px;line-height: inherit;border-collapse: collapse;" valign="top"><a href="{{ !empty($facebook->value) ? $facebook->value : '#' }}" target="_blank" style="line-height: inherit;background-color: transparent;"><img alt="facebook" height="32" src="{{ asset('facebook_2x.png') }}" style="text-decoration: none;-ms-interpolation-mode: bicubic;height: auto;border: none;display: block;line-height: inherit;border-style: none;" title="Instagram" width="32"></a></td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </table>
                
            </td>
        </tr>
        <tr>
            <td bgcolor="#f4f4f4" align="center" style="padding: 0px 10px 0px 10px;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                    <tr>
                        <td bgcolor="#f4f4f4" align="left" style="padding: 0px 30px 30px 30px; color: #666666; font-family: 'Lato', Helvetica, Arial, sans-serif; font-size: 14px; font-weight: 400; line-height: 18px;"> <br>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>

</html>