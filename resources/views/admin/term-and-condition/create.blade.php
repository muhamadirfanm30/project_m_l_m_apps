@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('success') }}</strong>
                        </div>
                    @endif
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3 class="card-title">
                                        FAQ
                                    </h3>
                                </div>
                                <div class="col-md-2">
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form action="{{ route('term-and-condition.store') }}" method="post">
                                @csrf
                                <div class="container">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1">Kategori FAQ</label>
                                            <select class="form-control" name="is_template" id="exampleFormControlSelect1">
                                                @foreach ($getDataFaq as $item_faq)
                                                    <option value="{{$item_faq->id}}">{{$item_faq->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Judul Utama</label>
                                            <input type="text" name="judul_utama" class="form-control" value="{{Request::old('judul_utama')}}" placeholder="Judul Utama ...">
                                            @error('judul_utama')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Deskripsi Utama</label>
                                            <textarea id="summernote" name="deskripsi_utama">{{Request::old('deskripsi_utama')}}</textarea>
                                            @error('deskripsi_utama')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div data-role="dynamic-fields">
                                        <div class="form-inline">
                                            <div class="card card-outline card-info" style="width:100%">
                                                <div class="card-header">
                                                    <div class="row">
                                                        <div class="col-md-11">
                                                            <h3 class="card-title">
                                                                
                                                            </h3>
                                                        </div>
                                                        <div class="col-md-1">
                                                            <button class="btn btn-danger btn-sm" data-role="remove" style="float:right">
                                                                <span class="fa fa-minus"> </span>
                                                            </button>
                                                            <button class="btn btn-primary btn-sm" data-role="add">
                                                                <span class="fa fa-plus"></span>
                                                            </button>
                                                            {{-- <a href="{{ url('admin/bonus-mobile-stokiest/show') }}" class="btn btn-primary btn-sm" style="float: right">Back</a> --}}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card-body">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>Judul Accordion</label>
                                                            <input type="text" name="judul_accordion[]" class="form-control" value="{{Request::old('judul_accordion[]')}}" placeholder="Judul Utama ..." style="width:100%">
                                                            @error('judul_accordion[]')
                                                                <span style="color:red"><small>{{ $message }}</small></span>
                                                            @enderror
                                                        </div><br>
                                                        <div class="form-group">
                                                            <label>Deskripsi Accordion</label><br><hr>
                                                            <textarea name="desc_accordion[]" class="form-control" style="width:100%">{{Request::old('desc_accordion[]')}}</textarea>
                                                            @error('desc_accordion[]')
                                                                <span style="color:red"><small>{{ $message }}</small></span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary btn-block">Save</button>
                            </form>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <style>
        .entry:not(:first-of-type)
            [data-role="dynamic-fields"] > .form-inline + .form-inline {
            margin-top: 0.5em;
        }

        [data-role="dynamic-fields"] > .form-inline [data-role="add"] {
            display: none;
        }

        [data-role="dynamic-fields"] > .form-inline:last-child [data-role="add"] {
            display: inline-block;
            float: right;
        }

        [data-role="dynamic-fields"] > .form-inline:last-child [data-role="remove"] {
            display: none;
            
        }
    </style>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>
        $(function () {
            // Summernote
            $('#summernote').summernote();
            $('#summernote2').summernote();
        })
    </script>
    <script>
        $(function() {
            // Remove button click
            $(document).on(
                'click',
                '[data-role="dynamic-fields"] > .form-inline [data-role="remove"]',
                function(e) {
                    e.preventDefault();
                    $(this).closest('.form-inline').remove();
                }
            );
            // Add button click
            $(document).on(
                'click',
                '[data-role="dynamic-fields"] > .form-inline [data-role="add"]',
                function(e) {
                    e.preventDefault();
                    var container = $(this).closest('[data-role="dynamic-fields"]');
                    new_field_group = container.children().filter('.form-inline:first-child').clone();
                    new_field_group.find('input').each(function(){
                        $(this).val('');
                    });
                    container.append(new_field_group);
                }
            );
        });
    </script>
@endsection