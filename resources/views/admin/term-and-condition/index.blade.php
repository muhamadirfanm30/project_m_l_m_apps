@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('success') }}</strong>
                        </div>
                    @endif
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3 class="card-title">
                                        FAQ
                                    </h3>
                                </div>
                                <div class="col-md-2">
                                    <a href="{{ route('term-and-condition.create') }}" class="btn btn-primary btn-sm btn-block">Tambah FAQ</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="userManagement" class="display table table-hover table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kategori</th>
                                        <th>Judul</th>
                                        <th width="130px">Action</th>
                                    </tr>
                                    
                                </thead>
                                @foreach ($faq as $key => $v)
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $v->faqKategori->name }}</td>
                                        <td>{{ $v->judul_utama }}</td>
                                        <td>
                                            <form action="{{ route('term-and-condition.delete',['id'=>$v->id]) }}" method="POST" id="deleteData-{{$v->id}}">
                                                @csrf
                                            </form>
                                            <a href="{{ url('admin/term-and-conditions/update/'.$v->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                                            <button class="btn btn-danger btn-sm" id="btn-delete" onclick="deleteData({{ $v->id }})"><i class="fa fa-trash"></i></button>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#userManagement').DataTable();
        } );

        function deleteData(id){
            swal({
                title: "Alert!",
                text: "Apakah anda yakin akan menghapus data tersebut?",
                type: 'error',
                showCancelButton: true,
                confirmButtonText: 'Ya, Saya yakin',
                cancelButtonText: 'Tidak',
            },function(result) {
                if(result){
                    document.getElementById('deleteData-'+id).submit();
                }
            });
        }
    </script>
@endsection