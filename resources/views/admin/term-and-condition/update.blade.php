@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('success') }}</strong>
                        </div>
                    @endif
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3 class="card-title">
                                        FAQ
                                    </h3>
                                </div>
                                <div class="col-md-2">
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form action="{{ url('admin/term-and-conditions/edit/'.$dataEdit->id) }}" method="post">
                                @csrf
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1">Kategori FAQ</label>
                                            <select class="form-control" name="is_template" id="exampleFormControlSelect1">
                                                @foreach ($getDataFaq as $item_faq)
                                                    <option value="{{$item_faq->id}}" {{$dataEdit->is_template == $item_faq->id ? 'selected' : ''}}>{{$item_faq->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Judul Utama</label>
                                            <input type="text" name="judul_utama" class="form-control" value="{{$dataEdit->judul_utama}}" placeholder="Judul Utama ...">
                                            @error('judul_utama')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Deskripsi Utama</label>
                                            <textarea id="summernote" name="deskripsi_utama">{{$dataEdit->deskripsi_utama}}</textarea>
                                            @error('deskripsi_utama')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                    </div>
                                    
                                <button type="submit" class="btn btn-primary"style="float:right">Update</button>
                            </form>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>

                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                    <div class="col-md-10">
                                        <h3 class="card-title">
                                            Detail Syarat & Ketentuan
                                        </h3>
                                    </div>
                                    <div class="col-md-2">
                                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal" style="float: right">
                                            Tambah Data
                                        </button>
                                    </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="detail_faq" class="display table table-hover table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th style="display: none"></th>
                                        <th>Judul</th>
                                        <th>Deskripsi</th>
                                        <th width="130px">Action</th>
                                    </tr>
                                    
                                </thead>
                                @foreach ($faqDetail as $item)
                                    <tr>
                                        <th style="display: none">{{ $item->id }}</th>
                                        <td>{{$item->judul_accordion}}</td>
                                        <td>{!! substr($item->desc_accordion,0, 130) !!}</td>
                                        <td>
                                            <form action="{{ route('term-and-condition.delete.faq',['id'=>$item->id]) }}" method="POST" id="deleteData-{{$item->id}}">
                                                @csrf
                                            </form>
                                            <button type="button" data-id ="{{ $item->id }}" class="btn btn-primary btn-sm" id="update_url_vidio" data-toggle="tooltip" data-html="true" title="Update Data"><i class="fa fa-edit"></i></button>
                                            <button class="btn btn-danger btn-sm" id="btn-delete" onclick="deleteData({{ $item->id }})"><i class="fa fa-trash"></i></button>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                            
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade bd-example-modal-lg" id="modal_update_faq" data-backdrop="false" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Update Data</h5>
                            
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <input type="hidden" name="id" id="getIds">
                            <div id="content_update_faq"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Tambah Data</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form action="{{ url('admin/term-and-conditions/store-faq') }}" method="post">
                            @csrf
                            
                            <div class="modal-body">
                                <div class="form-group">
                                    <input type="hidden" name="parent_faq" id="parent_faq" value="{{ $dataEdit->id }}">
                                    <label for="">Judul Accordion</label>
                                    <input type="text" name="judul_accordion" id="judul_accordion" class="form-control"  placeholder="Masukan Judul Vidio" required>
                                </div>
                                <div class="form-group">
                                    <label for="">Desc Accordion</label>
                                    <textarea name="desc_accordion" id="desc_accordion" cols="15" rows="5" class="form-control" required></textarea>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save changes</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <style>
        .entry:not(:first-of-type)
            [data-role="dynamic-fields"] > .form-inline + .form-inline {
            margin-top: 0.5em;
        }

        [data-role="dynamic-fields"] > .form-inline [data-role="add"] {
            display: none;
        }

        [data-role="dynamic-fields"] > .form-inline:last-child [data-role="add"] {
            display: inline-block;
            float: right;
        }

        [data-role="dynamic-fields"] > .form-inline:last-child [data-role="remove"] {
            display: none;
            
        }
    </style>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script>
        function deleteData(id){
            swal({
                title: "Alert!",
                text: "Apakah anda yakin akan menghapus data tersebut?",
                type: 'error',
                showCancelButton: true,
                confirmButtonText: 'Ya, Saya yakin',
                cancelButtonText: 'Tidak',
            },function(result) {
                if(result){
                    document.getElementById('deleteData-'+id).submit();
                }
            });
        }
        
        $(document).on('click', '#update_data_faq', function(){
            id_parents = $(this).attr('data-parent');
            id_save = $(this).attr('data-save');
            let id_faq = $('#id_faq').val();
            let judul_accordion = $('#judul_accordion').val();
            let desc_accordion = $('#desc_accordion').val();
            Helper.loadingStart()
            $.ajax({
                url: '/admin/term-and-conditions/edit-faq/' + id_save,
                data:{
                    id_faq,
                    judul_accordion,
                    desc_accordion,
                },
                type: 'post',
                success: function(resp) {
                    Helper.loadingStop()
                    console.log(resp)
                    swal({
                    title: "Sukses!",
                        text: "Data Berhasil Diubah",
                        type: 'success'
                    }, function() {
                        window.location.href='/admin/term-and-conditions/update/'+id_parents;
                    });
                },
                error:function(a,v,c){
                    Helper.loadingStop()
                    var msg = JSON.parse(a.responseText);
                    swal({
                    title: "Ups!",
                        text: 'Terjadi kesalahan saat menghapus Data',
                        type: 'error'
                    });
                }
            })
        });

        var $modal = $('#modal_update_faq').modal({
            show: false
        });

        var ProductAdditionalObj = {
            // isi field input
            isiDataFormModal: function(id) {
                $.ajax({
                    url: '/admin/term-and-conditions/update-faq/' + id,
                    type: 'get',
                    success: function(resp) {
                        $("#content_update_faq").html('');
                        var content = '';
                        var templateTopup = '';

                        templateTopup +=   `<div class="form-group">
                                                <div class="form-group">
                                                    <input type="hidden" name="id_faq" id="id_faq" value="${resp.id}">
                                                    <label for="">Judul Accordion</label>
                                                    <input type="text" name="judul_accordion" id="judul_accordion" class="form-control" value="${resp.judul_accordion}" placeholder="Masukan Judul Vidio">
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Desc Accordion</label>
                                                    <textarea name="desc_accordion" id="desc_accordion" cols="15" rows="5" class="form-control">${resp.desc_accordion}</textarea>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button class="btn btn-primary waves-effect" data-save="${resp.id}" data-parent="${resp.term_condition_id}" id="update_data_faq" type="submit">
                                                    Simpan
                                                </button>
                                            </div>`;
                        $("#content_update_faq").append(templateTopup);
                    },
                    error: function(xhr, status, error) {
                        Helper.errorMsgRequest(xhr, status, error);
                    },
                })

            },
            // hadle ketika response sukses
            successHandle: function(resp) {
                // send notif
                Helper.successNotif(resp.msg);
                $modal.modal('hide');
            },
        }
        
        $(document)
            .on('click', '#update_url_vidio', function() {
                id = $(this).attr('data-id');
                $('#getIds').val(id)
                $('.modal_sales_training').show();
                console.log(ProductAdditionalObj.isiDataFormModal(id));
                $modal.modal('show');
            })
    </script>
    <script>
        $(function () {
            // Summernote
            $('#summernote').summernote();
            $('#summernote2').summernote();
        })

        $(document).ready(function() {
            $('#detail_faq').DataTable({
                processing: true,
                responsive: true,
                aaSorting: [[ 0, "desc" ]]
            });
        } );
    </script>
    <script>
        $(function() {
            // Remove button click
            $(document).on(
                'click',
                '[data-role="dynamic-fields"] > .form-inline [data-role="remove"]',
                function(e) {
                    e.preventDefault();
                    $(this).closest('.form-inline').remove();
                }
            );
            // Add button click
            $(document).on(
                'click',
                '[data-role="dynamic-fields"] > .form-inline [data-role="add"]',
                function(e) {
                    e.preventDefault();
                    var container = $(this).closest('[data-role="dynamic-fields"]');
                    new_field_group = container.children().filter('.form-inline:first-child').clone();
                    new_field_group.find('input').each(function(){
                        $(this).val('');
                    });
                    container.append(new_field_group);
                }
            );
        });
    </script>
@endsection