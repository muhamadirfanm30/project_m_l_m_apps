@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('success') }}</strong>
                        </div>
                    @endif
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3 class="card-title">
                                        Bonus Member MS
                                    </h3>
                                </div>
                                <div class="col-md-2">
                                    <a href="{{ url('admin/bonus-mobile-stokiest/create') }}" class="btn btn-primary btn-sm" style="float: right">Tambah Bonus Member</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="col-md-12">
                                <table id="userManagement" class="display table table-hover table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Judul</th>
                                            <th>Url Vidio</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    @foreach ($listBonusMs as $key => $bonus)
                                        <tr>
                                            <td>{{ $no++ }}</td>
                                            <td>{{ $bonus->judul }}</td>
                                            <td>{{ $bonus->vidio }}</td>
                                            <td>
                                                <form action="{{ url('admin/bonus-mobile-stokiest/destroy/'.$bonus->id) }}" method="POST" id="deleteData-{{$bonus->id}}">
                                                    @csrf
                                                </form>
                                                <a href="{{ url('admin/bonus-mobile-stokiest/update/'.$bonus->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                                                <button type="submit" class="btn btn-danger btn-sm" onclick="deleteData({{ $bonus->id }})"><i class="fa fa-trash"></i></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                                <form action="{{ url('admin/bonus-mobile-stokiest/deleteAll') }}" method="post" id="deleteAll">
                                    @csrf
                                </form>
                                    <button type="submit" class="btn btn-warning btn-sm" onclick="deleteData(0,true)" style="color:white"><i class="fa fa-trash"></i> &nbsp;Hapus Semua Data</button>
                            </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#userManagement').DataTable();
        } );

        function deleteData(id,deleteAll = false){
            swal({
                title: "Alert!",
                text: "Apakah anda yakin akan menghapus data tersebut?",
                type: 'error',
                showCancelButton: true,
                confirmButtonText: 'Ya, Saya yakin',
                cancelButtonText: 'Tidak',
            },function(result) {
                if(result){
                    if(deleteAll){
                        document.getElementById('deleteAll').submit();
                    }else{
                        document.getElementById('deleteData-'+id).submit();
                    }
                }
            });
        }
    </script>
@endsection