@extends('home')
@section('content')
    <br>
    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-outline">
                        <div class="card-header">
                            <h3 class="card-title">
                                <center>Dashboard</center>
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="card">
                                        <div class="card-header">
                                            <h3 class="card-title">
                                                <i class="fas fa-chart mr-1"></i>
                                                Omset
                                            </h3>
                                            <div class="card-tools">
                                                <ul class="nav nav-pills ml-auto">
                                                    <li class="nav-item">
                                                        <a class="nav-link @if($type=='Weekly') active @endif" href="{{ route('adm-dashboards',['Weekly']) }}">Weekly</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link @if($type=='Monthly') active @endif" href="{{ route('adm-dashboards',['Monthly']) }}">Monthly</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link @if($type=='Yearly') active @endif" href="{{ route('adm-dashboards',['Yearly']) }}">Yearly</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="card-body">
                                                <div class="tab-content p-0">
                                                    <div class="chart tab-pane active" id="revenue-chart" style="position: relative; height: 300px; width:100%">
                                                        <canvas id="revenue-chart-canvas" height="300" style="height: 300px;"></canvas>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="card card-outline">
                                        <div class="card-header">
                                            <h3 class="card-title">
                                                Total Penjualan Produk
                                            </h3>
                                        </div>
                                        <div class="card-body">
                                            <div class="info-box">
                                                <span class="info-box-icon bg-info elevation-1"><i class="fas fa-box"></i></span>
                                                <div class="info-box-content">
                                                    <span class="info-box-text">Produk Terjual</span>
                                                    <span class="info-box-number">
                                                        10
                                                        <small>%</small>
                                                    </span>
                                                </div>
                                            </div>
                                            @foreach ($showProduct as $item)
                                                <div class="progress-group">
                                                    
                                                    {{$item->nama_produk}}
                                                    <span class="float-right"><b>{{ $item->countSell($item->id) }}</b>/{{$item->stok}}</span>
                                                    <div class="progress progress-sm">
                                                        <div class="progress-bar w-{{$item->countSell($item->id)}}" role="progressbar" aria-valuenow="{{$item->countSell($item->id)}}" aria-valuemin="0"></div>
                                                    </div>
                                                    {{-- <span class="float-right"><b>{{ $item->countSell($item->id) }}</b>/{{$item->stok}}</span>
                                                    <div class="progress progress-sm">
                                                        <div class="progress-bar w-{{$item->countSell($item->id)}}" role="progressbar" aria-valuenow="{{$item->countSell($item->id)}}" aria-valuemin="0"></div>
                                                    </div> --}}
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-12 col-sm-6 col-md-3">
                                        <div class="info-box">
                                            <span class="info-box-icon bg-info elevation-1"><i class="fas fa-users"></i></span>
                                            <div class="info-box-content">
                                                <span class="info-box-text">Total Member MS</span>
                                                <span class="info-box-number">
                                                    {{number_format($memberMS)}}
                                                    {{-- <small>%</small> --}}
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-12 col-sm-6 col-md-3">
                                        <div class="info-box mb-3">
                                            <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-user-friends"></i></span>
                                            <div class="info-box-content">
                                                <span class="info-box-text">Total Member Reguler</span>
                                                <span class="info-box-number">{{number_format($memberReguler)}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix hidden-md-up"></div>
                                    <div class="col-12 col-sm-6 col-md-3">
                                        <div class="info-box mb-3">
                                            <span class="info-box-icon bg-success elevation-1"><i class="fas fa-user-check"></i></span>
                                            <div class="info-box-content">
                                                <span class="info-box-text">Member Aktif</span>
                                                <span class="info-box-number">{{number_format($memberAktif)}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-12 col-sm-6 col-md-3">
                                        <div class="info-box mb-3">
                                            <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-user-slash"></i></span>
                                            <div class="info-box-content">
                                                <span class="info-box-text">Member Tidak Aktif</span>
                                                <span class="info-box-number">2,000</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer"></div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>
        $(function () {
            'use strict'
            var salesChartCanvas = document.getElementById('revenue-chart-canvas').getContext('2d')
            var salesChartData = {
                labels: {!! json_encode($title) !!}, //['January', 'February', 'March', 'April'],
                datasets: [
                    {
                        label: 'Omset',
                        backgroundColor: 'rgba(60,141,188,0.9)',
                        borderColor: 'rgba(60,141,188,0.8)',
                        pointRadius: false,
                        pointColor: '#3b8bba',
                        pointStrokeColor: 'rgba(60,141,188,1)',
                        pointHighlightFill: '#fff',
                        pointHighlightStroke: 'rgba(60,141,188,1)',
                        data: {!! json_encode(array_column($datas, 'total')) !!}
                    },
                ]
            }

            var salesChartOptions = {
                maintainAspectRatio: false,
                responsive: true,
                // legend: {
                //     display: false
                // },
                scales: {
                    xAxes: [{
                        gridLines: {
                        display: false
                        }
                    }],
                    yAxes: [{
                        gridLines: {
                        display: false
                        }
                    }]
                }
            }

            // This will get the first returned node in the jQuery collection.
            // eslint-disable-next-line no-unused-vars
            var salesChart = new Chart(salesChartCanvas, {
                type: 'bar',
                data: salesChartData,
                options: salesChartOptions
            })
        })
    </script>
@endsection
