@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3 class="card-title">
                                        Kurir
                                    </h3>
                                </div>
                                <div class="col-md-2">
                                    {{-- <a href="{{ url('admin/mobile-stokiest/show') }}" class="btn btn-primary btn-sm" style="float: right">Back</a> --}}
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <form action="{{ url('admin/courier/edit/'.$dataEdit->id) }}" method="post">
                            @csrf
                            <div class="card-body">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Nama Kurir</label>
                                                <input type="text" name="text" class="form-control" placeholder="nama kurir ..."  value="{{ $dataEdit->text }}">
                                                @error('text')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="exampleFormControlSelect1">Status</label>
                                                <select class="form-control"  name="is_active" id="exampleFormControlSelect1">
                                                    <option value="1" {{ $dataEdit->is_active == 1 ? 'selected' : '' }}>Aktif</option>
                                                    <option value="2" {{ $dataEdit->is_active == 2 ? 'selected' : '' }}>Tidak Aktif</option>
                                                </select>
                                            </div>
                                            @error('is_active')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary" style="float: right">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>
        $(function () {
            // Summernote
            $('#summernote').summernote()

            //Date range picker
            $('#reservationdate').datetimepicker({
                format: 'YYYY-MM-DD'
            });
        })
    </script>
@endsection