@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
                        
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3 class="card-title">
                                        Tambah Data
                                    </h3>
                                </div>
                                <div class="col-md-2">
                                    <a href="{{ route('kategori') }}" class="btn btn-primary btn-sm" style="float: right">Back</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <form action="{{ route('kategori-create') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>Nama</label>
                                            <input type="text" name="judul" class="form-control" value="{{Request::old('judul')}}" placeholder="Judul ...">
                                            @error('judul')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>Photo</label>
                                            <input type="file" name="foto" class="form-control" placeholder="Photo ...">
                                            @error('foto')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Deskripsi</label>
                                            <textarea id="summernote" name="deskripsi" row="5">{{Request::old('deskripsi')}}</textarea>
                                            @error('deskripsi')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary" style="float: right">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    
    <script>
        $(function () {
            // Summernote
            $('#summernote').summernote()
        })
    </script>
@endsection