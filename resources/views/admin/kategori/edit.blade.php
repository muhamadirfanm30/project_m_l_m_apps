@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                        
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3 class="card-title">
                                        Update Data
                                    </h3>
                                </div>
                                <div class="col-md-2">
                                    <a href="{{ route('kategori') }}" class="btn btn-primary btn-sm" style="float: right">Back</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <form action="{{ route('kategori-update',['id'=>$id->id]) }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>Nama</label>
                                            <input type="text" name="judul" class="form-control" value="{{ $id->judul }}" placeholder="Judul ...">
                                            @error('judul')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>Foto</label>
                                            <input type="file" name="foto" class="form-control" placeholder="Photo ...">
                                            @if (!empty($id->foto))
                                                <a href="{{ url('storage/image-kategori/'.$id->foto) }}" target="_blank" rel="noopener noreferrer" class="badge badge-primary"><i class="fa fa-image"></i>  {{$id->foto}}</a>    
                                            @else
                                                <span class="badge badge-warning" style="color: white"><i class="fa fa-image"></i>  Gambar Tidak Ada</span>
                                            @endif
                                            @error('foto')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Deskripsi</label>
                                            <textarea id="summernote" name="deskripsi" row="5">{{ $id->deskripsi }}</textarea>
                                            @error('deskripsi')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary" style="float: right">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    
    <script>
        $(function () {
            // Summernote
            $('#summernote').summernote()
        })
    </script>
@endsection