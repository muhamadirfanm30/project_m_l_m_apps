@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('success') }}</strong>
                        </div>
                    @endif
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3 class="card-title">
                                        Produk
                                    </h3>
                                </div>
                                <div class="col-md-2">
                                    <a href="{{ url('admin/product/create') }}" class="btn btn-primary btn-sm" style="float: right">Tambah Produk</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="col-md-12">
                                <table id="userManagement" class="display table table-hover table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Nama Produk</th>
                                            <th>Harga</th>
                                            <th>Stok</th>
                                            <th style="display: none"></th>
                                           <th>Dibuat Tanggal</th>
                                            <th width="130px">Action</th>
                                        </tr>
                                        
                                    </thead>
                                    @foreach ($showProduk as $key => $produk)
                                        <tr>
                                            <td>{{ $produk->nama_produk }}</td>
                                            <td>
                                                @if (!empty($produk->harga_promo))
                                                    <strike>Rp.{{ number_format($produk->harga) }}</strike> &nbsp; <strong>Rp. {{ number_format($produk->harga_promo) }}</strong>
                                                @else
                                                    <strong>Rp. {{ number_format($produk->harga) }}</strong>
                                                @endif
                                            </td>
                                            <td>{{ $produk->stok }}</td>
                                            <td style="display: none">{{ $produk->id }}</td>
                                            <td>{{ date('d M Y H:i', strtotime($produk->created_at)) }}</td>
                                            <td>
                                                {{-- <form action="{{ url('admin/product/destroy/'.$produk->id) }}" method="POST">
                                                    @csrf --}}
                                                    <form action="{{ route('product.destroy',['id'=>$produk->id]) }}" method="POST" id="deleteData-{{$produk->id}}">
                                                        @csrf
                                                    </form>
                                                    <input type="hidden" name="get_id" id="get_id" value="{{ $produk->id }}">
                                                    <a href="{{ url('admin/product/update/'.$produk->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                                                    {{-- <a href="{{ url('admin/product/detail/'.$produk->id) }}" class="btn btn-success btn-sm"><i class="fa fa-eye"></i></a> --}}
                                                    <button type="submit" class="btn btn-danger btn-sm" onclick="deleteData({{ $produk->id }})"><i class="fa fa-trash"></i></button>
                                                {{-- </form> --}}
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#userManagement').DataTable( {
                processing: true,
                responsive: true,
                aaSorting: [[ 3, "desc" ]]
            }  );
        } );
    </script>

<script>
    function deleteData(id){
        swal({
            title: "Alert!",
            text: "Apakah anda yakin akan menghapus data tersebut?",
            type: 'error',
            showCancelButton: true,
            confirmButtonText: 'Ya, Saya yakin',
            cancelButtonText: 'Tidak',
        },function(result) {
            if(result){
                document.getElementById('deleteData-'+id).submit();
            }
        });
    }
</script>
@endsection