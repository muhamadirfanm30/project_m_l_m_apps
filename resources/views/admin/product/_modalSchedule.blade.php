<div class="modal fade" id="modal-default">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Schedule Produk Discount</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-sm-12">
                    <!-- text input -->
                    <div class="form-group">
                        <label>Start Date</label>
                        <div class="input-group date" id="reservationdate1" data-target-input="nearest">
                            <input type="text" name="valid_at" value="{{$showProduk->valid_at}}" id="validDate" data-toggle="datetimepicker" class="form-control datetimepicker-input" data-target="#reservationdate1" readonly/>
                            <div class="input-group-append" data-target="#reservationdate1" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <!-- text input -->
                    <div class="form-group">
                        <label>End Date</label>
                        <div class="input-group date" id="reservationdate2" data-target-input="nearest">
                            <input type="text" name="expired_at" value="{{$showProduk->expired_at}}" data-toggle="datetimepicker" id="expiredDate" class="form-control datetimepicker-input" data-target="#reservationdate2" readonly/>
                            <div class="input-group-append" data-target="#reservationdate2" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <!-- text input -->
                    <div class="form-group" style="text-align:right">
                        <label id="resetCalendar">Reset</label>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Save</button>
                {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
            </div>
        </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>