@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">

                    @if($errors->any())
                        <div class="alert alert-danger alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{$errors->first()}}</strong>
                        </div>
                    @endif
                        
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3 class="card-title">
                                        Tambah Produk
                                    </h3>
                                </div>
                                <div class="col-md-2">
                                    <a href="{{ url('admin/product/show') }}" class="btn btn-primary btn-sm" style="float: right">Back</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <form action="{{ url('admin/product/edit/'.$showProduk->id) }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body"> 
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Nama Produk</label>
                                                <input type="text" name="nama_produk" class="form-control" value="{{$showProduk->nama_produk}}" placeholder="Nama Produk ...">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Kategori</label>
                                                <select name="kategori_produk_id" class="form-control">
                                                    <option value="">Pilih Kategori</option>
                                                    @foreach($listKategori as $r)
                                                        <option value="{{ $r->id }}" {{ $showProduk->kategori_produk_id == $r->id ? 'selected' : '' }}>{{ $r->name }}</option>
                                                    @endforeach
                                                </select>
                                                @error('kategori_produk_id')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Stok</label>
                                                <input type="number" name="stok" class="form-control" value="{{$showProduk->stok}}" placeholder="Stok ...">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Photo</label>
                                                <input type="file" name="image_produk" class="form-control" placeholder="Photo ...">
                                                @if (!empty($showProduk->image_produk))
                                                    <a href="{{ url('storage/product-image/'.$showProduk->image_produk) }}" target="_blank" rel="noopener noreferrer" class="badge badge-primary"><i class="fa fa-image"></i>  {{$showProduk->image_produk}}</a>    
                                                @else
                                                    <span class="badge badge-warning" style="color: white"><i class="fa fa-image"></i>  Gambar Tidak Ada</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Harga Reguler</label>
                                                <input type="number" name="harga" class="form-control" value="{{$showProduk->harga}}" placeholder="Harga Reguler ...">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Harga Promo</label>
                                                <div class="row">
                                                    <div class="col-md-10">
                                                        <input type="number" name="harga_promo" value="{{$showProduk->harga_promo}}" class="form-control" placeholder="Harga Promo ...">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <a href="" data-toggle="modal" data-target="#modal-default" style="text-decoration: underline; color:blue">Schedule</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @include('admin.product._modalSchedule')
                                        <div class="col-sm-3">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Panjang (CM)</label>
                                                <input type="number" name="panjang" value="{{$showProduk->panjang}}" class="form-control" placeholder="Panjang">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Berat (Gram)</label>
                                                <input type="text" name="berat" value="{{$showProduk->berat}}" class="form-control" placeholder="Berat">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Lebar (CM)</label>
                                                <input type="number" name="lebar" value="{{$showProduk->lebar}}"  class="form-control" placeholder="Lebar">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Tinggi (CM)</label>
                                                <input type="text" name="tinggi" value="{{$showProduk->tinggi}}" class="form-control" placeholder="Tinggi">
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Deskripsi</label>
                                                <textarea id="summernote" name="deskripsi_produk">{{$showProduk->deskripsi_produk}}</textarea>
                                            </div>
                                        </div>
                                        {{-- <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="">Pilih Template</label>
                                                <select class="form-control" name="template_product" aria-label="Default select example">
                                                    <option selected>Open this select menu</option>
                                                    @foreach ($landingPage as $item)
                                                        <option value="{{ $item->id }}" {{$showProduk->template_product == $item->id ? 'selected' : null }}>{{$item->is_judul_landing_page}}</option>
                                                    @endforeach
                                                </select>
                                                @error('template_product')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div> --}}
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary" style="float: right">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    
    <script>
        $(document).ready(function() {
            $("#validDate").keypress(function(event) {event.preventDefault();});
            $("#expiredDate").keypress(function(event) {event.preventDefault();});

            $('#ststus-orders').DataTable();
            // Summernote
            $('#summernote').summernote()

            //Date range picker
            $('#reservationdate1').datetimepicker({
                format: 'YYYY-MM-DD'
            });
            $('#reservationdate2').datetimepicker({
                format: 'YYYY-MM-DD',
            });

            $('#resetCalendar').on("click",function(){
                $('#validDate').val("");
                $('#expiredDate').val("");
            })
        })
    </script>
@endsection