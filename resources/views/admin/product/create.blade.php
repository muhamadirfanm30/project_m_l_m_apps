@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                        
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3 class="card-title">
                                        Tambah Produk
                                    </h3>
                                </div>
                                <div class="col-md-2">
                                    <a href="{{ url('admin/product/show') }}" class="btn btn-primary btn-sm" style="float: right">Back</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <form action="{{ url('admin/product/store') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Nama Produk</label>
                                            <input type="text" name="nama_produk" class="form-control" value="{{Request::old('nama_produk')}}" onload="convertToSlug(this.value)" onkeyup="convertToSlug(this.value)" placeholder="Nama Produk ...">
                                            @error('nama_produk')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>slug</label>
                                            <input name="slug" class="form-control" id="to_slug" />
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Kategori</label>
                                            <select name="kategori_produk_id" class="form-control">
                                                <option value="">Pilih Kategori</option>
                                                @foreach($listKategori as $r)
                                                    <option value="{{ $r->id }}" {{ old('kategori_produk_id') == $r->id ? 'selected' : '' }}>{{ $r->name }}</option>
                                                @endforeach
                                            </select>
                                            @error('kategori_produk_id')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Stok</label>
                                            <input type="number" name="stok" class="form-control" value="{{Request::old('stok')}}" placeholder="Stok ...">
                                            @error('stok')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Photo</label>
                                            <input type="file" name="image_produk" class="form-control" placeholder="Photo ...">
                                            @error('image_produk')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Harga Reguler</label>
                                            <input type="number" name="harga" class="form-control" value="{{Request::old('harga')}}" placeholder="Hatga Regulrt ...">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Harga Promo</label>
                                            <div class="row">
                                                <div class="col-md-10">
                                                    <input type="number" name="harga_promo" value="{{Request::old('harga_promo')}}" class="form-control" placeholder="Promo ...">
                                                </div>
                                                <div class="col-md-2">
                                                    <a href="" data-toggle="modal" data-target="#modal-default" style="text-decoration: underline; color:blue">Schedule</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Panjang (CM)</label>
                                            <input type="number" name="panjang" value="{{Request::old('panjang')}}" class="form-control" placeholder="Panjang">
                                            @error('panjang')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Berat (Gram)</label>
                                            <input type="text" name="berat" value="{{Request::old('berat')}}" class="form-control" placeholder="Berat">
                                            @error('berat')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Lebar (CM)</label>
                                            <input type="number" name="lebar" value="{{Request::old('lebar')}}"  class="form-control" placeholder="Lebar">
                                            @error('lebar')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Tinggi (CM)</label>
                                            <input type="text" name="tinggi" value="{{Request::old('tinggi')}}" class="form-control" placeholder="Tinggi">
                                            @error('tinggi')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <!-- text input -->
                                        <div class="form-group">
                                            <label>Deskripsi</label>
                                            <textarea id="summernote" name="deskripsi_produk">{{Request::old('deskripsi_produk')}}</textarea>
                                            @error('deskripsi_produk')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                    </div>
                                    {{-- <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Pilih Template</label>
                                            <select class="form-control" name="template_product" aria-label="Default select example">
                                                <option selected>Open this select menu</option>
                                                @foreach ($landingPage as $item)
                                                    <option value="{{ $item->id }}" {{Request::old("type") == $item->id ? 'selected' : null }}>{{$item->is_judul_landing_page}}</option>
                                                @endforeach
                                            </select>
                                            @error('template_product')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                    </div> --}}
                                </div>
                            </div>
                            <div class="modal fade" id="modal-default">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Schedule Produk Discount</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="col-sm-12">
                                                <!-- text input -->
                                                <div class="form-group">
                                                    <label>Start Date</label>
                                                    <div class="input-group date" id="reservationdate1" data-target-input="nearest">
                                                        <input type="text" name="valid_at" value="" id="validDate" data-toggle="datetimepicker" class="form-control datetimepicker-input" data-target="#reservationdate1" readonly>
                                                        <div class="input-group-append" data-target="#reservationdate1" data-toggle="datetimepicker">
                                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <!-- text input -->
                                                <div class="form-group">
                                                    <label>End Date</label>
                                                    <div class="input-group date" id="reservationdate2" data-target-input="nearest">
                                                        <input type="text" name="expired_at" value="" id="expiredDate" data-toggle="datetimepicker" class="form-control datetimepicker-input" data-target="#reservationdate2" readonly/>
                                                        <div class="input-group-append" data-target="#reservationdate2" data-toggle="datetimepicker">
                                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <!-- text input -->
                                                <div class="form-group" style="text-align:right">
                                                    <label id="resetCalendar">Reset</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-primary" data-dismiss="modal">Save</button>
                                            {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
                                        </div>
                                    </div>
                                  <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            @foreach ($message as $msg)
                                <input type="hidden" name="message[]" value="{{$msg->message}}">
                            @endforeach
                            
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary" style="float: right">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    
    <script>
        $(document).ready(function() {
            $("#validDate").keypress(function(event) {event.preventDefault();});
            $("#expiredDate").keypress(function(event) {event.preventDefault();});
            
            $('#to_slug').prop('readonly', true);
            $('#model_id').hide();
            $('#ststus-orders').DataTable();
            $('#summernote').summernote()

            //Date range picker
            $('#reservationdate1').datetimepicker({
                format: 'YYYY-MM-DD'
            });
            $('#reservationdate2').datetimepicker({
                format: 'YYYY-MM-DD'
            });

            $('#resetCalendar').on("click",function(){
                $('#validDate').val("");
                $('#expiredDate').val("");
            })
        });

        function convertToSlug(str) {
            elem = 'to_slug';
            Helper.toSlug(str, elem);
        }
    </script>
@endsection