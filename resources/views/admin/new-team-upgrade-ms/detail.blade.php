@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('success') }}</strong>
                        </div>
                    @endif
                    @if(session()->has('error'))
                        <div class="alert alert-danger alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('error') }}</strong>
                        </div>
                    @endif
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-9">
                                    <h3 class="card-title">
                                        Detail User Upgrade MS
                                    </h3>
                                </div>
                                <div class="col-md-3">
                                    {{-- <a href="{{ url('admin/user/create') }}" class="btn btn-primary btn-sm" style="float: right">Approve</a> --}}
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <!-- text input -->
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="text" name="email" id="email" class="form-control" value="{{$user->email}}"  placeholder="First Name ..." disabled>
                                        @error('email')
                                            <span style="color:red"><strong>{{ $message }}</strong></span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <!-- text input -->
                                    <div class="form-group">
                                        <label>Member Id</label>
                                        <input type="text" name="membership_id" id="membership_id" class="form-control" value="{{$user->membership_id}}"  placeholder="First Name ..." disabled>
                                        @error('membership_id')
                                            <span style="color:red"><strong>{{ $message }}</strong></span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <!-- text input -->
                                    <div class="form-group">
                                        <label>First Name</label>
                                        <input type="text" name="first_name" id="first_name" class="form-control" value="{{$user->first_name}}"  placeholder="First Name ..." disabled>
                                        @error('first_name')
                                            <span style="color:red"><strong>{{ $message }}</strong></span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <!-- text input -->
                                    <div class="form-group">
                                        <label>Last Name</label>
                                        <input type="text" name="last_name" id="last_name" class="form-control" value="{{$user->last_name}}"  placeholder="Last Name ..." disabled>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <!-- text input -->
                                    <div class="form-group">
                                        <label>Membership Status</label>
                                        <input type="text" name="membersip_status" id="membersip_status" class="form-control" value="{{$user->membersip_status}}" placeholder="No KTP ..." disabled>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <!-- text input -->
                                    <div class="form-group">
                                        <label>Kode Pos</label>
                                        <input type="text" name="phone" id="phone" class="form-control" value="{{$user->phone}}"  placeholder="Phone..." disabled>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <!-- text input -->
                                    <div class="form-group">
                                        <label>Jenis Kelamin</label>
                                        <input type="text" name="gender" id="gender" class="form-control" value="{{$user->gender}}"  placeholder="JK ..." disabled>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <!-- text input -->
                                    <div class="form-group">
                                        <label>WhatsApp No</label>
                                        <input type="text" name="whatsapp_no" id="whatsapp_no"  class="form-control" value="{{$user->whatsapp_no}}" placeholder="WhatsApp No" disabled>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <!-- text input -->
                                    <div class="form-group">
                                        <label>Alamat</label>
                                        <textarea class="form-control" name="deskripsi_produk" id="address" cols="10" rows="3" disabled>{{$user->address}}</textarea>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <!-- text input -->
                                    <div class="form-group">
                                        <label>Nomor KTP</label>
                                        <input type="text" name="no_ktp" id="no_ktp"  class="form-control" value="{{$user->no_ktp}}" placeholder="WhatsApp No" disabled>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <p><strong>Foto KTP (Kartu Tanda Penduduk)</strong></p>
                                        <p>* Pastikan Foto KTP Anda Terlihat Jelas, Bisa Dibaca dan Tidak Terpotong. Format JPEG, PNG, JPG, Maximal Size 5 MB</p>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <input type='file' name="foto_ktp" id="imgInp" disabled /><br><br>
                                                @if (!empty($user->foto_ktp))
                                                    <img id="blah" src="{{ url('storage/foto-ktp/'.$user->foto_ktp) }}" alt="your image" width="90%"/><br>
                                                    <small>Foto KTP User</small>
                                                @else
                                                    <img id="blah" src="" width="90%"/>
                                                @endif
                                                
                                            </div>
                                            <div class="col-md-8">
                                                <center><img src="{{ asset('ktp.png') }}" alt="" width="90%"></center>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>No Id Mobile Stokiest (MS) Sponsor Langsung <span style="color: red">*</span></label>
                                        <input type="text" id="no_id_sponsor" name="no_id_sponsor" class="form-control" value="{{!empty($user->get_posisi) ? $user->get_posisi->user_sponsor_name : ''}}" placeholder="No Id Mobile Stokiest (MS) Sponsor Langsung ..." disabled>
                                        @error('no_id_sponsor')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Nama Id Mobile Stokiest (MS) Sponsor Langsung <span style="color: red">*</span></label>
                                        <input type="text" id="nama_id_sponsor" name="nama_id_sponsor" class="form-control" value="{{!empty($user->get_posisi) ? $user->get_posisi->user_sponsor_status : ''}}" placeholder="Nama Id Mobile Stokiest (MS) Sponsor Langsung ..." disabled>
                                        @error('nama_id_sponsor')
                                            <span style="color:red"><small>{{ $message }}</small></span>
                                        @enderror
                                    </div>
                                </div>

                                {{-- <div class="row"> --}}
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>No MS Id Upline <span style="color: red">*</span></label>
                                            <input type="text" id="no_id_sponsor_upline" name="no_id_sponsor_upline" value="{{!empty($user->sponsor_upline_id) ? $user->sponsor_upline_id : ''}}" class="form-control" placeholder="No MS Id Upline ..." disabled>
                                            @error('no_id_sponsor_upline')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Nama Upline <span style="color: red">*</span></label>
                                            <input type="text" id="nama_upline" name="sponsor_upline_name" value="{{!empty($user->sponsor_upline_name) ? $user->sponsor_upline_name : ''}}" class="form-control" placeholder="Nama Upline ..." disabled>
                                            @error('nama_upline')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Kaki <span style="color: red">*</span></label>
                                            <select name="posisi" id="posisi" class="form-control" disabled>
                                                @if (!empty($user->get_posisi))
                                                    @if ($user->get_posisi->posisi == 'Kiri')
                                                        <option value="Kiri">Kiri</option>
                                                        <option value="Kanan">Kanan</option>
                                                    @elseif($user->get_posisi->posisi == 'Kanan')
                                                        <option value="Kanan">Kanan</option>
                                                        <option value="Kiri">Kiri</option>
                                                    @else
                                                        <option value="">Pilih Posisi</option>
                                                        <option value="Kiri">Kiri</option>
                                                        <option value="Kanan">Kanan</option>
                                                    @endif
                                                    {{-- <option value="">Pilih Posisi</option>
                                                    <option value="Kiri" {{ $user->get_posisi->posisi == 'Kiri' ? 'checked' : '' }}>Kiri</option>
                                                    <option value="Kanan" {{ $user->get_posisi->posisi == 'Kanan' ? 'checked' : '' }}>Kanan</option> --}}
                                                @else
                                                    <option value="">Pilih Posisi</option>
                                                    <option value="Kiri">Kiri</option>
                                                    <option value="Kanan">Kanan</option>
                                                @endif
                                                
                                            </select>
                                            @error('posisi')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                <div class="col-md-12" id="btn_update" hidden>
                                    <button type="submit" class="btn btn-primary btn-sm btn-block">
                                        <i class="fa fa-check">&nbsp; Save</i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="modal-default">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Approve Mobile Stokiest</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form action="{{url('/admin/new-team-upgrade-ms/my-team-approve-ms/'.$user->id)}}" method="post">
                                        @csrf
                                        <div class="modal-body">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label>Memberships Status</label>
                                                    <input type="text" name="membersip_status" value="MS(Mobile Stokiest)"  class="form-control" placeholder="Memberships Status" readonly>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label>Member ID</label>
                                                    <input type="text" name="membership_id"  class="form-control" placeholder="Memberships ID">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Save changes</button>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="row">
                                        <div class="col-md-4" id="btn_approve">
                                            <a href="{{ url('admin/user/create') }}" class="btn btn-primary btn-sm btn-block" data-toggle="modal" data-target="#modal-default" style="float: right">
                                                <i class="fa fa-check">&nbsp; Approve</i>
                                            </a>
                                        </div>
                                        <div class="col-md-4" id="btn_buka_update">
                                            <a href="{{url('admin/new-team-upgrade-ms/edit/'.$user->id)}}" class="btn btn-warning btn-sm btn-block" style="color: white">
                                                <i class="fa fa-edit" >&nbsp; Update</i>
                                            </a>
                                        </div>
                                        <div class="col-md-4" id="btn_hubungi">
                                            <a href="" class="btn btn-success btn-sm btn-block" style="float: right">
                                                <i class="fab fa-whatsapp">&nbsp; Hubungi</i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#userManagement').DataTable();
            $('#summernote').summernote();
        } );
    </script>
@endsection