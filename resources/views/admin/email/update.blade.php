


@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3 class="card-title">
                                        Create Users
                                    </h3>
                                </div>
                                <div class="col-md-2">
                                    <a href="{{ url('admin/email-template/show') }}" class="btn btn-primary btn-sm" style="float: right">Back</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <form action="{{ url('admin/email-template/edit/'.$getListEmail->id) }}" method="post">
                            @csrf
                            <div class="card-body">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Subject</label>
                                                <input type="text" name="subject" value="{{$getListEmail->subject}}" class="form-control" placeholder="Subject ...">
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <!-- text input -->
                                            <div class="row">
                                                <div class="col-md-10">
                                                    <div class="form-group">
                                                        <label>Content</label>
                                                        <textarea id="summernote" cols="30" rows="5" name="content">{{$getListEmail->content}}</textarea>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <h6>Paramenter : </h6>
                                                    <small>[{FIRST_NAME}]</small>
                                                    <small>[{LAST_NAME}]</small>
                                                    <small>[{PHONE_NUMBER}]</small>
                                                    <small>[{ORDER_ID}]</small>
                                                    <small>[{ORDER_DATE}]</small>
                                                    <small>[{ORDER_STATUS}]</small>
                                                    <small>[{ORDER_METOD}]</small>
                                                    <small>[{KATEGORI}]</small>
                                                    <small>[{PRICE}]</small>
                                                    <small>[{EMAIL_MEMBER}]</small>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" onclick="loader()" class="btn btn-primary" style="float: right">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#ststus-orders').DataTable();
        } );

        
        function loader(){
            Helper.loadingStart();
        }

        $(function () {
            // Summernote
            $('#summernote').summernote()
            $('#summernote1').summernote()

            //Date range picker
            $('#reservationdate').datetimepicker({
                format: 'YYYY-MM-DD'
            });
        })
    </script>
@endsection