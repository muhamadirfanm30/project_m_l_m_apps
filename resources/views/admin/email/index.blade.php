@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('success') }}</strong>
                        </div>
                    @endif
                    @if(session()->has('error'))
                        <div class="alert alert-danger alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('error') }}</strong>
                        </div>
                    @endif
                    
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-12">
                                    {{-- <h3 class="card-title">
                                        Show List Users
                                    </h3> --}}
                                    <nav>
                                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                            <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Email Template</a>
                                            <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Logo Email</a>
                                            {{-- <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Contact</a> --}}
                                        </div>
                                    </nav><br>
                                    <div class="tab-content" id="nav-tabContent">
                                        <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                            <div class="card card-outline card-info">
                                                <div class="card-header">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <h3 class="card-title">
                                                                Show List Email
                                                            </h3>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- /.card-header -->
                                                <div class="card-body">
                                                    <div class="col-md-12">
                                                        <table id="userManagement" class="display table table-hover table-bordered" style="width:100%">
                                                            <thead>
                                                                <tr>
                                                                    <th>No</th>
                                                                    <th>Tipe Email</th>
                                                                    <th>Action</th>
                                                                </tr>
                                                                
                                                            </thead>
                                                            @foreach ($getListEmail as $key => $even)
                                                                <tr>
                                                                    <td>{{ $no++ }}</td>
                                                                    <td>{{ $even->subject }}</td>
                                                                    <td>
                                                                        <a href="{{ url('admin/email-template/update/'.$even->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="card-footer">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                                            <div class="card card-outline card-info">
                                                <div class="card-header">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <h3 class="card-title">
                                                                Logo Email
                                                            </h3>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- /.card-header -->
                                                <div class="card-body">
                                                    <div class="col-md-12">
                                                        <form action="{{ url('admin/email-template/create-update-logo') }}" method="post" enctype="multipart/form-data">
                                                            @csrf
                                                            <div class="form-group">
                                                                <label for="">Logo</label>
                                                                <input type="file" name="logo" class="form-control">
                                                                @if (!empty($cekData->logo))
                                                                    <a href="{{ url('storage/logo-email/'.$cekData->logo) }}" target="_blank" rel="noopener noreferrer" class="badge badge-primary"><i class="fa fa-image"></i>  {{$cekData->logo}}</a>    
                                                                @else
                                                                    <span class="badge badge-warning" style="color: white"><i class="fa fa-image"></i>  Gambar Tidak Ada</span>
                                                                @endif
                                                            </div>
                                                            <button type="submit" class="btn btn-primary btn-block btn-sm">Simpan</button>
                                                        </form>
                                                    </div>
                                                </div>
                                                <div class="card-footer">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">...</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#userManagement').DataTable();
        } );
    </script>
@endsection