@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('success') }}</strong>
                        </div>
                    @endif
                    <div id='loader'>
                        <img src='https://i.pinimg.com/originals/d1/ab/97/d1ab977eb069821741f072bef1b77ded.gif' width='32px' height='32px'>
                    </div>
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3 class="card-title">
                                        New MS Upgrade
                                    </h3>
                                </div>
                                <div class="col-md-2">
                                </div>
                            </div>
                        </div>
                        
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="col-md-12">
                                <table id="userManagement" class="display table table-hover table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Memberships Status</th>
                                            <th>Member ID</th>
                                            <th>Dibuat Pada</th>
                                            <th>Status</th>
                                            <td style="display: none"></td>
                                            <th width="130px">Action</th>
                                        </tr>
                                    </thead>
                                   @foreach ($dataMS as $k => $v)
                                       <tr>
                                           <td>{{!empty($v->first_name) ? $v->first_name : '-'}} {{!empty($v->last_name) ? $v->last_name : '-'}} <span class="badge badge-warning" style="color: white">{{ $v->notif_no_read_new_member_request != null ? 'new' : '' }}</span></td>
                                           <td>{{!empty($v->email) ? $v->email : '-'  }}</td>
                                           <td>
                                                <span class="badge badge-primary">
                                                    <strong style="color:white">{{!empty($v->membersip_status) ? $v->membersip_status : '-'  }}</strong>
                                                </span>
                                            </td>
                                           <td>
                                               <span class="badge badge-primary">
                                                   <strong style="color:white">{{!empty($v->membership_id) ? $v->membership_id : '-'  }}</strong>
                                                </span>
                                            </td>
                                            <td>{{ date('d M Y', strtotime($v->created_at)) }}</td>
                                           <td> <span class="badge badge-danger">{{$v->is_tmp_user == 1 ? 'Menunggu Persetujuan Admin' : ''}}</span></td>
                                           <td style="display: none">{{ $v->id }}</td>
                                           <td>
                                                <a href="{{url('/admin/new-member-request/update/'.$v->id)}}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Update">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                                {{-- <button type="button" data-id ="{{$v->id}}" onclick="approveUsers()" class="btn btn-warning btn-sm" id="btn_confrim_part" data-toggle="tooltip" data-html="true" title="Terima User"><i class="fa fa-check"></i></button> --}}
                                           </td>
                                       </tr>
                                   @endforeach
                                </table>
                            </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.5.2/bootbox.min.js"></script>
    <script>
        $(document).ready(function() {
            var table = $('#userManagement').DataTable({
                processing: true,
                responsive: true,
                aaSorting: [[ 6, "desc" ]]
            }  );
            $('#example').tooltip('show');
        });

        function approveUsers() {
            var id = $('#btn_confrim_part').attr('data-id')
            Helper.confirm(function(){
                Helper.loadingStart()
                $.ajax({
                    url: "/admin/new-member-request/approve-new-memner/"+id,
                    type: "POST",
                    dataType: "html",
                    success: function () {
                        Helper.loadingStop();
                        swal("Done!", "It was succesfully deleted!", "success");
                        swal({
                            title: "Sukses!",
                            text: "",
                            type: 'success'
                        }, function() {
                            window.location.href='/admin/new-member-request/show';
                        });
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        Helper.loadingStop();
                        swal("Approve User Error", "Please try again", "error");
                    }
                });
            })
        }

    </script>

{{-- swal({
    title: "",
    text: "Apakah anda yakin, Sudah Meriksa dengan valid data user ini ?",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Approve",
    closeOnConfirm: false
}, function (isConfirm) {
    if (!isConfirm) return;
    
    $.ajax({
        url: "/admin/new-member-request/approve-new-memner/"+id,
        type: "POST",
        dataType: "html",
        beforeSend: function(){
            // Show image container
            $("#loader").show();
        },
        success: function () {
            swal("Done!", "It was succesfully deleted!", "success");
            swal({
                title: "Sukses!",
                text: "",
                type: 'success'
            }, function() {
                window.location.href='/admin/new-member-request/show';
            });
        },
        error: function (xhr, ajaxOptions, thrownError) {
            swal("Approve User Error", "Please try again", "error");
        }
    });
}); --}}
@endsection