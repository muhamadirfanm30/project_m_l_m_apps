@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    {{-- @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                            </ul>
                        </div>
                    @endif --}}
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3 class="card-title">
                                        Create Users
                                    </h3>
                                </div>
                                <div class="col-md-2">
                                    <a href="{{ url('/admin/new-member-request/show') }}" class="btn btn-primary btn-sm" style="float: right">Back</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <form action="{{ url('admin/new-member-request/update-data-user/'.$dataEdit->id) }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input type="email" value="{{$dataEdit->email}}" name="email" class="form-control" placeholder="Email ..." disabled>
                                                @error('email')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>First Name</label>
                                                <input type="text" value="{{$dataEdit->first_name}}" name="first_name" class="form-control" placeholder="First Name ...">
                                                @error('first_name')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Last Name</label>
                                                <input type="text" value="{{$dataEdit->last_name}}" name="last_name" class="form-control" placeholder="Last Name ...">
                                                @error('last_name')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Memberships ID</label>
                                                <input type="text" name="membership_id" class="form-control" readonly value="{{!empty($dataEdit->membership_id) ? $dataEdit->membership_id : ''}}" placeholder="Memberships ID ...">
                                                {{-- <input type="hidden" name="is_upgrade_ms" value="3"> --}}
                                                {{-- 0 menunggu approve admin --}}
                                                {{-- 1 sudah jadi ms --}}
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Nomor Whatsapp</label>
                                                <input type="text" id="nomor_ponsel" value="{{$dataEdit->phone}}" name="phone" class="form-control" placeholder="Nomor Whatsapp ...">
                                                @error('phone')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                        {{-- <div class="col-sm-6">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Password</label>
                                                <input type="password" name="password" class="form-control" placeholder="Enter ...">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>confirm password</label>
                                                <input type="confirm-password" name="confirm-password" class="form-control" placeholder="Enter ...">
                                            </div>
                                        </div> --}}
                                        {{-- <div class="col-sm-6">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Nomor KTP</label>
                                                <input type="text" name="no_ktp" class="form-control" value="{{!empty($dataEdit->no_ktp) ? $dataEdit->no_ktp : ''}}" placeholder="Nomor KTP ...">
                                                @error('no_ktp')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div> --}}
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Memberships</label>
                                                <input type="text" name="membersip_status" class="form-control" readonly value="{{!empty($dataEdit->membersip_status) ? $dataEdit->membersip_status : ''}}" placeholder="MemberShip ...">
                                                
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="exampleFormControlSelect1">Jenis Kelamin</label>
                                                <select class="form-control"  name="gender" id="exampleFormControlSelect1">
                                                    <option value="Laki Laki" {{ $dataEdit->gender == "Laki Laki" ? "selected" : '' }}>Laki Laki</option>
                                                    <option value="Perempuan" {{ $dataEdit->gender == "Perempuan" ? "selected" : '' }}>Perempuan</option>
                                                    <option value="Lain Nya" {{ $dataEdit->gender == "Lain Nya" ? "selected" : '' }}>Lain nya</option>
                                                </select>
                                                @error('gender')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Alamat Lengkap</label>
                                                <textarea name="address" class="form-control" id="" cols="30" rows="10">{{!empty($dataEdit->address) ? $dataEdit->address : ''}}</textarea>
                                                @error('address')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Kode Pos</label>
                                                <input type="text" name="kode_pos" class="form-control" value="{{!empty($dataEdit->kode_pos) ? $dataEdit->kode_pos : ''}}" placeholder="Kode Pos ...">
                                                @error('kode_pos')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                        {{-- <div class="col-sm-4"> 
                                            <div class="form-group">
                                                <label>Nama Bank</label>
                                                <input type="text" name="rekening_bank" class="form-control" value="{{!empty($dataEdit->rekening_bank) ? $dataEdit->rekening_bank : ''}}" placeholder="Rekening Bank ...">
                                                @error('rekening_bank')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Nama Pemilik Rekening</label>
                                                <input type="text" name="nama_pemilik_rekening" class="form-control" value="{{!empty($dataEdit->nama_pemilik_rekening) ? $dataEdit->nama_pemilik_rekening : ''}}" placeholder="Nama Pemilik Rekening ...">
                                                @error('nama_pemilik_rekening')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Nomor Rekening</label>
                                                <input type="text" name="nomor_rekening" class="form-control" value="{{!empty($dataEdit->nomor_rekening) ? $dataEdit->nomor_rekening : ''}}" placeholder="Nomor Rekening ...">
                                                @error('nomor_rekening')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div> --}}
                                        {{-- <div class="col-md-12">
                                            <small style="color: red">* Nama Pemilik Rekening dan nama Pemilik KTP harus sama. Perbedaan Nama Rekening dan KTP akan menyebabkan Bonus tidak akan di Transfer.</small>
                                        </div> --}}

                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label>No KTP</label>
                                                <input type="text" name="no_ktp" value="{{!empty($dataEdit->no_ktp) ? $dataEdit->no_ktp : ''}}" class="form-control" placeholder="Masukan Nomor KTP...">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <p><strong>Foto KTP (Kartu Tanda Penduduk)</strong></p>
                                                <p>* Pastikan Foto KTP Anda Terlihat Jelas, Bisa Dibaca dan Tidak Terpotong. Format JPEG, PNG, JPG, Maximal Size 5 MB</p>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <input type='file' name="foto_ktp" id="imgInp" /><br><br>
                                                        @if (!empty($dataEdit->foto_ktp))
                                                            <img id="blah" src="{{ url('storage/foto-ktp/'.$dataEdit->foto_ktp) }}" alt="your image" width="90%"/><br>
                                                            <small>Foto KTP User</small>
                                                        @else
                                                            <img id="blah" src="" width="90%"/>
                                                        @endif
                                                        
                                                    </div>
                                                    <div class="col-md-8">
                                                        <center><img src="{{ asset('ktp.png') }}" alt="" width="90%"></center>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <input type="hidden" name="roles" value="Customer">
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Status</label>
                                                <select class="form-control" name="status">
                                                    {{-- @if ($dataEdit->status == 0)
                                                        <option value="0">Active</option>
                                                        <option value="1">In Active</option>
                                                    @elseif($dataEdit->status == 1)
                                                        <option value="1">In Active</option>
                                                        <option value="0">Active</option>
                                                    @else --}}
                                                        <option value="0">Active</option>
                                                        <option value="1">In Active</option>
                                                    {{-- @endif --}}
                                                </select>
                                                @error('nomor_rekening')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary" style="float: right">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>
        $(document).ready(function() {
            onlyNumber('#nomor_ponsel');
            $('#ststus-orders').DataTable();
        } );
    </script>
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function(e) {
                $('#blah').attr('src', e.target.result);
                }
                
                reader.readAsDataURL(input.files[0]); // convert to base64 string
            }
        }

        $("#imgInp").change(function() {
            readURL(this);
        });
    </script>
@endsection