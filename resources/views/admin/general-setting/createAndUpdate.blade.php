@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('success') }}</strong>
                        </div>
                    @endif
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3 class="card-title">
                                        Pengaturan Umum
                                    </h3>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <form action="{{ url('admin/general-setting/update') }}" method="post"  enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <!-- text input -->
                                            @foreach ($cek as $val)
                                                <div class="form-group">
                                                    <label>{{$val->desc}}</label>
                                                    <input type="hidden" name="id_setting[]" value="{{ $val->id }}">
                                                    @if ($val->name == 'setting_expired_payment')
                                                        <input type="text" name="value[]" class="form-control" id="expired_pay" placeholder="{{$val->desc}} ..." value="{{ $val->value }}">
                                                        <span style="color:red">Durasi Pembayaran dalam bentuk (Jam/Hour) </span>
                                                        <input type="hidden" name="type[]" value="jam - hour" />
                                                    @elseif($val->name == 'akumulasi_upgrade_ms')
                                                        <input type="text" name="value[]" class="form-control" id="akumulasi_upgrade" placeholder="{{$val->desc}} ..." value="{{ $val->value }}">
                                                        <span style="color:red">Dalam Bentuk Rupiah (Rp)</span>
                                                        <input type="hidden" name="type[]" value="Rupiah" />
                                                    @elseif($val->name == 'kota_asal')
                                                        <select class="js-example-basic-single" name="value[]" id="kota_id" style="width:100%">
                                                                <option>Pilih Kota</option>
                                                                @foreach ($city as $get_city)
                                                                    <option value="{{ $get_city->data_rajaongkir_results_city_id }}" {{ $val->value == $get_city->data_rajaongkir_results_city_id ? 'selected' : '' }}>{{ $get_city->data_rajaongkir_results_city_name }}</option>
                                                                @endforeach
                                                        </select>
                                                        <input type="hidden" name="type[]" name="get_kota" id="get_kota" />
                                                    @elseif($val->name == 'deskripsi_tutorial_order')
                                                        <textarea id="summernote1" name="value[]">{{ $val->value }}</textarea>
                                                        <input type="hidden" name="type[]" value="-" />
                                                    @elseif($val->name == 'desc_vidio_live_training')
                                                        <textarea id="summernote2" name="value[]">{{ $val->value }}</textarea>
                                                        <input type="hidden" name="type[]" value="-" />
                                                    @elseif($val->name == 'fb_pixel')
                                                        <textarea name="value[]" id="" cols="20" rows="5" class="form-control">{{ $val->value }}</textarea>
                                                        <input type="hidden" name="type[]" value="-" />
                                                    @else
                                                        <input type="text" name="value[]" class="form-control" placeholder="{{$val->desc}} ..." value="{{ $val->value }}">
                                                        <input type="hidden" name="type[]" value="-" />
                                                    @endif
                                                    @error('value')
                                                        <span style="color:red"><small>{{ $message }}</small></span>
                                                    @enderror
                                                </div>  
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary" style="float: right">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @include('admin.general-setting._style-css')
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.60/inputmask/jquery.inputmask.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.js-example-basic-single').select2();
        });
        $(function () {
            // Summernote
            $('#summernote').summernote()
            $('#summernote1').summernote()
            $('#summernote2').summernote()
            $('#summernote3').summernote()
        });
        (function($) {
            $.fn.inputFilter = function(inputFilter) {
                return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                } else {
                    this.value = "";
                }
                });
            };
            }(jQuery));

            $("#expired_pay").inputFilter(function(value) {
                return /^-?\d*$/.test(value); 
            });
            $("#akumulasi_upgrade").inputFilter(function(value) {
                return /^-?\d*$/.test(value); 
            });

            $(document).ready(function(){

                $('#kota_id').change(function() {
                    var get_city =  $("#kota_id option:selected").text()
                    $('#get_kota').val(get_city);
                })
            })
    </script>
@endsection
