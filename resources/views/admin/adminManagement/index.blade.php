@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3 class="card-title">
                                        Show List Admin
                                    </h3>
                                </div>
                                <div class="col-md-2">
                                    <a href="{{ url('admin/list-admin/create') }}" class="btn btn-primary btn-sm" style="float: right">Create Admin</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="col-md-12">
                                <table id="userManagement" class="display table table-hover table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>WhatsApp No</th>
                                            <th>Status</th>
                                            <th>Roles</th>
                                            <th>Dibuat Tanggal</th>
                                            <th style="display: none"></th>
                                            <th width="130px">Action</th>
                                        </tr>
                                    </thead>
                                    @foreach ($data as $key => $user)
                                        <tr>
                                            <td>{{ $user->first_name.' '.$user->last_name }}</td>
                                            <td>{{ $user->email }}</td>
                                            <td>{{ $user->phone }}</td>
                                            @if ($user->status == 0)
                                                <td> <label class="badge badge-success">Active</label></td>
                                            @else
                                                <td> <label class="badge badge-danger">In Active</label></td>
                                            @endif
                                            <td>
                                                {{-- @if(!empty($user->getRoleNames()))
                                                    @foreach($user->getRoleNames() as $v)
                                                        <label class="badge badge-success">{{ $v }}</label>
                                                    @endforeach
                                                @endif --}}
                                                {{$user->roles}}
                                            </td>
                                            <td>{{ date('d M Y', strtotime($user->created_at)) }}</td>
                                            <td style="display: none">{{ $user->id }}</td>
                                            <td>
                                                {{-- <form action="{{ url('admin/list-admin/destroy/'.$user->id) }}" method="POST">
                                                    @csrf --}}
                                                    <input type="hidden" name="get_id" id="get_id" value="{{ $user->id }}">
                                                    <a href="{{ url('admin/list-admin/update/'.$user->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                                                    <a href="https://api.whatsapp.com/send/?phone={{$user->phone}}" class="btn btn-success btn-sm"><i class="fab fa-whatsapp"></i></a>
                                                    <button type="submit" class="btn btn-danger btn-sm" id="btn-delete"><i class="fa fa-trash"></i></button>
                                                {{-- </form> --}}
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#userManagement').DataTable({
                processing: true,
                responsive: true,
                aaSorting: [[ 6, "desc" ]]
            }  );
        } );

        $(document).on('click', '#btn-delete', function(){
            swal({
                title: "Apa anda yakin?",
                text: "Akan Menghapus Data Admin ini ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ya, Saya Yakin",
                cancelButtonText: 'Tidak',
                closeOnConfirm: false,
                showCloseButton: true
            }, function (isConfirm) {
                if (!isConfirm) return;
                id = $('#get_id').val();
                Helper.loadingStart()
                $.ajax({
                    url: '/admin/list-admin/destroy/' + id,
                    type: 'post',
                    success: function(resp) {
                        Helper.loadingStop()
                        console.log(resp)
                        swal({
                        title: "Sukses!",
                            text: "Data Admin Berhasil di hapus",
                            type: 'success'
                        }, function() {
                            window.location.href='/admin/list-admin/show';
                        });
                    },
                    error:function(a,v,c){
                        Helper.loadingStop()
                        var msg = JSON.parse(a.responseText);
                        swal({
                        title: "Ups!",
                            text: 'Terjadi kesalahan saat menghapus Data Admin',
                            type: 'error'
                        });
                    }
                })
            });
            
        });
    </script>
@endsection