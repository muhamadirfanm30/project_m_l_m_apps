@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3 class="card-title">
                                        Create Users
                                    </h3>
                                </div>
                                <div class="col-md-2">
                                    <a href="{{ url('admin/list-admin/show') }}" class="btn btn-primary btn-sm" style="float: right">Back</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <form action="{{ url('admin/list-admin/edit/'.$user->id) }}" method="post">
                            @csrf
                            <div class="card-body">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>First Name</label>
                                                <input type="text" value="{{$user->first_name}}" name="first_name" class="form-control" placeholder="First Name ...">
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Last Name</label>
                                                <input type="text" value="{{$user->last_name}}" name="last_name" class="form-control" placeholder="Last Name ...">
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input type="email" value="{{$user->email}}" name="email" class="form-control" placeholder="Email ...">
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Nomor Whatsapp</label>
                                                <input class="form-control" value="{{$user->phone}}" id="phone" name="phone" placeholder="Whatsapp no"  data-inputmask="'alias': 'phonebe'">
                                            
                                            </div>
                                        </div>
                                        {{-- <div class="col-sm-12">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Password</label>
                                                <input type="password" name="password" class="form-control" placeholder="Enter ...">
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>confirm password</label>
                                                <input type="confirm-password" name="confirm-password" class="form-control" placeholder="Enter ...">
                                            </div>
                                        </div> --}}
                                        <div class="col-sm-12">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Role</label>
                                                <select multiple class="form-control" name="roles" id="exampleFormControlSelect2">
                                                    {{-- <option>1</option> --}}
                                                    @foreach ($roles as $role)
                                                        <option value="{{ $role->name }}" {{ $user->roles == $role->name ? 'selected' : '' }}>{{ $role->name }}</option>
                                                    @endforeach
                                                    
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Status</label>
                                                <select class="form-control" name="status">
                                                    @if ($user->status == 0)
                                                        <option value="0">Active</option>
                                                        <option value="1">In Active</option>
                                                    @elseif($user->status == 1)
                                                        <option value="1">In Active</option>
                                                        <option value="0">Active</option>
                                                    @else
                                                        <option value="0">Active</option>
                                                        <option value="1">In Active</option>
                                                    @endif
                                                    
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary" style="float: right">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.60/inputmask/jquery.inputmask.js"></script>
    <script>
        $(document).ready(function() {
            $('#ststus-orders').DataTable();
        } );

        $("#phone").inputmask({
            mask: '628 9999 9999 99',
            placeholder: ' ',
            showMaskOnHover: false,
            showMaskOnFocus: false,
            onBeforePaste: function (pastedValue, opts) {
            var processedValue = pastedValue;

            //do something with it

            return processedValue;
            }
        });
    </script>
@endsection