@extends('home')
@section('content')
<div class="content-wrapper">
    <section class="content"><br>
        <div class="row">
            <div class="col-md-12">
                <div class="card card-outline">
                    <div class="card-header">
                        <h3 class="card-title">
                            <strong>
                                Penjelasan Tentang Event
                            </strong>
                        </h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-3"></div>
                                <div class="col-md-6">
                                    <div class="embed-responsive embed-responsive-16by9" style="width:auto">
                                        <iframe class="embed-responsive-item" src="{{$detailEvent->url_vidio}}" allowfullscreen></iframe>
                                    </div>
                                </div>
                                <div class="col-md-3"></div>
                            </div><br><br>
                            <div class="row">
                                <h3 align="center"><strong>{{$detailEvent->nama_event}}</strong></h3><br>
                                
                            </div>
                            <h6>
                                {!!$detailEvent->deskripsi!!}
                            </h6>
                        </div>
                        
                    </div>
                    <div class="card-footer">
                        {{-- <a href="http://" class="btn btn-primary btn-block">Daftar Event</a> --}}
                    </div>
                </div>
            </div>
        </div><br>
    </section>
</div>
@endsection
