@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3 class="card-title">
                                        Create Events
                                    </h3>
                                </div>
                                <div class="col-md-2">
                                    <a href="{{ url('admin/events/show') }}" class="btn btn-primary btn-sm" style="float: right">Back</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <form action="{{ url('admin/events/store') }}" method="post"  enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Nama Event</label>
                                                <input type="text" name="nama_event" class="form-control" placeholder="Nama Event ..." value="{{ old('nama_event') }}">
                                                @error('nama_event')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Photo</label>
                                                <input type="file" name="gambar" class="form-control" placeholder="Photo ..." value="{{ old('gambar') }}">
                                                @error('gambar')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Deskripsi Event</label>
                                                <textarea id="summernote" name="deskripsi">{{ old('deskripsi') }}</textarea>
                                                @error('deskripsi')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Publish At</label>
                                                <div class="input-group date" id="reservationdate" data-target-input="nearest">
                                                    <input type="text" name="publish_at" value="{{ old('publish_at') }}" class="form-control datetimepicker-input" data-target="#reservationdate"/>
                                                   
                                                    <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                    </div>
                                                </div>
                                                @error('publish_at')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Harga</label>
                                                    <input type="number" name="harga" value="{{ old('harga') }}" class="form-control"/>
                                                    @error('harga')
                                                        <span style="color:red"><small>{{ $message }}</small></span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card card-outline card-info">
                                    <div class="card-header">
                                        <div class="row">
                                            <div class="col-md-10">
                                                <h3 class="card-title">
                                                    Vidio Penjelasan
                                                </h3>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <!-- text input -->
                                                    <div class="form-group">
                                                        <label>Url Vidio</label>
                                                        <input type="text" name="url_vidio" class="form-control" placeholder="Url Vidio ..." value="{{ old('url_vidio') }}">
                                                        @error('url_vidio')
                                                            <span style="color:red"><small>{{ $message }}</small></span>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <!-- text input -->
                                                    <div class="form-group">
                                                        <label>Deskripsi Vidio</label>
                                                        <textarea id="summernote1" name="deskripsi_vidio">{{ old('deskripsi_vidio') }}</textarea>
                                                        @error('deskripsi_vidio')
                                                            <span style="color:red"><small>{{ $message }}</small></span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary" style="float: right">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#ststus-orders').DataTable();
        } );

        

        $(function () {
            // Summernote
            $('#summernote').summernote()
            $('#summernote1').summernote()

            //Date range picker
            $('#reservationdate').datetimepicker({
                format: 'YYYY-MM-DD'
            });
        })
    </script>
@endsection
