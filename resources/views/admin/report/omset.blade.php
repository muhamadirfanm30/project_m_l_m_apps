@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <i class="fas fa-chart mr-1"></i>
                                Omset
                            </h3>
                            <div class="card-tools">
                                <ul class="nav nav-pills ml-auto">
                                    <li class="nav-item">
                                        <a class="nav-link @if($type=='Weekly') active @endif" href="{{ route('report.omset',['Weekly']) }}">Weekly</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link @if($type=='Monthly') active @endif" href="{{ route('report.omset',['Monthly']) }}">Monthly</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link @if($type=='Yearly') active @endif" href="{{ route('report.omset',['Yearly']) }}">Yearly</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="tab-content p-0">
                                <div class="chart tab-pane active" id="revenue-chart" style="position: relative; height: 300px;">
                                    <canvas id="revenue-chart-canvas" height="300" style="height: 300px;"></canvas>
                                </div>
                                <hr>
                                <table>
                                    <tr>
                                        <td width="70%"><b>This Month</b></td>
                                        <td>{{ 'Rp. '.number_format($listTotal['thisMonth']) }}  ({{ $listTotal['percentage'] }})</td>
                                    </tr>
                                    <tr>
                                        <td width="70%"><b>Previous Month</b></td>
                                        <td>{{ 'Rp. '.number_format($listTotal['prevMonth']) }}</td>
                                    </tr>
                                    <tr>
                                        <td width="70%"><b>This Year</b></td>
                                        <td>{{'Rp. '.number_format($listTotal['thisYear']) }}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>    
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>
        $(function () {
            'use strict'
            var salesChartCanvas = document.getElementById('revenue-chart-canvas').getContext('2d')
            var salesChartData = {
                labels: {!! json_encode($title) !!}, //['January', 'February', 'March', 'April'],
                datasets: [
                    {
                        label: 'Omset',
                        backgroundColor: 'rgba(60,141,188,0.9)',
                        borderColor: 'rgba(60,141,188,0.8)',
                        pointRadius: false,
                        pointColor: '#3b8bba',
                        pointStrokeColor: 'rgba(60,141,188,1)',
                        pointHighlightFill: '#fff',
                        pointHighlightStroke: 'rgba(60,141,188,1)',
                        data: {!! json_encode(array_column($datas, 'total')) !!}
                    },
                ]
            }

            var salesChartOptions = {
                maintainAspectRatio: false,
                responsive: true,
                // legend: {
                //     display: false
                // },
                scales: {
                    xAxes: [{
                        gridLines: {
                        display: false
                        }
                    }],
                    yAxes: [{
                        gridLines: {
                        display: false
                        }
                    }]
                }
            }

            // This will get the first returned node in the jQuery collection.
            // eslint-disable-next-line no-unused-vars
            var salesChart = new Chart(salesChartCanvas, {
                type: 'bar',
                data: salesChartData,
                options: salesChartOptions
            })
        })
    </script>
@endsection