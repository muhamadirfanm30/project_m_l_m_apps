@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('success') }}</strong>
                        </div>
                    @endif
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3 class="card-title">
                                        Show List Users
                                    </h3>
                                </div>
                                <div class="col-md-2">
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="col-md-12">
                                <table id="userManagement" class="display table table-hover table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Nama</th>
                                            <th>Total Omset</th>
                                            <th>Total Produk Terjual</th>
                                            <th>Total Reseller</th>
                                            <th>Tanggal Bergabung</th>
                                            <th style="display: none"></th>
                                            <th width="130px">Action</th>
                                        </tr>
                                        
                                    </thead>
                                    @foreach ($getData as $key => $value)
                                    @php
                                        $omset = 0;
                                        $produkTerjual = 0;
                                        foreach($value->order as $key => $order){
                                            $omset += $order->totalKeseluruhan;
                                            foreach($order->order_detail as $key => $salesProduk){
                                                $produkTerjual += $salesProduk->qty;
                                                
                                            }
                                        }
                                    @endphp
                                        <tr>
                                            <td>{{ $value->first_name }} {{ $value->last_name }}</td>
                                            <td>Rp. {{ number_format($omset) }}</td>
                                            <td>{{ number_format($produkTerjual) }}</td>
                                            <td>{{ number_format($value->my_member) }}</td>
                                            <td>{{ date('d M Y', strtotime($value->created_at)) }}</td>
                                            <td style="display: none"> {{ $value->id }}</td>
                                            <td>
                                                <a href="{{url('https://wa.me/'.$value->phone.'?text=Hallo, perkenalkan Saya Admin Aplikasi .....')}}" target="_blank" class="btn btn-success btn-sm"><i class="fab fa-whatsapp"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                                <a href="{{url('admin/member-performence/download-report')}}" class="btn btn-success my-3" target="_blank">EXPORT EXCEL</a>
                            </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#userManagement').DataTable( {
                processing: true,
                responsive: true,
                aaSorting: [[ 5, "desc" ]],
                // dom: 'Bfrtip',
                // buttons: [
                //     'excel', 'pdf',
                // ]
            } );
        } );
    </script>
@endsection