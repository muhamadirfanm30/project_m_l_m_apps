@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('success') }}</strong>
                        </div>
                    @endif
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3 class="card-title">
                                        Mobile Stokiest List
                                    </h3>
                                </div>
                                <div class="col-md-2">
                                    {{-- <a href="{{ url('admin/mobile-stokiest/create') }}" class="btn btn-primary btn-sm" style="float: right">Tambah Baru</a> --}}
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="col-md-12">
                                <table id="userManagement" class="display table table-hover table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Judul</th>
                                            <th>Url Vidio</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    @foreach ($data as $key => $ms)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            <td>{{ $ms->judul }}</td>
                                            <td>{{ $ms->url_vidio }}</td>
                                            <td>
                                                <form action="{{ url('admin/mobile-stokiest/destroy/'.$ms->id) }}" method="POST" id="deleteData-{{$ms->id}}">
                                                    @csrf
                                                </form>
                                                    <a href="{{ url('admin/mobile-stokiest/update/'.$ms->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                                                    <button type="submit" class="btn btn-danger btn-sm" onclick="deleteData({{ $ms->id }})"><i class="fa fa-trash"></i></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#userManagement').DataTable();
        } );

        function deleteData(id){
            swal({
                title: "Alert!",
                text: "Apakah anda yakin akan menghapus data tersebut?",
                type: 'error',
                showCancelButton: true,
                confirmButtonText: 'Ya, Saya yakin',
                cancelButtonText: 'Tidak',
            },function(result) {
                if(result){
                    document.getElementById('deleteData-'+id).submit();
                }
            });
        }
    </script>
@endsection