@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3 class="card-title">
                                        Mobile Stokiest Update
                                    </h3>
                                </div>
                                <div class="col-md-2">
                                    {{-- <a href="{{ url('admin/mobile-stokiest/show') }}" class="btn btn-primary btn-sm" style="float: right">Back</a> --}}
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <form action="{{ url('admin/mobile-stokiest/edit/'.$ms->id) }}" method="post">
                            @csrf
                            <div class="card-body">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Judul</label>
                                                <input type="text" name="judul" value="{{$ms->judul}}" class="form-control" placeholder="Judul ...">
                                                @error('judul')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Url Vidio</label>
                                                <input type="text" name="url_vidio" value="{{$ms->url_vidio}}" class="form-control" placeholder="Url Vidio ...">
                                                @error('url_vidio')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Deskripsi</label>
                                                <textarea id="summernote" name="deskripsi">{{$ms->deskripsi}}</textarea>
                                                @error('deskripsi')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary" style="float: right">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>
        $(function () {
            // Summernote
            $('#summernote').summernote()
        })
    </script>
@endsection