@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3 class="card-title">
                                        Create Roles
                                    </h3>
                                </div>
                                <div class="col-md-2">
                                    <a href="{{ url('admin/role/show') }}" class="btn btn-primary btn-sm" style="float: right">Back</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <form action="{{ url('admin/role/store') }}" method="post">
                            @csrf
                            <div class="card-body">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Name</label>
                                                <input type="text" name="name" class="form-control" placeholder="Name ...">
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <label>Permission</label>
                                            <br />
                                            <input id="" type="checkbox" class="permission-checkbox-all"> Check All
                                            <hr />
                                            <div class="row">
                                                @foreach ($permission  as $nameGrup => $group)
                                                <div class="col-md-3" style="margin-bottom: 10px;">
                                                    <div class="card">
                                                        <div class="card-body">
                                                            <input id="" type="checkbox" class="permission-checkbox-parent" data-id="{{ $nameGrup }}">
                                                            <label>{{ $no++ }}. {{ str_replace('_', ' ', ucfirst($nameGrup)) }}</label>
                                                            @foreach ($group as $parent)
                                                            <div class="checkbox">
                                                                <input id="checkbox-{{ $parent->id }}" type="checkbox" name="permissions[]" value="{{ $parent->id }}" class="permission-checkbox p-c-{{ $nameGrup }}">
                                                                <label for="checkbox-{{ $parent->id }}">{{ $parent->name }}</label>
                                                            </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary" style="float: right">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#ststus-orders').DataTable();
        } );
    </script>
@endsection