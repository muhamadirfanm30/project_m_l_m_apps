@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3 class="card-title">
                                        Show List
                                    </h3>
                                </div>
                                <div class="col-md-2">
                                    <a href="{{ url('admin/role/create') }}" class="btn btn-primary btn-sm" style="float: right">Create Roles</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="col-md-12">
                                <table id="ststus-orders" class="display table table-hover table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Role Name</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                        @foreach ($roles as $k)
                                            <tr>
                                                <td>{{$no++}}</td>
                                                <td>{{$k->name}}</td>
                                                <td>
                                                    <form action="{{ url('admin/role/destroy/'.$k->id) }}" method="POST">
                                                        @csrf
                                                        <a href="{{ url('admin/role/update/'.$k->id) }}" class="btn btn-success btn-sm"><i class="fa fa-edit"></i></a>
                                                        <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                </table>
                            </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#ststus-orders').DataTable();
        } );
    </script>
@endsection