@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('success') }}</strong>
                        </div>
                    @endif
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3 class="card-title">
                                        FAQ Kategori
                                    </h3>
                                </div>
                                <div class="col-md-2">
                                    <a href="{{ url('admin/faq-kategori/create') }}" class="btn btn-primary btn-sm" style="float: right">Tambah FAQ Kategori</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="col-md-12">
                                <table id="faq-categori" class="display table table-hover table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Kategori FAQ</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    @foreach ($showData as $key => $faq_detail)
                                        <tr>
                                            <td>{{ $no++ }}</td>
                                            <td>{{ $faq_detail->name }}</td>
                                            <td>
                                                <form action="{{ url('admin/faq-kategori/destroy/'.$faq_detail->id) }}" method="POST">
                                                    @csrf
                                                    <a href="{{ url('admin/faq-kategori/update/'.$faq_detail->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                                                    <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#faq-categori').DataTable();
        } );
    </script>
@endsection