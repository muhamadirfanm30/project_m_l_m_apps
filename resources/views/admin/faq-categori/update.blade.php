@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3 class="card-title">
                                        FAQ Kategori 
                                    </h3>
                                </div>
                                <div class="col-md-2">
                                    <a href="{{ url('admin/faq-kategori/show') }}" class="btn btn-primary btn-sm" style="float: right">Back</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <form action="{{ url('admin/faq-kategori/edit/'.$dataEdit->id) }}" method="post">
                            @csrf
                            <div class="card-body">
                                <div class="col-md-12">
                                    <div data-role="dynamic-fields">
                                        <div class="form-inline">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Nama FAQ Kategori</label>
                                                    <input type="text" name="name" class="form-control" id="field-name" value="{{ $dataEdit->name }}" placeholder="FAQ Kategori" style="width:100%" required>
                                                </div>
                                            </div>
                                        </div>  <!-- /div.form-inline -->
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary" style="float: right">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    
@endsection