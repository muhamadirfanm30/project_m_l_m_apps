@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                        
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3 class="card-title">
                                        Tambah Data
                                    </h3>
                                </div>
                                <div class="col-md-2">
                                    <a href="{{ route('konten') }}" class="btn btn-primary btn-sm" style="float: right">Back</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <form action="{{ route('konten-update',['id'=>$id->id]) }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>Type</label>
                                            <select name="type" class="form-control" onchange="changes(this.value)">
                                                <option value="">Pilih Type</option>
                                                <option value="0" {{ $id->type == 0 ? 'selected' : null }}>Video</option>
                                                <option value="1" {{ $id->type == 1 ? 'selected' : null }}>File</option>
                                                <option value="2" {{ $id->type == 2 ? 'selected' : null }}>Accordion</option>
                                            </select>
                                            @error('type')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                    </div>
                                     <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>Judul</label>
                                            <input type="text" name="judul" class="form-control" value="{{ $id->judul }}" placeholder="Judul ...">
                                            @error('judul')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>Free Text</label>
                                            <textarea class="summernote" name="free_text" row="5">{{ $id->free_text }}</textarea>
                                            @error('free_text')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-sm-12" id="content-url-download">
                                        <div class="form-group">
                                            <label>Url / Video Url</label>
                                            <input type="text" name="url_download" class="form-control" value="{{ $id->url_download }}" placeholder="Url Download ...">
                                            @error('url_download')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                    </div>
                                      <div class="col-sm-12" id="video-download">
                                        <div class="form-group">
                                            <label>Video url Download</label>
                                            <input class="form-control" type="text" id="vidlink" name="deskripsi" placeholder="https://" value="{{ $id->deskripsi }}">
                                            @error('deskripsi')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-sm-12" id="content-thumbnail">
                                        <div class="form-group">
                                            <label>Thumbnail</label>
                                            <input type="file" name="thumbnail" class="form-control" value="{{ $id->thumbnail }}" placeholder="Thumbnil ...">
                                                @if (!empty($id->foto))
                                                    <a href="{{ url('storage/konten/'.$id->foto) }}" target="_blank" rel="noopener noreferrer" class="badge badge-primary"><i class="fa fa-image"></i>  {{$id->foto}}</a>    
                                                @else
                                                    <span class="badge badge-warning" style="color: white"><i class="fa fa-image"></i>  Gambar Tidak Ada</span>
                                                @endif
                                            @error('thumbnail')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>Kategori</label>
                                            <select name="kategori_id" class="form-control">
                                                <option value="">Pilih Kategori</option>
                                                @foreach($listKategori as $r)
                                                    <option value="{{ $r->id }}" {{ $id->kategori_id == $r->id ? 'selected' : null }}>{{ $r->judul }}</option>
                                                @endforeach
                                            </select>
                                            @error('kategori_id')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>Sub Kategori</label>
                                            <select name="sub_kategori_id" class="form-control">
                                                <option value="">Pilih Sub Kategori</option>
                                                @foreach($listSubKategori as $r)
                                                    <option value="{{ $r->id }}" {{ $id->sub_kategori_id == $r->id ? 'selected' : null }}>{{ $r->nama }}</option>
                                                @endforeach
                                            </select>
                                            @error('sub_kategori_id')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                    </div>
                                   
                                    <div class="col-sm-12" id="cont-deskripsi">
                                        <div class="form-group">
                                            <label>Deskripsi</label>
                                            <textarea id="des" class="summernote" name="deskripsi" row="5">{{ $id->deskripsi }}</textarea>
                                            @error('deskripsi')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary" style="float: right">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>
        function changes(type){
            if(type == 0){
                $('#des').val('none'); 
                 $('#vidlink').attr('name', 'deskripsi');
                $('#des').attr('name', 'newName');
                document.getElementById("video-download").style.display = 'block';
                document.getElementById("cont-deskripsi").style.display = 'none';
                document.getElementById("content-thumbnail").style.display = 'none';
                document.getElementById("content-url-download").style.display = 'block';
            }else if(type == 1){
                  $('#vidlink').attr('name', 'newName');
                 $('#des').attr('name', 'deskripsi');
                document.getElementById("video-download").style.display = 'none';
                document.getElementById("cont-deskripsi").style.display = 'none';
                document.getElementById("content-thumbnail").style.display = 'block';
                document.getElementById("content-url-download").style.display = 'block';
            }else{
                  $('#vidlink').attr('name', 'newName');
                 $('#des').attr('name', 'deskripsi');
                document.getElementById("video-download").style.display = 'none';
                document.getElementById("cont-deskripsi").style.display = 'block';
                document.getElementById("content-thumbnail").style.display = 'none';
                document.getElementById("content-url-download").style.display = 'none';
            }
        }

        $(function () {
            changes({{ $id->type }});
            $('.summernote').summernote()
        })
    </script>
@endsection