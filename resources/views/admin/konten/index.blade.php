@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('success') }}</strong>
                        </div>
                    @elseif(session()->has('failed'))
                        <div class="alert alert-danger alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('failed') }}</strong>
                        </div>
                    @endif
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3 class="card-title">
                                        Show List Data
                                    </h3>
                                </div>
                                <div class="col-md-2">
                                    <a href="{{ route('konten-create') }}" class="btn btn-primary btn-sm" style="float: right">Tambah Data</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="col-md-12">
                                <table class="display datatable table table-hover table-bordered" id="kategoris" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Kategori</th>
                                            <th>Sub Kategori</th>
                                            <th>Judul</th>
                                            <th style="display: none"></th>
                                            <th width="130px">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $no = 1;
                                        @endphp
                                        @foreach($data as $r)
                                            <tr>
                                                <td>{{ !empty($r->getKategori) ? $r->getKategori->judul : 'Kategori Tidak Ditemukan' }}</td>
                                                <td>{{ !empty($r->getSubKategori) ? $r->getSubKategori->nama  : 'Sub Kategori Tidak Ditemukan'}}</td>
                                                <td>{{ $r->judul }}</td>
                                                {{-- <td>{!! Str::limit($r->deskripsi, $limit = 380, $end = '...') !!}</td> --}}
                                                <!--<td>{!! $r->deskripsi !!}</td>-->
                                                <td style="display: none">{{ $r->id }}</td>
                                                <td>
                                                    <form action="{{ route('konten-destroy',['id'=>$r->id]) }}" method="POST"  id="deleteData-{{$r->id}}">
                                                        @csrf
                                                    </form>
                                                        <a href="{{ route('konten-update',['id'=>$r->id]) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                                                        <button type="submit" class="btn btn-danger btn-sm" onclick="deleteData({{ $r->id }})"><i class="fa fa-trash"></i></button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#kategoris').DataTable({
                processing: true,
                responsive: true,
                aaSorting: [[ 3, "desc" ]]
            }  );
        } );

        function deleteData(id){
            swal({
                title: "Alert!",
                text: "Apakah anda yakin akan menghapus data tersebut?",
                type: 'error',
                showCancelButton: true,
                confirmButtonText: 'Ya, Saya yakin',
                cancelButtonText: 'Tidak',
            },function(result) {
                if(result){
                    document.getElementById('deleteData-'+id).submit();
                }
            });
        }
    </script>
@endsection