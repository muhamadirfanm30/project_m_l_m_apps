@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                        
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3 class="card-title">
                                        Tambah Paket
                                    </h3>
                                </div>
                                <div class="col-md-2">
                                    <a href="{{ url('admin/paket-upgrade-ms/show') }}" class="btn btn-primary btn-sm" style="float: right">Back</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <form action="{{ url('admin/paket-upgrade-ms/update/'.$detailPaketReguler->id) }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Nama Paket</label>
                                                <input type="text" name="nama_paket" class="form-control" value="{{$detailPaketReguler->nama_paket}}" placeholder="Nama Paket">
                                                @error('nama_paket')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Stok</label>
                                                <input type="number" name="stok" class="form-control" value="{{$detailPaketReguler->stok}}" placeholder="Stok Paket">
                                                @error('stok')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Photo</label>
                                                <input type="file" name="image_produk" class="form-control" placeholder="Photo">
                                                @if (!empty($detailPaketReguler->image_produk))
                                                    <a href="{{ url('storage/package-upgrade-ms/'.$detailPaketReguler->image_produk) }}" target="_blank" rel="noopener noreferrer" class="badge badge-primary"><i class="fa fa-image"></i>  {{$detailPaketReguler->image_produk}}</a>    
                                                @else
                                                    <span class="badge badge-warning" style="color: white"><i class="fa fa-image"></i>  Gambar Tidak Ada</span>
                                                @endif
                                                @error('image_produk')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Harga</label>
                                                <input type="number" name="harga" class="form-control" value="{{$detailPaketReguler->harga}}" placeholder="Harga Paket">
                                                @error('harga')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                        
                                        <div class="col-sm-3">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Panjang (CM)</label>
                                                <input type="number" name="panjang" value="{{$detailPaketReguler->panjang}}" class="form-control" placeholder="Panjang">
                                                @error('panjang')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Berat (Gram)</label>
                                                <input type="text" name="berat" value="{{$detailPaketReguler->berat}}" class="form-control" placeholder="Berat">
                                                @error('berat')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Lebar (CM)</label>
                                                <input type="number" name="lebar" value="{{$detailPaketReguler->lebar}}"  class="form-control" placeholder="Lebar">
                                                @error('lebar')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Tinggi (CM)</label>
                                                <input type="text" name="tinggi" value="{{$detailPaketReguler->tinggi}}" class="form-control" placeholder="Tinggi">
                                                @error('tinggi')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Deskripsi</label>
                                                <textarea id="summernote" name="deskripsi_produk">{{$detailPaketReguler->deskripsi_produk}}</textarea>
                                                @error('deskripsi_produk')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary" style="float: right">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    
    <script>
        $(document).ready(function() {
            $('#ststus-orders').DataTable();
        } );

        $(function () {
            // Summernote
            $('#summernote').summernote()
        })
    </script>
@endsection