@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('success') }}</strong>
                        </div>
                    @endif
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3 class="card-title">
                                        Starterkit Download List
                                    </h3>
                                </div>
                                <div class="col-md-2">
                                    <a href="{{ url('admin/starterkit-downloads/create') }}" class="btn btn-primary btn-sm" style="float: right">Tambah Starterkit</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="col-md-12">
                                <table id="userManagement" class="display table table-hover table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Link Download</th>
                                            <th>Url Download</th>
                                            <th width="130px">Action</th>
                                        </tr>
                                        
                                    </thead>
                                    @foreach ($showdata as $key => $v)
                                        <tr>
                                            <td>{{ $no++ }}</td>
                                            <td>{{ $v->link_name }}</td>
                                            @if (strlen(strip_tags($v->url_download)) > 50)
                                                <td>{{ substr($v->url_download,0, 50) }} ....</td>
                                            @else
                                                <td>{{ $v->url_download }}</td>
                                            @endif
                                            <td>
                                                <form action="{{ url('admin/starterkit-downloads/destroy/'.$v->id) }}" method="POST" id="deleteData-{{$v->id}}">
                                                    @csrf
                                                </form>
                                                <a href="{{ url('admin/starterkit-downloads/show/'.$v->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                                                <button type="submit" class="btn btn-danger btn-sm" onclick="deleteData({{ $v->id }})"><i class="fa fa-trash"></i></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#userManagement').DataTable();
        } );

        function deleteData(id){
            swal({
                title: "Alert!",
                text: "Apakah anda yakin akan menghapus data tersebut?",
                type: 'error',
                showCancelButton: true,
                confirmButtonText: 'Ya, Saya yakin',
                cancelButtonText: 'Tidak',
            },function(result) {
                if(result){
                    document.getElementById('deleteData-'+id).submit();
                }
            });
        }
    </script>
@endsection