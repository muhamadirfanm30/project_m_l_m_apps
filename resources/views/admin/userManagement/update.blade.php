@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                            </ul>
                        </div>
                    @endif
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('success') }}</strong>
                        </div>
                    @endif
                    @if(session()->has('error'))
                        <div class="alert alert-danger alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('error') }}</strong>
                        </div>
                    @endif
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3 class="card-title">
                                        Update Users
                                    </h3>
                                </div>
                                <div class="col-md-2">
                                    <a href="{{ url('admin/user/show') }}" class="btn btn-primary btn-sm" style="float: right">Back</a>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" id="getProv" value="{{ $user->province_id }}">
                        <!-- /.card-header -->
                        <form action="{{ url('admin/user/edit/'.$user->id) }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" value="R-{{$randomString}}" name="member_id">
                            <input type="hidden" value="Reguler" name="membersip_status">
                            <input type="hidden" id="txtProvinsi" name="txtProvinsi">
                            <input type="hidden" id="txtKota" name="txtKota">
                            <div class="card-body">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input type="text" name="email" value="{{!empty($user->email) ? $user->email : ''}}" class="form-control" placeholder="Email ...">
                                            </div>
                                        </div>

                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label>Member ID</label>
                                                <input type="text" name="membership_id" value="{{!empty($user->membership_id) ? $user->membership_id : ''}}" class="form-control" placeholder="Email ..." readonly>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>first name</label>
                                                <input type="text" name="first_name" value="{{!empty($user->first_name) ? $user->first_name : ''}}" class="form-control" placeholder="First Name ...">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Last name</label>
                                                <input type="text" name="last_name" value="{{!empty($user->last_name) ? $user->last_name : ''}}" class="form-control" placeholder="Last name ...">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Status MemberShip</label>
                                                <input type="text" name="membersip_status" value="{{!empty($user->membersip_status) ? $user->membersip_status : ''}}" class="form-control" placeholder="membersip status ..." disabled>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="exampleFormControlSelect1">Jenis Kelamin</label>
                                                <select class="form-control" name="gender" id="exampleFormControlSelect1">
                                                    <option value="Laki Laki" {{ $user->gender == "Laki Laki" ? "selected" : '' }}>Laki Laki</option>
                                                    <option value="Perempuan" {{ $user->gender == "Perempuan" ? "selected" : '' }}>Perempuan</option>
                                                    <option value="Lain Nya" {{ $user->gender == "Lain Nya" ? "selected" : '' }}>Lain nya</option>
                                                </select>
                                                {{-- <label>Gender</label>
                                                <input type="text"  value="{{!empty($user->gender) ? $user->gender : ''}}" class="form-control" placeholder="Gender ..."> --}}
                                            </div>
                                        </div>
                                        {{-- <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Nama Bank</label>
                                                <input type="text" name="rekening_bank" value="{{!empty($user->rekening_bank) ? $user->rekening_bank : ''}}" class="form-control" placeholder="Nama Bank ...">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Nama Rekening Bank</label>
                                                <input type="text" name="nama_pemilik_rekening" value="{{!empty($user->nama_pemilik_rekening) ? $user->nama_pemilik_rekening : ''}}" class="form-control" placeholder="Nama Rekening Bank ...">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Nomor Rekening Bank</label>
                                                <input type="text" name="nomor_rekening" value="{{!empty($user->nomor_rekening) ? $user->nomor_rekening : ''}}" class="form-control" placeholder="Nomor Rekening Bank ...">
                                            </div>
                                        </div> --}}
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Kode Pos</label>
                                                <input type="text" name="kode_pos" value="{{!empty($user->kode_pos) ? $user->kode_pos : ''}}" class="form-control" placeholder="Kode Pos ...">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Nomor Ponsel</label>
                                                <input type="text" name="phone" id="nomor_ponsel" value="{{!empty($user->phone) ? $user->phone : ''}}" class="form-control" placeholder="Nomor Ponsel ...">
                                            </div>
                                        </div>
                                        
                                        <div class="col-sm-6" id="provHide">
                                            <div class="form-group">
                                                <label>Provinsi</label>
                                                <select class="form-control" name="province_id" id="province_id" style="width:100%">
                                                    <option>Pilih Provinsi</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6" id="cityHide">
                                            <div class="form-group">
                                                <label>Kota / Kabupaten</label>
                                                <select class="form-control" name="kota_id" id="kota_id" style="width:100%">
                                                    <option>Pilih Provinsi Terlebih dahulu</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label>Alamat</label>
                                                <textarea id="summernote" name="address">{!! !empty($user->address) ? $user->address : '' !!}</textarea>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label>No KTP</label>
                                                <input type="text" name="no_ktp" value="{{!empty($user->no_ktp) ? $user->no_ktp : ''}}" class="form-control" placeholder="Masukan Nomor KTP...">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <p><strong>Foto KTP (Kartu Tanda Penduduk)</strong></p>
                                                <p>* Pastikan Foto KTP Anda Terlihat Jelas, Bisa Dibaca dan Tidak Terpotong. Format JPEG, PNG, JPG, Maximal Size 5 MB</p>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <input type='file' name="foto_ktp" id="imgInp" /><br><br>
                                                        @if (!empty($user->foto_ktp))
                                                            <img id="blah" src="{{ url('storage/foto-ktp/'.$user->foto_ktp) }}" alt="your image" width="90%"/><br>
                                                            <small>Foto KTP User</small>
                                                        @else
                                                            <img id="blah" src="" width="90%"/>
                                                        @endif
                                                        
                                                    </div>
                                                    <div class="col-md-8">
                                                        <center><img src="{{ asset('ktp.png') }}" alt="" width="90%"></center>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                                            <hr>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>No Id Mobile Stokiest (MS) Sponsor Langsung <span style="color: red">*</span></label>
                                                <input type="text" id="no_id_sponsor" name="no_id_sponsor" class="form-control" value="{{!empty($user->get_posisi) ? $user->get_posisi->user_sponsor_name : ''}}" placeholder="No Id Mobile Stokiest (MS) Sponsor Langsung ...">
                                                @error('no_id_sponsor')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Nama Id Mobile Stokiest (MS) Sponsor Langsung <span style="color: red">*</span></label>
                                                <input type="text" id="nama_id_sponsor" name="nama_id_sponsor" class="form-control" value="{{!empty($user->get_posisi) ? $user->get_posisi->user_sponsor_status : ''}}" placeholder="Nama Id Mobile Stokiest (MS) Sponsor Langsung ...">
                                                @error('nama_id_sponsor')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>

                                        {{-- <div class="row"> --}}
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label>No MS Id Upline <span style="color: red">*</span></label>
                                                    <input type="text" id="no_id_sponsor_upline" name="no_id_sponsor_upline" value="{{!empty($user->sponsor_upline_id) ? $user->sponsor_upline_id : ''}}" class="form-control" placeholder="No MS Id Upline ...">
                                                    @error('no_id_sponsor_upline')
                                                        <span style="color:red"><small>{{ $message }}</small></span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label>Nama Upline <span style="color: red">*</span></label>
                                                    <input type="text" id="nama_upline" name="sponsor_upline_name" value="{{!empty($user->sponsor_upline_name) ? $user->sponsor_upline_name : ''}}" class="form-control" placeholder="Nama Upline ...">
                                                    @error('nama_upline')
                                                        <span style="color:red"><small>{{ $message }}</small></span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label>Kaki <span style="color: red">*</span></label>
                                                    <select name="posisi" id="posisi" class="form-control">
                                                        @if (!empty($user->get_posisi))
                                                            @if ($user->get_posisi->posisi == 'Kiri')
                                                                <option value="Kiri">Kiri</option>
                                                                <option value="Kanan">Kanan</option>
                                                            @elseif($user->get_posisi->posisi == 'Kanan')
                                                                <option value="Kanan">Kanan</option>
                                                                <option value="Kiri">Kiri</option>
                                                            @else
                                                                <option value="">Pilih Posisi</option>
                                                                <option value="Kiri">Kiri</option>
                                                                <option value="Kanan">Kanan</option>
                                                            @endif
                                                            {{-- <option value="">Pilih Posisi</option>
                                                            <option value="Kiri" {{ $user->get_posisi->posisi == 'Kiri' ? 'checked' : '' }}>Kiri</option>
                                                            <option value="Kanan" {{ $user->get_posisi->posisi == 'Kanan' ? 'checked' : '' }}>Kanan</option> --}}
                                                        @else
                                                            <option value="">Pilih Posisi</option>
                                                            <option value="Kiri">Kiri</option>
                                                            <option value="Kanan">Kanan</option>
                                                        @endif
                                                        
                                                    </select>
                                                    @error('posisi')
                                                        <span style="color:red"><small>{{ $message }}</small></span>
                                                    @enderror
                                                </div>
                                            </div>
                                        {{-- </div> --}}
                                        <div class="col-sm-12">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Status</label>
                                                <select class="form-control" name="status">
                                                    @if ($user->status == 0)
                                                        <option value="0">Active</option>
                                                        <option value="1">In Active</option>
                                                    @elseif($user->status == 1)
                                                        <option value="1">In Active</option>
                                                        <option value="0">Active</option>
                                                    @else
                                                        <option value="0">Active</option>
                                                        <option value="1">In Active</option>
                                                    @endif
                                                    
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary" style="float: right">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function(e) {
                $('#blah').attr('src', e.target.result);
                }
                
                reader.readAsDataURL(input.files[0]); // convert to base64 string
            }
        }

        $("#imgInp").change(function() {
            readURL(this);
        });
    </script>
    <script>
        $(document).ready(function() {
            $('#summernote').summernote();
            onlyNumber('#nomor_ponsel');
            var provinsiUser = '{{$user->province_id}}'
            ajaxRequest('GET', '/api/rajaongkir/provinsi', function(response) {
                var select = '<option value="">Pilih Provinsi</option>';
                $.each(response.data.rajaongkir.results, function(item,item2) {
                    // console.log(item2)
                    select += `<option value="${item2.province_id}" ${item2.province == provinsiUser?"selected":""}>${item2.province}</option>` ;
                    if(item2.province == provinsiUser){
                        getKota(item2.province_id)
                    }
                });
                $('#province_id').html(select)
            });
            
            // on chnege provinsi
            $('#province_id').change(function() {
                province_id = $(this).val();
                var a = $('#province_id option:selected').text();
                $('#txtProvinsi').val(a);
                getKota(province_id);
            })

            $('#kota_id').change(function() {
                var a = $('#kota_id option:selected').text();
                $('#txtKota').val(a);
            })
        });

        function getKota(province_id){
            var kotaUser = '{{ $user->kota_id }}'
            Helper.loadingStart()
            ajaxRequest('GET', '/api/rajaongkir/city?province=' + province_id, function(response) {
                console.log(response)
                var city = '<option value="">Pilih Kota / Kabupaten</option>';
                $.each(response.data.rajaongkir.results, function(item,item2) {
                    city += `<option value="${item2.city_id}" ${item2.type + ' ' + item2.city_name == kotaUser?"selected":""} >${item2.type + ' ' + item2.city_name}</option>`;
                });
                Helper.loadingStop()
                $('#kota_id').html(city)
            })
            // empty select
            $('#kota_id').html('').append('<option value="">Pilih Kota / Kabupaten</option>');
            $('#distrik_id').html('').append('<option value="">Pilih Kota / Kabupaten Terlebih Dahulu</option>');
            $('#kurir').html('').append('<option value="">Pilih Kecamatan Terlebih Dahulu</option>');
            $('#layanan').html('').append('<option value="">Pilih Kurir Terlebih Dahulu</option>');
        }
    </script>
@endsection