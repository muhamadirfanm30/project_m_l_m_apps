@extends('home')
@section('content')
<script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('success') }}</strong>
                        </div>
                    @endif
                    @if(session()->has('error'))
                        <div class="alert alert-danger alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('error') }}</strong>
                        </div>
                    @endif
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3 class="card-title">
                                        Show List Users
                                    </h3>
                                </div>
                                <div class="col-md-2">
                                    <a href="{{ url('admin/user/create') }}" class="btn btn-primary btn-sm" style="float: right">Create User</a>
                                </div>
                            </div>
                        </div>
                        
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="col-md-12">
                                <table id="userManagement" class="display table table-hover table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Memberships Status</th>
                                            <th>Member ID</th>
                                            <th>Status</th>
                                            <th>Dibuat Pada</th>
                                            <th style="display: none"></th>
                                            <th width="130px">Action</th>
                                        </tr>
                                    </thead>
                                    
                                    @foreach ($data as $key => $user)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            <td>{{ $user->first_name.' '.$user->last_name }}</td>
                                            <td>{{ $user->email }}</td>
                                            <td>
                                                <span>
                                                    <strong>{{!empty($user->membersip_status) ? $user->membersip_status : '-'  }}</strong>
                                                </span>
                                            </td>
                                            <td>
                                                <span>
                                                    <strong>{{!empty($user->membership_id) ? $user->membership_id : '-'  }}</strong>
                                                </span>
                                            </td>
                                            @if ($user->status == 0)
                                                <td> <label class="badge badge-success">Active</label></td>
                                            @else
                                                <td> <label class="badge badge-danger">In Active</label></td>
                                            @endif
                                            <td>{{ date('d M Y', strtotime($user->created_at)) }}</td>
                                            <td style="display: none">{{ $user->id }}</td>
                                            <td>
                                                {{-- <form action="{{ url('admin/user/destroy/'.$user->id) }}" method="POST">
                                                    @csrf --}}
                                                    <form action="{{ route('users.destroy',['id'=>$user->id]) }}" method="POST" id="deleteData-{{$user->id}}">
                                                        @csrf
                                                    </form>
                                                    <input type="hidden" name="get_id" id="get_id" value="{{ $user->id }}">
                                                    <a href="{{ url('admin/user/update/'.$user->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                                                    <a href="https://api.whatsapp.com/send/?phone={{$user->phone}}" class="btn btn-success btn-sm"><i class="fab fa-whatsapp"></i></a>
                                                    <span class="btn-delete_{{ $user->id }} btn btn-danger btn-sm" id="{{ $user->id }}"><i class="fa fa-trash"></i></span>
                                                {{-- </form> --}}
                                            </td>
                                        </tr>
                                        <script>
                                            $(document).on('click', '.btn-delete_{{ $user->id }}', function(){
           
                                                    swal({
                                                        title: "Apa anda yakin?",
                                                        text: "Akan Menghapus Data User ini ?",
                                                        type: "warning",
                                                        showCancelButton: true,
                                                        confirmButtonColor: "#DD6B55",
                                                        confirmButtonText: "Ya, Saya Yakin",
                                                        cancelButtonText: 'Tidak',
                                                        closeOnConfirm: false,
                                                        showCloseButton: true
                                                    }, function (isConfirm) {
                                                        if (!isConfirm) return;
                                                        //id = $(this).attr('data');
                                                        
                                                        Helper.loadingStart()
                                                        $.ajax({
                                                            url: '/admin/user/destroy/{{ $user->id }}',
                                                            type: 'post',
                                                            success: function(resp) {
                                                                Helper.loadingStop()
                                                                console.log(resp)
                                                                swal({
                                                                title: "Sukses!",
                                                                    text: "Data User Berhasil di hapus",
                                                                    type: 'success'
                                                                }, function() {
                                                                    window.location.href='/admin/user/show';
                                                                });
                                                            },
                                                            error:function(a,v,c){
                                                                Helper.loadingStop()
                                                                var msg = JSON.parse(a.responseText);
                                                                swal({
                                                                title: "Ups!",
                                                                    text: 'Terjadi kesalahan saat menghapus Data User',
                                                                    type: 'error'
                                                                });
                                                            }
                                                        })
                                                    });
                                                    
                                                });
                                            
                                            
                                        </script>
                                    @endforeach
                                </table>
                                <a href="{{url('admin/user/export-excel')}}" class="btn btn-success my-3" target="_blank">EXPORT EXCEL</a>
                            </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    
    <script>
        $(document).ready(function() {
            $('#userManagement').DataTable({
                processing: true,
                responsive: true,
                aaSorting: [[ 7, "desc" ]],
                // dom: 'Bfrtip',
                // buttons: [
                //     'excel', 'pdf',
                // ]
            }  );
        } );

        
    </script>
@endsection