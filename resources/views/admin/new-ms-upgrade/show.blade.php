@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('success') }}</strong>
                        </div>
                    @endif
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3 class="card-title">
                                        New MS Upgrade
                                    </h3>
                                </div>
                                <div class="col-md-2">
                                    {{-- <a href="{{ url('admin/user/create') }}" class="btn btn-primary btn-sm" style="float: right">Create User</a> --}}
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="col-md-12">
                                <table id="userManagement" class="display table table-hover table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Memberships Status</th>
                                            <th>Member ID</th>
                                            <th>WhatsApp No</th>
                                            <th>Status</th>
                                            <th width="130px">Action</th>
                                            <td style="display: none"></td>
                                        </tr>
                                    </thead>
                                   @foreach ($dataMS as $k => $v)
                                       <tr>
                                           <td>{{!empty($v->first_name) ? $v->first_name : '-'}} {{!empty($v->last_name) ? $v->last_name : '-'}} <span class="badge badge-warning" style="color: white">{{ $v->notif_no_read_leader_upgrade_ms != null ? 'new' : '' }}</span></td>
                                           <td>{{!empty($v->email) ? $v->email : '-'}}</td>
                                           <td>{{!empty($v->membersip_status) ? $v->membersip_status : '-'}}</td>
                                           <td>{{!empty($v->membership_id) ? $v->membership_id : '-'}}</td>
                                           <td>{{!empty($v->whatsapp_no) ? $v->whatsapp_no : '-'}}</td>
                                           <td><span class="badge badge-warning" style="color: white">{{$v->is_upgrade_ms == 3 ? 'Menunggu Persetujuan Admin' : ''}}</span></td>
                                           <td>
                                                <a href="{{url('/admin/new-ms-upgrade-request/detail/'.$v->id)}}" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                                           </td>
                                           <td style="display: none">{{ $v->id }}</td>
                                       </tr>
                                   @endforeach
                                </table>
                            </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#userManagement').DataTable({
                processing: true,
                responsive: true,
                aaSorting: [[ 7, "desc" ]]
            } );
        });
    </script>
@endsection