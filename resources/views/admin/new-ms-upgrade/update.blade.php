@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-9">
                                    <h3 class="card-title">
                                        Detail User Upgrade MS
                                    </h3>
                                </div>
                                <div class="col-md-3">
                                    {{-- <a href="{{ url('admin/user/create') }}" class="btn btn-primary btn-sm" style="float: right">Approve</a> --}}
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <form action="{{url('admin/new-ms-upgrade-request/update/'.$dataEdit->id)}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input type="text" name="email" value="{{!empty($dataEdit->email) ? $dataEdit->email : ''}}" class="form-control" placeholder="Email ...">
                                            </div>
                                        </div>

                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label>Member ID</label>
                                                <input type="text" name="membership_id" value="{{!empty($dataEdit->membership_id) ? $dataEdit->membership_id : ''}}" class="form-control" placeholder="Email ..." readonly>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>first name</label>
                                                <input type="text" name="first_name" value="{{!empty($dataEdit->first_name) ? $dataEdit->first_name : ''}}" class="form-control" placeholder="First Name ...">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Last name</label>
                                                <input type="text" name="last_name" value="{{!empty($dataEdit->last_name) ? $dataEdit->last_name : ''}}" class="form-control" placeholder="Last name ...">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Status MemberShip</label>
                                                <input type="text" name="membersip_status" value="{{!empty($dataEdit->membersip_status) ? $dataEdit->membersip_status : ''}}" class="form-control" placeholder="membersip status ..." disabled>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="exampleFormControlSelect1">Jenis Kelamin</label>
                                                <select class="form-control" name="gender" id="exampleFormControlSelect1">
                                                    <option value="Laki Laki" {{ $dataEdit->gender == "Laki Laki" ? "selected" : '' }}>Laki Laki</option>
                                                    <option value="Perempuan" {{ $dataEdit->gender == "Perempuan" ? "selected" : '' }}>Perempuan</option>
                                                    <option value="Lain Nya" {{ $dataEdit->gender == "Lain Nya" ? "selected" : '' }}>Lain nya</option>
                                                </select>
                                                {{-- <label>Gender</label>
                                                <input type="text"  value="{{!empty($dataEdit->gender) ? $dataEdit->gender : ''}}" class="form-control" placeholder="Gender ..."> --}}
                                            </div>
                                        </div>
                                        {{-- <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Nama Bank</label>
                                                <input type="text" name="rekening_bank" value="{{!empty($dataEdit->rekening_bank) ? $dataEdit->rekening_bank : ''}}" class="form-control" placeholder="Nama Bank ...">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Nama Rekening Bank</label>
                                                <input type="text" name="nama_pemilik_rekening" value="{{!empty($dataEdit->nama_pemilik_rekening) ? $dataEdit->nama_pemilik_rekening : ''}}" class="form-control" placeholder="Nama Rekening Bank ...">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Nomor Rekening Bank</label>
                                                <input type="text" name="nomor_rekening" value="{{!empty($dataEdit->nomor_rekening) ? $dataEdit->nomor_rekening : ''}}" class="form-control" placeholder="Nomor Rekening Bank ...">
                                            </div>
                                        </div> --}}
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Kode Pos</label>
                                                <input type="text" name="kode_pos" value="{{!empty($dataEdit->kode_pos) ? $dataEdit->kode_pos : ''}}" class="form-control" placeholder="Kode Pos ...">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Nomor Ponsel</label>
                                                <input type="text" name="phone" id="nomor_ponsel" value="{{!empty($dataEdit->phone) ? $dataEdit->phone : ''}}" class="form-control" placeholder="Nomor Ponsel ...">
                                            </div>
                                        </div>
                                        
                                        <div class="col-sm-6" id="provHide">
                                            <div class="form-group">
                                                <label>Provinsi</label>
                                                <select class="form-control" name="province_id" id="province_id" style="width:100%">
                                                    <option>Pilih Provinsi</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6" id="cityHide">
                                            <div class="form-group">
                                                <label>Kota / Kabupaten</label>
                                                <select class="form-control" name="kota_id" id="kota_id" style="width:100%">
                                                    <option>Pilih Provinsi Terlebih dahulu</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label>Alamat</label>
                                                <textarea id="summernote" name="address">{!! !empty($dataEdit->address) ? $dataEdit->address : '' !!}</textarea>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label>No KTP</label>
                                                <input type="text" name="no_ktp" value="{{!empty($dataEdit->no_ktp) ? $dataEdit->no_ktp : ''}}" class="form-control" placeholder="Masukan Nomor KTP...">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <p><strong>Foto KTP (Kartu Tanda Penduduk)</strong></p>
                                                <p>* Pastikan Foto KTP Anda Terlihat Jelas, Bisa Dibaca dan Tidak Terpotong. Format JPEG, PNG, JPG, Maximal Size 5 MB</p>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <input type='file' name="foto_ktp" id="imgInp" /><br><br>
                                                        @if (!empty($dataEdit->foto_ktp))
                                                            <img id="blah" src="{{ url('storage/foto-ktp/'.$dataEdit->foto_ktp) }}" alt="your image" width="90%"/><br>
                                                            <small>Foto KTP User</small>
                                                        @else
                                                            <img id="blah" src="" width="90%"/>
                                                        @endif
                                                        
                                                    </div>
                                                    <div class="col-md-8">
                                                        <center><img src="{{ asset('ktp.png') }}" alt="" width="90%"></center>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                                            <hr>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>No Id Mobile Stokiest (MS) Sponsor Langsung <span style="color: red">*</span></label>
                                                <input type="text" id="no_id_sponsor" name="no_id_sponsor" class="form-control" value="{{!empty($dataEdit->get_posisi) ? $dataEdit->get_posisi->user_sponsor_name : ''}}" placeholder="No Id Mobile Stokiest (MS) Sponsor Langsung ...">
                                                @error('no_id_sponsor')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Nama Id Mobile Stokiest (MS) Sponsor Langsung <span style="color: red">*</span></label>
                                                <input type="text" id="nama_id_sponsor" name="nama_id_sponsor" class="form-control" value="{{!empty($dataEdit->get_posisi) ? $dataEdit->get_posisi->user_sponsor_status : ''}}" placeholder="Nama Id Mobile Stokiest (MS) Sponsor Langsung ...">
                                                @error('nama_id_sponsor')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>

                                        {{-- <div class="row"> --}}
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label>No MS Id Upline <span style="color: red">*</span></label>
                                                    <input type="text" id="no_id_sponsor_upline" name="no_id_sponsor_upline" value="{{!empty($dataEdit->sponsor_upline_name) ? $dataEdit->sponsor_upline_name : ''}}" class="form-control" placeholder="No MS Id Upline ...">
                                                    @error('no_id_sponsor_upline')
                                                        <span style="color:red"><small>{{ $message }}</small></span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label>Nama Upline <span style="color: red">*</span></label>
                                                    <input type="text" id="nama_upline" name="sponsor_upline_name" value="{{!empty($dataEdit->sponsor_upline_name) ? $dataEdit->sponsor_upline_name : ''}}" class="form-control" placeholder="Nama Upline ...">
                                                    @error('nama_upline')
                                                        <span style="color:red"><small>{{ $message }}</small></span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label>Kaki <span style="color: red">*</span></label>
                                                    <select name="posisi" id="posisi" class="form-control">
                                                        @if (!empty($dataEdit->get_posisi))
                                                            @if ($dataEdit->get_posisi->posisi == 'Kiri')
                                                                <option value="Kiri">Kiri</option>
                                                                <option value="Kanan">Kanan</option>
                                                            @elseif($dataEdit->get_posisi->posisi == 'Kanan')
                                                                <option value="Kanan">Kanan</option>
                                                                <option value="Kiri">Kiri</option>
                                                            @else
                                                                <option value="">Pilih Posisi</option>
                                                                <option value="Kiri">Kiri</option>
                                                                <option value="Kanan">Kanan</option>
                                                            @endif
                                                            {{-- <option value="">Pilih Posisi</option>
                                                            <option value="Kiri" {{ $dataEdit->get_posisi->posisi == 'Kiri' ? 'checked' : '' }}>Kiri</option>
                                                            <option value="Kanan" {{ $dataEdit->get_posisi->posisi == 'Kanan' ? 'checked' : '' }}>Kanan</option> --}}
                                                        @else
                                                            <option value="">Pilih Posisi</option>
                                                            <option value="Kiri">Kiri</option>
                                                            <option value="Kanan">Kanan</option>
                                                        @endif
                                                        
                                                    </select>
                                                    @error('posisi')
                                                        <span style="color:red"><small>{{ $message }}</small></span>
                                                    @enderror
                                                </div>
                                            </div>
                                        {{-- </div> --}}
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-block btn-sm">Simpan Perubahan</button>
                                </div>
                            </form>
                        </div>
                        <div class="modal fade" id="modal-default">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Approve Mobile Stokiest</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label>Memberships Status</label>
                                                <input type="text" name="membersip_status"  class="form-control" placeholder="Memberships Status">
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label>Member ID</label>
                                                <input type="text" name="membership_id"  class="form-control" placeholder="Memberships ID">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer justify-content-between">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary">Save changes</button>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <div class="card-footer">
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function(e) {
                $('#blah').attr('src', e.target.result);
                }
                
                reader.readAsDataURL(input.files[0]); // convert to base64 string
            }
        }

        $("#imgInp").change(function() {
            readURL(this);
        });
    </script>
    <script>
        $(document).ready(function() {
            $('#summernote').summernote();
            onlyNumber('#nomor_ponsel');
            var provinsiUser = '{{$dataEdit->province_id}}'
            

            ajaxRequest('GET', '/api/rajaongkir/provinsi', function(response) {
                var select = '<option value="">Pilih Provinsi</option>';
                $.each(response.data.rajaongkir.results, function(item,item2) {
                    // console.log(item2)
                    select += `<option value="${item2.province_id}" ${item2.province == provinsiUser?"selected":""}>${item2.province}</option>` ;
                    if(item2.province == provinsiUser){
                        getKota(item2.province_id)
                    }
                });
                $('#province_id').html(select)
            });
            
            // on chnege provinsi
            $('#province_id').change(function() {
                province_id = $(this).val();
                var a = $('#province_id option:selected').text();
                $('#txtProvinsi').val(a);
                getKota(province_id);
            })

            $('#kota_id').change(function() {
                var a = $('#kota_id option:selected').text();
                $('#txtKota').val(a);
            })
        });

        function getKota(province_id){
            var kotaUser = '{{ $dataEdit->kota_id }}'
            Helper.loadingStart()
            ajaxRequest('GET', '/api/rajaongkir/city?province=' + province_id, function(response) {
                console.log(response)
                var city = '<option value="">Pilih Kota / Kabupaten</option>';
                $.each(response.data.rajaongkir.results, function(item,item2) {
                    city += `<option value="${item2.city_id}" ${item2.type + ' ' + item2.city_name == kotaUser?"selected":""} >${item2.type + ' ' + item2.city_name}</option>`;
                });
                Helper.loadingStop()
                $('#kota_id').html(city)
            })
            // empty select
            $('#kota_id').html('').append('<option value="">Pilih Kota / Kabupaten</option>');
            $('#distrik_id').html('').append('<option value="">Pilih Kota / Kabupaten Terlebih Dahulu</option>');
            $('#kurir').html('').append('<option value="">Pilih Kecamatan Terlebih Dahulu</option>');
            $('#layanan').html('').append('<option value="">Pilih Kurir Terlebih Dahulu</option>');
        }
    </script>
    <script>
        $(document).ready(function() {
            $('#userManagement').DataTable();
            $('#summernote').summernote();
        } );
    </script>
@endsection