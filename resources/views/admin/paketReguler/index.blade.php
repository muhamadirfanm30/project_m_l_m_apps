@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('success') }}</strong>
                        </div>
                    @endif
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3 class="card-title">
                                        Paket Reguler List
                                    </h3>
                                </div>
                                <div class="col-md-2">
                                    <a href="{{ url('admin/daftar-paket-reguler/create') }}" class="btn btn-primary btn-sm" style="float: right">Tambah Paket</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="col-md-12">
                                <table id="list-daftar-paket-reguler" class="display table table-hover table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Nama paket</th>
                                            <th>Harga</th>
                                            <th>Stok</th>
                                            <th style="display: none"></th>
                                            <th>Dibuat Tanggal</th>
                                            <th>Action</th>
                                        </tr>
                                        
                                    </thead>
                                    @foreach ($paketRegulerList as $key => $pr)
                                        <tr>
                                            <td>{{ $pr->nama_paket }}</td>
                                            <td>{{ $pr->harga }}</td>
                                            <td>{{ $pr->stok }}</td>
                                            <td style="display: none">{{ $pr->id }}</td>
                                            <td>{{ date('d M Y', strtotime($pr->created_at)) }}</td>
                                            <td>
                                                <form action="{{ url('admin/daftar-paket-reguler/destroy/'.$pr->id) }}" method="POST" id="deleteData-{{$pr->id}}">
                                                    @csrf
                                                </form>
                                                    <a href="{{ url('admin/daftar-paket-reguler/update/'.$pr->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                                                    <!-- <a href="{{ url('admin/daftar-paket-reguler/detail/'.$pr->id) }}" class="btn btn-success btn-sm"><i class="fa fa-eye"></i></a> -->
                                                    <button type="submit" class="btn btn-danger btn-sm" onclick="deleteData({{ $pr->id }})"><i class="fa fa-trash"></i></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#list-daftar-paket-reguler').DataTable({
                processing: true,
                responsive: true,
                aaSorting: [[ 3, "desc" ]]
            }  );
        } );

        function deleteData(id){
            swal({
                title: "Alert!",
                text: "Apakah anda yakin akan menghapus data tersebut?",
                type: 'error',
                showCancelButton: true,
                confirmButtonText: 'Ya, Saya yakin',
                cancelButtonText: 'Tidak',
            },function(result) {
                if(result){
                    document.getElementById('deleteData-'+id).submit();
                }
            });
        }
    </script>
@endsection