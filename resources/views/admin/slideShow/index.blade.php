@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('success') }}</strong>
                        </div>
                    @endif
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3 class="card-title">
                                        Slide Show
                                    </h3>
                                </div>
                                <div class="col-md-2">
                                    <a href="{{ url('admin/slide-show/create') }}" class="btn btn-primary btn-sm" style="float: right">Tambah Slide Show</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="col-md-12">
                                <table id="userManagement" class="display table table-hover table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Parent</th>
                                            <th>Total Slide Show</th>
                                            <th width="130px">Action</th>
                                        </tr>
                                        
                                    </thead>
                                    @foreach ($show as $key => $v)
                                        <tr>
                                            <td>{{ $no++ }}</td>
                                            <td>
                                                @if ($v->is_template == 1)
                                                    <p>Slide Show Page 1</p>
                                                @elseif ($v->is_template == 2)
                                                    <p>Slide Show Page 2</p>
                                                @elseif ($v->is_template == 6)
                                                    <p>Slide Show Page 6</p>
                                                @else 
                                                    <p>-</p>
                                                @endif
                                            </td>
                                            <td>{{ $v->total }}</td>
                                            <td>
                                                <a href="{{ url('admin/slide-show/detail-parent/'.$v->is_template) }}" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#userManagement').DataTable();
        } );
    </script>
@endsection