@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('success') }}</strong>
                        </div>
                    @endif
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3 class="card-title">
                                        Slide Show
                                    </h3>
                                </div>
                                <div class="col-md-2">
                                    <a href="{{ url('admin/slide-show/create') }}" class="btn btn-primary btn-sm" style="float: right">Tambah Slide Show</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="col-md-12">
                                <table id="userManagement" class="display table table-hover table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Judul</th>
                                            <th>Deskripsi</th>
                                            <th>Images</th>
                                            <th width="130px">Action</th>
                                        </tr>
                                        
                                    </thead>
                                    @foreach ($show as $key => $v)
                                        <tr>
                                            <td>{{ $no++ }}</td>
                                            <td>{{ $v->judul }}</td>
                                            <td>{!! $v->deskripsi !!}</td>
                                            <td><center><img src="{{ url('storage/img_slide-show/'.$v->images) }}" alt="" width="70px"></center></td>
                                            <td>
                                                <form action="{{url('admin/slide-show/destroy/'.$v->id)}}" method="post">
                                                    @csrf
                                                    <a href="{{ url('admin/slide-show/update/'.$v->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                                                    <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                                                </form>
                                                
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#userManagement').DataTable();
        } );
    </script>
@endsection