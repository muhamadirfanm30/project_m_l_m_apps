@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <form action="{{ url('admin/slide-show/edit/'.$dataEdit->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="col-md-12">
                    <div class="container">
                        <div data-role="dynamic-fields">
                            <div class="form-inline">
                                <div class="col-md-12">
                                    <div class="card card-outline card-info">
                                        <div class="card-header">
                                            <div class="row">
                                                <div class="col-md-11">
                                                    <h3 class="card-title">
                                                        Slide Show
                                                    </h3>
                                                </div>
                                                <div class="col-md-1">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <div class="col-md-1">
                                                                <label for="exampleFormControlSelect1">Template</label>
                                                            </div>
                                                            <div class="col-md-11">
                                                                <select class="form-control" name="is_template" id="exampleFormControlSelect1" style="width:100%">
                                                                    @if($dataEdit->is_template == 1)
                                                                        <option value="1" selected>Slide Show Landing Page 1</option>
                                                                        <option value="2">Slide Show Landing Page 2</option>
                                                                        <option value="6">Slide Show Landing Page 6</option>
                                                                    @elseif($dataEdit->is_template == 2)
                                                                        <option value="1">Slide Show Landing Page 1</option>
                                                                        <option value="2" selected>Slide Show Landing Page 2</option>
                                                                        <option value="6">Slide Show Landing Page 6</option>
                                                                    @elseif($dataEdit->is_template == 6)
                                                                        <option value="1">Slide Show Landing Page 1</option>
                                                                        <option value="2">Slide Show Landing Page 2</option>
                                                                        <option value="6" selected>Slide Show Landing Page 6</option>
                                                                    @endif
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div><br><br><br>
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <div class="col-md-1">
                                                                <label>Judul </label>
                                                            </div>
                                                            <div class="col-md-11">
                                                                <input type="text" name="judul" class="form-control" value="{{$dataEdit->judul}}" placeholder="Judul ..." style="width:100%">
                                                                @error('judul')
                                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    </div><br><br><br>
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <div class="col-md-1">
                                                                <label>Deskripsi</label>
                                                            </div>
                                                            <div class="col-md-11">
                                                                <textarea id="summernote" name="deskripsi">{!! $dataEdit->deskripsi !!}</textarea>
                                                                @error('deskripsi')
                                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    </div><br><br><br>
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <div class="col-md-1">
                                                                <label>Gambar</label>
                                                            </div>
                                                            <div class="col-md-11">
                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <input type="file" name="images" class="form-control" id="" style="widh:100%">
                                                                        @error('images')
                                                                            <span style="color:red"><small>{{ $message }}</small></span>
                                                                        @enderror
                                                                    </div>
                                                                    <div class="col-md-1">
                                                                        @if (!empty($dataEdit->images))
                                                                            <a href="{{ url('storage/img_slide-show/'.$dataEdit->images) }}" target="_blank" class="badge badge-success">Lhat Photo</a>
                                                                        @else
                                                                            <span class="badge badge-warning">Gambar tidak ditemukan</span>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>  <!-- /div.form-inline -->
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary" style="float: right">Submit</button>
                </div>
            </form>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>

        $(function () {
            // Summernote
            $('#summernote').summernote()

            //Date range picker
            $('#reservationdate').datetimepicker({
                format: 'YYYY-MM-DD'
            });
        })
        </script>
@endsection