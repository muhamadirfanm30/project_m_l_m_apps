<table>
    <thead>
        <tr>
            <th>Nama</th>
            <th>Total Omset</th>
            <th>Total Produk Terjual</th>
            <th>Total Reseller</th>
            <th>Tanggal Bergabung</th>
        </tr>
    </thead>
    <tbody>
    @php $no = 1 @endphp
        @foreach($datas as $value)
            @php
                $omset = 0;
                $produkTerjual = 0;
                foreach($value->order as $key => $order){
                    $omset += $order->totalKeseluruhan;
                    foreach($order->order_detail as $key => $salesProduk){
                        $produkTerjual += $salesProduk->qty;
                        
                    }
                }
            @endphp
            <tr>
                <td>{{ $value->first_name }} {{ $value->last_name }}</td>
                <td>Rp. {{ number_format($omset) }}</td>
                <td>{{ number_format($produkTerjual) }}</td>
                <td>{{ number_format($value->my_member) }}</td>
                <td>{{ date('d M Y', strtotime($value->created_at)) }}</td>
            </tr>
        @endforeach
    </tbody>
</table>