<table>
    <thead>
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Email</th>
            <th>Membership Status</th>
            <th>Membership ID</th>
            <th>Register At</th>
        </tr>
    </thead>
    <tbody>
    @php $no = 1 @endphp
    @foreach($users as $user)
        <tr>
            <td>{{ $no++ }}</td>
            <td>{{ !empty($user->first_name) ? $user->first_name : '-' }}</td>
            <td>{{ !empty($user->email) ? $user->email : '-' }}</td>
            <td>{{ !empty($user->membersip_status) ? $user->membersip_status : '-' }}</td>
            <td>{{ !empty($user->membership_id) ? $user->membership_id : '-' }}</td>
            <td>{{ date('d M Y', strtotime($user->created_at)) }}</td>
        </tr>
    @endforeach
    </tbody>
</table>