@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('success') }}</strong>
                        </div>
                    @endif
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3 class="card-title">
                                        Update Data Sales Training
                                    </h3>
                                </div>
                                <div class="col-md-2">
                                    <a href="{{ url('admin/sales-and-training/show') }}" class="btn btn-primary btn-sm" style="float: right">Back</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <form action="{{ url('admin/sales-and-training/edit/'.$getDataSales->id) }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Judul</label>
                                                <input type="text" name="judul" value="{{$getDataSales->judul}}" class="form-control" placeholder="Judul ...">
                                                <input type="hidden" name="id_parent" value="{{$getDataSales->id}}">
                                                @error('judul')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Foto</label>
                                                <input type="file" name="foto" value="{{$getDataSales->foto}}" class="form-control" placeholder="Photo ...">
                                                @if (!empty($getDataSales->foto))
                                                    <a href="{{ url('storage/sales-training-image/'.$getDataSales->foto) }}" target="_blank" rel="noopener noreferrer" class="badge badge-primary"><i class="fa fa-image"></i>  {{$getDataSales->foto}}</a>    
                                                @else
                                                    <span class="badge badge-warning" style="color: white"><i class="fa fa-image"></i>  Gambar Tidak Ada</span>
                                                @endif
                                                @error('foto')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Deskripsi</label>
                                                <textarea id="summernote" name="deskripsi">{{$getDataSales->deskripsi}}</textarea>
                                                @error('deskripsi')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Publish At</label>
                                                <div class="input-group date" id="reservationdate" data-target-input="nearest">
                                                    <input type="text" name="publish_at" value="{{$getDataSales->publish_at}}" class="form-control datetimepicker-input" data-target="#reservationdate"/>
                                                    <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                    </div>
                                                </div>
                                                @error('publish_at')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary" style="float: right">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="card card-outline card-info">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-10">
                            <h3 class="card-title">
                                Update Detail Sales Training
                            </h3>
                        </div>
                        <div class="col-md-2">
                            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal" style="float: right">
                                Tambah Data
                            </button>
                        </div>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="konten" class="display table table-hover table-bordered" style="width:100%">
                                <tr>
                                    <td style="display: none"></td>
                                    <td>Judul Vidio</td>
                                    <td>Url Vidio</td>
                                    <td>Action</td>
                                </tr>
                                <tbody>
                                    @foreach ($getVidio as $key => $val)
                                        <tr>
                                            <td>{{$val->judul_vidio}}</td>
                                            <td>{{$val->url_vidio}}</td>
                                            <td>
                                                <form action="{{ route('sales.destroy.vidio',['id'=>$val->id]) }}" method="POST" id="deleteData-{{$val->id}}">
                                                    @csrf
                                                </form>
                                                <button type="button" data-id ="{{ $val->id }}" class="btn btn-primary btn-sm" id="update_url_vidio" data-toggle="tooltip" data-html="true" title="Update Data"><i class="fa fa-edit"></i></button>
                                                <button class="btn btn-danger btn-sm" id="btn-delete" onclick="deleteData({{ $val->id }})"><i class="fa fa-trash"></i></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>  <!-- /div.col-md-12 -->
                    </div> 
                </div>
            </div>
            <div class="modal fade" id="modal_update" data-backdrop="false" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Update Data</h5>
                            
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <input type="hidden" name="id" id="getIds">
                            <div id="content_topup"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Tambah Data</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form action="{{ url('admin/sales-and-training/store_vidio') }}" method="post">
                            @csrf
                            
                            <div class="modal-body">
                                <div class="form-group">
                                    
                                    <label for="">Judul Vidio</label>
                                    <input type="text" name="judul_vidio" class="form-control"  placeholder="Masukan Judul Vidio" required>
                                    <input type="hidden" name="parent_id" class="form-control" value="{{ $getDataSales->id }}">
                                </div>
                                <div class="form-group">
                                    <label for="">Url Vidio</label>
                                    <input type="text" name="url_vidio" class="form-control" placeholder="Masukan Url Vidio" required>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save changes</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <style>
        .entry:not(:first-of-type)
            [data-role="dynamic-fields"] > .form-inline + .form-inline {
            margin-top: 0.5em;
        }

        [data-role="dynamic-fields"] > .form-inline [data-role="add"] {
            display: none;
        }

        [data-role="dynamic-fields"] > .form-inline:last-child [data-role="add"] {
            display: inline-block;
        }

        [data-role="dynamic-fields"] > .form-inline:last-child [data-role="remove"] {
            display: none;
        }
    </style>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js">
    </script>

    <script>
        $(document).on('click', '#update_vidios', function(){
            id_parents = $(this).attr('data-parent');
            id_save = $(this).attr('data-save');
            let judul_vidio = $('#judul_vidio').val();
            let url_vidio = $('#url_vidio').val();
            Helper.loadingStart()
            $.ajax({
                url: '/admin/sales-and-training/edit_vidio/' + id_save,
                data:{
                    judul_vidio,
                    url_vidio,
                },
                type: 'post',
                success: function(resp) {
                    Helper.loadingStop()
                    console.log(resp)
                    swal({
                    title: "Sukses!",
                        text: "Data Berhasil Diubah",
                        type: 'success'
                    }, function() {
                        window.location.href='/admin/sales-and-training/update/'+id_parents;
                    });
                },
                error:function(a,v,c){
                    Helper.loadingStop()
                    var msg = JSON.parse(a.responseText);
                    swal({
                    title: "Ups!",
                        text: 'Terjadi kesalahan saat mengubah Data',
                        type: 'error'
                    });
                }
            })
        });

        function deleteData(id){
            swal({
                title: "Alert!",
                text: "Apakah anda yakin akan menghapus data tersebut?",
                type: 'error',
                showCancelButton: true,
                confirmButtonText: 'Ya, Saya yakin',
                cancelButtonText: 'Tidak',
            },function(result) {
                if(result){
                    document.getElementById('deleteData-'+id).submit();
                }
            });
        }

        var $modal = $('#modal_update').modal({
            show: false
        });



        var ProductAdditionalObj = {
            // isi field input
            isiDataFormModal: function(id) {
                $.ajax({
                    url: '/admin/sales-and-training/get_vidio/' + id,
                    type: 'get',
                    success: function(resp) {
                        $("#content_topup").html('');
                        var content = '';
                        var templateTopup = '';

                        templateTopup +=   `<div class="form-group">
                                                <div class="form-group">
                                                    <input type="hidden" name="id" value="${resp.id}">
                                                    <label for="">Judul Vidio</label>
                                                    <input type="text" name="judul_vidio" id="judul_vidio" class="form-control" value="${resp.judul_vidio}" placeholder="Masukan Judul Vidio">
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Url Vidio</label>
                                                    <input type="text" name="url_vidio" id="url_vidio" class="form-control" value="${resp.url_vidio}" placeholder="Masukan Url Vidio">
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button class="btn btn-primary waves-effect" data-save="${resp.id}" data-parent="${resp.parent_id}" id="update_vidios" type="submit">
                                                    Simpan
                                                </button>
                                            </div>`;
                        $("#content_topup").append(templateTopup);
                    },
                    error: function(xhr, status, error) {
                        Helper.errorMsgRequest(xhr, status, error);
                    },
                })

            },
            // hadle ketika response sukses
            successHandle: function(resp) {
                // send notif
                Helper.successNotif(resp.msg);
                $modal.modal('hide');
            },
        }
        
        $(document)
            .on('click', '#update_url_vidio', function() {
                id = $(this).attr('data-id');
                $('#getIds').val(id)
                $('.modal_sales_training').show();
                console.log(ProductAdditionalObj.isiDataFormModal(id));
                $modal.modal('show');
            })
    </script>
    <script>
        $(function() {
            // Remove button click
            $(document).on(
                'click',
                '[data-role="dynamic-fields"] > .form-inline [data-role="remove"]',
                function(e) {
                    e.preventDefault();
                    $(this).closest('.form-inline').remove();
                }
            );
            // Add button click
            $(document).on(
                'click',
                '[data-role="dynamic-fields"] > .form-inline [data-role="add"]',
                function(e) {
                    e.preventDefault();
                    var container = $(this).closest('[data-role="dynamic-fields"]');
                    new_field_group = container.children().filter('.form-inline:first-child').clone();
                    new_field_group.find('input').each(function(){
                        $(this).val('');
                    });
                    container.append(new_field_group);
                }
            );
        });
    </script>
    <script>
        $(document).ready(function() {
            $('#ststus-orders').DataTable();
        } );

        $(function () {
            // Summernote
            $('#summernote').summernote()

            //Date range picker
            $('#reservationdate').datetimepicker({
                format: 'YYYY-MM-DD'
            });
        })
    </script>
@endsection