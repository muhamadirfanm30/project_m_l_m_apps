@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3 class="card-title">
                                        Penjelasan Panel HWI
                                    </h3>
                                </div>
                                <div class="col-md-2">
                                    <a href="{{ url('admin/panel-hwi/show') }}" class="btn btn-primary btn-sm" style="float: right">Back</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <form action="{{ url('admin/panel-hwi/store') }}" method="post">
                            @csrf
                            <div class="card-body">
                                <div class="col-md-12">
                                    <div data-role="dynamic-fields">
                                        <div class="form-inline">
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label>Judul Vidio</label>
                                                    <input type="text" name="judul[]" class="form-control" id="field-name" placeholder="Judul Vidio" style="width:100%" required>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Url Vidio</label>
                                                    <input type="text" name="vidio[]" class="form-control" id="field-value" placeholder="Url Vidio" style="width:100%" required>
                                                </div>
                                            </div>
                                            <div class="col-md-1">
                                                <label for="" style="color:white">add btn</label>
                                                <button class="btn btn-danger btn-sm" data-role="remove">
                                                    <span class="fa fa-minus"></span>
                                                </button>
                                                <button class="btn btn-primary btn-sm" data-role="add">
                                                    <span class="fa fa-plus"></span>
                                                </button>
                                            </div><br><br><br>
                                        </div>  <!-- /div.form-inline -->
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary" style="float: right">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <style>
        .entry:not(:first-of-type)
            [data-role="dynamic-fields"] > .form-inline + .form-inline {
            margin-top: 0.5em;
        }

        [data-role="dynamic-fields"] > .form-inline [data-role="add"] {
            display: none;
        }

        [data-role="dynamic-fields"] > .form-inline:last-child [data-role="add"] {
            display: inline-block;
        }

        [data-role="dynamic-fields"] > .form-inline:last-child [data-role="remove"] {
            display: none;
        }
    </style>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>
        $(function() {
            // Remove button click
            $(document).on(
                'click',
                '[data-role="dynamic-fields"] > .form-inline [data-role="remove"]',
                function(e) {
                    e.preventDefault();
                    $(this).closest('.form-inline').remove();
                }
            );
            // Add button click
            $(document).on(
                'click',
                '[data-role="dynamic-fields"] > .form-inline [data-role="add"]',
                function(e) {
                    e.preventDefault();
                    var container = $(this).closest('[data-role="dynamic-fields"]');
                    new_field_group = container.children().filter('.form-inline:first-child').clone();
                    new_field_group.find('input').each(function(){
                        $(this).val('');
                    });
                    container.append(new_field_group);
                }
            );
        });
    </script>
    <script>
        $(document).ready(function() {
            $('#ststus-orders').DataTable();
        } );

        $(function () {
            // Summernote
            $('#summernote').summernote()

            //Date range picker
            $('#reservationdate').datetimepicker({
                format: 'YYYY-MM-DD'
            });
        })
    </script>
@endsection