@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('success') }}</strong>
                        </div>
                    @endif
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-9">
                                    <h3 class="card-title">
                                        Panel HWI
                                    </h3>
                                </div>
                                <div class="col-md-3">
                                    <a href="{{ url('admin/panel-hwi/create') }}" class="btn btn-primary btn-sm" style="float: right">Tambah Penjelasan Panel HWI</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="col-md-12">
                                <table id="userManagement" class="display table table-hover table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            {{-- <th>No</th> --}}
                                            <th>Judul</th>
                                            <th>Url Vidio</th>
                                            <th style="display: none"></th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    @foreach ($show as $key => $hwi)
                                        <tr>
                                            {{-- <td>{{ $no++ }}</td> --}}
                                            <td>{{ $hwi->judul }}</td>
                                            <td>{{ $hwi->vidio }}</td>
                                            <td style="display: none">{{ $hwi->id }}</td>
                                            <td>
                                                <form action="{{ url('admin/panel-hwi/destroy/'.$hwi->id) }}" method="POST">
                                                    @csrf
                                                    <a href="{{ url('admin/panel-hwi/edit/'.$hwi->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                                                    <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                                {{-- <form action="{{ url('admin/panel-hwi/deleteAll') }}" method="post">
                                    @csrf
                                    <button type="submit" class="btn btn-warning btn-sm" style="color:white"><i class="fa fa-trash"></i> &nbsp;Hapus Semua Data</button>
                                </form> --}}
                            </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#userManagement').DataTable({
                processing: true,
                responsive: true,
                aaSorting: [[ 2, "desc" ]]
            }  );
        } );
    </script>
@endsection