@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3 class="card-title">
                                        Update Kategori
                                    </h3>
                                </div>
                                <div class="col-md-2">
                                    <a href="{{ url('admin/kategori-produk') }}" class="btn btn-primary btn-sm" style="float: right">Back</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <form action="{{ url('admin/kategori-produk/update/'.$edit->id) }}" method="post">
                            @csrf
                            <div class="card-body">
                                <div class="col-md-12">
                                    <div data-role="dynamic-fields">
                                        <div class="form-inline">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Kategori Produk</label>
                                                    <input type="text" name="name" class="form-control" value="{{$edit->name}}" id="field-name" placeholder="Field Name" style="width:100%" required>
                                                    @error('name')
                                                    <span style="color: red"><small>{{ $message }}</small></span>
                                                @enderror
                                                </div>
                                            </div>
                                        </div>  <!-- /div.form-inline -->
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary" style="float: right">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <style>
        .entry:not(:first-of-type)
            [data-role="dynamic-fields"] > .form-inline + .form-inline {
            margin-top: 0.5em;
        }

        [data-role="dynamic-fields"] > .form-inline [data-role="add"] {
            display: none;
        }

        [data-role="dynamic-fields"] > .form-inline:last-child [data-role="add"] {
            display: inline-block;
        }

        [data-role="dynamic-fields"] > .form-inline:last-child [data-role="remove"] {
            display: none;
        }
    </style>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
@endsection