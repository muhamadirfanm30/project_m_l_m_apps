@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('success') }}</strong>
                        </div>
                    @endif
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3 class="card-title">
                                        Show List kategori Produk
                                    </h3>
                                </div>
                                <div class="col-md-2">
                                    <a href="{{ url('admin/kategori-produk/create') }}" class="btn btn-primary btn-sm" style="float: right">Tambah Kategori</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="col-md-12">
                                <table id="userManagement" class="display table table-hover table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Kategori</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    @foreach ($showData as $key => $kategori)
                                        <tr>
                                            <td>{{ $no++ }}</td>
                                            <td>{{ $kategori->name }}</td>
                                            <td>
                                                {{-- <form action="{{ url('admin/kategori-produk/destroy/'.$kategori->id) }}" method="POST">
                                                    @csrf --}}
                                                    <input type="hidden" name="ids" value="{{ $kategori->id }}" id="get_id">
                                                    <a href="{{ url('admin/kategori-produk/edit/'.$kategori->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                                                    <button type="submit" class="btn btn-danger btn-sm" id="btn-delete"><i class="fa fa-trash"></i></button>
                                                {{-- </form> --}}
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                                <form action="{{ url('admin/kategori-produk/deleteAll') }}" method="post">
                                    @csrf
                                    <button type="submit" class="btn btn-warning btn-sm" style="color:white"><i class="fa fa-trash"></i> &nbsp;Hapus Semua Data</button>
                                </form>
                            </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#userManagement').DataTable();
        } );

        $(document).on('click', '#btn-delete', function(){
            swal({
                title: "Apa anda yakin?",
                text: "Akan Menghapus Kategori ini ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ya, Saya Yakin",
                cancelButtonText: 'Tidak',
                closeOnConfirm: false,
                showCloseButton: true
            }, function (isConfirm) {
                if (!isConfirm) return;
                id = $('#get_id').val();
                Helper.loadingStart()
                $.ajax({
                    url: '/admin/kategori-produk/destroy/' + id,
                    type: 'post',
                    success: function(resp) {
                        Helper.loadingStop()
                        console.log(resp)
                        swal({
                        title: "Sukses!",
                            text: "Kategori Berhasil di hapus",
                            type: 'success'
                        }, function() {
                            window.location.href='/admin/kategori-produk';
                        });
                    },
                    error:function(a,v,c){
                        Helper.loadingStop()
                        var msg = JSON.parse(a.responseText);
                        swal({
                        title: "Ups!",
                            text: 'Terjadi kesalahan saat menghapus Kategori',
                            type: 'error'
                        });
                    }
                })
            });
            
        });
    </script>
@endsection