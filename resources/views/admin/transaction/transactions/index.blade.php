@extends('home')
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <!-- Button trigger modal -->
                    <div class="modal fade bd-example-modal-lg" id="approve_user" data-backdrop="true" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Konfirmasi Pembayaran</h5>
                                    <input type="hidden" name="id" id="data_id">
                                    <input type="hidden" name="selisih" id="data_selisih">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <div class="col-md-4"><strong>Order Code</strong></div>
                                                        <div class="col-md-8">: <strong><span id="order_code"></span></strong></div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-5"><strong>Tanggal Pesanan</strong></div>
                                                        <div class="col-md-6">: <strong><span id="date"></span></strong></div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-5"><strong>Bukti Transfer</strong></div>
                                                        <div class="col-md-6" id="appen_image">:  </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-5"><strong>Metode Pembayaran</strong></div>
                                                        <div class="col-md-6">: <strong><span id="payment"></span></strong> </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6" style="float:right">
                                                    <div class="row">
                                                        <div class="col-md-4"><strong>Pesanan Dari</strong></div>
                                                        <div class="col-md-6">: <strong><span id="nama_user"></span></strong> </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4"><strong>Email</strong></div>
                                                        <div class="col-md-8">: <strong><span id="email"></span></strong> </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4"><strong>Status</strong></div>
                                                        <div class="col-md-6">: <span id="status" class="badge badge-warning" style="color:white"><strong>Menunggu Approval Admin</strong></span> </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div> <br>
                                    <table class="table table-hover table-bordered additional_parts" style="width:100%">
                                        <thead>
                                            <th>Sparepart Name</th>
                                            <th>Qty</th>
                                            <th>Price</th>
                                        </thead>
                                        <tbody id="data_produk"></tbody>
                                        <tbody>
                                            <tr>
                                                <td colspan="2" align="right">Ongkos Kirim</td>
                                                <td id="getShippCost">Rp. </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" align="right">Grand Total</td>
                                                <td id="getTotals">Rp. </td>
                                            </tr>
                                        </tbody>
                                    </table><hr id="line">
                                    <input type="hidden" name="id" id="getIds">
                                    <input type="hidden" id="amounts" name="getTotals">
                                    <div id="btn_approve"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('success') }}</strong>
                        </div>
                    @endif
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3 class="card-title">
                                        Data Transaksi User
                                    </h3>
                                </div>
                                <div class="col-md-2">
                                </div>
                            </div>
                        </div>
                         
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="col-md-12">
                                <table id="userManagement" class="display table table-hover table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            {{-- <th>No</th> --}}
                                            <th>Order Id</th>
                                            <th>Status</th>
                                            <th style="display: none"></th>
                                            <th>Tanggal Transaksi</th>
                                            <th>Batas Pembayaran</th>
                                            <th width="130px">Action</th>
                                        </tr>
                                        
                                    </thead>
                                    @foreach ($dataTransaksi as $key => $data)
                                        <tr>
                                            {{-- <td>{{ $no++ }}</td> --}}
                                            <td><a href="{{url('admin/data-transactions/detail/'.$data->orderId)}}" style="color: blue; text-decoration:underline"><strong>{{ $data->orderId }}</strong></span></a>&nbsp;&nbsp;<span class="badge badge-warning">{{ $data->notif_no_read_data_konfirmasi_payment != null ? 'new' : '' }}</span></td>
                                            <td><span class="badge badge-danger">{{ $data->status == 0 ? 'Menunggu Konfirmasi Admin' : '-' }}</span></td>
                                            <td style="display: none">{{ $data->id }}</td>
                                            <td>{{ date('d M Y H:i', strtotime($data->created_at)) }}</td>
                                            <td>{{ date('d M Y H:i', strtotime($data->expired_date)) }}</td>
                                            <td>
                                                <form action="" method="POST">
                                                    @csrf
                                                    <a href="{{url('admin/data-transactions/detail/'.$data->orderId)}}" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                                                    {{-- <button type="button" data-id ="{{$data->orderId}}" class="btn btn-success btn-sm btn_confrim_part" data-toggle="tooltip" data-html="true" title="Detail Pesanan"><i class="fa fa-check"></i></button> --}}
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
    </div>
   
    
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#userManagement').DataTable(
                {
                    processing: true,
                    responsive: true,
                    aaSorting: [[ 2, "desc" ]]
                }
            );
        } );
    </script>

    <script>
        function thousandsSeparators(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
        
        var $modal = $('#approve_user').modal({
            show: false,
        });
        
        var ProductAdditionalObj = {
            // isi field input
            isiDataFormModal: function(id) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/admin/transactions/get-product/'+id,
                    type: 'get',
                    success: function(resp) {
                        // alert(resp.data.orders_statuses_id)
                        console.log(resp.orderId)
                        $("#data_produk").html('');
                        $("#btn_approve").html('');
                        $("#getShippCost").html('');
                        $("#getTotals").html('');
                        $("#order_code").html('');
                        $("#date").html('');
                        $('#appen_image').html('');
                        $('#va').html('');
                        $('#nama_user').html('');
                        $('#email').html('');
                        $('#payment').html('');
                        var btn     = '';
                        var content     = '';
                        var srcImage    = '';
                        var virtualAccount = '';
                        var total_price = 0;
                        var shipping    = 0;
                        var url         = '/storage/image-transaction/'+resp.photo
                        // var url         = 'http://localhost:8000/storage/image-transaction/'+resp.photo
                        srcImage        = `<a href="${url}" target="_blank" rel="noopener noreferrer" class="badge badge-primary">Bukti Transfer</a>`;
                        virtualAccount  = resp.va
                        user            = resp.get_user.first_name
                        email           = resp.get_user.email
                        pembayaran      = resp.payment_method
                        btn             = '<button class="btn btn-primary terima_pembayaran" style="float: right"><i class="fa fa-check">&nbsp;</i>&nbsp; Terima Pembayaran</button>';
                        _.each(resp.order_detail, function(detail) {
                            total_price = resp.totalKeseluruhan;
                            shipping = resp.totalOngkir != null ?  formatRupiah(resp.totalOngkir) : '-';
                            nama_produk = '';
                            if(detail.get_product != null){
                                nama_produk = detail.get_product.nama_produk;
                            }else{
                                nama_produk = detail.product_id;
                            }
                            content += `<tr>
                                            <td>${nama_produk}</td>
                                            <td>${detail.qty}</td>
                                            <td>${formatRupiah(detail.harga * detail.qty )}</td>
                                        </tr>`;
                        });

                        $('#getTotals').append(formatRupiah(total_price))
                        $('#btn_approve').append(btn)
                        $("#getShippCost").html(shipping);
                        $("#data_produk").append(content);
                        $("#appen_image").append(srcImage);
                        $("#order_code").text(id)
                        $('#nama_user').text(user);
                        $('#email').text(email);
                        $('#payment').text(pembayaran);
                        $("#va").text(virtualAccount)
                        $("#date").text( moment(resp.created_at).format("DD MMMM YYYY"))
                    },
                    error:function(a,v,c){
                        var msg = JSON.parse(a.responseText);
                        console.log(msg);
                        swal({
                            title: "Ups!",
                            text: msg.message,
                            type: 'error'
                        });
                    }
                });

            },
            // hadle ketika response sukses
            // success:function(data) {
            //     // send notif
            //     // Helper.successNotif(resp.msg);
            //     $modal.modal('hide');
            // },
        }

        $(document)
            .on('click', '.btn_confrim_part', function() {
                console.log(id = $(this).attr('data-id'));
                $('#getIds').val(id)
                ProductAdditionalObj.isiDataFormModal(id);
                $modal.modal('show');
            });

        $(document).on('click', '.terima_pembayaran', function(){
            id = $('#getIds').val();
            Helper.loadingStart()
            $.ajax({
                url: '/admin/transactions/terima-pembayaran/' + id,
                type: 'post',
                success: function(resp) {
                    Helper.loadingStop()
                    console.log(resp)
                    swal({
                    title: "Sukses!",
                        text: "Pembayaran Sukses",
                        type: 'success'
                    }, function() {
                        window.location.href='/admin/transactions/show';
                    });
                },
                error:function(a,v,c){
                    Helper.loadingStop()
                    var msg = JSON.parse(a.responseText);
                    console.log(msg);
                    swal({
                        title: "Ups!",
                            text: msg.message,
                            type: 'error'
                        });
                }
            })

        });
    </script>
@endsection