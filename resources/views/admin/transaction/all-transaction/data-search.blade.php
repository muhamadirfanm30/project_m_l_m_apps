@extends('home')
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <!-- Button trigger modal -->
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('success') }}</strong>
                        </div>
                    @endif
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <form action="{{ url('/admin/data-transactions/show/search') }}" method="post">
                                @csrf
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-9">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    Sort By Kategori :
                                                                </div>
                                                                <div class="col-md-8">
                                                                    <select class="form-control" name="by_kategori">
                                                                        <option>Semua Kategori</option>
                                                                        <option {{ $by_kategori == "Pembelian Produk" ? "selected":"" }} value="Pembelian Produk">Pembelian Produk</option>
                                                                        <option {{ $by_kategori == "Akumulasi Paket MS Upgrade" ? "selected":"" }} value="Akumulasi Paket MS Upgrade">Akumulasi Paket Upgrade MS</option>
                                                                        <option {{ $by_kategori == "Pendaftaran Member Reguler" ? "selected":"" }} value="Pendaftaran Member Reguler">Pendaftaran Member Reguler</option>
                                                                        <option {{ $by_kategori == "Pembayaran Daftar Event" ? "selected":"" }} value="Pembayaran Daftar Event">Pendaftaran Even</option>
                                                                        <option {{ $by_kategori == "Ongkos Kirim Pengiriman Stok Produk" ? "selected":"" }} value="Ongkos Kirim Pengiriman Stok Produk">Ongkos Kirim Produk Online</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    Sort By Status :
                                                                </div>
                                                                <div class="col-md-8">
                                                                    <select class="form-control" name="by_status">
                                                                        <option>Semua Pesanan</option>
                                                                        <option {{ $by_status == "0" ? "selected":"" }} value="0">Menunggu Pembayaran</option>
                                                                        {{-- <option {{ $by_status == "m" ? "selected":"" }} value="menunggu_konfirmasi_admin">Menunggu Konfirmasi Admin</option> --}}
                                                                        <option {{ $by_status == "1" ? "selected":"" }} value="1">Pesanan Sedang Diproses</option>
                                                                        <option {{ $by_status == "4" ? "selected":"" }} value="4">Pesanan Dikirim</option>
                                                                        <option {{ $by_status == "5" ? "selected":"" }} value="5">Pesanan Selesai</option>
                                                                        <option {{ $by_status == "2" ? "selected":"" }} value="2">Pesan Dibatalkan</option>
                                                                        <option {{ $by_status == "3" ? "selected":"" }} value="3">Pesan Expired</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <button type="submit" class="btn btn-primary btn-sm btn-block">Cari</button>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <a href="{{url('/admin/data-transactions/show')}}" class="btn btn-success btn-sm btn-block">Reset Pencarian</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            Data Transaksi User 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                         
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="col-md-12">
                                <table id="userManagement" class="display table table-hover table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            {{-- <th>No</th> --}}
                                            <th>Order Id</th>
                                            <th>Kategori</th>
                                            <th>Status</th>
                                            <th style="display: none"></th>
                                            {{-- <th>Nomor Resi</th> --}}
                                            <th>Tanggal Transaksi</th>
                                            <th>Batas Pembayaran</th>
                                            <th width="130px">Action</th>
                                        </tr>
                                        
                                    </thead>
                                    @foreach ($data_search as $key => $data)
                                    @php
                                        $status = '';
                                        $warna = '';
                                        $resi = '';
                                        if($data->status == 0 && empty($data->photo)){
                                            $status = 'Menunggu Pembayaran';
                                            $resi = '-';
                                            $warna = 'danger';
                                        }elseif($data->status == 0 && !empty($data->photo) ){
                                            $status = 'Menunggu Konfirmasi Admin';
                                            $resi = '-';
                                            $warna = 'danger';
                                        }elseif($data->status == 1){
                                            $status = 'Pesanan Sedang Diproses dan Dikemas';
                                            $resi = 'Menunggu Resi Pengiriman';
                                            $warna = 'success';
                                        }elseif($data->status == 2){
                                            $status = 'Batal';
                                            $resi = '-';
                                            $warna = 'danger';
                                        }elseif($data->status == 3){
                                            $status = 'Expired';
                                            $warna = 'warning';
                                            $resi = '-';
                                        }elseif($data->status == 4){
                                            $status = 'Pesanan Dikirim';
                                            $warna = 'primary';
                                            $resi = $data->resi_pengiriman;
                                        }elseif($data->status == 5){
                                            $status = 'Pesanan Selesai';
                                            $warna = 'primary';
                                            $resi ='-';
                                        }else{
                                            $status = '-';
                                            $warna = 'seccondary';
                                            $resi = '-';
                                        }
                                    @endphp
                                        <tr>
                                            {{-- <td>{{ $no++ }} {{ $data->notif_no_read_data_transaksi != null ? 'new' : '' }}</td> --}}
                                            <td><a href="{{url('admin/data-transactions/detail/'.$data->orderId)}}" style="color: blue; text-decoration:underline"><strong>{{ $data->orderId }}</strong></a>&nbsp;&nbsp;<span class="badge badge-warning" style="color: white">{{ $data->notif_no_read_data_transaksi != null ? 'new' : '' }}</span></td>
                                            <td>{{ $data->kategori }}</td>
                                            <td><span class="badge badge-{{$warna}}">{{ $status }}</span></td>
                                            {{-- <td><span class="badge badge-{{$warna}}">{{ $resi }}</span></td> --}}
                                            <td style="display: none">{{ $data->id }}</td>
                                            <td>{{ date('d M Y H:i', strtotime($data->created_at)) }}</td>
                                            <td>{{ date('d M Y H:i', strtotime($data->expired_date)) }}</td>
                                            <td>
                                                <form action="" method="POST">
                                                    @csrf
                                                    {{-- @if($data->status == 0 && !empty($data->photo))
                                                        <a href="{{url('admin/data-transactions/detail/'.$data->orderId)}}" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                                                        <button type="button" data-id ="{{$data->orderId}}" class="btn btn-success btn-sm" id="btn_approve_pembayaran" data-toggle="tooltip" data-html="true" title="Detail Pesanan"><i class="fa fa-check"></i></button>
                                                    @elseif($data->status == 0 && empty($data->photo))
                                                        <a href="{{url('admin/data-transactions/detail/'.$data->orderId)}}" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                                                    @elseif($data->status == 1) 
                                                        <a href="{{url('admin/data-transactions/detail/'.$data->orderId)}}" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                                                        <button type="button" data-id ="{{$data->orderId}}" class="btn btn-success btn-sm" id="btn_update_resi" data-toggle="tooltip" data-html="true" title="Update Resi Pengiriman"><i class="fa fa-edit"></i></button>
                                                    @elseif($data->status == 4)
                                                        <button type="submit" id="terima_pesanan" class="btn btn-primary btn-sm" data-pesanan="{{ $data->orderId }}"> Pesanan Diterima</button> 
                                                    @else
                                                        <a href="{{url('admin/data-transactions/detail/'.$data->orderId)}}" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                                                    @endif --}}
                                                    <a href="{{url('admin/data-transactions/detail/'.$data->orderId)}}" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <!-- modal resi -->
    <div class="modal fade bd-example-modal-lg" id="update_resi" data-backdrop="true" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Update Resi Pengiriman</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" id="getIds">
                    <div class="row">
                        <div class="container">
                            <label for="">Input Nomor Resi</label>
                            <input type="text" class="form-control" id="resi_pengiriman" placeholder="Nomor Resi" name="resi_pengiriman">
                        </div>
                    </div><br>
                    <button class="btn btn-primary udate_resi btn-block" style="float: right"><i class="fa fa-check">&nbsp;</i>&nbsp; Update Resi pengiriman</button>
                </div>
            </div>
        </div>
    </div>

    <!-- modal approve pembayaran -->
    <div class="modal fade bd-example-modal-lg" id="approve_payment" data-backdrop="true" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Konfirmasi Pembayaran</h5>
                    <input type="hidden" name="id" id="data_id">
                    <input type="hidden" name="selisih" id="data_selisih">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6"><strong>Order Code</strong></div>
                                        <div class="col-md-6">: <strong><span id="order_code"></span></strong></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6"><strong>Tanggal Pesanan</strong></div>
                                        <div class="col-md-6">: <strong><span id="date"></span></strong></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6"><strong>Bukti Transfer</strong></div>
                                        <div class="col-md-6" id="appen_image">:  </div>
                                    </div>
                                </div>
                                <div class="col-md-6" style="float:right">
                                    <div class="row">
                                        <div class="col-md-4"><strong>Pesanan Dari</strong></div>
                                        <div class="col-md-6">: <strong><span id="nama_user"></span></strong> </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4"><strong>Email</strong></div>
                                        <div class="col-md-8">: <strong><span id="email"></span></strong> </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4"><strong>Status</strong></div>
                                        <div class="col-md-6">: <span id="status" class="badge badge-warning" style="color:white"><strong>Menunggu Approval Admin</strong></span> </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-5"><strong>Metode Pembayaran</strong></div>
                                        <div class="col-md-6">: <strong><span id="payment"></span></strong> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <br>
                    <table class="table table-hover table-bordered additional_parts" style="width:100%">
                        <thead>
                            <th>Sparepart Name</th>
                            <th>Qty</th>
                            <th>Price</th>
                        </thead>
                        <tbody id="data_produk"></tbody>
                        <tbody>
                            <tr>
                                <td colspan="2" align="right">Ongkos Kirim</td>
                                <td id="getShippCost">Rp. </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="right">Grand Total</td>
                                <td id="getTotals">Rp. </td>
                            </tr>
                        </tbody>
                    </table><hr id="line">
                    <input type="hidden" name="id" id="getIdPayment">
                    <input type="hidden" id="amounts" name="getTotals">
                    <div id="btn_approve"></div>
                </div>
            </div>
        </div>
    </div>
    
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script type="text/javascript"
        src="https://app.sandbox.midtrans.com/snap/snap.js"
        data-client-key="{{ env('MD_CLIENT_KEY') }}"></script>
    <script>
        $(document).on('click', '#terima_pesanan', function(){
            id = $(this).attr('data-pesanan');
            console.log(id)
            Helper.loadingStart()
            $.ajax({
                url: '/admin/data-transactions/konfirmasi/pesanan-sampai/' + id,
                type: 'post',
                success: function(resp) {
                    Helper.loadingStop()
                    console.log(resp)
                    swal({
                    title: "Sukses!",
                        text: "Pesanan Sudah Diterima",
                        type: 'success'
                    }, function() {
                        window.location.href='/admin/data-transactions/show';
                    });
                },
                error:function(a,v,c){
                    Helper.loadingStop()
                    var msg = JSON.parse(a.responseText);
                    console.log(msg);
                    swal({
                        title: "Ups!",
                            text: msg.message,
                            type: 'error'
                        });
                }
            })

        });

        $(document).ready(function() {
            $('#userManagement').DataTable( {
                processing: true,
                responsive: true,
                aaSorting: [[ 3, "desc" ]]
            } );

            // $('#sort_data_order').on("change",function(){
            //     window.location.href="/admin/data-transactions/show?sort="+$(this).val()
            // })

            // $('#sort_data_order_kategori').on("change",function(){
            //     window.location.href="/admin/data-transactions/show?sort="+$(this).val()
            // })
        });

        var $modalResi = $('#update_resi').modal({
            show: false,
        });

        $(document)
            .on('click', '#btn_update_resi', function() {
                id = $(this).attr('data-id');
                $('#getIds').val(id)
                $modalResi.modal('show');
            });

            $(document).on('click', '.udate_resi', function(){
                id = $('#getIds').val();
                resi_pengiriman = $('#resi_pengiriman').val();
                dataInput = {
                    resi_pengiriman,
                };
                console.log(dataInput)
                Helper.loadingStart()
                $.ajax({
                    url: '/admin/update-resi/update-resi-pengiriman/' + id,
                    type: 'post',
                    data:dataInput,
                    success: function(resp) {
                        Helper.loadingStop()
                        console.log(resp)
                        swal({
                        title: "Sukses!",
                            text: "Resi Berhasil diubah",
                            type: 'success'
                        }, function() {
                            window.location.href='/admin/data-transactions';
                        });
                    },
                    error:function(a,v,c){
                        Helper.loadingStop()
                        var msg = JSON.parse(a.responseText);
                        console.log(msg.errors.resi_pengiriman);
                        swal({
                            title: "Ups!",
                                text: msg.errors.resi_pengiriman,
                                type: 'error'
                            });
                    }
                })

            });

        var $modal = $('#approve_payment').modal({
            show: false,
        });
        
        var ProductAdditionalObj = {
            // isi field input
            isiDataFormModal: function(id) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/admin/transactions/get-product/'+id,
                    type: 'get',
                    success: function(resp) {
                        console.log('asdasdasd')
                        $("#data_produk").html('');
                        $("#btn_approve").html('');
                        $("#getShippCost").html('');
                        $("#getTotals").html('');
                        $("#order_code").html('');
                        $("#date").html('');
                        $('#appen_image').html('');
                        $('#va').html('');
                        $('#nama_user').html('');
                        $('#email').html('');
                        $('#payment').html('');
                        var btn     = '';
                        var content     = '';
                        var srcImage    = '';
                        var virtualAccount = '';
                        var total_price = 0;
                        var shipping    = 0;
                        var url         = '/storage/image-transaction/'+resp.photo
                        // var url         = 'http://localhost:8000/storage/image-transaction/'+resp.photo
                        srcImage        = `: <a href="${url}" target="_blank" rel="noopener noreferrer" class="badge badge-primary">Bukti Transfer</a>`;
                        virtualAccount  = resp.payment_method == 0 ? '<b><label id="cekVA">Cek VA</label></b>' : resp.va
                        user            = resp.get_user.first_name
                        email           = resp.get_user.email
                        pembayaran      = resp.payment_method
                        btn             = '<button class="btn btn-primary terima_pembayaran" style="float: right"><i class="fa fa-check">&nbsp;</i>&nbsp; Terima Pembayaran</button>';
                        _.each(resp.order_detail, function(detail) {
                            total_price =resp.totalKeseluruhan;
                            shipping = resp.totalOngkir;
                            nama_produk = '';
                            if(detail.get_product != null){
                                nama_produk = detail.get_product.nama_produk;
                            }else{
                                nama_produk = detail.product_id;
                            }
                            content += `<tr>
                                            <td>${nama_produk}</td>
                                            <td>${detail.qty}</td>
                                            <td>Rp.${(detail.harga * detail.qty )}</td>
                                        </tr>`;
                        });

                        $('#getTotals').append(total_price)
                        $('#btn_approve').append(btn)
                        $("#getShippCost").html(shipping);
                        $("#data_produk").append(content);
                        $("#appen_image").append(srcImage);
                        $("#order_code").text(id)
                        $('#nama_user').text(user);
                        $('#email').text(email);
                        $('#payment').text(pembayaran);
                        $("#va").text(virtualAccount)
                        $("#date").text( moment(resp.created_at).format("DD MMMM YYYY"))

                        $('#cekVA').on('click', function(){
                            snap.pay(resp.va,{
                                onSuccess: function(result){
                                },
                                onPending: function(result){
                                },
                                onError: function(result){
                                },
                                onClose: function(){
                                }
                            });
                        })
                    },
                    error:function(a,v,c){
                        var msg = JSON.parse(a.responseText);
                        console.log(msg);
                        swal({
                            title: "Ups!",
                            text: msg.message,
                            type: 'error'
                        });
                    }
                });

            },
            // hadle ketika response sukses
            // success:function(data) {
            //     // send notif
            //     // Helper.successNotif(resp.msg);
            //     $modal.modal('hide');
            // },
        }

        $(document)
            .on('click', '#btn_approve_pembayaran', function() {
                console.log(id = $(this).attr('data-id'));
                $('#getIdPayment').val(id)
                ProductAdditionalObj.isiDataFormModal(id);
                $modal.modal('show');
            });

        $(document).on('click', '.terima_pembayaran', function(){
            id = $('#getIdPayment').val();
            Helper.loadingStart()
            $.ajax({
                url: '/admin/transactions/terima-pembayaran/' + id,
                type: 'post',
                success: function(resp) {
                    Helper.loadingStop()
                    console.log(resp)
                    swal({
                    title: "Sukses!",
                        text: "Pembayaran Sukses",
                        type: 'success'
                    }, function() {
                        window.location.href='/admin/transactions/show';
                    });
                },
                error:function(a,v,c){
                    Helper.loadingStop()
                    var msg = JSON.parse(a.responseText);
                    console.log(msg);
                    swal({
                        title: "Ups!",
                            text: msg.message,
                            type: 'error'
                        });
                }
            })

        });


    </script>
@endsection