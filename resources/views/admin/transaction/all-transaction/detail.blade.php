@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('success') }}</strong>
                        </div>
                    @endif
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h3 class="card-title">
                                                Order Code : <strong>{{ $transaction->orderId }}</strong>
                                            </h3>
                                        </div>
                                        <div class="col-md-6">
                                            <?php
                                                $arrStatus = ['Menunggu Pembayaran','Sukses','Gagal','Expired','Sedang Dikirim','Pesanan Selesai'];
                                            ?>
                                            <h3 class="card-title" style="float: right">Status : <strong>{{ $arrStatus[$transaction->status] }}</strong></h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            Update Status
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <form action="{{ url('admin/data-transactions/update-status/'.$transaction->orderId) }}" method="post">
                                @csrf
                                @if ($transaction->status == 1)
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                @if(count($shipment) != 0)
                                                    <table class="table table-striped table-bordered">
                                                        <thead>
                                                            <th>Nama Penerima</th>
                                                            <th>Nama Produk</th>
                                                            <th>QTY</th>
                                                            <th>No Resi</th>
                                                        </thead>
                                                        @foreach($transaction->getShipment as $r)
                                                            <tr>
                                                                <td>{{ $r->nama_lengkap }}</td>
                                                                <td>{{ $r->getProduk->nama_produk }}</td>
                                                                <td>{{ $r->qty }}</td>
                                                                <td>
                                                                    @if ($transaction->is_input_new_meber == 1)
                                                                        <input type="hidden" name="id_resi[]" value="{{ $r->id }}">
                                                                        <input type="text" name="no_resi[]" class="form-control" value="-"  placeholder="Paket Ini Tidak Ada Resi Pengiriman" disabled>
                                                                    @else
                                                                        <input type="hidden" name="id_resi[]" value="{{ $r->id }}">
                                                                        <input type="text" name="no_resi[]" class="form-control"  placeholder="Masukan Nomor Resi Pengiriman" required>
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    </table>
                                                @else 
                                                    @if ($transaction->own_transaction != 1)
                                                        <table id="ststus-orders" class="display table table-hover table-bordered" style="width:100%">
                                                            <thead>
                                                                <tr>
                                                                    <th>Nama Penerima</th>
                                                                    <th>Nomor Ponsel</th>
                                                                    <th>Resi Pengiriman</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        {{ $transaction->nama_lengkap }}
                                                                    </td>
                                                                    <td>
                                                                        {{ $transaction->nomor_ponsel }}
                                                                    </td>
                                                                    <td>
                                                                        @if ($transaction->is_input_new_meber == 1)
                                                                            <input type="text" name="resi_pengiriman" class="form-control" placeholder="Paket Ini Tidak Ada Resi Pengiriman" disabled>
                                                                        @else
                                                                            <input type="text" name="resi_pengiriman" class="form-control" placeholder="Masukan Nomor Resi Pengiriman" required>
                                                                        @endif
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    @endif
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="exampleFormControlSelect1">Status  </label>
                                                @if ($transaction->is_input_new_meber == null)
                                                    <select class="form-control" id="exampleFormControlSelect1" name="status">
                                                        <option value="">Ubah Status Pesanan</option>
                                                        <option value="0"  {{ $transaction->status == 0 ? 'selected' : '' }}>Pesanan Baru</option>
                                                        <option value="1"  {{ $transaction->status == 1 ? 'selected' : '' }}>Pesanan Diterima</option>
                                                        <option value="4"  {{ $transaction->status == 4 ? 'selected' : '' }}>Pesanan Dikirim</option>
                                                        <option value="5"  {{ $transaction->status == 5 ? 'selected' : '' }}>Pesanan Selesai</option>
                                                        <option value="2"  {{ $transaction->status == 2 ? 'selected' : '' }}>Batalkan Pesanan</option>
                                                    </select>
                                                @else
                                                    <select class="form-control" id="exampleFormControlSelect1" name="status">
                                                        <option value="">Ubah Status Pesanan</option>
                                                        {{-- <option value="0"  {{ $transaction->status == 0 ? 'selected' : '' }}>Pesanan Baru</option> --}}
                                                        <option value="1"  {{ $transaction->status == 1 ? 'selected' : '' }}>Pesanan Diterima</option>
                                                        {{-- <option value="4"  {{ $transaction->status == 4 ? 'selected' : '' }}>Pesanan Dikirim</option> --}}
                                                        <option value="5"  {{ $transaction->status == 5 ? 'selected' : '' }}>Pesanan Selesai</option>
                                                        <option value="2"  {{ $transaction->status == 2 ? 'selected' : '' }}>Batalkan Pesanan</option>
                                                    </select>
                                                @endif
                                                
                                            </div>
                                            <button type="submit" class="btn btn-primary">Ubah Status Pembayaran</button>
                                            @if ($transaction->status == 0 && $transaction->payment_method != "Midtrans")
                                                {{-- @if ($transaction->is_input_new_meber == null) --}}
                                                    <button type="button" class="btn btn-success" id="terima_pembayaran" data-id="{{ $transaction->orderId }}">Terima Pembayaran</button>
                                                {{-- @endif --}}
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <form action="{{ url('admin/data-transactions/update-multiple-resi') }}" method="post">
                                @csrf
                                <div class="card-header">
                                    <h3 class="card-title">
                                        Detail Order
                                    </h3>
                                </div>
                                <div class="card-body">
                                    <div class="col-md-12">
                                        <table id="ststus-orders" class="display table table-hover table-bordered" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>Product</th>
                                                    <th>Jumlah</th>
                                                    <th>Harga</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($orderDetail as $r)
                                                @if ($r->is_produk == 'get_produk' || $r->is_produk == 'get_produk_online')
                                                    @foreach ($r->products as $getProduk)
                                                        <tr>
                                                            <td>{{ $getProduk->nama_produk }}</td>
                                                            <td>{{ $r->qty }}</td>
                                                            <td>{{ 'Rp. '.number_format($r->harga) }}</td>
                                                        </tr>
                                                    @endforeach
                                                @else
                                                    <tr>
                                                        <td>
                                                            @if($r->is_produk == 'get_paket')
                                                                {{ $r->getpaketupgrade->nama_paket }}
                                                            @elseif($r->is_produk == 'get_paket_reguler')
                                                                {{ $r->getpaketreguler->nama_paket }}
                                                            @else 
                                                                -
                                                            @endif
                                                            {{-- {{ !empty($r->getProduct) ? $r->getProduct->nama_produk : $r->product_id }} --}}
                                                        </td>
                                                        <td>{{ $r->qty }}</td>
                                                        <td>{{ 'Rp. '.number_format($r->harga) }}</td>
                                                    </tr>
                                                @endif
                                                    
                                                @endforeach
                                            </tbody>
                                            <tbody>
                                                <tr>
                                                    <th colspan="2">Ongkir</th>
                                                    <th>{{ $transaction->totalOngkir > 0 ? 'Rp. '.number_format($transaction->totalOngkir) : "-" }}</th>
                                                </tr>
                                                <tr>
                                                    <th colspan="2">Total Pembayaran</th>
                                                    <th>{{ $transaction->totalKeseluruhan > 0 ? 'Rp. '.number_format($transaction->totalKeseluruhan) : "-" }}</th>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="card-header">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-11">Detail Pengiriman</div>
                                                <div class="col-md-1">
                                                    <!-- Large modal -->
                                                    @if(count($shipment) != 0)
                                                    @else 
                                                        @if ($transaction->status == 1 || $transaction->status == 0)
                                                            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target=".bd-example-modal-lg-lg"><i class="fa fa-edit"></i></button>
                                                        @endif
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            Order Code
                                                        </div>
                                                        <div class="col-md-8">
                                                            : <strong>{{ $transaction->orderId }}</strong>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="row">
                                                        <div class="col-md-5">
                                                            Tanggal Transaksi
                                                        </div>
                                                        <div class="col-md-7">
                                                            : <strong>{{date('d M Y H:i', strtotime($transaction->created_at))}}</strong>
                                                        </div>
                                                    </div>
                                                </div>
                                                @if ($transaction->payment_method != 'Midtrans')
                                                    <div class="col-md-4">
                                                        <div class="row">
                                                            <div class="col-md-5">
                                                                Bukti Pembayaran
                                                            </div>
                                                            <div class="col-md-7">
                                                                : 
                                                                @if (!empty($transaction->photo))
                                                                    <a href="{{ url('/storage/image-transaction/'.$transaction->photo) }}" target="_blank" rel="noopener noreferrer" class="badge badge-success">Lihat Bukti Pembayaran</a>
                                                                @else
                                                                    <span class="badge badge-warning" style="color: white">Pembayaran Belum di upload</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                @else
                                                    <div class="col-md-4">
                                                        <div class="row">
                                                            <div class="col-md-5">
                                                                Bukti Pembayaran
                                                            </div>
                                                            <div class="col-md-7">
                                                                : <span class="badge badge-success" style="color: white">Pembayaran Sukses</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div><hr>
                                    @if(count($shipment) != 0)
                                        <style>
                                            table {
                                                font-size: 1em;
                                            }
    
                                            .ui-draggable, .ui-droppable {
                                                background-position: top;
                                            }
                                        </style>
                                        <div id="accordion">
                                            @foreach($transaction->getShipment as $index => $r)
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <div class="col-md-11"><h5> Alamat {{ $no++ }}</h5></div>
                                                            <div class="col-md-1">
                                                                @if ($transaction->status == 1 || $transaction->status == 0)
                                                                    <input type="hidden" name="id" value="{{$r->id}}" id="getIdPayment">
                                                                    <a type="button" data-id ="{{$r->id}}" id="btn_update_address" data-toggle="tooltip" data-html="true" title="Edit Alamat Pengiriman" style="float: right;"><i class="fa fa-edit"></i></a>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                
                                                <div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="row">
                                                                <table class="table table-striped table-bordered">
                                                                    <tr>
                                                                        <td>Nama Produk</td>
                                                                        <td>QTY</td>
                                                                        <td>Berat</td>
                                                                    </tr>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td>{{ $r->getProduk->nama_produk }}</td>
                                                                            <td>{{ $r->qty }}</td>
                                                                            <td>{{ $r->getProduk->berat }} (gram)</td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div><hr>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <table>
                                                                        <tr>
                                                                            <td>Nama Pengirim</td>
                                                                            <td> : {{ $r->nama_pengirim }}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Nomor Pengirim</td>
                                                                            <td> : {{ !empty($transaction->get_user) ? $transaction->get_user->whatsapp_no : '-' }}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Nama penerima</td>
                                                                            <td> : {{ $r->nama_lengkap }}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Nomor Penerima</td>
                                                                            <td> : {{ $r->nomor_ponsel }}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Tanggal Order</td>
                                                                            <td> : {{ date('d M Y H:i', strtotime($transaction->created_at)) }}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width:26%">Alamat</td>
                                                                            <td> :
                                                                                {{$r->alamat != null ? $r->alamat : '-' }}, 
                                                                                {{$r->province_id != null ? $r->province_id : '-' }},  
                                                                                {{$r->kota_id != null ? $r->kota_id : '-' }}  
                                                                                {{$r->kode_pos != null ? $r->kode_pos : '-' }},
                                                                                {{$r->district_id != null ? $r->district_id : '-' }}
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <table>
                                                                        <tr>
                                                                            <td>Status Pembayaran</td>
                                                                            <?php
                                                                                $arrStatus = ['Menunggu Pembayaran','Sukses','Gagal','Expired','Sedang Dikirim','Pesanan Selesai'];
                                                                            ?>
                                                                            <td> : {{ $arrStatus[$transaction->status] }}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Kurir</td>
                                                                            <td>  :  {{ !empty($r->kurir) ? strtoupper($r->kurir) : "-" }}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Subtotal Pengiriman</td>
                                                                            <td> :  Rp.  {{ number_format($r->totalOngkir) }}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Nomor Resi</td>
                                                                            <td> :  {{ !empty($r->no_resi) ? $r->no_resi : "Resi Pengiriman Belum di Update" }}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Metode Pembayaran</td>
                                                                            @if($transaction->payment_method != "Midtrans")
                                                                                @if (!empty($transaction->getBank))
                                                                                     <td> :  {{ $transaction->payment_method }} - {{ $transaction->getBank->nomor_rekening.' A/N '.$transaction->getBank->nama_rekening  }} </td>
                                                                                @else
                                                                                     <td> : -</td>
                                                                                @endif
                                                                            @elseif($transaction->payment_method == "Midtrans")
                                                                                <td> : Midtrans @if( in_array($transaction->payment_type,['bank_transfer','other_va'])) - <label id="cekVA">Cek VA</label> @endif</td>
                                                                            @endif
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>

                                    @else
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            {{-- @if($transaction->own_transaction == 0 && $transaction->nama_pengirim != null) --}}
                                                                <div class="row">
                                                                    <div class="col-md-3">Nama Pengirim</div>
                                                                    <div class="col-md-9">: &nbsp; {{$transaction->nama_pengirim != null ? $transaction->nama_pengirim : '-' }}</div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-3">Nomor Pengirim</div>
                                                                    <div class="col-md-9">: &nbsp; {{ !empty($transaction->get_user) ? $transaction->get_user->whatsapp_no : '-' }}</div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-3">Nama Penerima</div>
                                                                    <div class="col-md-9">: &nbsp; {{$transaction->nama_lengkap != null ? $transaction->nama_lengkap : '-' }}</div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-3">Nomor Penerima</div>
                                                                    <div class="col-md-9">: &nbsp; {{$transaction->nomor_ponsel != null ? $transaction->nomor_ponsel : '-' }}</div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-3">Kurir</div>
                                                                    <div class="col-md-9">: &nbsp; {{ strtoupper($transaction->kurir != null ? $transaction->kurir : '-' ) }}</div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-3">Nomor Resi</div>
                                                                    <div class="col-md-9">: &nbsp; {{$transaction->resi_pengiriman != null ? $transaction->resi_pengiriman : '-' }}</div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-3">Alamat</div>
                                                                    <div class="col-md-9">: &nbsp; 
                                                                        {{$transaction->alamat != null ? $transaction->alamat : '-' }}, 
                                                                        {{$transaction->province_id != null ? $transaction->province_id : '-' }},  
                                                                        {{$transaction->kota_id != null ? $transaction->kota_id : '-' }}  
                                                                        {{$transaction->kode_pos != null ? $transaction->kode_pos : '-' }},
                                                                        {{$transaction->district_id != null ? $transaction->district_id : '-' }}
                                                                    </div>
                                                                </div>
                                                            {{-- @endif --}}
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-md-4">Status Pembayaran</div>
                                                                <?php
                                                                    $arrStatus = ['Menunggu Pembayaran','Sukses','Gagal','Expired','Sedang Dikirim','Pesanan Selesai'];
                                                                ?>
                                                                <div class="col-md-8">
                                                                    @if (!empty($transaction->status))
                                                                         : &nbsp; {{ $arrStatus[$transaction->status] }}
                                                                    @else
                                                                         : &nbsp; Menunggu Pembayaran
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-4">Pembayaran Berakhir Pada</div>
                                                                <div class="col-md-8">
                                                                    : &nbsp; {{ date('d M Y, H:i', strtotime($transaction->expired_date)) }}
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-4">Metode Pembayaran</div>
                                                                <div class="col-md-8">
                                                                    @if (!empty($transaction->getBank))
                                                                        : &nbsp; {{ strtoupper($transaction->payment_method != "Midtrans" ? $transaction->getBank->nama_bank : "Midtrans") }}
                                                                    @else
                                                                        : &nbsp; Midtrans  {{ $transaction->payment_type }}
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-4">Sub Total Pesanan</div>
                                                                <div class="col-md-8">: &nbsp; Rp. {{ $transaction->totalKeseluruhan > 0 ? number_format($transaction->totalKeseluruhan) : "-" }} </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-4">Sub Total Pengiriman</div>
                                                                <div class="col-md-8">: &nbsp; Rp. {{ $transaction->totalOngkir > 0 ? number_format($transaction->totalOngkir) : "-" }} </div>
                                                            </div>
                                                            @if($transaction->payment_method != "Midtrans")
                                                                <div class="row">
                                                                    <div class="col-md-4">Nomor Rekening</div>
                                                                    <div class="col-md-8">
                                                                        @if (!empty($transaction->getBank))
                                                                            <p> : &nbsp; <b>{{ $transaction->getBank->nomor_rekening.' A/N '.$transaction->getBank->nama_rekening  }}  </b></p>
                                                                        @else
                                                                            <p> : -</p>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            @endif
                                                            <div class="row">
                                                                <div class="col-md-4">Total Berat</div>
                                                                <div class="col-md-8">: &nbsp; <b>{{ $transaction->total_berat > 0 ? number_format($transaction->total_berat)." (Gram)" : "0 Gram" }}</b></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                            </div>
                                        </div>
                                    @endif
                                </div>
                                <input type="hidden" name="order_id" value="{{$transaction->orderId}}">
                            </form>

                            
                        </div>
                    </div>
                            {{-- <div class="card-footer">
                                @if(count($shipment) != 0)
                                    @if($transaction->status == 1)
                                        <button type="submit" class="btn btn-primary btn-sm btn-block">
                                            <i class="fa fa-check"> Update Resi Pengiriman</i>
                                        </button>
                                    @endif
                                @else 
                                    @if($transaction->own_transaction == 1)
                                        @if(empty($transaction->resi_pengiriman))
                                            <button type="button" class="btn btn-primary btn-sm btn-block" data-toggle="modal" data-target="#exampleModal">
                                                <i class="fa fa-check"> Update Resi Pengiriman</i>
                                            </button>
                                        @endif
                                    @endif
                                @endif
                            </div> --}}
                        {{-- <div class="card-footer">
                            @if ($transaction->status == 0 && !empty($transaction->photo))
                                <form action="{{ url('admin/data-transactions/update-status/'.$transaction->orderId) }}" method="post">
                                    @csrf
                                    <button type="submit" class="btn btn-danger btn-sm" title="Detail Pesanan"><i class="fa fa-times"></i> &nbsp; Batalkan Pembayaran</button>
                                    <button type="button" class="btn btn-success btn-sm " id="btn_approve_pembayaran" data-code={{$transaction->orderId}}><i class="fa fa-check"></i> &nbsp; Terima Pembayaran</button>
                                </form>
                            @elseif($transaction->status == 1)
                                @if($transaction->own_transaction == 1)
                                    @if(empty($transaction->resi_pengiriman))
                                        <button type="button" class="btn btn-primary btn-sm btn-block" data-toggle="modal" data-target="#exampleModal">
                                            <i class="fa fa-check"> Update Resi Pengiriman</i>
                                        </button>
                                    @endif
                                @endif
                            @endif
                        </div> --}}
                    </div>
                </div>
                <input type="hidden" id="get_id_update" name="get_id_update" value="{{$transaction->orderId}}">
                <input type="hidden" value="{{ $transaction->orderId }}" name="get_ids">
            </div>
        </section>
    </div>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Input Resi Pengiriman</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="containter">
                    <label for="">Input Resi Pengiriman</label>
                    <input type="text" class="form-control" id="resi_pengiriman" placeholder="Nomor Resi" name="resi_pengiriman">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary udate_resi">Update Resi</button>
            </div>
            </div>
        </div>
    </div>


    <!-- modal approve pembayaran -->
    <div class="modal fade bd-example-modal-lg" id="approve_payment" data-backdrop="true" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Konfirmasi Pembayaranasdfasd</h5>
                    <input type="hidden" name="id" id="data_id">
                    <input type="hidden" name="selisih" id="data_selisih">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6"><strong>Order Code</strong></div>
                                        <div class="col-md-6">: <strong><span id="order_code"></span></strong></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6"><strong>Tanggal Pesanan</strong></div>
                                        <div class="col-md-6">: <strong><span id="date"></span></strong></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6"><strong>Bukti Transfer</strong></div>
                                        <div class="col-md-6" id="appen_image">:  </div>
                                    </div>
                                    
                                </div>
                                <div class="col-md-6" style="float:right">
                                    <div class="row">
                                        <div class="col-md-4"><strong>Pesanan Dari</strong></div>
                                        <div class="col-md-6">: <strong><span id="nama_user"></span></strong> </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4"><strong>Email</strong></div>
                                        <div class="col-md-8">: <strong><span id="email"></span></strong> </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4"><strong>Status</strong></div>
                                        <div class="col-md-6">: <span id="status" class="badge badge-warning" style="color:white"><strong>Menunggu Approval Admin</strong></span> </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-5"><strong>Metode Pembayaran</strong></div>
                                        <div class="col-md-6"> : <strong><span id="payment"></span></strong> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <br>
                    <table class="table table-hover table-bordered additional_parts" style="width:100%">
                        <thead>
                            <th>Sparepart Name</th>
                            <th>Qty</th>
                            <th>Price</th>
                        </thead>
                        <tbody id="data_produk"></tbody>
                        <tbody>
                            <tr>
                                <td colspan="2" align="right">Ongkos Kirim</td>
                                <td id="getShippCost">Rp. </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="right">Grand Total</td>
                                <td id="getTotals">Rp. </td>
                            </tr>
                        </tbody>
                    </table><hr id="line">
                    <input type="hidden" name="id" id="getIdPayment">
                    <input type="hidden" id="amounts" name="getTotals">
                    <div id="btn_approve"></div>
                </div>
            </div>
        </div>
    </div><!-- modal approve pembayaran -->

    <div class="modal fade bd-example-modal-lg" id="modal_address" data-backdrop="true" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Alamat Pengiriman</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <input type="hidden" name="ambil_id" id="ambil_id">
                            <input type="hidden" name="orderId" id="orderId">
                            <textarea name="alamat" id="get_alamat" cols="10" rows="5" class="form-control"></textarea>
                            {{-- <input  name="alamat" class="get_alamat" id=""> --}}
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary" id="save_alamat" style="float: right">Simpan Perubahan Alamat</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bd-example-modal-lg-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <form action="{{ url('/admin/transactions/transaction-address/'.$transaction->orderId) }}" method="post">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Alamat Pengiriman</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <input type="hidden" name="get_id_trans" id="get_id_trans">
                                <textarea name="alamat" id="get_alamat" cols="10" rows="5" class="form-control">{!! $transaction->alamat  !!}</textarea>
                                {{-- <input  name="alamat" class="get_alamat" id=""> --}}
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary" id="save_alamat" style="float: right">Simpan Perubahan Alamat</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <link rel="stylesheet" href="{{ asset('styles.css') }}">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    {{-- <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script> --}}
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script type="text/javascript"
        src="https://app.sandbox.midtrans.com/snap/snap.js"
        data-client-key="{{ env('MD_CLIENT_KEY') }}"></script>
    <script>
        $( function() {
            $( "#accordion" ).accordion({
            collapsible: true
            });
        } );
    </script>
    <script>
         $('#cekVA').on('click', function(){
            snap.pay('{{ $transaction->va }}',{
                onSuccess: function(result){
                },
                onPending: function(result){
                },
                onError: function(result){
                },
                onClose: function(){
                }
            });
        })

        $(document).on('click', '.udate_resi', function(){
            $('#ststus-orders').DataTable();
            id = $('#get_id_update').val();
            resi_pengiriman = $('#resi_pengiriman').val();
            dataInput = {
                resi_pengiriman,
            };
            console.log(dataInput)
            $.ajax({
                url: '/admin/update-resi/update-resi-pengiriman/' + id,
                type: 'post',
                data:dataInput,
                success: function(resp) {
                    console.log(resp)
                    swal({
                    title: "Sukses!",
                        text: "Resi Berhasil diubah",
                        type: 'success'
                    }, function() {
                        window.location.href='/admin/update-resi/show';
                    });
                },
                error:function(a,v,c){
                    var msg = JSON.parse(a.responseText);
                    console.log(msg.errors.resi_pengiriman);
                    swal({
                        title: "Ups!",
                            text: msg.errors.resi_pengiriman,
                            type: 'error'
                        });
                }
            })
        });

        $(document).on('click', '#btn_approve_pembayaran', function(){
            id = $(this).attr('data-code');
            console.log(id)
            Helper.loadingStart()
            $.ajax({
                url: '/admin/transactions/terima-pembayaran/' + id,
                type: 'post',
                success: function(resp) {
                    Helper.loadingStop()
                    console.log(resp)
                    swal({
                    title: "Sukses!",
                        text: "Pembayaran Sukses",
                        type: 'success'
                    }, function() {
                        window.location.href='/admin/transactions/show';
                    });
                },
                error:function(a,v,c){
                    Helper.loadingStop()
                    var msg = JSON.parse(a.responseText);
                    console.log(msg);
                    swal({
                        title: "Ups!",
                            text: msg.message,
                            type: 'error'
                        });
                }
            })

        });
    </script>

    {{-- update alamat --}}
    <script>

        $(document).on('click', '#terima_pembayaran', function(){
            id = $(this).attr('data-id');
            Helper.loadingStart()
            $.ajax({
                url: '/admin/transactions/terima-pembayaran/' + id,
                type: 'post',
                success: function(resp) {
                    Helper.loadingStop()
                    console.log(resp)
                    swal({
                    title: "Sukses!",
                        text: "Pembayaran Sukses",
                        type: 'success'
                    }, function() {
                        window.location.href='/admin/transactions/show';
                    });
                },
                error:function(a,v,c){
                    Helper.loadingStop()
                    var msg = JSON.parse(a.responseText);
                    console.log(msg);
                    swal({
                        title: "Ups!",
                            text: msg.message,
                            type: 'error'
                        });
                }
            })

        });

        $(document).on('click', '#save_alamat', function(){
            id = $("#ambil_id").val();
            alamat = $('#get_alamat').val();
            orderId = $("#orderId").val();
            dataInput = {
                alamat,
            };
            console.log(dataInput)
            $.ajax({
                url: '/admin/transactions/ubah-alamat/' + id,
                type: 'post',
                data:dataInput,
                success: function(resp) {
                    console.log(resp)
                    swal({
                    title: "Sukses!",
                        text: "Alamat Pengiriman Berhasil diubah",
                        type: 'success'
                    }, function() {
                        window.location.href='/admin/data-transactions/detail/'+ orderId;
                    });
                },
                error:function(a,v,c){
                    var msg = JSON.parse(a.responseText);
                    console.log(msg.errors.resi_pengiriman);
                    swal({
                        title: "Ups!",
                            text: msg.errors.resi_pengiriman,
                            type: 'error'
                        });
                }
            })
        });

        var $modal = $('#modal_address').modal({
            show: false,
        });

        $(function () {
            // Summernote
            $('#summernote').summernote()
        })

        var ProductAdditionalObj = {
            // isi field input
            isiDataFormModal: function(id) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/admin/transactions/get-address/'+id,
                    type: 'get',
                    success: function(resp) {
                        console.log(resp.alamat)
                        $("#get_alamat").html('');
                        $("#ambil_id").html('');
                        $("#orderId").html('');
                        var getAlamat = resp.alamat
                        var id = resp.id
                        var orderId = resp.orderId
                        document.getElementById("get_alamat").value = getAlamat;
                        document.getElementById("ambil_id").value = id;
                        document.getElementById("orderId").value = orderId;
                        // $(".get_alamat").append(getAlamat);
                    },
                    error:function(a,v,c){
                        var msg = JSON.parse(a.responseText);
                        console.log(msg);
                        swal({
                            title: "Ups!",
                            text: msg.message,
                            type: 'error'
                        });
                    }
                });

            },
        }

        $(document)
            .on('click', '#btn_update_address', function() {
                console.log(id = $(this).attr('data-id'));
                $('#getIdPayment').val(id)
                ProductAdditionalObj.isiDataFormModal(id);
                $modal.modal('show');
            });

            
    </script>
@endsection