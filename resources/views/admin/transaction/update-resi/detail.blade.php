@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <h3 class="card-title">
                                Status Ordersasdasd
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="card-header">
                                <h3 class="card-title">
                                    Detail Order
                                </h3>
                            </div>
                            <div class="card-body">
                                <div class="col-md-12">
                                    <table id="ststus-orders" class="display table table-hover table-bordered" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Product</th>
                                                <th>Jumlah</th>
                                                <th>Harga</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($orderDetail as $r)
                                                <tr>
                                                    <td>{{ $r->product_id }}</td>
                                                    <td>{{ $r->qty }}</td>
                                                    <td>{{ 'Rp. '.number_format($r->harga) }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                        <tbody>
                                            <tr>
                                                <th colspan="2">Ongkir</th>
                                                <th>{{ $transaction->totalOngkir > 0 ? 'Rp. '.number_format($transaction->totalOngkir) : "-" }}</th>
                                            </tr>
                                            <tr>
                                                <th colspan="2">Total Pembayaran</th>
                                                <th>{{ $transaction->totalKeseluruhan > 0 ? 'Rp. '.number_format($transaction->totalKeseluruhan) : "-" }}</th>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="card-header">
                                <h3 class="card-title">
                                    Detail Pembayaran
                                </h3>
                            </div>
                            <div class="card-body">
                                <div class="col-md-12">
                                    <div class="row">
                                        @if($transaction->own_transaction == 0)
                                        <div class="col-md-8">
                                            <table>
                                                <tr>
                                                    <td>Pengirim</td>
                                                    <td> : &nbsp; {{$transaction->nama_pengirim != null ? $transaction->nama_pengirim : '-' }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Nomor Pengirim </td>
                                                    <td> : &nbsp; {{$transaction->nomor_ponsel != null ? $transaction->nomor_ponsel : '-' }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Alamat Pengirim &nbsp;&nbsp;</td>
                                                    <td> : &nbsp; {{$transaction->alamat != null ? $transaction->alamat : '-' }} </td>
                                                </tr>
                                                <tr>
                                                    <td>Kurir</td>
                                                    <td> : &nbsp; {{ strtoupper($transaction->kurir != null ? $transaction->kurir : '-' ) }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Resi</td>
                                                    <td> : &nbsp; {{ !empty($transaction->resi_pengiriman) ? $transaction->resi_pengiriman : '-' }}</td>
                                                </tr>
                                            </table>
                                        </div>
                                        @endif

                                        <div class="col-md-4">
                                            <table>
                                                <tr>
                                                    <td>Status Pembayaran</td>
                                                    <?php
                                                        $arrStatus = ['Pending','Berhasil','Dibatalkan','Expired','Sedang Dikirim','Pesanan Selesai'];
                                                    ?>
                                                    @if (!empty($transaction->status))
                                                        <td> : &nbsp; {{ $arrStatus[$transaction->status] }}</td>
                                                    @else
                                                        <td> : &nbsp; Menunggu Pembayaran</td>
                                                    @endif
                                                </tr>
                                                <tr>
                                                    <td>Pembayaran Berkahir Pada</td>
                                                    <td>{{ date('d M Y H:i', strtotime($transaction->expired_date)) }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Bank Transfer </td>
                                                    @if (!empty($transaction->getBank))
                                                        <td> : &nbsp; {{ strtoupper($transaction->payment_method != "Midtrans" ? $transaction->getBank->nama_bank : "Midtrans") }}</td>
                                                    @else
                                                        <td> : Midtrans - {{ $transaction->payment_type }}</td>
                                                    @endif
                                                </tr>
                                                <tr>
                                                    <td>Total Transfer &nbsp;&nbsp;</td>
                                                    <td> : &nbsp; Rp. {{ $transaction->totalKeseluruhan > 0 ? number_format($transaction->totalKeseluruhan) : "-" }}  </td>
                                                </tr>
                                                <tr>
                                                    @if($transaction->payment_method != "Midtrans")
                                                        <td>No. Rekening  &nbsp;&nbsp;</td>
                                                        @if (!empty($transaction->getBank))
                                                            <td> : &nbsp; <b>{{ $transaction->getBank->nomor_rekening.' A/N '.$transaction->getBank->nama_rekening  }}  </b></td>
                                                        @else
                                                            <td> : -</td>
                                                        @endif
                                                    {{-- @elseif($transaction->payment_method == "Midtrans" && in_array($transaction->payment_type,['bank_transfer','other_va'])) --}}
                                                    @elseif($transaction->payment_method == "Midtrans")
                                                        <td>Virtual Account  &nbsp;&nbsp;</td>
                                                        <td> : &nbsp; <b><label id="cekVA">Cek VA</label></b></td>
                                                    @endif
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="text" name="getIds" value="{{$transaction->orderId}}">
                            
                        </div>
                        <div class="card-footer">
                        <button type="button" class="btn btn-primary btn-sm btn-block" data-toggle="modal" data-target="#exampleModal">
                            <i class="fa fa-check"> Update Resi Pengiriman</i>
                        </button>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Input Resi Pengiriman</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="containter">
                    <label for="">Input Resi Pengiriman</label>
                    <input type="text" class="form-control" id="resi_pengiriman" placeholder="Nomor Resi" name="resi_pengiriman">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary udate_resi">Update Resi</button>
            </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#ststus-orders').DataTable();
        } );

        $(document).on('click', '.udate_resi', function(){
            id = $('#get_id_update').val();
            resi_pengiriman = $('#resi_pengiriman').val();
            dataInput = {
                resi_pengiriman,
            };
            console.log(dataInput)
            Helper.loadingStart()
            $.ajax({
                url: '/admin/update-resi/update-resi-pengiriman/' + id,
                type: 'post',
                data:dataInput,
                success: function(resp) {
                    Helper.loadingStop()
                    console.log(resp)
                    // swal({
                    // title: "Sukses!",
                    //     text: "Resi Berhasil diubah",
                    //     type: 'success'
                    // }, function() {
                    //     window.location.href='/admin/update-resi/show';
                    // });
                },
                error:function(a,v,c){
                    Helper.loadingStop()
                    var msg = JSON.parse(a.responseText);
                    console.log(msg.errors.resi_pengiriman);
                    swal({
                        title: "Ups!",
                            text: msg.errors.resi_pengiriman,
                            type: 'error'
                        });
                }
            })

        });
    </script>
@endsection