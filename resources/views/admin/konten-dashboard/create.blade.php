@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3 class="card-title">
                                        Konten Dashboard
                                    </h3>
                                </div>
                                <div class="col-md-2">
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <form action="{{url('admin/konten-dashboard/store')}}" method="post">
                            @csrf
                            <div class="card-body">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Url Group Telegram</label>
                                                <input type="text" name="link_telegram" value={{ Request::old('link_telegram') }} class="form-control" placeholder="Url Group Telegram ...">
                                                @error('link_telegram')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Url Group Content</label>
                                                <input type="text" name="link_group_content" value={{ Request::old('link_group_content') }} class="form-control" placeholder="Url Group Content ...">
                                                @error('link_group_content')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Inatagram</label>
                                                <input type="text" name="nama_instagram" value={{ Request::old('nama_instagram') }} class="form-control" placeholder="Instagram ...">
                                                @error('nama_instagram')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Facebookt</label>
                                                <input type="text" name="nama_facebook" class="form-control" placeholder="Facebook ...">
                                                @error('nama_facebook')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="col-sm-12">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Url Vidio Tutorial Dashboard</label>
                                                <input type="text" name="url_vidio_tutorial_dashboard" class="form-control" placeholder="Url Vidio Tutorial Dashboard ...">
                                                @error('url_vidio_tutorial_dashboard')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <label>Url Group Content</label>
                                                <input type="text" name="url_vidio_generasi_online" class="form-control" placeholder="Url Group Content ...">
                                                @error('url_vidio_generasi_online')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary" style="float: right">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
    
@endsection
