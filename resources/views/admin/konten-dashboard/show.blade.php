@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('success') }}</strong>
                        </div>
                    @endif
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3 class="card-title">
                                        Konten Dashboard
                                    </h3>
                                </div>
                                <div class="col-md-2">
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <form action="{{url('admin/konten-dashboard/update/'.$data->id)}}" method="post">
                            @csrf
                            <div class="card-body">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <input type="hidden" name="id" value="{{$data->id}}">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Url Group Telegram</label>
                                                <input type="text" name="link_telegram" value="{{$data->link_telegram}}" class="form-control" placeholder="Url Group Telegram ...">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Url Group Content</label>
                                                <input type="text" name="link_group_content" value="{{$data->link_group_content}}" class="form-control" placeholder="Url Group Content ...">
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Inatagram</label>
                                                <input type="text" name="nama_instagram" value="{{$data->nama_instagram}}" class="form-control" placeholder="Inatagram ...">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Facebookt</label>
                                                <input type="text" name="nama_facebook" value="{{$data->nama_facebook}}" class="form-control" placeholder="Facebookt ...">
                                            </div>
                                        </div>

                                        <div class="col-sm-12">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Url Vidio Tutorial Dashboard</label>
                                                <input type="text" name="url_vidio_tutorial_dashboard" value="{{$data->url_vidio_tutorial_dashboard}}" class="form-control" placeholder="Url Vidio Tutorial Dashboard ...">
                                            </div>
                                            <div class="form-group">
                                                <label>Url Group Content</label>
                                                <input type="text" name="url_vidio_generasi_online" value="{{$data->url_vidio_generasi_online}}" class="form-control" placeholder="Url Group Content ...">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary" style="float: right">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
    
@endsection
