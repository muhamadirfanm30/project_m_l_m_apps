@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('success') }}</strong>
                        </div>
                    @elseif(session()->has('failed'))
                        <div class="alert alert-danger alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('failed') }}</strong>
                        </div>
                    @endif
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-8">
                                    <h3 class="card-title">
                                        Landing Page 1 Update 
                                    </h3>
                                </div>
                                <div class="col-md-4">
                                    <form action="{{ url('/admin/landing-page/page_1/delete-image/'.$page_1->id) }}" method="post"  enctype="multipart/form-data" id="deleteData-{{$page_1->id}}">
                                        @csrf
                                    </form>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <a href="{{ url('/admin/landing-page/show') }}" class="btn btn-primary btn-sm btn-block" style="float: right">Kembali</a>
                                        </div>
                                        <div class="col-md-6">
                                            <button type="submit" class="btn btn-danger btn-sm btn-block" style="float: right" onclick="deleteData({{ $page_1->id }})">Hapus Gambar</button>
                                            {{-- <input type="hidden" value="{{ $page_1->image_1 }}" name="image_1" class="custom-file-input" id="customFile">
                                            <input type="hidden" value="{{ $page_1->image_2 }}" name="image_2" class="custom-file-input" id="customFile">
                                            <input type="hidden" value="{{ $page_1->image_3 }}" name="image_3" class="custom-file-input" id="customFile">
                                            <input type="hidden" value="{{ $page_1->image_4 }}" name="image_4" class="custom-file-input" id="customFile">
                                            <input type="hidden" value="{{ $page_1->image_5 }}" name="image_5" class="custom-file-input" id="customFile">
                                            <input type="hidden" value="{{ $page_1->image_6 }}" name="image_6" class="custom-file-input" id="customFile">
                                            <input type="hidden" value="{{ $page_1->image_7 }}" name="image_7" class="custom-file-input" id="customFile">
                                            <input type="hidden" value="{{ $page_1->image_8 }}" name="image_8" class="custom-file-input" id="customFile"> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="col-md-12">
                                <form action="{{url('/admin/landing-page/page_1/update/'.$page_1->id)}}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Judul Template</label>
                                            <input type="text" name="is_judul_landing_page" class="form-control" placeholder="Judul Template ..." value="{{ $page_1->is_judul_landing_page }}">
                                            @error('is_judul_landing_page')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="">Image 1</label>
                                            <div class="custom-file">
                                                <input type="file" name="image_1" class="custom-file-input" id="customFile">
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if (!empty($page_1->image_1))
                                                    <a href="{{ url('storage/landing-page-image-1/'.$page_1->image_1) }}" target="_blank" rel="noopener noreferrer" class="badge badge-primary"><i class="fa fa-image"></i>  {{$page_1->image_1}}</a>    
                                                @else
                                                    <span class="badge badge-warning" style="color: white"><i class="fa fa-image"></i>  Gambar Tidak Ada</span>
                                                @endif
                                                
                                                 @error('image_1')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Judul Vidio</label>
                                            <input type="text" name="judul_vidio_embed" value="{{ $page_1->judul_vidio_embed }}" class="form-control" placeholder="Judul Vidio ...">
                                             @error('judul_vidio_embed')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                
                                        <div class="form-group">
                                            <label>Embed Link Vidio</label>
                                            <input type="text" name="embed_link_vidio" value="{{ $page_1->embed_link_vidio }}" class="form-control" placeholder="Embed Link Vidio ...">
                                             @error('embed_link_vidio')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                
                                        <div class="form-group">
                                            <label>Judul Deskripsi</label>
                                            <input type="text" name="judul_deskripsi" value="{{ $page_1->judul_deskripsi }}" class="form-control" placeholder="Judul Deskripsi ...">
                                             @error('judul_deskripsi')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                
                                        <div class="form-group">
                                            <label>Deskripsi</label>
                                            <textarea class="summernote" name="deskripsi" row="5">{!! $page_1->deskripsi !!}</textarea>
                                             @error('deskripsi')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="">Image 2</label>
                                            <div class="custom-file">
                                                <input type="file" name="image_2" class="custom-file-input" id="customFile">
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if (!empty($page_1->image_2))
                                                    <a href="{{ url('storage/landing-page-image-2/'.$page_1->image_2) }}" target="_blank" rel="noopener noreferrer" class="badge badge-primary"><i class="fa fa-image"></i>  {{$page_1->image_2}}</a>    
                                                @else
                                                    <span class="badge badge-warning" style="color: white"><i class="fa fa-image"></i>  Gambar Tidak Ada</span>
                                                @endif
                                                
                                                 @error('image_2')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Nama Button 1</label>
                                            <input type="text" name="nama_button_1" value="{{ $page_1->nama_button_1 }}" class="form-control" placeholder="Nama Button 1 ...">
                                             @error('nama_button_1')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="">Image 3</label>
                                            <div class="custom-file">
                                                <input type="file" name="image_3" class="custom-file-input" id="customFile">
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if (!empty($page_1->image_3))
                                                    <a href="{{ url('storage/landing-page-image-3/'.$page_1->image_3) }}" target="_blank" rel="noopener noreferrer" class="badge badge-primary"><i class="fa fa-image"></i>  {{$page_1->image_3}}</a>    
                                                @else
                                                    <span class="badge badge-warning" style="color: white"><i class="fa fa-image"></i>  Gambar Tidak Ada</span>
                                                @endif
                                                
                                                 @error('image_3')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Image 4</label>
                                            <div class="custom-file">
                                                <input type="file" name="image_4" class="custom-file-input" id="customFile">
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if (!empty($page_1->image_4))
                                                    <a href="{{ url('storage/landing-page-image-4/'.$page_1->image_4) }}" target="_blank" rel="noopener noreferrer" class="badge badge-primary"><i class="fa fa-image"></i>  {{$page_1->image_4}}</a>    
                                                @else
                                                    <span class="badge badge-warning" style="color: white"><i class="fa fa-image"></i>  Gambar Tidak Ada</span>
                                                @endif
                                                
                                                 @error('image_4')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Image 5</label>
                                            <div class="custom-file">
                                                <input type="file" name="image_5" class="custom-file-input" id="customFile">
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if (!empty($page_1->image_5))
                                                    <a href="{{ url('storage/landing-page-image-5/'.$page_1->image_5) }}" target="_blank" rel="noopener noreferrer" class="badge badge-primary"><i class="fa fa-image"></i>  {{$page_1->image_5}}</a>    
                                                @else
                                                    <span class="badge badge-warning" style="color: white"><i class="fa fa-image"></i>  Gambar Tidak Ada</span>
                                                @endif
                                                
                                                 @error('image_5')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Nama Button 2</label>
                                            <input type="text" name="nama_button_2" value="{{ $page_1->nama_button_2 }}" class="form-control" placeholder="Nama Button 2 ...">
                                             @error('nama_button_2')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label>judul 1</label>
                                            <input type="text" name="judul_1" class="form-control" placeholder="judul 1 ..." value="{{ $page_1->judul_1 }}">
                                            @error('judul_1')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label>judul 2</label>
                                            <input type="text" name="judul_2" class="form-control" placeholder="judul 2 ..." value="{{ $page_1->judul_2 }}">
                                            @error('judul_2')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label>Nama User 1</label>
                                            <input type="text" name="nama_user_1" value="{{ $page_1->nama_user_1 }}" class="form-control" placeholder="Nama User 1 ...">
                                             @error('nama_user_1')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label>Url Vidio 1</label>
                                            <input type="text" name="url_vidio_1" value="{{ $page_1->url_vidio_user_1 }}" class="form-control" placeholder="Url Vidio 1 ...">
                                             @error('url_vidio_1')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label>Deskripsi user 1</label>
                                            <textarea class="summernote"  name="deskripsi_user_1" row="5">{!! $page_1->deskripsi_user_1 !!}</textarea>
                                             @error('deskripsi_user_1')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label>Nama User 2</label>
                                            <input type="text" name="nama_user_2" value="{{ $page_1->nama_user_2 }}" class="form-control" placeholder="Nama User 2 ...">
                                             @error('nama_user_2')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label>Url Vidio 2</label>
                                            <input type="text" name="url_vidio_2" value="{{ $page_1->url_vidio_user_2 }}" class="form-control" placeholder="Url Vidio 2 ...">
                                             @error('url_vidio_2')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label>Deskripsi user 2</label>
                                            <textarea class="summernote" name="deskripsi_user_2" row="5">{!! $page_1->deskripsi_user_2 !!}</textarea>
                                             @error('deskripsi_user_2')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label>Nama User 3</label>
                                            <input type="text" name="nama_user_3" value="{{ $page_1->nama_user_3 }}" class="form-control" placeholder="Nama User 3 ...">
                                             @error('nama_user_3')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label>Url Vidio 3</label>
                                            <input type="text" name="url_vidio_3" value="{{ $page_1->url_vidio_user_3 }}" class="form-control" placeholder="Url Vidio 3 ...">
                                             @error('url_vidio_3')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label>Deskripsi user 3</label>
                                            <textarea class="summernote" name="deskripsi_user_3" row="5">{!! $page_1->deskripsi_user_3 !!}</textarea>
                                             @error('deskripsi_user_3')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label>Nama Button 3</label>
                                            <input type="text" name="nama_button_3" value="{{ $page_1->nama_button_3 }}" class="form-control" placeholder="Nama Button 3 ...">
                                             @error('nama_button_3')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="">Image 6</label>
                                            <div class="custom-file">
                                                <input type="file" name="image_6" class="custom-file-input" id="customFile">
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if (!empty($page_1->image_6))
                                                    <a href="{{ url('storage/landing-page-image-6/'.$page_1->image_6) }}" target="_blank" rel="noopener noreferrer" class="badge badge-primary"><i class="fa fa-image"></i>  {{$page_1->image_6}}</a>    
                                                @else
                                                    <span class="badge badge-warning" style="color: white"><i class="fa fa-image"></i>  Gambar Tidak Ada</span>
                                                @endif
                                                
                                                 @error('image_6')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Image 7</label>
                                            <div class="custom-file">
                                                <input type="file" name="image_7" class="custom-file-input" id="customFile">
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if (!empty($page_1->image_7))
                                                    <a href="{{ url('storage/landing-page-image-7/'.$page_1->image_7) }}" target="_blank" rel="noopener noreferrer" class="badge badge-primary"><i class="fa fa-image"></i>  {{$page_1->image_7}}</a>    
                                                @else
                                                    <span class="badge badge-warning" style="color: white"><i class="fa fa-image"></i>  Gambar Tidak Ada</span>
                                                @endif
                                                
                                                 @error('image_7')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Image 8</label>
                                            <div class="custom-file">
                                                <input type="file" name="image_8" class="custom-file-input" id="customFile">
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if (!empty($page_1->image_8))
                                                    <a href="{{ url('storage/landing-page-image-8/'.$page_1->image_8) }}" target="_blank" rel="noopener noreferrer" class="badge badge-primary"><i class="fa fa-image"></i>  {{$page_1->image_8}}</a>    
                                                @else
                                                    <span class="badge badge-warning" style="color: white"><i class="fa fa-image"></i>  Gambar Tidak Ada</span>
                                                @endif
                                                
                                                 @error('image_8')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Nama Button 4</label>
                                            <input type="text" name="nama_button_4" value="{{ $page_1->nama_button_4 }}" class="form-control" placeholder="Nama Button 4 ...">
                                             @error('nama_button_4')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label>judul 3</label>
                                            <input type="text" name="judul_3" class="form-control" placeholder="judul 3 ..." value="{{ $page_1->judul_3 }}">
                                            @error('judul_3')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label>Acordion 1</label>
                                            <input type="text" name="judul_accordion_1" value="{{ $page_1->judul_accordion_1 }}" class="form-control" placeholder="Acordion 1 ...">
                                             @error('judul_accordion_1')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label>Deskripsi 2</label>
                                            <textarea class="summernote" name="deskripsi_accordion_1"  row="5">{!! $page_1->deskripsi_accordion_1 !!}</textarea>
                                             @error('deskripsi_accordion_1')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label>Acordion 2</label>
                                            <input type="text" name="judul_accordion_2" value="{{ $page_1->judul_accordion_2 }}" class="form-control" placeholder="Acordion 2 ...">
                                             @error('judul_accordion_2')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label>Deskripsi 2</label>
                                            <textarea class="summernote" name="deskripsi_accordion_2"  row="5">{!! $page_1->deskripsi_accordion_2 !!}</textarea>
                                             @error('deskripsi_accordion_2')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label>Acordion 3</label>
                                            <input type="text" name="judul_accordion_3" value="{{ $page_1->judul_accordion_3 }}" class="form-control" placeholder="Acordion 3 ...">
                                             @error('judul_accordion_3')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label>Deskripsi 3</label>
                                            <textarea class="summernote" name="deskripsi_accordion_3"  row="5">{!! $page_1->deskripsi_accordion_3 !!}</textarea>
                                             @error('deskripsi_accordion_3')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label>Acordion 4</label>
                                            <input type="text" name="judul_accordion_4" value="{{ $page_1->judul_accordion_4 }}" class="form-control" placeholder="Acordion 4 ...">
                                             @error('judul_accordion_4')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label>Deskripsi 4</label>
                                            <textarea class="summernote" name="deskripsi_accordion_4" row="5">{!! $page_1->deskripsi_accordion_4 !!}</textarea>
                                             @error('deskripsi_accordion_4')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-sm">Simpan Perubahan Template</button>
                                </form>
                            </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>
        $(function () {
            $('.summernote').summernote()
            $('.desc_prof1').summernote()
        })

        $(function () {
            bsCustomFileInput.init();
        });

        function deleteData(id){
            swal({
                title: "Alert!",
                text: "Apakah anda yakin akan menghapus Semua Gambar tersebut ?",
                type: 'error',
                showCancelButton: true,
                confirmButtonText: 'Ya, Saya yakin',
                cancelButtonText: 'Tidak',
            },function(result) {
                if(result){
                    document.getElementById('deleteData-'+id).submit();
                }
            });
        }
    </script>
@endsection



