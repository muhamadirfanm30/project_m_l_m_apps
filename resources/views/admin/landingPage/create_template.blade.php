@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('success') }}</strong>
                        </div>
                    @elseif(session()->has('failed'))
                        <div class="alert alert-danger alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('failed') }}</strong>
                        </div>
                    @endif
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3 class="card-title">
                                        Pilih Template
                                    </h3>
                                </div>
                                <div class="col-md-2">
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <form action="{{ url('/admin/landing-page/store') }}" method="post">
                            @csrf
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="">Template</label>
                                    <select class="form-control" name="is_landing_page">
                                        <option value="">Pilih Template</option>
                                        <option value="1">Template 1</option>
                                        <option value="2">Template 2</option>
                                        <option value="3">Template 3</option>
                                        <option value="4">Template 4</option>
                                        <option value="5">Template 5</option>
                                        <option value="6">Template 6</option>
                                    </select>
                                    @error('is_landing_page')
                                        <span style="color:red"><small>{{ $message }}</small></span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="">Judul</label>
                                    <input type="text" name="is_judul_landing_page" class="form-control" placeholder="Judul Template">
                                    @error('is_judul_landing_page')
                                        <span style="color:red"><small>{{ $message }}</small></span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success btn-block">Simpan</button>
                                </div>
                            </div>
                        </form>
                        <div class="card-footer">
                        </div>
                    </div>


                    {{-- <div id="form_1_hide" style="display: none">
                        <div class="card card-outline card-info">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-md-10">
                                        <h3 class="card-title">
                                            Template 1
                                        </h3>
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>
                            </div>
                            <div class="card-body">
                                @include('admin.landingPage.page1')
                            </div>
                            <div class="card-footer">
                            </div>
                        </div>
                    </div>

                    <div id="form_2_hide" style="display: none">
                        <div class="card card-outline card-info">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-md-10">
                                        <h3 class="card-title">
                                            Template 2
                                        </h3>
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>
                            </div>
                            <div class="card-body">
                                @include('admin.landingPage.page2')
                            </div>
                            <div class="card-footer">
                            </div>
                        </div>
                    </div>

                    <div id="form_3_hide" style="display: none">
                        <div class="card card-outline card-info">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-md-10">
                                        <h3 class="card-title">
                                            Template 3
                                        </h3>
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>
                            </div>
                            <div class="card-body">
                                @include('admin.landingPage.page3')
                            </div>
                            <div class="card-footer">
                            </div>
                        </div>
                    </div>

                    <div id="form_4_hide" style="display: none">
                        <div class="card card-outline card-info">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-md-10">
                                        <h3 class="card-title">
                                            Template 4
                                        </h3>
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>
                            </div>
                            <div class="card-body">
                                @include('admin.landingPage.page4')
                            </div>
                            <div class="card-footer">
                            </div>
                        </div>
                    </div>

                    <div id="form_5_hide" style="display: none">
                        <div class="card card-outline card-info">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-md-10">
                                        <h3 class="card-title">
                                            Template 5
                                        </h3>
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>
                            </div>
                            <div class="card-body">
                                @include('admin.landingPage.page5')
                            </div>
                            <div class="card-footer">
                            </div>
                        </div>
                    </div>

                    <div id="form_6_hide" style="display: none">
                        <div class="card card-outline card-info">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-md-10">
                                        <h3 class="card-title">
                                            Template 6
                                        </h3>
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>
                            </div>
                            <div class="card-body">
                                @include('admin.landingPage.page6')
                            </div>
                            <div class="card-footer">
                            </div>
                        </div>
                    </div> --}}
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>
        $(function () {
            $('.summernote').summernote()
            $('.desc_prof1').summernote()
        })

        $(function () {
            bsCustomFileInput.init();
        });
    </script>

{{-- <script>  
    $(document).ready(function(){
        $("#form_1_hide").hide();
        $("#form_2_hide").hide();
        $("#form_3_hide").hide();
        $("#form_4_hide").hide();
        $("#form_5_hide").hide();
        $("#form_6_hide").hide();

        $('#pilih_template').on('change', function() {
          if ( this.value == 'template_1'){
            $("#form_1_hide").show();
            $("#form_2_hide").hide();
            $("#form_3_hide").hide();
            $("#form_4_hide").hide();
            $("#form_5_hide").hide();
            $("#form_6_hide").hide();
          }else if ( this.value == 'template_2'){
            $("#form_1_hide").hide();
            $("#form_2_hide").show();
            $("#form_3_hide").hide();
            $("#form_4_hide").hide();
            $("#form_5_hide").hide();
            $("#form_6_hide").hide();
          }else if ( this.value == 'template_3'){
            $("#form_1_hide").hide();
            $("#form_2_hide").hide();
            $("#form_3_hide").show();
            $("#form_4_hide").hide();
            $("#form_5_hide").hide();
            $("#form_6_hide").hide();
          }else if ( this.value == 'template_4'){
            $("#form_1_hide").hide();
            $("#form_2_hide").hide();
            $("#form_3_hide").hide();
            $("#form_4_hide").show();
            $("#form_5_hide").hide();
            $("#form_6_hide").hide();
          }else if ( this.value == 'template_5'){
            $("#form_1_hide").hide();
            $("#form_2_hide").hide();
            $("#form_3_hide").hide();
            $("#form_4_hide").hide();
            $("#form_5_hide").show();
            $("#form_6_hide").hide();
          }else if ( this.value == 'template_6'){
            $("#form_1_hide").hide();
            $("#form_2_hide").hide();
            $("#form_3_hide").hide();
            $("#form_4_hide").hide();
            $("#form_5_hide").hide();
            $("#form_6_hide").show();
          }else if ( this.value == ''){
            $("#form_1_hide").hide();
            $("#form_2_hide").hide();
            $("#form_3_hide").hide();
            $("#form_4_hide").hide();
            $("#form_5_hide").hide();
            $("#form_6_hide").hide();
          }else{
            $("#form_1_hide").show();
            $("#form_2_hide").show();
            $("#form_3_hide").show();
            $("#form_4_hide").show();
            $("#form_5_hide").show();
            $("#form_6_hide").show();
          }
        });
    });
</script> --}}
@endsection