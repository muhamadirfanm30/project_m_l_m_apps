@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('success') }}</strong>
                        </div>
                    @elseif(session()->has('failed'))
                        <div class="alert alert-danger alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('failed') }}</strong>
                        </div>
                    @endif
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-8">
                                    <h3 class="card-title">
                                        Landing Page 2 Update 
                                    </h3>
                                </div>
                                <div class="col-md-4">
                                    <form action="{{ url('/admin/landing-page/page_2/delete-image/'.$page_2->id) }}" method="post"  enctype="multipart/form-data" id="deleteData-{{$page_2->id}}">
                                        @csrf
                                    </form>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <a href="{{ url('/admin/landing-page/show') }}" class="btn btn-primary btn-sm btn-block" style="float: right">Kembali</a>
                                        </div>
                                        <div class="col-md-6">
                                            <button type="submit" class="btn btn-danger btn-sm btn-block" style="float: right" onclick="deleteData({{ $page_2->id }})">Hapus Gambar</button>
                                            {{-- <input type="hidden" value="{{ $page_2->image_1 }}" name="image_1" class="custom-file-input" id="customFile">
                                            <input type="hidden" value="{{ $page_2->image_2 }}" name="image_2" class="custom-file-input" id="customFile">
                                            <input type="hidden" value="{{ $page_2->image_3 }}" name="image_3" class="custom-file-input" id="customFile">
                                            <input type="hidden" value="{{ $page_2->image_4 }}" name="image_4" class="custom-file-input" id="customFile">
                                            <input type="hidden" value="{{ $page_2->image_5 }}" name="image_5" class="custom-file-input" id="customFile">
                                            <input type="hidden" value="{{ $page_2->image_6 }}" name="image_6" class="custom-file-input" id="customFile">
                                            <input type="hidden" value="{{ $page_2->image_7 }}" name="image_7" class="custom-file-input" id="customFile">
                                            <input type="hidden" value="{{ $page_2->image_8 }}" name="image_8" class="custom-file-input" id="customFile"> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="col-md-12">
                                <form action="{{url('/admin/landing-page/page_2/update/'.$page_2->id)}}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Judul Template</label>
                                            <input type="text" name="is_judul_landing_page" class="form-control" placeholder="Judul Template ..." value="{{ $page_2->is_judul_landing_page }}">
                                            @error('is_judul_landing_page')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="">Image 1</label>
                                            <div class="custom-file">
                                                <input type="file" name="image_1" class="custom-file-input" id="customFile">
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if (!empty($page_2->image_1))
                                                    <a href="{{ url('storage/landing-page-image-2/'.$page_2->image_1) }}" target="_blank" rel="noopener noreferrer" class="badge badge-primary"><i class="fa fa-image"></i>  {{$page_2->image_1}}</a>    
                                                @else
                                                    <span class="badge badge-warning" style="color: white"><i class="fa fa-image"></i>  Gambar Tidak Ada</span>
                                                @endif
                                            </div>
                                        </div>
                                
                                        <div class="form-group">
                                            <label>Judul Vidio</label>
                                            <input type="text" name="judul_vidio_embed" class="form-control" value="{{$page_2->judul_vidio_embed}}" placeholder="Judul Vidio ...">
                                        </div>
                                
                                        <div class="form-group">
                                            <label>Embed Link Vidio</label>
                                            <input type="text" name="embed_link_vidio" class="form-control" value="{{$page_2->embed_link_vidio}}" placeholder="Embed Link Vidio ...">
                                        </div>
                                
                                        <div class="form-group">
                                            <label for="">Image 2</label>
                                            <div class="custom-file">
                                                <input type="file" name="image_2" class="custom-file-input" id="customFile">
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if (!empty($page_2->image_2))
                                                    <a href="{{ url('storage/landing-page-image-2/'.$page_2->image_2) }}" target="_blank" rel="noopener noreferrer" class="badge badge-primary"><i class="fa fa-image"></i>  {{$page_2->image_2}}</a>    
                                                @else
                                                    <span class="badge badge-warning" style="color: white"><i class="fa fa-image"></i>  Gambar Tidak Ada</span>
                                                @endif
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label for="">Image 3</label>
                                            <div class="custom-file">
                                                <input type="file" name="image_3" class="custom-file-input" id="customFile">
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if (!empty($page_2->image_3))
                                                    <a href="{{ url('storage/landing-page-image-2/'.$page_2->image_3) }}" target="_blank" rel="noopener noreferrer" class="badge badge-primary"><i class="fa fa-image"></i>  {{$page_2->image_3}}</a>    
                                                @else
                                                    <span class="badge badge-warning" style="color: white"><i class="fa fa-image"></i>  Gambar Tidak Ada</span>
                                                @endif
                                            </div>
                                        </div>
                                
                                        <div class="form-group">
                                            <label>Nama Button 1</label>
                                            <input type="text" name="nama_button_1" class="form-control" value="{{$page_2->nama_button_1}}" placeholder="Nama Button 1 ...">
                                        </div>
                                
                                        <div class="form-group">
                                            <label for="">Image 4</label>
                                            <div class="custom-file">
                                                <input type="file" name="image_4" class="custom-file-input" id="customFile">
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if (!empty($page_2->image_4))
                                                    <a href="{{ url('storage/landing-page-image-2/'.$page_2->image_4) }}" target="_blank" rel="noopener noreferrer" class="badge badge-primary"><i class="fa fa-image"></i>  {{$page_2->image_4}}</a>    
                                                @else
                                                    <span class="badge badge-warning" style="color: white"><i class="fa fa-image"></i>  Gambar Tidak Ada</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Image 5</label>
                                            <div class="custom-file">
                                                <input type="file" name="image_5" class="custom-file-input" id="customFile">
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if (!empty($page_2->image_5))
                                                    <a href="{{ url('storage/landing-page-image-2/'.$page_2->image_5) }}" target="_blank" rel="noopener noreferrer" class="badge badge-primary"><i class="fa fa-image"></i>  {{$page_2->image_5}}</a>    
                                                @else
                                                    <span class="badge badge-warning" style="color: white"><i class="fa fa-image"></i>  Gambar Tidak Ada</span>
                                                @endif
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label for="">Image 6</label>
                                            <div class="custom-file">
                                                <input type="file" name="image_6" class="custom-file-input" id="customFile">
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if (!empty($page_2->image_6))
                                                    <a href="{{ url('storage/landing-page-image-2/'.$page_2->image_6) }}" target="_blank" rel="noopener noreferrer" class="badge badge-primary"><i class="fa fa-image"></i>  {{$page_2->image_6}}</a>    
                                                @else
                                                    <span class="badge badge-warning" style="color: white"><i class="fa fa-image"></i>  Gambar Tidak Ada</span>
                                                @endif
                                            </div>
                                        </div>
                                
                                        <div class="form-group">
                                            <label>Nama Button 2</label>
                                            <input type="text" name="nama_button_1" class="form-control" value="{{$page_2->nama_button_1}}" placeholder="Nama Button 2 ...">
                                        </div>
                                        <div class="form-group">
                                            <label>Judul 1</label>
                                            <input type="text" name="judul_1" class="form-control" placeholder="Judul 1 ..." value="{{$page_2->judul_1}}">
                                            @error('judul_1')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                
                                        <div class="form-group">
                                            <label>Judul 2</label>
                                            <input type="text" name="judul_2" class="form-control" placeholder="Judul 2 ..." value="{{$page_2->judul_2}}">
                                            @error('judul_2')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label>Nama User 1</label>
                                            <input type="text" name="nama_user_1" class="form-control" value="{{$page_2->nama_user_1}}" placeholder="Nama User 1 ...">
                                        </div>
                                        <div class="form-group">
                                            <label>Url Vidio 1</label>
                                            <input type="text" name="url_vidio_user_1" class="form-control" value="{{$page_2->url_vidio_user_1}}" placeholder="Url Vidio 1 ...">
                                        </div>
                                        <div class="form-group">
                                            <label>Deskripsi user 1</label>
                                            <textarea class="summernote" name="deskripsi_user_1" row="5">{!!$page_2->deskripsi_user_1!!}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Nama User 2</label>
                                            <input type="text" name="nama_user_2" class="form-control" value="{{$page_2->nama_user_2}}" placeholder="Nama User 2 ...">
                                        </div>
                                        <div class="form-group">
                                            <label>Url Vidio 2</label>
                                            <input type="text" name="url_vidio_user_2" class="form-control" value="{{$page_2->url_vidio_user_2}}" placeholder="Url Vidio 2 ...">
                                        </div>
                                        <div class="form-group">
                                            <label>Deskripsi user 2</label>
                                            <textarea class="summernote" name="deskripsi_user_2" row="5">{!!$page_2->deskripsi_user_2!!}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Nama User 3</label>
                                            <input type="text" name="nama_user_3" class="form-control" value="{{$page_2->nama_user_3}}" placeholder="Nama User 3 ...">
                                        </div>
                                        <div class="form-group">
                                            <label>Url Vidio 3</label>
                                            <input type="text" name="url_vidio_user_3" class="form-control" value="{{$page_2->url_vidio_user_3}}" placeholder="Url Vidio 3 ...">
                                        </div>
                                        <div class="form-group">
                                            <label>Deskripsi user 3</label>
                                            <textarea class="summernote" name="deskripsi_user_3" row="5">{!!$page_2->deskripsi_user_3!!}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Judul 3</label>
                                            <input type="text" name="judul_3" class="form-control" placeholder="Judul 3 ..." value="{{$page_2->judul_3}}">
                                            @error('judul_3')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label>Nama User 4</label>
                                            <input type="text" name="nama_user_4" class="form-control" value="{{$page_2->nama_user_4}}" placeholder="Nama User 4 ...">
                                        </div>
                                        <div class="form-group">
                                            <label>Url Vidio 4</label>
                                            <input type="text" name="url_vidio_user_4" class="form-control" value="{{$page_2->url_vidio_user_4}}" placeholder="Url Vidio 4 ...">
                                        </div>
                                        <div class="form-group">
                                            <label>Deskripsi user 4</label>
                                            <textarea class="summernote" name="deskripsi_user_4" row="5">{!!$page_2->deskripsi_user_4!!}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Nama User 5</label>
                                            <input type="text" name="nama_user_5" class="form-control" value="{{$page_2->nama_user_5}}" placeholder="Nama User 5 ...">
                                        </div>
                                        <div class="form-group">
                                            <label>Url Vidio 5</label>
                                            <input type="text" name="url_vidio_user_5" class="form-control" value="{{$page_2->url_vidio_user_5}}" placeholder="Url Vidio 5 ...">
                                        </div>
                                        <div class="form-group">
                                            <label>Deskripsi user 5</label>
                                            <textarea class="summernote" name="deskripsi_user_5" row="5">{!!$page_2->deskripsi_user_5!!}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Nama Button 3</label>
                                            <input type="text" name="nama_button_3" class="form-control" value="{{$page_2->nama_button_3}}" placeholder="Nama Button 3 ...">
                                        </div>
                                        <div class="form-group">
                                            <label for="">Image 7</label>
                                            <div class="custom-file">
                                                <input type="file" name="image_7" class="custom-file-input" id="customFile">
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if (!empty($page_2->image_7))
                                                    <a href="{{ url('storage/landing-page-image-2/'.$page_2->image_7) }}" target="_blank" rel="noopener noreferrer" class="badge badge-primary"><i class="fa fa-image"></i>  {{$page_2->image_7}}</a>    
                                                @else
                                                    <span class="badge badge-warning" style="color: white"><i class="fa fa-image"></i>  Gambar Tidak Ada</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Image 8</label>
                                            <div class="custom-file">
                                                <input type="file" name="image_8" class="custom-file-input" id="customFile">
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if (!empty($page_2->image_8))
                                                    <a href="{{ url('storage/landing-page-image-2/'.$page_2->image_8) }}" target="_blank" rel="noopener noreferrer" class="badge badge-primary"><i class="fa fa-image"></i>  {{$page_2->image_8}}</a>    
                                                @else
                                                    <span class="badge badge-warning" style="color: white"><i class="fa fa-image"></i>  Gambar Tidak Ada</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Nama Button 4</label>
                                            <input type="text" name="nama_button_4" class="form-control" value="{{$page_2->nama_button_4}}" placeholder="Nama Button 4 ...">
                                        </div>
                                        <div class="form-group">
                                            <label>Judul 4</label>
                                            <input type="text" name="judul_4" class="form-control" placeholder="Judul 4 ..." value="{{$page_2->judul_4}}">
                                            @error('judul_4')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label>Acordion 1</label>
                                            <input type="text" name="judul_accordion_1" class="form-control" value="{{$page_2->judul_accordion_1}}" placeholder="Acordion 1 ...">
                                        </div>
                                        <div class="form-group">
                                            <label>Deskripsi 1</label>
                                            <textarea class="summernote" name="deskripsi_accordion_1" row="5">{!!$page_2->deskripsi_accordion_2!!}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Acordion 2</label>
                                            <input type="text" name="judul_accordion_2" class="form-control" value="{{$page_2->judul_accordion_2}}" placeholder="Acordion 2 ...">
                                        </div>
                                        <div class="form-group">
                                            <label>Deskripsi 2</label>
                                            <textarea class="summernote" name="deskripsi_accordion_2" row="5">{!!$page_2->deskripsi_accordion_2!!}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Acordion 3</label>
                                            <input type="text" name="judul_accordion_3" class="form-control" value="{{$page_2->judul_accordion_3}}" placeholder="Acordion 3 ...">
                                        </div>
                                        <div class="form-group">
                                            <label>Deskripsi 3</label>
                                            <textarea class="summernote" name="deskripsi_accordion_3" row="5">{!!$page_2->deskripsi_accordion_3!!}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Acordion 4</label>
                                            <input type="text" name="judul_accordion_4" class="form-control" value="{{$page_2->judul_accordion_4}}" placeholder="Acordion 4 ...">
                                        </div>
                                        <div class="form-group">
                                            <label>Deskripsi 4</label>
                                            <textarea class="summernote" name="deskripsi_accordion_4" row="5">{!!$page_2->deskripsi_accordion_4!!}</textarea>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-sm">Simpan Perubahan</button>
                                </form>
                            </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>
        $(function () {
            $('.summernote').summernote()
            $('.desc_prof1').summernote()
        })

        $(function () {
            bsCustomFileInput.init();
        });

        function deleteData(id){
            swal({
                title: "Alert!",
                text: "Apakah anda yakin akan menghapus Semua Gambar tersebut ?",
                type: 'error',
                showCancelButton: true,
                confirmButtonText: 'Ya, Saya yakin',
                cancelButtonText: 'Tidak',
            },function(result) {
                if(result){
                    document.getElementById('deleteData-'+id).submit();
                }
            });
        }
    </script>
@endsection