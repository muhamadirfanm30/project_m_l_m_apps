<form action="{{route('landing-page-1.store')}}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="col-md-12">
        <div class="form-group">
            <label>Judul Template *</label>
            <input type="text" name="is_judul_landing_page" class="form-control" placeholder="Judul Template ..." value="{{ old('is_judul_landing_page') }}">
            @error('is_judul_landing_page')
                <span style="color:red"><small>{{ $message }}</small></span>
            @enderror
        </div>
        <div class="form-group">
            <label for="">Image 1</label>
            <div class="custom-file">
                <input type="file" name="image_1" class="custom-file-input" id="customFile">
                <label class="custom-file-label" for="customFile">Choose file</label>
                @error('image_1')
                    <span style="color:red"><small>{{ $message }}</small></span>
                @enderror
            </div>
        </div>
        <div class="form-group">
            <label>Judul Vidio</label>
            <input type="text" name="judul_vidio_embed" class="form-control" placeholder="Judul Vidio ..." value="{{ old('judul_vidio_embed') }}">
            @error('judul_vidio_embed')
                <span style="color:red"><small>{{ $message }}</small></span>
            @enderror
        </div>

        <div class="form-group">
            <label>Embed Link Vidio</label>
            <input type="text" name="embed_link_vidio" class="form-control" placeholder="Embed Link Vidio ..." value="{{ old('embed_link_vidio') }}">
            @error('embed_link_vidio')
                <span style="color:red"><small>{{ $message }}</small></span>
            @enderror
        </div>

        <div class="form-group">
            <label>Judul Deskripsi</label>
            <input type="text" name="judul_deskripsi" class="form-control" placeholder="Judul Deskripsi ..." value="{{ old('judul_deskripsi') }}">
            @error('judul_deskripsi')
                <span style="color:red"><small>{{ $message }}</small></span>
            @enderror
        </div>

        <div class="form-group">
            <label>Deskripsi</label>
            <textarea class="summernote" name="deskripsi" row="5">{!! old('judul_deskripsi') !!}</textarea>
            @error('judul_deskripsi')
                <span style="color:red"><small>{{ $message }}</small></span>
            @enderror
        </div>
        <div class="form-group">
            <label for="">Image 2</label>
            <div class="custom-file">
                <input type="file" name="image_2" class="custom-file-input" id="customFile">
                <label class="custom-file-label" for="customFile">Choose file</label>
                @error('image_2')
                    <span style="color:red"><small>{{ $message }}</small></span>
                @enderror
            </div>
        </div>
        <div class="form-group">
            <label>Nama Button 1</label>
            <input type="text" name="nama_button_1" class="form-control" placeholder="Nama Button 1 ..." value="{{ old('nama_button_1') }}">
            @error('nama_button_1')
                <span style="color:red"><small>{{ $message }}</small></span>
            @enderror
        </div>
        <div class="form-group">
            <label for="">Image 3</label>
            <div class="custom-file">
                <input type="file" name="image_3" class="custom-file-input" id="customFile">
                <label class="custom-file-label" for="customFile">Choose file</label>
                @error('image_3')
                    <span style="color:red"><small>{{ $message }}</small></span>
                @enderror
            </div>
        </div>
        <div class="form-group">
            <label for="">Image 4</label>
            <div class="custom-file">
                <input type="file" name="image_4" class="custom-file-input" id="customFile">
                <label class="custom-file-label" for="customFile">Choose file</label>
                @error('image_4')
                    <span style="color:red"><small>{{ $message }}</small></span>
                @enderror
            </div>
        </div>
        <div class="form-group">
            <label for="">Image 5</label>
            <div class="custom-file">
                <input type="file" name="image_5" class="custom-file-input" id="customFile">
                <label class="custom-file-label" for="customFile">Choose file</label>
                @error('image_5')
                    <span style="color:red"><small>{{ $message }}</small></span>
                @enderror
            </div>
        </div>
        <div class="form-group">
            <label>Nama Button 2</label>
            <input type="text" name="nama_button_2" class="form-control" placeholder="Nama Button 2 ..." value="{{ old('nama_button_2') }}">
            @error('nama_button_2')
                <span style="color:red"><small>{{ $message }}</small></span>
            @enderror
        </div>
        <div class="form-group">
            <label>judul 1</label>
            <input type="text" name="judul_1" class="form-control" placeholder="judul 1 ..." value="{{ old('judul_1') }}">
            @error('judul_1')
                <span style="color:red"><small>{{ $message }}</small></span>
            @enderror
        </div>
        <div class="form-group">
            <label>judul 2</label>
            <input type="text" name="judul_2" class="form-control" placeholder="judul 2 ..." value="{{ old('judul_2') }}">
            @error('judul_2')
                <span style="color:red"><small>{{ $message }}</small></span>
            @enderror
        </div>
        <div class="form-group">
            <label>Nama User 1</label>
            <input type="text" name="nama_user_1" class="form-control" placeholder="Nama User 1 ..." value="{{ old('nama_user_1') }}">
            @error('nama_user_1')
                <span style="color:red"><small>{{ $message }}</small></span>
            @enderror
        </div>
        <div class="form-group">
            <label>Url Vidio 1</label>
            <input type="text" name="url_vidio_1" class="form-control" placeholder="Url Vidio 1 ..." value="{{ old('url_vidio_1') }}">
            @error('url_vidio_1')
                <span style="color:red"><small>{{ $message }}</small></span>
            @enderror
        </div>
        <div class="form-group">
            <label>Deskripsi user 1</label>
            <textarea class="summernote" placeholder="Deskripsi user 1" name="deskripsi_user_1" row="5">{!! old('deskripsi_user_1') !!}</textarea>
            @error('deskripsi_user_1')
                <span style="color:red"><small>{{ $message }}</small></span>
            @enderror
        </div>
        <div class="form-group">
            <label>Nama User 2</label>
            <input type="text" name="nama_user_2" class="form-control" placeholder="Nama User 2 ..." value="{{ old('nama_user_2') }}">
            @error('nama_user_2')
                <span style="color:red"><small>{{ $message }}</small></span>
            @enderror
        </div>
        <div class="form-group">
            <label>Url Vidio 2</label>
            <input type="text" name="url_vidio_2" class="form-control" placeholder="Url Vidio 2 ..." value="{{ old('url_vidio_2') }}">
            @error('url_vidio_2')
                <span style="color:red"><small>{{ $message }}</small></span>
            @enderror
        </div>
        <div class="form-group">
            <label>Deskripsi user 2</label>
            <textarea class="summernote" placeholder="Deskripsi user 2" name="deskripsi_user_2" row="5">{!! old('deskripsi_user_2') !!}</textarea>
            @error('deskripsi_user_2')
                <span style="color:red"><small>{{ $message }}</small></span>
            @enderror
        </div>
        <div class="form-group">
            <label>Nama User 3</label>
            <input type="text" name="nama_user_3" class="form-control" placeholder="Nama User 3 ..." value="{{ old('nama_user_3') }}">
            @error('nama_user_3')
                <span style="color:red"><small>{{ $message }}</small></span>
            @enderror
        </div>
        <div class="form-group">
            <label>Url Vidio 3</label>
            <input type="text" name="url_vidio_3" class="form-control" placeholder="Url Vidio 3 ..." value="{{ old('url_vidio_3') }}">
            @error('url_vidio_3')
                <span style="color:red"><small>{{ $message }}</small></span>
            @enderror
        </div>
        <div class="form-group">
            <label>Deskripsi user 3</label>
            <textarea class="summernote" placeholder="Deskripsi user 3" name="deskripsi_user_3" row="5">{!! old('deskripsi_user_3') !!}</textarea>
            @error('deskripsi_user_3')
                <span style="color:red"><small>{{ $message }}</small></span>
            @enderror
        </div>
        <div class="form-group">
            <label>Nama Button 3</label>
            <input type="text" name="nama_button_3" class="form-control" placeholder="Nama Button 3 ..." value="{{ old('nama_button_3') }}">
            @error('nama_button_3')
                <span style="color:red"><small>{{ $message }}</small></span>
            @enderror
        </div>
        <div class="form-group">
            <label for="">Image 6</label>
            <div class="custom-file">
                <input type="file" name="image_6" class="custom-file-input" id="customFile">
                <label class="custom-file-label" for="customFile">Choose file</label>
                @error('image_6')
                    <span style="color:red"><small>{{ $message }}</small></span>
                @enderror
            </div>
        </div>
        <div class="form-group">
            <label for="">Image 7</label>
            <div class="custom-file">
                <input type="file" name="image_7" class="custom-file-input" id="customFile">
                <label class="custom-file-label" for="customFile">Choose file</label>
                @error('image_7')
                    <span style="color:red"><small>{{ $message }}</small></span>
                @enderror
            </div>
        </div>
        <div class="form-group">
            <label for="">Image 8</label>
            <div class="custom-file">
                <input type="file" name="image_8" class="custom-file-input" id="customFile">
                <label class="custom-file-label" for="customFile">Choose file</label>
                @error('image_8')
                    <span style="color:red"><small>{{ $message }}</small></span>
                @enderror
            </div>
        </div>
        <div class="form-group">
            <label>Nama Button 4</label>
            <input type="text" name="nama_button_4" class="form-control" placeholder="Nama Button 4 ..." value="{{ old('nama_button_4') }}">
            @error('nama_button_4')
                <span style="color:red"><small>{{ $message }}</small></span>
            @enderror
        </div>
        <div class="form-group">
            <label>judul 3</label>
            <input type="text" name="judul_3" class="form-control" placeholder="judul 3 ..." value="{{ old('judul_3') }}">
            @error('judul_3')
                <span style="color:red"><small>{{ $message }}</small></span>
            @enderror
        </div>
        <div class="form-group">
            <label>Acordion 1</label>
            <input type="text" name="judul_accordion_1" class="form-control" placeholder="Acordion 1 ..." value="{{ old('judul_accordion_1') }}">
            @error('judul_accordion_1')
                <span style="color:red"><small>{{ $message }}</small></span>
            @enderror
        </div>
        <div class="form-group">
            <label>Deskripsi 2</label>
            <textarea class="summernote" name="deskripsi_accordion_1" row="5">{!! old('deskripsi_accordion_1') !!}</textarea>
            @error('deskripsi_accordion_1')
                <span style="color:red"><small>{{ $message }}</small></span>
            @enderror
        </div>
        <div class="form-group">
            <label>Acordion 2</label>
            <input type="text" name="judul_accordion_2" class="form-control" placeholder="Acordion 2 ..." value="{{ old('judul_accordion_2') }}">
            @error('judul_accordion_2')
                <span style="color:red"><small>{{ $message }}</small></span>
            @enderror
        </div>
        <div class="form-group">
            <label>Deskripsi 2</label>
            <textarea class="summernote" name="deskripsi_accordion_2" row="5">{!! old('deskripsi_accordion_2') !!}</textarea>
            @error('deskripsi_accordion_2')
                <span style="color:red"><small>{{ $message }}</small></span>
            @enderror
        </div>
        <div class="form-group">
            <label>Acordion 3</label>
            <input type="text" name="judul_accordion_3" class="form-control" placeholder="Acordion 3 ..." value="{{ old('judul_accordion_3') }}">
            @error('judul_accordion_3')
                <span style="color:red"><small>{{ $message }}</small></span>
            @enderror
        </div>
        <div class="form-group">
            <label>Deskripsi 3</label>
            <textarea class="summernote" name="deskripsi_accordion_3" row="5">{!! old('deskripsi_accordion_3') !!}</textarea>
            @error('deskripsi_accordion_3')
                <span style="color:red"><small>{{ $message }}</small></span>
            @enderror
        </div>
        <div class="form-group">
            <label>Acordion 4</label>
            <input type="text" name="judul_accordion_4" class="form-control" placeholder="Acordion 4 ..." value="{{ old('judul_accordion_4') }}">
            @error('judul_accordion_4')
                <span style="color:red"><small>{{ $message }}</small></span>
            @enderror
        </div>
        <div class="form-group">
            <label>Deskripsi 4</label>
            <textarea class="summernote" name="deskripsi_accordion_4" row="5">{!! old('deskripsi_accordion_4') !!}</textarea>
            @error('deskripsi_accordion_4')
                <span style="color:red"><small>{{ $message }}</small></span>
            @enderror
        </div>
    </div>
    <button type="submit" class="btn btn-primary btn-sm">Simpan Perubahan Template</button>
</form>

