
@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('success') }}</strong>
                        </div>
                    @elseif(session()->has('failed'))
                        <div class="alert alert-danger alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('failed') }}</strong>
                        </div>
                    @endif
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3 class="card-title">
                                        Landing Page
                                    </h3>
                                </div>
                                <div class="col-md-2">
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="col-md-12">
                                <div class="card card-primary card-tabs">
                                    <div class="card-header p-0 pt-1">
                                        <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="custom-tabs-one-page-1-tab" data-toggle="pill" href="#custom-tabs-one-page-1" role="tab" aria-controls="custom-tabs-one-page-1" aria-selected="true">Template 1</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="custom-tabs-one-page-2-tab" data-toggle="pill" href="#custom-tabs-one-page-2" role="tab" aria-controls="custom-tabs-one-page-2" aria-selected="false">Template 2</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="custom-tabs-one-page-3-tab" data-toggle="pill" href="#custom-tabs-one-page-3" role="tab" aria-controls="custom-tabs-one-page-3" aria-selected="false">Template 3</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="custom-tabs-one-page-4-tab" data-toggle="pill" href="#custom-tabs-one-page-4" role="tab" aria-controls="custom-tabs-one-page-4" aria-selected="false">Template 4</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="custom-tabs-one-page-5-tab" data-toggle="pill" href="#custom-tabs-one-page-5" role="tab" aria-controls="custom-tabs-one-page-5" aria-selected="false">Template 5</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="custom-tabs-one-page-6-tab" data-toggle="pill" href="#custom-tabs-one-page-6" role="tab" aria-controls="custom-tabs-one-page-6" aria-selected="false">Template 6</a>
                                        </li>
                                        </ul>
                                    </div>
                                    <div class="card-body">
                                        <div class="tab-content" id="custom-tabs-one-tabContent">
                                            <div class="tab-pane fade show active" id="custom-tabs-one-page-1" role="tabpanel" aria-labelledby="custom-tabs-one-page-1-tab">
                                                @if(!empty($page_1))
                                                    @include('admin.landingPage.page1Update')
                                                @else 
                                                    @include('admin.landingPage.page1')
                                                @endif
                                            </div>
                                            <div class="tab-pane fade" id="custom-tabs-one-page-2" role="tabpanel" aria-labelledby="custom-tabs-one-page-2-tab">
                                                @if(!empty($page_2))    
                                                    @include('admin.landingPage.page2Update')
                                                @else 
                                                    @include('admin.landingPage.page2')
                                                @endif
                                            </div>
                                            <div class="tab-pane fade" id="custom-tabs-one-page-3" role="tabpanel" aria-labelledby="custom-tabs-one-page-3-tab">
                                                @if(!empty($page_3))    
                                                    @include('admin.landingPage.page3Update')
                                                @else 
                                                    @include('admin.landingPage.page3')
                                                @endif
                                            </div>
                                            <div class="tab-pane fade" id="custom-tabs-one-page-4" role="tabpanel" aria-labelledby="custom-tabs-one-page-4-tab">
                                                @if(!empty($page_4))    
                                                    @include('admin.landingPage.page4Update')
                                                @else 
                                                    @include('admin.landingPage.page4')
                                                @endif
                                            </div>
                                            <div class="tab-pane fade" id="custom-tabs-one-page-5" role="tabpanel" aria-labelledby="custom-tabs-one-page-5-tab">
                                                @if(!empty($page_5))    
                                                    @include('admin.landingPage.page5Update')
                                                @else 
                                                    @include('admin.landingPage.page5')
                                                @endif
                                            </div>
                                            <div class="tab-pane fade" id="custom-tabs-one-page-6" role="tabpanel" aria-labelledby="custom-tabs-one-page-6-tab">
                                                @if(!empty($page_6))    
                                                    @include('admin.landingPage.page6Update')
                                                @else 
                                                    @include('admin.landingPage.page6')
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.card -->
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>
        $(function () {
            $('.summernote').summernote()
            $('.desc_prof1').summernote()
        })

        $(function () {
            bsCustomFileInput.init();
        });
    </script>
@endsection