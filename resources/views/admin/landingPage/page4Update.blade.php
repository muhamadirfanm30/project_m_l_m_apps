

@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('success') }}</strong>
                        </div>
                    @elseif(session()->has('failed'))
                        <div class="alert alert-danger alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('failed') }}</strong>
                        </div>
                    @endif
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-8">
                                    <h3 class="card-title">
                                        Landing Page 4 Update 
                                    </h3>
                                </div>
                                <div class="col-md-4">
                                    <form action="{{ url('/admin/landing-page/page_4/delete-image/'.$page_4->id) }}" method="post"  enctype="multipart/form-data" id="deleteData-{{$page_4->id}}">
                                        @csrf
                                    </form>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <a href="{{ url('/admin/landing-page/show') }}" class="btn btn-primary btn-sm btn-block" style="float: right">Kembali</a>
                                        </div>
                                        <div class="col-md-6">
                                            <button type="submit" class="btn btn-danger btn-sm btn-block" style="float: right" onclick="deleteData({{ $page_4->id }})">Hapus Gambar</button>
                                            {{-- <input type="hidden" value="{{ $page_4->image_1 }}" name="image_1" class="custom-file-input" id="customFile">
                                            <input type="hidden" value="{{ $page_4->image_2 }}" name="image_2" class="custom-file-input" id="customFile">
                                            <input type="hidden" value="{{ $page_4->image_3 }}" name="image_3" class="custom-file-input" id="customFile">
                                            <input type="hidden" value="{{ $page_4->image_4 }}" name="image_4" class="custom-file-input" id="customFile">
                                            <input type="hidden" value="{{ $page_4->image_5 }}" name="image_5" class="custom-file-input" id="customFile">
                                            <input type="hidden" value="{{ $page_4->image_6 }}" name="image_6" class="custom-file-input" id="customFile">
                                            <input type="hidden" value="{{ $page_4->image_7 }}" name="image_7" class="custom-file-input" id="customFile">
                                            <input type="hidden" value="{{ $page_4->image_8 }}" name="image_8" class="custom-file-input" id="customFile">
                                            <input type="hidden" value="{{ $page_4->image_9 }}" name="image_8" class="custom-file-input" id="customFile">
                                            <input type="hidden" value="{{ $page_4->image_10 }}" name="image_8" class="custom-file-input" id="customFile">
                                            <input type="hidden" value="{{ $page_4->image_11 }}" name="image_8" class="custom-file-input" id="customFile"> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="col-md-12">
                                <form action="{{url('/admin/landing-page/page_4/update/'.$page_4->id)}}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Judul Template</label>
                                            <input type="text" name="is_judul_landing_page" class="form-control" placeholder="Judul Template ..." value="{{ $page_4->is_judul_landing_page }}">
                                            @error('is_judul_landing_page')
                                                <span style="color:red"><small>{{ $message }}</small></span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="">Image 1</label>
                                            <div class="custom-file">
                                                <input type="file" name="image_1" class="custom-file-input" id="customFile">
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if (!empty($page_4->image_1))
                                                    <a href="{{ url('storage/landing-page-image-3/'.$page_4->image_1) }}" target="_blank" rel="noopener noreferrer" class="badge badge-primary"><i class="fa fa-image"></i>  {{$page_4->image_1}}</a>    
                                                @else
                                                    <span class="badge badge-warning" style="color: white"><i class="fa fa-image"></i>  Gambar Tidak Ada</span>
                                                @endif
                                            </div>
                                        </div>
                                
                                        <div class="form-group">
                                            <label>Judul Vidio</label>
                                            <input type="text" name="judul_vidio_embed" class="form-control" placeholder="Judul Vidio ..." value="{{ $page_4->judul_vidio_embed }}">
                                        </div>
                                
                                        <div class="form-group">
                                            <label>Embed Link Vidio</label>
                                            <input type="text" name="embed_link_vidio" class="form-control" placeholder="Embed Link Vidio ..." value="{{ $page_4->embed_link_vidio }}">
                                        </div>
                                
                                        <div class="form-group">
                                            <label for="">Image 2</label>
                                            <div class="custom-file">
                                                <input type="file" name="image_2" class="custom-file-input" id="customFile">
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if (!empty($page_4->image_2))
                                                    <a href="{{ url('storage/landing-page-image-3/'.$page_4->image_2) }}" target="_blank" rel="noopener noreferrer" class="badge badge-primary"><i class="fa fa-image"></i>  {{$page_4->image_2}}</a>    
                                                @else
                                                    <span class="badge badge-warning" style="color: white"><i class="fa fa-image"></i>  Gambar Tidak Ada</span>
                                                @endif
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label for="">Image 3</label>
                                            <div class="custom-file">
                                                <input type="file" name="image_3" class="custom-file-input" id="customFile">
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if (!empty($page_4->image_3))
                                                    <a href="{{ url('storage/landing-page-image-3/'.$page_4->image_3) }}" target="_blank" rel="noopener noreferrer" class="badge badge-primary"><i class="fa fa-image"></i>  {{$page_4->image_3}}</a>    
                                                @else
                                                    <span class="badge badge-warning" style="color: white"><i class="fa fa-image"></i>  Gambar Tidak Ada</span>
                                                @endif
                                            </div>
                                        </div>
                                
                                        <div class="form-group">
                                            <label>Nama Button 1</label>
                                            <input type="text" name="nama_button_1" class="form-control" placeholder="Nama Button 1 ..." value="{{ $page_4->nama_button_1 }}">
                                        </div>
                                
                                        <div class="form-group">
                                            <label for="">Image 4</label>
                                            <div class="custom-file">
                                                <input type="file" name="image_4" class="custom-file-input" id="customFile">
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if (!empty($page_4->image_4))
                                                    <a href="{{ url('storage/landing-page-image-3/'.$page_4->image_4) }}" target="_blank" rel="noopener noreferrer" class="badge badge-primary"><i class="fa fa-image"></i>  {{$page_4->image_4}}</a>    
                                                @else
                                                    <span class="badge badge-warning" style="color: white"><i class="fa fa-image"></i>  Gambar Tidak Ada</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Image 5</label>
                                            <div class="custom-file">
                                                <input type="file" name="image_5" class="custom-file-input" id="customFile">
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if (!empty($page_4->image_5))
                                                    <a href="{{ url('storage/landing-page-image-3/'.$page_4->image_5) }}" target="_blank" rel="noopener noreferrer" class="badge badge-primary"><i class="fa fa-image"></i>  {{$page_4->image_5}}</a>    
                                                @else
                                                    <span class="badge badge-warning" style="color: white"><i class="fa fa-image"></i>  Gambar Tidak Ada</span>
                                                @endif
                                            </div>
                                        </div>
                                
                                        <div class="form-group">
                                            <label>Judul 1</label>
                                            <input type="text" name="judul_1" class="form-control" placeholder="Judul 1 ..." value="{{ $page_4->judul_1 }}">
                                        </div>
                                        <div class="form-group">
                                            <label>Nama User 1</label>
                                            <input type="text" name="nama_user_1" class="form-control" placeholder="Nama User 1 ..." value="{{ $page_4->nama_user_1 }}">
                                        </div>
                                        <div class="form-group">
                                            <label>Url Vidio 1</label>
                                            <input type="text" name="url_vidio_user_1" class="form-control" placeholder="Url Vidio 1 ..." value="{{ $page_4->url_vidio_user_1 }}">
                                        </div>
                                        <div class="form-group">
                                            <label>Deskripsi user 1</label>
                                            <textarea class="summernote" name="deskripsi_user_1" row="5">{!! $page_4->deskripsi_user_1 !!}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Nama User 2</label>
                                            <input type="text" name="nama_user_2" class="form-control" placeholder="Nama User 2 ..." value="{{ $page_4->nama_user_2 }}">
                                        </div>
                                        <div class="form-group">
                                            <label>Url Vidio 2</label>
                                            <input type="text" name="url_vidio_user_2" class="form-control" placeholder="Url Vidio 2 ..." value="{{ $page_4->url_vidio_user_2 }}">
                                        </div>
                                        <div class="form-group">
                                            <label>Deskripsi user 2</label>
                                            <textarea class="summernote" name="deskripsi_user_2" row="5">{!! $page_4->deskripsi_user_2 !!}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Nama User 3</label>
                                            <input type="text" name="nama_user_3" class="form-control" placeholder="Nama User 3 ..." value="{{ $page_4->nama_user_3 }}">
                                        </div>
                                        <div class="form-group">
                                            <label>Url Vidio 3</label>
                                            <input type="text" name="url_vidio_user_3" class="form-control" placeholder="Url Vidio 3 ..." value="{{ $page_4->url_vidio_user_3 }}">
                                        </div>
                                        <div class="form-group">
                                            <label>Deskripsi user 3</label>
                                            <textarea class="summernote" name="deskripsi_user_3" row="5">{!! $page_4->deskripsi_user_3 !!}</textarea>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>Nama Button 2</label>
                                            <input type="text" name="nama_button_2" class="form-control" placeholder="Nama Button 2 ..." value="{{ $page_4->nama_button_2 }}">
                                        </div>
                                
                                        <div class="form-group">
                                            <label for="">Image 6</label>
                                            <div class="custom-file">
                                                <input type="file" name="image_6" class="custom-file-input" id="customFile">
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if (!empty($page_4->image_6))
                                                    <a href="{{ url('storage/landing-page-image-3/'.$page_4->image_6) }}" target="_blank" rel="noopener noreferrer" class="badge badge-primary"><i class="fa fa-image"></i>  {{$page_4->image_6}}</a>    
                                                @else
                                                    <span class="badge badge-warning" style="color: white"><i class="fa fa-image"></i>  Gambar Tidak Ada</span>
                                                @endif
                                            </div>
                                        </div>
                                
                                        <div class="form-group">
                                            <label for="">Image 7</label>
                                            <div class="custom-file">
                                                <input type="file" name="image_7" class="custom-file-input" id="customFile">
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if (!empty($page_4->image_7))
                                                    <a href="{{ url('storage/landing-page-image-3/'.$page_4->image_7) }}" target="_blank" rel="noopener noreferrer" class="badge badge-primary"><i class="fa fa-image"></i>  {{$page_4->image_7}}</a>    
                                                @else
                                                    <span class="badge badge-warning" style="color: white"><i class="fa fa-image"></i>  Gambar Tidak Ada</span>
                                                @endif
                                            </div>
                                        </div>
                                
                                        <div class="form-group">
                                            <label for="">Image 8</label>
                                            <div class="custom-file">
                                                <input type="file" name="image_8" class="custom-file-input" id="customFile">
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if (!empty($page_4->image_8))
                                                    <a href="{{ url('storage/landing-page-image-3/'.$page_4->image_8) }}" target="_blank" rel="noopener noreferrer" class="badge badge-primary"><i class="fa fa-image"></i>  {{$page_4->image_8}}</a>    
                                                @else
                                                    <span class="badge badge-warning" style="color: white"><i class="fa fa-image"></i>  Gambar Tidak Ada</span>
                                                @endif
                                            </div>
                                        </div>
                                
                                        <div class="form-group">
                                            <label>Nama Button 3</label>
                                            <input type="text" name="nama_button_3" class="form-control" placeholder="Nama Button 3 ..." value="{{ $page_4->nama_button_3 }}">
                                        </div>
                                
                                        <div class="form-group">
                                            <label for="">Image 9</label>
                                            <div class="custom-file">
                                                <input type="file" name="image_9" class="custom-file-input" id="customFile">
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if (!empty($page_4->image_9))
                                                    <a href="{{ url('storage/landing-page-image-3/'.$page_4->image_9) }}" target="_blank" rel="noopener noreferrer" class="badge badge-primary"><i class="fa fa-image"></i>  {{$page_4->image_9}}</a>    
                                                @else
                                                    <span class="badge badge-warning" style="color: white"><i class="fa fa-image"></i>  Gambar Tidak Ada</span>
                                                @endif
                                            </div>
                                        </div>
                                
                                        <div class="form-group">
                                            <label for="">Image 10</label>
                                            <div class="custom-file">
                                                <input type="file" name="image_10" class="custom-file-input" id="customFile">
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if (!empty($page_4->image_10))
                                                    <a href="{{ url('storage/landing-page-image-3/'.$page_4->image_10) }}" target="_blank" rel="noopener noreferrer" class="badge badge-primary"><i class="fa fa-image"></i>  {{$page_4->image_10}}</a>    
                                                @else
                                                    <span class="badge badge-warning" style="color: white"><i class="fa fa-image"></i>  Gambar Tidak Ada</span>
                                                @endif
                                            </div>
                                        </div>
                                
                                        <div class="form-group">
                                            <label>Judul 2</label>
                                            <input type="text" name="judul_2" class="form-control" placeholder="Judul 2 ..." value="{{ $page_4->judul_2 }}">
                                        </div>
                                        <div class="form-group">
                                            <label>Nama User 4</label>
                                            <input type="text" name="nama_user_4" class="form-control" placeholder="Nama User 4 ..." value="{{ $page_4->nama_user_4 }}">
                                        </div>
                                        <div class="form-group">
                                            <label>Url Vidio 4</label>
                                            <input type="text" name="url_vidio_user_4" class="form-control" placeholder="Url Vidio 4 ..." value="{{ $page_4->url_vidio_user_4 }}">
                                        </div>
                                        <div class="form-group">
                                            <label>Deskripsi user 4</label>
                                            <textarea class="summernote" name="deskripsi_user_4" row="5">{!! $page_4->deskripsi_user_4 !!}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Nama User 5</label>
                                            <input type="text" name="nama_user_5" class="form-control" placeholder="Nama User 5 ..." value="{{ $page_4->nama_user_5 }}">
                                        </div>
                                        <div class="form-group">
                                            <label>Url Vidio 5</label>
                                            <input type="text" name="url_vidio_user_5" class="form-control" placeholder="Url Vidio 5 ..." value="{{ $page_4->url_vidio_user_5 }}">
                                        </div>
                                        <div class="form-group">
                                            <label>Deskripsi user 5</label>
                                            <textarea class="summernote" name="deskripsi_user_5" row="5">{!! $page_4->deskripsi_user_5 !!}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Nama User 6</label>
                                            <input type="text" name="nama_user_6" class="form-control" placeholder="Nama User 6 ..." value="{{ $page_4->nama_user_6 }}">
                                        </div>
                                        <div class="form-group">
                                            <label>Url Vidio 6</label>
                                            <input type="text" name="url_vidio_user_6" class="form-control" placeholder="Url Vidio 6 ..." value="{{ $page_4->url_vidio_user_6 }}">
                                        </div>
                                        <div class="form-group">
                                            <label>Deskripsi user 6</label>
                                            <textarea class="summernote" name="deskripsi_user_6" row="5">{!! $page_4->deskripsi_user_6 !!}</textarea>
                                        </div>
                                
                                        <div class="form-group">
                                            <label>Nama Button 4</label>
                                            <input type="text" name="nama_button_4" class="form-control" placeholder="Nama Button 4 ..." value="{{ $page_4->nama_button_4 }}">
                                        </div>
                                
                                        <div class="form-group">
                                            <label for="">Image 11</label>
                                            <div class="custom-file">
                                                <input type="file" name="image_11" class="custom-file-input" id="customFile">
                                                <label class="custom-file-label" for="customFile">Choose file</label>
                                                @if (!empty($page_4->image_11))
                                                    <a href="{{ url('storage/landing-page-image-3/'.$page_4->image_11) }}" target="_blank" rel="noopener noreferrer" class="badge badge-primary"><i class="fa fa-image"></i>  {{$page_4->image_11}}</a>    
                                                @else
                                                    <span class="badge badge-warning" style="color: white"><i class="fa fa-image"></i>  Gambar Tidak Ada</span>
                                                @endif
                                            </div>
                                        </div>
                                
                                        <div class="form-group">
                                            <label>judul 3</label>
                                            <input type="text" name="judul_3" class="form-control" placeholder="judul 3 ..." value="{{ $page_4->judul_3 }}">
                                        </div>
                                
                                        <div class="form-group">
                                            <label>Acordion 1</label>
                                            <input type="text" name="judul_accordion_1" class="form-control" placeholder="Acordion 1 ..." value="{{ $page_4->judul_accordion_1 }}">
                                        </div>
                                
                                        <div class="form-group">
                                            <label>Deskripsi 1</label>
                                            <textarea class="summernote" name="deskripsi_accordion_1" row="5">{!! $page_4->deskripsi_accordion_1 !!}</textarea>
                                        </div>
                                
                                        <div class="form-group">
                                            <label>Acordion 2</label>
                                            <input type="text" name="judul_accordion_2" class="form-control" placeholder="Acordion 2 ..." value="{{ $page_4->judul_accordion_2 }}">
                                        </div>
                                
                                        <div class="form-group">
                                            <label>Deskripsi 2</label>
                                            <textarea class="summernote" name="deskripsi_accordion_2" row="5">{!! $page_4->deskripsi_accordion_2 !!}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Acordion 3</label>
                                            <input type="text" name="judul_accordion_3" class="form-control" placeholder="Acordion 3 ..." value="{{ $page_4->judul_accordion_3 }}">
                                        </div>
                                
                                        <div class="form-group">
                                            <label>Deskripsi 3</label>
                                            <textarea class="summernote" name="deskripsi_accordion_3" row="5">{!! $page_4->deskripsi_accordion_3 !!}</textarea>
                                        </div>
                                
                                        <div class="form-group">
                                            <label>Acordion 4</label>
                                            <input type="text" name="judul_accordion_4" class="form-control" placeholder="Acordion 4 ..." value="{{ $page_4->judul_accordion_4 }}">
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>Deskripsi 4</label>
                                            <textarea class="summernote" name="deskripsi_accordion_4" row="5">{!! $page_4->deskripsi_accordion_4 !!}</textarea>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-sm">Simpan Perubahan</button>
                                </form>
                            </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>
        $(function () {
            $('.summernote').summernote()
            $('.desc_prof1').summernote()
        })

        $(function () {
            bsCustomFileInput.init();
        });

        function deleteData(id){
            swal({
                title: "Alert!",
                text: "Apakah anda yakin akan menghapus Semua Gambar tersebut ?",
                type: 'error',
                showCancelButton: true,
                confirmButtonText: 'Ya, Saya yakin',
                cancelButtonText: 'Tidak',
            },function(result) {
                if(result){
                    document.getElementById('deleteData-'+id).submit();
                }
            });
        }
    </script>
@endsection

