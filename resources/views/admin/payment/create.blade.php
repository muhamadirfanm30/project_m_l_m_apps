@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                        
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3 class="card-title">
                                        Tambah Bank transfer
                                    </h3>
                                </div>
                                <div class="col-md-2">
                                    <a href="{{ url('admin/payment/detail') }}" class="btn btn-primary btn-sm" style="float: right">Back</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <form action="{{ url('admin/payment/store') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Nama Bank</label>
                                                <input type="text" name="nama_bank" class="form-control" value="{{Request::old('nama_bank')}}" placeholder="Nama Bank ...">
                                                @error('nama_bank')
                                                    <span style="color: red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Nama rekening Bank</label>
                                                <input type="text" name="nama_rekening" class="form-control" value="{{Request::old('nama_rekening')}}" placeholder="Nama rekening Bank ...">
                                                @error('nama_rekening')
                                                    <span style="color: red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Nomor rekening</label>
                                                <input type="text" name="nomor_rekening" class="form-control" value="{{Request::old('nomor_rekening')}}" placeholder="Nomor rekening ...">
                                                @error('nomor_rekening')
                                                    <span style="color: red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Photo</label>
                                                <input type="file" name="image" class="form-control" placeholder="Photo ...">
                                                @error('image')
                                                    <span style="color: red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Deskripsi</label>
                                                <textarea id="summernote" name="desc">{{Request::old('desc')}}</textarea>
                                                @error('desc')
                                                    <span style="color: red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary" style="float: right">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    
    <script>

        $(function () {
            // Summernote
            $('#summernote').summernote()
        })
    </script>
@endsection