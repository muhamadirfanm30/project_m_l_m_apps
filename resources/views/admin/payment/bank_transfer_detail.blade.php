@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('success') }}</strong>
                        </div>
                    @endif
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-8">
                                    <h3 class="card-title">
                                        Bank Transfer
                                    </h3>
                                </div>
                                <div class="col-md-4">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <a href="{{ url('admin/payment') }}" class="btn btn-danger btn-sm btn-block" style="float: right">Kembali</a>                                        </div>
                                        <div class="col-md-6">
                                            <a href="{{ url('admin/payment/create') }}" class="btn btn-primary btn-sm btn-block" style="float: right">Tambah Bank</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="col-md-12">
                                <table id="payment" class="display table table-hover table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Nama Bank</th>
                                            <th>Nomor Rekening</th>
                                            <th>Nama Rekening Pemilik</th>
                                            <th>Action</th>
                                        </tr>
                                        
                                    </thead>
                                    @foreach ($getPayment as $key => $pr)
                                        <tr>
                                            <th><img src="{{url('storage/bank-icon/'.$pr->image)}}" alt="Bank-icon" width=50px></th>
                                            <td>{{ $pr->nama_bank }}</td>
                                            <td>{{ $pr->nomor_rekening }}</td>
                                            <td>{{ $pr->nama_rekening }}</td>
                                            <td>
                                                <form action="{{ url('admin/payment/destroy/'.$pr->id) }}" method="POST">
                                                    @csrf
                                                    <a href="{{ url('admin/payment/update/'.$pr->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                                                    <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#payment').DataTable();
        } );
    </script>
@endsection