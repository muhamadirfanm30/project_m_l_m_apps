@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('success') }}</strong>
                        </div>
                    @endif
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3 class="card-title">
                                        Bank Transfer
                                    </h3>
                                </div>
                                <div class="col-md-2">
                                    {{-- <a href="{{ url('admin/payment/create') }}" class="btn btn-primary btn-sm" style="float: right">Tambah Bank</a> --}}
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="col-md-12">
                                <table id="payment" class="display table table-hover table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Nama</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        
                                    </thead>
                                    @foreach ($getMaster as $key => $pr)
                                        <tr>
                                            <td>{{ $pr->payment_name }}</td>
                                            <td>
                                                @if ($pr->is_active == 0)
                                                    <span class="badge badge-primary" style="color:white">Aktif</span>
                                                @else
                                                    <span class="badge badge-warning" style="color:white">Non Aktif</span>
                                                @endif
                                            </td>
                                            <td>
                                                <form action="" method="POST">
                                                    @csrf
                                                    @if ($pr->id == 1)
                                                        <a href="{{ route('payment.detail') }}" class="btn btn-success btn-sm"><i class="fa fa-eye"></i></a>
                                                    @endif
                                                    <a href="{{ url('/admin/payment/ms-payment-update/'.$pr->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#payment').DataTable();
        } );
    </script>
@endsection