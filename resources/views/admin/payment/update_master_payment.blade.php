@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                        
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3 class="card-title">
                                        Edit Master Payment
                                    </h3>
                                </div>
                                <div class="col-md-2">
                                    <a href="{{ url('admin/payment') }}" class="btn btn-primary btn-sm" style="float: right">Back</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <form action="{{ url('admin/payment/edit-ms-payment/'.$masterEdit->id) }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Nama</label>
                                                <input type="text" name="payment_name" class="form-control" value="{{$masterEdit->payment_name}}" placeholder="Nama ...">
                                                @error('nama_bank')
                                                    <span style="color: red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Status</label>
                                                <select class="form-control" name="is_active" id="exampleFormControlSelect1">
                                                    <option value="1" {{ $masterEdit->is_active == 1 ? 'selected' : '' }}>Tidak Aktif</option>
                                                    <option value="0" {{ $masterEdit->is_active == 0 ? 'selected' : '' }}>Aktif</option>
                                                </select>
                                            </div>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary" style="float: right">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    
    <script>

        $(function () {
            // Summernote
            $('#summernote').summernote()
        })
    </script>
@endsection