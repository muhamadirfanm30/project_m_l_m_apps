@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ session()->get('success') }}</strong>
                        </div>
                    @endif
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3 class="card-title">
                                        Show List Users
                                    </h3>
                                </div>
                                <div class="col-md-2">
                                    <a href="{{ url('admin/news-and-update/create') }}" class="btn btn-primary btn-sm" style="float: right">Tambah Berita</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="col-md-12">
                                <table id="userManagement" class="display table table-hover table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Judul</th>
                                            {{-- <th>Deskripsi</th> --}}
                                            {{-- <th>Url Vidio</th> --}}
                                            <th>Diterbitkan Tanggal</th>
                                            <th width="130px">Action</th>
                                        </tr>
                                        
                                    </thead>
                                    @foreach ($showListNews as $key => $user)
                                        <tr>
                                            <td>{{ $no++ }}</td>
                                            <td>{{ $user->judul }}</td>
                                            {{-- @if (strlen(strip_tags($user->deskripsi)) > 50)
                                                <td>{!! substr($user->deskripsi,0, 50) !!} <a href="{{ url('admin/news-and-update/detail/'.$user->id) }}">...Read More</a></td>
                                            @else
                                                <td>{!! $user->deskripsi !!}</td>
                                            @endif --}}

                                            {{-- @if (strlen(strip_tags($user->url_vidio)) > 50)
                                                <td>{!! substr($user->url_vidio,0, 50) !!} <a href="{{ url('admin/news-and-update/detail/'.$user->id) }}">...Read More</a></td>
                                            @else
                                                <td>{{ $user->url_vidio }}</td>
                                            @endif --}}
                                            <td>{{ date('d M Y', strtotime($user->publish_at)) }}</td>
                                            <td>
                                                <form action="{{ url('admin/news-and-update/destroy/'.$user->id) }}" method="POST" id="deleteData-{{$user->id}}">
                                                    @csrf
                                                </form>
                                                <a href="{{ url('admin/news-and-update/update/'.$user->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                                                <a href="{{ url('admin/news-and-update/detail/'.$user->id) }}" class="btn btn-success btn-sm"><i class="fa fa-eye"></i></a>
                                                <button type="submit" class="btn btn-danger btn-sm" onclick="deleteData({{$user->id}})"><i class="fa fa-trash"></i></button>
                                        </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#userManagement').DataTable();
        } );

        function deleteData(id){
            swal({
                title: "Alert!",
                text: "Apakah anda yakin akan menghapus data tersebut?",
                type: 'error',
                showCancelButton: true,
                confirmButtonText: 'Ya, Saya yakin',
                cancelButtonText: 'Tidak',
            },function(result) {
                if(result){
                    document.getElementById('deleteData-'+id).submit();
                }
            });
        }
    </script>
@endsection