@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-outline card-info">

                        <div class="card-header">
                            <h3 class="card-title">
                             News & Update
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="col-md-12">
                                <center><strong><h5>{{ $getData->judul }}</h5></strong></center>
                                <div class="row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-6">
                                        <div class="embed-responsive embed-responsive-16by9" style="width:auto">
                                            <iframe class="embed-responsive-item" src="{{ $getData->url_vidio }}" allowfullscreen></iframe>
                                        </div><br>
                                    </div>
                                    <div class="col-md-3"></div>
                                </div><br>
                                <h6 class="card-title">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-2"></div>
                                                <div class="col-md-8">
                                                    <center>
                                                        {!! $getData->deskripsi !!}
                                                    </center>
                                                </div>
                                                <div class="col-md-2"></div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </h6>
                            </div>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    {{-- modal --}}

    @include('customer.memberships._modalUpgradeMs')
@endsection