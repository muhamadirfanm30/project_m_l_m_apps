@extends('home')
@section('content')
    <div class="content-wrapper"><br>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-10">
                                    <h3 class="card-title">
                                        News And Update
                                    </h3>
                                </div>
                                <div class="col-md-2">
                                    <a href="{{ url('admin/news-and-update/show') }}" class="btn btn-primary btn-sm" style="float: right">Back</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <form action="{{ url('admin/news-and-update/store') }}" method="post">
                            @csrf
                            <div class="card-body">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Judul</label>
                                                <input type="text" name="judul" class="form-control" placeholder="Judul ..." value="{{ old('judul') }}">
                                                @error('judul')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Url Vidio</label>
                                                <input type="text" name="url_vidio" class="form-control" placeholder="Url Vidio ..." value="{{ old('url_vidio') }}">
                                                @error('url_vidio')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Deskripsi</label>
                                                <textarea id="summernote" name="deskripsi">{{ old('deskripsi') }}</textarea>
                                                @error('deskripsi')
                                                    <span style="color:red"><small>{{ $message }}</small></span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label>Publish At</label>
                                                <div class="input-group date" id="reservationdate" data-target-input="nearest">
                                                    <input type="text" name="publish_at" class="form-control datetimepicker-input" data-target="#reservationdate"/>
                                                    <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                    </div>
                                                    
                                                </div>
                                                @error('publish_at')
                                                        <span style="color:red"><small>{{ $message }}</small></span>
                                                    @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary" style="float: right">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#ststus-orders').DataTable();
        } );

        

        $(function () {
            // Summernote
            $('#summernote').summernote()

            //Date range picker
            $('#reservationdate').datetimepicker({
                format: 'YYYY-MM-DD'
            });
        })
    </script>
@endsection