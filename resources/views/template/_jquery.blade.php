@section('javascript')
<!-- jQuery -->
<script src="{{ asset('assets/plugins/jquery/jquery.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('assets/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
$.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- ChartJS -->
<script src="{{ asset('assets/plugins/chart.js/Chart.min.js') }}"></script>
<!-- bs-custom-file-input -->
<script src="{{ asset('assets/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ asset('assets/plugins/sparklines/sparkline.js') }}"></script>
<!-- JQVMap -->
<script src="{{ asset('assets/plugins/jqvmap/jquery.vmap.min.js') }}"></script>
<script src="{{ asset('assets/plugins/jqvmap/maps/jquery.vmap.usa.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('assets/plugins/jquery-knob/jquery.knob.min.js') }}"></script>
<!-- daterangepicker -->
<script src="{{ asset('assets/plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('assets/plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{ asset('assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
<!-- Summernote -->
<script src="{{ asset('assets/plugins/summernote/summernote-bs4.min.js') }}"></script>
<!-- overlayScrollbars -->
<script src="{{ asset('assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('assets/dist/js/adminlte.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('assets/dist/js/demo.js') }}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{ asset('assets/dist/js/pages/dashboard.js') }}"></script>

<script src="{{ asset('assets/dist/js/swal.js')}}"></script>
<script src="{{ asset('assets/dist/js/swal.min.js')}}"></script>

<!-- axios -->
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<!-- datatables -->
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css">
<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script>

<!-- Select2 -->
<script src="{{asset('assets/plugins/select2/js/select2.full.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/lodash.js') }}"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/css-loader/3.3.3/css-loader.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.4.0/bootbox.min.js"></script>
<script>
    $(document).ready(function() {
        $('.datatable').DataTable();
    } );
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<style>
    .bootbox-my-modal-style .modal-content{
        text-align: center;
        max-width: 300px;
        border: none;
    }

    .bootbox-close-button{
        color: #fff;
    }

    .bootbox-my-modal-style .modal-dialog{
        box-shadow: none;
    }

    .bootbox-my-modal-style .modal-footer{
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        height: 100%;
    }

    .btn-clus{
        color: #3f51b5 !important;
        background-color: #fff !important;
        border-color: #3f51b5 !important;
        border: 1px solid;
    }

    .btn-oke{
        color: #fff !important;
        background-color: #3f51b5 !important;
        border-color: #fff !important;
        border: 2px solid;
    }

    .bootbox-my-modal-style .bootbox-cancel{
        color: #fb483a !important;
        background-color: transparent !important;
        border-color: #fb483a !important;
        border: 2px solid;
    }

    .bootbox-my-modal-style .bootbox-accept{
        color: #fff !important;
        background-color: #fb483a !important;
        border-color: #fff !important;
        border: 2px solid;
    }

    .bootbox-my-modal-style .modal-header{
        background-color: #fb483a;
        border: 0;
        -webkit-box-shadow: 0 2px 5px 0 rgba(0,0,0,0.16), 0 2px 10px 0 rgba(0,0,0,0.12);
        box-shadow: 0 2px 5px 0 rgba(0,0,0,0.16), 0 2px 10px 0 rgba(0,0,0,0.12);
    }
</style>
<!-- Loader active -->
<div id="loading-indikator" class="loader loader-default"></div>
<script>
    (function($) {
        $.fn.inputFilter = function(inputFilter) {
            return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                } else {
                    this.value = "";
                }
            });
        };
    }(jQuery));
    var Helper = {
        apiUrl: function(url = '') {
            return '{{ url("/") }}' + url;
        },
        thousandsSeparators: function(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        },
        loadingStart: function() {
            $('#loading-indikator').addClass('is-active');
        },
        loadingStop: function() {
            $('#loading-indikator').removeClass('is-active');
        },
        onlyNumberInput: function(selector) {
            $(selector).inputFilter(function(value) {
                return /^\d*$/.test(value); // Allow digits only, using a RegExp
            });
            return this;
        },
        confirm: function(callbackAction, option = null) {
            if (option == null) {
                option = {
                    title: "Are You Sure?",
                    message: "You sure about this?",
                };
            }
            bootbox.confirm({
                title: "Are You Sure?",
                message: option.message,
                className: 'bootbox-my-modal-style',
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-times"></i> Cancel'
                    },
                    confirm: {
                        label: '<i class="fa fa-check"></i> Confirm'
                    }
                },
                callback: function(result) {
                    if (result) {
                        callbackAction();
                    }
                }
            });
        },
        toSlug: function(str, elem) {
            //replace all special characters | symbols with a space
            str = str.replace(/[`~!@#$%^&*()_\-+=\[\]{};:'"\\|\/,.<>?\s]/g, ' ').toLowerCase();
            // trim spaces at start and end of string
            str = str.replace(/^\s+|\s+$/gm, '');
            // replace space with dash/hyphen
            str = str.replace(/\s+/g, '-');
            $('#' + elem + '').val(str);
            //return str;
        },
    }

    function formatRupiah(bilangan){
            var	number_string = bilangan.toString(),
            sisa 	= number_string.length % 3,
            rupiah 	= number_string.substr(0, sisa),
            ribuan 	= number_string.substr(sisa).match(/\d{3}/g);
                
        if (ribuan) {
            separator = sisa ? ',' : '';
            rupiah += separator + ribuan.join(',');
        }

        return 'Rp. '+ rupiah;
    }

    function convertInteger(id){
        let total = document.getElementById(id).textContent;
        total = total.replace(/ |Rp |\- |\./gi, "")
        total = parseInt(total)
        return total;
    }

    function ajaxRequest(type, url, callbackSuccess = null) {
        $.ajax({
            url: url,
            type: type,
            success: function(response) {
                if (callbackSuccess != null) {
                    callbackSuccess(response);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });
    }

    function createSelect2(selector, data) {
        console.log('createSelect2', data)
        $(selector).select2({
            data: data
        });
    }

    function onlyNumber(element){
        $(element).keypress(function (e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        });
    }

    // function ajaxGetCostRajaOngkir(callbackSuccess = null) {
    //     costParam = {
    //         origin: $('#distrik_id').val(), // ID kota/kabupaten atau kecamatan asal
    //         originType: 'subdistrict', // Tipe origin: 'city' atau 'subdistrict'.
    //         weight: 1000, // Berat kiriman dalam gram
    //         courier: $('#kurir').val(),
    //     };

    //     $.ajax({
    //         url: 'http://localhost:8000/api/rajaongkir/cost',
    //         type: 'POST',
    //         data: costParam,
    //         success: function(response) {
    //             callbackSuccess(response);
    //         },
    //         error: function(jqXHR, textStatus, errorThrown) {
    //             console.log(textStatus, errorThrown);
    //         }
    //     });
    // }

    function ajaxGetCostRajaOngkir(callbackSuccess = null,distrikId = 'distrik_id',kurirId='kurir',weight=1000) {
        costParam = {
            origin: $('#'+distrikId).val(), // ID kota/kabupaten atau kecamatan asal
            originType: 'subdistrict', // Tipe origin: 'city' atau 'subdistrict'.
            weight: weight, // Berat kiriman dalam gram
            courier: $('#'+kurirId).val(),
        };
        $.ajax({
            url: '/api/rajaongkir/cost',
            type: 'POST',
            data: costParam,
            success: function(response) {
                callbackSuccess(response);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });
    }

    function loopingLayananRajaOngkir(rajaongkir) {
        costs = rajaongkir.results[0].costs; // ambil data pertama
        if (costs.length == 0) {
            // alert('data tidak ada');
            swal({
                title: "Ups!",
                text: "Layanan Tidak Tersedia!",
                type: 'error'
            });
            return [];
        }

        return $.map(costs, function(item) {
            return $.map(item.cost, function(c) {
                text = item.service + ' - (' + formatRupiah(c.value) + ') - ' + c.etd +' Hari';
                return {
                    text: text,
                    id: c.value
                }
            })
        });

    }

    // axios instance
    const Axios = axios.create({
        baseURL: Helper.apiUrl(),
        timeout: 8000,
        headers: {
            'X-Requested-With': 'XMLHttpRequest',
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function CountDownTimer(dt, id, end)
    {
        console.log(id);
        if(end !== ""){
            var end = new Date(end)
            var _second = 1000;
            var _minute = _second * 60;
            var _hour = _minute * 60;
            var _day = _hour * 24;
            var timer;
            timer = setInterval(showRemaining, 1000);
        }else{
            document.getElementById(id).innerHTML = '';
        }
        
        function showRemaining() {
            var now = new Date();
            var distance = end - now;
            if (distance < 0) {
                clearInterval(timer);
                document.getElementById(id).innerHTML = '<b>PROMO SUDAH BERAKHIR</b> ';
                return;
            }
            var Expired = 'Berahir Sampai';
            var days = Math.floor(distance / _day);
            var hours = Math.floor((distance % _day) / _hour);
            var minutes = Math.floor((distance % _hour) / _minute);
            var seconds = Math.floor((distance % _minute) / _second);

            document.getElementById(id).innerHTML ="";
            if(days > 0 ){
                document.getElementById(id).innerHTML = days + ' Hari, ';
            }
            document.getElementById(id).innerHTML += hours + ' Jam ';
            document.getElementById(id).innerHTML += minutes + ' Menit ';
            document.getElementById(id).innerHTML += seconds + ' Detik';
        }
    }

    
</script>
@show