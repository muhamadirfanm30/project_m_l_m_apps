<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
    </ul>

    <!-- Right navbar links -->
    <?php $total = 0 ?>
    @foreach((array) session('cart') as $id => $details)
        @if ($details > 0)
            <?php $total += $details['harga'] * $details['qty'] ?>
        @else
            <?php $total = 0 ?>
        @endif
    @endforeach
    <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#">
              <i class="far fa-user"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                    @if (Auth::user()->roles == 'Customer')
                        <a href="{{url('/customer/my-profile/show')}}" class="dropdown-item">
                    @else
                        <a href="{{url('/admin/my-profile/show')}}" class="dropdown-item">
                    @endif
                    <!-- Message Start -->
                    <div class="media">
                        <div class="image">
                            <div class="profile-userpic text-center">
                                <div class="avatar-upload">
                                    <div class="avatar-previews">
                                        @if(!empty(Auth::user()->avatar_user))
                                            <div id="imagePreview" style="background-image: url( {{url('storage/avatar/'.Auth::user()->avatar_user)}} )"></div>
                                        @else
                                            <div id="imagePreview" style="background-image: url( {{url('storage/avatar/default_avatar.jpg')}} )"></div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                            @if(!empty(Auth::user()->avatar_user))
                                <img src="{{url('storage/avatar/'.Auth::user()->avatar_user)}}" alt="User Avatar" class="img-circle mr-3" width="1px">
                            @else
                                <img src="{{url('storage/avatar/default_avatar.jpg')}}" alt="User Avatar" class="img-circle mr-3" width="1px">
                            @endif
                        <div class="media-body">
                            <h3 class="dropdown-item-title">
                                HI &nbsp; {{Auth::user()->first_name}}
                            </h3>
                            <p class="text-sm">{{!empty(Auth::user()->membership_id) ? Auth::user()->membership_id : 'Tidak Ada Member' }}</p>
                        </div>
                    </div>
                     {{-- <center>
                        <a href="">asda</a>
                     </center> --}}
                    <!-- Message End -->
                </a>
              <div class="dropdown-divider"></div>
            </div>
          </li>
        <li class="nav-item">
            <li class="nav-item dropdown">
                @if(Auth::user()->roles == 'Customer')
                    <a class="nav-link" data-toggle="dropdown" href="#">
                        <i class="fas fa-shopping-cart"></i>
                        <span class="badge badge-warning navbar-badge">{{ count((array) session('cart')) }}</span>
                    </a>
                @endif
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                    {{-- <span class="dropdown-item dropdown-header">{{ $total }}</span> --}}
                    @if(session('cart'))
                        @foreach(session('cart') as $id => $details)
                            <div class="dropdown-divider"></div>
                            <a href="#" class="dropdown-item">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <img class="img-thumbnail" src="{{ url('storage/product-image/'.$details['image_produk']) }}" />
                                            </div>
                                            <div class="col-md-8">
                                                <p>{{ $details['nama_produk'] }}</p>
                                                @if (!empty( $details['harga_promo']))
                                                    <span class="harga text-info"> Rp. {{ number_format($details['harga_promo']) }}</span><br> <span class="count"> Qty:{{ $details['qty'] }}</span>
                                                @else
                                                    <span class="harga text-info"> Rp. {{ number_format($details['harga']) }}</span><br> <span class="count"> Qty:{{ $details['qty'] }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            {{-- <div class="row cart-detail">
                                <div class="col-lg-4 col-sm-4 col-4 cart-detail-img">
                                    <img src="{{ $details['image_produk'] }}" />
                                </div>
                                <div class="col-lg-8 col-sm-8 col-8 cart-detail-product">
                                    <p>{{ $details['nama_produk'] }}</p>
                                    <span class="harga text-info"> ${{ $details['harga'] }}</span> <span class="count"> Quantity:{{ $details['qty'] }}</span>
                                </div>
                            </div> --}}
                        @endforeach
                    @endif
                    <div class="dropdown-divider"></div>
                    <a href="{{url('customer/order-produk/cart')}}" class="dropdown-item dropdown-footer">Checkout</a>
                </div>
            </li>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-slide="true" href="{{ route('logout') }}" role="button" onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                <i class="fas fa-sign-out-alt"></i>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </li>
    </ul>
</nav>