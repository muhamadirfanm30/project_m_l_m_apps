<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <!-- <a href="index3.html" class="brand-link">
        <img src="dist/img/AdminLTELogo.png" alt="" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">AdminLTE 3</span>
    </a> -->

    <!-- Sidebar -->
    <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
            <div class="profile-userpic text-center">
                <div class="avatar-upload">
                    <div class="avatar-previews">
                        @if(!empty(Auth::user()->avatar_user))
                            <div id="imagePreview" style="background-image: url( {{url('storage/avatar/'.Auth::user()->avatar_user)}} )"></div>
                        @else
                            <div id="imagePreview" style="background-image: url( {{url('storage/avatar/default_avatar.jpg')}} )"></div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="info">
            @if (Auth::user()->roles == 'Customer')
            <a href="{{url('/customer/my-profile/show')}}" class="d-block"> {{Auth::user()->first_name}}</a>
        @else
            <a href="{{url('/admin/my-profile/show')}}" class="d-block"> {{Auth::user()->first_name}}</a>
        @endif
        </div>
    </div>
        {{-- <div class="form-inline">
            <div class="input-group" data-widget="sidebar-search">
                @if(!empty(Auth::user()->membership_id))
                    <strong  style="color:white; font-size:12px">
                        <p align="center">{{ Auth::user()->membersip_status }}  {{Auth::user()->membership_id}}</p>
                    </strong>
                @endif
            </div>
          </div> --}}
        
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
                with font-awesome or any other icon font library -->
            @if (Auth::user()->roles == 'Customer')
                @php
                    $menuOpen = '';
                    $arrMenu = ['landing-pages',
                                'konten',
                                'starter-kit-download'
                            ];
                    if(in_array(Request::segment(2),$arrMenu)){
                        $menuOpen = 'menu-open';
                    } 
                @endphp
                <li class="nav-item">
                    <a href="{{ url('/customer/dasboard') }}" class="nav-link {{Request::segment(2) == 'dashboard' ? 'active' : ''}}">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ url('/customer/member/show') }}" class="nav-link {{Request::segment(2) == 'member' ? 'active' : ''}}">
                        <i class="nav-icon fas fa-star"></i>
                        <p>
                            Memberships
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ url('/customer/news-update/show') }}" class="nav-link {{Request::segment(2) == 'news-update' ? 'active' : ''}}">
                        <i class="nav-icon fas fa-bell"></i>
                        <p>
                            News & Updates
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ url('/customer/my-team/show') }}" class="nav-link {{Request::segment(2) == 'my-team' ? 'active' : ''}}">
                        <i class="nav-icon fas fa-users"></i>
                        <p>
                            My Superteam
                        </p>
                    </a>
                </li>
                
                
                <li class="nav-header">Training Center</li>
                <li class="nav-item">
                    <a href="{{ url('/customer/sales-training/show') }}" class="nav-link {{Request::segment(2) == 'sales-training' ? 'active' : ''}}">
                        <i class="nav-icon fas fa-chart-line"></i>
                        <p>
                            Sales Training
                        </p>
                    </a>
                </li>
                <li class="nav-item {{$menuOpen}}">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Tools
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ url('customer/landing-pages/show') }}" class="nav-link {{Request::segment(2) == 'landing-pages' ? 'active' : ''}}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Landing Pages</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('customer/konten') }}" class="nav-link {{Request::segment(2) == 'konten' ? 'active' : ''}}">
                                <i class="far fa-image nav-icon"></i>
                                <p>Konten Produk & Bisnis</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('customer/starter-kit-download/') }}" class="nav-link {{Request::segment(2) == 'starter-kit-download' ? 'active' : ''}}">
                                <i class="far fa-file-alt nav-icon"></i>
                                <p>Starter kit Download</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="{{ url('/customer/live-training/show') }}" class="nav-link {{Request::segment(2) == 'live-training' ? 'active' : ''}}">
                        <i class="nav-icon far fa-calendar-alt"></i>
                        <p>
                            Live Training
                        </p>
                    </a>
                </li>

                <li class="nav-header">Shop & Orders</li>
                    <li class="nav-item">
                        <a href="{{ url('customer/new-promo/show') }}" class="nav-link {{Request::segment(2) == 'new-promo' ? 'active' : ''}}">
                            <i class="nav-icon fas fa-clock"></i>
                            <p>
                                Promo Terbaru
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('customer/order-produk/show') }}" class="nav-link {{Request::segment(2) == 'order-produk' ? 'active' : ''}}">
                            <i class="nav-icon fas fa-shopping-cart"></i>
                            <p>
                                Order Produk
                            </p>
                        </a>
                    </li>
                    @if (Auth::user()->is_upgrade_ms != 1)
                        <li class="nav-item">
                            <a href="{{ url('customer/paket-ms-upgrade/show') }}" class="nav-link {{Request::segment(2) == 'paket-ms-upgrade' ? 'active' : ''}}">
                                <i class="nav-icon fas fa-cubes"></i>
                                <p>
                                    Paket Upgrade MS
                                </p>
                            </a>
                        </li>
                    @endif
                    <li class="nav-item">
                        <a href="{{ url('customer/status-order/show') }}" class="nav-link {{Request::segment(2) == 'status-order' ? 'active' : ''}}">
                            <i class="nav-icon fas fa-shopping-bag"></i>
                            <p>
                                Status Order
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('customer/myproduct') }}" class="nav-link {{Request::segment(2) == 'myproduct' ? 'active' : ''}}">
                            <i class="nav-icon fas fa-box"></i>
                            <p>
                                Stok Produk
                            </p>
                        </a>
                    </li>

                @if (Auth::user()->is_upgrade_ms == 1)
                    <li class="nav-header">Menu Khusus Member MS</li>
                    <li class="nav-item">
                        <a href="{{ url('customer/menu-mobile-stokiest/show') }}" class="nav-link {{Request::segment(2) == 'menu-mobile-stokiest' ? 'active' : ''}}">
                            <i class="nav-icon fas fa-cog"></i>
                            <p>
                                Penjelasan Menu Baru MS
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('customer/bonus-member-ms/show') }}" class="nav-link {{Request::segment(2) == 'bonus-member-ms' ? 'active' : ''}}">
                            <i class="nav-icon fas fa-dollar-sign"></i>
                            <p>
                                Bonus Member MS
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('customer/input-reguler-member/show') }}" class="nav-link {{Request::segment(2) == 'input-reguler-member' ? 'active' : ''}}">
                            <i class="nav-icon fas fa-user-friends"></i>
                            <p>
                                Pendaftaran Member Baru
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('customer/list-team-upgrade-ms/show') }}" class="nav-link {{Request::segment(2) == 'list-team-upgrade-ms' ? 'active' : ''}}">
                            <i class="nav-icon fas fa-users"></i>
                            <p>
                                Daftar Calon MS
                            </p>
                        </a>
                    </li>

                   <!--  <li class="nav-header">Panel HWI</li>
                <li class="nav-item">
                    <a href="{{ url('customer/abot-panel-hwi/show') }}" class="nav-link {{Request::segment(2) == 'abot-panel-hwi' ? 'active' : ''}}">
                        <i class="nav-icon far fa-calendar-alt"></i>
                        <small>
                            Penjelasan Panel HWI
                        </small>
                    </a>
                </li>-->
                
                @php
                    $url_mitra_usaha = DB::table('general_settings')->where('id', 11)->first();
                    $url_ms = DB::table('general_settings')->where('id', 12)->first();
                @endphp
                <!-- <li class="nav-item">
                    <a href="{{ $url_mitra_usaha->value }}" class="nav-link" target="_blank" rel="noopener noreferrer">
                        <i class="nav-icon far fa-calendar-alt"></i>
                        <small>
                            Login Dahboard Mitra usaha
                        </small>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ $url_ms->value }}" class="nav-link" target="_blank" rel="noopener noreferrer">
                        <i class="nav-icon far fa-calendar-alt"></i>
                        <small>
                            Login Dashboard Mobile Stokiest
                        </small>
                    </a>
                </li> -->
                
                <li class="nav-item {{$menuOpen}}">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Panel HWI
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="{{ url('customer/abot-panel-hwi/show') }}" class="nav-link {{Request::segment(2) == 'abot-panel-hwi' ? 'active' : ''}}">
                        <i class="nav-icon far fa-calendar-alt"></i>
                        <small>
                            Penjelasan Panel HWI
                        </small>
                    </a>
					<li class="nav-item">
                    <a href="{{ $url_mitra_usaha->value }}" class="nav-link" target="_blank" rel="noopener noreferrer">
                        <i class="nav-icon far fa-calendar-alt"></i>
                        <small>
                            Login Dahboard Mitra usaha
                        </small>
                    </a>
                </li>
				<li class="nav-item">
                    <a href="{{ $url_ms->value }}" class="nav-link" target="_blank" rel="noopener noreferrer">
                        <i class="nav-icon far fa-calendar-alt"></i>
                        <small>
                            Login Dashboard Mobile Stokiest
                        </small>
                    </a>
                </li>
                </li>
                       </ul>
                </li>
                @endif

                


                <li class="nav-header">Syarat & Ketentuan</li>
                <li class="nav-item">
                    <a href="{{ url('/customer/syarat-ketentuan/show') }}" class="nav-link {{Request::segment(2) == 'syarat-ketentuan' ? 'active' : ''}}">
                        <i class="nav-icon fas fa-chart-line"></i>
                        <p>
                            Syarat & Ketentuan
                        </p>
                    </a>
                </li>
            @elseif(Auth::user()->roles == 'admin')
                <li class="nav-item">
                    <a href="{{ url('/admin/dashboard') }}" class="nav-link {{Request::segment(2) == '' ? 'active' : ''}}">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                </li> 
                <li class="nav-item">
                    <a href="{{ url('admin/email-template/show') }}" class="nav-link {{Request::segment(2) == 'email-template' ? 'active' : ''}}">
                        <i class="nav-icon fas fa-envelope"></i>
                        <p>
                            Email Template
                        </p>
                    </a>
                </li> 
                <li class="nav-item">
                    <a href="{{ url('admin/whatsapp-template/show') }}" class="nav-link {{Request::segment(2) == 'whatsapp-template' ? 'active' : ''}}">
                        <i class="nav-icon fab fa-whatsapp"></i>
                        <p>
                            WhatsApp Template
                        </p>
                    </a>
                </li> 
                <li class="nav-item">
                    <a href="{{ url('admin/landing-page/show') }}" class="nav-link {{Request::segment(2) == 'landing-page' ? 'active' : ''}}">
                        <i class="nav-icon fas fa-plane-arrival"></i>
                        <p>
                            Landing Pages
                        </p>
                    </a>
                </li> 
                <!-- <li class="nav-item">
                    <a href="{{ url('admin/slide-show/show') }}" class="nav-link {{Request::segment(2) == 'slide-show' ? 'active' : ''}}">
                        <i class="nav-icon fas fa-sliders-h"></i>
                        <p>
                            Slide Show Landing Pages
                        </p>
                    </a>
                </li> -->
                <li class="nav-item">
                    <a href="{{ url('admin/product/show') }}" class="nav-link {{Request::segment(2) == 'product' ? 'active' : ''}}">
                        <i class="nav-icon fas fa-boxes"></i>
                        <p>Product</p>
                    </a>
                </li>
                @php
                    $menuFaq = '';
                    $arrMenu = ['term-and-conditions', 'faq-kategori'];
                    if(in_array(Request::segment(2),$arrMenu)){
                        $menuFaq = 'menu-open';
                    } 
                @endphp
                <li class="nav-item {{$menuFaq}}">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-exclamation-triangle"></i>
                        <p>
                            FAQ
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ url('admin/faq-kategori/show') }}" class="nav-link {{Request::segment(2) == 'faq-kategori' ? 'active' : ''}}">
                                <i class="{{Request::segment(2) == 'faq-kategori' ? 'far fa-check-circle' : 'far fa-circle'}}  nav-icon"></i>
                                <p>Kategori S&K</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('admin/term-and-conditions/show') }}" class="nav-link {{Request::segment(2) == 'term-and-conditions' ? 'active' : ''}}">
                                <i class="{{Request::segment(2) == 'term-and-conditions' ? 'far fa-check-circle' : 'far fa-circle'}}  nav-icon"></i>
                                <p>Detail S&K</p>
                            </a>
                        </li>
                    </ul>
                </li> 
                @php
                    $menuTransaksi = '';
                    $arrMenu = ['data-transactions',
                                'update-resi',
                                'transactions'
                            ];
                    if(in_array(Request::segment(2),$arrMenu)){
                        $menuTransaksi = 'menu-open';
                    } 
                @endphp
                <li class="nav-item {{$menuTransaksi}}">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-money-bill-alt"></i>
                        <p>
                            Data Transaksi  
                            <i class="fas fa-angle-left right"></i>
                            {{-- <span class="badge badge-warning right" style="color:#fff;">{{ $status_text_info }}</span> --}}
                            <span class="badge badge-warning right" style="color:#fff;">{{ $status_text_info }}</span>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ url('admin/data-transactions/show') }}" class="nav-link {{Request::segment(2) == 'data-transactions' ? 'active' : ''}}">
                                <i class="{{Request::segment(2) == 'data-transactions' ? 'far fa-check-circle' : 'far fa-circle'}}  nav-icon"></i>
                                <p>
                                    Data Transaksi 
                                    <span class="right badge badge-danger">{{ $count_data_transaksi_from_admin_not_read > 0 ? $count_data_transaksi_from_admin_not_read : '' }}</span>
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('admin/update-resi/show') }}" class="nav-link {{Request::segment(2) == 'update-resi' ? 'active' : ''}}">
                                <i class="{{Request::segment(2) == 'update-resi' ? 'far fa-check-circle' : 'far fa-circle'}}  nav-icon"></i>
                                <p>
                                    Update Resi Pesanan
                                    <span class="right badge badge-danger">{{ $count_update_resi_pengiriman_from_admin_not_read > 0 ? $count_update_resi_pengiriman_from_admin_not_read : '' }}</span>
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('admin/transactions/show') }}" class="nav-link {{Request::segment(2) == 'transactions' ? 'active' : ''}}">
                                <i class="{{Request::segment(2) == 'transactions' ? 'far fa-check-circle' : 'far fa-circle'}}  nav-icon"></i>
                                <p>
                                    Konfirmasi Pembayaran 
                                    <span class="right badge badge-danger">{{ $count_konfirmasi_pembayaran_from_admin_not_read > 0 ? $count_konfirmasi_pembayaran_from_admin_not_read : '' }}</span>
                                </p>
                            </a>
                        </li>
                    </ul>
                </li> 
                @php
                    $menuMaster = '';
                    $arrMenu = ['news-and-update',
                                'sales-and-training',
                                'events',
                                'mobile-stokiest',
                                'bonus-mobile-stokiest',
                                'daftar-paket-reguler',
                                'kategori',
                                'sub-kategori',
                                'konten',
                                'starterkit-downloads',
                                'paket-upgrade-ms',
                                'payment',
                                'kategori-produk',
                                'abot-panel-hwi'
                            ];
                    if(in_array(Request::segment(2),$arrMenu)){
                        $menuMaster = 'menu-open';
                    } 
                @endphp
                <li class="nav-item {{$menuMaster}}">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-asterisk"></i>
                        <p>
                            Master Data 
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ url('admin/news-and-update/show') }}" class="nav-link {{Request::segment(2) == 'news-and-update' ? 'active' : ''}}">
                                <i class="{{Request::segment(2) == 'news-and-update' ? 'far fa-check-circle' : 'far fa-circle'}}  nav-icon"></i>
                                <p>News And Update</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('admin/starterkit-downloads') }}" class="nav-link {{Request::segment(2) == 'starterkit-downloads' ? 'active' : ''}}">
                                <i class="{{Request::segment(2) == 'starterkit-downloads' ? 'far fa-check-circle' : 'far fa-circle'}}  nav-icon"></i>
                                <p>Starterkit Download</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('admin/sales-and-training/show') }}" class="nav-link {{Request::segment(2) == 'sales-and-training' ? 'active' : ''}}">
                                <i class="{{Request::segment(2) == 'sales-and-training' ? 'far fa-check-circle' : 'far fa-circle'}}  nav-icon"></i>
                                <p>Sales Training</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('admin/events/show') }}" class="nav-link {{Request::segment(2) == 'events' ? 'active' : ''}}">
                                <i class="{{Request::segment(2) == 'events' ? 'far fa-check-circle' : 'far fa-circle'}}  nav-icon"></i>
                                <p>Events & Live Training</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('admin/mobile-stokiest/show') }}" class="nav-link {{Request::segment(2) == 'mobile-stokiest' ? 'active' : ''}}">
                                <i class="{{Request::segment(2) == 'mobile-stokiest' ? 'far fa-check-circle' : 'far fa-circle'}}  nav-icon"></i>
                                <p>Penjelasan Menu MS</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('admin/bonus-mobile-stokiest/show') }}" class="nav-link {{Request::segment(2) == 'bonus-mobile-stokiest' ? 'active' : ''}}">
                                <i class="{{Request::segment(2) == 'bonus-mobile-stokiest' ? 'far fa-check-circle' : 'far fa-circle'}}  nav-icon"></i>
                                <p>Bonus Member MS</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('admin/daftar-paket-reguler/show') }}" class="nav-link {{Request::segment(2) == 'daftar-paket-reguler' ? 'active' : ''}}">
                                <i class="{{Request::segment(2) == 'daftar-paket-reguler' ? 'far fa-check-circle' : 'far fa-circle'}}  nav-icon"></i>
                                <p>Paket Reguler</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('admin/paket-upgrade-ms/show') }}" class="nav-link {{Request::segment(2) == 'paket-upgrade-ms' ? 'active' : ''}}">
                                <i class="{{Request::segment(2) == 'paket-upgrade-ms' ? 'far fa-check-circle' : 'far fa-circle'}}  nav-icon"></i>
                                <p>Paket Upgrade MS</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('kategori') }}" class="nav-link {{ Route::currentRouteName() == 'kategori' ? 'active' : ''}}">
                                <i class="{{Request::segment(2) == 'kategori' ? 'far fa-check-circle' : 'far fa-circle'}}  nav-icon"></i>
                                <p>Kategori Konten</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('sub-kategori') }}" class="nav-link {{ Route::currentRouteName() == 'sub-kategori' ? 'active' : ''}}">
                                <i class="{{Request::segment(2) == 'sub-kategori' ? 'far fa-check-circle' : 'far fa-circle'}}  nav-icon"></i>
                                <p>Sub Kategori Konten</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('konten') }}" class="nav-link {{ Route::currentRouteName() == 'konten' ? 'active' : ''}}">
                                <i class="{{Request::segment(2) == 'konten' ? 'far fa-check-circle' : 'far fa-circle'}}  nav-icon"></i>
                                <p>Detail Konten</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('admin/payment') }}" class="nav-link {{ Request::segment(2) == 'payment' ? 'active' : ''}}">
                                <i class="{{Request::segment(2) == 'payment' ? 'far fa-check-circle' : 'far fa-circle'}}  nav-icon"></i>
                                <p>Metode Pembayaran</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('admin/courier/show') }}" class="nav-link {{ Request::segment(2) == 'courier' ? 'active' : ''}}">
                                <i class="{{Request::segment(2) == 'courier' ? 'far fa-check-circle' : 'far fa-circle'}}  nav-icon"></i>
                                <p>Kurir</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('admin/kategori-produk') }}" class="nav-link {{ Request::segment(2) == 'kategori-produk' ? 'active' : ''}}">
                                <i class="{{Request::segment(2) == 'kategori-produk' ? 'far fa-check-circle' : 'far fa-circle'}}  nav-icon"></i>
                                <p>Kategori Produk</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('admin/panel-hwi/show') }}" class="nav-link {{ Request::segment(2) == 'panel-hwi' ? 'active' : ''}}">
                                <i class="{{Request::segment(2) == 'panel-hwi' ? 'far fa-check-circle' : 'far fa-circle'}}  nav-icon"></i>
                                <p>Penjelasan HWI</p>
                            </a>
                        </li>
                    </ul>
                </li>  
                @php
                    $menuUsermanagement = '';
                    $arrMenu = ['user',
                                'new-member-request',
                                'new-team-upgrade-ms',
                                'new-ms-upgrade-request'
                            ];
                    if(in_array(Request::segment(2),$arrMenu)){
                        $menuUsermanagement = 'menu-open';
                    } 
                @endphp
                <li class="nav-item {{$menuUsermanagement}}">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-users-cog"></i>
                        <p>
                            User Management
                            <i class="fas fa-angle-left right"></i>
                            <span class="badge badge-info right">{{ $status_text_info_user_menu }}</span>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ url('admin/list-admin/show') }}" class="nav-link {{Request::segment(2) == 'list-admin' ? 'active' : ''}}">
                                 <i class="{{Request::segment(2) == 'list-admin' ? 'far fa-check-circle' : 'far fa-circle'}}  nav-icon"></i>
                                <p>Admin List</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('admin/user/show') }}" class="nav-link {{Request::segment(2) == 'user' ? 'active' : ''}}">
                                 <i class="{{Request::segment(2) == 'user' ? 'far fa-check-circle' : 'far fa-circle'}}  nav-icon"></i>
                                <p>Member List</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('admin/new-member-request/show') }}" class="nav-link {{Request::segment(2) == 'new-member-request' ? 'active' : ''}}">
                                 <i class="{{Request::segment(2) == 'new-member-request' ? 'far fa-check-circle' : 'far fa-circle'}}  nav-icon"></i>
                                <p>New Member Request <span class="right badge badge-danger">{{ $count_new_member_request_from_admin_not_read > 0 ? $count_new_member_request_from_admin_not_read : '' }}</span></p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('admin/new-team-upgrade-ms/show') }}" class="nav-link {{Request::segment(2) == 'new-team-upgrade-ms' ? 'active' : ''}}">
                                 <i class="{{Request::segment(2) == 'new-team-upgrade-ms' ? 'far fa-check-circle' : 'far fa-circle'}}  nav-icon"></i>
                                <p>New Team MS Upgrade <span class="right badge badge-danger">{{ $count_myteam_upgrade_ms_from_admin_not_read > 0 ? $count_myteam_upgrade_ms_from_admin_not_read : '' }}</span></p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('admin/new-ms-upgrade-request/show') }}" class="nav-link {{Request::segment(2) == 'new-ms-upgrade-request' ? 'active' : ''}}">
                                 <i class="{{Request::segment(2) == 'new-ms-upgrade-request' ? 'far fa-check-circle' : 'far fa-circle'}}  nav-icon"></i>
                                <p>New MS Upgrade Request <span class="right badge badge-danger">{{ $count_leader_upgrade_ms_from_admin_not_read > 0 ? $count_leader_upgrade_ms_from_admin_not_read : '' }}</span></p>
                            </a>
                        </li>
                    </ul>
                </li>     
                @php
                    $menuReports = '';
                    $arrMenu = ['member-performence','data'];
                    $report = ['member','product','omset'];
                    if(in_array(Request::segment(2),$arrMenu)){
                        $menuReports = 'menu-open';
                    }elseif(in_array(Request::segment(3),$report)){
                        $menuReports = 'menu-open';
                    }
                @endphp
                <li class="nav-item {{$menuReports}}">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-file-archive"></i>
                        <p>
                            Laporan Performa
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ url('admin/member-performence/') }}" class="nav-link {{Request::segment(2) == 'member-performence' ? 'active' : ''}}">
                                 <i class="{{Request::segment(2) == 'member-performence' ? 'far fa-check-circle' : 'far fa-circle'}}  nav-icon"></i>
                                <p>Performa Anggota</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('admin/report/member/Monthly') }}" class="nav-link {{Request::segment(3) == 'member' ? 'active' : ''}}">
                                 <i class="{{Request::segment(3) == 'member' ? 'far fa-check-circle' : 'far fa-circle'}}  nav-icon"></i>
                                <p>Total Anggota</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('admin/report/product/Monthly') }}" class="nav-link {{Request::segment(3) == 'product' ? 'active' : ''}}">
                                 <i class="{{Request::segment(3) == 'product' ? 'far fa-check-circle' : 'far fa-circle'}}  nav-icon"></i>
                                <p>Total Penjualan</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('admin/report/omset/Monthly') }}" class="nav-link {{Request::segment(3) == 'omset' ? 'active' : ''}}">
                                 <i class="{{Request::segment(3) == 'omset' ? 'far fa-check-circle' : 'far fa-circle'}}  nav-icon"></i>
                                <p>Total Omset</p>
                            </a>
                        </li>
                    </ul>
                </li> 

                @php
                    $general_setting = '';
                    $arrMenu = ['konten-dashboard','general-setting'];
                    if(in_array(Request::segment(2),$arrMenu)){
                        $general_setting = 'menu-open';
                    }
                @endphp
                <li class="nav-item {{$general_setting}}">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-cogs"></i>
                        <p>
                            Pengaturan 
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ url('admin/konten-dashboard') }}" class="nav-link {{Request::segment(2) == 'konten-dashboard' ? 'active' : ''}}">
                                <i class="{{Request::segment(2) == 'konten-dashboard' ? 'far fa-check-circle' : 'far fa-circle'}}  nav-icon"></i>
                                <p>Pengaturan Dashboard</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('admin/general-setting/show') }}" class="nav-link {{Request::segment(2) == 'general-setting' ? 'active' : ''}}">
                                <i class="{{Request::segment(2) == 'general-setting' ? 'far fa-check-circle' : 'far fa-circle'}}  nav-icon"></i>
                                <p>Pengaturan Global</p>
                            </a>
                        </li>
                    </ul>
                </li> 
            @elseif(Auth::user()->roles == 'Sub Admin') 
                <li class="nav-header">Data Transaction</li>
                <li class="nav-item">
                    <a href="{{ url('admin/data-transactions/show') }}" class="nav-link {{Request::segment(2) == 'data-transactions' ? 'active' : ''}}">
                        <i class="{{Request::segment(2) == 'data-transactions' ? 'far fa-check-circle' : 'far fa-circle'}}  nav-icon"></i>
                        <p>
                            Data Transaksi 
                            <span class="right badge badge-danger">{{ $count_data_transaksi_from_admin_not_read > 0 ? $count_data_transaksi_from_admin_not_read : '' }}</span>
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('admin/update-resi/show') }}" class="nav-link {{Request::segment(2) == 'update-resi' ? 'active' : ''}}">
                        <i class="{{Request::segment(2) == 'update-resi' ? 'far fa-check-circle' : 'far fa-circle'}}  nav-icon"></i>
                        <p>
                            Update Resi Pesanan
                            <span class="right badge badge-danger">{{ $count_update_resi_pengiriman_from_admin_not_read > 0 ? $count_update_resi_pengiriman_from_admin_not_read : '' }}</span>
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('admin/transactions/show') }}" class="nav-link {{Request::segment(2) == 'transactions' ? 'active' : ''}}">
                        <i class="{{Request::segment(2) == 'transactions' ? 'far fa-check-circle' : 'far fa-circle'}}  nav-icon"></i>
                        <p>
                            Konfirmasi Pembayaran 
                            <span class="right badge badge-danger">{{ $count_konfirmasi_pembayaran_from_admin_not_read > 0 ? $count_konfirmasi_pembayaran_from_admin_not_read : '' }}</span>
                        </p>
                    </a>
                </li>

                <li class="nav-header">User Management</li> 
                <li class="nav-item">
                    <a href="{{ url('admin/list-admin/show') }}" class="nav-link {{Request::segment(2) == 'list-admin' ? 'active' : ''}}">
                        <i class="nav-icon fas fa-user-lock"></i>
                        <p>
                            Admin List
                        </p>
                    </a>
                </li> 
                <li class="nav-item">
                    <a href="{{ url('admin/user/show') }}" class="nav-link {{Request::segment(2) == 'user' ? 'active' : ''}}">
                        <i class="nav-icon fas fa-users-cog"></i>
                        <p>
                            Member List
                        </p>
                    </a>
                </li> 
                <li class="nav-item">
                    <a href="{{ url('admin/new-member-request/show') }}" class="nav-link {{Request::segment(2) == 'new-member-request' ? 'active' : ''}}">
                         <i class="{{Request::segment(2) == 'new-member-request' ? 'far fa-check-circle' : 'far fa-circle'}}  nav-icon"></i>
                        <p>New Member Request <span class="right badge badge-danger">{{ $count_new_member_request_from_admin_not_read > 0 ? $count_new_member_request_from_admin_not_read : '' }}</span></p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('admin/new-team-upgrade-ms/show') }}" class="nav-link {{Request::segment(2) == 'new-team-upgrade-ms' ? 'active' : ''}}">
                         <i class="{{Request::segment(2) == 'new-team-upgrade-ms' ? 'far fa-check-circle' : 'far fa-circle'}}  nav-icon"></i>
                        <p>New Team MS Upgrade <span class="right badge badge-danger">{{ $count_myteam_upgrade_ms_from_admin_not_read > 0 ? $count_myteam_upgrade_ms_from_admin_not_read : '' }}</span></p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('admin/new-ms-upgrade-request/show') }}" class="nav-link {{Request::segment(2) == 'new-ms-upgrade-request' ? 'active' : ''}}">
                         <i class="{{Request::segment(2) == 'new-ms-upgrade-request' ? 'far fa-check-circle' : 'far fa-circle'}}  nav-icon"></i>
                        <p>New MS Upgrade Request <span class="right badge badge-danger">{{ $count_leader_upgrade_ms_from_admin_not_read > 0 ? $count_leader_upgrade_ms_from_admin_not_read : '' }}</span></p>
                    </a>
                </li>            
            @elseif(Auth::user()->roles == 'Admin Produk')   
                <li class="nav-header">Data Transaction</li>
                

                <li class="nav-item">
                    <a href="{{ url('admin/data-transactions/show') }}" class="nav-link {{Request::segment(2) == 'data-transactions' ? 'active' : ''}}">
                        <i class="{{Request::segment(2) == 'data-transactions' ? 'far fa-check-circle' : 'far fa-circle'}}  nav-icon"></i>
                        <p>
                            Data Transaksi 
                            <span class="right badge badge-danger">{{ $count_data_transaksi_from_admin_not_read > 0 ? $count_data_transaksi_from_admin_not_read : '' }}</span>
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('admin/update-resi/show') }}" class="nav-link {{Request::segment(2) == 'update-resi' ? 'active' : ''}}">
                        <i class="{{Request::segment(2) == 'update-resi' ? 'far fa-check-circle' : 'far fa-circle'}}  nav-icon"></i>
                        <p>
                            Update Resi Pesanan
                            <span class="right badge badge-danger">{{ $count_update_resi_pengiriman_from_admin_not_read > 0 ? $count_update_resi_pengiriman_from_admin_not_read : '' }}</span>
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('admin/transactions/show') }}" class="nav-link {{Request::segment(2) == 'transactions' ? 'active' : ''}}">
                        <i class="{{Request::segment(2) == 'transactions' ? 'far fa-check-circle' : 'far fa-circle'}}  nav-icon"></i>
                        <p>
                            Konfirmasi Pembayaran 
                            <span class="right badge badge-danger">{{ $count_konfirmasi_pembayaran_from_admin_not_read > 0 ? $count_konfirmasi_pembayaran_from_admin_not_read : '' }}</span>
                        </p>
                    </a>
                </li>
                            
            @elseif(Auth::user()->roles == 'Admin Member')
                <li class="nav-header">User Management</li> 
                <li class="nav-item">
                    <a href="{{ url('admin/list-admin/show') }}" class="nav-link {{Request::segment(2) == 'list-admin' ? 'active' : ''}}">
                        <i class="nav-icon fas fa-user-lock"></i>
                        <p>
                            Admin List
                        </p>
                    </a>
                </li> 
                <li class="nav-item">
                    <a href="{{ url('admin/user/show') }}" class="nav-link {{Request::segment(2) == 'user' ? 'active' : ''}}">
                        <i class="nav-icon fas fa-users-cog"></i>
                        <p>
                            Member List
                        </p>
                    </a>
                </li>  

                <li class="nav-item">
                    <a href="{{ url('admin/new-member-request/show') }}" class="nav-link {{Request::segment(2) == 'new-member-request' ? 'active' : ''}}">
                         <i class="{{Request::segment(2) == 'new-member-request' ? 'far fa-check-circle' : 'far fa-circle'}}  nav-icon"></i>
                        <p>New Member Request <span class="right badge badge-danger">{{ $count_new_member_request_from_admin_not_read > 0 ? $count_new_member_request_from_admin_not_read : '' }}</span></p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('admin/new-team-upgrade-ms/show') }}" class="nav-link {{Request::segment(2) == 'new-team-upgrade-ms' ? 'active' : ''}}">
                         <i class="{{Request::segment(2) == 'new-team-upgrade-ms' ? 'far fa-check-circle' : 'far fa-circle'}}  nav-icon"></i>
                        <p>New Team MS Upgrade <span class="right badge badge-danger">{{ $count_myteam_upgrade_ms_from_admin_not_read > 0 ? $count_myteam_upgrade_ms_from_admin_not_read : '' }}</span></p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('admin/new-ms-upgrade-request/show') }}" class="nav-link {{Request::segment(2) == 'new-ms-upgrade-request' ? 'active' : ''}}">
                         <i class="{{Request::segment(2) == 'new-ms-upgrade-request' ? 'far fa-check-circle' : 'far fa-circle'}}  nav-icon"></i>
                        <p>New MS Upgrade Request <span class="right badge badge-danger">{{ $count_leader_upgrade_ms_from_admin_not_read > 0 ? $count_leader_upgrade_ms_from_admin_not_read : '' }}</span></p>
                    </a>
                </li>
            @endif
            
        </nav>
    <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
    <style>
        <style>
    /* Profile container */
    .profile {
      margin: 20px 0;
    }
    
    /* Profile sidebar */
    .profile-sidebar {
      background: #fff;
    }
    
    .profile-userpic img {
      float: none;
      margin: 0 auto;
      width: 10%;
      -webkit-border-radius: 10% !important;
      -moz-border-radius: 10% !important;
      border-radius: 10% !important;
    }
    
    .profile-usertitle {
      text-align: center;
      margin-top: 20px;
    }
    
    .profile-usertitle-name {
      color: #5a7391;
      font-size: 16px;
      font-weight: 600;
      margin-bottom: 7px;
    }
    
    .profile-usertitle-job {
      text-transform: uppercase;
      color: #555;
      font-size: 12px;
      font-weight: 600;
      margin-bottom: 15px;
    }
    
    .profile-userbuttons {
      text-align: center;
      margin-top: 10px;
    }
    
    .profile-userbuttons .btn {
      text-transform: uppercase;
      font-size: 11px;
      font-weight: 600;
      padding: 6px 15px;
      margin-right: 5px;
    }
    
    .profile-userbuttons .btn:last-child {
      margin-right: 0px;
    }
    
    .profile-usermenu {
      margin-top: 30px;
    }
    
    /* Profile Content */
    .profile-content {
      padding: 20px;
      background: #fff;
      min-height: 460px;
    }
    
    .avatar-upload {
      position: relative;
      max-width: 205px;
      margin: 0px auto;
    }
    .avatar-upload .avatar-edit {
      position: absolute;
      right: 12px;
      z-index: 1;
      top: 10px;
    }
    .avatar-upload .avatar-edit input {
      display: none;
    }
    .avatar-upload .avatar-edit input + label {
      display: inline-block;
      width: 34px;
      height: 34px;
      margin-bottom: 0;
      border-radius: 100%;
      background: #FFFFFF;
      border: 1px solid transparent;
      box-shadow: 0 5px 5px rgba(0, 0, 0, 0.2);
      cursor: pointer;
      font-weight: normal;
      transition: all 0.2s ease-in-out;
    }
    .avatar-upload .avatar-edit input + label:hover {
      background: #f1f1f1;
      border-color: #d6d6d6;
    }
    .avatar-upload .avatar-edit input + label:after {
      content: "\f040";
      font-family: 'FontAwesome';
      color: #757575;
      position: absolute;
      top: 10px;
      left: 0;
      right: 0;
      text-align: center;
      margin: auto;
    }
    .avatar-upload .avatar-previews {
      width: 35px;
      height: 35px;
      position: relative;
      border-radius: 100%;
      border: 3px solid #F8F8F8;
      box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
    }
    .avatar-upload .avatar-previews > div {
      width: 100%;
      height: 100%;
      border-radius: 100%;
      background-size: cover;
      background-repeat: no-repeat;
      background-position: center;
    }
</style>
    </style>
</aside>