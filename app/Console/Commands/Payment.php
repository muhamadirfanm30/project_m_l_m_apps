<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helpers\Midtrans;
use App\Model\Transaction;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use App\Model\ItemTransaction;
use App\Model\CsStockProduct;
use App\Model\Product;
use App\Model\GeneralSetting;
use App\Mail\successPaymentEmail;
use App\Mail\expiredPaymentEmail;
use App\Model\TransactionShipment;
use App\Model\NotifInfo;
use Carbon\Carbon;
use App\User;
class Payment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'midtrans:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Payment Midtrans';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $getData = Transaction::where('status',0)->where('payment_method','Midtrans')->get();
        foreach ($getData as $r) {
            $a = Midtrans::Cek($r->orderId);
        }

        $getDataNonMidtrans = Transaction::where('status',0)->where('payment_method','!=','Midtrans')->where('expired_date','<',date('Y-m-d H:i:s'))->get();
        foreach($getDataNonMidtrans as $data){
            $getItem = ItemTransaction::where('orderId',$data->orderId)->get();
            $field['status'] = 3;
            if(($data->own_transaction == 1 && $data->own_product == 0) || ($data->own_transaction == 0 && $data->own_product == 0) ){ //tidak diberikan kepada konsumen && ditimbun

                foreach ($getItem as $r) {

                    $getMyStok = Product::where('id',$r->product_id)->first();

                    if($getMyStok){
                        $fieldMyStok = [
                            'stok'=> (int)$getMyStok->stok + (int)$r->qty,
                        ];
                        $getMyStok->update($fieldMyStok);
                    }
                }

            }else if($data->own_transaction == 0 && $data->own_product == 1){
                foreach ($getItem as $r) {

                    $getMyStok = CsStockProduct::where('product_id',$r->product_id)->where('cs_id',$data->cs_id)->first();

                    if($getMyStok){
                        $fieldMyStok = [
                            'product_id'=>$r->product_id,
                            'stok'=> (int)$getMyStok->stok + (int)$r->qty,
                            'cs_id'=>$data->cs_id
                        ];
                        $getMyStok->update($fieldMyStok);
                    }else{
                        $fieldMyStok = [
                            'product_id'=>$r->product_id,
                            'stok'=>$r->qty,
                            'cs_id'=>$data->cs_id
                        ];
                        CsStockProduct::create($fieldMyStok);
                    }
                }
            }
            if(!empty($data->get_user->email)){
                Mail::to($data->get_user->email)->send(new expiredPaymentEmail($data->orderId));
            }
            $data->update($field);
        }
    }
}