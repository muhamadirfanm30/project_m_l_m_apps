<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Model\Product;
class UpdateProduct extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'product:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Product By Expired Date';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $field=[
            'harga_promo'=>null,
            'is_promo'=>0,
            'valid_at'=>null,
            'expired_at'=>null
        ];

        $update = Product::where('expired_at','<=',date('Y-m-d H:i:s'))->update($field);
    }
}
