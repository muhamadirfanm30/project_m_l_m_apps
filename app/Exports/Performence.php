<?php
namespace App\Exports;
use App\User;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;

class Performence implements FromView
{
    use Exportable;
    
    public function view(): View
    {
        return view('admin.excel.performence', [
            'datas' =>  User::where('roles', 'Customer')->with(['order.order_detail'])->get()
        ]);
    }
}