<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Spatie\Permission\Models\Role;
use App\Mail\SuccessRegisterEmail;
use Illuminate\Support\Facades\Mail;
use DB;
use Hash;
use Illuminate\Support\Str;

class ListAdminController extends Controller
{
    public function index(Request $request)
    {
        $data = User::orderBy('id','DESC')->whereIn('roles', ['admin', 'Sub Admin', 'Admin Produk', 'Admin Member'])->whereNull('is_super_admin')->paginate(10);
        return view('admin.adminManagement.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function create()
    {
        $roles = Role::pluck('name','name');
        return view('admin.adminManagement.create',compact('roles'));
    }

    public function store(Request $request)
    {
        $password = Str::random(8);
        $this->validate($request, [
            'first_name' => 'required',
            'email' => 'required|email|unique:users,email',
            'phone' => 'required|unique:users,whatsapp_no',
            'status' => 'required',
            'roles' => 'required'
        ]);
    
        $user = User::create([
            'first_name' => $request->first_name, 
            'last_name' => $request->last_name, 
            'email' => $request->email, 
            'phone' => str_replace(" ", "",$request->phone), 
            'whatsapp_no' => str_replace(" ", "",$request->phone),
            'status' => $request->status, 
            'password' => Hash::make($password), 
            'roles' => $request->roles,
            'membership_id' => $request->membership_id,
        ]);
        $user->assignRole($request->input('roles'));

        if(!empty($request->email)){
            Mail::to($request->email)->send(new SuccessRegisterEmail($user->id,$password));
            $response = [
                'status'=>true,
                'status_code'=>200,
                'message'=>'success',
                'email'=>'Sent Email register Email'
            ];
        }else{
            $response = [
                'status'=>true,
                'status_code'=>400,
                'message'=>'error',
                'email'=>'Failed Sent Email : register Email'
            ];
        }

        return redirect()->route('admin.show')
                        ->with('success','Admin created successfully');
    }

    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::get();
        return view('admin.adminManagement.update',compact('user', 'roles'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'status' => 'required',
            // 'roles' => 'required'
        ]);

        $user = User::find($id)->update([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'roles' => $request->roles,
            'phone' => str_replace(" ", "",$request->phone),
            'whatsapp_no' => str_replace(" ", "",$request->phone),
            'status' => $request->status,
        ]);

        return redirect()->route('admin.show')
                        ->with('success','Admin List updated successfully');
    }

    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect()->route('admin.show')
                        ->with('success','Admin list deleted successfully');
    }
}
