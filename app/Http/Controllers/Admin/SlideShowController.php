<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\SlideShow;
use App\Helpers\ImageUpload;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use DB;

class SlideShowController extends Controller
{
    public function index()
    {
        $no =1;
        $show = DB::table('slide_shows')
                        ->select('is_template', DB::raw('count(*) as total'))
                        ->groupBy('is_template')
                        ->get();
        return view('admin.slideShow.index', [
            'show' => $show,
            'no' => $no
        ]);
    }

    public function create()
    {
        
        $show = SlideShow::get();
        return view('admin.slideShow.create', [
            'show' => $show,
            
        ]);
    }

    public function detailParent($is_template)
    {
        $no = 1;
        $show = SlideShow::where('is_template', $is_template)->get();
        return view('admin.slideShow.detailParent', [
            'show' => $show,
            'no' => $no
        ]);
    }

    public function update($id)
    {
        $dataEdit = SlideShow::where('id', $id)->first();
        return view('admin.slideShow.update', [
            'dataEdit' => $dataEdit,
        ]);
    }

    public function edit(Request $request, $id)
    {
        $edit = SlideShow::where('id', $id)->first();

        if(!empty($request->images)){
            if ($request->hasFile('images')) {
                $image      = $request->file('images');
                $file_name   = time() . '.' . $image->getClientOriginalExtension();
                $img = Image::make($image);
                $img->stream(); // <-- Key point
                Storage::disk('local')->put('public/slide-show/' . $file_name, $img);
    
                $exists = Storage::disk('local')->has('public/slide-show/' . $edit->images);
                if ($exists) {
                    Storage::delete('public/slide-show/' . $edit->images);
                }
            }
        }else{
            $file_name = $edit->images;
        }

        SlideShow::where('id', $id)->update([
            'is_template' => $request->is_template,
            'judul' => $request->judul,
            'deskripsi' => $request->deskripsi,
            'images' => $file_name,
        ]);

        return redirect()->route('slide.show')
                        ->with('success','Slide Show Update successfully');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'judul' => ['required'],
            'deskripsi' => ['required'],
            'images' => ['required'],
            'is_template' => ['required'],
        ]);

        $path = SlideShow::getImagePathUpload();
        $filename = null;

        // if ($request->images != null) {
        //     $image = (new ImageUpload)->upload($request->images, $path);
        //     $filename = $image->getFilename();
        // }

        if($request->hasFile('images')){
            foreach($request->file('images') as $image){
                $name = $image->getClientOriginalName();
                $img = Image::make($image);
                $img->stream(); // <-- Key point
                Storage::disk('local')->put('public/slide-show/' . $name, $img);
                $filename[] = $name;
            }
        }
        


        for ($i = 0; $i < count($request->is_template); $i++) {
            $insert[] = [
                'is_template' => $request->is_template[$i],
                'judul' => $request->judul[$i],
                'deskripsi' => $request->deskripsi[$i],
                'images' => $filename[$i],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
        }

        SlideShow::insert($insert);
        return redirect()->route('slide.show')
                        ->with('success','Slide Show created successfully');
    }

    public function destroy($id)
    {
        $slideShow = SlideShow::where('id', $id)->first();
        $exists = Storage::disk('local')->has('public/slide-show/' . $slideShow->images);
        if ($exists) {
            Storage::delete('public/slide-show/' . $slideShow->images);
        }
        SlideShow::find($id)->delete();
        return redirect()->route('slide.show')
                        ->with('success','Paket Upgrade deleted successfully');
    }
}
