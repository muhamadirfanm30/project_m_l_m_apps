<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\PanelHWI;

class PanelHWIController extends Controller
{
    public function index()
    {
        $no = 1;
        $show = PanelHWI::get();
        return view('admin.panel-hwi.index', [
            'show' => $show,
            'no' => $no,
        ]);
    }

    public function create()
    {
        return view('admin.panel-hwi.create');
    }

    public function edit($id)
    {
        $listHwi = PanelHWI::where('id', $id)->first();
        return view('admin.panel-hwi.update', [
            'listHwi' => $listHwi
        ]);
    }

    public function store(Request $request)
    {
        for ($i = 0; $i < count($request->judul); $i++) {
            $answers[] = [
                'judul' => $request->judul[$i],
                'vidio' => $request->vidio[$i],
                'created_at' => date('Y-m-d H:i:s'),
            ];
        }

        PanelHWI::insert($answers);
        return redirect()->route('hwi.show')
                        ->with('success','Penjelasan Panel WHI Berhasil di Simpan');
        
    }

    public function update(Request $request, $id)
    {
        PanelHWI::where('id', $id)->update([
            'judul' => $request->judul,
            'vidio' => $request->vidio
        ]);

        return redirect()->route('hwi.show')
                        ->with('success','Penjelasan Panel WHI Berhasil di Ubah');
    }

    public function destroy($id)
    {
        PanelHWI::where('id', $id)->delete();
        return redirect()->route('hwi.show')
                        ->with('success','Penjelasan Panel WHI Berhasil di Ubah');
    }
}
