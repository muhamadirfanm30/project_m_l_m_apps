<?php

namespace App\Http\Controllers\Admin;

use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\ImageUpload;
use App\Model\Payment;
use DB;

class PaymentController extends Controller
{
    public function index()
    {
        $getMaster = DB::table('payment_master')->get();
        return view('admin.payment.index', [
            'getMaster' => $getMaster
        ]);
    }

    public function detail()
    {
        $getPayment = Payment::get();
        return view('admin.payment.bank_transfer_detail', [
            'getPayment' => $getPayment
        ]);
    }

    public function create()
    {
        return view('admin.payment.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'nama_bank' => 'required',
            'nama_rekening' => 'required',
            'nomor_rekening' => 'required',
            'image' => 'required',
            'desc' => 'required',
        ]);
        
        $path = Payment::getImagePathUpload();
        $filename = null;

        if ($request->image != null) {
            $image = (new ImageUpload)->upload($request->image, $path);
            $filename = $image->getFilename();
        }
        

        $storeSalTraining = Payment::create([
            'nama_bank' => $request->nama_bank,
            'nama_rekening' => $request->nama_rekening,
            'image' => $filename,
            'nomor_rekening' => $request->nomor_rekening,
            'desc' => $request->desc,
        ]);

        return redirect()->route('payment.detail')
                        ->with('success','Bank Transfer created successfully');
    }

    public function update($id)
    {
        $dataEdit = Payment::find($id);
        return view('admin.payment.update', [
            'dataEdit' => $dataEdit
        ]);
    }

    public function msPaymentUpdate($id)
    {
        // return 'asd';
        $masterEdit = $getMaster = DB::table('payment_master')->find($id);
        return view('admin.payment.update_master_payment', [
            'masterEdit' => $masterEdit
        ]);
    }

    public function editMsPayment(Request $request, $id)
    {
        DB::table('payment_master')->where('id', $id)->update([
            'payment_name' => $request->payment_name,
            'is_active' => $request->is_active,
        ]);
        return redirect()->route('payment.show')
                        ->with('success','Master Payment Updated successfully');
    }

    public function edit(Request $request, $id)
    {
        $this->validate($request, [
            'nama_bank' => 'required',
            'nama_rekening' => 'required',
            'nomor_rekening' => 'required',
            'desc' => 'required',
        ]);

        $getImg = Payment::where('id', $id)->first();

        if(!empty($request->image)){
            if ($request->hasFile('image')) {
                $image      = $request->file('image');
                $file_name   = time() . '.' . $image->getClientOriginalExtension();
                $img = Image::make($image);
                $img->stream(); // <-- Key point
                Storage::disk('local')->put('public/bankIcon/' . $file_name, $img);
    
                $exists = Storage::disk('local')->has('public/bankIcon/' . $getImg->image);
                if ($exists) {
                    Storage::delete('public/bankIcon/' . $getImg->image);
                }
            }
        }else{
            $file_name = $getImg->image;
        }

        

        $storeSalTraining = Payment::where('id', $id)->update([
            'nama_bank' => $request->nama_bank,
            'nama_rekening' => $request->nama_rekening,
            'image' => $file_name,
            'nomor_rekening' => $request->nomor_rekening,
            'desc' => $request->desc,
        ]);

        return redirect()->route('payment.detail')
                        ->with('success','Bank Transfer Updated successfully');
    }

    public function destroy($id)
    {
        $getImg = Payment::where('id', $id)->first();
        $exists = Storage::disk('local')->has('public/bankIcon/' . $getImg->image);
        if ($exists) {
            Storage::delete('public/bankIcon/' . $getImg->image);
        }
        Payment::find($id)->delete();
        return redirect()->route('payment.detail')
                        ->with('success','Bank Transfer deleted successfully');
    }
}
