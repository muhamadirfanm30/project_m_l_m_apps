<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\TermAndCondition;
use App\Model\FaqKategori;
use App\Model\AccordionTermCondition;
use DB;

class TermAndConditionController extends Controller
{
    public function index()
    {
        $no = 1;
        $faq = TermAndCondition::with('faqKategori')->orderBy('id', 'desc')->get();
        return view('admin.term-and-condition.index', [
            'faq' => $faq, 
            'no' => $no
        ]);
    }

    public function create()
    {
        $getDataFaq = FaqKategori::get();
        return view('admin.term-and-condition.create', [
            'getDataFaq' => $getDataFaq
        ]);
    }

    public function update($id)
    {
        
        $getDataFaq = FaqKategori::get();
        $dataEdit = TermAndCondition::where('id', $id)->first();
        $faqDetail = DB::table('accordion_term_conditions')->where('term_condition_id', $id)->get();
        return view('admin.term-and-condition.update', [
            'faqDetail' => $faqDetail,
            'dataEdit' => $dataEdit,
            'getDataFaq' => $getDataFaq
        ]);
    }

    public function updateFaq($id)
    {
        $faqDetail = AccordionTermCondition::where('id', $id)->first();
        return $faqDetail;
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'is_template' => ['required'],
            'judul_utama' => ['required'],
            'deskripsi_utama' => ['required'],
        ]);

        $faq = TermAndCondition::create([
            'is_template' => $request->is_template,
            'judul_utama' => $request->judul_utama,
            'deskripsi_utama' => $request->deskripsi_utama,
        ]);

        for ($i = 0; $i < count($request->judul_accordion); $i++) {
            $createFaqDetail[] = [
                'judul_accordion' => $request->judul_accordion[$i],
                'desc_accordion' => $request->desc_accordion[$i],
                'term_condition_id' => $faq->id,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
        }

        DB::table('accordion_term_conditions')->insert($createFaqDetail);
        return redirect()->route('term-and-condition.show')
                        ->with('success','FAQ created successfully');
    }


    public function edit(Request $request, $id)
    {
        $this->validate($request, [
            'is_template' => ['required'],
            'judul_utama' => ['required'],
            'deskripsi_utama' => ['required'],
        ]);

        $faq = TermAndCondition::where('id', $id)->update([
            'is_template' => $request->is_template,
            'judul_utama' => $request->judul_utama,
            'deskripsi_utama' => $request->deskripsi_utama,
        ]);
            return redirect()->route('term-and-condition.show')
                        ->with('success','FAQ created successfully');
              
    }

    public function editFaq(Request $request, $id)
    {
        AccordionTermCondition::where('id', $request->id_faq)->update([
            'judul_accordion' => $request->judul_accordion,
            'desc_accordion' => $request->desc_accordion,
        ]);
    }

    public function storeFaq(Request $request)
    {
        $this->validate($request, [
            'judul_accordion' => ['required'],
            'desc_accordion' => ['required'],
        ]);

        AccordionTermCondition::create([
            'judul_accordion' => $request->judul_accordion,
            'desc_accordion' => $request->desc_accordion,
            'term_condition_id' => $request->parent_faq
        ]);

        return redirect()->back()->with('success','FAQ created successfully');
    }

    public function deleteFaq(Request $request, $id)
    {
        AccordionTermCondition::where('id', $request->id)->delete();
        return redirect()->back()->with('success','FAQ Deleted successfully');
    }

    public function delete(Request $request, $id)
    {
        $ids = AccordionTermCondition::where('term_condition_id', $id)->get('id');
        TermAndCondition::where('id', $id)->delete();
        AccordionTermCondition::whereIn('term_condition_id', $ids)->delete();
        return redirect()->back()->with('success','FAQ Deleted successfully');
    }

}
