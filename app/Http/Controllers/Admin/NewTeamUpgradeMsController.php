<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\NotifInfo;
use App\Model\PositionUpdateModel;
use Illuminate\Http\Request;
use App\User;
use App\Helpers\ImageUpload;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class NewTeamUpgradeMsController extends Controller
{
    public function index(Type $var = null)
    {
        // NotifInfo::readAtMyTeamUpgrade();

        $no = 1;
        $dataUpgradeMs = User::with('notif_no_read_my_team_upgrade_ms')->where('is_upgrade_ms', 3)->where('is_admin_created', 0)->orderBy('id', 'DESC')->get();
        return view('admin.new-team-upgrade-ms.show', [
            'dataUpgradeMs' => $dataUpgradeMs,
            'no' => $no
        ]);
    }

    public function detail($id)
    {
        $user = User::where('id', $id)->with('get_posisi')->first();
        $posisi = PositionUpdateModel::where('calon_ms_id', $id)->with('get_posisi')->first();
        NotifInfo::readAtMyTeamUpgrade($user->id, 'admin');
        return view('admin.new-team-upgrade-ms.detail', [
            'user' => $user,
            'posisi' => $posisi

        ]);
    }

    public function edit($id)
    {
        $dataEdit = User::where('id', $id)->first();
        $posisi = PositionUpdateModel::where('calon_ms_id', $id)->with('get_posisi')->first();
        return view('admin.new-team-upgrade-ms.update', [
            'dataEdit' => $dataEdit,
            'posisi' => $posisi
        ]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'foto_ktp' => 'max:5120',
            'email' => 'required',
        ]);

        $cek = User::where('id', $id)->first();

            if(!empty($request->foto_ktp)){
                if ($request->hasFile('foto_ktp')) {
                    $image1      = $request->file('foto_ktp');
                    $file_name   = time() . '.' . $image1->getClientOriginalExtension();
                    $img1 = Image::make($image1);
                    $img1->stream(); // <-- Key point
                    Storage::disk('local')->put('public/foto_ktp/' . $file_name, $img1);
        
                    $exists1 = Storage::disk('local')->has('public/foto_ktp/' . $cek->foto_ktp);
                    if ($exists1) {
                        Storage::delete('public/foto_ktp/' . $cek->foto_ktp);
                    }
                }
            }else{
                $file_name = $cek->foto_ktp;
            }

        User::where('id', $id)->update([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'phone' => $request->phone,
            'whatsapp_no' => $request->phone,
            'gender'  => $request->gender,
            'address'  => $request->address,
            'province_id'  => $request->txtProvinsi,
            'kota_id'  => $request->txtKota,
            'kode_pos'  => $request->kode_pos,
            'sponsor_upline_id' => $request->no_id_sponsor_upline,
            'sponsor_upline_name' => $request->sponsor_upline_name,
            'foto_ktp' => $file_name,
            'is_random_position' => 0
        ]);

        PositionUpdateModel::where('calon_ms_id', $id)->update([
            'posisi' => $request->posisi,
            'user_sponsor_name' => $request->sponsor_upline_name,
            'user_sponsor_status' => $request->no_id_sponsor_upline,
        ]);

        return redirect('/admin/new-team-upgrade-ms/detail/' . $id)->with('success', 'Update Data Membership Telah berhasil');
    }

    public function approveMS(Request $request, $id)
    {
        $cek = User::where('membership_id', $request->membership_id)->count();
        $cekPosisi = User::where('id', $id)->with('get_posisi')->first();
        if ($cek == 0) {
            if (empty($cekPosisi->get_posisi) || empty($cekPosisi->get_posisi->posisi)) {
                return redirect('/admin/new-team-upgrade-ms/detail/' . $id)->with('error', 'Silahkan menentukan Posisi terlebih dahulu sebelum menekan tombol approve.');
            } else {
                $this->validate($request, [
                    'membersip_status' => 'required',
                    'membership_id' => 'required',
                ]);
                User::where('id', $id)->update([
                    'membersip_status'  => $request->membersip_status,
                    'membership_id'  => $request->membership_id,
                    'is_upgrade_ms'  => 1,
                ]);
                return redirect('/admin/new-team-upgrade-ms/show')->with('success', 'Upgrade Ms Has Been Success');
            }
        } else {
            return redirect('/admin/new-team-upgrade-ms/detail/' . $id)->with('error', 'Kode ' . $request->membership_id . ' telah digunakan user lain');
        }
    }
}
