<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Helpers\ImageUpload;
use Intervention\Image\ImageManagerStatic as Image;
use Storage;
use App\Model\Konten;
use App\Model\Kategori;
use App\Model\SubKategori;
class KontenController extends Controller
{
    public function index(){
        $data = Konten::orderBy('judul','asc')->get();
        return view('admin.konten.index',compact(['data']));
    }

    public function store(Request $request){
        $listKategori = Kategori::orderBy('created_at','desc')->get();
        $listSubKategori = SubKategori::orderBy('created_at','desc')->get();

        if($request->isMethod('post')){
            $this->validate($request, [
                'free_text'=>'required',
                // 'url_download'=>'required',
                // 'thumbnail'=>'required',
                'kategori_id'=>'required',
                'sub_kategori_id'=>'required',
                'type'=>'required',//URL VIDEO, input file, accordion
                'judul'=>'required',
                // 'deskripsi'=>'required',
            ]);
            $field = $request->only(['free_text','url_download','kategori_id','sub_kategori_id','type','judul','deskripsi','source']);
            
            if ($request->hasFile('thumbnail')) {
                $image = (new ImageUpload)->upload($request->thumbnail, 'public/konten');
                $filename = $image->getFilename();
                $field['foto'] = $filename;
            }

            // if ($request->hasFile('source') && $request->type == 1) {
            //     $image = (new ImageUpload)->upload($request->source, 'public/konten');
            //     $filename = $image->getFilename();
            //     $field['source'] = $filename;
            // }

            $save = Konten::create($field);

            return redirect()->route('konten')->with('success','Data created successfully');
        }
        return view('admin.konten.create',compact(['listKategori','listSubKategori']));
    }

    public function edit(Konten $id,Request $request){
        $listKategori = Kategori::orderBy('created_at','desc')->get();
        $listSubKategori = SubKategori::orderBy('created_at','desc')->get();

        if($request->isMethod('post')){
            $this->validate($request, [
                'free_text'=>'required',
                // 'url_download'=>'required',
                'kategori_id'=>'required',
                'sub_kategori_id'=>'required',
                'type'=>'required',//URL VIDEO, input file, accordion
                'judul'=>'required',
                // 'deskripsi'=>'required',
            ]);
            $field = $request->only(['free_text','url_download','kategori_id','sub_kategori_id','type','judul','deskripsi']);
            if ($request->hasFile('thumbnail')) {
                $image = (new ImageUpload)->upload($request->thumbnail, 'public/konten');
                $filename = $image->getFilename();
                $field['foto'] = $filename;
            }

            // if ($request->hasFile('source') && $request->type == 1) {
            //     $image = (new ImageUpload)->upload($request->source, 'public/konten');
            //     $filename = $image->getFilename();
            //     $field['source'] = $filename;
            // }else{
            //     $field['source'] = $request->source;
            // }
            $id->update($field);
            return redirect()->route('konten')->with('success','Data updated successfully');
        }
        return view('admin.konten.edit',compact(['id','listKategori','listSubKategori']));
    }

    public function destroy(Konten $id){
        if($id->delete()){
            return redirect()->route('konten')
                            ->with('success','Data has been deleted');
        }else{
            return redirect()->route('konten')
                            ->with('failed','Failed delete data');
        }
    }
}
