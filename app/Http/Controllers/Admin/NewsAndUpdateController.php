<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\NewsAndUpdate;

class NewsAndUpdateController extends Controller
{
    public function index()
    {
        $showListNews = NewsAndUpdate::orderBy('id','DESC')->get();
        // return $showListNews;
        $no =1;
        return view('admin.newsAndUpdate.index', [
            'showListNews' => $showListNews,
            'no' => $no
        ]);
    }

    public function create()
    {
        return view('admin.newsAndUpdate.create');
    }

    public function edit($id)
    {
        $getData = NewsAndUpdate::where('id', $id)->first();
        return view('admin.newsAndUpdate.update', [
            'getData' => $getData
        ]);
    }

    public function detail($id)
    {
        $getData = NewsAndUpdate::where('id', $id)->first();
        return view('admin.newsAndUpdate.detail', [
            'getData' => $getData
        ]);
    }

    public function destroy($id)
    {
        NewsAndUpdate::find($id)->delete();
        return redirect()->route('news.show')
                        ->with('success','News And Update deleted successfully');
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'judul' => 'required',
            'deskripsi' => 'required',
            'url_vidio' => 'required',
            'publish_at' => 'required',
        ]);

        $input = $request->all();
        $user = NewsAndUpdate::find($id);
        $user->update($input);

        return redirect()->route('news.show')
                        ->with('success','News And Update updated successfully');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'judul' => 'required',
            'deskripsi' => 'required',
            'url_vidio' => 'required',
            'publish_at' => 'required',
        ]);

        $input = $request->all();
        $user = NewsAndUpdate::create($input);
        return redirect()->route('news.show')
                        ->with('success','News And Update created successfully');
    }
}

