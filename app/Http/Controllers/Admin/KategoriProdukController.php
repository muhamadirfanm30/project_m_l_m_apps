<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\KategoriProduk;
use Illuminate\Http\Request;

class KategoriProdukController extends Controller
{
    public function index()
    {
        $no = 1;
        $showData = KategoriProduk::orderBy('id', 'DESC')->get();
        return view('admin.kategoriProduk.show', [
            'no' => $no,
            'showData' => $showData,
        ]);
    }

    public function create()
    {
        return view('admin.kategoriProduk.create');
    }

    public function edit($id)
    {
        $edit = KategoriProduk::find($id);
        return view('admin.kategoriProduk.update', [
            'edit' => $edit
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => ['required'],
        ]);

        for ($i = 0; $i < count($request->name); $i++) {
            $answers[] = [
                'name' => $request->name[$i],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
        }

        KategoriProduk::insert($answers);
        return redirect()->route('kategoori-produk.show')
                        ->with('success','Kategori Produk created successfully');
        
    }

    public function update(Request $request, $id)
    {
        KategoriProduk::where('id', $id)->update([
            'name' => $request->name
        ]);

        return redirect()->route('kategoori-produk.show')
                        ->with('success','Kategori Produk Update successfully');
    }

    public function destroy($id)
    {
        KategoriProduk::where('id', $id)->delete();

        return redirect()->route('kategoori-produk.show')
                        ->with('success','Kategori Produk Delete successfully');
    }

    public function teruncate()
    {
        KategoriProduk::truncate();
        return redirect()->route('kategoori-produk.show')
                        ->with('success','Kategori Produk Delete successfully');
    }
}
