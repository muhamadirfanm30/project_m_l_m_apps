<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Email;
use App\Model\EmailLogo;
use App\Helpers\ImageUpload;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class EmailController extends Controller
{
    public function index()
    {
        $no=1;
        $cekData = EmailLogo::first();
        $getListEmail = Email::get();
        return view('admin.email.index', [
            'getListEmail' => $getListEmail,
            'cekData' => $cekData,
            'no' => $no
        ]);
    }

    public function edit($id)
    {
        $getListEmail = Email::find($id);
        return view('admin.email.update', [
            'getListEmail' => $getListEmail
        ]);
    }

    public function update(Request $request, $id)
    {
        $update = Email::where('id', $id)->update([
            'subject' => $request->subject,
            'content' => $request->content
        ]);

        return redirect()->route('email.show')
                        ->with('success','Updated Email successfully');
    }

    public function createUpdateLogo(Request $request)
    {
        $cek = EmailLogo::count();
        $path = EmailLogo::getImagePathUpload();
        $filename = null;
        if($cek == 0){
            if(!empty($request->logo)){
                if ($request->logo != null) {
                    $logo = (new ImageUpload)->upload($request->logo, $path);
                    $filename = $logo->getFilename();
                }else{
                    $filename = '';
                }

                EmailLogo::create([
                    'logo' => $filename
                ]);

                return redirect()->route('email.show')
                        ->with('success','Logo Berhasil Diubah');
            }else{
                return redirect()->route('email.show')
                        ->with('error','Terjadi Kesalahan Saat Menggubah Logo');
            }
        }else{
            $cekData = EmailLogo::first();
            if(!empty($request->logo)){
                if ($request->hasFile('logo')) {
                    $image      = $request->file('logo');
                    $file_name   = time(). '.' . $image->getClientOriginalExtension();
                    $img = Image::make($image);
                    $img->stream(); // <-- Key point
                    Storage::disk('local')->put('public/email-logo/' . $file_name, $img);
        
                    $exists1 = Storage::disk('local')->has('public/email-logo/' . $cekData->logo);
                    if ($exists1) {
                        Storage::delete('public/email-logo/' . $cekData->logo);
                    }
                }
            }else{
                // $file_name = $cekData->logo;
                return redirect()->route('email.show')
                        ->with('error','Terjadi Kesalahan Saat Menggubah Logo');
            }

            EmailLogo::where('id', $cekData->id)->update([
                'logo' => $file_name
            ]);

            return redirect()->route('email.show')
                        ->with('success','Logo Berhasil Diubah');
        }
        
    }
    
}
