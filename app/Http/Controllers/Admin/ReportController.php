<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Transaction;
use Carbon\Carbon;
use App\User;
use DB;
class ReportController extends Controller
{
    public function indexProduct($type = "Monthly"){
        $datas = [];
        $title = [];
        switch ($type) {
            case 'Weekly':
                $arrayDate = [
                    'Week 1'=> [ '01','07' ],
                    'Week 2'=> [ '08','14' ],
                    'Week 3'=> [ '15','21' ],
                    'Week 4'=> [ '22','28' ],
                    'Week 5'=> [ '29','31' ]
                ];

                foreach ($arrayDate as $key => $value) {
                    $data = Transaction::selectRaw(" '$key' as week, count(*) as total")
                                            ->whereIn('status',[1,2,3,4,5])
                                            ->whereRaw('created_at BETWEEN "'. date('Y-m-'.$value[0]) .'" AND "'. date('Y-m-'.$value[1]).'"')
                                            ->first()
                                            ->toArray();
                    if($data['total'] > 0){
                        $datas[] = $data;
                    }
                }
                $title = array_column($datas, 'week');
                break;
            case 'Monthly':
                $datas = Transaction::selectRaw("MONTH(created_at) as month, count(*) as total")
                            ->whereIn('status',[1,2,3,4,5])
                            ->whereBetween('created_at',[date('Y').'-01-01',date('Y').'-12-30'])
                            ->groupBy(DB::raw("MONTH(created_at)"))
                            ->get()
                            ->toArray();
                foreach ($datas as $key) {
                    $date = Carbon::parse(Date('2020-'.$key['month'].'-01'));
                    $title[].= $date->format('F');
                }
                break;
            case 'Yearly':
                $subYear = Date('Y') - 5;
                $datas = Transaction::selectRaw("YEAR(created_at) as year, count(*) as total")
                            ->whereIn('status',[1,2,3,4,5])
                            ->whereRaw('created_at BETWEEN '. $subYear .' AND '. Date('Y'))
                            ->groupBy(DB::raw("YEAR(created_at)"))
                            ->get()
                            ->toArray();
                $title = array_column($datas, 'year');
                break;
        }

        $TotalThisMonth = 0;
        $TotalThisYear = 0;
        $TotalPrevMonth = 0;
        $percentage = 0;
        $allTotalThisMonth = Transaction::selectRaw("count(*) as total")
                            ->where('status',1)
                            ->whereMonth('created_at',date('m'))
                            ->whereYear('created_at',date('Y'))
                            ->first();
        if($allTotalThisMonth) $TotalThisMonth = $allTotalThisMonth->total;
        
        $allTotalThisYear = Transaction::selectRaw("count(*) as total")
                            ->where('status',1)
                            ->whereYear('created_at',date('Y'))
                            ->first();
        if($allTotalThisYear) $TotalThisYear = $allTotalThisYear->total;

        $allTotalPrevMonth = Transaction::selectRaw("count(*) as total")
                            ->where('status',1)
                            ->whereMonth('created_at', (date('m') == 1 ? 12 : (date('m') - 1)) )
                            ->whereYear('created_at',date('Y'))
                            ->first();
        if($allTotalPrevMonth) $TotalPrevMonth = $allTotalPrevMonth->total;
        
        // ((Total sebelumnya - Total sekarang) / total sebelumnya) * 100
        if($TotalThisMonth > 0){
            if($TotalPrevMonth > $TotalThisMonth){
                $percentage = ($TotalPrevMonth / $TotalThisMonth) * 100;
            }else{
                // $percentage = ($TotalThisMonth / $TotalPrevMonth) * 100;
                if($TotalPrevMonth > 0){
                    $percentage = ($TotalThisMonth / $TotalPrevMonth) * 100;
                }else{
                    $percentage = 0;
                }
            }
        }else{
            $percentage = 0;
        }
        
        $listTotal = [
            'prevMonth'=>$TotalPrevMonth,
            'thisMonth'=>$TotalThisMonth,
            'percentage'=> ($TotalPrevMonth > $TotalThisMonth ? '-' : '+').' '.$percentage.'%' ,
            'thisYear'=>$TotalThisYear
        ];
        return view('admin.report.product',compact('datas','title','type','listTotal'));
    }

    public function indexOmset($type = "Monthly"){
        $datas = [];
        $title = [];
        switch ($type) {
            case 'Weekly':
                $arrayDate = [
                    'Week 1'=> [ '01','07' ],
                    'Week 2'=> [ '08','14' ],
                    'Week 3'=> [ '15','21' ],
                    'Week 4'=> [ '22','28' ],
                    'Week 5'=> [ '29','31' ]
                ];

                foreach ($arrayDate as $key => $value) {
                    $data = Transaction::selectRaw(" '$key' as week, SUM(totalKeseluruhan) as total")
                                            ->whereIn('status',[1,2,3,4,5])
                                            ->whereRaw('created_at BETWEEN "'. date('Y-m-'.$value[0]) .'" AND "'. date('Y-m-'.$value[1]).'"')
                                            ->first()
                                            ->toArray();
                    if($data['total'] > 0){
                        $datas[] = $data;
                    }
                }
                $title = array_column($datas, 'week');
                break;
            case 'Monthly':
                $datas = Transaction::selectRaw("MONTH(created_at) as month, SUM(totalKeseluruhan) as total")
                            ->whereIn('status',[1,2,3,4,5])
                            ->whereBetween('created_at',[date('Y').'-01-01',date('Y').'-12-30'])
                            ->groupBy(DB::raw("MONTH(created_at)"))
                            ->get()
                            ->toArray();
                foreach ($datas as $key) {
                    $date = Carbon::parse(Date('2020-'.$key['month'].'-01'));
                    $title[].= $date->format('F');
                }
                break;
            case 'Yearly':
                $subYear = Date('Y') - 5;
                $datas = Transaction::selectRaw("YEAR(created_at) as year, SUM(totalKeseluruhan) as total")
                            ->whereIn('status',[1,2,3,4,5])
                            ->whereRaw('created_at BETWEEN '. $subYear .' AND '. Date('Y'))
                            ->groupBy(DB::raw("YEAR(created_at)"))
                            ->get()
                            ->toArray();
                $title = array_column($datas, 'year');
                break;
        }
        $TotalThisMonth = 0;
        $TotalThisYear = 0;
        $TotalPrevMonth = 0;
        $percentage = 0;
        $allTotalThisMonth = Transaction::selectRaw("SUM(totalKeseluruhan) as total")
                            ->whereIn('status',[1,2,3,4,5])
                            ->whereMonth('created_at',date('m'))
                            ->whereYear('created_at',date('Y'))
                            ->first();
        if($allTotalThisMonth) $TotalThisMonth = $allTotalThisMonth->total;
        
        $allTotalThisYear = Transaction::selectRaw("SUM(totalKeseluruhan) as total")
                            ->whereIn('status',[1,2,3,4,5])
                            ->whereYear('created_at',date('Y'))
                            ->first();
        if($allTotalThisYear) $TotalThisYear = $allTotalThisYear->total;

        $allTotalPrevMonth = Transaction::selectRaw("SUM(totalKeseluruhan) as total")
                            ->whereIn('status',[1,2,3,4,5])
                            ->whereMonth('created_at', (date('m') == 1 ? 12 : (date('m') - 1)) )
                            ->whereYear('created_at',date('Y'))
                            ->first();
        if($allTotalPrevMonth) $TotalPrevMonth = $allTotalPrevMonth->total;
        
        // ((Total sebelumnya - Total sekarang) / total sebelumnya) * 100
        if($TotalThisMonth > 0){
            if($TotalPrevMonth > $TotalThisMonth){
                $percentage = ($TotalPrevMonth / $TotalThisMonth) * 100;
            }else{
                // $percentage = ($TotalThisMonth / $TotalPrevMonth) * 100;
                if($TotalPrevMonth > 0){
                    $percentage = ($TotalThisMonth / $TotalPrevMonth) * 100;
                }else{
                    $percentage = 0;
                }
            }
        }else{
            $percentage = 0;
        }
        
        $listTotal = [
            'prevMonth'=>$TotalPrevMonth,
            'thisMonth'=>$TotalThisMonth,
            'percentage'=> ($TotalPrevMonth > $TotalThisMonth ? '-' : '+').' '.$percentage.'%' ,
            'thisYear'=>$TotalThisYear
        ];
        
        return view('admin.report.omset',compact('datas','title','type','listTotal'));
    }

    public function indexMember($type = "Monthly"){
        $datas = [];
        $title = [];
        switch ($type) {
            case 'Weekly':
                $arrayDate = [
                    'Week 1'=> [ '01','07' ],
                    'Week 2'=> [ '08','14' ],
                    'Week 3'=> [ '15','21' ],
                    'Week 4'=> [ '22','28' ],
                    'Week 5'=> [ '29','31' ]
                ];

                foreach ($arrayDate as $key => $value) {
                    $data = User::selectRaw(" '$key' as week, count(*) as total")
                                            ->where('roles','Customer')
                                            ->whereRaw('created_at BETWEEN "'. date('Y-m-'.$value[0]) .'" AND "'. date('Y-m-'.$value[1]).'"')
                                            ->first()
                                            ->toArray();
                    if($data['total'] > 0){
                        $datas[] = $data;
                    }
                }
                
                $title = array_column($datas, 'week');
                break;
            case 'Monthly':
                $datas = User::selectRaw("MONTH(created_at) as month, count(*) as total")
                            ->where('roles','Customer')
                            ->whereBetween('created_at',[date('Y').'-01-01',date('Y').'-12-30'])
                            ->groupBy(DB::raw("MONTH(created_at)"))
                            ->get()
                            ->toArray();
                foreach ($datas as $key) {
                    $date = Carbon::parse(Date('2020-'.$key['month'].'-01'));
                    $title[].= $date->format('F');
                }
                
                break;
            case 'Yearly':
                $subYear = Date('Y') - 5;
                 $datas = User::selectRaw("YEAR(created_at) as year, count(*) as total")
                            ->where('roles','Customer')
                            // ->whereRaw('created_at BETWEEN '. $subYear .' AND '. Date('Y'))
                            ->groupBy(DB::raw("YEAR(created_at)"))
                            ->get()
                            ->toArray();
                $title = array_column($datas, 'year');
                break;
        }


        $RegulerThisMonth = 0;
        $RegulerLastMont = 0;
        $RegulerThisMonth = 0;
        $RegulerLastMont = 0;
        // $TotalThisYear = 0;
        $percentage = 0;
        $MemberRegulerThisMonth = User::selectRaw("count(*) as total")
                            ->where('membersip_status','Reguler')
                            ->whereMonth('created_at',date('m'))
                            ->whereYear('created_at',date('Y'))
                            ->first();
        if($MemberRegulerThisMonth) $RegulerThisMonth = $MemberRegulerThisMonth->total;

        $MemberRegulerLastMonth = User::selectRaw("count(*) as total")
                            ->where('membersip_status','Reguler')
                            ->whereMonth('created_at', (date('m') == 1 ? 12 : (date('m') - 1)) )
                            ->whereYear('created_at',date('Y'))
                            ->first();
        if($MemberRegulerLastMonth) $RegulerLastMont = $MemberRegulerLastMonth->total;
        
        // $allTotalThisYear = Transaction::selectRaw("count(*) as total")
        //                     ->where('status',1)
        //                     ->whereYear('created_at',date('Y'))
        //                     ->first();
        // if($allTotalThisYear) $TotalThisYear = $allTotalThisYear->total;

        // return [
        //     'str' => substr("R-UV94QYW45",0,1),
        //     'reguler this mont' => $RegulerThisMonth,
        //     'reguler last mont' => $RegulerLastMont,
        // ];
        
        // ((Total sebelumnya - Total sekarang) / total sebelumnya) * 100
        if($RegulerThisMonth > 0){
            if($RegulerLastMont > $RegulerThisMonth){
                $percentage = ($RegulerLastMont / $RegulerThisMonth) * 100;
            }else{
                // $percentage = ($RegulerThisMonth / $RegulerLastMont) * 100;
                if($RegulerLastMont > 0){
                    $percentage = ($RegulerThisMonth / $RegulerLastMont) * 100;
                }else{
                    $percentage = 0;
                }
            }
        }else{
            $percentage = 0;
        }
        
        $listTotal = [
            'RegulerLastMont'=>$RegulerLastMont,
            'RegulerThisMonth'=>$RegulerThisMonth,
            'percentage'=> ($RegulerLastMont > $RegulerThisMonth ? '-' : '+').' '.$percentage.'%' ,
            // 'thisYear'=>$TotalThisYear
        ];
        return view('admin.report.member',compact('datas','title','type','listTotal'));
    }
}