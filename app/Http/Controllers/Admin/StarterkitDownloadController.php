<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\StarterkitDownload;

class StarterkitDownloadController extends Controller
{
    public function index()
    {
        $no = 1;
        $showdata = StarterkitDownload::get();
        return view('admin.starterkit-downloads.index', [
            'showdata' => $showdata,
            'no' => $no,
        ]);
    }

    public function create()
    {
        return view('admin.starterkit-downloads.create');
    }

    public function show($id)
    {
        $update = StarterkitDownload::find($id);
        return view('admin.starterkit-downloads.update', [
            'update' => $update,
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'link_name' => ['required'],
            'url_download' => ['required'],
        ]);

        for ($i = 0; $i < count($request->link_name); $i++) {
            $insert[] = [
                'link_name' => $request->link_name[$i],
                'url_download' => $request->url_download[$i],
                'created_at' => date('Y-m-d H:i:s'),
            ];
        }

        StarterkitDownload::insert($insert);
        return redirect()->route('download.show')
                        ->with('success','Starterkit Download created successfully');
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'link_name' => ['required'],
            'url_download' => ['required'],
        ]);

        StarterkitDownload::where('id', $id)->update([
            'link_name' => $request->link_name,
            'url_download' => $request->url_download,
        ]);

        return redirect()->route('download.show')
                        ->with('success','Starterkit Download Updated successfully');
    }

    public function destroy($id)
    {
        StarterkitDownload::where('id', $id)->delete();
        return redirect()->route('download.show')
                        ->with('success','Starterkit Download Deketed successfully');
    }
}
