<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\WhatsAppTemplate;

class WhatsAppTemplateController extends Controller
{
    public function index()
    {
        $no=1;
        $whatsapp_template = WhatsAppTemplate::get();
        return view('admin.whatsApp.index', [
            'whatsapp_template' => $whatsapp_template,
            'no' => $no
        ]);
    }

    public function edit($id)
    {
        $whatsapp_template = WhatsAppTemplate::find($id);
        return view('admin.whatsApp.update', [
            'whatsapp_template' => $whatsapp_template
        ]);
    }

    public function update(Request $request, $id)
    {
        $update = WhatsAppTemplate::where('id', $id)->update([
            'message' => $request->message,
        ]);

        return redirect()->route('wa.show')
                        ->with('success','Updated WhatsApp Message successfully');
    }
}
