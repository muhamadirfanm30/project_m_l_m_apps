<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
USE App\Model\LandingPage;

class LandingPageController extends Controller
{
    // public function index()
    // {
    //     $page_1 = LandingPage::where('is_landing_page', 1)->first();
    //     $page_2 = LandingPage::where('is_landing_page', 2)->first();
    //     $page_3 = LandingPage::where('is_landing_page', 3)->first();
    //     $page_4 = LandingPage::where('is_landing_page', 4)->first();
    //     $page_5 = LandingPage::where('is_landing_page', 5)->first();
    //     $page_6 = LandingPage::where('is_landing_page', 6)->first();
    //     return view('admin.landingPage.show', [
    //         'page_1' => $page_1,
    //         'page_2' => $page_2,
    //         'page_3' => $page_3,
    //         'page_4' => $page_4,
    //         'page_5' => $page_5,
    //         'page_6' => $page_6,
    //     ]);
    // }

    public function showList()
    {
        $getList = LandingPage::get();
        $no = 1;
        return view('admin.landingPage.show_list',[
            'getList' => $getList,
            'no' => $no,
        ]);
    }

    public function destroy($id)
    {
        LandingPage::find($id)->delete();
        return redirect()->route('landing-page.show.list')->with('success', 'Template Deleted Successfully');
    }

    public function create()
    {
        return view('admin.landingPage.create_template');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'is_judul_landing_page' => 'required|unique:landing_pages',
            'is_landing_page' => 'required',
        ]);

        $save_judul = LandingPage::create([
            'is_judul_landing_page' => $request->is_judul_landing_page,
            'is_landing_page' => $request->is_landing_page

        ]);

        return redirect('/admin/landing-page/update/'.$save_judul->id)->with('success', 'Data Berhasil Dibuat');
    }

    public function update($id)
    {
        $get_template = LandingPage::where('id', $id)->first();
        if ($get_template->is_landing_page == 1) {
            $page_1 = LandingPage::where('id', $id)->first();
            return view('admin.landingPage.page1Update',[
                'page_1' => $page_1
            ]);
        }elseif ($get_template->is_landing_page == 2) {
            $page_2 = LandingPage::where('id', $id)->first();
            return view('admin.landingPage.page2Update',[
                'page_2' => $page_2
            ]);
        }elseif ($get_template->is_landing_page == 3) {
            $page_3 = LandingPage::where('id', $id)->first();
            return view('admin.landingPage.page3Update',[
                'page_3' => $page_3
            ]);
        } elseif ($get_template->is_landing_page == 4) {
            $page_4 = LandingPage::where('id', $id)->first();
            return view('admin.landingPage.page4Update',[
                'page_4' => $page_4
            ]);
        } elseif ($get_template->is_landing_page == 5) {
            $page_5 = LandingPage::where('id', $id)->first();
            return view('admin.landingPage.page5Update',[
                'page_5' => $page_5
            ]);
        } elseif ($get_template->is_landing_page == 6) {
            $page_6 = LandingPage::where('id', $id)->first();
            return view('admin.landingPage.page6Update',[
                'page_6' => $page_6
            ]);
        }   else {
            return redirect()->back()->with('error', 'Trmplate Tidak Ditemukan');
        }
        
    }
   
}
