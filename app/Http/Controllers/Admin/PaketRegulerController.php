<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\PaketReguler;
use App\Helpers\ImageUpload;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class PaketRegulerController extends Controller
{
    public function index()
    {
        $no=1;
        $paketRegulerList = PaketReguler::orderBy('id', 'DESC')->get();
        return view('admin.paketReguler.index', [
            'paketRegulerList' => $paketRegulerList,
            'no' => $no
        ]);
    }

    public function create()
    {
        return view('admin.paketReguler.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'nama_paket' => 'required',
            'harga' => 'required',
            'stok' => 'required',
            'deskripsi_produk' => 'required',
            'image_produk' => 'required',
            'berat' => 'required',
            'panjang' => 'required',
            'lebar' => 'required',
            'tinggi' => 'required',
        ]);
        
        $path = PaketReguler::getImagePathUpload();
        $filename = null;

        if ($request->image_produk != null) {
            $image = (new ImageUpload)->upload($request->image_produk, $path);
            $filename = $image->getFilename();
        }
        

        $storeSalTraining = PaketReguler::create([
            'nama_paket' => $request->nama_paket,
            'harga' => $request->harga,
            'image_produk' => $filename,
            'stok' => $request->stok,
            'deskripsi_produk' => $request->deskripsi_produk,
            'berat' => $request->berat,
            'panjang' => $request->panjang,
            'lebar' => $request->lebar,
            'tinggi' => $request->tinggi,
        ]);

        return redirect()->route('PR.show')
                        ->with('success','Produk Reguler created successfully');
    }

    public function edit($id)
    {
        $detailPaketReguler = PaketReguler::where('id', $id)->first();
        return view('admin.paketReguler.update', [
            'detailPaketReguler' => $detailPaketReguler
        ]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama_paket' => 'required',
            'harga' => 'required',
            'stok' => 'required',
            'deskripsi_produk' => 'required',
            // 'image_produk' => 'required',
            'berat' => 'required',
            'panjang' => 'required',
            'lebar' => 'required',
            'tinggi' => 'required',
        ]);

        $detailPaketReguler = PaketReguler::where('id', $id)->first();

        if(!empty($request->image_produk)){
            if ($request->hasFile('image_produk')) {
                $image      = $request->file('image_produk');
                $file_name   = time() . '.' . $image->getClientOriginalExtension();
                $img = Image::make($image);
                $img->stream(); // <-- Key point
                Storage::disk('local')->put('public/paketRegulers/' . $file_name, $img);
    
                $exists = Storage::disk('local')->has('public/paketRegulers/' . $detailPaketReguler->image_produk);
                if ($exists) {
                    Storage::delete('public/paketRegulers/' . $detailPaketReguler->image_produk);
                }
            }
        }else{
            $file_name = $detailPaketReguler->image_produk;
        }

        

        $storeSalTraining = PaketReguler::where('id', $id)->update([
            'nama_paket' => $request->nama_paket,
            'harga' => $request->harga,
            'image_produk' => $file_name,
            'stok' => $request->stok,
            'deskripsi_produk' => $request->deskripsi_produk,
            'berat' => $request->berat,
            'panjang' => $request->panjang,
            'lebar' => $request->lebar,
            'tinggi' => $request->tinggi,
        ]);

        return redirect()->route('PR.show')
                        ->with('success','Paket Reguler created successfully');
    }

    public function destroy($id)
    {
        $detailPaketReguler = PaketReguler::where('id', $id)->first();
        $exists = Storage::disk('local')->has('public/paketRegulers/' . $detailPaketReguler->image_produk);
        if ($exists) {
            Storage::delete('public/paketRegulers/' . $detailPaketReguler->image_produk);
        }
        PaketReguler::find($id)->delete();
        return redirect()->route('PR.show')
                        ->with('success','Paket Reguler deleted successfully');
    }

    public function detail($id)
    {
        $detailPaketReguler = PaketReguler::where('id', $id)->first();
        return view('admin.paketReguler.detail', [
            'detailPaketReguler' => $detailPaketReguler
        ]);
    }
}
