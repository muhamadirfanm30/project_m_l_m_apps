<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\TransactionItem;
use App\Model\ItemTransaction;
use App\Model\Transaction;
use App\Model\Product;
use Carbon\Carbon;
use App\User;
use DB;

class DasboardAdminController extends Controller
{
    public function index($type = "Monthly", Request $request)
    {
        $datas = [];
        $title = [];
        switch ($type) {
            case 'Weekly':
                $arrayDate = [
                    'Week 1'=> [ '01','07' ],
                    'Week 2'=> [ '08','14' ],
                    'Week 3'=> [ '15','21' ],
                    'Week 4'=> [ '22','28' ],
                    'Week 5'=> [ '29','31' ]
                ];

                foreach ($arrayDate as $key => $value) {
                    $data = Transaction::selectRaw(" '$key' as week, SUM(totalKeseluruhan) as total")
                                            ->where('status',1)
                                            ->whereRaw('created_at BETWEEN "'. date('Y-m-'.$value[0]) .'" AND "'. date('Y-m-'.$value[1]).'"')
                                            ->first()
                                            ->toArray();
                    if($data['total'] > 0){
                        $datas[] = $data;
                    }
                }
                $title = array_column($datas, 'week');
                break;
            case 'Monthly':
                $datas = Transaction::selectRaw("MONTH(created_at) as month, SUM(totalKeseluruhan) as total")
                            ->where('status',1)
                            ->whereBetween('created_at',[date('Y').'-01-01',date('Y').'-12-30'])
                            ->groupBy(DB::raw("MONTH(created_at)"))
                            ->get()
                            ->toArray();
                foreach ($datas as $key) {
                    $date = Carbon::parse(Date('2020-'.$key['month'].'-01'));
                    $title[].= $date->format('F');
                }
                break;
            case 'Yearly':
                $subYear = Date('Y') - 5;
                $datas = Transaction::selectRaw("YEAR(created_at) as year, SUM(totalKeseluruhan) as total")
                            ->where('status',1)
                            ->whereRaw('created_at BETWEEN '. $subYear .' AND '. Date('Y'))
                            ->groupBy(DB::raw("YEAR(created_at)"))
                            ->get()
                            ->toArray();
                $title = array_column($datas, 'year');
                break;
        }
        $TotalThisMonth = 0;
        $TotalThisYear = 0;
        $TotalPrevMonth = 0;
        $percentage = 0;
        $allTotalThisMonth = Transaction::selectRaw("count(*) as total")
                            ->where('status',1)
                            ->whereMonth('created_at',date('m'))
                            ->whereYear('created_at',date('Y'))
                            ->first();
        if($allTotalThisMonth) $TotalThisMonth = $allTotalThisMonth->total;
        
        $allTotalThisYear = Transaction::selectRaw("count(*) as total")
                            ->where('status',1)
                            ->whereYear('created_at',date('Y'))
                            ->first();
        if($allTotalThisYear) $TotalThisYear = $allTotalThisYear->total;

        $allTotalPrevMonth = Transaction::selectRaw("count(*) as total")
                            ->where('status',1)
                            ->whereMonth('created_at', (date('m') == 1 ? 12 : (date('m') - 1)) )
                            ->whereYear('created_at',date('Y'))
                            ->first();
        if($allTotalPrevMonth) $TotalPrevMonth = $allTotalPrevMonth->total;
        
        // ((Total sebelumnya - Total sekarang) / total sebelumnya) * 100
        // dd([
        //     'a'=>$TotalThisMonth,
        //     'b'=>$TotalPrevMonth
        // ]);
        if($TotalThisMonth != 0 && $TotalPrevMonth != 0){
            if($TotalThisMonth > $TotalThisMonth){
                $percentage = ($TotalPrevMonth / $TotalThisMonth) * 100;
            }else{
                if($TotalPrevMonth > 0){
                    $percentage = ($TotalThisMonth / $TotalPrevMonth) * 100;
                }else{
                    $percentage = 0;
                }
            }
        }
        $listTotal = [
            'prevMonth'=>$TotalPrevMonth,
            'thisMonth'=>$TotalThisMonth,
            'percentage'=> ($TotalPrevMonth > $TotalThisMonth ? '-' : '+').' '.$percentage.'%' ,
            'thisYear'=>$TotalThisYear
        ];

        $getSales =  ItemTransaction::where('product_id',1)->whereMonth('created_at',date('m'))->whereYear('created_at',date('Y'))->get();
        $total_produk_terjual = 0;
        foreach($getSales as $sales){
            $total_produk_terjual += $sales->qty;
        }

        // return $total_produk_terjual;
        $showProduct = Product::take(4)->get();
        $memberMS = User::where('membersip_status', 'MS (Mobile Stokiest)')->count();
        $memberReguler = User::where('membersip_status', 'Reguler')->count();
        $memberAktif = User::where('status', '0')->count();
        return view('admin.dashboard',compact('datas','title','type','listTotal', 'showProduct', 'memberMS', 'memberReguler', 'memberAktif', 'total_produk_terjual'));
    }
}
