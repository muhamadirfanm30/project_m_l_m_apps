<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\SubKategori;
class SubKategoriController extends Controller
{
    public function index(){
        $data = SubKategori::orderBy('nama','asc')->get();
        return view('admin.sub-kategori.index',compact('data'));
    }

    public function store(Request $request){
        if($request->isMethod('post')){
            $this->validate($request, [
                'nama' => 'required',
            ]);
            $field = $request->only(['nama']);

            $save = SubKategori::create($field);

            return redirect()->route('sub-kategori')->with('success','Data created successfully');
        }
        return view('admin.sub-kategori.create');
    }

    public function edit(SubKategori $id,Request $request){
        if($request->isMethod('post')){
            $this->validate($request, [
                'nama' => 'required',
            ]);
            $field = $request->only(['nama']);
        
            $id->update($field);
            return redirect()->route('sub-kategori')->with('success','Data updated successfully');
        }
        return view('admin.sub-kategori.edit',compact('id'));
    }

    public function destroy(SubKategori $id){
        if($id->delete()){
            return redirect()->route('sub-kategori')
                            ->with('success','Data has been deleted');
        }else{
            return redirect()->route('sub-kategori')
                            ->with('failed','Failed delete data');
        }
    }
}
