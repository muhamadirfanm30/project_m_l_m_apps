<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\GeneralSetting;
use DB;

class GeneralSettingController extends Controller
{
    public function index()
    {
        $city = DB::table('city')->get();
        $cek = GeneralSetting::get();
        return view('admin.general-setting.createAndUpdate', [
            'cek' => $cek,
            'city' => $city,
        ]);
    }

    public function updateSetting(Request $request)
    {
        $value = $request->value;
        $type = $request->type;
        $ids = $request->id_setting;

        foreach ($value as $key => $values) {
            $id = $ids[$key];
            GeneralSetting::where('id', $id)->update([
                'value' => $value[$key],
                'type' => $type[$key],
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        }  
        
        // for ($i=0; $i < count($request->value) ; $i++) 
        // { 
        //     DB::table('general_settings')->where('id',$request->id_setting[$i])->update(
        //         [
        //             'value' => $request->value[$i],
        //         ]
        //     );
            return redirect()->route('general-setting.show')
                        ->with('success','Pengaturan Umum Berhasil diubah');
        // } 
    }
}
