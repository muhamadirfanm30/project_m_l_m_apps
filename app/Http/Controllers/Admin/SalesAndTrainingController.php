<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\SalesAndTraining;
use App\Model\ModelVidio;
use Illuminate\Support\Facades\Storage;
use DB;
use App\Helpers\ImageUpload;
use Intervention\Image\ImageManagerStatic as Image;

class SalesAndTrainingController extends Controller
{
    public function index()
    {
        $showListSales = SalesAndTraining::orderBy('id','DESC')->get();
        // return $showListSales;
        $no =1;
        return view('admin.salesAndTraining.index', [
            'showListSales' => $showListSales,
            'no' => $no
        ]);
    }

    public function getVidio($id)
    {
        return ModelVidio::where('id', $id)->first();
    }

    public function create()
    {
        return view('admin.salesAndTraining.create');
    }

    public function edit($id)
    {
        $getDataSales = SalesAndTraining::where('id', $id)->first();
        $getVidio = ModelVidio::where('parent_id', $id)->get();
        return view('admin.salesAndTraining.update', [
            'getDataSales' => $getDataSales,
            'getVidio' => $getVidio
        ]);
    }

    public function detail($id)
    {
        $getData = SalesAndTraining::where('id', $id)->first();
        $getVidio = ModelVidio::where('parent_id', $id)->get();
        return view('admin.salesAndTraining.detail', [
            'getData' => $getData,
            'getVidio' => $getVidio
        ]);
    }

    public function destroy(Request $request, $id)
    {
        $salesTraining = SalesAndTraining::where('id', $id)->first();
        $exists = Storage::disk('local')->has('public/salesTrainingImages/' . $salesTraining->image_produk);
        if ($exists) {
            Storage::delete('public/salesTrainingImages/' . $salesTraining->image_produk);
        }

        ModelVidio::whereIn('id', [$request->id])->delete();
        SalesAndTraining::find($id)->delete();
        return redirect()->route('sales.show')
                        ->with('success','Sales Training deleted successfully');
    }

    public function destroyVidio(Request $request, $id)
    {
        ModelVidio::where('id', $request->id)->delete();
        return redirect()->back()->with('success','Url And Title Vidio Created successfully');
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'judul' => 'required',
            'deskripsi' => 'required',
            'publish_at' => 'required',
        ]);

        try {
            $getId = SalesAndTraining::where('id', $id)->first();
            if(!empty($request->foto)){
                if ($request->hasFile('foto')) {
                    $image      = $request->file('foto');
                    $file_name   = time() . '.' . $image->getClientOriginalExtension();
                    $img = Image::make($image);
                    $img->stream(); // <-- Key point
                    Storage::disk('local')->put('public/salesTrainingImages/' . $file_name, $img);
        
                    $exists = Storage::disk('local')->has('public/salesTrainingImages/' . $getId->foto);
                    if ($exists) {
                        Storage::delete('public/salesTrainingImages/' . $getId->foto);
                    }
                }
            }else{
                $file_name = $getId->foto;
            }
            

            $storeSalTraining = SalesAndTraining::where('id', $id)->update([
                'judul' => $request->judul,
                'deskripsi' => $request->deskripsi,
                'foto' => $file_name,
                'publish_at' => $request->publish_at,
            ]);

            DB::commit();
            return redirect()->route('sales.show')
                            ->with('success','Sales Training created successfully');
        } catch (Exception $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }
    }

    public function updateVidio(Request $request, $id)
    {
        ModelVidio::where('id', $request->id)->update([
            'judul_vidio' => $request->judul_vidio,
            'url_vidio' => $request->url_vidio,
        ]);
        return redirect()->back()->with('success','Url And Title Vidio Created successfully');
    }

    public function storeVidio(Request $request)
    {
        ModelVidio::create([
            'judul_vidio' => $request->judul_vidio,
            'url_vidio' => $request->url_vidio,
            'parent_id' => $request->parent_id,
        ]);
        return redirect()->back()->with('success','Url And Title Vidio Created successfully');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'judul' => 'required',
            'deskripsi' => 'required',
            'foto' => 'required|file|max:7000',
            'publish_at' => 'required',
            // 'judul_vidio[]' => 'required',
            // 'url_vidio[]' => 'required',
            // 'parent_id' => 'required',
        ]);

        try {
            $path = SalesAndTraining::getImagePathUpload();
            $filename = null;

            if ($request->foto != null) {
                $image = (new ImageUpload)->upload($request->foto, $path);
                $filename = $image->getFilename();
            }

            $storeSalTraining = SalesAndTraining::create([
                'judul' => $request->judul,
                'deskripsi' => $request->deskripsi,
                'foto' => $filename,
                'publish_at' => $request->publish_at,
            ]);

            // return $storeSalTraining;
            for ($i = 0; $i < count($request->judul_vidio); $i++) {
                $answers[] = [
                    'judul_vidio' => $request->judul_vidio[$i],
                    'url_vidio' => $request->url_vidio[$i],
                    'parent_id' => $storeSalTraining->id
                ];
            }
            ModelVidio::insert($answers);

            DB::commit();
            return redirect()->route('sales.show')
                            ->with('success','Sales Training created successfully');
        } catch (Exception $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }

        
    }
}
