<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Helpers\ImageUpload;
use Intervention\Image\ImageManagerStatic as Image;
use Storage;
use App\Model\Kategori;
class KategoriController extends Controller
{
    public function index(){
        $data = Kategori::orderBy('judul','asc')->get();
        return view('admin.kategori.index',compact('data'));
    }

    public function store(Request $request){
        if($request->isMethod('post')){
            $this->validate($request, [
                'judul' => 'required',
                'foto' => 'required',
                'deskripsi' => 'required',
            ]);
            $field = $request->only(['judul','deskripsi']);
            
            if ($request->hasFile('foto')) {
                $image = (new ImageUpload)->upload($request->foto, 'public/kategori');
                $filename = $image->getFilename();
                $field['foto'] = $filename;
            }

            $save = Kategori::create($field);

            return redirect()->route('kategori')->with('success','Data created successfully');
        }
        return view('admin.kategori.create');
    }

    public function edit(Kategori $id,Request $request){
        if($request->isMethod('post')){
            $this->validate($request, [
                'judul' => 'required',
                'deskripsi' => 'required',
            ]);
            $field = $request->only(['judul','deskripsi']);
            
            if ($request->hasFile('foto')) {
                $image = (new ImageUpload)->upload($request->foto, 'public/kategori');
                $filename = $image->getFilename();
                $field['foto'] = $filename;
            }

            $id->update($field);
            return redirect()->route('kategori')->with('success','Data updated successfully');
        }
        return view('admin.kategori.edit',compact('id'));
    }

    public function destroy(Kategori $id){
        if($id->delete()){
            return redirect()->route('kategori')
                            ->with('success','Data has been deleted');
        }else{
            return redirect()->route('kategori')
                            ->with('failed','Failed delete data');
        }
    }
}
