<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Kurir;

class KurirController extends Controller
{
    public function index()
    {
        $no = 1;
        $showKurir = Kurir::get();
        return view('admin.kurir.index', [
            'showKurir' => $showKurir,
            'no' => $no,
        ]);
    }

    public function update($id)
    {
        $dataEdit = Kurir::find($id);
        return view('admin.kurir.update', [
            'dataEdit' => $dataEdit,
        ]);
    }

    public function edit(Request $request, $id)
    {
        $this->validate($request, [
            'text' => 'required',
        ]);

        Kurir::find($id)->update([
            'text' => $request->text,
            'is_active' => $request->is_active,
        ]);

        return redirect()->route('kurir.show')->with('success', 'Kurir Berhasil diubah');
    }
}
