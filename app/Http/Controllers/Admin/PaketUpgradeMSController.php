<?php

namespace App\Http\Controllers\Admin;

use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use App\Model\PaketUpgradeMS;
use App\Helpers\ImageUpload;
use Illuminate\Http\Request;
use App\Model\PaketReguler;

class PaketUpgradeMSController extends Controller
{
    public function index()
    {
        $no=1;
        $show = PaketUpgradeMS::orderBy('id', 'DESC')->get();
        return view('admin.paketUpgradeMS.index', [
            'show' => $show,
            'no' => $no,
        ]);
    }

    public function create()
    {
        return view('admin.paketUpgradeMS.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'nama_paket' => 'required',
            'harga' => 'required',
            'stok' => 'required',
            'deskripsi_produk' => 'required',
            'image_produk' => 'required',
            'berat' => 'required',
            'panjang' => 'required',
            'lebar' => 'required',
            'tinggi' => 'required',
        ]);
        
        $path = PaketUpgradeMS::getImagePathUpload();
        $filename = null;

        if ($request->image_produk != null) {
            $image = (new ImageUpload)->upload($request->image_produk, $path);
            $filename = $image->getFilename();
        }
        

        $storeSalTraining = PaketUpgradeMS::create([
            'nama_paket' => $request->nama_paket,
            'harga' => $request->harga,
            'image_produk' => $filename,
            'stok' => $request->stok,
            'deskripsi_produk' => $request->deskripsi_produk,
            'berat' => $request->berat,
            'panjang' => $request->panjang,
            'lebar' => $request->lebar,
            'tinggi' => $request->tinggi,
        ]);

        return redirect()->route('paket.upgrade.show')
                        ->with('success','Produk Upgrade MS created successfully');
    }

    public function edit($id)
    {
        $detailPaketReguler = PaketUpgradeMS::where('id', $id)->first();
        return view('admin.paketUpgradeMS.update', [
            'detailPaketReguler' => $detailPaketReguler
        ]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama_paket' => 'required',
            'harga' => 'required',
            'stok' => 'required',
            'deskripsi_produk' => 'required',
            // 'image_produk' => 'required',
            'berat' => 'required',
            'panjang' => 'required',
            'lebar' => 'required',
            'tinggi' => 'required',
        ]);

        $detailPaketReguler = PaketUpgradeMS::where('id', $id)->first();

        if(!empty($request->image_produk)){
            if ($request->hasFile('image_produk')) {
                $image      = $request->file('image_produk');
                $file_name   = time() . '.' . $image->getClientOriginalExtension();
                $img = Image::make($image);
                $img->stream(); // <-- Key point
                Storage::disk('local')->put('public/paketUpdateMS/' . $file_name, $img);
    
                $exists = Storage::disk('local')->has('public/paketUpdateMS/' . $detailPaketReguler->image_produk);
                if ($exists) {
                    Storage::delete('public/paketUpdateMS/' . $detailPaketReguler->image_produk);
                }
            }
        }else{
            $file_name = $detailPaketReguler->image_produk;
        }

        

        $storeSalTraining = PaketUpgradeMS::where('id', $id)->update([
            'nama_paket' => $request->nama_paket,
            'harga' => $request->harga,
            'image_produk' => $file_name,
            'stok' => $request->stok,
            'deskripsi_produk' => $request->deskripsi_produk,
            'berat' => $request->berat,
            'panjang' => $request->panjang,
            'lebar' => $request->lebar,
            'tinggi' => $request->tinggi,
        ]);

        return redirect()->route('paket.upgrade.show')
                        ->with('success','Paket Upgrade created successfully');
    }

    public function destroy($id)
    {
        $detailPaketReguler = PaketUpgradeMS::where('id', $id)->first();
        $exists = Storage::disk('local')->has('public/paketUpdateMS/' . $detailPaketReguler->image_produk);
        if ($exists) {
            Storage::delete('public/paketUpdateMS/' . $detailPaketReguler->image_produk);
        }
        PaketUpgradeMS::find($id)->delete();
        return redirect()->route('paket.upgrade.show')
                        ->with('success','Paket Upgrade deleted successfully');
    }

    public function detail($id)
    {
        $detailPaketReguler = PaketUpgradeMS::where('id', $id)->first();
        return view('admin.paketUpgradeMS.detail', [
            'detailPaketReguler' => $detailPaketReguler
        ]);
    }
}
