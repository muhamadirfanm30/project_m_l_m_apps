<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\BonusMS;

class BonusMSController extends Controller
{
    public function index()
    {
        $no = 1;
        $listBonusMs = BonusMS::orderBy('id', 'DESC')->get();
        return view('admin.bonusMs.index', [
            'listBonusMs' => $listBonusMs,
            'no' => $no,
        ]);
    }

    public function create()
    {
        return view('admin.bonusMs.create');
    }

    public function edit($id)
    {
        $listBonusMs = BonusMS::where('id', $id)->first();
        return view('admin.bonusMs.update', [
            'listBonusMs' => $listBonusMs
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'judul' => ['required'],
            'vidio' => ['required'],
        ]);

        for ($i = 0; $i < count($request->judul); $i++) {
            $answers[] = [
                'judul' => $request->judul[$i],
                'vidio' => $request->vidio[$i],
                'created_at' => date('Y-m-d H:i:s'),
            ];
        }

        BonusMS::insert($answers);
        return redirect()->route('BonusMS.show')
                        ->with('success','Bonus MS created successfully');
        
    }

    public function update(Request $request, $id)
    {
        BonusMs::where('id', $id)->update([
            'judul' => $request->judul,
            'vidio' => $request->vidio
        ]);

        return redirect()->route('BonusMS.show')
                        ->with('success','Update Bonus MS  successfully');
    }

    public function teruncate()
    {
        BonusMS::truncate();
        return redirect()->route('BonusMS.show')
                        ->with('success','Truncate Bonus MS  successfully');
    }

    public function destroy($id)
    {
        BonusMS::where('id', $id)->delete();
        return redirect()->route('BonusMS.show')
                        ->with('success','Delete Bonus MS  successfully');
    }
}
