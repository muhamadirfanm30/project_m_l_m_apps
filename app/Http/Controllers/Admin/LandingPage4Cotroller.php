<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\LandingPage;
use App\Helpers\ImageUpload;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class LandingPage4Cotroller extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [
            'image_1' => 'max:1024',
            'image_2' => 'max:1024',
            'image_3' => 'max:1024',
            'image_4' => 'max:1024',
            'image_5' => 'max:1024',
            'image_6' => 'max:1024',
            'image_7' => 'max:1024',
            'image_8' => 'max:1024',
            'image_9' => 'max:1024',
            'image_10' => 'max:1024',
            'image_11' => 'max:1024',
            'is_judul_landing_page' => 'required',
            // 'judul_vidio_embed' => 'required',
            // 'embed_link_vidio' => 'required',
            // 'nama_button_1' => 'required',
            // 'judul_1' => 'required',
            // 'judul_2' => 'required',
            // 'judul_3' => 'required',
            // 'nama_user_1' => 'required',
            // 'url_vidio_user_1' => 'required',
            // 'deskripsi_user_1' => 'required',
            // 'nama_user_2' => 'required',
            // 'url_vidio_user_2' => 'required',
            // 'deskripsi_user_2' => 'required',
            // 'nama_user_3' => 'required',
            // 'url_vidio_user_3' => 'required',
            // 'deskripsi_user_3' => 'required',
            // 'nama_user_4' => 'required',
            // 'url_vidio_user_4' => 'required',
            // 'deskripsi_user_4' => 'required',
            // 'nama_user_5' => 'required',
            // 'url_vidio_user_5' => 'required',
            // 'deskripsi_user_5' => 'required',
            // 'nama_user_6' => 'required',
            // 'url_vidio_user_6' => 'required',
            // 'deskripsi_user_6' => 'required',
            // 'nama_button_2' => 'required',
            // 'nama_button_3' => 'required',
            // 'nama_button_4' => 'required',
            // 'judul_accordion_1' => 'required',
            // 'deskripsi_accordion_1' => 'required',
            // 'judul_accordion_2' => 'required',
            // 'deskripsi_accordion_2' => 'required',
            // 'judul_accordion_3' => 'required',
            // 'deskripsi_accordion_3' => 'required',
            // 'judul_accordion_4' => 'required',
            // 'deskripsi_accordion_4' => 'required',
        ]);

        $path = LandingPage::getImagePathUpload();
        $filename = null;

        if ($request->image_1 != null) {
            $image_1 = (new ImageUpload)->upload($request->image_1, $path);
            $filename_1 = $image_1->getFilename();
        }else{
            $filename_1 = '';
        }
        if ($request->image_2 != null) {
            $image_2 = (new ImageUpload)->upload($request->image_2, $path);
            $filename_2 = $image_2->getFilename();
        }else{
            $filename_2 = '';
        }
        if ($request->image_3 != null) {
            $image_3 = (new ImageUpload)->upload($request->image_3, $path);
            $filename_3 = $image_3->getFilename();
        }else{
            $filename_3 = '';
        }
        if ($request->image_4 != null) {
            $image_4 = (new ImageUpload)->upload($request->image_4, $path);
            $filename_4 = $image_4->getFilename();
        }else{
            $filename_4 = '';
        }
        if ($request->image_5 != null) {
            $image_5 = (new ImageUpload)->upload($request->image_5, $path);
            $filename_5 = $image_5->getFilename();
        }else{
            $filename_5 = '';
        }
        if ($request->image_6 != null) {
            $image_6 = (new ImageUpload)->upload($request->image_6, $path);
            $filename_6 = $image_6->getFilename();
        }else{
            $filename_6 = '';
        }
        if ($request->image_7 != null) {
            $image_7 = (new ImageUpload)->upload($request->image_7, $path);
            $filename_7 = $image_7->getFilename();
        }else{
            $filename_7 = '';
        }
        if ($request->image_8 != null) {
            $image_8 = (new ImageUpload)->upload($request->image_8, $path);
            $filename_8 = $image_8->getFilename();
        }else{
            $filename_8 = '';
        }
        if ($request->image_9 != null) {
            $image_9 = (new ImageUpload)->upload($request->image_9, $path);
            $filename_9 = $image_9->getFilename();
        }else{
            $filename_9 = '';
        }
        if ($request->image_10 != null) {
            $image_10 = (new ImageUpload)->upload($request->image_10, $path);
            $filename_10 = $image_10->getFilename();
        }else{
            $filename_10 = '';
        }
        if ($request->image_11 != null) {
            $image_11 = (new ImageUpload)->upload($request->image_11, $path);
            $filename_11 = $image_11->getFilename();
        }else{
            $filename_11 = '';
        }

        $created = [
            'is_landing_page' => 4,
            'image_1' => $filename_1,
            'image_2' => $filename_2,
            'image_3' => $filename_3,
            'image_4' => $filename_4,
            'image_5' => $filename_5,
            'image_6' => $filename_6,
            'image_7' => $filename_7,
            'image_8' => $filename_8,
            'image_9' => $filename_9,
            'image_10' => $filename_10,
            'image_11' => $filename_11,
            'judul_vidio_embed' => $request->judul_vidio_embed,
            'embed_link_vidio' => $request->embed_link_vidio,
            'nama_button_1' => $request->nama_button_1,
            'judul_1' => $request->judul_1,
            'judul_2' => $request->judul_2,
            'judul_3' => $request->judul_3,
            'nama_user_1' => $request->nama_user_1,
            'url_vidio_user_1' => $request->url_vidio_user_1,
            'deskripsi_user_1' => $request->deskripsi_user_1,
            'nama_user_2' => $request->nama_user_2,
            'url_vidio_user_2' => $request->url_vidio_user_2,
            'deskripsi_user_2' => $request->deskripsi_user_2,
            'nama_user_3' => $request->nama_user_3,
            'url_vidio_user_3' => $request->url_vidio_user_3,
            'deskripsi_user_3' => $request->deskripsi_user_3,
            'nama_user_4' => $request->nama_user_4,
            'url_vidio_user_4' => $request->url_vidio_user_4,
            'deskripsi_user_4' => $request->deskripsi_user_4,
            'nama_user_5' => $request->nama_user_5,
            'url_vidio_user_5' => $request->url_vidio_user_5,
            'deskripsi_user_5' => $request->deskripsi_user_5,
            'nama_user_6' => $request->nama_user_6,
            'url_vidio_user_6' => $request->url_vidio_user_6,
            'deskripsi_user_6' => $request->deskripsi_user_6,
            'nama_button_2' => $request->nama_button_2,
            'nama_button_3' => $request->nama_button_3,
            'nama_button_4' => $request->nama_button_4,
            'judul_accordion_1' => $request->judul_accordion_1,
            'deskripsi_accordion_1' => $request->deskripsi_accordion_1,
            'judul_accordion_2' => $request->judul_accordion_2,
            'deskripsi_accordion_2' => $request->deskripsi_accordion_2,
            'judul_accordion_3' => $request->judul_accordion_3,
            'deskripsi_accordion_3' => $request->deskripsi_accordion_3,
            'judul_accordion_4' => $request->judul_accordion_4,
            'deskripsi_accordion_4' => $request->deskripsi_accordion_4,
            'is_judul_landing_page' => $request->is_judul_landing_page
        ];
        LandingPage::create($created);
        return redirect()->route('landing-page.show.list')
                        ->with('success','Template 4 Update successfully');
    }

    public function update(Request $request, $id)
    {
        // return $request->all();
        $this->validate($request, [
            'image_1' => 'max:1024',
            'image_2' => 'max:1024',
            'image_3' => 'max:1024',
            'image_4' => 'max:1024',
            'image_5' => 'max:1024',
            'image_6' => 'max:1024',
            'image_7' => 'max:1024',
            'image_8' => 'max:1024',
            'image_9' => 'max:1024',
            'image_10' => 'max:1024',
            'image_11' => 'max:1024',
            'is_judul_landing_page' => 'required',
            // 'judul_vidio_embed' => 'required',
            // 'embed_link_vidio' => 'required',
            // 'nama_button_1' => 'required',
            // 'judul_1' => 'required',
            // 'judul_2' => 'required',
            // 'judul_3' => 'required',
            // 'nama_user_1' => 'required',
            // 'url_vidio_user_1' => 'required',
            // 'deskripsi_user_1' => 'required',
            // 'nama_user_2' => 'required',
            // 'url_vidio_user_2' => 'required',
            // 'deskripsi_user_2' => 'required',
            // 'nama_user_3' => 'required',
            // 'url_vidio_user_3' => 'required',
            // 'deskripsi_user_3' => 'required',
            // 'nama_user_4' => 'required',
            // 'url_vidio_user_4' => 'required',
            // 'deskripsi_user_4' => 'required',
            // 'nama_user_5' => 'required',
            // 'url_vidio_user_5' => 'required',
            // 'deskripsi_user_5' => 'required',
            // 'nama_user_6' => 'required',
            // 'url_vidio_user_6' => 'required',
            // 'deskripsi_user_6' => 'required',
            // 'nama_button_2' => 'required',
            // 'nama_button_3' => 'required',
            // 'nama_button_4' => 'required',
            // 'judul_accordion_1' => 'required',
            // 'deskripsi_accordion_1' => 'required',
            // 'judul_accordion_2' => 'required',
            // 'deskripsi_accordion_2' => 'required',
            // 'judul_accordion_3' => 'required',
            // 'deskripsi_accordion_3' => 'required',
            // 'judul_accordion_4' => 'required',
            // 'deskripsi_accordion_4' => 'required',
        ]);

        $cek = LandingPage::where('id', $id)->first();

        if(!empty($request->image_1)){
            if ($request->hasFile('image_1')) {
                $image1      = $request->file('image_1');
                $file_name_1   = time() .'1'. '.' . $image1->getClientOriginalExtension();
                $img1 = Image::make($image1);
                $img1->stream(); // <-- Key point
                Storage::disk('local')->put('public/landing-page-images/' . $file_name_1, $img1);
    
                $exists1 = Storage::disk('local')->has('public/landing-page-images/' . $cek->image_1);
                if ($exists1) {
                    Storage::delete('public/landing-page-images/' . $cek->image_1);
                }
            }
        }else{
            $file_name_1 = $cek->image_1;
        }

        if(!empty($request->image_2)){
            if ($request->hasFile('image_2')) {
                $image2      = $request->file('image_2');
                $file_name_2   = time() .'2'. '.' . $image2->getClientOriginalExtension();
                $img2 = Image::make($image2);
                $img2->stream(); // <-- Key point
                Storage::disk('local')->put('public/landing-page-images/' . $file_name_2, $img2);
    
                $exists2 = Storage::disk('local')->has('public/landing-page-images/' . $cek->image_2);
                if ($exists2) {
                    Storage::delete('public/landing-page-images/' . $cek->image_2);
                }
            }
        }else{
            $file_name_2 = $cek->image_2;
        }
        if(!empty($request->image_3)){
            if ($request->hasFile('image_3')) {
                $image3      = $request->file('image_3');
                $file_name_3   = time() .'3'. '.' . $image3->getClientOriginalExtension();
                $img3 = Image::make($image3);
                $img3->stream(); // <-- Key point
                Storage::disk('local')->put('public/landing-page-images/' . $file_name_3, $img3);
    
                $exists3 = Storage::disk('local')->has('public/landing-page-images/' . $cek->image_3);
                if ($exists3) {
                    Storage::delete('public/landing-page-images/' . $cek->image_3);
                }
            }
        }else{
            $file_name_3 = $cek->image_3;
        }
        if(!empty($request->image_4)){
            if ($request->hasFile('image_4')) {
                $image4      = $request->file('image_4');
                $file_name_4   = time() .'4'. '.' . $image4->getClientOriginalExtension();
                $img4 = Image::make($image4);
                $img4->stream(); // <-- Key point
                Storage::disk('local')->put('public/landing-page-images/' . $file_name_4, $img4);
    
                $exists4 = Storage::disk('local')->has('public/landing-page-images/' . $cek->image_4);
                if ($exists4) {
                    Storage::delete('public/landing-page-images/' . $cek->image_4);
                }
            }
        }else{
            $file_name_4 = $cek->image_4;
        }
        if(!empty($request->image_5)){
            if ($request->hasFile('image_5')) {
                $image5      = $request->file('image_5');
                $file_name_5   = time() .'5'. '.' . $image5->getClientOriginalExtension();
                $img5 = Image::make($image5);
                $img5->stream(); // <-- Key point
                Storage::disk('local')->put('public/landing-page-images/' . $file_name_5, $img5);
    
                $exists5 = Storage::disk('local')->has('public/landing-page-images/' . $cek->image_5);
                if ($exists5) {
                    Storage::delete('public/landing-page-images/' . $cek->image_5);
                }
            }
        }else{
            $file_name_5 = $cek->image_5;
        }
        if(!empty($request->image_6)){
            if ($request->hasFile('image_6')) {
                $image6      = $request->file('image_6');
                $file_name_6   = time() .'6'. '.' . $image6->getClientOriginalExtension();
                $img6 = Image::make($image6);
                $img6->stream(); // <-- Key point
                Storage::disk('local')->put('public/landing-page-images/' . $file_name_6, $img6);
    
                $exists6 = Storage::disk('local')->has('public/landing-page-images/' . $cek->image_6);
                if ($exists6) {
                    Storage::delete('public/landing-page-images/' . $cek->image_6);
                }
            }
        }else{
            $file_name_6 = $cek->image_6;
        }
        if(!empty($request->image_7)){
            if ($request->hasFile('image_7')) {
                $image7      = $request->file('image_7');
                $file_name_7   = time() .'7'. '.' . $image7->getClientOriginalExtension();
                $img8 = Image::make($image7);
                $img8->stream(); // <-- Key point
                Storage::disk('local')->put('public/landing-page-images/' . $file_name_7, $img8);
    
                $exists7 = Storage::disk('local')->has('public/landing-page-images/' . $cek->image_7);
                if ($exists7) {
                    Storage::delete('public/landing-page-images/' . $cek->image_7);
                }
            }
        }else{
            $file_name_7 = $cek->image_7;
        }
        if(!empty($request->image_8)){
            if ($request->hasFile('image_8')) {
                $image8      = $request->file('image_8');
                $file_name_8   = time() .'8'. '.' . $image8->getClientOriginalExtension();
                $img8 = Image::make($image8);
                $img8->stream(); // <-- Key point
                Storage::disk('local')->put('public/landing-page-images/' . $file_name_8, $img8);
    
                $exists8 = Storage::disk('local')->has('public/landing-page-images/' . $cek->image_8);
                if ($exists8) {
                    Storage::delete('public/landing-page-images/' . $cek->image_8);
                }
            }
        }else{
            $file_name_8 = $cek->image_8;
        }
        if(!empty($request->image_9)){
            if ($request->hasFile('image_9')) {
                $image9      = $request->file('image_9');
                $file_name_9   = time() . '9'. '.' . $image9->getClientOriginalExtension();
                $img9 = Image::make($image9);
                $img9->stream(); // <-- Key point
                Storage::disk('local')->put('public/landing-page-images/' . $file_name_9, $img9);
    
                $exists9 = Storage::disk('local')->has('public/landing-page-images/' . $cek->image_9);
                if ($exists9) {
                    Storage::delete('public/landing-page-images/' . $cek->image_9);
                }
            }
        }else{
            $file_name_9 = $cek->image_9;
        }
        if(!empty($request->image_10)){
            if ($request->hasFile('image_10')) {
                $image10      = $request->file('image_10');
                $file_name_10   = time() . '10'. '.' . $image10->getClientOriginalExtension();
                $img10 = Image::make($image10);
                $img10->stream(); // <-- Key point
                Storage::disk('local')->put('public/landing-page-images/' . $file_name_10, $img10);
    
                $exists10 = Storage::disk('local')->has('public/landing-page-images/' . $cek->image_10);
                if ($exists10) {
                    Storage::delete('public/landing-page-images/' . $cek->image_10);
                }
            }
        }else{
            $file_name_10 = $cek->image_10;
        }
        if(!empty($request->image_11)){
            if ($request->hasFile('image_11')) {
                $image11      = $request->file('image_11');
                $file_name_11   = time() . '11'. '.' . $image11->getClientOriginalExtension();
                $img11 = Image::make($image11);
                $img11->stream(); // <-- Key point
                Storage::disk('local')->put('public/landing-page-images/' . $file_name_11, $img11);
    
                $exists11 = Storage::disk('local')->has('public/landing-page-images/' . $cek->image_11);
                if ($exists11) {
                    Storage::delete('public/landing-page-images/' . $cek->image_11);
                }
            }
        }else{
            $file_name_11 = $cek->image_11;
        }

        $updated = [
            'image_1' => $file_name_1,
            'image_2' => $file_name_2,
            'image_3' => $file_name_3,
            'image_4' => $file_name_4,
            'image_5' => $file_name_5,
            'image_6' => $file_name_6,
            'image_7' => $file_name_7,
            'image_8' => $file_name_8,
            'image_9' => $file_name_9,
            'image_10' => $file_name_10,
            'image_11' => $file_name_11,
            'judul_vidio_embed' => $request->judul_vidio_embed,
            'embed_link_vidio' => $request->embed_link_vidio,
            'nama_button_1' => $request->nama_button_1,
            'nama_button_2' => $request->nama_button_2,
            'nama_button_3' => $request->nama_button_3,
            'nama_button_4' => $request->nama_button_4,
            'judul_1' => $request->judul_1,
            'judul_2' => $request->judul_2,
            'judul_3' => $request->judul_3,
            'nama_user_1' => $request->nama_user_1,
            'url_vidio_user_1' => $request->url_vidio_user_1,
            'deskripsi_user_1' => $request->deskripsi_user_1,
            'nama_user_2' => $request->nama_user_2,
            'url_vidio_user_2' => $request->url_vidio_user_2,
            'deskripsi_user_2' => $request->deskripsi_user_2,
            'nama_user_3' => $request->nama_user_3,
            'url_vidio_user_3' => $request->url_vidio_user_3,
            'deskripsi_user_3' => $request->deskripsi_user_3,
            'nama_user_4' => $request->nama_user_4,
            'url_vidio_user_4' => $request->url_vidio_user_4,
            'deskripsi_user_4' => $request->deskripsi_user_4,
            'nama_user_5' => $request->nama_user_5,
            'url_vidio_user_5' => $request->url_vidio_user_5,
            'deskripsi_user_5' => $request->deskripsi_user_5,
            'nama_user_6' => $request->nama_user_6,
            'url_vidio_user_6' => $request->url_vidio_user_6,
            'deskripsi_user_6' => $request->deskripsi_user_6,
            'judul_accordion_1' => $request->judul_accordion_1,
            'deskripsi_accordion_1' => $request->deskripsi_accordion_1,
            'judul_accordion_2' => $request->judul_accordion_2,
            'deskripsi_accordion_2' => $request->deskripsi_accordion_2,
            'judul_accordion_3' => $request->judul_accordion_3,
            'deskripsi_accordion_3' => $request->deskripsi_accordion_3,
            'judul_accordion_4' => $request->judul_accordion_4,
            'deskripsi_accordion_4' => $request->deskripsi_accordion_4,
            'is_judul_landing_page' => $request->is_judul_landing_page
        ];

        $cek->update($updated);
        return redirect()->route('landing-page.show.list')
                        ->with('success','Template 4 Update successfully');
    }

    public function deleteAllImage(Request $request, $id)
    {
        $delImagePage_4 = LandingPage::where('id', $id)->first();
        $exists_1 = Storage::disk('local')->has('public/landing-page-images/' . $delImagePage_4->image_1);
        if ($exists_1) {
            $img_1 =Storage::delete('public/landing-page-images/' . $delImagePage_4->image_1);
        }
        $exists_2 = Storage::disk('local')->has('public/landing-page-images/' . $delImagePage_4->image_2);
        if ($exists_2) {
            $img_2 =Storage::delete('public/landing-page-images/' . $delImagePage_4->image_2);
        }
        $exists_3 = Storage::disk('local')->has('public/landing-page-images/' . $delImagePage_4->image_3);
        if ($exists_3) {
            $img_3 =Storage::delete('public/landing-page-images/' . $delImagePage_4->image_3);
        }
        $exists_4 = Storage::disk('local')->has('public/landing-page-images/' . $delImagePage_4->image_4);
        if ($exists_4) {
            $img_4 =Storage::delete('public/landing-page-images/' . $delImagePage_4->image_4);
        }
        $exists_5 = Storage::disk('local')->has('public/landing-page-images/' . $delImagePage_4->image_5);
        if ($exists_5) {
            $img_5 =Storage::delete('public/landing-page-images/' . $delImagePage_4->image_5);
        }
        $exists_6 = Storage::disk('local')->has('public/landing-page-images/' . $delImagePage_4->image_6);
        if ($exists_6) {
            $img_6 =Storage::delete('public/landing-page-images/' . $delImagePage_4->image_6);
        }
        $exists_7 = Storage::disk('local')->has('public/landing-page-images/' . $delImagePage_4->image_7);
        if ($exists_7) {
            $img_7 =Storage::delete('public/landing-page-images/' . $delImagePage_4->image_7);
        }
        $exists_8 = Storage::disk('local')->has('public/landing-page-images/' . $delImagePage_4->image_8);
        if ($exists_8) {
            $img_8 =Storage::delete('public/landing-page-images/' . $delImagePage_4->image_8);
        }
        $exists_9 = Storage::disk('local')->has('public/landing-page-images/' . $delImagePage_4->image_9);
        if ($exists_9) {
            $img_9 =Storage::delete('public/landing-page-images/' . $delImagePage_4->image_9);
        }
        $exists_10 = Storage::disk('local')->has('public/landing-page-images/' . $delImagePage_4->image_10);
        if ($exists_10) {
            $img_10 =Storage::delete('public/landing-page-images/' . $delImagePage_4->image_10);
        }
        $exists_11 = Storage::disk('local')->has('public/landing-page-images/' . $delImagePage_4->image_11);
        if ($exists_11) {
            $img_11 =Storage::delete('public/landing-page-images/' . $delImagePage_4->image_11);
        }

        LandingPage::find($id)->update([
            'image_1' => null, 
            'image_2' => null, 
            'image_3' => null, 
            'image_4' => null, 
            'image_5' => null, 
            'image_6' => null, 
            'image_7' => null, 
            'image_8' => null, 
            'image_9' => null, 
            'image_10' => null, 
            'image_11' => null, 
        ]);

        return redirect()->back()
                        ->with('success','Images Deleted successfully');
    }
}
