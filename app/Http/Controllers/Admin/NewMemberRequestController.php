<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Spatie\Permission\Models\Role;
use App\Mail\CustomerApproved;
use App\Model\NotifInfo;
use App\Model\Transaction;
use Illuminate\Http\Request;
use App\User;
use Hash;
use Auth;
use DB;
use Illuminate\Support\Str;
use App\Helpers\ImageUpload;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;



class NewMemberRequestController extends Controller
{
    public function index()
    {
        // NotifInfo::readAtNewTeamRequest();
        
        $no = 1;
        $dataMS = User::with('notif_no_read_new_member_request')->where('is_tmp_user', '1')->orderBy('id', 'DESC')->get();
        return view('admin.new-member-request.show', [
            'dataMS' => $dataMS,
            'no' => $no
        ]);
    }

    public function update($id)
    {
        $user = User::find($id);
        NotifInfo::readAtNewTeamRequest($user->id, 'admin');
        $dataEdit = User::where('id', $id)->first();
        return view('admin.new-member-request.updateDataMember', [
            'dataEdit' => $dataEdit,
        ]);
    }

    public function edit(Request $request, $id)
    {
        $this->validate($request, [
            'foto_ktp' => 'max:5120',
        ]);

        $length = 8;
        $ms_code = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $password = Str::random(8);
        $cek = User::where('id', $id)->first();

        if(!empty($request->foto_ktp)){
            if ($request->hasFile('foto_ktp')) {
                $image1      = $request->file('foto_ktp');
                $file_name   = time() . '.' . $image1->getClientOriginalExtension();
                $img1 = Image::make($image1);
                $img1->stream(); // <-- Key point
                Storage::disk('local')->put('public/foto_ktp/' . $file_name, $img1);
    
                $exists1 = Storage::disk('local')->has('public/foto_ktp/' . $cek->foto_ktp);
                if ($exists1) {
                    Storage::delete('public/foto_ktp/' . $cek->foto_ktp);
                }
            }
        }else{
            $file_name = $cek->foto_ktp;
        }
        User::where('id', $id)->update([
            'first_name'  => $request->first_name,
            'last_name'  => $request->last_name,
            'no_ktp'  => $request->no_ktp,
            'gender'  => $request->gender,
            'whatsapp_no'  => $request->phone,
            'address'  => $request->address,
            'password' => Hash::make($password), 
            'kode_pos'  => $request->kode_pos,
            'rekening_bank'  => $request->rekening_bank,
            'nama_pemilik_rekening'  => $request->nama_pemilik_rekening,
            'nomor_rekening'  => $request->nomor_rekening,
            'roles'  => 'Customer',
            'status' => $request->status,
            'is_tmp_user' => 0,
            'is_approve_admin' => 0,
            'foto_ktp' => $file_name
        ]);

        $user = User::where('id', $id)->first();

        if(!empty($user->email)){
            Mail::to($user->email)->send(new CustomerApproved($id,$password));
            $response = [
                'status'=>true,
                'status_code'=>200,
                'message'=>'suksess',
                'email'=>'Sent Email CustomerApproved'
            ];
        }else{
            $response = [
                'status'=>true,
                'status_code'=>500,
                'message'=>'error',
                'email'=>'Failed Sent Email : CustomerApproved'
            ];
        }

        // $this->approveNewUser($id);
            
        // status upgrade ms [
        //     0 => lengkapi data untuk ugrade
        //     1 => suskes upgrade,
        //     2 => cerify gagal,
        //     3 => akun diverufu admin
        // ]

        return redirect()->route('member-request.show')
                        ->with('success','User Baru Telah bergabung');
    }

//     public function approveNewUser(Request $request, $id)
//     {
//         User::where('id', $id)->update([
//             'is_tmp_user' => 0,
//             'is_approve_admin' => 0,
//         ]);
//         $user = User::where('id', $id)->first();

//         if(!empty($user->email)){
//             Mail::to($user->email)->send(new CustomerApproved($id));
//             $response = [
//                 'status'=>true,
//                 'status_code'=>200,
//                 'message'=>'suksess',
//                 'email'=>'Sent Email CustomerApproved'
//             ];
//         }else{
//             $response = [
//                 'status'=>true,
//                 'status_code'=>500,
//                 'message'=>'error',
//                 'email'=>'Failed Sent Email : CustomerApproved'
//             ];
//         }
//     }
}
