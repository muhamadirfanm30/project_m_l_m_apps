<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Transaction;
use App\Model\ItemTransaction;
use App\Mail\successPaymentEmail;
use App\Model\TransactionShipment;
use Illuminate\Support\Facades\Mail;
use App\Model\CsStockProduct;
use App\Model\NotifInfo;
use App\User;

class TransactionController extends Controller
{
    public function index()
    {
        $no = 1;
        $dataTransaksi = Transaction::where('status', 0)->with('notif_no_read_data_konfirmasi_payment')->whereNotNull('photo')->orderBy('id', 'DESC')->get();
        return view('admin.transaction.transactions.index', [
            'dataTransaksi' => $dataTransaksi,
            'no' => $no
        ]);
    }

    public function showDetailProduct($order_id)
    {
        return $dataTransaksi = Transaction::where('orderId', $order_id)->with(['order_detail.getProduct', 'get_user'])->first();
    }

    public function getAddress($id)
    {
        return TransactionShipment::where('id', $id)->first();
    }

    public function updateAlamatPengiriman(Request $request, $id)
    {
        TransactionShipment::where('id', $id)->update([
            'alamat' => $request->alamat
        ]);
    }

    public function transactionAddress(Request $request, $orderId)
    {
        Transaction::where('orderId', $orderId)->update([
            'alamat' => $request->alamat
        ]);

        return redirect()->back()->with('success', 'Alamat Berhasil diubah');
    }

    public function acceptPayment($order_id)
    {
        $data = Transaction::where('orderId', $order_id)->with(['order_detail', 'get_user'])->first();
        if($data->type == 1){
            $length = 7;
            $ms_code = '0123456789';
            $getMyTeam = User::where('referal_code', $data->get_user->membership_id)->count();
            
            $data->update([
                'status' => 1,
            ]);

            $create = User::create([
                'first_name' =>$data->nama_lengkap,
                'phone' =>$data->nomor_ponsel,
                'whatsapp_no' =>$data->nomor_ponsel,
                'gender' =>$data->jenis_kelamin,
                'province_id' =>$data->province_id,
                'kota_id' =>$data->kota_id,
                'email' =>$data->email,
                'is_tmp_user' => 1,
                'is_approve_admin' => 1,
                'status' => 1,
                'membership_id' => 'R-'.substr(str_shuffle(str_repeat($ms_code, 7)), 0, $length),
                'membersip_status' => 'Reguler',
                'address' => $data->alamat,
                'referal_code' => $data->get_user->membership_id,
                'created_at'=>now(),
                'updated_at'=>now(),
                'is_admin_created' => 0
            ]);

            User::where('membership_id', $data->get_user->membership_id)->update([
                'my_member' => $getMyTeam + 1
            ]);

            NotifInfo::pushNotifNewTeamRequest($create->id, 'admin');

        }else{
            $data->update([
                'status' => 1,
            ]);
        }

        if($data->own_transaction == 1){
            $data = Transaction::where('orderId',$order_id)->with('get_user')->first();
            $getItem = ItemTransaction::where('orderId',$order_id)->get();
            $data->update([
                'status' => 5,
            ]);
            foreach ($getItem as $r) {
                $getMyStok = CsStockProduct::where('product_id',$r->product_id)->where('cs_id',$data->cs_id)->first();
                if($getMyStok){
                    $fieldMyStok = [
                        'product_id'=>$r->product_id,
                        'stok'=> (int)$getMyStok->stok + (int)$r->qty,
                        'cs_id'=>$data->cs_id
                    ];
                    $getMyStok->update($fieldMyStok);
                }else{
                    $fieldMyStok = [
                        'product_id'=>$r->product_id,
                        'stok'=>$r->qty,
                        'cs_id'=>$data->cs_id
                    ];
                    CsStockProduct::create($fieldMyStok);
                }
            }
        }else{
            $data->update([
                'status' => 1,
            ]);
        }
        if($data->own_transaction != 1){
            NotifInfo::pushNotifResiPengiriman($data->id);
        }
        
        if(!empty($data->get_user->email)){
            Mail::to($data->get_user->email)->send(new successPaymentEmail($order_id));
            return "Email telah dikirim";
        }else{
            return "Email Gagal Terkirim";
        }
    }
}
