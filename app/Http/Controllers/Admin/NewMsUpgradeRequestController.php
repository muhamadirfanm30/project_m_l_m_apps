<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\NotifInfo;
use Illuminate\Http\Request;
use App\Model\PositionUpdateModel;
use App\User;
use App\Helpers\ImageUpload;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use Auth;

class NewMsUpgradeRequestController extends Controller
{
    public function index()
    {
        // NotifInfo::readAtLeaderUpgrade(false, 'admin');

        $no = 1;
        $dataMS = User::with('notif_no_read_leader_upgrade_ms')->where('is_upgrade_ms', 3)->where('is_admin_created', 1)->orderBy('id', 'DESC')->get();
        return view('admin.new-ms-upgrade.show', [
            'dataMS' => $dataMS,
            'no' => $no
        ]);
    }

    public function detail($id)
    {
        $user = User::where('id', $id)->with('get_posisi')->first();
        NotifInfo::readAtLeaderUpgrade($user->id, 'admin');
        return view('admin.new-ms-upgrade.detail', [
            'user' => $user,
        ]);
    }

    public function edit($id)
    {
        $dataEdit = User::where('id', $id)->with('get_posisi')->first();
        return view('admin.new-ms-upgrade.update', [
            'dataEdit' => $dataEdit
        ]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'foto_ktp' => 'max:5120',
            'email' => 'required',
        ]);

        $cek = User::where('id', $id)->first();

            if(!empty($request->foto_ktp)){
                if ($request->hasFile('foto_ktp')) {
                    $image1      = $request->file('foto_ktp');
                    $file_name   = time() . '.' . $image1->getClientOriginalExtension();
                    $img1 = Image::make($image1);
                    $img1->stream(); // <-- Key point
                    Storage::disk('local')->put('public/foto_ktp/' . $file_name, $img1);
        
                    $exists1 = Storage::disk('local')->has('public/foto_ktp/' . $cek->foto_ktp);
                    if ($exists1) {
                        Storage::delete('public/foto_ktp/' . $cek->foto_ktp);
                    }
                }
            }else{
                $file_name = $cek->foto_ktp;
            }

        User::where('id', $id)->update([
            'first_name'  => $request->first_name,
            'last_name'  => $request->last_name,
            'no_ktp'  => $request->no_ktp,
            'gender'  => $request->gender,
            'email'  => $request->email,
            'whatsapp_no'  => $request->whatsapp_no,
            'address'  => $request->address,
            'rekening_bank'  => $request->rekening_bank,
            'nama_pemilik_rekening'  => $request->nama_pemilik_rekening,
            'nomor_rekening'  => $request->nomor_rekening,
            'foto_ktp' => $file_name,
            'sponsor_upline_id' => $request->no_id_sponsor_upline,
            'sponsor_upline_name' => $request->sponsor_upline_name,
        ]);

        PositionUpdateModel::where('calon_ms_id', $id)->update([
            'posisi' => $request->posisi,
            'user_sponsor_name' => $request->sponsor_upline_name,
            'user_sponsor_status' => $request->no_id_sponsor_upline,
        ]);

        return redirect('/admin/new-ms-upgrade-request/detail/' . $id)->with('success', 'Update Data Membership Telah berhasil');
    }

    public function approveMS(Request $request, $id)
    {
        $cek = User::where('membership_id', $request->membership_id)->count();
        if ($cek == 0) {
            $this->validate($request, [
                'membersip_status' => 'required',
                'membership_id' => 'required',
            ]);

            User::where('id', $id)->update([
                'membersip_status'  => $request->membersip_status,
                'membership_id'  => $request->membership_id,
                'is_upgrade_ms'  => 1,
            ]);

            return redirect('/admin/new-ms-upgrade-request/show')->with('success', 'Upgrade Ms Has Been Success');
        } else {
            return redirect('/admin/new-ms-upgrade-request/detail/' . $id)->with('error', 'Kode ' . $request->membership_id . ' telah digunakan user lain');
        }
    }
}
