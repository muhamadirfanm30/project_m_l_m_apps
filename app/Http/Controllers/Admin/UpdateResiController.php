<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\TransactionItem;
use Illuminate\Http\Request;
use App\Model\Transaction;
use App\Model\NotifInfo;

class UpdateResiController extends Controller
{
    public function index()
    {
        $no = 1;
        $show = Transaction::where('status', 1)->where('type', 0)->with(['get_user', 'notif_no_read_data_update_resi'])->get();
        return view('admin.transaction.update-resi.index', [
            'no' => $no,
            'show' => $show
        ]);
    }

    public function detail($orderId)
    {
        $transaction = Transaction::where('orderId', $orderId)->first();
        $orderDetail = TransactionItem::where('orderId', $orderId)->get();
        return view('admin.transaction.update-resi.detail', [
            'transaction' => $transaction,
            'orderDetail' => $orderDetail
        ]);
    }

    public function resi(Request $request, $orderId)
    {
        $this->validate($request, [
            'resi_pengiriman' => 'required',
        ]);

        // if(!empty($request->resi_pengiriman)){
            Transaction::where('orderId', $orderId)->update([
                'resi_pengiriman' => $request->resi_pengiriman,
                'status' => 4, // status dikirim
            ]);
        // }else{
        //     Transaction::where('orderId', $orderId)->update([
        //         'resi_pengiriman' => $request->resi_pengiriman,
        //     ]);
        // }
        
    }
}
