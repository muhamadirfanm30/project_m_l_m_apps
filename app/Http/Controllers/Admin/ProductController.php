<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Product;
use App\Model\LandingPage;
use App\Model\WhatsAppTemplate;
use App\Model\KategoriProduk;
use DB;
use Redirect;
use App\Helpers\ImageUpload;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;


class ProductController extends Controller
{
    public function index()
    {
        $no =1;
        $showProduk = Product::get();
        return view('admin.product.index', [
            'showProduk' => $showProduk,
            'no' => $no
        ]);
    }

    public function create()
    {
        $landingPage = LandingPage::get();
        $message = WhatsAppTemplate::get();
        $listKategori = KategoriProduk::get();
        return view('admin.product.create', [
            'landingPage' => $landingPage,
            'message' => $message,
            'listKategori' => $listKategori
        ]);
    }

    public function edit($id)
    {
        $showProduk = Product::where('id', $id)->first();
        $landingPage = LandingPage::get();
        $listKategori = KategoriProduk::get();
        return view('admin.product.update', [
            'showProduk' => $showProduk,
            'landingPage' => $landingPage,
            'listKategori' => $listKategori
        ]);
    }

    public function detail($id)
    {
        $showProduk = Product::where('id', $id)->first();
        return view('admin.product.detail', [
            'showProduk' => $showProduk
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'nama_produk' => 'required',
            'stok' => 'required',
            'harga' => 'required',
            'deskripsi_produk' => 'required',
            'image_produk' => 'required',
            'berat' => 'required',
            'panjang' => 'required',
            'lebar' => 'required',
            'tinggi' => 'required',
            'kategori_produk_id' => 'required',
        ]);

        if($request->harga_promo >= $request->harga){
            return Redirect::back()->withErrors(['Harga Promo Tidak boleh Lebih besar dari Harga Reguler']);
        }else{
            $path = Product::getImagePathUpload();
            $filename = null;

            if ($request->image_produk != null) {
                $image = (new ImageUpload)->upload($request->image_produk, $path);
                $filename = $image->getFilename();
            }
            
            $storeSalTraining = Product::create([
                'nama_produk' => $request->nama_produk,
                'stok' => $request->stok,
                'harga' => $request->harga,
                'harga_promo' =>  $request->harga_promo,
                'is_promo' =>  $request->harga_promo > 0 ? 1 : 0,
                'deskripsi_produk' => $request->deskripsi_produk,
                'image_produk' => $filename,
                'berat' => $request->berat,
                'panjang' => $request->panjang,
                'lebar' => $request->lebar,
                'tinggi' => $request->tinggi,
                'valid_at' => $request->valid_at != null ? $request->valid_at.' 00:00:00' : null,
                'expired_at' => $request->expired_at != null ? $request->expired_at.' 29:59:00' : null,
                // 'template_product' => $request->template_product,
                'kategori_produk_id' => $request->kategori_produk_id,
                'slug' => $request->slug,
            ]);

            // for ($i = 0; $i < count($request->message); $i++) {
            //     $answers[] = [
            //         'product_id' => $request->nama_produk,
            //         'url_1' => 'http://localhost:8000/',
            //         'nama_produk' => $request->slug,
            //         'is_template' => $request->template_product,
            //         'phone_number' => '',
            //         'message' => $request->message[$i],
            //         'created_at' => date('Y-m-d H:i:s'),
            //         'updated_at' => date('Y-m-d H:i:s'),
            //     ];
            // }
    
            // DB::table('url_landing_pages')->insert($answers);
    
            return redirect()->route('product.show')
                            ->with('success','Product Update successfully');
        }
        
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama_produk' => 'required',
            'stok' => 'required',
            'harga' => 'required',
            'deskripsi_produk' => 'required',
            'berat' => 'required',
            'panjang' => 'required',
            'lebar' => 'required',
            'tinggi' => 'required',
            'kategori_produk_id' => 'required',
        ]);

        $dataProduk = Product::where('id', $id)->first();
        if($request->harga_promo >= $request->harga){
            return Redirect::back()->withErrors(['Harga Promo Tidak boleh Lebih besar dari Harga Reguler']);
        }else{
            if(!empty($request->image_produk)){
                if ($request->hasFile('image_produk')) {
                    $image      = $request->file('image_produk');
                    $file_name   = time() . '.' . $image->getClientOriginalExtension();
                    $img = Image::make($image);
                    $img->stream(); // <-- Key point
                    Storage::disk('local')->put('public/productImages/' . $file_name, $img);
        
                    $exists = Storage::disk('local')->has('public/productImages/' . $dataProduk->image_produk);
                    if ($exists) {
                        Storage::delete('public/productImages/' . $dataProduk->image_produk);
                    }
                }
            }else{
                $file_name = $dataProduk->image_produk;
            }

            $storeSalTraining = Product::where('id', $id)->update([
                'nama_produk' => $request->nama_produk,
                'stok' => $request->stok,
                'harga' => $request->harga,
                'harga_promo' =>  $request->harga_promo,
                'is_promo' =>  $request->harga_promo > 0 ? 1 : 0,
                'deskripsi_produk' => $request->deskripsi_produk,
                'image_produk' => $file_name,
                'berat' => $request->berat,
                'panjang' => $request->panjang,
                'lebar' => $request->lebar,
                'tinggi' => $request->tinggi,
                'valid_at' => $request->valid_at != null ? $request->valid_at.' 00:00:00' : null,
                'expired_at' => $request->expired_at != null ? $request->expired_at.' 23:59:00' : null,
                // 'template_product' => $request->template_product,
                'kategori_produk_id' => $request->kategori_produk_id,
            ]);
    
            return redirect()->route('product.show')
                            ->with('success','Product created successfully');
        }
    }

    public function destroy($id)
    {
        $dataProduk = Product::where('id', $id)->first();
        $exists = Storage::disk('local')->has('public/productImages/' . $dataProduk->image_produk);
        if ($exists) {
            Storage::delete('public/productImages/' . $dataProduk->image_produk);
        }
        Product::find($id)->delete();
        return redirect()->route('product.show')
                        ->with('success','Product deleted successfully');
    }
}
