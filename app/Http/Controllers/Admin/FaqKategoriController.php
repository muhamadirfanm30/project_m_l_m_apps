<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
Use App\Model\FaqKategori;

class FaqKategoriController extends Controller
{
    public function index()
    {
        $no = 1;
        $showData = FaqKategori::get();
        return view('admin.faq-categori.index', [
            'showData' => $showData,
            'no' => $no,
        ]);
    }

    public function create()
    {
        return view('admin.faq-categori.create');
    }

    public function update($id)
    {
        $dataEdit = FaqKategori::find($id);
        return view('admin.faq-categori.update', [
            'dataEdit' => $dataEdit
        ]);
    }

    public function store(Request $request)
    {

        for ($i = 0; $i < count($request->name); $i++) {
            $answers[] = [
                'name' => $request->name[$i],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
        }

        FaqKategori::insert($answers);
        return redirect()->route('faq.show')
                        ->with('success', 'FAQ Category Created Successfuly');

        
    }

    public function edit(Request $request, $id)
    {
        FaqKategori::find($id)->update([
            'name' => $request->name
        ]);

        return redirect()->route('faq.show')->with('success', 'FAQ Category Updated Successfuly');
    }

    public function destroy($id)
    {
        FaqKategori::find($id)->delete();
        return redirect()->route('faq.show')->with('success', 'FAQ Category Delete Successfuly');
    }
}
