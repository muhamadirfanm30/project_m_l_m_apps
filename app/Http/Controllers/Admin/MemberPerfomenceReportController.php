<?php

namespace App\Http\Controllers\Admin;

use App\Exports\MemberPerformenceReports;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
use App\Exports\Performence;
use Illuminate\Http\Request;
use App\User;

class MemberPerfomenceReportController extends Controller
{
    public function index()
    {
        $no = 1;
        $getData = User::where('roles', 'Customer')->with(['order.order_detail'])->get();
        return view('admin.report.report-performence.index', [
            'getData' => $getData,
            'no' => $no,
        ]);
    }

    function export()
    {
        return (new Performence)->download('performence_member'.date('Y-M-d H:i:s').'.xlsx');
    }
}
