<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Events;
use App\Helpers\ImageUpload;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class EventsController extends Controller
{
    public function index()
    {
        $no=1;
        $getListEvents = Events::get();
        return view('admin.events.index', [
            'getListEvents' => $getListEvents,
            'no' => $no
        ]);
    }

    public function create()
    {
        return view('admin.events.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'nama_event' => 'required',
            'deskripsi' => 'required',
            'gambar' => 'required',
            'url_vidio' => 'required',
            'deskripsi_vidio' => 'required',
            'publish_at' => 'required',
            'harga' => 'required',
        ]);
        
        $path = Events::getImagePathUpload();
        $filename = null;

        if ($request->gambar != null) {
            $image = (new ImageUpload)->upload($request->gambar, $path);
            $filename = $image->getFilename();
        }
        

        $storeSalTraining = Events::create([
            'nama_event' => $request->nama_event,
            'deskripsi' => $request->deskripsi,
            'gambar' => $filename,
            'url_vidio' => $request->url_vidio,
            'deskripsi_vidio' => $request->deskripsi_vidio,
            'publish_at' => $request->publish_at,
            'harga' => $request->harga,
        ]);

        return redirect()->route('events.show')
                        ->with('success','Events created successfully');
    }

    public function edit($id)
    {
        $detailEvent = Events::where('id', $id)->first();
        return view('admin.events.update', [
            'detailEvent' => $detailEvent
        ]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama_event' => 'required',
            'deskripsi' => 'required',
            // 'gambar' => 'required',
            'url_vidio' => 'required',
            'deskripsi_vidio' => 'required',
            'publish_at' => 'required',
            'harga' => 'required',
        ]);

        $detailEvent = Events::where('id', $id)->first();

        if(!empty($request->gambar)){
            if ($request->hasFile('gambar')) {
                $image      = $request->file('gambar');
                $file_name   = time() . '.' . $image->getClientOriginalExtension();
                $img = Image::make($image);
                $img->stream(); // <-- Key point
                Storage::disk('local')->put('public/eventsImages/' . $file_name, $img);
    
                $exists = Storage::disk('local')->has('public/eventsImages/' . $detailEvent->gambar);
                if ($exists) {
                    Storage::delete('public/eventsImages/' . $detailEvent->gambar);
                }
            }
        }else{
            $file_name = $detailEvent->gambar;
        }

        

        $storeSalTraining = Events::where('id', $id)->update([
            'nama_event' => $request->nama_event,
            'deskripsi' => $request->deskripsi,
            'gambar' => $file_name,
            'url_vidio' => $request->url_vidio,
            'deskripsi_vidio' => $request->deskripsi_vidio,
            'publish_at' => $request->publish_at,
            'harga' => $request->harga,
        ]);

        return redirect()->route('events.show')
                        ->with('success','Events created successfully');
    }

    public function destroy($id)
    {
        $detailEvent = Events::where('id', $id)->first();
        $exists = Storage::disk('local')->has('public/eventsImages/' . $detailEvent->gambar);
        if ($exists) {
            Storage::delete('public/eventsImages/' . $detailEvent->gambar);
        }
        Events::find($id)->delete();
        return redirect()->route('events.show')
                        ->with('success','Events deleted successfully');
    }

    public function detail($id)
    {
        $detailEvent = Events::where('id', $id)->first();
        return view('admin.events.detail', [
            'detailEvent' => $detailEvent
        ]);
    }
}
