<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\TransactionShipment;
use App\Model\TransactionItem;
use App\Model\ItemTransaction;
use App\Model\NotifInfo;
use App\Model\PaketUpgradeMS;
use App\Model\PaketReguler;
use App\Model\Transaction;
use App\Model\CsStockProduct;
use App\Model\GeneralSetting;
use App\Model\Product;
use Illuminate\Http\Request;
use App\User;
use App\Mail\successPaymentEmail;
use App\Mail\SuccessRegisterEmail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use DB;
use Hash;

class AllTransactionsController extends Controller
{
    public function index(Request $request)
    {
        $no = 1;
        $dataTransaksi = Transaction::with('notif_no_read_data_transaksi')->get();
        $by_status = $request->by_status;
        $by_kategori = $request->by_kategori;
        
        // $sort = $request->sort ?? 'default';
        // if($sort){
        //     switch ($sort) {
        //         case 'semua_pesanan':
        //             $dataTransaksi = $dataTransaksi;
        //             break;
        //         case 'menunggu_pembayaran':
        //             $dataTransaksi = $dataTransaksi->where('status', 0)->whereNull('photo');
        //             break;
        //         case 'menunggu_konfirmasi_admin':
        //             $dataTransaksi = $dataTransaksi->where('status', 0)->whereNotNull('photo');
        //             break;
        //         case 'pesanan_sedang_diproses':
        //             $dataTransaksi = $dataTransaksi->where('status', 1);
        //             break;
        //         case 'pesanan_dikirim':
        //             $dataTransaksi = $dataTransaksi->where('status', 4);
        //             break;
        //         case 'pesanan_selesai':
        //             $dataTransaksi = $dataTransaksi->where('status', 5);
        //             break;
        //         case 'pesan_dibatalkan':
        //             $dataTransaksi = $dataTransaksi->where('status', 2);
        //             break;
        //         default:
        //             $dataTransaksi = $dataTransaksi;
        //             break;
        //     }
        // }

        // $kategori = $request->kategori ?? 'default';
        // if($kategori){
        //     switch ($kategori) {
        //         case 'semua_kategori':
        //             $dataTransaksi = $dataTransaksi->where('kategori', 'Pembelian Produk');
        //             break;
        //         case 'pembelian_produk':
        //             $dataTransaksi = $dataTransaksi->where('kategori', 'Pembelian Produk');
        //             break;
        //         case 'akumulasi_paket_upgrade_ms':
        //             $dataTransaksi = $dataTransaksi->where('kategori', 'Akumulasi Paket MS Upgrade');
        //             break;
        //         case 'pendaftaran_member_reguler':
        //             $dataTransaksi = $dataTransaksi->where('kategori', 'Pendaftaran Member Reguler');
        //             break;
        //         case 'pendaftaran_event':
        //             $dataTransaksi = $dataTransaksi->where('kategori', 'Pembayaran Daftar Event');
        //             break;
        //         case 'ongkos_kirim_stok_produk_online':
        //             $dataTransaksi = $dataTransaksi->where('kategori', 'Ongkos Kirim Pengiriman Stok Produk');
        //             break;
        //         default:
        //             $dataTransaksi = $dataTransaksi;
        //             break;
        //     }
        // }
        return view('admin.transaction.all-transaction.index', [
            'dataTransaksi' => $dataTransaksi,
            'by_kategori' => $by_kategori,
            'by_status' => $by_status,
            'no' => $no,
        ]);
    }

    

    public function search(Request $request)
    {
        // return $request->all();
        $no = 1;
        $by_status = $request->by_status;
        $by_kategori = $request->by_kategori;
        
        if($by_status != "Semua Pesanan" && $by_kategori != "Semua Kategori"){
            // return 'semua';
            $data_search = Transaction::with('notif_no_read_data_transaksi')
                        ->where('status', 'like', '%' . $request->by_status . '%')
                        ->Where('kategori', 'like',  '%' . $request->by_kategori . '%')
                        ->get();
        }elseif($by_status != "Semua Pesanan" && $by_kategori == "Semua Kategori"){
            // return 'statuis';
            $data_search = Transaction::with('notif_no_read_data_transaksi')
                        ->Where('status', 'like',  '%' . $request->by_status . '%')
                        ->get();
        }elseif($by_kategori != "Semua Kategori" && $by_status == "Semua Pesanan"){
            // return 'kategori';
            $data_search = Transaction::with('notif_no_read_data_transaksi')
                        ->where('kategori', 'like', '%' . $request->by_kategori . '%')
                        ->get();
        }elseif($by_kategori == "Semua Kategori" && $by_status == "Semua Pesanan"){
            // return 'balik ke awal';
            $data_search = Transaction::with('notif_no_read_data_transaksi')->get();
        }

        return view('admin.transaction.all-transaction.data-search', [
            'data_search' => $data_search,
            'by_kategori' => $by_kategori,
            'by_status' => $by_status,
            'no' => $no,
        ]);
    }

    public function detail($orderId)
    {
        $no = 1;
        $transaction = Transaction::where('orderId', $orderId)->with('get_user')->first();
        $orderDetail = ItemTransaction::where('orderId', $orderId)->with(['products', 'getpaketupgrade', 'getpaketreguler'])->get();
        $detailShippment = TransactionShipment::where('orderId', $orderId)->with('getProduk')->get();

        // read notif
        NotifInfo::readAtDataTransaksi($transaction->id, 'admin');
        NotifInfo::readAtKonfirmasiPembayaran($transaction->id, 'admin');
        NotifInfo::readAtRegulerMember($transaction->id, 'admin');
        NotifInfo::readAtResiPengiriman($transaction->id, 'admin');
        NotifInfo::readAtPaketUpgrade($transaction->id, 'admin');
        NotifInfo::readAtStokProdukOnline($transaction->id, 'admin');


        $count_data_transaksi_from_admin_not_read = NotifInfo::whereIn('modul_name', ['data-transactions', 'paket-upgrade-member', 'input-reguler-member'])
            ->whereNull('read_at')
            ->where('role_name', 'admin')
            ->count();

        $count_konfirmasi_pembayaran_from_admin_not_read = NotifInfo::where('modul_name', 'konfirmasi-pembayaran')
            ->whereNull('read_at')
            ->where('role_name', 'admin')
            ->count();

        $status_text_info = $count_konfirmasi_pembayaran_from_admin_not_read + $count_data_transaksi_from_admin_not_read > 0 ? "News" : '';

        return view('admin.transaction.all-transaction.detail', [
            'transaction' => $transaction,
            'orderDetail' => $orderDetail,
            'no' => $no,
            'shipment' => $detailShippment,
            'count_data_transaksi_from_admin_not_read' => $count_data_transaksi_from_admin_not_read,
            'count_konfirmasi_pembayaran_from_admin_not_read' => $count_konfirmasi_pembayaran_from_admin_not_read,
            'status_text_info' => $status_text_info,
        ]);
    }

    public function updateMultipleResi(Request $request)
    {
        
        

        
        return redirect()->route('resi.show')
            ->with('success', 'Nomor Resi Telah diperbarui');
    }

    public function updateStatus($orderId, Request $request)
    {
        if($request->status == ""){
            return redirect()->back()->with('error', 'Silahkan Pilih Status yang Bersangkutan Terlebih dahulu');
        }else{
            if($request->status == 1){
                $data = Transaction::where('orderId', $orderId)->with(['order_detail', 'get_user'])->first();
                if($data->type == 1){
                    $length = 7;
                    $ms_code = '0123456789';
                    $getMyTeam = User::where('referal_code', $data->get_user->membership_id)->count();
                    $data->update([
                        'status' => 1,
                    ]);

                    $checkAvailableUser = User::where('email',$data->email)->exists();
                    if(!$checkAvailableUser){
                        $create = User::create([
                            'first_name' =>$data->nama_lengkap,
                            'phone' =>$data->nomor_ponsel,
                            'whatsapp_no' =>$data->nomor_ponsel,
                            'gender' =>$data->jenis_kelamin,
                            'province_id' =>$data->province_id,
                            'kota_id' =>$data->kota_id,
                            'email' =>$data->email,
                            'is_tmp_user' => 1,
                            'is_approve_admin' => 1,
                            'status' => 1,
                            'membership_id' => 'R-'.substr(str_shuffle(str_repeat($ms_code, 7)), 0, $length),
                            'membersip_status' => 'Reguler',
                            'address' => $data->alamat,
                            'referal_code' => $data->get_user->membership_id,
                            'created_at'=>now(),
                            'updated_at'=>now(),
                            'is_admin_created' => 0
                        ]);

                        User::where('membership_id', $data->get_user->membership_id)->update([
                            'my_member' => $getMyTeam + 1
                        ]);

                        NotifInfo::pushNotifNewTeamRequest($create->id, 'admin');
                    }
                }else{
                    $data->update([
                        'status' => 1,
                    ]);
                }

                if($data->own_transaction == 1){
                    $data = Transaction::where('orderId',$orderId)->with('get_user')->first();
                    $getItem = ItemTransaction::where('orderId',$orderId)->get();
                    $data->update([
                        'status' => 5,
                    ]);
                    foreach ($getItem as $r) {
                        $getMyStok = CsStockProduct::where('product_id',$r->product_id)->where('cs_id',$data->cs_id)->first();
                        if($getMyStok){
                            $fieldMyStok = [
                                'product_id'=>$r->product_id,
                                'stok'=> (int)$getMyStok->stok + (int)$r->qty,
                                'cs_id'=>$data->cs_id
                            ];
                            $getMyStok->update($fieldMyStok);
                        }else{
                            $fieldMyStok = [
                                'product_id'=>$r->product_id,
                                'stok'=>$r->qty,
                                'cs_id'=>$data->cs_id
                            ];
                            CsStockProduct::create($fieldMyStok);
                        }
                    }
                }else{
                    $data->update([
                        'status' => 1,
                    ]);
                }

                NotifInfo::pushNotifResiPengiriman($data->id);
                
                if(!empty($data->get_user->email)){
                    Mail::to($data->get_user->email)->send(new successPaymentEmail($orderId));
                    return redirect()->route('transaksi.show')
                        ->with('success', 'Status Pembayaran Berhasil diubah');
                }else{
                    return "Email Gagal Terkirim";
                }
                
            }elseif($request->status == 2 ){
                $cekIsCancel = Transaction::where('orderId', $orderId)->first();
                $cekOrder = TransactionItem::where('orderId', $orderId)->get();
                $is_product = '';
                foreach ($cekOrder as $total) {
                    $is_product = $total->is_produk;
                }
                if($cekIsCancel->is_cancel == 0 || $cekIsCancel->is_cancel == null){
                    // try {
                        if ($is_product == 'get_produk') {
                            $get_order_detail = TransactionItem::where('orderId', $orderId)->get();
                            if (count($get_order_detail) > 0) {
                                foreach ($get_order_detail as $order_detail) {
                                    $get_product = Product::withTrashed()->find($order_detail->product_id);
                                    $get_product->stok = $get_product->stok + $order_detail->qty;
                                    $get_product->save();
                                }
                            }
                            Transaction::where('orderId', $orderId)->update([
                                'status' => 2,
                                'is_cancel' => 1,
                            ]);
                            return redirect()->route('transaksi.show')->with('success', 'Pesanan Dibatalkan');
                        } elseif ($is_product == 'get_paket') {
                            // return 'get_paket';
                            $get_paket_upgrade = TransactionItem::where('orderId', $orderId)->get();
                            if (count($get_paket_upgrade) > 0) {
                                foreach ($get_paket_upgrade as $paket_detail) {
                                    $paket_upgrade_ms = PaketUpgradeMS::find($paket_detail->product_id);
                                    $paket_upgrade_ms->stok = $paket_upgrade_ms->stok + $paket_detail->qty;
                                    $paket_upgrade_ms->save();
                                }
                            }
                            Transaction::where('orderId', $orderId)->update([
                                'status' => 2,
                                'is_cancel' => 1,
                            ]);
                            return redirect()->route('transaksi.show')->with('success', 'Pesanan Dibatalkan');
                        } elseif ($is_product == 'get_paket_reguler') {
                            // return 'get_paket_reguler';
                            $get_paket_reguler = TransactionItem::where('orderId', $orderId)->get();
                            if (count($get_paket_reguler) > 0) {
                                foreach ($get_paket_reguler as $paket_reguler) {
                                    $paket_upgrade_ms = PaketReguler::find($paket_reguler->product_id);
                                    $paket_upgrade_ms->stok = $paket_upgrade_ms->stok + $paket_reguler->qty;
                                    $paket_upgrade_ms->save();
                                    // $hapus_order = OrderDetail::destroy($paket_detail->id);
                                }
                            }
                            Transaction::where('orderId', $orderId)->update([
                                'status' => 2,
                                'is_cancel' => 1,
                            ]);
                            return redirect()->route('transaksi.show')->with('success', 'Pesanan Dibatalkan');
                        }
                    // } catch (\Throwable $e) {
                    //     DB::rollback();
                    //     return redirect()->back()->with('error', 'Terjadi kesalahan dalam mengubah data');
                    //     $e->getMessage();
                    // }
                }else{
                    Transaction::where('orderId', $orderId)->update([
                        'status' => 2
                    ]);
                }
            }elseif($request->status == 0){
                Transaction::where('orderId', $orderId)->update([
                    'status' => 0
                ]);
            }elseif($request->status == 4){
                Transaction::where('orderId', $orderId)->update([
                    'status' => 4
                ]);
                $detailShippment = TransactionShipment::where('orderId', $orderId)->count();
                if($detailShippment != 0){
                    $value = $request->no_resi;
                    $ids = $request->id_resi;
            
                    foreach ($value as $key => $values) {
                        // return $type;
                        $id = $ids[$key];
                        TransactionShipment::where('id', $id)->update([
                            'no_resi' => $value[$key],
                            'updated_at' => date('Y-m-d H:i:s'),
                        ]);
                    }
                }else{
                    $cekIsCancel = Transaction::where('orderId', $orderId)->first();
                    Transaction::where('orderId', $orderId)->update([
                        'resi_pengiriman' => $request->resi_pengiriman,
                        'status' => 4, // status dikirim
                    ]);
                }
            }elseif($request->status == 5){
                Transaction::where('orderId', $orderId)->update([
                    'status' => 5
                ]);
            }

            return redirect()->route('transaksi.show')
                ->with('success', 'Status Pesanan Telah diperbaharui');
        }
        
        

        // if ($request->status == 0 || $request->status == 1) {
        
        // }
    }
}
