<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\KontenDashboard;

class KontenDashboardController extends Controller
{
    public function index()
    {
       $data = KontenDashboard::first();
        if(KontenDashboard::count() == 0){
            return view('admin.konten-dashboard.create');
        }else{
            return view('admin.konten-dashboard.show', [
                'data' => $data
            ]);
        }
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'url_vidio_generasi_online' => 'required',
            'url_vidio_tutorial_dashboard' => 'required',
            'link_telegram' => 'required',
            'link_group_content' => 'required',
            'nama_instagram' => 'required',
            'nama_facebook' => 'required',
        ]);
        
        KontenDashboard::create([
            'url_vidio_generasi_online' => $request->url_vidio_generasi_online,
            'url_vidio_tutorial_dashboard' => $request->url_vidio_tutorial_dashboard,
            'link_telegram' => $request->link_telegram,
            'link_group_content' => $request->link_group_content, 
            'nama_instagram' => $request->nama_instagram, 
            'nama_facebook' => $request->nama_facebook, 
        ]);

        return redirect()->route('konten.dashboard.show')
                        ->with('success','created successfully');
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'url_vidio_generasi_online' => 'required',
            'url_vidio_tutorial_dashboard' => 'required',
            'link_telegram' => 'required',
            'link_group_content' => 'required',
            'nama_instagram' => 'required',
            'nama_facebook' => 'required',
        ]);
        
        KontenDashboard::where('id', 1)->update([
            'url_vidio_generasi_online' => $request->url_vidio_generasi_online,
            'url_vidio_tutorial_dashboard' => $request->url_vidio_tutorial_dashboard,
            'link_telegram' => $request->link_telegram,
            'link_group_content' => $request->link_group_content, 
            'nama_instagram' => $request->nama_instagram, 
            'nama_facebook' => $request->nama_facebook, 
        ]);

        return redirect()->route('konten.dashboard.show')
                        ->with('success','Update successfully');
    }
}
