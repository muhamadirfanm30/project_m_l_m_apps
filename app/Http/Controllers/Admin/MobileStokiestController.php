<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\MobileStokiest;

class MobileStokiestController extends Controller
{
      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = MobileStokiest::orderBy('id','DESC')->get();
        if(count($data) == 0){
            return view('admin.mobileStokiest.create');
        }else{
            return view('admin.mobileStokiest.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'judul' => 'required',
            'url_vidio' => 'required',
            'deskripsi' => 'required',
        ]);

        $input = $request->all();
        $user = MobileStokiest::create($input);
        return redirect()->route('MS.show')
                        ->with('success','Mobile Stokiest created successfully');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ms = MobileStokiest::find($id);
        return view('admin.mobileStokiest.update',compact('ms'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'judul' => 'required',
            'url_vidio' => 'required',
            'deskripsi' => 'required',
        ]);

        $input = $request->all();
        $msUpdate = MobileStokiest::find($id);
        $msUpdate->update($input);
        return redirect()->route('MS.show')
                        ->with('success','Mobile Stokiest Update successfully');

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        MobileStokiest::find($id)->delete();
        return redirect()->route('MS.sho')
                        ->with('success','Mobile Stokiest successfully');
    }
}
