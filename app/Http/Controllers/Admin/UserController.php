<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Spatie\Permission\Models\Role;
use App\Mail\SuccessRegisterEmail;
use Illuminate\Support\Facades\Mail;
use App\Model\PositionUpdateModel;
use App\Exports\MemberList;
use DB;
use Hash;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Str;
use App\Exports\MemberListExport;
use App\Helpers\ImageUpload;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class UserController extends Controller
{
      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = User::orderBy('id','DESC')->where('roles', 'Customer')->paginate();
        return view('admin.userManagement.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    function exportExcel()
    {
        return (new MemberListExport)->download('Member_list'.date('Y-M-d H:i:s').'.xlsx');
    }

    // public function exportExcel()
    // {
    //     Excel::create('new MemberList', function($excel){
    //         $excel->sheet('First Sheet', function($sheet){
    //             $sheet->loadView('admin..export.member-list');
    //         });
    //     })->export('xls');
    // }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $length = 7;
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        $roles = Role::pluck('name','name')->all();
        return view('admin.userManagement.create',compact('roles', 'randomString'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    //    return $request->all();
        $password = Str::random(8);
        $this->validate($request, [
            'first_name' => 'required',
            'email' => 'required|email|unique:users,email',
            'phone' => 'required|unique:users,whatsapp_no',
            'status' => 'required',
        ]);
        // return $request->roles;
        // $input = $request->all();
        // $input['password'] = Hash::make($input['password']);
    
        $user = User::create([
            'first_name' => $request->first_name, 
            'last_name' => $request->last_name, 
            'email' => $request->email, 
            'phone' => str_replace(" ", "",$request->phone), 
            'whatsapp_no' => str_replace(" ", "",$request->phone),
            'status' => $request->status, 
            'password' => Hash::make($password), 
            'roles' => 'Customer',
            'membership_id' => $request->membership_id,
            'membersip_status' => 'Reguler',
            'is_admin_created' => 1,
        ]);
        $user->assignRole($request->input('roles'));

        if(!empty($request->email)){
            Mail::to($request->email)->send(new SuccessRegisterEmail($user->id,$password));
            $response = [
                'status'=>true,
                'status_code'=>200,
                'message'=>'success',
                'email'=>'Sent Email register Email'
            ];
        }else{
            $response = [
                'status'=>true,
                'status_code'=>400,
                'message'=>'error',
                'email'=>'Failed Sent Email : register Email'
            ];
        }

        return redirect()->route('users.show')
                        ->with('success','User created successfully');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('admin.userManagement.show',compact('user'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $length = 6;
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        $user = User::where('id', $id)->with('get_posisi')->first();

        return view('admin.userManagement.update',compact('user', 'randomString'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'status' => 'required',
            'foto_ktp' => 'max:5120',
            // 'roles' => 'required'
        ]);


        // $input = $request->all();
        // if(!empty($input['password'])){ 
        //     $input['password'] = Hash::make($input['password']);
        // }else{
        //     $input = array_except($input,array('password'));    
        // }
        // if($request->posisi == ""){
        //     return redirect()->back()
        //                 ->with('error','Position can not be empty');
        // }else{
            $cek = User::where('id', $id)->first();

            if(!empty($request->foto_ktp)){
                if ($request->hasFile('foto_ktp')) {
                    $image1      = $request->file('foto_ktp');
                    $file_name   = time() . '.' . $image1->getClientOriginalExtension();
                    $img1 = Image::make($image1);
                    $img1->stream(); // <-- Key point
                    Storage::disk('local')->put('public/foto_ktp/' . $file_name, $img1);
        
                    $exists1 = Storage::disk('local')->has('public/foto_ktp/' . $cek->foto_ktp);
                    if ($exists1) {
                        Storage::delete('public/foto_ktp/' . $cek->foto_ktp);
                    }
                }
            }else{
                $file_name = $cek->foto_ktp;
            }

            $user = User::find($id)->update([
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'email' => $request->email,
                'phone' => str_replace(" ", "",$request->phone),
                'status' => $request->status,
                'gender'  => $request->gender,
                'address'  => $request->address,
                'province_id'  => $request->txtProvinsi,
                'kota_id'  => $request->txtKota,
                'kode_pos'  => $request->kode_pos,
                'rekening_bank'  => $request->rekening_bank,
                'nama_pemilik_rekening'  => $request->nama_pemilik_rekening,
                'nomor_rekening'  => $request->nomor_rekening,
                'sponsor_upline_id' => $request->no_id_sponsor_upline,
                'foto_ktp' => $file_name
            ]);

            PositionUpdateModel::where('calon_ms_id', $id)->update([
                'posisi' => $request->posisi,
                'user_sponsor_name' => $request->no_id_sponsor,
                'user_sponsor_status' => $request->nama_id_sponsor,
            ]);
        // }
        // $user->update($input);
        // DB::table('model_has_roles')->where('model_id',$id)->delete();


        // $user->assignRole($request->input('roles'));


        return redirect()->route('users.show')
                        ->with('success','User updated successfully');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect()->route('users.show')
                        ->with('success','User deleted successfully');
    }
}
