<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Auth;

class MyTeamController extends Controller
{
    public function index(Request $request)
    {
        $no = 1;
        $myTeam = User::where('referal_code', Auth::user()->membership_id)->whereNotNull('referal_code')->with('order')->get();
        return view('customer.my-team.index', compact('myTeam', 'no'));
    }
}
