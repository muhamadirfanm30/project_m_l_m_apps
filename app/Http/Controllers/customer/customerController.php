<?php

namespace App\Http\Controllers\customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Kategori;
use App\Model\KontenDashboard;
use App\Model\Transaction;
use App\Model\ItemTransaction;
use GuzzleHttp\Client;

class customerController extends Controller
{
    public function showDashboard()
    {
        $getKonten = KontenDashboard::first();
        return view('customer.dashboard', [
            'getKonten' => $getKonten
        ]);
    }

    public function cekResi()
    {
        $info = $this->getInfoPengiriman('JP4339666457', 'jnt');
        dd($info);
    }

    private function getInfoPengiriman($no_resi, $kurir)
    {
        $client = new Client([
            'base_uri' => 'https://pro.rajaongkir.com',
        ]);
        $keyApi = '17461d353db677af78769faf2be1cb82';
        $param = [
            'key' => $keyApi,
            'waybill' => $no_resi,
            'courier' => $kurir
        ];
        try {
            $response = $client->request('POST', '/api/waybill', [
                'form_params' => $param
            ]);
            return json_decode($response->getBody()->getContents());
        } catch (\Exception  $e) {
            return $e->getMessage();
        }
    }
}
