<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\ItemTransaction;
use App\Model\Events;
use App\Model\Transaction;
use App\Model\Payment;
use App\Model\TransactionItem;
use App\Helpers\Midtrans;
use Illuminate\Support\Str;
use DB;

use App\Model\GeneralSetting;
use Carbon\Carbon;

class LiveTrainingController extends Controller
{
    //live training
    public function liveTraining(Request $request)
    {
        $getUrl = DB::table('general_settings')->where('name', 'url_vidio_live_training')->first();
        $getDesc = DB::table('general_settings')->where('name', 'desc_vidio_live_training')->first();
        $liveTraining = Events::orderBy('id', 'DESC')->paginate(4);
        return view('customer.live-training.index',compact('liveTraining', 'getUrl', 'getDesc'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function liveTrainingDetail($id)
    {
        $liveTraining = Events::where('id', $id)->first();
        return view('customer.live-training.detail', [
            'liveTraining' => $liveTraining
        ]);
    }

    public function paymentCard($id)
    {
        $bank = db::table('payment_master')->where('id', 1)->where('is_active', 0)->first();
        $midtrans = db::table('payment_master')->where('id', 2)->where('is_active', 0)->first();
        $listPayment = Payment::get();
        $detail = Events::where('id', $id)->first();
        return view('customer.live-training.payment', [
            'detail' => $detail,
            'listPayment' => $listPayment,
            'bank' => $bank,
            'midtrans' => $midtrans,
        ]);
    }

    public function generateOrderCode()
    {
        $q          = Transaction::count();
        $prefix     = 'O'; //prefix = O-16032020-00001
        $separator  = '-';
        $date       = date('dmys');
        $number     = 1; //format
        $new_number = sprintf("%04s", $number);
        $code       = $date .  ($new_number);
        $order_code   = $code;
        if ($q > 0) {
            $last     = Transaction::orderBy('id', 'desc')->value('orderId');
            $last     = explode("-", $last);
            $order_code = $date .  (sprintf("%04s", $last[2] + 1));
        }
        return $order_code;
    }

    public function checkoutEvent(Request $request)
    {
        $snapToken = "";
        DB::beginTransaction();
        if($request->payment_method != ""){
            if(session()->get('orderId_Event')){
                $orderId = session()->get('orderId_Event');
                Transaction::where('orderId',$orderId)->delete();
                ItemTransaction::where('orderId',$orderId)->delete();
            }else{
                $orderId = 'EVN-'.$this->generateOrderCode();
            }

            if($request->payment_method == '0'){
                $getSnapToken = Midtrans::TokenSnap($orderId,$request->totalKeseluruhan);
                if($getSnapToken['status'] == false){
                    return response()->json($getSnapToken, 400, [] , JSON_UNESCAPED_SLASHES);
                }
                $snapToken = $getSnapToken['token'];
            }

            $rangeExpiredHour = GeneralSetting::where('name','setting_expired_payment');
            if($rangeExpiredHour){
                $rangeExpiredHour = $rangeExpiredHour->first()->value;
                $rangeExpiredHour = Carbon::now()->addHours($rangeExpiredHour);
            }else{
                $rangeExpiredHour = Carbon::now()->addHours(24);
            }

            try {
                $createTransaction = Transaction::create([
                    'expired_date'=>$rangeExpiredHour,
                    'orderId'=>$orderId,
                    'kategori'=>'Pembayaran Daftar Event',
                    'totalHarga' =>$request->totalHarga,
                    'totalOngkir' => 0,
                    'totalKeseluruhan' => $request->payment_method == '0' ? $request->totalKeseluruhan : ((int)$request->totalKeseluruhan + rand(100,999)),
                    'payment_method'=>$request->payment_method == '0' ? 'Midtrans' : $request->payment_method,
                    'va'=>$snapToken,
                    'cs_id'=>auth()->user()->id,
                    'kategori' => 'Pendaftaran Event & Training Online',
                    'type' => 0,
                ]);
    
                TransactionItem::create([
                    'orderId'=> $createTransaction->orderId,
                    'product_id'=>$request->namaEvent,
                    'qty'=> 1,
                    'harga'=>$request->payment_method == '0' ? $request->totalKeseluruhan : ((int)$request->totalKeseluruhan + rand(100,999)),
                    'created_at'=>now(),
                    'updated_at'=>now(),
                    'status'=> 0,
                    'cs_id'=>auth()->user()->id
                ]);
                DB::commit();
                $resp = [
                    'status'=>true,
                    'orderId'=>$orderId,
                    'token'=>$snapToken
                ];
                session()->put('orderId_Event',$orderId);
                return response()->json($resp, 200, [] , JSON_UNESCAPED_SLASHES);
            } catch (Exception $e) {
                DB::rollback();
                $resp = [
                    'status'=>false,
                    'message'=>$e->getMessage()
                ];
                return response()->json($resp, 400, [] , JSON_UNESCAPED_SLASHES);
            }
        }else{
            $resp = [
                'status'=>false,
                'message'=>'Silahkan pilih metode pembayaran terlebih dahulu'
            ];
            return response()->json($resp, 400, [] , JSON_UNESCAPED_SLASHES);
        }
        
    }
}
