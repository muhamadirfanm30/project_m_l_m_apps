<?php

namespace App\Http\Controllers\Customer;

use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Rules\MatchOldPassword;
use Illuminate\Http\Request;
use App\Helpers\ImageUpload;
use Validator;
use App\User;
use Auth;

class MyProfileController extends Controller
{
    public function updateWhatsappNo(Request $request)
    {
        User::where('id', Auth::user()->id)->update([
            'whatsapp_no' => str_replace(" ","",$request->whatsapp_no),
            'phone' => str_replace(" ","",$request->whatsapp_no),
        ]);
        return redirect()->back()
                        ->with('success','Phone number Updated successfully');
    }

    public function show()
    {
        $user = User::where('id', Auth::user()->id)->first();
        return view('customer.my-profile.show', [
            'user' => $user
        ]);
    }

    public function update($id)
    {
        $provinsi = $this->get_province();
        $dataEdit = User::where('id', Auth::user()->id)->first();
        return view('customer.my-profile.update', [
            'dataEdit' => $dataEdit,
            'provinsi' => $provinsi
        ]);
    }

    public function get_province(){
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.rajaongkir.com/starter/province",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "key: 60b137e92094b5e6fa05ddd10e2c1cbb"
          ),
        ));
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        
        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
            //ini kita decode data nya terlebih dahulu
            $response=json_decode($response,true);
            //ini untuk mengambil data provinsi yang ada di dalam rajaongkir resul
            $data_pengirim = $response['rajaongkir']['results'];
            return $data_pengirim;
        }
    }

    public function updateDataProfile(Request $request, $id)
    {
        // $this->validate($request, [
        //     'first_name' => 'required',
        //     'last_name' => 'required',
        //     'no_ktp' => 'required',
        //     'gender' => 'required',
        //     'phone' => 'required',
        //     'address' => 'required',
        //     'kode_pos' => 'required',
        //     'rekening_bank' => 'required',
        //     'nama_pemilik_rekening' => 'required',
        //     'nomor_rekening' => 'required',
        // ]);

        $cek = User::where('id', $id)->first();

        if(!empty($request->foto_ktp)){
            if ($request->hasFile('foto_ktp')) {
                $image1      = $request->file('foto_ktp');
                $file_name   = time() . '.' . $image1->getClientOriginalExtension();
                $img1 = Image::make($image1);
                $img1->stream(); // <-- Key point
                Storage::disk('local')->put('public/foto_ktp/' . $file_name, $img1);
    
                $exists1 = Storage::disk('local')->has('public/foto_ktp/' . $cek->foto_ktp);
                if ($exists1) {
                    Storage::delete('public/foto_ktp/' . $cek->foto_ktp);
                }
            }
        }else{
            $file_name = $cek->foto_ktp;
        }

        User::where('id', $id)->update([
            'first_name'  => $request->first_name,
            'last_name'  => $request->last_name,
            'no_ktp'  => $request->no_ktp,
            'gender'  => $request->gender,
            'phone'  => $request->phone,
            'address'  => $request->address,
            'province_id'  => $request->txtProvinsi,
            'kota_id'  => $request->txtKota,
            'kode_pos'  => $request->kode_pos,
            'rekening_bank'  => $request->rekening_bank,
            'nama_pemilik_rekening'  => $request->nama_pemilik_rekening,
            'nomor_rekening'  => $request->nomor_rekening,
            'foto_ktp' => $file_name
        ]);
            
        // status upgrade ms [
        //     0 => lengkapi data untuk ugrade
        //     1 => suskes upgrade,
        //     2 => cerify gagal,
        //     3 => akun diverufu admin
        // ]

        return redirect()->route('profile.show')
                        ->with('success','Profile Has been Updated');
    }

    public function updateDataProfileAdmin(Request $request, $id)
    {
        $cek = User::where('id', $id)->first();

        if(!empty($request->foto_ktp)){
            if ($request->hasFile('foto_ktp')) {
                $image1      = $request->file('foto_ktp');
                $file_name   = time() . '.' . $image1->getClientOriginalExtension();
                $img1 = Image::make($image1);
                $img1->stream(); // <-- Key point
                Storage::disk('local')->put('public/foto_ktp/' . $file_name, $img1);
    
                $exists1 = Storage::disk('local')->has('public/foto_ktp/' . $cek->foto_ktp);
                if ($exists1) {
                    Storage::delete('public/foto_ktp/' . $cek->foto_ktp);
                }
            }
        }else{
            $file_name = $cek->foto_ktp;
        }

        User::where('id', $id)->update([
            'first_name'  => $request->first_name,
            'last_name'  => $request->last_name,
            'no_ktp'  => $request->no_ktp,
            'gender'  => $request->gender,
            'phone'  => $request->phone,
            'address'  => $request->address,
            'province_id'  => $request->province_id,
            'kota_id'  => $request->kota_id,
            'kode_pos'  => $request->kode_pos,
            'rekening_bank'  => $request->rekening_bank,
            'nama_pemilik_rekening'  => $request->nama_pemilik_rekening,
            'nomor_rekening'  => $request->nomor_rekening,
            'foto_ktp' => $file_name
        ]);
            
        // status upgrade ms [
        //     0 => lengkapi data untuk ugrade
        //     1 => suskes upgrade,
        //     2 => cerify gagal,
        //     3 => akun diverufu admin
        // ]

        return redirect()->route('profile.admin.show')
                        ->with('success','Profile Has been Updated');
    }

    public function uploadImage(Request $request)
    {
        $dataUsers = User::where('id', Auth::user()->id)->first();
        //  dd($request->hasFile('image'));
        if ($request->hasFile('image')) {
            $image      = $request->file('image');
            $file_name   = time() . '.' . $image->getClientOriginalExtension();
            $img = Image::make($image);
            $img->stream(); // <-- Key point
            Storage::disk('local')->put('public/avatar-users/' . $file_name, $img);

            $exists = Storage::disk('local')->has('public/avatar-users/' . $dataUsers->avatar_user);
            if ($exists) {
                Storage::delete('public/avatar-users/' . $dataUsers->avatar_user);
            }
        }

        // return $file_name;

        User::where('id', Auth::user()->id)->update([
            'avatar_user' => $file_name,
        ]); 
    }

    public function admin_credential_rules(array $data)
    {
    $messages = [
        'current-password.required' => 'Please enter current password',
        'password.required' => 'Please enter password',
    ];

    $validator = Validator::make($data, [
        'current-password' => 'required',
        'password' => 'required|same:password',
        'password_confirmation' => 'required|same:password',     
    ], $messages);

    return $validator;
    }  

    public function changePassword(Request $request)
    {
        if(Auth::Check()){
            $request_data = $request->All();
            $validator = $this->admin_credential_rules($request_data);
            if($validator->fails()){
                // return response()->json(array('error' => $validator->getMessageBag()->toArray()), 400);
                return redirect()->back()->with("error","Kata sandi baru harus sama dengan kata sandi Anda yang telah dikonfirmasi. Harap ketik ulang kata sandi baru.");
            } else {  
                $current_password = Auth::User()->password;           
                if(Hash::check($request_data['current-password'], $current_password)){           
                    $user_id = Auth::User()->id;                       
                    $obj_user = User::find($user_id);
                    $obj_user->password = Hash::make($request_data['password']);
                    $obj_user->save(); 
                    return redirect()->back()->with("success","Password Berhasil diubah !");
                }else{           
                    return redirect()->back()->with("error","Kata sandi Anda saat ini tidak cocok dengan kata sandi yang Anda berikan. Silakan coba lagi.");
                    // $error = array('current-password' => 'Please enter correct current password');
                    // return response()->json(array('error' => $error), 400);   
                }
            }        
        }else{
            return redirect()->to('/customer/my-profile/show');
        }   
        // if(!(Hash::check($request->get('current-password'), Auth::user()->password))){
        //     return redirect()->back()->with("error","Kata sandi Anda saat ini tidak cocok dengan kata sandi yang Anda berikan. Silakan coba lagi.");
        // }
        // if(strcmp($request->get('current-password'), $request->get('new-password'))){
        //     return redirect()->back()->with("error","Kata sandi baru tidak boleh sama dengan kata sandi Anda saat ini. Pilih kata sandi yang berbeda.");
        // }
        // if(!(strcmp($request->get('new-password'), $request->get('new-password-confirm'))) == 0){
        //     return redirect()->back()->with("error","Kata sandi baru harus sama dengan kata sandi Anda yang telah dikonfirmasi. Harap ketik ulang kata sandi baru.");
        // }
        // $user = Auth::user();
        // $user->password = bcrypt($request->get('new-password'));
        // $user->save();

        // return redirect()->back()->with("success","Password Berhasil diubah !");

        // $request->validate([
        //     'current_password' => ['required', new MatchOldPassword],
        //     'new_password' => ['required'],
        //     'new_confirm_password' => ['same:new_password'],
        // ]);
   
        // User::find(auth()->user()->id)->update(['password'=> Hash::make($request->new_password)]);
   
        // return redirect()->back()->with("success","Password Berhasil diubah !");
 
    }
}
