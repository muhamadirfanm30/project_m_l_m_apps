<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Request;
use App\Model\LandingPage;
use App\Model\WhatsAppTemplate;
use App\Model\Product;
use App\Model\SlideShow;
use App\Model\GeneralSetting;
use App\User;
use DB;

class LandingPagesController extends Controller
{

    public function showPage()
    {
        $listWa = User::select('phone')->where('roles','customer')->whereNotNull('phone')->where('phone','!=','')->get()->toArray();
        $totalCS = count($listWa) - 1;
        $index = rand(0,$totalCS);
        $noWa = auth()->user()->phone !="" ? auth()->user()->phone : $listWa[$index]['phone'];
        $wa = WhatsAppTemplate::get();
        $showTemplate = LandingPage::orderBy('is_judul_landing_page', 'asc')->get();
        $showUrlVidio = GeneralSetting::where('name', 'landing_page_vidio')->first();
        
        return view('customer.landing-pages.index', [
            'showTemplate' => $showTemplate,
            'wa' => $wa,
            'noWa' => $noWa,
            'showUrlVidio' => $showUrlVidio
        ]);
    }

    public function pageTemplate(Request $request, $is_template, $phone)
    {
        $qw = str_replace('-', ' ', $is_template);
        $cek_segment = LandingPage::where('is_judul_landing_page', $qw)->first();

        if(empty($cek_segment)){
            return view('customer.404_form_landing_page');
        }else{
            $noWa = $phone;
            $is_template = base64_decode($is_template);
            // $message = base64_decode($message);

            if($cek_segment->is_landing_page == 1){
                // return 'ini segment 1';
                $slideShow1 = SlideShow::where('is_template', 1)->get();
                $page1 = LandingPage::where('is_judul_landing_page', $cek_segment->is_judul_landing_page)->first();
                if (!empty($page1)) {
                    return view('customer.landingPage.page1', [
                        'page1' => $page1,
                        'slideShow1' => $slideShow1,
                        'noWa'=>$noWa,
                    ]);
                } else {
                    return view('customer.404_form_landing_page');
                }
            }elseif($cek_segment->is_landing_page == 2){
                // return 'ini segment 2';
                $slideShow2 = SlideShow::where('is_template', 2)->get();
                $page2 = LandingPage::where('is_judul_landing_page', $cek_segment->is_judul_landing_page)->first();
                if (!empty($page2)) {
                    return view('customer.landingPage.page2', [
                        'page2' => $page2,
                        'slideShow2' => $slideShow2,
                        'noWa'=>$noWa,
                    ]);
                } else {
                    return view('customer.404_form_landing_page');
                }
                
            }elseif($cek_segment->is_landing_page == 3){
                // return 'ini segment 3';
                $page3 = LandingPage::where('is_judul_landing_page', $cek_segment->is_judul_landing_page)->first();
                if (!empty($page3)) {
                    return view('customer.landingPage.page3', [
                        'page3' => $page3,
                        'noWa'=>$noWa,
                    ]);
                } else {
                    return view('customer.404_form_landing_page');
                }
            }elseif($cek_segment->is_landing_page == 4){
                // return 'ini segment 4';
                $page4 = LandingPage::where('is_judul_landing_page', $cek_segment->is_judul_landing_page)->first();
                if (!empty($page4)) {
                    return view('customer.landingPage.page4', [
                        'page4' => $page4,
                        'noWa'=>$noWa,
                    ]);
                } else {
                    return view('customer.404_form_landing_page');
                }
            }elseif($cek_segment->is_landing_page == 5){
                // return 'ini segment 5';
                $page5 = LandingPage::where('is_judul_landing_page', $cek_segment->is_judul_landing_page)->first();
                if (!empty($page5)) {
                    return view('customer.landingPage.page5', [
                        'page5' => $page5,
                        'noWa'=>$noWa,
                    ]);
                } else {
                    return view('customer.404_form_landing_page');
                }
            }elseif($cek_segment->is_landing_page == 6){
                // return 'ini segment 6';
                $slideShow6 = SlideShow::where('is_template', 6)->get();
                $page6 = LandingPage::where('is_judul_landing_page', $cek_segment->is_judul_landing_page)->first();
                if (!empty($page6)) {
                    return view('customer.landingPage.page6', [
                        'page6' => $page6,
                        'slideShow6' => $slideShow6,
                        'noWa'=>$noWa,
                    ]);
                } else {
                    return view('customer.404_form_landing_page');
                }
            } else {
                return view('customer.404_form_landing_page');
            }
        }
    }


}
