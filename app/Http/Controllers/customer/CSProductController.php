<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Model\CsStockProduct;
use App\Model\Payment;
use App\Helpers\RajaOngkir;
use App\Model\Product;

use Validator;
use App\Model\NotifInfo;

use Illuminate\Support\Str;

use App\Model\Transaction;
use App\Model\ItemTransaction;
use App\Helpers\Midtrans;
use App\Model\GeneralSetting;
use Carbon\Carbon;
use DB;

class CSProductController extends Controller
{
    public function index(){
        $data = CsStockProduct::where('cs_id',auth()->user()->id)->get();
        return view('customer.cs-product.index',compact('data'));
    }

    public function sendItem(Request $request){
        if($request->isMethod('post')){
            session()->put('cart_stok_produk', $request->qtyCsStock);
        }else if($request->isMethod('delete')){
            $productId = $request->productId;
            $arr = session()->get('cart_stok_produk');
            if (($key = array_search($productId, $arr)) !== false) {
                unset($arr[$key]);
            }
            session()->put('cart_stok_produk', $arr);
        }
        return redirect()->route('myproduct-cart');
    }

    public function cart(Request $request){
        $session = session()->get('cart_stok_produk');
        // $dataProduct = Product::withTrashed()->whereRaw('id in(SELECT product_id FROM cs_stock_products where cs_id='.auth()->user()->id.')')->get();
        $dataProduct = Product::withTrashed()->whereIn('id',$session)->get();
        $listPayment = Payment::get();
        $midtrans = DB::table('payment_master')->where('id', 2)->where('is_active', 0)->first();
        $provinsi = RajaOngkir::getProvinsi()['data'];
        return view('customer.cs-product.checkout',compact(['provinsi','dataProduct','listPayment','midtrans']));
    }

    public function checkout(Request $request){
        $validator = Validator::make($request->all(), [
            'nama_lengkap'=>'required',
            'nomor_ponsel'=>'required',
            // 'nama_pengirim'=>'required',
            'alamat'=>'required',
            'namaPaketOngkir'=>'required',
            'payment_method'=>'required',
        ],[
            'namaPaketOngkir.required'=>'Layanan Kurir Wajib Dipilih'
        ]);
        
        if ($validator->fails()) {
            $messages = '';
            $i=1;
            foreach ($validator->errors()->all() as $message) {
                $messages .= $i.". ".$message."\n";
                $i++;
            }
            $messages =rtrim($messages,"\n");

            $err = [
                'status'=>false,
                'message'=>$messages
            ];
            return response()->json($err, 400, [] , JSON_UNESCAPED_SLASHES);
        }

        if(session()->get('orderId_Konsumen')){
            $orderId = session()->get('orderId_Konsumen');
            Transaction::where('orderId',$orderId)->delete();
            ItemTransaction::where('orderId',$orderId)->delete();
            TransactionShipment::where('orderId',$orderId)->delete();
        }else{
            $orderId = Midtrans::generateOrderId();
        }

        $item = [];
        DB::beginTransaction();
        try {
            foreach ($request->items as $id => $details){
                if($details['qty'] > 0){
                    $item[] = [
                        'own_transaction'=>$request->kirim_ke,//0 Ke Konsumen & 1 : Nimbun & 2 : Nimbun -> Konsumen
                        'orderId'=>$orderId,
                        'product_id'=>$details['id'],
                        'qty'=> $details['qty'],
                        'harga'=>0,
                        'is_produk' => 'get_produk_online',
                        'created_at'=>now(),
                        'updated_at'=>now()
                    ];
        
                    $decrementStok = CsStockProduct::where('product_id',$details['id'])->where('cs_id',auth()->user()->id)->first();
                    if($decrementStok){
                        if($details['qty'] > $decrementStok->stok){
                            $err = [
                                'status'=>false,
                                'message'=>"Sisa stok ".$details['nama_produk']." : ".$decrementStok->stok
                            ];
                            return response()->json($err, 400, [] , JSON_UNESCAPED_SLASHES);
                        }
            
                        $stok = (int)$decrementStok->stok - (int)$details['qty'];
                        $decrementStok->update(['stok'=>$stok]);
                    }else{
                        $err = [
                            'status'=>false,
                            'message'=>$details['nama_produk']." Tidak Tersedia.\nSilahkan QTY diisi 0"
                        ];
                        return response()->json($err, 400, [] , JSON_UNESCAPED_SLASHES);
                    }

                }
            }

            $snapToken = "";
            if($request->payment_method == '0'){
                $getSnapToken = Midtrans::TokenSnap($orderId,$request->totalKeseluruhan);
                if($getSnapToken['status'] == false){
                    return response()->json($getSnapToken, 400, [] , JSON_UNESCAPED_SLASHES);
                }
                $snapToken = $getSnapToken['token'];
            }

            $rangeExpiredHour = GeneralSetting::where('name','setting_expired_payment');
            if($rangeExpiredHour){
                $rangeExpiredHour = $rangeExpiredHour->first()->value;
                $rangeExpiredHour = Carbon::now()->addHours($rangeExpiredHour);
            }else{
                $rangeExpiredHour = Carbon::now()->addHours(24);
            }

            $detailTransaction = [
                'expired_date'=> $rangeExpiredHour,
                'total_berat'=>$request->total_berat,
                'province_id' => $request->province_id,
                'kota_id'=>$request->kota_id,
                'kode_pos'=>$request->kode_pos,
                'district_id'=>$request->district_id,
                'own_transaction'=>$request->kirim_ke,//0 Ke Konsumen & 1 : Nimbun
                'orderId'=>$orderId,
                'nama_lengkap' =>$request->nama_lengkap,
                'nomor_ponsel' =>$request->nomor_ponsel,
                'jenis_kelamin' =>$request->jenis_kelamin,
                'nama_pengirim' => auth()->user()->first_name.' '.auth()->user()->last_name,//$request->nama_pengirim,
                'alamat' =>$request->alamat,
                'kurir'=>$request->namaPaketOngkir,
                'totalHarga' =>$request->totalHarga,
                'totalOngkir' =>$request->totalOngkir,
                'totalKeseluruhan' =>$request->payment_method == '0' ? $request->totalKeseluruhan : ((int)$request->totalKeseluruhan + rand(100,999)),
                'payment_method'=>$request->payment_method == '0' ? 'Midtrans' : $request->payment_method,
                'va'=>$snapToken,
                'cs_id'=>auth()->user()->id,
                'kategori' => 'Ongkos Kirim Pengiriman Stok Produk',
                'own_product'=>1,
                'type' => 0,
            ];
            $saveTransaction = Transaction::create($detailTransaction);
            $saveItem = ItemTransaction::insert($item);
            NotifInfo::pushNotifStokProdukOnline($saveTransaction->id, 'admin');
            DB::commit();
            $resp = [
                'status'=>true,
                'orderId'=>$orderId,
                'token'=>$snapToken
            ];
            session()->put('orderId_Konsumen',$orderId);
            session()->forget('cart_konsumen');
            return response()->json($resp, 200, [] , JSON_UNESCAPED_SLASHES);
        }catch (\Exception $e) {
            DB::rollback();
            dd($e->getMessage());
            $resp = [
                'status'=>false,
                'orderId'=>'Gagal Transaksi',
            ];
            return response()->json($resp, 400, [] , JSON_UNESCAPED_SLASHES);
        }
    }
    
}
