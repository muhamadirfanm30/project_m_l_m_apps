<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Model\ItemTransaction;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Model\Transaction;
use App\Helpers\Midtrans;
use App\Mail\OrderEmail;
use App\Mail\OrderEmailAdmin;
use App\Model\Product;
use App\Model\Payment;
use App\Model\TransactionShipment;
use App\Model\KategoriProduk;
use App\Model\NotifInfo;
use App\Model\GeneralSetting;
use Carbon\Carbon;
use App\User;
use Validator;
use DB;
use Auth;

class ListProductAndPromoController extends Controller
{
    public function orderProduk(Request $request)
    {

        $showProduct = Product::whereNull('harga_promo');
        $listKategori = KategoriProduk::get();
        $getJudul = GeneralSetting::where('name', 'judul_tutorial_order')->first();
        $getVidio = GeneralSetting::where('name', 'vidio_tutorial_order')->first();
        $getDesc = GeneralSetting::where('name', 'deskripsi_tutorial_order')->first();
        $sort = $request->sort ?? 'default';
        $kategori = $request->kategori;
        if($sort){
            switch ($sort) {
                case 'terbaru':
                    $showProduct = $showProduct->orderBy('created_at','desc');
                    break;
                case 'termahal':
                    $showProduct = $showProduct->orderBy('harga','desc');
                    break;
                case 'termurah':
                    $showProduct = $showProduct->orderBy('harga','asc');
                    break;
                default:
                    $showProduct = $showProduct->orderBy('harga','asc');
                    break;
            }
        }

        if($kategori){
            $showProduct = $showProduct->where('kategori_produk_id',$kategori);
        }

        $showProduct = $showProduct->paginate(170);
        return view('customer.order-product.showProduct', compact('showProduct','sort','listKategori','kategori', 'getJudul', 'getVidio', 'getDesc'))
                ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function orderProdukPromo(Request $request)
    {

        $showProduct = Product::whereNotNull('harga_promo')
        // ->whereRaw(' ("'.date('Y-m-d 00:00:00').'" BETWEEN valid_at and expired_at) or (valid_at is not null and expired_at is null)')
        ->paginate(170);
        return view('customer.order-produk-promo.index', compact('showProduct'))
                ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function detailProduk($id)
    {
        $showProduct = Product::where('id', $id)->first();
        return view('customer.order-product.detail', compact('showProduct'));
    }

    public function generateOrderCode()
    {
        $q          = Transaction::count();
        $prefix     = 'O'; //prefix = O-16032020-00001
        $separator  = '-';
        $date       = date('dmy');
        $number     = 1; //format
        $new_number = sprintf("%04s", $number);
        $code       = $date .  ($new_number);
        $order_code   = $code;
        if ($q > 0) {
            $last     = Transaction::orderBy('id', 'desc')->value('orderId');
            $last     = explode("-", $last);
            $order_code = $date .  (sprintf("%04s", $last[2] + 1));
        }
        return $order_code;
    }

    // public function checkout(Request $request)
    // {
    //     if($request->isMethod('post')){
    //         try {
    //             //code...
    //         } catch (\Throwable $th) {
    //             //throw $th;
    //         }
    //         $validator = Validator::make($request->all(), [
    //             'nama_lengkap'=>'required_if:kirim_ke,0',
    //             'nomor_ponsel'=>'required_if:kirim_ke,0',
    //             'jenis_kelamin'=>'required_if:kirim_ke,0',
    //             'nama_pengirim'=>'required_if:kirim_ke,0',
    //             'alamat'=>'required_if:kirim_ke,0',
    //             'namaPaketOngkir'=>'required_if:kirim_ke,0',
    //             'payment_method'=>'required_if:kirim_ke,0',
    //         ],[
    //             'namaPaketOngkir.required'=>'Layanan Kurir Wajib Dipilih'
    //         ]);
            
    //         if ($validator->fails()) {
    //             $messages = '';
    //             $i=1;
    //             foreach ($validator->errors()->all() as $message) {
    //                 $messages .= $i.". ".$message."\n";
    //                 $i++;
    //             }
    //             $messages =rtrim($messages,"\n");

    //             $err = [
    //                 'status'=>false,
    //                 'message'=>$messages
    //             ];
    //             return response()->json($err, 400, [] , JSON_UNESCAPED_SLASHES);
    //         }

    //         $orderId = Midtrans::generateOrderId();
    //         $item = [];
    //         DB::beginTransaction();
    //         try {
    //             foreach (session('cart') as $id => $details){
    //                 $item[] = [
    //                     'own_transaction'=>$request->kirim_ke,//0 Ke Konsumen & 1 : Nimbun
    //                     'orderId'=>$orderId,
    //                     'product_id'=>$details['id'],
    //                     'qty'=> $details['qty'],
    //                     'harga'=>$details['qty'] * $details['harga'],
    //                     'created_at'=>now(),
    //                     'updated_at'=>now(),
    //                     // 'status'=> 0,
    //                     // 'cs_id'=>auth()->user()->id
    //                 ];
    
    //                 $decrementStok = Product::find($details['id']);
    //                 if($details['qty'] > $decrementStok->stok){
    //                     $err = [
    //                         'status'=>false,
    //                         'message'=>"Sisa stok ".$details['nama_produk']." : ".$decrementStok->stok
    //                     ];
    //                     return response()->json($err, 400, [] , JSON_UNESCAPED_SLASHES);
    //                 }
    
    //                 $stok = (int)$decrementStok->stok - (int)$details['qty'];
    //                 $decrementStok->update(['stok'=>$stok]);
    //             }

    //             DB::commit();
    //         } catch (\Exception $e) {
    //             DB::rollback();

    //         }
            
    //         // $hitPayment = Midtrans::Transaction($orderId,$request->totalKeseluruhan,$request->payment_method);
    //         if($hitPayment['status'] == true){
    //             $detailTransaction = [
    //                 'own_transaction'=>$request->kirim_ke,//0 Ke Konsumen & 1 : Nimbun
    //                 'orderId'=> $orderId,
    //                 'nama_lengkap' =>$request->nama_lengkap,
    //                 'nomor_ponsel' =>$request->nomor_ponsel,
    //                 'jenis_kelamin' =>$request->jenis_kelamin,
    //                 'nama_pengirim' =>$request->nama_pengirim,
    //                 'alamat' =>$request->alamat,
    //                 'kurir'=>$request->namaPaketOngkir,
    //                 'totalHarga' =>$request->totalHarga,
    //                 'totalOngkir' =>$request->totalOngkir,
    //                 'totalKeseluruhan' =>$request->totalKeseluruhan,
    //                 'payment_method'=>$request->payment_method,
    //                 'va'=>$hitPayment['detailVA'][0]->va_number,
    //                 'cs_id'=>auth()->user()->id,
    //                 'kategori' => 'Pembelian Produk',
    //                 'type' => 0,
    //             ];
    //             $saveTransaction = Transaction::create($detailTransaction);
    //             $saveItem = ItemTransaction::insert($item);
    //             // $this->sendMail($orderId);
    //             $resp = [
    //                 'status'=>true,
    //                 'orderId'=>$orderId,
    //             ];
    //             session()->forget('cart');
    //             return response()->json($resp, 200, [] , JSON_UNESCAPED_SLASHES);
    //         }else{
    //             return response()->json($hitPayment, 400, [] , JSON_UNESCAPED_SLASHES);
    //         }
    //     }
    //     $provinsi = $this->get_province();
    //     $listPayment = Payment::get();
    //     return view('customer.checkout.checkout-list', compact('provinsi', 'listPayment'));
    // }

    // public function sendMail()
    // {
    //     if(!empty(Auth::user()->email)){
    //         Mail::to(Auth::user()->email)->send(new OrderEmail($trxId));
    //         return "Email telah dikirim";
    //     }else{
    //         return "Email Gagal Terkirim";
    //     }
        
    // }

    public function sendMail($orderId)
    {
        $getEmailAdmin = User::where('roles', 'admin')->first();

        if(!empty(Auth::user()->email)){
            Mail::to(Auth::user()->email)->send(new OrderEmail($orderId));
            Mail::to($getEmailAdmin->email)->send(new OrderEmailAdmin($orderId));
            return "Email telah dikirim";
        }else{
            return "Email Gagal Terkirim";
        }
        
    }

    public function get_province(){
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.rajaongkir.com/starter/province",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "key: 60b137e92094b5e6fa05ddd10e2c1cbb"
          ),
        ));
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        
        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
            //ini kita decode data nya terlebih dahulu
            $response=json_decode($response,true);
            //ini untuk mengambil data provinsi yang ada di dalam rajaongkir resul
            $data_pengirim = $response['rajaongkir']['results'];
            return $data_pengirim;
        }
    }
    public function get_city($id){
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.rajaongkir.com/starter/city?&province=$id",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "key: 60b137e92094b5e6fa05ddd10e2c1cbb"
          ),
        ));
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        
        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
            $response=json_decode($response,true);
            $data_kota = $response['rajaongkir']['results'];
            return json_encode($data_kota);
        }
    }

    public function get_ongkir($origin, $destination, $weight, $courier){
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.rajaongkir.com/starter/cost",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            // CURLOPT_POSTFIELDS => "origin=501&destination=114&weight=1700&courier=jne",
            CURLOPT_POSTFIELDS => "origin=$origin&destination=$destination&weight=$weight&courier=$courier",
            CURLOPT_HTTPHEADER => array(
                "content-type: application/x-www-form-urlencoded",
                "key: 60b137e92094b5e6fa05ddd10e2c1cbb"
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $response=json_decode($response,true);
            $data_ongkir = $response['rajaongkir']['results'];
            return json_encode($data_ongkir);
        }
    }

    public function addToChart($id, $qty, Request $request)
    {
        
        $product = Product::find($id);
        if(!$product){
            abort(404);
        }

        if($product->stok < $request->qty){
            return redirect()->back()->with('error', 'Maaf Jumlah yang Anda Masukan Melebihi Stok yang Ada.');
        }else{
            $cart = session()->get('cart');
            $date = date('Y-m-d H:i:s');
            $harga = $product->harga;
            // return $cart;
            if($product->is_promo == 1 && ($date >= $product->valid_at && ($date <=$product->expired_at || $product->expired_at == "") ) ){
                $harga = $product->harga_promo;
            }

            if(!$cart){
                $cart = [
                    $id => [
                        "id"=>$id,
                        "nama_produk" => $product->nama_produk,
                        "qty" => $qty,
                        "harga" => $harga,
                        "harga_promo" => $product->harga_promo,
                        "image_produk" => $product->image_produk,
                        "berat" => $product->berat,
                        "panjang" => $product->panjang,
                        "lebar" => $product->lebar,
                        "tinggi" => $product->tinggi,
                    ]
                ];
                session()->put('cart', $cart);
                return redirect()->back()->with('success', 'Product added to cart successfully!');
            }

            if(isset($cart[$id])){
                $cart[$id]['qty']+=$qty;
                session()->put('cart', $cart);
                return redirect()->back()->with('success', 'Product added to cart successfully!');
            }

            $cart[$id] = [
                "id"=>$id,
                "nama_produk" => $product->nama_produk,
                "qty" => $qty,
                "harga" => $harga,
                "harga_promo" => $product->harga_promo,
                "image_produk" => $product->image_produk,
                "berat" => $product->berat,
                "panjang" => $product->panjang,
                "lebar" => $product->lebar,
                "tinggi" => $product->tinggi,
            ];
            session()->put('cart', $cart);
            return redirect()->back()->with('success', 'Product added to cart successfully!');
        }
    }

    public function update(Request $request)
    {
        if($request->id and $request->qty)
        {
            $cekStok = Product::find($request->id);
            if($request->qty > $cekStok->stok ){
                session()->flash('success', 'Stok produk hanya tersisa : '.$cekStok->stok);
            }else{
                $cart = session()->get('cart');
                $cart[$request->id]["qty"] = $request->qty;
                session()->put('cart', $cart);
                session()->flash('success', 'Cart updated successfully');
            }
        }
    }
    public function remove(Request $request)
    {
        if($request->id) {
            $cart = session()->get('cart');
            if(isset($cart[$request->id])) {
                unset($cart[$request->id]);
                session()->put('cart', $cart);
            }
            return redirect()->back()->with('success', 'Product added to cart successfully!');
        }
    }

    public function cart(Request $request)
    {
        $bank = db::table('payment_master')->where('id', 1)->where('is_active', 0)->first();
        $midtrans = db::table('payment_master')->where('id', 2)->where('is_active', 0)->first();
        $listPayment = Payment::get();
        $cart = session()->get('cart');
        if(!empty($cart)){
            if(count($cart) == 0){
                return redirect()->route('cs-order-produk.show');
            }
        }
        

        if($request->isMethod('post')){
            if(session()->get('orderId')){
                $orderId = session()->get('orderId');
                Transaction::where('orderId',$orderId)->delete();
                ItemTransaction::where('orderId',$orderId)->delete();
                TransactionShipment::where('orderId',$orderId)->delete();
            }else{
                $orderId = Midtrans::generateOrderId();
            }
            $snapToken = "";
            if($request->payment_method == '0'){
                $getSnapToken = Midtrans::TokenSnap($orderId,$request->totalKeseluruhan);
                if($getSnapToken['status'] == false){
                    return response()->json($getSnapToken, 400, [] , JSON_UNESCAPED_SLASHES);
                }
                $snapToken = $getSnapToken['token'];
            }
            $item = [];
            DB::beginTransaction();
            try {
                foreach ($request->items as $id => $details){
                    $item[] = [
                        'own_transaction'=>$request->kirim_ke,//0 Ke Konsumen & 1 : Nimbun
                        'orderId'=>$orderId,
                        'product_id'=>$details['id'],
                        'qty'=> $details['qty'],
                        'harga'=>$details['qty'] * $details['harga'],
                        'is_produk' => 'get_produk',
                        'created_at'=>now(),
                        'updated_at'=>now(),
                        // 'status'=> 0,
                        // 'cs_id'=>auth()->user()->id
                    ];

                    $decrementStok = Product::find($details['id']);
                    if($details['qty'] > $decrementStok->stok){
                        $err = [
                            'status'=>false,
                            'message'=>"Sisa stok ".$details['nama_produk']." : ".$decrementStok->stok
                        ];
                        return response()->json($err, 400, [] , JSON_UNESCAPED_SLASHES);
                    }

                    $stok = (int)$decrementStok->stok - (int)$details['qty'];
                    $decrementStok->update(['stok'=>$stok]);
                }
                $dataAlamat = [];
                
                $rangeExpiredHour = GeneralSetting::where('name','setting_expired_payment');
                if($rangeExpiredHour){
                    $rangeExpiredHour = $rangeExpiredHour->first()->value;
                    $rangeExpiredHour = Carbon::now()->addHours($rangeExpiredHour);
                }else{
                    $rangeExpiredHour = Carbon::now()->addHours(24);
                }

                if($request->kirim_ke == "0" || $request->kirim_ke == 0){
                    $detailTransaction = [
                        'own_transaction'=>0,//0 Ke Konsumen & 1 : Nimbun
                        'orderId'=> $orderId,
                        'nama_lengkap' => $request->detailAlamat[0]['namaLengkap'],
                        'nomor_ponsel' => $request->detailAlamat[0]['nomorPonsel'],
                        'province_id'  => $request->detailAlamat[0]['province_id'],
                        'kota_id'  => $request->detailAlamat[0]['kota_id'],
                        'district_id'  => $request->detailAlamat[0]['district_id'],
                        'kode_pos'  => $request->detailAlamat[0]['kode_pos'],
                        'total_berat'  => $request->detailAlamat[0]['totalBerat'],
                        'nama_pengirim' => auth()->user()->first_name.' '.auth()->user()->last_name,
                        'alamat' => $request->detailAlamat[0]['alamat'],
                        'kurir'=> $request->detailAlamat[0]['kurir'],
                        'totalHarga' =>$request->totalHarga,
                        'totalOngkir' =>$request->totalOngkir,
                        'totalKeseluruhan' =>$request->payment_method == '0' ? $request->totalKeseluruhan : ((int)$request->totalKeseluruhan + rand(111,999)),
                        'payment_method'=>$request->payment_method == '0' ? 'Midtrans' : $request->payment_method,
                        'va'=>$snapToken,
                        'cs_id'=>auth()->user()->id,
                        'kategori' => 'Pembelian Produk',
                        'type' => 0,
                        'expired_date'=>$rangeExpiredHour,
                    ];

                    if(count($request->detailAlamat) > 1 && $request->detailAlamat[0]['productId'] != "Semua Produk"){
                        foreach ($request->detailAlamat as $key => $value) {
                            $dataAlamat[]= [
                                'orderId'=>$orderId,
                                'nama_lengkap'=>$value['namaLengkap'],
                                'nomor_ponsel'=>$value['nomorPonsel'],
                                'nama_pengirim'=> auth()->user()->first_name.' '.auth()->user()->last_name,
                                'province_id'  => $value['province_id'],
                                'kota_id'  => $value['kota_id'],
                                'district_id'  => $value['district_id'],
                                'kode_pos'  => $value['kode_pos'],
                                'total_berat'  => $value['totalBerat'],
                                'alamat'=>$value['alamat'],
                                'kurir'=>$value['kurir'],
                                'totalOngkir'=>$value['totalOngkir'],
                                'productId'=>$value['productId'],
                                'qty'=>$value['qty'],
                            ];
                        }
                        TransactionShipment::insert($dataAlamat);
                    }
                }else{
                    $detailTransaction = [
                        'own_transaction'=>1,//0 Ke Konsumen & 1 : Nimbun
                        'orderId'=> $orderId,
                        'nama_lengkap' => '',
                        'nomor_ponsel' => '',
                        // 'jenis_kelamin' => '',
                        'nama_pengirim' => '',
                        'alamat' => '',
                        'kurir'=> '',
                        'totalHarga' =>$request->totalHarga,
                        'totalOngkir' =>$request->totalOngkir,
                        'totalKeseluruhan' =>$request->payment_method == '0' ? $request->totalKeseluruhan : ((int)$request->totalKeseluruhan + rand(100,999)),
                        'payment_method'=>$request->payment_method == '0' ? 'Midtrans' : $request->payment_method,
                        'va'=>$snapToken,
                        'cs_id'=>auth()->user()->id,
                        'kategori' => 'Stok Produk',
                        'type' => 0,
                        'expired_date'=>$rangeExpiredHour,
                    ];
                }
                $saveTransaction = Transaction::create($detailTransaction);
                $saveItem = ItemTransaction::insert($item);
                $this->sendMail($orderId);

                // push notif info
                NotifInfo::pushNotifDataTransaksi($saveTransaction->id, 'admin');
                DB::commit();
                $resp = [
                    'status'=>true,
                    'orderId'=>$orderId,
                    'token'=>$snapToken
                ];
                session()->put('orderId',$orderId);
                session()->forget('cart');
                return response()->json($resp, 200, [] , JSON_UNESCAPED_SLASHES);
            } catch (\Exception $e) {
                DB::rollback();
                $resp = [
                    'status'=>false,
                    'orderId'=>$e->getMessage()
                ];
                return response()->json($resp, 400, [] , JSON_UNESCAPED_SLASHES);
            }
        }
        return view('customer.cart.cart',compact('listPayment', 'bank', 'midtrans'));
    }

    public function itemCheckout($id,$qty, Request $request){
        $product = Product::find($id);
        if(!$product){
            abort(404);
        }

        if($product->stok < $request->qty){
            return redirect()->back()->with('error', 'Maaf Jumlah yang Anda Masukan Melebihi Stok yang Ada.');
        }else{
            // return 'bisa';
            $cart = session()->get('cart');
            $date = date('Y-m-d H:i:s');
            $harga = $product->harga;
            // return $cart;
            if($product->is_promo == 1 && ($date >= $product->valid_at && ($date <=$product->expired_at || $product->expired_at == "") ) ){
                $harga = $product->harga_promo;
            }

            if(!$cart){
                $cart = [
                    $id => [
                        "id"=>$id,
                        "nama_produk" => $product->nama_produk,
                        "qty" => $qty,
                        "harga" => $harga,
                        "harga_promo" => $product->harga_promo,
                        "image_produk" => $product->image_produk,
                        "berat" => $product->berat,
                        "panjang" => $product->panjang,
                        "lebar" => $product->lebar,
                        "tinggi" => $product->tinggi,
                    ]
                ];
                session()->put('cart', $cart);
                return redirect()->route('pageCart');
            }

            if(isset($cart[$id])){
                $cart[$id]['qty']+=$qty;
                session()->put('cart', $cart);
                return redirect()->route('pageCart');
            }

            $cart[$id] = [
                "id"=>$id,
                "nama_produk" => $product->nama_produk,
                "qty" => $qty,
                "harga" => $harga,
                "harga_promo" => $product->harga_promo,
                "image_produk" => $product->image_produk,
                "berat" => $product->berat,
                "panjang" => $product->panjang,
                "lebar" => $product->lebar,
                "tinggi" => $product->tinggi,
            ];
            session()->put('cart', $cart);
            return redirect()->route('pageCart');
        }
        
    }
}
