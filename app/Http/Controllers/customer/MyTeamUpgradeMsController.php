<?php

namespace App\Http\Controllers\customer;

use App\Http\Controllers\Controller;
use App\Model\PositionUpdateModel;
use Illuminate\Http\Request;
use App\User;
use Auth;

class MyTeamUpgradeMsController extends Controller
{
    public function index(Type $var = null)
    {
        $no = 1;
        $dataUpgradeMs = User::where('is_upgrade_ms', 3)->where('is_admin_created', 0)->with('get_posisi')->orderBy('id', 'DESC')->get();
        return view('customer.my-team-upgrade-ms.show', [
            'dataUpgradeMs' => $dataUpgradeMs,
            'no' => $no
        ]);
    }

    public function update($id)
    {
        $data = User::find($id);
        return view('customer.my-team-upgrade-ms.updatePosisiManual', [
            'data' => $data,
        ]);
    }

    public function updateRandom($id)
    {
        $datas = User::find($id);
        return view('customer.my-team-upgrade-ms.updatePosisiRandom', [
            'datas' => $datas,
        ]);
    }

    public function storePosition(Request $request)
    {
        if($request->posisi == ""){
            return redirect()->back()
                        ->with('error','Position can not be empty');
        }else{
            $this->validate($request, [
                'nama_calom_ms' => 'required',
                'posisi' => 'required',
                'nama_id_sponsor' => 'required',
                'no_id_sponsor' => 'required',
                'no_id_sponsor_upline' => 'required',
                'nama_upline' => 'required'

            ]);

            User::where('id', $request->id)->update([
                'is_random_position' => 0,
                'sponsor_upline_id' => $request->no_id_sponsor_upline, 
                'sponsor_upline_name' => $request->nama_upline, 
                // 0 : di tentukan manual
                // 1 : di tentukan random
            ]);
    
            PositionUpdateModel::create([
                'nama_calom_ms' => $request->nama_calom_ms,
                'calon_ms_id' => $request->calon_member_id,
                'posisi' => $request->posisi,
                'user_sponsor_name' => $request->nama_id_sponsor,
                'user_sponsor_status' => $request->no_id_sponsor,
                'user_sponsor_id' => Auth::user()->id,
            ]);
    
            return redirect()->route('teams.show')
                            ->with('success','Position Confirmed');
        }
    }

    public function storeRandomPosition(Request $request)
    {
        User::where('id', $request->id)->update([
            'is_random_position' => 1, 
            // 0 : di tentukan manual
            // 1 : di tentukan random
        ]);

        PositionUpdateModel::create([
            'nama_calom_ms' => $request->nama_calom_ms,
            'calon_ms_id' => $request->calon_member_id,
            'posisi' => '',
            'user_sponsor_name' => Auth::user()->first_name,
            'user_sponsor_status' => Auth::user()->membership_id,
            'user_sponsor_id' => Auth::user()->id,
        ]);

        return redirect()->route('teams.show')
                        ->with('success','Position Confirmed');
    }
}
