<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Kategori;
use App\Model\SubKategori;
use App\Model\Konten;

class KontenController extends Controller
{
    public function index(){
        $data = Kategori::whereRaw('id IN(SELECT kategori_id FROM konten)')->get();
        return view('customer.konten.index',compact('data'));
    }

    public function detail($id){
        $kategori = SubKategori::whereRaw('id IN (SELECT sub_kategori_id FROM konten WHERE kategori_id = '.$id.')')->get();
        $konten  = Konten::where('kategori_id',$id)->get();
        $detailData = Kategori::where('id',$id)->first();
        return view('customer.konten.detail',compact('konten','kategori','id','detailData'));
    }
}
