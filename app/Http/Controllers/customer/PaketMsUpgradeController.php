<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Model\PaketUpgradeMS;
use App\Model\TransactionItem;
use App\Model\ItemTransaction;
use Illuminate\Support\Str;
use App\Mail\PaketUpgradeMsEmail;
use App\Model\Transaction;
use App\Model\Payment;
use App\Helpers\Midtrans;
use App\Model\GeneralSetting;
use Carbon\Carbon;
use App\Model\NotifInfo;
use Validator;
use Auth;
use DB;

class PaketMsUpgradeController extends Controller
{
    public function index(Request $request)
    {
        if(Auth::user()->is_upgrade_ms != 1){
            $showPaket = PaketUpgradeMS::paginate(17);
            return view('customer.paketUpgradeMs.index', [
                'showPaket' => $showPaket
            ])->with('i', ($request->input('page', 1) - 1) * 5);
        }else{
            return view('customer.404_form');
        }
        
    }

    public function detail($id)
    {
        $showProduct = PaketUpgradeMS::where('id', $id)->first();
        return view('customer.paketUpgradeMs.detail', [
            'showProduct' => $showProduct
        ]);
    }

    public function checkoutPaketMs($id)
    {
        $bank = db::table('payment_master')->where('id', 1)->where('is_active', 0)->first();
        $midtrans = db::table('payment_master')->where('id', 2)->where('is_active', 0)->first();
        $data = PaketUpgradeMS::where('id', $id)->get();
        $listPayment = Payment::get();
        return view('customer.paketUpgradeMs.checkoutPaket', [
            'data' => $data,
            'listPayment' => $listPayment,
            'bank' => $bank,
            'midtrans' => $midtrans,
        ]);
    }

    public function generateOrderCode()
    {
        $q          = Transaction::count();
        $prefix     = 'O'; //prefix = O-16032020-00001
        $separator  = '-';
        $date       = date('dmys');
        $number     = 1; //format
        $new_number = sprintf("%04s", $number);
        $code       = $date .  ($new_number);
        $order_code   = $code;
        if ($q > 0) {
            $last     = Transaction::orderBy('id', 'desc')->value('orderId');
            $last     = explode("-", $last);
            $order_code = $date .  (sprintf("%04s", $last[2] + 1));
        }
        return $order_code;
    }

    public function checkout(Request $request)
    {
        if($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'nama_lengkap'=>'required_if:kirim_ke,0',
                'nomor_ponsel'=>'required_if:kirim_ke,0',
                'jenis_kelamin'=>'required_if:kirim_ke,0',
                // 'nama_pengirim'=>'required_if:kirim_ke,0',
                'alamat'=>'required_if:kirim_ke,0',
                'namaPaketOngkir'=>'required_if:kirim_ke,0',
                'payment_method'=>'required_if:kirim_ke,0',
            ],[
                'namaPaketOngkir.required'=>'Layanan Kurir Wajib Dipilih'
            ]);
            
            if ($validator->fails()) {
                $messages = '';
                $i=1;
                foreach ($validator->errors()->all() as $message) {
                    $messages .= $i.". ".$message."\n";
                    $i++;
                }
                $messages =rtrim($messages,"\n");

                $err = [
                    'status'=>false,
                    'message'=>$messages
                ];
                return response()->json($err, 400, [] , JSON_UNESCAPED_SLASHES);
            }

            if(session()->get('orderId_MsUpgrade')){
                $orderId = session()->get('orderId_MsUpgrade');
                Transaction::where('orderId',$orderId)->delete();
                ItemTransaction::where('orderId',$orderId)->delete();
            }else{
                $orderId = Midtrans::generateOrderId();
            }

            $length = 9;
            $ms_code = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';

                $createTransactionDetail = [
                    'orderId'=>$orderId,
                    'product_id'=>$request->id,
                    'qty'=> $request->qty,
                    'harga'=> $request->qty * $request->harga,
                    'created_at'=>now(),
                    'updated_at'=>now(),
                    'status'=> 0,
                    'is_produk' => 'get_paket',
                    'cs_id'=> Auth::user()->id
                ];

                $decrementStok = PaketUpgradeMS::find($request->id);
                if($request->qty > $decrementStok->stok){
                    $err = [
                        'status'=>false,
                        'message'=>"Sisa stok ".$request->nama_produk." : ".$decrementStok->stok
                    ];
                    return response()->json($err, 400, [] , JSON_UNESCAPED_SLASHES);
                }

                $stok = (int)$decrementStok->stok - (int)$request->qty;
                $decrementStok->update(['stok'=>$stok]);
            // }

            $snapToken = "";
            if($request->payment_method == '0'){
                $getSnapToken = Midtrans::TokenSnap($orderId,$request->totalKeseluruhan);
                if($getSnapToken['status'] == false){
                    return response()->json($getSnapToken, 400, [] , JSON_UNESCAPED_SLASHES);
                }
                $snapToken = $getSnapToken['token'];
            }

            $rangeExpiredHour = GeneralSetting::where('name','setting_expired_payment');
            if($rangeExpiredHour){
                $rangeExpiredHour = $rangeExpiredHour->first()->value;
                $rangeExpiredHour = Carbon::now()->addHours($rangeExpiredHour);
            }else{
                $rangeExpiredHour = Carbon::now()->addHours(24);
            }
            DB::beginTransaction();
            try {
                $detailTransaction = [
                    'expired_date'=>$rangeExpiredHour,
                    'own_transaction'=>0,
                    'orderId'=>$orderId,
                    'total_berat'=>$request->total_berat,
                    'province_id' => $request->province_id,
                    'kota_id'=>$request->kota_id,
                    'kode_pos'=>$request->kode_pos,
                    'district_id'=>$request->district_id,
                    'nama_lengkap' =>$request->nama_lengkap,
                    'nomor_ponsel' =>$request->nomor_ponsel,
                    'jenis_kelamin' =>$request->jenis_kelamin,
                    'nama_pengirim' =>auth()->user()->first_name.' '.auth()->user()->last_name,//$request->nama_pengirim,
                    'alamat' =>$request->alamat,
                    'kurir'=>$request->namaPaketOngkir,
                    'totalHarga' =>$request->totalHarga,
                    'totalOngkir' =>$request->totalOngkir,
                    'totalKeseluruhan' =>$request->payment_method == '0' ? $request->totalKeseluruhan : ((int)$request->totalKeseluruhan + rand(100,999)),
                    'payment_method'=>$request->payment_method == '0' ? 'Midtrans' : $request->payment_method,
                    'va'=>$snapToken,
                    'cs_id'=> Auth::user()->id,
                    'kategori' => 'Akumulasi Paket MS Upgrade',
                    'type' => 0,
                    'email' =>$request->email,
                ];
                $saveTransaction = Transaction::create($detailTransaction);
                $saveItem = ItemTransaction::insert($createTransactionDetail);
                NotifInfo::pushNotifPaketUpgrade($saveTransaction->id, 'admin');
                if(!empty(Auth::user()->email)){
                    Mail::to(Auth::user()->email)->send(new PaketUpgradeMsEmail($orderId));
                    // return "Email telah dikirim";
                }else{
                    // return "Email Gagal Terkirim";
                }
                DB::commit();
                $resp = [
                    'status'=>true,
                    'orderId'=>$orderId,
                    'token'=>$snapToken
                ];
                session()->put('orderId_MsUpgrade',$orderId);
                return response()->json($resp, 200, [] , JSON_UNESCAPED_SLASHES);
            }catch (\Exception $e) {
                DB::rollback();
                dd($e->getMessage());
                $resp = [
                    'status'=>false,
                    'orderId'=>'Gagal Transaksi',
                ];
                return response()->json($resp, 400, [] , JSON_UNESCAPED_SLASHES);
            }
           
            // }else{
                // return response()->json($hitPayment, 400, [] , JSON_UNESCAPED_SLASHES);
            // }
        }
        // $provinsi = $this->get_province();
        // return view('customer.checkout.checkout-list', compact('provinsi'));
    }
}
