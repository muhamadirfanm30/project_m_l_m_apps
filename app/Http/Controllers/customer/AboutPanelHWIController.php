<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\PanelHWI;

class AboutPanelHWIController extends Controller
{
    public function index()
    {
        $show = PanelHWI::get();
        return view('customer.about-panel-hwi.index', [
            'show' => $show
        ]);
    }
}
