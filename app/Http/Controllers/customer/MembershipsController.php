<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Model\Transaction;
use App\Model\GeneralSetting;
use App\Model\NotifInfo;
use Auth;
use DB;
use App\Helpers\ImageUpload;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class MembershipsController extends Controller
{
    // memberships
    public function showMemberShip()
    {
        $getValue = GeneralSetting::where('name', 'akumulasi_upgrade_ms')->first();
        $getUrl = DB::table('general_settings')->where('id', 3)->first();
        $getSales = Transaction::where('cs_id', Auth::user()->id)->whereIn('status', [1,4,5])->get();
        $cek_ms = Transaction::where('cs_id', Auth::user()->id)->where('kategori', 'Akumulasi Paket MS Upgrade')->whereIn('status', [1,4,5])->count();
        $getProdukSales = Transaction::where('cs_id', Auth::user()->id)->with('order_detail')->where('status', [1,4,5])->get();
        $getMyTeam = User::where('referal_code', Auth::user()->membership_id)->whereNotNull('referal_code')->count();
        return view('customer.memberships.index', [
            'getSales' => $getSales,
            'getProdukSales' => $getProdukSales,
            'getMyTeam' => $getMyTeam,
            'getUrl' => $getUrl,
            'cek_ms' => $cek_ms,
            'getValue' => $getValue,
        ]);
    }

    public function upgradeMember($id)
    {
        $getDataUser = User::where('id', Auth::user()->id)->first();
        $provinsi = $this->get_province();
        return view('customer.memberships.upgradeMS',  [
            'provinsi' => $provinsi,
            'getDataUser' => $getDataUser
        ]);
    }

    public function storeMemberMS(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'no_ktp' => 'required',
            'gender' => 'required',
            'whatsapp_no' => 'required',
            'address' => 'required',
            'kode_pos' => 'required',
        ]);

        $path = User::getImagePathUpload();
        $filename = null;

        if ($request->foto_ktp != null) {
            $foto_ktp = (new ImageUpload)->upload($request->foto_ktp, $path);
            $file_name = $foto_ktp->getFilename();
        }else{
            $file_name = '';
        }

        $save = User::where('id', Auth::user()->id)->update([
            'first_name'  => $request->first_name,
            'last_name'  => $request->last_name,
            'no_ktp'  => $request->no_ktp,
            'gender'  => $request->gender,
            'whatsapp_no'  => $request->whatsapp_no,
            'address'  => $request->address,
            'province_id'  => $request->province_id,
            'kota_id'  => $request->kota_id,
            'kode_pos'  => $request->kode_pos,
            'rekening_bank'  => $request->rekening_bank,
            'nama_pemilik_rekening'  => $request->nama_pemilik_rekening,
            'nomor_rekening'  => $request->nomor_rekening,
            'is_upgrade_ms'  => 3,
            'foto_ktp' => $file_name
        ]);

        $cek_created_admin = User::where('id', Auth::user()->id)->first();
        if($cek_created_admin->is_admin_created == 1){
            NotifInfo::pushNotifLeaderUpgrade($cek_created_admin->id, 'admin');
        }else{
            NotifInfo::pushNotifMyTeamUpgrade($cek_created_admin->id, 'admin');
        }
            
        // status upgrade ms [
        //     0 => lengkapi data untuk ugrade
        //     1 => suskes upgrade,
        //     2 => cerify gagal,
        //     3 => akun diverufu admin
        // ]

        return redirect()->route('member.show')
                        ->with('success','Upgrade Membership Telah berhasil');
    }

    public function get_province(){
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.rajaongkir.com/starter/province",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "key: 60b137e92094b5e6fa05ddd10e2c1cbb"
          ),
        ));
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        
        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
            //ini kita decode data nya terlebih dahulu
            $response=json_decode($response,true);
            //ini untuk mengambil data provinsi yang ada di dalam rajaongkir resul
            $data_pengirim = $response['rajaongkir']['results'];
            return $data_pengirim;
        }
    }
}
