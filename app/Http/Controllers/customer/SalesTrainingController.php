<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\SalesAndTraining;
use App\Model\ModelVidio;

class SalesTrainingController extends Controller
{
    public function salesTraining(Request $request)
    {
        $getSalesTraining = SalesAndTraining::orderBy('id', 'DESC')->paginate(4);
        return view('customer.salesTraining.index',compact('getSalesTraining'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
       
    }

    public function salesTrainingDetail($id)
    {
        $getVidio = ModelVidio::where('parent_id', $id)->get();
        return view('customer.salesTraining.detail', [
            'getVidio' => $getVidio
        ]);
    }
}
