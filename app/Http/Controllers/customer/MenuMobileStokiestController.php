<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\MobileStokiest;
use App\Model\BonusMS;
use App\Model\PaketReguler;
use App\Model\Payment;
use Illuminate\Support\Str;

use App\Model\GeneralSetting;
use App\Model\Transaction;
use App\Model\ItemTransaction;

use App\Model\Product;
use Carbon\Carbon;
use App\Model\NotifInfo;
use Validator;
use Auth;
use App\User;
use App\Helpers\Midtrans;
use DB;

class MenuMobileStokiestController extends Controller
{
    // penjelasan menu baru
    public function showMenuBaruMS()
    {
        $menuBaruMS = MobileStokiest::first();
        return view('customer.penjelasan-menu-baru-ms.index', [
            'menuBaruMS' => $menuBaruMS
        ]);
    }

    /// bonus member MS

    public function showBonusMember()
    {
        $getBonusMS = BonusMS::orderBy('id', 'DESC')->get();
        return view('customer.bonus-member-ms.index', [
            'getBonusMS' => $getBonusMS
        ]);
    }


    // paket reguler

    public function showPaketReguler(Request $request)
    {
        $paketReguler = PaketReguler::paginate(8);
        return view('customer.paket-reguler.index', [
            'paketReguler' => $paketReguler
        ]) ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function detailPaketReguler($id)
    {
        $detail = PaketReguler::where('id', $id)->first();
        return view('customer.paket-reguler.detail', [
            'detail' => $detail
        ]);
    }

    public function packageCheckout($id)
    {
        $bank = db::table('payment_master')->where('id', 1)->where('is_active', 0)->first();
        $midtrans = db::table('payment_master')->where('id', 2)->where('is_active', 0)->first();
        $provinsi = $this->get_province();
        $listPayment = Payment::get();
        $data = PaketReguler::where('id', $id)->get();
        return view('customer.paket-reguler.checkoutPaket', [
            'data' => $data,
            'provinsi' => $provinsi,
            'listPayment' => $listPayment,
            'bank' => $bank,
            'midtrans' => $midtrans,

        ]);
    }

    public function get_province(){
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.rajaongkir.com/starter/province",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "key: 60b137e92094b5e6fa05ddd10e2c1cbb"
          ),
        ));
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        
        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
            //ini kita decode data nya terlebih dahulu
            $response=json_decode($response,true);
            //ini untuk mengambil data provinsi yang ada di dalam rajaongkir resul
            $data_pengirim = $response['rajaongkir']['results'];
            return $data_pengirim;
        }
    }
    public function get_city($id){
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.rajaongkir.com/starter/city?&province=$id",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "key: 60b137e92094b5e6fa05ddd10e2c1cbb"
          ),
        ));
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        
        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
            $response=json_decode($response,true);
            $data_kota = $response['rajaongkir']['results'];
            return json_encode($data_kota);
        }
    }
    public function get_ongkir($origin, $destination, $weight, $courier){
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.rajaongkir.com/starter/cost",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            // CURLOPT_POSTFIELDS => "origin=501&destination=114&weight=1700&courier=jne",
            CURLOPT_POSTFIELDS => "origin=$origin&destination=$destination&weight=$weight&courier=$courier",
            CURLOPT_HTTPHEADER => array(
                "content-type: application/x-www-form-urlencoded",
                "key: 60b137e92094b5e6fa05ddd10e2c1cbb"
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $response=json_decode($response,true);
            $data_ongkir = $response['rajaongkir']['results'];
            return json_encode($data_ongkir);
        }
    }

    public function generateOrderCode()
    {
        $q          = Transaction::count();
        $prefix     = 'O'; //prefix = O-16032020-00001
        $separator  = '-';
        $date       = date('dmys');
        $number     = 1; //format
        $new_number = sprintf("%04s", $number);
        $code       = $date .  ($new_number);
        $order_code   = $code;
        if ($q > 0) {
            $last     = Transaction::orderBy('id', 'desc')->value('orderId');
            $last     = explode("-", $last);
            $order_code = $date .  (sprintf("%04s", $last[2] + 1));
        }
        return $order_code;
    }

    public function checkout(Request $request)
    {
        // return $request->berat;
        if($request->isMethod('post')){
            $validator = Validator::make($request->all(), [
                'nama_lengkap'=>'required_if:kirim_ke,0',
                'nomor_ponsel'=>'required_if:kirim_ke,0',
                // 'nama_pengirim'=>'required_if:kirim_ke,0',
                'alamat'=>'required_if:kirim_ke,0',
                'namaPaketOngkir'=>'required_if:kirim_ke,0',
                'payment_method'=>'required_if:kirim_ke,0',
            ],[
                'namaPaketOngkir.required'=>'Layanan Kurir Wajib Dipilih'
            ]);
            
            if ($validator->fails()) {
                $messages = '';
                $i=1;
                foreach ($validator->errors()->all() as $message) {
                    $messages .= $i.". ".$message."\n";
                    $i++;
                }
                $messages =rtrim($messages,"\n");

                $err = [
                    'status'=>false,
                    'message'=>$messages
                ];
                return response()->json($err, 400, [] , JSON_UNESCAPED_SLASHES);
            }

            if(session()->get('orderId')){
                $orderId = session()->get('orderId');
                Transaction::where('orderId',$orderId)->delete();
                ItemTransaction::where('orderId',$orderId)->delete();
                TransactionShipment::where('orderId',$orderId)->delete();
            }else{
                $orderId = Midtrans::generateOrderId();
            }
            $length = 9;
            $ms_code = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';

            $rangeExpiredHour = GeneralSetting::where('name','setting_expired_payment');
            if($rangeExpiredHour){
                $rangeExpiredHour = $rangeExpiredHour->first()->value;
                $rangeExpiredHour = Carbon::now()->addHours($rangeExpiredHour);
            }else{
                $rangeExpiredHour = Carbon::now()->addHours(24);
            }
            // $item = [];
            // foreach (session('cart') as $id => $details){
                // $createNewUser = User::create([
                //     'first_name' =>$request->nama_lengkap,
                //     'phone' =>$request->nomor_ponsel,
                //     'whatsapp_no' =>$request->nomor_ponsel,
                //     'gender' =>$request->jenis_kelamin,
                //     'province_id' =>$request->province_id,
                //     'kota_id' =>$request->kota_id,
                //     'email' =>$request->email,
                //     'is_tmp_user' => 0,
                //     'is_approve_admin' => 0,
                //     'status' => 0,
                //     'membership_id' => 'R-'.substr(str_shuffle(str_repeat($ms_code, 5)), 0, $length),
                //     'membersip_status' => 'Reguler',
                //     'address' =>$request->alamat,
                //     'referal_code' => Auth::user()->membership_id,
                //     'created_at'=>now(),
                //     'updated_at'=>now(),
                // ]);

                $createTransactionDetail = [
                    'orderId'=>$orderId,
                    'product_id'=>$request->id,
                    'qty'=> $request->qty,
                    'harga'=> $request->qty * $request->harga,
                    'created_at'=>now(),
                    'updated_at'=>now(),
                    'status'=> 0,
                    'is_produk' => 'get_paket_reguler',
                    'cs_id'=> Auth::user()->id,
                    'berat' => $request->berat
                ];

                $decrementStok = PaketReguler::find($request->id);
                if($request->qty > $decrementStok->stok){
                    $err = [
                        'status'=>false,
                        'message'=>"Sisa stok ".$request->nama_produk." : ".$decrementStok->stok
                    ];
                    return response()->json($err, 400, [] , JSON_UNESCAPED_SLASHES);
                }

                $stok = (int)$decrementStok->stok - (int)$request->qty;
                $decrementStok->update(['stok'=>$stok]);
            // }

            
            $snapToken = "";
            if($request->payment_method == '0'){
                $getSnapToken = Midtrans::TokenSnap($orderId,$request->totalKeseluruhan);
                if($getSnapToken['status'] == false){
                    return response()->json($getSnapToken, 400, [] , JSON_UNESCAPED_SLASHES);
                }
                $snapToken = $getSnapToken['token'];
            }

            DB::beginTransaction();
            try 
            { 
                $detailTransaction = [
                    'expired_date'=>$rangeExpiredHour,
                    'own_transaction'=>0,
                    'orderId'=>$orderId,
                    'nama_lengkap' =>$request->nama_lengkap,
                    'nomor_ponsel' => str_replace(" ", "", $request->nomor_ponsel),
                    // 'jenis_kelamin' =>$request->jenis_kelamin,
                    'nama_pengirim' => auth()->user()->first_name.' '.auth()->user()->last_name,//$request->nama_pengirim,
                    'alamat' =>$request->alamat,
                    'kurir'=>'-',
                    'totalOngkir'=>0,
                    'totalHarga' =>$request->totalHarga,
                    'totalOngkir' =>0,
                    'totalKeseluruhan' =>$request->payment_method == '0' ? $request->totalKeseluruhan : ((int)$request->totalKeseluruhan + rand(100,999)),
                    'payment_method'=>$request->payment_method == '0' ? 'Midtrans' : $request->payment_method,
                    'va'=>$snapToken,
                    'cs_id'=> Auth::user()->id,
                    'kategori' => 'Pendaftaran Member Reguler',
                    'type' => 1,
                    'email' =>$request->email,
                    'province_id' => $request->get_prov,
                    'kota_id' => $request->get_kota,
                    'district_id' => $request->get_kabupaten,
                    'kode_pos' => $request->kode_pos,
                    'is_input_new_meber' => 1,
                    'total_berat' => $request->berat,
                ];

                $saveTransaction = Transaction::create($detailTransaction);
                $saveItem = ItemTransaction::insert($createTransactionDetail);
                // push notif info
                NotifInfo::pushNotifRegulerMember($saveTransaction->id, 'admin');
                DB::commit();
                $resp = [
                    'status'=>true,
                    'orderId'=>$orderId,
                    'token'=>$snapToken
                ];
                session()->put('orderId_regulerMember',$orderId);
                return response()->json($resp, 200, [] , JSON_UNESCAPED_SLASHES);
            } catch (\Exception $e) {
                DB::rollback();
                $resp = [
                    'status'=>false,
                    'orderId'=>'Gagal Transaksi',
                ];
                throw($e);
                return response()->json($resp, 400, [] , JSON_UNESCAPED_SLASHES);
            }
                
        }
        // $provinsi = $this->get_province();
        // return view('customer.checkout.checkout-list', compact('provinsi'));
    }
}
