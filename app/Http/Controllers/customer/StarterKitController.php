<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\StarterkitDownload;

class StarterKitController extends Controller
{
    public function index()
    {
        $download = StarterkitDownload::get();
        return view('customer.starterkit-download.show', [
            'download' => $download
        ]);
    }
}
