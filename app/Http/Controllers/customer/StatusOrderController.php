<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Transaction;
use App\Model\ItemTransaction;
use App\Model\TransactionShipment;
use App\Mail\PaymentConfirmation;
use App\Mail\PaymentConfirmationAdmin;
use Illuminate\Support\Facades\Mail;
use App\Model\Payment;
use App\User;
use Auth;
use DB;

use App\Helpers\ImageUpload;
use App\Model\NotifInfo;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class StatusOrderController extends Controller
{
    public function show()
    {
        // $listTransaction = Transaction::where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->with('order_status')->get();
        $data = Transaction::orderBy('id', 'desc')->with('order_detail')->where('cs_id', auth()->user()->id)->get();
        return view('customer.status-order.index', [
            'data' => $data
        ]);
    }

    public function sendMail($orderId)
    {
        $getEmailAdmin = User::where('roles', 'admin')->first();

        if (!empty(Auth::user()->email)) {
            Mail::to(Auth::user()->email)->send(new PaymentConfirmation($orderId));
            Mail::to($getEmailAdmin->email)->send(new PaymentConfirmationAdmin($orderId));
            return "Email telah dikirim";
        } else {
            return "Email Gagal Terkirim";
        }
    }

    public function konfirmasiBayar($orderId)
    {
        $show = Transaction::where('orderId', $orderId)->first();
        return view('customer.status-order.konfirmasi-bayar', [
            'show' => $show
        ]);
    }

    public function statusOrderDetail($orderId)
    {
        $no = 1;
        $transaction = Transaction::where('orderId', $orderId)->with('get_user')->first();
        if ($transaction == null) {
            abort(404);
        }
        $orderDetail = ItemTransaction::where('orderId', $orderId)->with(['products', 'getpaketupgrade', 'getpaketreguler'])->get();
        $detailShippment = TransactionShipment::where('orderId', $orderId)->get();
        return view('customer.status-order.detail', [
            'transaction' => $transaction,
            'orderDetail' => $orderDetail,
            'shipment' => $detailShippment,
            'no' => $no,
        ]);
    }

    public function konfirmasiPesananSampai($orderId)
    {
        Transaction::where('orderId', $orderId)->update(['status' => 5]);
        return redirect()->route('statusOrder')->with('success', 'pesanan sudah diterima');
    }

    public function payment($orderId)
    {
        session()->forget('orderId');
        session()->forget('orderId_MsUpgrade');
        session()->forget('orderId_Konsumen');
        session()->forget('orderId_Event');
        $transaction = Transaction::where('orderId', $orderId)->first();
        return view('customer.status-order.konfirmasi-pembayaran', [
            'transaction' => $transaction
        ]);
    }

    public function konfirmasiPayment(Request $request, $orderId)
    {
        $check = Transaction::where('orderId', $orderId)->first();
        if (!$check) {
            return Redirect::back()->withErrors(['Transaksi Anda Tidak Ditemukan']);
        }

        $path = 'public/transaction-attachment/';
        $filename = null;

        if ($request->hasFile('files')) {
            $image      = $request->file('files');
            $filename   = time() . '.' . $image->getClientOriginalExtension();
            $img = Image::make($image);
            $img->stream(); // <-- Key point
            Storage::disk('local')->put($path . $filename, $img);

            // $exists = Storage::disk('local')->has($path. $filename);
            // if ($exists) {
            //     Storage::delete($path. $filename);
            // }
        }

        if (!empty($filename)) {
            // pushNotifKonfirmasiPembayaran
            NotifInfo::pushNotifKonfirmasiPembayaran($check->id, 'admin');
            $check->photo = $filename;
            $check->save();
            $this->sendMail($orderId);
            return redirect()->route('statusOrder')->with('success', 'Bukti pembayaran berhasil diupload');
        } else {
            // return 'blom upload';
            return redirect()->back()->with('error', 'Bukti pembayaran belum diupload');
        }
    }
}
