<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\NewsAndUpdate;

class NewsAndUpdateController extends Controller
{
     // news & update
     public function newsAndUpdate(Request $request)
     {
        //  $getNews = NewsAndUpdate::orderBy('id', 'DESC')->get();
        //  return view('customer.newsAndUpdate.index', [
        //      'getNews' => $getNews
        //  ]);

        $getNews = NewsAndUpdate::orderBy('id', 'DESC')->paginate(4);
        return view('customer.newsAndUpdate.index',compact('getNews'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
     }

     public function detail($id)
     {
         $detailGetNews = NewsAndUpdate::where('id', $id)->first();
         return view('customer.newsAndUpdate.detail', [
             'detailGetNews' => $detailGetNews
         ]);
     }


}
