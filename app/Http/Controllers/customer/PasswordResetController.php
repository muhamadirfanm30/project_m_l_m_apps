<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Mail;
use App\Mail\ResetPasswordEmail;
use Illuminate\Http\Request;
use App\Model\PasswordReset;
use App\Model\Email;
use Carbon\Carbon; 
use DB; 
use App\User;
use Illuminate\Support\Facades\Hash;

class PasswordResetController extends Controller
{
    public function show()
    {
        return view('customer.password-reset.show');
    }

    public function postEmail(Request $request)
    {
        // dd($request->email);
        // $request->validate([
        //     'email' => 'required|email|exists:users',
        // ]);

        $cekUser = User::where('email', $request->email)->count();
        
        if($cekUser == 1){
            $token = md5(64);
            $subject = Email::where('id', 10)->first();
            DB::table('password_resets')->insert(
                ['email' => $request->email, 'token' => $token, 'created_at' => Carbon::now()]
            );
            Mail::send('customer.password-reset.verify', ['token' => $token, 'subject' => $subject], function ($m) use ($request) {
    
                $m->to($request->email)->subject('Reset Password');
            });
            return redirect()->back()->with('success', 'Kami telah mengirimkan email tautan pengaturan ulang kata sandi Anda!');
        }else{
            return redirect()->back()->with('error', 'Email Tidak ditemukan!');
        }
    }

    public function getPassword($token) 
    { 
        return view('customer.password-reset.reset', ['token' => $token]);
    }

    public function updatePassword(Request $request)
    {
        $request->validate([
            'email' => 'required|email|exists:users',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required',
    
        ]);
    
        $updatePassword = DB::table('password_resets')
                            ->where(['email' => $request->email, 'token' => $request->token])
                            ->first();
    
        if(!$updatePassword){
            return back()->withInput()->with('error', 'Invalid token!');
        }
    
        $user = User::where('email', $request->email)
                    ->update(['password' => Hash::make($request->password)]);
    
        DB::table('password_resets')->where(['email'=> $request->email])->delete();
    
        return redirect('/')->with('message', 'Your password has been changed!');
  
    }
}

