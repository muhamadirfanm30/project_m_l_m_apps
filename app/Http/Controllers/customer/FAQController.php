<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\FaqKategori;
use App\Model\TermAndCondition;
use App\Model\AccordionTermCondition;

class FAQController extends Controller
{
    public function index()
    {
        $getMenuFaq = TermAndCondition::with('faqKategori')->get();
        $detailFaq = TermAndCondition::with('termDetail')->first();
        // return $detailFaq->termDetail;
        return view('customer.faq.index', [
            'getMenuFaq' => $getMenuFaq,
            'detailFaq' => $detailFaq,
        ]);
    }

    public function detail($id)
    {
        $getMenuFaq = TermAndCondition::with('faqKategori')->get();
        $detailFaq = TermAndCondition::with('termDetail')->where('id', $id)->first();
        return view('customer.faq.index', [
            'getMenuFaq' => $getMenuFaq,
            'detailFaq' => $detailFaq
        ]);
    }
}
