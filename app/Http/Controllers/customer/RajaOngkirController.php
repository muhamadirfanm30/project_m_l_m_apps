<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Model\admin\Province;
use App\Model\admin\City;

class RajaOngkirController extends Controller
{
    public function getProfince()
    {
        $client = new Client();
        try{

            $response = $client->get('https://api.rajaongkir.com/starter/province',
                array(
                    'headers' => array(
                        'key' => '60b137e92094b5e6fa05ddd10e2c1cbb',
                    )
                )
            );

        }catch(RequestException $e){
            var_dump($e->getResponse()->getBody()->getContents());
        }

        $json = $response->getBody()->getContents();
        $array_result = json_decode($json, true);
        for($i = 0; $i < count($array_result["rajaongkir"]["results"]); $i++){
            $province = New Province;
            $province->id = $array_result["rajaongkir"]["results"][$i]["province_id"];
            $province->name = $array_result["rajaongkir"]["results"][$i]["province"];
            $province->save();

        }
    }

    public function getCityByid($id)
    {
        $http = new \GuzzleHttp\Client();
			try {

				$res = $http->request('GET', 'https://api.rajaongkir.com/starter/city?key=60b137e92094b5e6fa05ddd10e2c1cbb&province='. $id,  [
					'http_errors' => false,
				 		'headers' => [
                            'Accept' => 'application/json',
                            'key' => '60b137e92094b5e6fa05ddd10e2c1cbb',
				 		
				 		]
                ]);
                
                $datas = array(
                    'status' => 1,
                    'msg' => 'success',
                    'data' => json_decode($res->getBody()),
                );

                return $datas;

                } catch (\GuzzleHttp\Exception\RequestException $e) {
                    return redirect()->back()->with('error', $e->getResponse());
                }	
    }

    public function getCosts(Request $request)
    {
        $http = new \GuzzleHttp\Client();
			try {
                // return $request->courier;
				$res = $http->request('POST', 'https://api.rajaongkir.com/starter/cost',  [
                    
                        'form_params' => [
                            'origin'        => $request->origin,
                            'destination'   => $request->destination,
                            'weight'        => $request->weight,
                            'courier'       => $request->courier
                        ],

				 		'headers' => [
                            'Accept'        => 'application/json',
                            'key'           => '60b137e92094b5e6fa05ddd10e2c1cbb',
                            'content-type'  => 'application/x-www-form-urlencoded',
				 		]
				]);
                $dataCost = array(
                    'status' => 1,
                    'msg' => 'success',
                    'data' => json_decode($res->getBody()),
                );
                return $dataCost;
                } catch (\GuzzleHttp\Exception\RequestException $e) {
                    return redirect()->back()->with('error', $e->getResponse());
                }	
    }
}
