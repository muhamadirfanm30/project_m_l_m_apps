<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Model\Email;
use App\Model\Transaction;
use App\Model\ItemTransaction;
use App\Helpers\Midtrans;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    
    public function testingABC(){
        echo Midtrans::generateOrderId();
    //     $transaction = Transaction::where('orderId','pewWJDNI8')->first();   
    //     $content_email = Email::where('id', 2)->first();
    //     return view('email.SuccessPayment_notification',compact(['content_email','transaction']));
    }
}
