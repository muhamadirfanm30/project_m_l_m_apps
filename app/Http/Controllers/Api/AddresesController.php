<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Country;
use App\Model\City;
use App\Model\Village;
use App\Model\Province;
use App\Model\Distric;
use App\Model\Zipcode;

class AddresesController extends Controller
{
    public function selectCountry(Request $request)
    {
        $response = Country::where('name', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();

            return response()->json([
                'data' => $response,
                'msg' => 'success',
                'status_code' => 200,
                'success' => true,
            ], 200);
    }

    public function findCountry(Request $request, $country_id = null)
    {
        if ($country_id) {
            return $this->successResponse(
                Country::with('provinces')->find($country_id)
            );
        }

        $response = Country::where('name', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();

            return response()->json([
                'data' => $response,
                'msg' => 'success',
                'status_code' => 200,
                'success' => true,
            ], 200);
    }

    public function findProvince(Request $request, $province_id = null)
    {
        if ($province_id) {
            return $this->successResponse(
                Province::with('cities')->find($province_id)
            );
        }

        $response = Province::with('country')
            ->where('name', 'like', '%' . $request->q . '%')
            ->when($request->country_id, function ($query, $country_id) {
                return $query->where('ms_country_id', $country_id);
            })
            ->limit(20)
            ->get();

            return response()->json([
                'data' => $response,
                'msg' => 'success',
                'status_code' => 200,
                'success' => true,
            ], 200);
    }

    public function findCity(Request $request, $city_id = null)
    {
        if ($city_id) {
            return $this->successResponse(
                City::with('province', 'districts')->find($city_id)
            );
        }

        $response = City::with('province')
            ->where('name', 'like', '%' . $request->q . '%')
            ->when($request->province_id, function ($query, $province_id) {
                return $query->where('ms_province_id', $province_id);
            })
            ->limit(20)
            ->get();

            return response()->json([
                'data' => $response,
                'msg' => 'success',
                'status_code' => 200,
                'success' => true,
            ], 200);
    }

    public function findDistrict(Request $request, $district_id = null)
    {
        if ($district_id) {
            return $this->successResponse(
                Distric::with('villages', 'city', 'zipcode')->find($district_id)
            );
        }

        $response = Distric::with('zipcode')
            ->where('name', 'like', '%' . $request->q . '%')
            ->when($request->city_id, function ($query, $city_id) {
                return $query->where('ms_city_id', $city_id);
            })
            ->limit(20)
            ->get();

            return response()->json([
                'data' => $response,
                'msg' => 'success',
                'status_code' => 200,
                'success' => true,
            ], 200);
    }

    public function findVillage(Request $request, $village_id = null)
    {
        if ($village_id) {
            return $this->successResponse(
                Village::with('district')->find($village_id)
            );
        }

        $response = Village::where('name', 'like', '%' . $request->q . '%')
            ->when($request->district_id, function ($query, $district_id) {
                return $query->where('ms_district_id', $district_id);
            })
            ->limit(20)
            ->get();

            return response()->json([
                'data' => $response,
                'msg' => 'success',
                'status_code' => 200,
                'success' => true,
            ], 200);
    }

    public function findZipcode(Request $request, $zipcode_id = null)
    {
        if ($zipcode_id) {
            return $this->successResponse(
             Zipcode::with('district')->find($zipcode_id)->groupBy('zip_no asc')
            );
        }
        $response = Zipcode::where('zip_no', 'like', '%' . $request->q . '%')
            ->when($request->district_id, function ($query, $district_id) {
                return $query->where('ms_district_id', $district_id);
            })
            ->groupBy('zip_no')
            ->limit(20)
            ->get();

            return response()->json([
                'data' => $response,
                'msg' => 'success',
                'status_code' => 200,
                'success' => true,
            ], 200);
    }
}
