<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Model\Kurir;

class Rajaongkir extends Controller
{
    public $client;
    public $keyApi = '17461d353db677af78769faf2be1cb82';

    public function __construct()
    {
        $client = new Client([
            'base_uri' => 'https://pro.rajaongkir.com',
        ]);
        $this->client = $client;
    }

    public function provinsi(Request $request)
    {
        return response()->json([
            'data' => $this->getProvinsiApi(),
            'msg' => 'success',
            'status_code' => 200,
            'success' => true,
        ], 200);
    }

    public function city(Request $request)
    {
        $parameter = [];
        if (!empty($request->province)) {
            $parameter['province'] = $request->province;
        }
        return response()->json([
            'data' => $this->getCityApi($parameter),
            'msg' => 'success',
            'status_code' => 200,
            'success' => true,
        ], 200);
    }

    public function subdistrict(Request $request)
    {
        $parameter = [];
        $parameter['city'] = $request->city;

        return response()->json([
            'data' => $this->getDistrikApi($parameter),
            'msg' => 'success',
            'status_code' => 200,
            'success' => true,
        ], 200);
    }

    public function courier(Request $request)
    {
        // $data = [
        //     [
        //         'code' => 'jne',
        //         'text' => 'jne',
        //     ],
        //     [
        //         'code' => 'pos',
        //         'text' => 'pos',
        //     ],
        //     [
        //         'code' => 'tiki',
        //         'text' => 'tiki',
        //     ],
        //     [
        //         'code' => 'rpx',
        //         'text' => 'rpx',
        //     ],
        //     [
        //         'code' => 'pandu',
        //         'text' => 'pandu',
        //     ],
        //     [
        //         'code' => 'wahana',
        //         'text' => 'wahana',
        //     ],
        //     [
        //         'code' => 'sicepat',
        //         'text' => 'sicepat',
        //     ],
        //     [
        //         'code' => 'pahala',
        //         'text' => 'pahala',
        //     ],
        //     [
        //         'code' => 'sap',
        //         'text' => 'sap',
        //     ],
        //     [
        //         'code' => 'jet',
        //         'text' => 'jet',
        //     ],
        //     [
        //         'code' => 'indah',
        //         'text' => 'indah',
        //     ],
        //     [
        //         'code' => 'dse',
        //         'text' => 'dse',
        //     ],
        //     [
        //         'code' => 'slis',
        //         'text' => 'slis',
        //     ],
        //     [
        //         'code' => 'first',
        //         'text' => 'first',
        //     ],
        //     [
        //         'code' => 'ncs',
        //         'text' => 'ncs',
        //     ],
        //     [
        //         'code' => 'star',
        //         'text' => 'star',
        //     ],
        //     [
        //         'code' => 'ninja',
        //         'text' => 'ninja',
        //     ],
        //     [
        //         'code' => 'lion',
        //         'text' => 'lion',
        //     ],
        //     [
        //         'code' => 'idl',
        //         'text' => 'idl',
        //     ],
        //     [
        //         'code' => 'rex',
        //         'text' => 'rex',
        //     ],
        //     [
        //         'code' => 'ide',
        //         'text' => 'ide',
        //     ],
        //     [
        //         'code' => 'sentral',
        //         'text' => 'sentral',
        //     ],
        // ];

        $data = Kurir::where('is_active', 1)->get();
        return response()->json([
            'data' => $data,
            'msg' => 'success',
            'status_code' => 200,
            'success' => true,
        ], 200);
    }

    public function cost(Request $request)
    {
        $parameter = [];
        $parameter['origin'] = $request->origin; // ID kota/kabupaten atau kecamatan asal
        $parameter['originType'] = $request->originType; // Tipe origin: 'city' atau 'subdistrict'.
        // $parameter['destination'] = $request->destination; // ID kota/kabupaten atau kecamatan tujuan
        // $parameter['destinationType '] = $request->destinationType; // Tipe destination: 'city' atau 'subdistrict'.
        $parameter['destination'] = 152; // ditembak dulu,
        $parameter['destinationType'] = 'city'; // ditembak dulu
        $parameter['weight'] = $request->weight; // Berat kiriman dalam gram
        $parameter['courier'] = $request->courier; // Kode kurir: jne, pos, tiki, rpx, pandu, wahana, sicepat, jnt, pahala, sap, jet, indah, dse, slis, first, ncs, star, ninja, lion, idl, rex, ide, sentral.

        return response()->json([
            'data' => $this->getCostApi($parameter),
            'msg' => 'success',
            'status_code' => 200,
            'success' => true,
        ], 200);
    }

    private function getProvinsiApi()
    {
        try {
            $response = $this->client->request('GET', '/api/province', [
                'query' => ['key' => $this->keyApi]
            ]);
            return json_decode($response->getBody()->getContents());
        } catch (\Exception  $e) {
            return $e->getMessage();
        }
    }

    private function getCityApi($queryParam = [])
    {
        $param = [];
        $param['key'] = $this->keyApi;
        foreach ($queryParam as $key => $value) {
            $param[$key] = $value;
        }

        try {
            $response = $this->client->request('GET', '/api/city', [
                'query' => $param
            ]);
            return json_decode($response->getBody()->getContents());
        } catch (\Exception  $e) {
            return $e->getMessage();
        }
    }

    private function getDistrikApi($queryParam = [])
    {
        $param = [];
        $param['key'] = $this->keyApi;
        foreach ($queryParam as $key => $value) {
            $param[$key] = $value;
        }

        try {
            $response = $this->client->request('GET', '/api/subdistrict', [
                'query' => $param
            ]);
            return json_decode($response->getBody()->getContents());
        } catch (\Exception  $e) {
            return $e->getMessage();
        }
    }

    private function getCostApi($queryParam = [])
    {
        $param = [];
        $param['key'] = $this->keyApi;
        foreach ($queryParam as $key => $value) {
            $param[$key] = $value;
        }

        try {
            $response = $this->client->request('POST', '/api/cost', [
                'form_params' => $param
            ]);
            return json_decode($response->getBody()->getContents());
        } catch (\Exception  $e) {
            return $e->getMessage();
        }
    }
}
