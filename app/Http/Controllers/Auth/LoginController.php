<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Request;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        return view('login');
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        $auth = auth()->attempt(['email' => $request->email, 'password' => $request->password]);
        if($auth){
            if(auth()->user()->status == 0){
                return $this->sendLoginResponse($request);
            }else{
                $request->session()->invalidate();
                $this->sendFailedLoginResponseNonActive($request);
            }
        }else{
            $this->sendFailedLoginResponse($request);
        }
    }

    // public function credentials(Request $request){
    //     $auth = auth()->attempt(['email' => $request->email, 'password' => $request->password]);
    //     if($auth){
    //         if(auth()->user()->status == 0){
    //             $this->sendLoginResponse($request);
    //         }else{
    //             $request->session()->invalidate();
    //             $this->sendFailedLoginResponseNonActive($request);
    //         }
    //     }else{
    //         $this->sendFailedLoginResponse($request);
    //     }
    // }

    protected function sendFailedLoginResponse(Request $request)
    {
        throw ValidationException::withMessages([
            $this->username() => [trans('auth.failed')],
        ]);
    }

    protected function sendFailedLoginResponseNonActive(Request $request)
    {
        throw ValidationException::withMessages([
            $this->username() => [trans('auth.account-nonactive')],
        ]);
    }

    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();
        $authRoles = strtolower(auth()->user()->roles);
        switch ($authRoles) {
            case 'admin':
                return redirect()->route('adm-dashboards');
                break;
            case 'customer':
                return redirect()->route('cs-dashboard');
                break;
            case 'sub admin':
                return redirect()->route('transaction.show');
                break;
            case 'admin produk':
                return redirect()->route('transaction.show');
                break;
            case 'admin member':
                return redirect()->route('admin.show');
                break;
        }
    }
}
