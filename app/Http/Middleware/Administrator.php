<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Administrator
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()!=null) {
            if (strtolower(Auth::user()->roles) == "admin") {
                return $next($request);
            }else{
                abort(403);
            }
        }else{
            return redirect(route('login'));
        }
    }
}
