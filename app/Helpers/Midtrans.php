<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use App\Model\ItemTransaction;
use App\Model\CsStockProduct;
use App\Model\Product;
use App\Model\Transaction;
use App\Model\GeneralSetting;
use App\Mail\successPaymentEmail;
use App\Mail\expiredPaymentEmail;
use App\Model\TransactionShipment;
use App\Model\NotifInfo;
use Carbon\Carbon;
use App\User;
use Auth;

class Midtrans
{
    // $url = 'https://api.sandbox.midtrans.com/'; 
    // $url = 'https://api.midtrans.com'; //PRODUCTION

    public static function Transaction($trxId,$totalHarga,$bank = 'bca'){
        $serverKey = env('MD_SERVER_KEY').':';
        $serverKey = base64_encode($serverKey);
        $client = new \GuzzleHttp\Client();
        $url = 'https://api.sandbox.midtrans.com/v2/charge';
        $body = [
            'payment_type'=>'bank_transfer',
            'transaction_details'=>[
                'gross_amount'=> $totalHarga,
                'order_id'=> $trxId
            ],
            'bank_transfer'=>[
                'bank'=>$bank
            ],
            'customer_details'=> [
                'first_name'=> auth()->user()->first_name,
                'last_name'=> auth()->user()->last_name,
                'email'=> auth()->user()->email,
                'phone'=> auth()->user()->phone
            ]
        ];
        try {
            $request = $client->post(
                $url,
                [
                    'headers' => [
                        'Content-Type'=>'application/json',
                        'Accept'=>'application/json',
                        'Authorization'=> 'Basic '.$serverKey,
                    ],
                    'body' => json_encode($body)
                ]
            );
            if (!empty($request->getBody())) {
                $resp = json_decode($request->getBody());
                $mdStatusCode = (int)$resp->status_code;
                if($mdStatusCode >= 200 && $mdStatusCode <=202){
                    $response = [
                        'status'=>true,
                        'status_code'=>$resp->status_code,
                        'message'=>$resp->status_message,
                        'detailVA'=>!empty($resp->va_numbers) ? $resp->va_numbers : '-'
                    ];
                }else{
                    $response = [
                        'status'=>false,
                        'status_code'=>$resp->status_code,
                        'message'=>$resp->status_message
                    ];
                }
            } else {
                $response['status']    = false;
                $response['message']   = 'Tidak ada response dari payment gateway';
            }
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            $response['status']    = false;
            if($e->hasResponse()){
                $response['message'] = $e->getResponse();
            }else{
                $response['message'] = 'Tidak dapat terhubung dengan payment gateway';
            }
            $response_code   = $e->getCode();
        }
        return $response;
    }

    public static function Cek($trxId){
        $durationExpired = GeneralSetting::where('name','setting_expired_payment')->first();
        $serverKey = env('MD_SERVER_KEY').':';
        $serverKey = base64_encode($serverKey);
        $client = new \GuzzleHttp\Client();
        $url = 'https://api.sandbox.midtrans.com/v2/'.$trxId.'/status';
        
        \Log::channel('midtrans')->info('Transaction ID : '.$trxId);
        try {
            $request = $client->get(
                $url,
                [
                    'headers' => [
                        'Content-Type'=>'application/json',
                        'Accept'=>'application/json',
                        'Authorization'=> 'Basic '.$serverKey,
                    ],
                ]
            );

            if (!empty($request->getBody())) {
                $resp = json_decode($request->getBody());
                $mdStatusCode = (int)$resp->status_code;
                $field['payment_type'] = $resp->payment_type;
                \Log::channel('midtrans')->info('Status Code : '.$mdStatusCode);
                \Log::channel('midtrans')->info('Payment TYPE : '.$resp->payment_type);
                
                if($mdStatusCode >= 200 && $mdStatusCode <=202){
                    \Log::channel('midtrans')->info('Status : '.$resp->transaction_status);
                    $field['status'] = 0;
                    $response = [
                        'status'=>true,
                        'status_code'=>$resp->status_code,
                        'message'=>$resp->status_message,
                        'transaction_status'=>$resp->transaction_status
                    ];

                    $data = Transaction::where('orderId',$trxId)->with('get_user')->first();
                    $getMyTeam = User::where('referal_code', $data->get_user->membership_id)->count();
                    $getItem = ItemTransaction::where('orderId',$trxId)->get();

                    if($resp->transaction_status != "pending" && $resp->transaction_status != "cancel" && $resp->transaction_status !="Denied"){
                        $field['status'] = 1;
                        $length = 7;
                        $ms_code = '0123456789';
                        if($data->type == 1){
                            $checkAvailableUser = User::where('email',$data->email)->exists();
                            if(!$checkAvailableUser){
                                $create = User::create([
                                    'first_name' =>$data->nama_lengkap,
                                    'phone' =>$data->nomor_ponsel,
                                    'whatsapp_no' =>$data->nomor_ponsel,
                                    'gender' =>$data->jenis_kelamin,
                                    'province_id' =>$data->province_id,
                                    'kota_id' =>$data->kota_id,
                                    'email' =>$data->email,
                                    'is_tmp_user' => 1,
                                    'is_approve_admin' => 1,
                                    'status' => 1,
                                    'membership_id' => 'R-'.substr(str_shuffle(str_repeat($ms_code, 7)), 0, $length),
                                    'membersip_status' => 'Reguler',
                                    'address' => $data->alamat,
                                    'referal_code' => $data->get_user->membership_id,
                                    'created_at'=>now(),
                                    'updated_at'=>now(),
                                    'is_admin_created' => 0
                                ]);

                                User::where('membership_id', $data->get_user->membership_id)->update([
                                    'my_member' => $getMyTeam + 1
                                ]);

                                NotifInfo::pushNotifNewTeamRequest($create->id, 'admin');
                            }
                        }
                        
                        if($data->own_transaction == 1){ //tidak diberikan kepada konsumen

                            foreach ($getItem as $r) {

                                $getMyStok = CsStockProduct::where('product_id',$r->product_id)->where('cs_id',$data->cs_id)->first();

                                if($getMyStok){
                                    $fieldMyStok = [
                                        'product_id'=>$r->product_id,
                                        'stok'=> (int)$getMyStok->stok + (int)$r->qty,
                                        'cs_id'=>$data->cs_id
                                    ];
                                    $getMyStok->update($fieldMyStok);
                                }else{
                                    $fieldMyStok = [
                                        'product_id'=>$r->product_id,
                                        'stok'=>$r->qty,
                                        'cs_id'=>$data->cs_id
                                    ];
                                    CsStockProduct::create($fieldMyStok);
                                }
                            }

                        }
                        if(!empty($data->get_user->email)){
                            Mail::to($data->get_user->email)->send(new successPaymentEmail($trxId));
                            $response = [
                                'status'=>true,
                                'status_code'=>$resp->status_code,
                                'message'=>$resp->status_message,
                                'transaction_status'=>$resp->transaction_status,
                                'email'=>'Sent Email SuccessPaymentEmail'
                            ];
                        }else{
                            $response = [
                                'status'=>true,
                                'status_code'=>$resp->status_code,
                                'message'=>$resp->status_message,
                                'transaction_status'=>$resp->transaction_status,
                                'email'=>'Failed Sent Email : SuccessPaymentEmail'
                            ];
                        }
                    }else if($resp->transaction_status == "pending" && $resp->expired_date < date('Y-m-d H:i:s')){
                        //ini yg expired
                        $field['status'] = 3; //expired
                        Midtrans::cancel($trxId);

                    }else if($resp->transaction_status == "cancel"){
                        //ini yg ga diTF"in
                        $field['status'] = 2; //cancel
                        Midtrans::cancel($trxId);

                    }

                    //ini function balikin stok dri status 2 & 3
                    if($field['status'] == 3 || $field['status'] == 2){
                        if(($data->own_transaction == 1 && $data->own_product == 0) || ($data->own_transaction == 0 && $data->own_product == 0) ){ //tidak diberikan kepada konsumen && ditimbun

                            foreach ($getItem as $r) {

                                $getMyStok = Product::where('id',$r->product_id)->first();

                                if($getMyStok){
                                    $fieldMyStok = [
                                        'stok'=> (int)$getMyStok->stok + (int)$r->qty,
                                    ];
                                    $getMyStok->update($fieldMyStok);
                                }
                            }

                        }else if($data->own_transaction == 0 && $data->own_product == 1){
                            foreach ($getItem as $r) {

                                $getMyStok = CsStockProduct::where('product_id',$r->product_id)->where('cs_id',$data->cs_id)->first();

                                if($getMyStok){
                                    $fieldMyStok = [
                                        'product_id'=>$r->product_id,
                                        'stok'=> (int)$getMyStok->stok + (int)$r->qty,
                                        'cs_id'=>$data->cs_id
                                    ];
                                    $getMyStok->update($fieldMyStok);
                                }else{
                                    $fieldMyStok = [
                                        'product_id'=>$r->product_id,
                                        'stok'=>$r->qty,
                                        'cs_id'=>$data->cs_id
                                    ];
                                    CsStockProduct::create($fieldMyStok);
                                }
                            }
                        }

                        if(!empty($data->get_user->email)){
                            Mail::to($data->get_user->email)->send(new expiredPaymentEmail($trxId));
                            $response = [
                                'status'=>true,
                                'status_code'=>$resp->status_code,
                                'message'=>$resp->status_message,
                                'transaction_status'=>$resp->transaction_status,
                                'email'=>'Sent Email : expiredPaymentEmail'
                            ];
                        }else{
                            $response = [
                                'status'=>true,
                                'status_code'=>$resp->status_code,
                                'message'=>$resp->status_message,
                                'transaction_status'=>$resp->transaction_status,
                                'email'=>'Failed Sent Email : expiredPaymentEmail'
                            ];
                        }
                    }

                    $data->update($field);
                }else{

                    $response = [
                        'status'=>false,
                        'status_code'=>$resp->status_code,
                        'message'=>$resp->status_message
                    ];
                    \Log::channel('midtrans')->alert('Status : '.$resp->status_message);
                }
            } else {
                $response['status']    = false;
                $response['message']   = 'Tidak ada response dari payment gateway';
                \Log::channel('midtrans')->alert('Status : Tidak ada response dari payment gateway');
            }
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            $response['status']    = false;
            if($e->hasResponse()){
                $response['message'] = $e->getResponse();
            }else{
                $response['message'] = 'Tidak dapat terhubung dengan payment gateway';
            }
            \Log::channel('midtrans')->info('Status : false');
            \Log::channel('midtrans')->info('Status Code : '. $response['message']);
            $response_code   = $e->getCode();
        }
        \Log::channel('midtrans')->info('========== END '.$trxId.' ==========');
        return $response;
    }
    
    public static function cancel($trxId){
        $serverKey = env('MD_SERVER_KEY').':';
        $serverKey = base64_encode($serverKey);
        $client = new \GuzzleHttp\Client();
        $url = 'https://api.sandbox.midtrans.com/v2/'.$trxId.'/cancel';
        try {
            $request = $client->get(
                $url,
                [
                    'headers' => [
                        'Content-Type'=>'application/json',
                        'Accept'=>'application/json',
                        'Authorization'=> 'Basic '.$serverKey,
                    ],
                ]
            );

            if (!empty($request->getBody())) {
                $resp = json_decode($request->getBody());
                $mdStatusCode = (int)$resp->status_code;
                if($mdStatusCode >= 200 && $mdStatusCode <=202){
                    $response = [
                        'status'=>true,
                        'status_code'=>$resp->status_code,
                        'message'=>$resp->status_message,
                        'transaction_status'=>$resp->transaction_status
                    ];
                }else{
                    $response = [
                        'status'=>false,
                        'status_code'=>$resp->status_code,
                        'message'=>$resp->status_message
                    ];
                }
            } else {
                $response['status']    = false;
                $response['message']   = 'Tidak ada response dari payment gateway';
            }
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            $response['status']    = false;
            if($e->hasResponse()){
                $response['message'] = $e->getResponse();
            }else{
                $response['message'] = 'Tidak dapat terhubung dengan payment gateway';
            }
            $response_code   = $e->getCode();
        }
        return $response;
    }

    public static function TokenSnap($trxId,$totalHarga){
        $serverKey = env('MD_SERVER_KEY').':';
        $serverKey = base64_encode($serverKey);
        $client = new \GuzzleHttp\Client();
        $url = 'https://app.sandbox.midtrans.com/snap/v1/transactions';
        $body = [
            'customer_details'=> [
                'first_name'=> auth()->user()->first_name,
                'last_name'=> auth()->user()->last_name,
                'email'=> auth()->user()->email,
                'phone'=> auth()->user()->phone
            ],
            'transaction_details'=> [
                'order_id'=>$trxId,
                'gross_amount'=>$totalHarga
            ]
        ];
        try {
            $request = $client->post(
                $url,
                [
                    'headers' => [
                        'Content-Type'=>'application/json',
                        'Accept'=>'application/json',
                        'Authorization'=> 'Basic '.$serverKey,
                    ],
                    'body' => json_encode($body)
                ]
            );
            if (!empty($request->getBody())) {
                $resp = json_decode($request->getBody());
                if(array_key_exists('token',(array)$resp)){
                    $response = [
                        'status'=>true,
                        'status_code'=>200,
                        'token'=>$resp->token,
                        'redirectUrl'=>$resp->redirect_url,
                        'trxId'=>$trxId
                    ];
                }else{
                    $response = [
                        'status'=>false,
                        'status_code'=>401,
                        'message'=>$resp->error_messages[0],
                        'orderId'=>$trxId
                    ];
                }
            } else {
                $response['status']    = false;
                $response['message']   = 'Tidak ada response dari payment gateway';
            }
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            $response['status']    = false;
            if($e->hasResponse()){
                $var = json_decode($e->getResponse()->getBody(true)->getContents());
                $response['message'] = $var->error_messages[0];
            }else{
                $response['message'] = 'Tidak dapat terhubung dengan payment gateway';
            }
            $response['orderId'] = $trxId;
            $response_code   = $e->getCode();
        }

        if($response['status'] == false){
            Transaction::where('orderId',$trxId)->delete();
            ItemTransaction::where('orderId',$trxId)->delete();
            TransactionShipment::where('orderId',$trxId)->delete();
        }
        return $response;
    }

    public static function generateOrderId($lastInMidtrans = false){
        $q          = Transaction::count();
        $prefix     = 'O'; //prefix = O-16032020-00001
        $separator  = '-';
        $date       = date('dmy');
        $number     = 1; //format
        $new_number = sprintf("%04s", $number);
        $code       = $date . ($separator) .  ($new_number);
        $order_code   = $code;
        if ($q > 0) {
            $last     = Transaction::orderBy('id', 'desc')->value('orderId');
            $last     = $lastInMidtrans ? explode("-", $lastInMidtrans) : explode("-", $last);
            $order_code = $date . ($separator) . (sprintf("%04s", $last[count($last) - 1] + 1));
        }
        return 'ORD-'.$order_code;
    }
}