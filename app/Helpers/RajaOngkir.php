<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Storage;
use App\Model\Transaction;
use App\Model\ItemTransaction;
use App\Model\CsStockProduct;
use Carbon\Carbon;
class RajaOngkir
{
    public static function getProvinsi(){
        $serverKey = env('RJO_KEY');
        $client = new \GuzzleHttp\Client();
        $url = 'https://api.rajaongkir.com/starter/province';
        
        try {
            $request = $client->get(
                $url,
                [
                    'headers' => [
                        'Content-Type'=>'application/json',
                        'Accept'=>'application/json',
                        'Key'=> $serverKey,
                    ],
                ]
            );

            if (!empty($request->getBody())) {
                $resp = json_decode($request->getBody());
                $response = [
                    'status'=>true,
                    'data'=> $resp->rajaongkir->results
                ];
            } else {
                $response['status']    = false;
                $response['message']   = 'Tidak ada response dari payment gateway';
            }
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            $response['status']    = false;
            if($e->hasResponse()){
                $response['message'] = $e->getResponse();
            }else{
                $response['message'] = 'Tidak dapat terhubung dengan payment gateway';
            }
            $response_code   = $e->getCode();
        }
        return $response;
    }
}
    