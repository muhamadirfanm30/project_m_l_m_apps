<?php

namespace App;

use App\Model\NotifInfo;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use App\Model\PositionUpdateModel;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        // 'name', 'email', 'password', 'membersip_status', 'member_id', 'whatsapp_no', 'status'
        'id',
        'roles',
        'first_name',
        'last_name',
        'no_ktp',
        'gender',
        'phone',
        'province_id',
        'kota_id',
        'kode_pos',
        'membership',
        'membership_id',
        'myreferal',
        'referal_code',
        'status',
        'email',
        'password',
        'membersip_status',
        'is_upgrade_ms',
        'rekening_bank',
        'nama_pemilik_rekening',
        'nomor_rekening',
        'address',
        'is_approve_admin',
        'is_tmp_user',
        'is_admin_created',
        'is_random_position',
        'avatar_user',
        'my_member',
        'whatsapp_no',
        'foto_ktp',
        'sponsor_upline_id',
        'sponsor_upline_name'
    ];

    protected $appends = [
        'avatar',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function order(){
        return $this->hasMany('App\Model\Transaction','cs_id','id');
    }

    public function getAvatarAttribute()
    {
        if ($this->avatar_user == null) {
            return storage_path('app/public/avatar_users/default_avatar.jpg');
        } else {
            return storage_path('app/public/avatar_users/' . $this->avatar_user);
        }
    }

    public function get_posisi()
    {
        return $this->hasOne('App\Model\PositionUpdateModel', 'calon_ms_id', 'id');
    }

    public function notif_no_read_new_member_request(){
        return $this->hasOne(NotifInfo::class, 'model_id')->whereIn('modul_name', ['new-member-request'])->where('role_name', 'admin')->whereNull('read_at');
    }

    public function notif_no_read_my_team_upgrade_ms(){
        return $this->hasOne(NotifInfo::class, 'model_id')->whereIn('modul_name', ['my-team-upgrade-ms'])->where('role_name', 'admin')->whereNull('read_at');
    }

    public function notif_no_read_leader_upgrade_ms(){
        return $this->hasOne(NotifInfo::class, 'model_id')->whereIn('modul_name', ['leader-upgrade-ms'])->where('role_name', 'admin')->whereNull('read_at');
    }

    public static function getImagePathUpload()
    {
        return 'public/foto_ktp';
    }
}
