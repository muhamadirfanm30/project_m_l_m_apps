<?php

namespace App\Notifications\Order;

use App\Model\Master\Order;
use Illuminate\Bus\Queueable;
use App\Mail\CustomerRequestJobEmail;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class OrderNotification extends Notification
{
    use Queueable;

    public $order;
    public $opsi;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Order $order, $opsi = [])
    {
        $this->order = $order;
        $this->opsi = $opsi;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */

    public function toMail($notifiable)
    {
        return true;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $order = $this->order;
        $opsi = $this->opsi;
        
        return $opsi;
    }
}
