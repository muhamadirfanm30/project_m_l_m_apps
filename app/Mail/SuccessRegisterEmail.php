<?php
 
namespace App\Mail;
 
use Auth;
use App\Model\Email;
use App\Model\EmailLogo;
use App\Model\GeneralSetting;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class successRegisterEmail extends Mailable
{
    use Queueable, SerializesModels;
 
 
    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $id, $password;
    public function __construct($id,$password)
    {
        $this->id =  User::where('id',$id)->first();
        $this->password = $password;
    }
 
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(Request $request)
    {
        $subject = Email::where('id', 6)->first();
        return $this/*->from('MlmSystem@mail.com')*/
                    ->subject($subject->subject)
                    ->view('email.SuccessRegister_notification')
                    ->with(
                        [
                            'user'=>$this->id,
                            'content_email' => Email::where('id', 6)->first(),
                            'facebook' => GeneralSetting::where('id', 5)->first(),
                            'twiter' => GeneralSetting::where('id', 6)->first(),
                            'instagram' => GeneralSetting::where('id', 7)->first(),
                            'youtube' => GeneralSetting::where('id', 8)->first(),
                            'password'=>$this->password,
                            'logo' => EmailLogo::first(),
                        ]);
    }
}