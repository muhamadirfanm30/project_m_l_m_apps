<?php
 
namespace App\Mail;
 
use Auth;
use App\Model\Email;
use App\Model\EmailLogo;
use App\Model\GeneralSetting;
use App\Model\Transaction;
use App\Model\ItemTransaction;
use Illuminate\Http\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
 
class PaketUpgradeMsEmail extends Mailable
{
    use Queueable, SerializesModels;
 
 
    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $trxId,$detailItem;
    public function __construct($trxId)
    {
        $this->trxId =  Transaction::where('orderId',$trxId)->where('kategori', 'Akumulasi Paket MS Upgrade')->first();   
        $this->detailItem =  ItemTransaction::where('orderId',$trxId)->where('is_produk', 'get_paket')->get();   
    }
 
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(Request $request)
    {
        $subject = Email::where('id', 7)->first();
        return $this/*->from('MlmSystem@mail.com')*/
                    ->subject($subject->subject)
                    ->view('email.upgrade_ms_notification')
                    ->with(
                        [
                            'content_email' => Email::where('id', 7)->first(),
                            'detailItem'=>$this->detailItem,
                            'transaction'=>$this->trxId,
                            'facebook' => GeneralSetting::where('id', 5)->first(),
                            'twiter' => GeneralSetting::where('id', 6)->first(),
                            'instagram' => GeneralSetting::where('id', 7)->first(),
                            'youtube' => GeneralSetting::where('id', 8)->first(),
                            'logo' => EmailLogo::first(),
                        ]);
    }
}