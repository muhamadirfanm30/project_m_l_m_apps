<?php
 
namespace App\Mail;
 
use Auth;
use App\Model\Email;
use App\Model\EmailLogo;
use App\Model\GeneralSetting;
use App\Model\Transaction;
use Illuminate\Http\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
 
class successPaymentEmail extends Mailable
{
    use Queueable, SerializesModels;
 
 
    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $trxId;
    public function __construct($trxId)
    {
        $this->trxId =  Transaction::where('orderId',$trxId)->first();   
    }
 
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(Request $request)
    {
        $subject = Email::where('id', 2)->first();
       return $this/*->from('MlmSystem@mail.com')*/
                    ->subject($subject->subject)
                    ->view('email.SuccessPayment_notification')
                    ->with(
                        [
                            'transaction'=>$this->trxId,
                            'content_email' => Email::where('id', 2)->first(),
                            'facebook' => GeneralSetting::where('id', 5)->first(),
                            'twiter' => GeneralSetting::where('id', 6)->first(),
                            'instagram' => GeneralSetting::where('id', 7)->first(),
                            'youtube' => GeneralSetting::where('id', 8)->first(),
                            'logo' => EmailLogo::first(),
                        ]);
    }
}