<?php
 
namespace App\Mail;
 
use Auth;
use App\User;
use App\Model\Email;
use App\Model\EmailLogo;
use App\Model\GeneralSetting;
use Illuminate\Http\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
 
class ResetPasswordEmail extends Mailable
{
    use Queueable, SerializesModels;
 
 
    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $user,$request,$resetPass;
    public function __construct(User $user, Request $request, $resetPass)
    {
        $this->user = $user;
        $this->request = $request;
        $this->resetPass = $resetPass;
    }
 
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(Request $request)
    {
        $subject = Email::where('id', 1)->first();
       return $this/*->from('MlmSystem@mail.com')*/
                    ->subject($subject->subject)
                    ->view('email.resetPassword_notification')
                    ->with(
                        [
                            'content_email' => Email::where('id', 1)->first(),
                            'facebook' => GeneralSetting::where('id', 5)->first(),
                            'twiter' => GeneralSetting::where('id', 6)->first(),
                            'instagram' => GeneralSetting::where('id', 7)->first(),
                            'youtube' => GeneralSetting::where('id', 8)->first(),
                            'user'=>$this->user,
                            'request'=>$this->request,
                            'resetPass'=>$this->resetPass,
                            'logo' => EmailLogo::first(),
                        ]);
    }
}