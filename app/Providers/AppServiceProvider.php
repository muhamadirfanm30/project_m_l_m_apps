<?php

namespace App\Providers;

use App\Model\NotifInfo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //compose all the views....
        view()->composer('*', function ($view) {

            $count_data_transaksi_from_admin_not_read = NotifInfo::whereIn('modul_name', ['data-transactions', 'paket-upgrade-member', 'input-reguler-member', 'stok-online-produk'])
                ->whereNull('read_at')
                ->where('role_name', 'admin')
                ->count();

            $count_konfirmasi_pembayaran_from_admin_not_read = NotifInfo::where('modul_name', 'konfirmasi-pembayaran')
                ->whereNull('read_at')
                ->where('role_name', 'admin')
                ->count();

            $count_update_resi_pengiriman_from_admin_not_read = NotifInfo::where('modul_name', 'menunggu-resi-pengiriman')
                ->whereNull('read_at')
                ->where('role_name', 'admin')
                ->count();

            $count_new_member_request_from_admin_not_read = NotifInfo::where('modul_name', 'new-member-request')
                ->whereNull('read_at')
                ->where('role_name', 'admin')
                ->count();

            $count_myteam_upgrade_ms_from_admin_not_read = NotifInfo::where('modul_name', 'my-team-upgrade-ms')
                ->whereNull('read_at')
                ->where('role_name', 'admin')
                ->count();

            $count_leader_upgrade_ms_from_admin_not_read = NotifInfo::where('modul_name', 'leader-upgrade-ms')
                ->whereNull('read_at')
                ->where('role_name', 'admin')
                ->count();

            $status_text_info = $count_konfirmasi_pembayaran_from_admin_not_read + $count_data_transaksi_from_admin_not_read > 0 ? "News" : '';
            $status_text_info_user_menu = ($count_new_member_request_from_admin_not_read + $count_myteam_upgrade_ms_from_admin_not_read + $count_leader_upgrade_ms_from_admin_not_read) > 0 ? "News" : '';

            $view->with('count_data_transaksi_from_admin_not_read', $count_data_transaksi_from_admin_not_read);
            $view->with('count_update_resi_pengiriman_from_admin_not_read', $count_update_resi_pengiriman_from_admin_not_read);
            $view->with('count_konfirmasi_pembayaran_from_admin_not_read', $count_konfirmasi_pembayaran_from_admin_not_read);
            $view->with('count_new_member_request_from_admin_not_read', $count_new_member_request_from_admin_not_read);
            $view->with('count_myteam_upgrade_ms_from_admin_not_read', $count_myteam_upgrade_ms_from_admin_not_read);
            $view->with('count_leader_upgrade_ms_from_admin_not_read', $count_leader_upgrade_ms_from_admin_not_read);
            $view->with('status_text_info', $status_text_info);
            $view->with('status_text_info_user_menu', $status_text_info_user_menu);
        });
    }
}
