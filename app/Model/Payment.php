<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $guarded = [];
    protected $table = 'payments';

    public static function getImagePathUpload()
    {
        return 'public/bankIcon';
    }
}
