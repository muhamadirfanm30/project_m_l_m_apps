<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OrdersStatus extends Model
{
    protected $guarded = [];
    protected $table = 'status';
}
