<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EmailLogo extends Model
{
    protected $guarded = [];
    protected $table = 'emails_logo';

    public static function getImagePathUpload()
    {
        return 'public/email-logo';
    }
}
