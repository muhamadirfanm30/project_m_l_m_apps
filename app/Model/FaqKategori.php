<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FaqKategori extends Model
{
    protected $guarded = [];
    protected $table = 'faq_kategoris';
}
