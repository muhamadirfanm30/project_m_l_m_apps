<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class KontenDashboard extends Model
{
    protected $guarded = [];
    protected $table = 'konten_dashboards';
}
