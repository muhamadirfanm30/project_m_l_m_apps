<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PaketUpgradeMS extends Model
{
    protected $guarded = [];
    protected $table = 'paket_upgrade_m_s';

    public static function getImagePathUpload()
    {
        return 'public/paketUpdateMS';
    }
}
