<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\ItemTransaction;
use App\Model\CsStockProduct;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    
    protected $guarded = [];
    protected $table = 'products';

    public static function getImagePathUpload()
    {
        return 'public/productImages';
    }

    public function get_item_transaksi()
    {
        return $this->hasMany(TransactionItem::class, 'product_id', 'id');
    }

    public function countSell($id){
        return ItemTransaction::where('product_id',$id)->whereMonth('created_at',date('m'))->whereYear('created_at',date('Y'))->count();
    }

    public function stokCS($id){
        $data = CsStockProduct::where('product_id',$id)->where('cs_id',auth()->user()->id);
        if($data->exists()){
            return $data->first()->stok;
        }
        return 0;
        // return $this->belongsTo('App\Model\CsStockProduct','id','product_id');
    }
}
