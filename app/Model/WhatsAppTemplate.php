<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class WhatsAppTemplate extends Model
{
    protected $guarded = [];
    protected $table = 'whats_app_templates';
}
