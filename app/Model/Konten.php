<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Konten extends Model
{
    protected $table = 'konten';
    protected $fillable = [
        'free_text',
        'url_download',
        'foto',
        'kategori_id',
        'sub_kategori_id',
        'type',//URL VIDEO, input file, accordion
        'judul',
        'source',
        'deskripsi',
    ];

    public function getKategori(){
        return $this->belongsTo('App\Model\Kategori','kategori_id','id')->withTrashed();
    }

    public function getSubKategori(){
        return $this->belongsTo('App\Model\SubKategori','sub_kategori_id','id')->withTrashed();
    }

}
