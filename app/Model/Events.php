<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Events extends Model
{
    protected $guarded = [];
    protected $table = 'events';

    public static function getImagePathUpload()
    {
        return 'public/eventsImages';
    }
}
