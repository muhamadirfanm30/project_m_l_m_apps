<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Kategori extends Model
{
    use SoftDeletes;

    protected $table = 'kategori';
    protected $fillable = [
        'judul',
        'foto',
        'deskripsi'
    ];

    protected $dates = ['deleted_at'];

    public static function getImagePathUpload()
    {
        return 'public/kategori';
    }
}
