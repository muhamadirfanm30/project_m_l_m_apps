<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BonusMS extends Model
{
    protected $table = 'bomus_m_s';

    protected $guarded = [];
}
