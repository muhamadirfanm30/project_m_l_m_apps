<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $table = 'ms_provinces';

    protected $fillable = [
        'name', 'iso_code', 'ms_country_id', 'meta'
    ];

    // protected $casts = [
    //     'meta' => 'array',
    // ];

    public function country()
    {
        return $this->belongsTo(Country::class, 'ms_country_id');
    }

    public function city()
    {
        return $this->hasMany(City::class);
    }

    public function districts()
    {
        return $this->hasManyThrough(District::class, City::class);
    }
}
