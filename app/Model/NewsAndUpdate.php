<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class NewsAndUpdate extends Model
{
    protected $guarded = [];
    protected $table = 'news_and_update';
}
