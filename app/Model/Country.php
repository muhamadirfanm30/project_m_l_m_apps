<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'ms_countries';
    protected $fillable = [
        'name', 'code'
    ];

    public function provinces()
    {
        return $this->hasMany(Province::class);
    }
}
