<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Distric extends Model
{
    protected $table = 'ms_districts';

    protected $guarded = [];

    // protected $casts = [
    //     'meta' => 'array',
    // ];

    public function city()
    {
        return $this->belongsTo(City::class, 'ms_city_id');
    }

    public function villages()
    {
        return $this->hasMany(Village::class);
    }

    public function zipcode()
    {
        return $this->hasOne(Zipcode::class);
    }
}
