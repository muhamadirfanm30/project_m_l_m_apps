<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CsStockProduct extends Model
{
    protected $table = "cs_stock_products";
    protected $fillable = [
        'product_id',
        'cs_id',
        'stok'
    ];

    public function product(){
        return $this->belongsTo('App\Model\Product','product_id','id')->withTrashed();
    }
}
