<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TransactionItem extends Model
{
    protected $table = 'transaction_item';
    protected $fillable = [
        'orderId',
        'product_id',
        'qty',
        'harga',
        'user_id',
    ];

    public function get_produk()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function get_data_order()
    {
        return $this->hasMany(TRansaction::class, 'orderId', 'orderId');
    }
}
