<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class KontenHarian extends Model
{
    protected $table = 'konten_harian';
    protected $fillable = [
        'konten_id',
        'judul',
        'foto'
    ];
}
