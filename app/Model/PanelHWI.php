<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PanelHWI extends Model
{
    protected $guarded = [];
    protected $table = 'panel_h_w_i_s';
}
