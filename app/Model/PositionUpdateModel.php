<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PositionUpdateModel extends Model
{
    protected $guarded = [];
    protected $table = 'position_update_models';

    public function get_posisi()
    {
        return $this->hasOne('App\user', 'id', 'user_sponsor_id');
    }
}
