<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class KontenCopyWriting extends Model
{
    protected $table = 'konten_copywriting';
    protected $fillable = [
        'konten_id',
        'judul',
        'keterangan',
    ];
}
