<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PaketReguler extends Model
{
    protected $guarded = [];
    protected $table = 'paket_regulers';

    public static function getImagePathUpload()
    {
        return 'public/paketRegulers';
    }
}
