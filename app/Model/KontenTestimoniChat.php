<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class KontenTestimoniChat extends Model
{
    protected $table = 'konten_testimoni_chat';
    protected $fillable = [
        'konten_id',
        'nama',
        'foto'
    ];
}
