<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class StarterkitDownload extends Model
{
    protected $guarded = [];
    protected $table = 'starterkit_downloads';
}
