<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'ms_cities';

    protected $fillable = [
        'name', 'ms_province_id', 'meta'
    ];

    // protected $casts = [
    //     'meta' => 'array',
    // ];

    public function province()
    {
        return $this->belongsTo(Province::class, 'ms_province_id');
    }

    public function districts()
    {
        return $this->hasMany(District::class);
    }

    public function villages()
    {
        return $this->hasManyThrough(Village::class, District::class);
    }
}
