<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class KontenVideo extends Model
{
    /**
     * Katalog Module Id
     * 1 : Konten
     * 2 : Testimoni
     */
    protected $table = 'konten_video';
    protected $fillable = [
        'konten_id',
        'module_id',
        'judul',
        'url',
    ];
    
}
