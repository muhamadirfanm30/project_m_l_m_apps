<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SlideShow extends Model
{
    protected $guarded = [];
    protected $table = 'slide_shows';

    public static function getImagePathUpload()
    {
        return 'public/slide-show';
    }

}
