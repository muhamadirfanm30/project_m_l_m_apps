<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Transaction extends Model
{
    protected $table = 'transaction';
    protected $fillable = [
        'expired_date',
        'own_transaction',
        'orderId',
        'nama_lengkap',
        'nomor_ponsel',
        'jenis_kelamin',
        'nama_pengirim',
        'alamat',
        'kurir',
        'totalHarga',
        'totalOngkir',
        'totalKeseluruhan',
        'payment_method',
        'va',
        'status',
        'cs_id',
        'photo',
        'kategori',
        'email',
        'own_product',//  0 : Table Produck, 1 : Table Cs Produk
        'type',//  0 : non paket, 1 : paket
        'resi_pengiriman',
        'is_cancel',
        'province_id',
        'kota_id',
        'kode_pos',
        'district_id',
        'total_berat',
        'payment_type',
        'is_input_new_meber'
    ];

    public function get_user()
    {
        return $this->belongsTo(User::class, 'cs_id');
    }

    public function order_detail()
    {
        return $this->hasMany(ItemTransaction::class, 'orderId', 'orderId');
    }

    public function getBank(){
        return $this->hasOne(Payment::class,'nama_bank','payment_method');
    }

    public function getShipment(){
        return $this->hasMany(TransactionShipment::class, 'orderId', 'orderId');
    }

    public function notif_no_read_data_transaksi(){
        return $this->hasOne(NotifInfo::class, 'model_id')->whereIn('modul_name', ['data-transactions','paket-upgrade-member','input-reguler-member','konfirmasi-pembayaran', 'stok-online-produk'])->where('role_name', 'admin')->whereNull('read_at');
    }

    public function notif_no_read_data_konfirmasi_payment(){
        return $this->hasOne(NotifInfo::class, 'model_id')->whereIn('modul_name', ['konfirmasi-pembayaran', 'data-transactions'])->where('role_name', 'admin')->whereNull('read_at');
    }

    public function notif_no_read_data_update_resi(){
        return $this->hasOne(NotifInfo::class, 'model_id')->where('modul_name', 'menunggu-resi-pengiriman')->where('role_name', 'admin')->whereNull('read_at');
    }

    public function getImagePathUpload()
    {
        return 'public/transaction-attachment';
    }
}
