<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class KategoriProduk extends Model
{
    protected $guarded = [];
    protected $table = 'kategori_produks';
}
