<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ZipCode extends Model
{
    protected $table = 'ms_zipcodes';

    protected $fillable = [
        'zip_no', 'ms_district_id'
    ];

    public function district()
    {
        return $this->belongsTo(District::class, 'ms_district_id');
    }

    public function address() {
        return $this->hasMany(Address::class);
    }
}
