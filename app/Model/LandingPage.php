<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LandingPage extends Model
{
    protected $guarded = [];
    protected $table = 'landing_pages';

    public static function getImagePathUpload()
    {
        return 'public/landing-page-images';
    }
}
