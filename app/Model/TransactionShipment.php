<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TransactionShipment extends Model
{
    public $timestamps = false;
    protected $table = 'transaction_shipment';
    protected $fillable = [
        'orderId',
        'nama_lengkap',
        'nomor_ponsel',
        'jenis_kelamin',
        'nama_pengirim',
        'alamat',
        'kurir',
        'totalOngkir',
        'productId',
        'no_resi',
        'kode_pos',
        'province_id',
        'kota_id',
        'district_id',
        'total_berat',
    ];

    public function getProduk()
    {
        return $this->hasOne(Product::class,'id','productId');
    }
}
