<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AccordionTermCondition extends Model
{
    protected $table = 'accordion_term_conditions';

    protected $guarded = [];
}
