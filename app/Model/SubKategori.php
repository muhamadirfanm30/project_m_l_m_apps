<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubKategori extends Model
{
    use SoftDeletes;
    protected $table = 'sub_kategori';
    protected $fillable = ['nama'];

    protected $dates = ['deleted_at'];
    
    public function getKonten($subKategori,$kategori){
        return Konten::where('kategori_id',$kategori)->where('sub_kategori_id',$subKategori)->get();
    }

}
