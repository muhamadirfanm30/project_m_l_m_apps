<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Village extends Model
{
    protected $table = 'ms_villages';

    protected $fillable = [
        'name', 'meta', 'ms_district_id'
    ];

    // protected $casts = [
    //     'meta' => 'array',
    // ];

    public function district()
    {
        return $this->belongsTo(District::class, 'ms_district_id');
    }
}
