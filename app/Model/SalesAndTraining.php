<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SalesAndTraining extends Model
{
    protected $guarded = [];
    protected $table = 'sales_and_training';

    public static function getImagePathUpload()
    {
        return 'public/salesTrainingImages';
    }
}
