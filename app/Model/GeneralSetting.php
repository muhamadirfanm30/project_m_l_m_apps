<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class GeneralSetting extends Model
{
    protected $guarded = [];
    protected $table = 'general_settings';
}
