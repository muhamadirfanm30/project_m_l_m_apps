<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class KontenPertanyaan extends Model
{
    protected $table = 'konten_pertanyaan';
    protected $fillable = [
        'konten_id',
        'pertanyaan',
        'jawaban',
    ];
}
