<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Kurir extends Model
{
    protected $guarded = [];
    protected $table = 'master_kurir';
}
