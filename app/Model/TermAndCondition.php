<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\AccordionTermCondition;

class TermAndCondition extends Model
{
    protected $guarded = [];
    protected $table = 'term_conditions';

    public function faqKategori()
    {
        return $this->belongsTo(FaqKategori::class, 'is_template');
    }

    public function termDetail()
    {
        return $this->hasMany(AccordionTermCondition::class, 'term_condition_id', 'id');
    }
}
