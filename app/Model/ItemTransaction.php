<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ItemTransaction extends Model
{
    protected $primaryKey = null;
    public $incrementing = false;
    
    protected $table = 'transaction_item';
    protected $fillable = [
        'orderId',
        'product_id',
        'qty',
        'harga',
        'is_produk',
        'berat'
    ];

    public function getProduct(){
        return $this->belongsTo('App\Model\Product','product_id','id')->withTrashed();
    }

    public function getpaketupgrade(){
        return $this->belongsTo('App\Model\PaketUpgradeMS','product_id','id');
    }

    public function products(){
        return $this->hasMany('App\Model\Product', 'id','product_id')->withTrashed();
    }

    public function getpaketreguler(){
        return $this->belongsTo('App\Model\PaketReguler','product_id','id');
    }
}
