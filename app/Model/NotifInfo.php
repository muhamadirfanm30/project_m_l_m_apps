<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class NotifInfo extends Model
{
    protected $table = 'notif_info';

    protected $guarded = [];

    public static function pushNotif($data)
    {
        return NotifInfo::create($data);
    }

    public static function pushNotifDataTransaksi($model_id, $role_name = null)
    {
        return NotifInfo::pushNotif([
            'model_id' => $model_id,
            'user_id' => auth()->user()->id,
            'role_name' => $role_name == null ? auth()->user()->roles : $role_name,
            'modul_name' => 'data-transactions',
        ]);
    }

    public static function readAtDataTransaksi($model_id, $role_name = null)
    {
        return NotifInfo::where('modul_name', 'data-transactions')
            ->whereNull('read_at')
            ->where('role_name', $role_name == null ? auth()->user()->roles : $role_name)
            ->where('model_id', $model_id)
            ->update([
                'read_at' => now()
            ]);
    }

    public static function pushNotifKonfirmasiPembayaran($model_id, $role_name = null)
    {
        return NotifInfo::pushNotif([
            'model_id' => $model_id,
            'user_id' => auth()->user()->id,
            'role_name' => $role_name == null ? auth()->user()->roles : $role_name,
            'modul_name' => 'konfirmasi-pembayaran',
        ]);
    }

    public static function readAtKonfirmasiPembayaran($model_id, $role_name = null)
    {
        return NotifInfo::where('modul_name', 'konfirmasi-pembayaran')
            ->whereNull('read_at')
            ->where('role_name', $role_name == null ? auth()->user()->roles : $role_name)
            ->where('model_id', $model_id)
            ->update([
                'read_at' => now()
            ]);
    }

    public static function pushNotifRegulerMember($model_id, $role_name = null)
    {
        return NotifInfo::pushNotif([
            'model_id' => $model_id,
            'user_id' => auth()->user()->id,
            'role_name' => $role_name == null ? auth()->user()->roles : $role_name,
            'modul_name' => 'input-reguler-member',
        ]);
    }

    public static function readAtRegulerMember($model_id, $role_name = null)
    {
        return NotifInfo::where('modul_name', 'input-reguler-member')
            ->whereNull('read_at')
            ->where('role_name', $role_name == null ? auth()->user()->roles : $role_name)
            ->where('model_id', $model_id)
            ->update([
                'read_at' => now()
            ]);
    }

    public static function pushNotifPaketUpgrade($model_id, $role_name = null)
    {
        return NotifInfo::pushNotif([
            'model_id' => $model_id,
            'user_id' => auth()->user()->id,
            'role_name' => $role_name == null ? auth()->user()->roles : $role_name,
            'modul_name' => 'paket-upgrade-member',
        ]);
    }

    public static function readAtPaketUpgrade($model_id, $role_name = null)
    {
        return NotifInfo::where('modul_name', 'paket-upgrade-member')
            ->whereNull('read_at')
            ->where('role_name', $role_name == null ? auth()->user()->roles : $role_name)
            ->where('model_id', $model_id)
            ->update([
                'read_at' => now()
            ]);
    }

    public static function pushNotifResiPengiriman($model_id, $role_name = null)
    {
        return NotifInfo::pushNotif([
            'model_id' => $model_id,
            'user_id' => auth()->user()->id,
            'role_name' => $role_name == null ? auth()->user()->roles : $role_name,
            'modul_name' => 'menunggu-resi-pengiriman',
        ]);
    }

    public static function readAtResiPengiriman($model_id, $role_name = null)
    {
        return NotifInfo::where('modul_name', 'menunggu-resi-pengiriman')
            ->whereNull('read_at')
            ->where('role_name', $role_name == null ? auth()->user()->roles : $role_name)
            ->where('model_id', $model_id)
            ->update([
                'read_at' => now()
            ]);
    }


    public static function pushNotifLeaderUpgrade($model_id, $role_name = null)
    {
        return NotifInfo::pushNotif([
            'model_id' => $model_id,
            'user_id' => auth()->user()->id,
            'role_name' => $role_name == null ? auth()->user()->roles : $role_name,
            'modul_name' => 'leader-upgrade-ms',
        ]);
    }

    public static function readAtLeaderUpgrade($model_id = false, $role_name = null)
    {
        return NotifInfo::where('modul_name', 'leader-upgrade-ms')
            ->whereNull('read_at')
            ->where('role_name', $role_name == null ? auth()->user()->roles : $role_name)
            ->when($model_id, function ($query, $model_id) {
                return $query->where('model_id', $model_id);
            })
            ->update([
                'read_at' => now()
            ]);
    }

    public static function pushNotifMyTeamUpgrade($model_id, $role_name = null)
    {
        return NotifInfo::pushNotif([
            'model_id' => $model_id,
            'user_id' => auth()->user()->id,
            'role_name' => $role_name == null ? auth()->user()->roles : $role_name,
            'modul_name' => 'my-team-upgrade-ms',
        ]);
    }

    public static function readAtMyTeamUpgrade($model_id = false, $role_name = 'admin')
    {
        return NotifInfo::where('modul_name', 'my-team-upgrade-ms')
            ->whereNull('read_at')
            ->where('role_name', $role_name == 'admin' ? auth()->user()->roles : $role_name)
            ->when($model_id, function ($query, $model_id) {
                return $query->where('model_id', $model_id);
            })
            ->update([
                'read_at' => now()
            ]);
    }

    public static function pushNotifNewTeamRequest($model_id, $role_name = null)
    {
        return NotifInfo::pushNotif([
            'model_id' => $model_id,
            'user_id' => auth()->user()->id,
            'role_name' => $role_name == null ? auth()->user()->roles : $role_name,
            'modul_name' => 'new-member-request',
        ]);
    }

    public static function readAtNewTeamRequest($model_id = false, $role_name = 'admin')
    {
        return NotifInfo::where('modul_name', 'new-member-request')
            ->whereNull('read_at')
            ->where('role_name', $role_name == 'admin' ? auth()->user()->roles : $role_name)
            ->when($model_id, function ($query, $model_id) {
                return $query->where('model_id', $model_id);
            })
            ->update([
                'read_at' => now()
            ]);
    }

    public static function pushNotifStokProdukOnline($model_id, $role_name = null)
    {
        return NotifInfo::pushNotif([
            'model_id' => $model_id,
            'user_id' => auth()->user()->id,
            'role_name' => $role_name == null ? auth()->user()->roles : $role_name,
            'modul_name' => 'stok-online-produk',
        ]);
    }

    public static function readAtStokProdukOnline($model_id, $role_name = null)
    {
        return NotifInfo::where('modul_name', 'stok-online-produk')
            ->whereNull('read_at')
            ->where('role_name', $role_name == null ? auth()->user()->roles : $role_name)
            ->where('model_id', $model_id)
            ->update([
                'read_at' => now()
            ]);
    }
}
